package com.kh.itda.wrapper;

import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;


//회원가입 5번 시작
//HttpServletRequestWrapper인터페이스를 상속 받아서 우리가 가공처리를 해야한다.(인터페이스는 생성을 스스로 못하니까)
public class LoginWrapper extends HttpServletRequestWrapper{
	
	public LoginWrapper(HttpServletRequest request) {
		super(request);
	}
	
	//request.getParameter()처럼 내가 정의한 getParameter로 사용하기 위해서 오버라이딩 한다.
	@Override
	public String getParameter(String key) {
		String value = "";
		
		//패스워드인경우와 패스워드 이외의 경우를 나눈다.(패스워드만 암호화해야하니까)
		if(key != null && key.equals("perPwd")) {
			//1. 부모의 parameter를 사용하여 userPwd에 대해서 리턴받아 그 값을 밑에 정의한 getSha512메소드에 넘겨준다.
			//2. 넘겨준 값을 암호화 알고리즘을 사용하여 리턴 받아서 value에 넣어준다.
			//3. 암호화된 패스워드를 다시 필터로 리턴한다.
			value = getSha512(super.getParameter("perPwd"));
		}else if(key != null && key.equals("perPwd1")) {
			//1. 부모의 parameter를 사용하여 userPwd에 대해서 리턴받아 그 값을 밑에 정의한 getSha512메소드에 넘겨준다.
			//2. 넘겨준 값을 암호화 알고리즘을 사용하여 리턴 받아서 value에 넣어준다.
			//3. 암호화된 패스워드를 다시 필터로 리턴한다.
			value = getSha512(super.getParameter("perPwd1"));
		}else if(key != null && key.equals("perPwd2")) {
			//1. 부모의 parameter를 사용하여 userPwd에 대해서 리턴받아 그 값을 밑에 정의한 getSha512메소드에 넘겨준다.
			//2. 넘겨준 값을 암호화 알고리즘을 사용하여 리턴 받아서 value에 넣어준다.
			//3. 암호화된 패스워드를 다시 필터로 리턴한다.
			value = getSha512(super.getParameter("perPwd2"));
		}else if(key != null && key.equals("comPwd1")) {
			//1. 부모의 parameter를 사용하여 userPwd에 대해서 리턴받아 그 값을 밑에 정의한 getSha512메소드에 넘겨준다.
			//2. 넘겨준 값을 암호화 알고리즘을 사용하여 리턴 받아서 value에 넣어준다.
			//3. 암호화된 패스워드를 다시 필터로 리턴한다.
			value = getSha512(super.getParameter("comPwd1"));
		}else {
			value = super.getParameter(key); //패스워드가 아니면 그냥 원래대로 사용하기 위해서 부모의 parameter을 사용해서 리턴받아서 value에 넣어준다음 다시 필터로 리턴
		}
		
		return value;
	}
	
	private static String getSha512(String pwd) {
		String encPwd = "";
		
		try {
			//SHA-512암호화 알고리즘
			//암호화 된 것은 다이제스트라고 불린다.
			//단방향 해쉬 암호화 알고리즘(암호화로서는 부적합한 알고리즘이며 복호화 기능을 제공하지 않는다.)
			MessageDigest md = MessageDigest.getInstance("SHA-512");
			byte[] bytes = pwd.getBytes(Charset.forName("UTF-8"));
			md.update(bytes);//bytes(배열)를 얕은 복사하여 암호화 후 돌려준다.
			
			//ncPwd에는 이제 암호화가 완성 된 것(다이제스트를 생성하고 어쩌고) encodeToString(md.digest())에서 배열값을 인코딩하여 가져옴? 
			encPwd = Base64.getEncoder().encodeToString(md.digest());
			
			
		} catch (NoSuchAlgorithmException e) { //알고리즘이 사전에 정의되어있지 않으면 발생하는 에러(SHA-512라는 암호화 알고리즘이 위에 적용되어 있지 않으면)
			e.printStackTrace();
		}
		
		return encPwd;
	}//회원가입 5번 끝 insertMember로 돌아감
}
