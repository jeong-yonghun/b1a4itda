package com.kh.itda.recruitAndContest.model.vo;

import java.sql.Date;

public class Announcement implements java.io.Serializable{
	private String aNo;				//공고고유번호
	private String aTitle;			//공고제목
	private Date aStartDate;		//공고시작일자
	private Date aEndDate;			//공고종료일자
	private Date aReceiptStart;		//접수시작일자
	private Date aReceiptEnd;		//접수종료일자
	private String aDivision;		//공고구분
	private String comNo;			//기업회원번호
	private String duty;			//직무
	private String scale;			//규모
	private String career;	 		//경력
	private String service;			//서비스
	private String location;		//위치
	private String introduce;		//업무소개
	private String eligibility;		//자격조건
	private String preferential;	//우대사항	
	private String presentation;	//제출서류
	private String process;			//채용절차
	private String num_emp;			//사원수
	private String invest;			//투자
	private String sales;			//매출
	private String com_nmae;		//회사명
	private String com_phone;		//담당자번호
	private String com_id;			//회사아이디
	private String com_url;			//회사홈페이지
	private String status;			//
	private String item;			//채용공고 상단하단구분
	private String skill;			
	private String changeName;		//공고 이미지
	private int val;				
	
	public Announcement() {}

	public Announcement(String aNo, String aTitle, Date aStartDate, Date aEndDate, Date aReceiptStart, Date aReceiptEnd,
			String aDivision, String comNo, String duty, String scale, String career, String service, String location,
			String introduce, String eligibility, String preferential, String presentation, String process,
			String num_emp, String invest, String sales, String com_nmae, String com_phone, String com_id,
			String com_url, String status, String item, String skill, String changeName, int val) {
		super();
		this.aNo = aNo;
		this.aTitle = aTitle;
		this.aStartDate = aStartDate;
		this.aEndDate = aEndDate;
		this.aReceiptStart = aReceiptStart;
		this.aReceiptEnd = aReceiptEnd;
		this.aDivision = aDivision;
		this.comNo = comNo;
		this.duty = duty;
		this.scale = scale;
		this.career = career;
		this.service = service;
		this.location = location;
		this.introduce = introduce;
		this.eligibility = eligibility;
		this.preferential = preferential;
		this.presentation = presentation;
		this.process = process;
		this.num_emp = num_emp;
		this.invest = invest;
		this.sales = sales;
		this.com_nmae = com_nmae;
		this.com_phone = com_phone;
		this.com_id = com_id;
		this.com_url = com_url;
		this.status = status;
		this.item = item;
		this.skill = skill;
		this.changeName = changeName;
		this.val = val;
	}

	public String getaNo() {
		return aNo;
	}

	public void setaNo(String aNo) {
		this.aNo = aNo;
	}

	public String getaTitle() {
		return aTitle;
	}

	public void setaTitle(String aTitle) {
		this.aTitle = aTitle;
	}

	public Date getaStartDate() {
		return aStartDate;
	}

	public void setaStartDate(Date aStartDate) {
		this.aStartDate = aStartDate;
	}

	public Date getaEndDate() {
		return aEndDate;
	}

	public void setaEndDate(Date aEndDate) {
		this.aEndDate = aEndDate;
	}

	public Date getaReceiptStart() {
		return aReceiptStart;
	}

	public void setaReceiptStart(Date aReceiptStart) {
		this.aReceiptStart = aReceiptStart;
	}

	public Date getaReceiptEnd() {
		return aReceiptEnd;
	}

	public void setaReceiptEnd(Date aReceiptEnd) {
		this.aReceiptEnd = aReceiptEnd;
	}

	public String getaDivision() {
		return aDivision;
	}

	public void setaDivision(String aDivision) {
		this.aDivision = aDivision;
	}

	public String getComNo() {
		return comNo;
	}

	public void setComNo(String comNo) {
		this.comNo = comNo;
	}

	public String getDuty() {
		return duty;
	}

	public void setDuty(String duty) {
		this.duty = duty;
	}

	public String getScale() {
		return scale;
	}

	public void setScale(String scale) {
		this.scale = scale;
	}

	public String getCareer() {
		return career;
	}

	public void setCareer(String career) {
		this.career = career;
	}

	public String getService() {
		return service;
	}

	public void setService(String service) {
		this.service = service;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getIntroduce() {
		return introduce;
	}

	public void setIntroduce(String introduce) {
		this.introduce = introduce;
	}

	public String getEligibility() {
		return eligibility;
	}

	public void setEligibility(String eligibility) {
		this.eligibility = eligibility;
	}

	public String getPreferential() {
		return preferential;
	}

	public void setPreferential(String preferential) {
		this.preferential = preferential;
	}

	public String getPresentation() {
		return presentation;
	}

	public void setPresentation(String presentation) {
		this.presentation = presentation;
	}

	public String getProcess() {
		return process;
	}

	public void setProcess(String process) {
		this.process = process;
	}

	public String getNum_emp() {
		return num_emp;
	}

	public void setNum_emp(String num_emp) {
		this.num_emp = num_emp;
	}

	public String getInvest() {
		return invest;
	}

	public void setInvest(String invest) {
		this.invest = invest;
	}

	public String getSales() {
		return sales;
	}

	public void setSales(String sales) {
		this.sales = sales;
	}

	public String getCom_nmae() {
		return com_nmae;
	}

	public void setCom_nmae(String com_nmae) {
		this.com_nmae = com_nmae;
	}

	public String getCom_phone() {
		return com_phone;
	}

	public void setCom_phone(String com_phone) {
		this.com_phone = com_phone;
	}

	public String getCom_id() {
		return com_id;
	}

	public void setCom_id(String com_id) {
		this.com_id = com_id;
	}

	public String getCom_url() {
		return com_url;
	}

	public void setCom_url(String com_url) {
		this.com_url = com_url;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getItem() {
		return item;
	}

	public void setItem(String item) {
		this.item = item;
	}

	public String getSkill() {
		return skill;
	}

	public void setSkill(String skill) {
		this.skill = skill;
	}

	public String getChangeName() {
		return changeName;
	}

	public void setChangeName(String changeName) {
		this.changeName = changeName;
	}

	public int getVal() {
		return val;
	}

	public void setVal(int val) {
		this.val = val;
	}

	@Override
	public String toString() {
		return "Announcement [aNo=" + aNo + ", aTitle=" + aTitle + ", aStartDate=" + aStartDate + ", aEndDate="
				+ aEndDate + ", aReceiptStart=" + aReceiptStart + ", aReceiptEnd=" + aReceiptEnd + ", aDivision="
				+ aDivision + ", comNo=" + comNo + ", duty=" + duty + ", scale=" + scale + ", career=" + career
				+ ", service=" + service + ", location=" + location + ", introduce=" + introduce + ", eligibility="
				+ eligibility + ", preferential=" + preferential + ", presentation=" + presentation + ", process="
				+ process + ", num_emp=" + num_emp + ", invest=" + invest + ", sales=" + sales + ", com_nmae="
				+ com_nmae + ", com_phone=" + com_phone + ", com_id=" + com_id + ", com_url=" + com_url + ", status="
				+ status + ", item=" + item + ", skill=" + skill + ", changeName=" + changeName + ", val=" + val + "]";
	}
	
	
	
	
	
}
