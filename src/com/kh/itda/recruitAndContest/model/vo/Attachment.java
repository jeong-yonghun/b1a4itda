package com.kh.itda.recruitAndContest.model.vo;

import java.sql.Date;

public class Attachment implements java.io.Serializable{
	
	private String fno;
	private String ano;
	private String originName;
	private String changeName;
	private String filePath;
	private Date uploadDate;
	
	public Attachment() {}

	public Attachment(String fno, String ano, String originName, String changeName, String filePath, Date uploadDate) {
		super();
		this.fno = fno;
		this.ano = ano;
		this.originName = originName;
		this.changeName = changeName;
		this.filePath = filePath;
		this.uploadDate = uploadDate;
	}

	public String getFno() {
		return fno;
	}

	public void setFno(String fno) {
		this.fno = fno;
	}

	public String getAno() {
		return ano;
	}

	public void setAno(String ano) {
		this.ano = ano;
	}

	public String getOriginName() {
		return originName;
	}

	public void setOriginName(String originName) {
		this.originName = originName;
	}

	public String getChangeName() {
		return changeName;
	}

	public void setChangeName(String changeName) {
		this.changeName = changeName;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public Date getUploadDate() {
		return uploadDate;
	}

	public void setUploadDate(Date uploadDate) {
		this.uploadDate = uploadDate;
	}

	@Override
	public String toString() {
		return "Attachment [fno=" + fno + ", ano=" + ano + ", originName=" + originName + ", changeName=" + changeName
				+ ", filePath=" + filePath + ", uploadDate=" + uploadDate + "]";
	}

	
	
	
	

}
