package com.kh.itda.recruitAndContest.model.vo;

import java.sql.Date;

public class Jyh_Attachment implements java.io.Serializable {

	private String attNo;
	private String originName;
	private String changeName;
	private String filePath;
	private Date uploadDate;
	private String tbNo;
	private String status;
	
	public Jyh_Attachment() {}

	public Jyh_Attachment(String attNo, String originName, String changeName, String filePath, Date uploadDate,
			String tbNo, String status) {
		super();
		this.attNo = attNo;
		this.originName = originName;
		this.changeName = changeName;
		this.filePath = filePath;
		this.uploadDate = uploadDate;
		this.tbNo = tbNo;
		this.status = status;
	}

	public String getAttNo() {
		return attNo;
	}

	public void setAttNo(String attNo) {
		this.attNo = attNo;
	}

	public String getOriginName() {
		return originName;
	}

	public void setOriginName(String originName) {
		this.originName = originName;
	}

	public String getChangeName() {
		return changeName;
	}

	public void setChangeName(String changeName) {
		this.changeName = changeName;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public Date getUploadDate() {
		return uploadDate;
	}

	public void setUploadDate(Date uploadDate) {
		this.uploadDate = uploadDate;
	}

	public String getTbNo() {
		return tbNo;
	}

	public void setTbNo(String tbNo) {
		this.tbNo = tbNo;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "Jyh_Attachment [attNo=" + attNo + ", originName=" + originName + ", changeName=" + changeName
				+ ", filePath=" + filePath + ", uploadDate=" + uploadDate + ", tbNo=" + tbNo + ", status=" + status
				+ "]";
	}
	
	
}
