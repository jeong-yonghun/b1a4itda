package com.kh.itda.recruitAndContest.model.vo;

import java.io.Serializable;
import java.sql.Date;

public class Item implements Serializable{
	
	private String iNo;
	private String comNo;
	private String pCode;
	private String pNo;
	private int iCount;
	private Date iValidity;
	private String pName;
	
	public Item() {}

	public Item(String iNo, String comNo, String pCode, String pNo, int iCount, Date iValidity, String pName) {
		super();
		this.iNo = iNo;
		this.comNo = comNo;
		this.pCode = pCode;
		this.pNo = pNo;
		this.iCount = iCount;
		this.iValidity = iValidity;
		this.pName = pName;
	}

	public String getiNo() {
		return iNo;
	}

	public void setiNo(String iNo) {
		this.iNo = iNo;
	}

	public String getComNo() {
		return comNo;
	}

	public void setComNo(String comNo) {
		this.comNo = comNo;
	}

	public String getpCode() {
		return pCode;
	}

	public void setpCode(String pCode) {
		this.pCode = pCode;
	}

	public String getpNo() {
		return pNo;
	}

	public void setpNo(String pNo) {
		this.pNo = pNo;
	}

	public int getiCount() {
		return iCount;
	}

	public void setiCount(int iCount) {
		this.iCount = iCount;
	}

	public Date getiValidity() {
		return iValidity;
	}

	public void setiValidity(Date iValidity) {
		this.iValidity = iValidity;
	}

	public String getpName() {
		return pName;
	}

	public void setpName(String pName) {
		this.pName = pName;
	}

	@Override
	public String toString() {
		return "Item [iNo=" + iNo + ", comNo=" + comNo + ", pCode=" + pCode + ", pNo=" + pNo + ", iCount=" + iCount
				+ ", iValidity=" + iValidity + ", pName=" + pName + "]";
	}
	
	
	

}
