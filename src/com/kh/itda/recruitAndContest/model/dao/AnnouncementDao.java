 package com.kh.itda.recruitAndContest.model.dao;

import static com.kh.itda.common.JDBCTemplate.close;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Properties;

import com.kh.itda.approval.model.vo.PageInfo;
import com.kh.itda.recruitAndContest.model.vo.Announcement;
import com.kh.itda.recruitAndContest.model.vo.Attachment;
import com.kh.itda.recruitAndContest.model.vo.Item;

public class AnnouncementDao {
	
	private Properties prop = new Properties();
	
	public AnnouncementDao() {
		
		String fileName = AnnouncementDao.class.getResource("/sql/announcement/wjs-announcement-query.properties").getPath();
		
		try {
			prop.load(new FileReader(fileName));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	

	
	
	public int insertAnn(Connection con, Announcement an) {
		
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("insertAnnouncement");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, an.getaTitle());
			pstmt.setDate(2, an.getaStartDate());
			pstmt.setDate(3, an.getaEndDate());
			pstmt.setDate(4, an.getaReceiptStart());
			pstmt.setDate(5, an.getaReceiptEnd());
			pstmt.setString(6, an.getComNo());
			
			result = pstmt.executeUpdate();
			
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			close(pstmt);
		}
		
		return result;
	}

	public int insertAnn2(Connection con, Announcement an, String ano, Item it) {
		
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("insertAnnouncement2");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, ano);
			pstmt.setString(2, an.getDuty());
			pstmt.setString(3, an.getCareer());
			pstmt.setString(4, an.getScale());
			pstmt.setString(5, an.getService());
			pstmt.setString(6, an.getLocation());
			pstmt.setString(7, an.getIntroduce());
			pstmt.setString(8, an.getEligibility());
			pstmt.setString(9, an.getPreferential());
			pstmt.setString(10, an.getPresentation());
			pstmt.setString(11, an.getProcess());
			pstmt.setString(12,	it.getpName());
			pstmt.setString(13, an.getSkill());
			
			result = pstmt.executeUpdate();
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			close(pstmt);
		}
		
		return result;
	}
	
	public String selectCurrval(Connection con) {
		
		Statement stmt = null;
		ResultSet rset = null;
		String ano = "";
		
		String query = prop.getProperty("selectCurrval");
		
		try {
			stmt = con.createStatement();
			rset = stmt.executeQuery(query);
			
			
			if(rset.next()) {
				ano = rset.getString("currval");
			}
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			close(rset);
			close(stmt);
		}
		
		return ano;
	}

	public int insertAttachment(Connection con, Attachment at) {
		
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("insertAttachment");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, at.getOriginName());
			pstmt.setString(2, at.getChangeName());
			pstmt.setString(3, at.getFilePath());
			pstmt.setString(4, at.getAno());
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			close(pstmt);
		}
		

		return result;
	}

	public int insertARecord(Connection con, String ano) {
		
		PreparedStatement pstmt = null;
		int result3 = 0;
		
		String query = prop.getProperty("insertARecord");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, ano);
			
			result3 = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			close(pstmt);
		}
		
		return result3;
	}

	public int getListCount(Connection con) {
		
		Statement stmt = null;
		ResultSet rset = null;
		
		int listCount = 0;
		
		String query = prop.getProperty("listCount");
		
		try {
			stmt = con.createStatement();
			rset = stmt.executeQuery(query);
			
			if(rset.next()) {
				listCount = rset.getInt(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			close(rset);
			close(stmt);
		}
		
		return listCount;
	}


	public ArrayList<Announcement> approvalRecruitSelect(Connection con, PageInfo pi) {
		
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<Announcement> list = null;
		
		String query = prop.getProperty("selectWithPaging");
		
		try {
			pstmt = con.prepareStatement(query);
			
			int startRow = (pi.getCurrentPage() - 1) * pi.getLimit() + 1;
			int endRow = startRow + pi.getLimit() -1;
			
			pstmt.setInt(1, startRow);
			pstmt.setInt(2, endRow);
			
			rset = pstmt.executeQuery();
			
			list = new ArrayList<>();
			
			while(rset.next()) {
				Announcement a = new Announcement();
				a.setaNo(rset.getString("A_NO"));
				a.setaDivision(rset.getString("A_DIVISION"));
				a.setDuty(rset.getString("COM_NAME"));
				a.setEligibility(rset.getString("COM_ID"));
				a.setaTitle(rset.getString("A_TITLE"));
				a.setaStartDate(rset.getDate("A_START_DATE"));
				a.setCareer(rset.getString("PHONE"));
				a.setStatus(rset.getString("STATUS"));
				
				list.add(a);
			}	
			
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			close(rset);
			close(pstmt);
		}
		
		return list;
	}

	
	public Announcement approvalSelect(Connection con,	String annNum) {
		
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		Announcement announcement = null;
		
		String query = prop.getProperty("approvalSelect");
		
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, annNum);
			
			rset = pstmt.executeQuery();
			
			announcement = new Announcement();
			
			while(rset.next()) {
				
				announcement.setaNo(annNum);
				announcement.setaTitle(rset.getString("A_TITLE"));
				announcement.setCom_nmae(rset.getString("COM_NAME"));
				announcement.setDuty(rset.getString("AA_DUTY"));
				announcement.setScale(rset.getString("AA_SCALE"));
				announcement.setService(rset.getString("AA_SERVICE"));
				announcement.setLocation(rset.getString("AA_LOCATION"));
				announcement.setIntroduce(rset.getString("AA_INTRODUCE"));
				announcement.setEligibility(rset.getString("AA_ELIGIBILITY"));
				announcement.setPreferential(rset.getString("AA_PREFERENTIAL"));
				announcement.setPresentation(rset.getString("AA_PRESENTATION"));
				announcement.setProcess(rset.getString("AA_PROCESS"));
				announcement.setCom_url(rset.getString("COM_URL"));
				announcement.setNum_emp(rset.getString("NUM_EMP"));
				announcement.setInvest(rset.getString("INVEST"));
				announcement.setSales(rset.getString("SALES"));
				announcement.setSkill(rset.getString("A_SKILL"));
				announcement.setaReceiptStart(rset.getDate("A_RECEIPT_START"));
				announcement.setaReceiptEnd(rset.getDate("A_RECEIPT_END"));
				announcement.setCareer(rset.getString("AA_CAREER"));
				
				
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			close(pstmt);
			close(rset);
		}
		
		return announcement;
	}


	public Attachment approvalSelectAttach(Connection con, String annNum) {
		
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		Attachment at = null;
		
		String query = prop.getProperty("approvalSelectAt");
		
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setString(1, annNum);
			
			rset = pstmt.executeQuery();
			
			at = new Attachment();
			
			while(rset.next()) {
				
				at.setChangeName(rset.getString("CHANGE_NAME"));
				at.setFilePath(rset.getString("FILE_PATH"));
				at.setFno(rset.getString("ATT_NO"));
				
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			close(rset);
			close(pstmt);
		}
		
		
		
		return at;
	}


	public ArrayList<Item> itemListAn(Connection con, String no) {
		
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<Item> list = null;
		
		String query = prop.getProperty("itemListAn");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, no);
			
			rset = pstmt.executeQuery();
			
			list = new ArrayList<>();
			
			while(rset.next()) {
				Item item = new Item();
				item.setiNo(rset.getString("HI_NO"));
				item.setpName(rset.getString("PD_NAME"));
				item.setiCount(rset.getInt("HI_COUNT"));
				list.add(item);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			close(rset);
			close(pstmt);
		}
		
		return list;
	}


	public int updateItem(Connection con, Item it) {
		
		PreparedStatement pstmt = null;
		int result = 0;
		String query = prop.getProperty("updateItemAn");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, it.getiCount() -1);
			System.out.println(it.getiCount() -1);
			pstmt.setString(2, it.getiNo());
			System.out.println(it.getiNo());
			result = pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			close(pstmt);
		}
		
		
		return result;
	}


	public int insertAnn2(Connection con, Announcement an, String ano) {
		

		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("insertAnnouncement2");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, ano);
			pstmt.setString(2, an.getDuty());
			pstmt.setString(3, an.getCareer());
			pstmt.setString(4, an.getScale());
			pstmt.setString(5, an.getService());
			pstmt.setString(6, an.getLocation());
			pstmt.setString(7, an.getIntroduce());
			pstmt.setString(8, an.getEligibility());
			pstmt.setString(9, an.getPreferential());
			pstmt.setString(10, an.getPresentation());
			pstmt.setString(11, an.getProcess());
			pstmt.setString(12,	an.getItem());
			pstmt.setString(13, an.getSkill());
			
			result = pstmt.executeUpdate();
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			close(pstmt);
		}
		
		return result;
		
	}




	public Announcement selectUpdateRe(Connection con, String ano, int val) {
		
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		Announcement an = null;
		
		String query = prop.getProperty("selectUpdateRe");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, ano);
			
			rset = pstmt.executeQuery();
			
			System.out.println(ano);
			
			an = new Announcement();
			while(rset.next()) {
				an.setaNo(rset.getString("A_NO"));
				an.setaTitle(rset.getString("A_TITLE"));
				an.setaStartDate(rset.getDate("A_START_DATE"));
				an.setaEndDate(rset.getDate("A_END_DATE"));
				an.setaReceiptStart(rset.getDate("A_RECEIPT_START"));
				an.setaReceiptEnd(rset.getDate("A_RECEIPT_END"));
				an.setaDivision(rset.getString("A_DIVISION"));
				an.setComNo(rset.getString("COM_NO"));
				an.setDuty(rset.getString("AA_DUTY"));
				an.setCareer(rset.getString("AA_CAREER"));
				an.setScale(rset.getString("AA_SCALE"));
				an.setService(rset.getString("AA_SERVICE"));
				an.setLocation(rset.getString("AA_LOCATION"));
				an.setIntroduce(rset.getString("AA_INTRODUCE"));
				an.setEligibility(rset.getString("AA_ELIGIBILITY"));
				an.setPreferential(rset.getString("AA_PREFERENTIAL"));
				an.setPresentation(rset.getString("AA_PRESENTATION"));
				an.setProcess(rset.getString("AA_PROCESS"));
				an.setItem(rset.getString("ITEM"));
				an.setSkill(rset.getString("A_SKILL"));
				an.setVal(val);
				
				System.out.println(an);
				
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			close(rset);
			close(pstmt);
		}
		
		return an;
	}




	public int updateAn(Connection con, Announcement an) {
		
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("updateAn");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, an.getaTitle());
			pstmt.setDate(2, an.getaStartDate());
			pstmt.setDate(3, an.getaEndDate());
			pstmt.setDate(4, an.getaReceiptStart());
			pstmt.setDate(5, an.getaReceiptEnd());
			pstmt.setString(6, an.getaNo());
			
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			close(pstmt);
		}
		
		return result;
	}




	public int updateAn2(Connection con, Announcement an) {
		
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("updateAn2");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, an.getDuty());
			pstmt.setString(2, an.getCareer());
			pstmt.setString(3, an.getScale());
			pstmt.setString(4, an.getService());
			pstmt.setString(5, an.getLocation());
			pstmt.setString(6, an.getIntroduce());
			pstmt.setString(7, an.getEligibility());
			pstmt.setString(8, an.getPreferential());
			pstmt.setString(9, an.getPresentation());
			pstmt.setString(10, an.getProcess());
			pstmt.setString(11, an.getSkill());
			pstmt.setString(12, an.getaNo());
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			close(pstmt);
		}
		
		return result;
		
	}


	public int updateAt(Connection con, Announcement an, Attachment at) {
		
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("updateAt");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, at.getOriginName());
			pstmt.setString(2, at.getChangeName());
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		
		return result;
	}




	public int insertAttachment2(Connection con, Announcement an, Attachment at) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("insertAttachment");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, at.getOriginName());
			pstmt.setString(2, at.getChangeName());
			pstmt.setString(3, at.getFilePath());
			pstmt.setString(4, an.getaNo());
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			close(pstmt);
		}
		
		return result;
	}




	public int updateRecord(Connection con, Announcement an) {
		
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query =prop.getProperty("updateRecord");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, an.getaNo());
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			close(pstmt);
		}
		
		return result;
	}




	public int updateAttachment(Connection con, Announcement an, Attachment at) {
		
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("updateAttachment");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, at.getOriginName());
			pstmt.setString(2, at.getChangeName());
			pstmt.setString(3, at.getFilePath());
			pstmt.setString(4, an.getaNo());
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			close(pstmt);
		}
		
		return result;
	}




	public ArrayList<Announcement> approvalCompetitionSelect(Connection con, PageInfo pi) {
		
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<Announcement> list = null;
		
		String query = prop.getProperty("selectWithPaging2");
		
		try {
			pstmt = con.prepareStatement(query);
			
			int startRow = (pi.getCurrentPage() - 1) * pi.getLimit() + 1;
			int endRow = startRow + pi.getLimit() -1;
			
			pstmt.setInt(1, startRow);
			pstmt.setInt(2, endRow);
			
			rset = pstmt.executeQuery();
			
			list = new ArrayList<>();
			
			while(rset.next()) {
				Announcement a = new Announcement();
				a.setaNo(rset.getString("A_NO"));
				a.setaDivision(rset.getString("A_DIVISION"));
				a.setDuty(rset.getString("COM_NAME"));
				a.setEligibility(rset.getString("COM_ID"));
				a.setaTitle(rset.getString("A_TITLE"));
				a.setaStartDate(rset.getDate("A_START_DATE"));
				a.setCareer(rset.getString("PHONE"));
				a.setStatus(rset.getString("STATUS"));
				
				list.add(a);
			}	
			
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			close(rset);
			close(pstmt);
		}
		
		return list;
		
	}
		




}
