package com.kh.itda.recruitAndContest.model.dao;

import static com.kh.itda.common.JDBCTemplate.close;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;

import com.kh.itda.approval.model.vo.PageInfo;
import com.kh.itda.recruitAndContest.model.vo.Jyh_Attachment;

public class Jyh_RecruitDao {

private Properties prop = new Properties();
private String fileName = Jyh_RecruitDao.class.getResource("/sql/recruit/jyh-recruit-query.properties").getPath();
	
	public Jyh_RecruitDao() {
		
		try {
			prop.load(new FileReader(fileName));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public int insertAttachment(Connection con, Jyh_Attachment jyh_Attachment) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("insertAttachment");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, jyh_Attachment.getOriginName());
			pstmt.setString(2, jyh_Attachment.getChangeName());
			pstmt.setString(3, jyh_Attachment.getFilePath());
			
			result = pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}

	public int insertCompetitionAnn(Connection con, HashMap<String, Object> competitionMap) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		
		
		String query = prop.getProperty("insertCompetitionAnn");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, (String) competitionMap.get("title"));
			pstmt.setString(2, (String) competitionMap.get("aStartDate"));
			pstmt.setString(3, (String) competitionMap.get("aEndDate"));
			pstmt.setString(4, (String) competitionMap.get("aReceiptStart"));
			pstmt.setString(5, (String) competitionMap.get("aReceiptEnd"));
			pstmt.setString(6, (String) competitionMap.get("userNo"));
			pstmt.setString(7, (String) competitionMap.get("contents"));
			
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}

	public int selectCurrval(Connection con) {
		Statement stmt = null;
		ResultSet rset = null;
		int aNo = 0;
		
		String query = prop.getProperty("selectCurrval");
		
		try {
			stmt = con.createStatement();
			rset = stmt.executeQuery(query);
			
			if(rset.next()) {
				aNo = rset.getInt("currval");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(stmt);
			close(rset);
		}
		
		return aNo;
	}

	public int insertCompetitionDetails(Connection con, HashMap<String, Object> competitionMap, int aNo) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("insertCompetitionDetails");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, Integer.toString(aNo));
			pstmt.setString(2, (String) competitionMap.get("targetArr"));
			pstmt.setString(3, (String) competitionMap.get("requirements"));
			pstmt.setString(4, (String) competitionMap.get("progressStart"));
			pstmt.setString(5, (String) competitionMap.get("progressEnd"));
			pstmt.setString(6, (String) competitionMap.get("privilegeArr"));
			pstmt.setString(7, (String) competitionMap.get("guideArr"));
			pstmt.setString(8, (String) competitionMap.get("submissionDate"));
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}

	public HashMap<String, Object> selectCompetitionContent(Connection con, String annNo) {
		PreparedStatement pstmt = null;
		HashMap<String, Object> hmap = null;
		ResultSet rset = null;
		
		String query = prop.getProperty("selectCompetitionContent");
		
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setString(1, annNo);
			
			rset = pstmt.executeQuery();
			
			while(rset.next()) {
				hmap = new HashMap<String, Object>();
				
				hmap.put("aNo", rset.getString("A_NO"));
				hmap.put("title", rset.getString("A_TITLE"));
				hmap.put("receiptStart", rset.getDate("A_RECEIPT_START"));
				hmap.put("receiptEnd", rset.getDate("A_RECEIPT_END"));
				hmap.put("progressStart", rset.getDate("PROGRESS_START"));
				hmap.put("progressEnd", rset.getDate("PROGRESS_END"));
				hmap.put("contents", rset.getString("A_CONTENTS"));
				hmap.put("target", rset.getString("CPTT_TARGET"));
				hmap.put("submissionDate", rset.getDate("SUBMISSION_DATE"));
				hmap.put("admissionGuide", rset.getString("ADMISSION_GUIDE"));
				hmap.put("companyName", rset.getString("COMPANY_NAME"));
				hmap.put("aDivision", rset.getString("A_DIVISION"));
				hmap.put("division", rset.getString("DIVISION"));
				
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(rset);
			close(pstmt);
		}
		
		return hmap;
	}

	public int insertApplication(Connection con, HashMap<String, String> applicationHmap) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("insertCompetitionApplication");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, applicationHmap.get("userNo"));
			pstmt.setString(2, applicationHmap.get("aNo"));
			pstmt.setString(3, applicationHmap.get("division"));
			pstmt.setString(4, applicationHmap.get("career"));
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}

	public int changeQueryAnn(StringBuilder changeQuery, StringBuilder changeListCount) {
		int result = 0;
		
		prop.setProperty("changeQueryAnn", changeQuery.toString());
		prop.setProperty("changeListCountAnn", changeListCount.toString());
		
		OutputStream stream = null;
		try {
			stream = new FileOutputStream(fileName);
			prop.store(stream, "Server Info");
			
			result = 1;
			
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				stream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		return result;
	}

	public int getChangeListCountAnn(Connection con) {
		Statement stmt = null;
		ResultSet rset = null;
		
		int listCount = 0;

		String query = prop.getProperty("changeListCountAnn");
		
		try {
			stmt = con.createStatement();
			rset = stmt.executeQuery(query);
			
			if(rset.next()) {
				listCount = rset.getInt(1);
			}
			
			System.out.println(listCount);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(stmt);
			close(rset);
		}
		
		return listCount;
	}

	public ArrayList<HashMap<String, Object>> selectAnnouncementList(Connection con, PageInfo pi) {
		PreparedStatement pstmt = null;
		ArrayList<HashMap<String, Object>> list = null;
		HashMap<String, Object> hmap = null;
		ResultSet rset = null;
		
		String query = prop.getProperty("changeQueryAnn");
		
		try {
			pstmt = con.prepareStatement(query);
			int startRow = (pi.getCurrentPage() - 1) * pi.getLimit() + 1; //시작 페이지 -> 번호 무조건 10단위로 만든다음 +1을 시켜주는 계산식
			int endRow = startRow + pi.getLimit() - 1; //끝 페이지 -> 번호 무조건 10단위로 만든다음 -1을 시켜주는 계산식
			
			pstmt.setInt(1, startRow);
			pstmt.setInt(2, endRow);
			
			rset = pstmt.executeQuery();
			
			list = new ArrayList<HashMap<String, Object>>();
			
			while(rset.next()) {
				hmap = new HashMap<String, Object>();
				
				hmap.put("rnum", rset.getInt("RNUM"));
				hmap.put("comNo", rset.getString("COM_NO"));
				hmap.put("comId", rset.getString("COM_ID"));
				hmap.put("comName", rset.getString("COM_NAME"));
				hmap.put("companyName", rset.getString("COMPANY_NAME"));
				hmap.put("aTitle", rset.getString("A_TITLE"));
				hmap.put("phone", rset.getString("PHONE"));
				hmap.put("aStartDate", rset.getDate("A_START_DATE"));
				hmap.put("aEndDate", rset.getDate("A_END_DATE"));
				hmap.put("status", rset.getString("STATUS"));
				hmap.put("aNo", rset.getString("A_NO"));
				hmap.put("reDate", rset.getDate("RE_DATE"));
				hmap.put("aDivision", rset.getString("A_DIVISION"));
				
				list.add(hmap);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(rset);
			close(pstmt);
		}
		
		return list;
	}

	public int insertAnnRecord(Connection con, int aNo) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("insertAnnRecord");
		
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setInt(1, aNo);
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}

	public int insertSkill(Connection con, int aNo, ArrayList<String> skillCodeArr) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("insertSkill");
		
		try {
			pstmt = con.prepareStatement(query);
			
			
			for(int i = 0; i < skillCodeArr.size(); i++) {
				pstmt.setInt(1, aNo);
				pstmt.setString(2, skillCodeArr.get(i));
				result = pstmt.executeUpdate();
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}
	
	public ArrayList<String> selectSkillCode(Connection con, HashMap<String, Object> competitionMap) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		String[] skillArr = null;
		ArrayList<String> skillCodeArr = null;
		
		String query = prop.getProperty("selectSkillCode");
		
		try {
			pstmt = con.prepareStatement(query);
			skillArr = (String[]) competitionMap.get("skillArr");
			skillCodeArr = new ArrayList<String>();
			for(int i = 0; i < skillArr.length; i++) {
				pstmt.setString(1, skillArr[i]);
				rset = pstmt.executeQuery();
				if(rset.next()) {
					skillCodeArr.add(rset.getString(1));
				}
			}
			
			System.out.println("skillCodeArr == " + skillCodeArr);
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return skillCodeArr;
	}

	public int insertPosition(Connection con, int aNo, HashMap<String, Object> competitionMap) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("insertPosition");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, aNo);
			pstmt.setString(2, (String) competitionMap.get("position"));
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}

	public ArrayList<String> selectCompetitionSkill(Connection con, String annNo) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<String> skillArr = null;
		
		String query = prop.getProperty("selectCompetitionSkill");
		
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setString(1, annNo);
			
			rset = pstmt.executeQuery();
			
			skillArr = new ArrayList<>();
			while(rset.next()) {
				skillArr.add(rset.getString("SKILL_NAME"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(rset);
			close(pstmt);
		}
		
		return skillArr;
	}

	public String selectCompetitionPosition(Connection con, String annNo) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		String position = null;
		
		String query = prop.getProperty("selectCompetitionPosition");
		
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setString(1, annNo);
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				position = rset.getString("POSITION_NAME");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(rset);
			close(pstmt);
		}
		
		return position;
	}

	public int applicationYN(Connection con, String userNo, String annNo) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int result = 0;
		
		String query = prop.getProperty("applicationYN");
		
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setString(1, userNo);
			pstmt.setString(2, annNo);
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				result = rset.getInt(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(rset);
			close(pstmt);
		}
		
		return result;
	}

	public String submitResultYN(Connection con, String userNo, String aNo) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		String submitResultYN = null;
		
		String query = prop.getProperty("submitResultYN");
		
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setString(1, userNo);
			pstmt.setString(2, aNo);
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				submitResultYN = rset.getString(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(rset);
			close(pstmt);
		}
		
		return submitResultYN;
	}

	public HashMap<String, Object> selectTestContent(Connection con, String userNo, String aNo) {
		PreparedStatement pstmt = null;
		HashMap<String, Object> hmap = null;
		ResultSet rset = null;
		
		String query = prop.getProperty("selectTestContent");
		
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setString(1, aNo);
			pstmt.setString(2, userNo);
			
			rset = pstmt.executeQuery();
			
			while(rset.next()) {
				hmap = new HashMap<String, Object>();
				
				hmap.put("aNo", rset.getString("A_NO"));
				hmap.put("perNo", rset.getString("PER_NO"));
				hmap.put("applyDate", rset.getDate("APPLY_DATE"));
				hmap.put("cpttDivision", rset.getString("CPTT_DIVISION"));
				hmap.put("career", rset.getString("CAREER"));
				hmap.put("submission", rset.getString("SUBMISSION"));
				hmap.put("cpttTarget", rset.getString("CPTT_TARGET"));
				hmap.put("type", rset.getString("TYPE"));
				hmap.put("requirements", rset.getString("REQUIREMENTS"));
				hmap.put("progressStart", rset.getDate("PROGRESS_START"));
				hmap.put("progressEnd", rset.getDate("PROGRESS_END"));
				hmap.put("privilege", rset.getString("PRIVILEGE"));
				hmap.put("admissionGuide", rset.getString("ADMISSION_GUIDE"));
				hmap.put("submissionDate", rset.getDate("SUBMISSION_DATE"));
				hmap.put("perName", rset.getString("PER_NAME"));
				hmap.put("email", rset.getString("EMAIL"));
				hmap.put("company", rset.getString("COMPANY_NAME"));
				hmap.put("position", rset.getString("POSITION_NAME"));
				
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(rset);
			close(pstmt);
		}
		
		return hmap;
	}

	public int updateApplicationYN(Connection con, HashMap<String, String> applicationHmap) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("updateApplicationYN");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, "Y");
			pstmt.setString(2, applicationHmap.get("aNo"));
			pstmt.setString(3, applicationHmap.get("userNo"));
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}

	public int insertAnnURL(Connection con, int aNo, HashMap<String, Object> competitionMap) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("insertAnnURL");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, aNo);
			pstmt.setString(2, (String) competitionMap.get("imgSrc"));
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}

	public String selectAnnURL(Connection con, String annNo) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		String imgUrl = null;
		
		String query = prop.getProperty("selectAnnURL");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, annNo);
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				imgUrl = rset.getString("URL");
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return imgUrl;
	}

	public int insertTestResult(Connection con, String userNo, String aNo, String github) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("insertTestResult");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, aNo);
			pstmt.setString(2, userNo);
			pstmt.setString(3, github);
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}

	public int upDatesubmitResultYN(Connection con, String userNo, String aNo, String Y) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("upDatesubmitResultYN");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, Y);
			pstmt.setString(2, aNo);
			pstmt.setString(3, userNo);
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}

	public ArrayList<HashMap<String, Object>> selectCompetitionsList(Connection con) {
		Statement stmt = null;
		ResultSet rset = null;
		HashMap<String, Object> hmap = null;
		ArrayList<HashMap<String, Object>> list = null;
		
		String query = prop.getProperty("selectCompetitionsList");
		
		try {
			stmt = con.createStatement();
			
			
			rset = stmt.executeQuery(query);
			
			list = new ArrayList<HashMap<String, Object>>();
			while(rset.next()) {
				hmap = new HashMap<String, Object>();
				
				hmap.put("aNo", rset.getString("A_NO"));
				hmap.put("title", rset.getString("A_TITLE"));
				hmap.put("receiptStart", rset.getDate("A_RECEIPT_START"));
				hmap.put("receiptEnd", rset.getDate("A_RECEIPT_END"));
				hmap.put("progressStart", rset.getDate("PROGRESS_START"));
				hmap.put("progressEnd", rset.getDate("PROGRESS_END"));
				hmap.put("url", rset.getString("URL"));
				
				list.add(hmap);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(rset);
			close(stmt);
		}
		
		return list;
	}
}