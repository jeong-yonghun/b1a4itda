package com.kh.itda.recruitAndContest.model.service;

import static com.kh.itda.common.JDBCTemplate.close;
import static com.kh.itda.common.JDBCTemplate.commit;
import static com.kh.itda.common.JDBCTemplate.getConnection;
import static com.kh.itda.common.JDBCTemplate.rollback;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;

import com.kh.itda.approval.model.vo.PageInfo;
import com.kh.itda.member.model.dao.Jyh_MemberDao;
import com.kh.itda.recruitAndContest.model.dao.Jyh_RecruitDao;
import com.kh.itda.recruitAndContest.model.vo.Jyh_Attachment;

public class Jyh_RecruitService {

	public int insertImage(ArrayList<Jyh_Attachment> fileList) {
		Connection con = getConnection();

		int result2 = 0;
		
		for (int i = 0; i < fileList.size(); i++) { // 파일의 갯수만큼 Dao에 왔다갔다 한다. 파일이 4개이면 위에서 Dao 두 번 갔다오고(고정), 파일 저장이
													// 4번(가변)
			result2 += new Jyh_RecruitDao().insertAttachment(con, fileList.get(i));
		}

		int result = 0;
		if (result2 == fileList.size()) {
			commit(con);
			result = 1;
		} else {
			rollback(con);
			result = 0;
		}
		close(con);
		return result;
	}

	public int insertCompetition(HashMap<String, Object> competitionMap) {
		Connection con = getConnection();
		int result = 0;
		int result2 = 0;
		int result3 = 0;
		int result4 = 0;
		int result5 = 0;
		int result6 = 0;
		
		int result1 = new Jyh_RecruitDao().insertCompetitionAnn(con, competitionMap);
		
		if(result1 > 0) {
			int aNo = new Jyh_RecruitDao().selectCurrval(con); // bid는 파일이 어느 참조 번호를 가져야하는지 알기위해서 가져옴(어느 게시물의 파일인지).
			
			if(aNo > 0) {//시퀀스를 읽어온 경우(파일이 있는 경우)
					result2 = new Jyh_RecruitDao().insertCompetitionDetails(con, competitionMap, aNo);
					if(result2 > 0) {
						ArrayList<String> skillCodeArr = new Jyh_RecruitDao().selectSkillCode(con, competitionMap);
						result4 = new Jyh_RecruitDao().insertSkill(con, aNo, skillCodeArr);
						result5 = new Jyh_RecruitDao().insertPosition(con, aNo, competitionMap);
						result6 = new Jyh_RecruitDao().insertAnnRecord(con, aNo);
						result6 = new Jyh_RecruitDao().insertAnnURL(con, aNo, competitionMap);
					}
				}
			}
		if(result1 > 0 && result2 > 0 && result6 > 0) {
			commit(con);
			result = 1;
		} else {
			rollback(con);
			result = 0;
		}
		close(con);
		return result;
	}

	public ArrayList<Object> selectCompetitionContent(String annNo) {
		Connection con = getConnection();
		ArrayList<Object> list = new ArrayList<>();
		
		HashMap<String, Object> CPTTContent = new Jyh_RecruitDao().selectCompetitionContent(con, annNo);
		ArrayList<String> skillArr = new Jyh_RecruitDao().selectCompetitionSkill(con, annNo);
		String position = new Jyh_RecruitDao().selectCompetitionPosition(con, annNo);
		String imgUrl = new Jyh_RecruitDao().selectAnnURL(con, annNo);
		
		list.add(CPTTContent);
		list.add(skillArr);
		list.add(position);
		list.add(imgUrl);
		
		close(con);
		
		return list;
	}

	public int insertApplication(HashMap<String, String> applicationHmap) {
		Connection con = getConnection();
		int result = 0;
		int result1 = 0;
		
		result1 = new Jyh_RecruitDao().insertApplication(con, applicationHmap);
		
		if(result1 > 0) {
			result = new Jyh_RecruitDao().updateApplicationYN(con, applicationHmap);
		}
		
		if(result1 > 0 && result > 0) { //둘 다 만족해야 파일이 다 저장된 것?
			commit(con);
		} else {
			rollback(con);
		}
		close(con);
		return result;
	}

	public int changeQueryAnn(StringBuilder changeQuery, StringBuilder changeListCount) {
		int result = new Jyh_RecruitDao().changeQueryAnn(changeQuery, changeListCount);
		
		return result;
	}

	public int getChangeListCount() {
		Connection con = getConnection();
		
		
		int	listCount = new Jyh_RecruitDao().getChangeListCountAnn(con);
		
		close(con);
		
		return listCount;
	}

	public ArrayList<HashMap<String, Object>> selectAnnouncementList(PageInfo pi) {
		Connection con = getConnection();
		
		ArrayList<HashMap<String, Object>> list = new Jyh_RecruitDao().selectAnnouncementList(con, pi);
		
		close(con);
		
		return list;
	}

	public int applicationYN(String userNo, String annNo) {
		Connection con = getConnection();
		
		int result = new Jyh_RecruitDao().applicationYN(con, userNo, annNo);
		
		close(con);
		
		return result;
	}

	public String submitResultYN(String userNo, String aNo) {
		Connection con = getConnection();
		
		String submitResultYN = new Jyh_RecruitDao().submitResultYN(con, userNo, aNo);
		
		close(con);
		
		return submitResultYN;
	}

	public HashMap<String, Object> selectTestContent(String userNo, String aNo) {
		Connection con = getConnection();
		
		HashMap<String, Object> testContent = new Jyh_RecruitDao().selectTestContent(con, userNo, aNo);
		
		
		close(con);
		
		return testContent;
	}

	public int insertTestResult(String userNo, String aNo, String github) {
		Connection con = getConnection();
		
		int result = new Jyh_RecruitDao().insertTestResult(con, userNo, aNo, github);
		int result1 = new Jyh_RecruitDao().upDatesubmitResultYN(con, userNo, aNo, "Y");
		
		if(result > 0 && result1 > 0) {
			commit(con);
		}else {
			rollback(con);
		}
		
		close(con);
		
		return result;
	}

	public ArrayList<HashMap<String, Object>> selectCompetitionsList() {
		Connection con = getConnection();
		
		ArrayList<HashMap<String, Object>> list = new Jyh_RecruitDao().selectCompetitionsList(con);
		
		close(con);
		
		return list;
	}
	
}