package com.kh.itda.recruitAndContest.model.service;

import static com.kh.itda.common.JDBCTemplate.close;
import static com.kh.itda.common.JDBCTemplate.commit;
import static com.kh.itda.common.JDBCTemplate.getConnection;
import static com.kh.itda.common.JDBCTemplate.rollback;

import java.sql.Connection;
import java.util.ArrayList;

import com.kh.itda.approval.model.vo.PageInfo;
import com.kh.itda.member.controller.Wjs_SelectAnnounceMentServlet;
import com.kh.itda.member.model.dao.Wjs_MemberDao;
import com.kh.itda.recruitAndContest.model.dao.AnnouncementDao;
import com.kh.itda.recruitAndContest.model.vo.Announcement;
import com.kh.itda.recruitAndContest.model.vo.Attachment;
import com.kh.itda.recruitAndContest.model.vo.Item;

public class AnnouncementService {

	public int insertAnn(Announcement an, Attachment at, Item it) {
		
		Connection con = getConnection();
		
		int result1 = new AnnouncementDao().insertAnn(con, an);
		
		int result2 = 0;
		int result3 = 0;
		int result4 = 0;
		
		
		
		if(result1 > 0 ) {
			String ano = new AnnouncementDao().selectCurrval(con);
			ano = "T"+ano;
			int result11 = new AnnouncementDao().insertAnn2(con,an,ano,it);
			
			
			if(ano != null && result11 >0) {
				
				System.out.println(ano);
				at.setAno(ano);
				result2 = new AnnouncementDao().insertAttachment(con, at);
				System.out.println("result2 :" + result2);
				result3 = new AnnouncementDao().insertARecord(con, ano);
				System.out.println("result3 :" + result3);
				result4 = new AnnouncementDao().updateItem(con,it);
				System.out.println("result4 :" + result4);
				
			}
		}
		
		int result = 0;
		
		if(result1 > 0 && result2 > 0 && result3 > 0 && result4 >0) {
			commit(con);
			result = 1;
		}else {
			rollback(con);
			result = 0;
		}
		
		close(con);
		
		return result;
	}
	

	public int getListCount() {
		
		Connection con = getConnection();
		
		int listCount = new AnnouncementDao().getListCount(con);
		
		close(con);
		
		return listCount;
	}

	public ArrayList<Announcement> approvalRecruitSelect(PageInfo pi) {
		
		Connection con = getConnection();
		
		ArrayList<Announcement> list = new AnnouncementDao().approvalRecruitSelect(con, pi);
		
		close(con);
		
		return list;
	}

	public Announcement approvalSelect(String annNum) {
		
		Connection con =  getConnection();
		
		Announcement announcement = new AnnouncementDao().approvalSelect(con, annNum);
		
		
		
		close(con);
		
		return announcement;
	}

	public Attachment approvalSelectAttach(String annNum) {
		
		Connection con = getConnection();
		
		Attachment at = new AnnouncementDao().approvalSelectAttach(con, annNum);
		
		close(con);
		
		return at;
	}

	public ArrayList<Item> itemListAn(String no) {
		
		Connection con = getConnection();
		
		ArrayList<Item> list = new AnnouncementDao().itemListAn(con, no);
		
		close(con);
		
		return list;
	}

	public int insertAnn(Announcement an, Attachment at) {
		

		Connection con = getConnection();
		
		int result1 = new AnnouncementDao().insertAnn(con, an);
		
		int result2 = 0;
		int result3 = 0;
		
		if(result1 > 0 ) {
			String ano = new AnnouncementDao().selectCurrval(con);
			ano = "T"+ano;
			int result11 = new AnnouncementDao().insertAnn2(con,an,ano);
			
			if(ano != null && result11 >0) {
				
				System.out.println(ano);
				at.setAno(ano);
				result2 = new AnnouncementDao().insertAttachment(con, at);
				System.out.println("result2 :" + result2);
				result3 = new AnnouncementDao().insertARecord(con, ano);
				System.out.println("result3 :" + result3);
			}
		}
		
		int result = 0;
		
		if(result1 > 0 && result2 > 0 && result3 > 0) {
			commit(con);
			result = 1;
		}else {
			rollback(con);
		}
		
		close(con);
		
		return result;
	}


	public Announcement selectUpdateRe(String ano, int val) {
		
		Connection con = getConnection();
		
		Announcement an = new AnnouncementDao().selectUpdateRe(con, ano, val);
		
		close(con);
		
		return an;
	}


	public int updateAn(Announcement an, Attachment at) {
		
		Connection con = getConnection();
		
		int result = 0;
		
		int result1  = new AnnouncementDao().updateAn(con,an);
		int result2 = new AnnouncementDao().updateAn2(con,an);
		int result3 = new AnnouncementDao().updateAttachment(con, an, at);
		int result4 = new AnnouncementDao().updateRecord(con,an);
		
		System.out.println("result1 :" + result1);
		System.out.println("result2 :" + result2);
		System.out.println("result1 :" + result3);
		System.out.println("result1 :" + result4);
		if(result1 > 0 && result2 >0 && result3 > 0 && result4 > 0) {
			commit(con);
			result = 1;
		}else {
			rollback(con);
		}
		
		return result;
	}


	public ArrayList<Announcement> approvalCompetitionSelect(PageInfo pi) {
		
		Connection con = getConnection();
		
		ArrayList<Announcement> list = new AnnouncementDao().approvalCompetitionSelect(con, pi);
		
		close(con);
		
		return list;
		
	}


	public int insertAttach(Attachment at) {
		
		Connection con = getConnection();
		
		int result = new AnnouncementDao().insertAttachment(con, at);
		
		if(result > 0) {
			commit(con);
		}else {
			rollback(con);
		}
		
		return result;
	}


}
