package com.kh.itda.recruitAndContest.controller;

import java.io.IOException;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kh.itda.member.model.vo.Jyh_ComMember;

/**
 * Servlet implementation class Jyh_CompetitionMiriServlet
 */
@WebServlet("/jyhMiri.re")
public class Jyh_CompetitionMiriServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public Jyh_CompetitionMiriServlet() {
        super();
    }
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		Jyh_ComMember loginUser= (Jyh_ComMember) request.getSession().getAttribute("loginUser");
		
		String companyName = loginUser.getCompanyName();
		String title = request.getParameter("title");
		String contents = request.getParameter("summernote");
		String[] targetArr = request.getParameterValues("target-li");
	//	String[] positionArr = request.getParameterValues("position-li");
		String position = request.getParameter("position");
		String[] skillArr = request.getParameterValues("skill-li");
		String[] guideArr = request.getParameterValues("guide-li");
		String requirements = request.getParameter("summernote1");
		String[] linkArr = request.getParameterValues("link-li");
		String[] fileArr = request.getParameterValues("file-li");
		String[] privilegeArr = request.getParameterValues("privilege-li");
		String aStartDate = request.getParameter("AnnouncementDate1");
		String aEndDate = request.getParameter("AnnouncementDate2");
		String aReceiptStart = request.getParameter("receiptDate1");
		String aReceiptEnd = request.getParameter("receiptDate2");
		String progressStart = request.getParameter("progressDate1");
		String progressEnd = request.getParameter("progressDate2");
		String submissionDate = request.getParameter("submissionDate");
		String imgSrc = request.getParameter("imgSrc");
		
		String target = null;
//		String position = null;
		String skill = null;
		String guide = null;
		String link = null;
		String file = null;
		String privilege = null;
		
		/*if(targetArr != null) {
			target = String.join(",", targetArr);
		}*/
		/*if(positionArr != null) {
			position = String.join(",", positionArr);
		}*/
		if(skillArr != null) {
			skill = String.join(",", skillArr);
		}
		if(guideArr != null) {
			guide = String.join(",", guideArr);
		}
		if(linkArr != null) {
			link = String.join(",", linkArr);
		}
		if(fileArr != null) {
			file = String.join(",", fileArr);
		}
		if(privilegeArr != null) {
			privilege = String.join(",", privilegeArr);
		}
		
		System.out.println(aStartDate);
		System.out.println(aEndDate);
		System.out.println(aReceiptStart);
		System.out.println(aReceiptEnd);
		System.out.println(progressStart);
		System.out.println(progressEnd);
		System.out.println(submissionDate);
		
		HashMap<String, Object> competitionMap= new HashMap<String, Object>();
		
		competitionMap.put("title", title);
		competitionMap.put("contents", contents);
		//competitionMap.put("targetArr", target);
		competitionMap.put("position", position);
		competitionMap.put("skillArr", skillArr);
		competitionMap.put("guideArr", guide);
		competitionMap.put("requirements", requirements);
		competitionMap.put("linkArr", link);
		competitionMap.put("fileArr", file);
		competitionMap.put("privilegeArr", privilege);
		competitionMap.put("aStartDate", aStartDate);
		competitionMap.put("aEndDate", aEndDate);
		competitionMap.put("aReceiptStart", aReceiptStart);
		competitionMap.put("aReceiptEnd", aReceiptEnd);
		competitionMap.put("progressStart", progressStart);
		competitionMap.put("progressEnd", progressEnd);
		competitionMap.put("submissionDate", submissionDate);
		competitionMap.put("imgUrl", imgSrc);
		
		System.out.println(imgSrc);
		
		String page = "";
		if (competitionMap != null) {
			page = "views/company/com_competition/jyh_competitionMiri.jsp";
			request.setAttribute("content", competitionMap);
			if(skillArr != null) {
				request.setAttribute("skillArr", skillArr);
			}
			if(targetArr != null) {
				request.setAttribute("targetArr", targetArr);
			}
			request.setAttribute("position", position);
			request.setAttribute("applyYn", 0);
			request.setAttribute("imgUrl", imgSrc);
		} else {
			page = "views/common/errorPage.jsp";
			request.setAttribute("msg", "공모전 내용 불러오기 실패!");
		}
		request.getRequestDispatcher(page).forward(request, response);
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
