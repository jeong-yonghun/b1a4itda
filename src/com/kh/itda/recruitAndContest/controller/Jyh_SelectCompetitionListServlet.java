package com.kh.itda.recruitAndContest.controller;


import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kh.itda.recruitAndContest.model.service.Jyh_RecruitService;

@WebServlet("/selectCompetitionList.re")
public class Jyh_SelectCompetitionListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public Jyh_SelectCompetitionListServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		ArrayList<HashMap<String, Object>> list = new Jyh_RecruitService().selectCompetitionsList();
		

		System.out.println("list : " + list);

		String page = "";
		if (list != null) {
			page = "views/company/com_competition/jyh_hireCompetitionsList.jsp";
			request.setAttribute("list", list);
		} else {
			page = "views/common/errorPage.jsp";
			request.setAttribute("msg", "공모전 내용 불러오기 실패!");
		}
		request.getRequestDispatcher(page).forward(request, response);
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
