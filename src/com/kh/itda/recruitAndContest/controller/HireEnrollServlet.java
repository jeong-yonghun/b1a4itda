package com.kh.itda.recruitAndContest.controller;

import java.io.File;
import java.io.IOException;
import java.util.Enumeration;
import java.util.GregorianCalendar;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;

import com.kh.itda.common.MyFileRenamePolicy;
import com.kh.itda.member.model.vo.Jyh_ComMember;
import com.kh.itda.recruitAndContest.model.service.AnnouncementService;
import com.kh.itda.recruitAndContest.model.vo.Announcement;
import com.kh.itda.recruitAndContest.model.vo.Attachment;
import com.kh.itda.recruitAndContest.model.vo.Item;
import com.oreilly.servlet.MultipartRequest;


@WebServlet("/hire.re")
public class HireEnrollServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
   
    public HireEnrollServlet() {
        super();
    }

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		if(ServletFileUpload.isMultipartContent(request)) {
			
			int maxSize = 1024 * 1024 * 10;
			
			String root = request.getSession().getServletContext().getRealPath("/");
			
			System.out.println(root);
			
			String savePath = root + "uploadFiles/";
			
			MultipartRequest multiRequest = 
					new MultipartRequest(request, savePath, maxSize, "UTF-8", new MyFileRenamePolicy());
			
			
			
			Enumeration<String> file = multiRequest.getFileNames();
			
			String name = file.nextElement();
			
			String saveFile =  multiRequest.getFilesystemName(name);
			String originFile = multiRequest.getOriginalFileName(name);
			
			System.out.println(saveFile);
			System.out.println(originFile);
			
			
			String title = multiRequest.getParameter("title");
			String duty = multiRequest.getParameter("jikmu");
			String career1 = multiRequest.getParameter("gyunglyuk");
			String career2 = multiRequest.getParameter("gyunglyuk1");
			String career = career1 + "~" + career2;
			String scale = multiRequest.getParameter("gyumo");
			String service = multiRequest.getParameter("service");
			String startDate = multiRequest.getParameter("time1");
			String endDate = multiRequest.getParameter("time2");
			String receiptStart = multiRequest.getParameter("time3");
			String receiptEnd = multiRequest.getParameter("time4");
			String number = multiRequest.getParameter("number");
			String road = multiRequest.getParameter("roadAddress");
			String detail = multiRequest.getParameter("detailAddress");
			String location = number+ "," +  road + "," + detail;
			

			String[] target = multiRequest.getParameterValues("target");
			String eligibility1 = String.join(",", target);
			String eligibility = eligibility1.replaceFirst(",", "");
			
			String[] target1 = multiRequest.getParameterValues("target");
			String preferential1 = String.join(",", target1);
			String preferential = preferential1.replaceFirst(",", "");
			
			
			String[] target3 = multiRequest.getParameterValues("target3");
			String target33 = String.join(",", target3);
			String presentation =  target33.replaceFirst(",", "");;
			
			String[] target4 = multiRequest.getParameterValues("target4");
			String process1 = String.join(",", target4);
			String process = process1.replaceFirst(",", "");
			
			
			String[] target5 = multiRequest.getParameterValues("target5");
			String introduce1 = String.join(",", target5);
			String introduce = introduce1.replaceFirst(",", "");
			
			
			String[] tags = multiRequest.getParameterValues("tags");
			String skill2 = String.join(",", tags);
			String skill = skill2.replaceFirst(",", "");
			System.out.println("skill : " + skill);
				
			String select = multiRequest.getParameter("select");
			System.out.println("select :" +select);
			String[] select2 = select.split(",");
			String item1 = select2[0];
			String item2 = select2[1];
			String item22 = select2[2];
			System.out.println(item1);
			System.out.println(item2);
			
			int result =0;
			
			int item3 = 0;
			if(item22.equals("채용공고상단") || item22.equals("채용공고중단")) {
				item3 = Integer.parseInt(item2);
				
				System.out.println("채용공고하단아님");
				
				java.sql.Date day = null;
				java.sql.Date day1 = null;
				java.sql.Date day2= null;
				java.sql.Date day3 = null;
				
				if(startDate !=  "") {
					day = java.sql.Date.valueOf(startDate);
					
				}else {
					day = new java.sql.Date(new GregorianCalendar().getTimeInMillis());
				}
				if(endDate !=  "") {
					day1 = java.sql.Date.valueOf(endDate);
					
				}else {
					day1 = new java.sql.Date(new GregorianCalendar().getTimeInMillis());
				}
				if(receiptStart !=  "") {
					day2 = java.sql.Date.valueOf(receiptStart);
					
				}else {
					day2 = new java.sql.Date(new GregorianCalendar().getTimeInMillis());
				}
				if(receiptEnd !=  "") {
					day3 = java.sql.Date.valueOf(receiptEnd);
					
				}else {
					day3 = new java.sql.Date(new GregorianCalendar().getTimeInMillis());
				}
				
				
				/*Wjs_ComMember loginUser=(Wjs_ComMember)request.getSession().getAttribute("loginUser");*/
				Jyh_ComMember loginUser = (Jyh_ComMember) request.getSession().getAttribute("loginUser");
				System.out.println(loginUser.getComId());
				
				
				System.out.println(title);
				System.out.println(duty);
				System.out.println(career);
				System.out.println(scale);
				System.out.println(service);
				System.out.println(startDate);
				System.out.println(endDate);
				System.out.println(receiptStart);
				System.out.println(receiptEnd);
				System.out.println(location);
				System.out.println(eligibility);
				System.out.println(preferential);
				System.out.println(presentation);
				System.out.println(process);
				
				Announcement an = new Announcement();
				an.setaTitle(title);
				an.setaStartDate(day);
				an.setaEndDate(day1);
				an.setaReceiptStart(day2);
				an.setaReceiptEnd(day3);
				an.setDuty(duty);
				an.setScale(scale);
				an.setCareer(career);
				an.setService(service);
				an.setLocation(location);
				an.setEligibility(eligibility);
				an.setPreferential(preferential);
				an.setPresentation(presentation);
				an.setProcess(process);
				an.setIntroduce(introduce);
				an.setCom_id(loginUser.getComId());
				an.setComNo(loginUser.getComNo());
				an.setCom_nmae(loginUser.getComName());
				an.setCom_url(loginUser.getComUrl());
				an.setSkill(skill);
				
				Item it = new Item();
				it.setComNo(loginUser.getComNo());
				it.setiNo(item1);
				it.setiCount(item3);
				it.setpName(item22);
				
				
				
				Attachment at = new Attachment();
				at.setFilePath(savePath);
				at.setOriginName(originFile);
				at.setChangeName(saveFile);
				
				result = new AnnouncementService().insertAnn(an, at, it);
				
				
			}else {
				
				System.out.println("하단");
				
				java.sql.Date day = null;
				java.sql.Date day1 = null;
				java.sql.Date day2= null;
				java.sql.Date day3 = null;
				
				if(startDate !=  "") {
					day = java.sql.Date.valueOf(startDate);
					
				}else {
					day = new java.sql.Date(new GregorianCalendar().getTimeInMillis());
				}
				if(endDate !=  "") {
					day1 = java.sql.Date.valueOf(endDate);
					
				}else {
					day1 = new java.sql.Date(new GregorianCalendar().getTimeInMillis());
				}
				if(receiptStart !=  "") {
					day2 = java.sql.Date.valueOf(receiptStart);
					
				}else {
					day2 = new java.sql.Date(new GregorianCalendar().getTimeInMillis());
				}
				if(receiptEnd !=  "") {
					day3 = java.sql.Date.valueOf(receiptEnd);
					
				}else {
					day3 = new java.sql.Date(new GregorianCalendar().getTimeInMillis());
				}
				
				
				/*Wjs_ComMember loginUser=(Wjs_ComMember)request.getSession().getAttribute("loginUser");*/
				Jyh_ComMember loginUser = (Jyh_ComMember) request.getSession().getAttribute("loginUser");
				System.out.println(loginUser.getComId());
				
				
				System.out.println(title);
				System.out.println(duty);
				System.out.println(career);
				System.out.println(scale);
				System.out.println(service);
				System.out.println(startDate);
				System.out.println(endDate);
				System.out.println(receiptStart);
				System.out.println(receiptEnd);
				System.out.println(location);
				System.out.println(eligibility);
				System.out.println(preferential);
				System.out.println(presentation);
				System.out.println(process);
				
				Announcement an = new Announcement();
				an.setaTitle(title);
				an.setaStartDate(day);
				an.setaEndDate(day1);
				an.setaReceiptStart(day2);
				an.setaReceiptEnd(day3);
				an.setDuty(duty);
				an.setScale(scale);
				an.setCareer(career);
				an.setService(service);
				an.setLocation(location);
				an.setEligibility(eligibility);
				an.setPreferential(preferential);
				an.setPresentation(presentation);
				an.setProcess(process);
				an.setIntroduce(introduce);
				an.setCom_id(loginUser.getComId());
				an.setCom_nmae(loginUser.getComName());
				an.setCom_url(loginUser.getComUrl());
				an.setSkill(skill);
				
				
				
				Attachment at = new Attachment();
				at.setFilePath(savePath);
				at.setOriginName(originFile);
				at.setChangeName(saveFile);
				
				result = new AnnouncementService().insertAnn(an, at);
				
			}
			
			
			
			String page = "";
			
			if(result > 0) {
				
				page = "views/common/wjs_successPage.jsp";
				request.setAttribute("successCode", "HireEnroll");
				System.out.println("공고등록완료");
				
				/*page ="views/person/programmerRecruit/hireEnrollComplete.jsp";
				request.setAttribute("at", at);
				request.setAttribute("an", an);*/
				
				
			/*	page="views/index.jsp";
				request.setAttribute("at", at);
				request.setAttribute("an", an);*/
				
				
			}else {
				
				File failedFile = new File(savePath, saveFile);
				failedFile.delete();
				
			}
			
			request.getRequestDispatcher(page).forward(request, response);
			
			
		}
		
		
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
