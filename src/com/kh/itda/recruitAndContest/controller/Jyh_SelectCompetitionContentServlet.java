package com.kh.itda.recruitAndContest.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kh.itda.member.model.vo.Jyh_PerMember;
import com.kh.itda.recruitAndContest.model.service.Jyh_RecruitService;

@WebServlet("/jyhCompetitionContent.re")
public class Jyh_SelectCompetitionContentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public Jyh_SelectCompetitionContentServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		Jyh_PerMember loginUser= (Jyh_PerMember) request.getSession().getAttribute("loginUser");
		
		int result = 0;
		String annNo = request.getParameter("aNo");
		
		if(loginUser != null) {
		String userNo = loginUser.getPerNo();
		
		
		result = new Jyh_RecruitService().applicationYN(userNo, annNo);
		}
		int applyYn = 0;
		//HashMap<String, Object> CPTTContent = new Jyh_RecruitService().selectCompetitionContent(annNo);
		if(loginUser != null) {
		if(result > 0) {
			applyYn = 1;
		}
		}
		ArrayList<Object> list = new Jyh_RecruitService().selectCompetitionContent(annNo);
		
		HashMap<String, Object> CPTTContent = (HashMap<String, Object>) list.get(0);
		ArrayList<String> skillArr = (ArrayList<String>) list.get(1);
		String position = (String) list.get(2);
		String imgUrl = (String) list.get(3);

		System.out.println("CPTTContent : " + CPTTContent);
		System.out.println("skillArr : " + skillArr);
		System.out.println("position : " + position);
		if(loginUser != null) {
		System.out.println("applyYn : " + applyYn);
		}
		System.out.println("imgUrl : " + imgUrl);

		String page = "";
		if (CPTTContent != null) {
			page = "views/company/com_competition/jyh_competitionContents.jsp";
			request.setAttribute("content", CPTTContent);
			request.setAttribute("skillArr", skillArr);
			request.setAttribute("position", position);
			request.setAttribute("applyYn", applyYn);
			request.setAttribute("imgUrl", imgUrl);
		} else {
			page = "views/common/errorPage.jsp";
			request.setAttribute("msg", "공모전 내용 불러오기 실패!");
		}
		request.getRequestDispatcher(page).forward(request, response);
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
