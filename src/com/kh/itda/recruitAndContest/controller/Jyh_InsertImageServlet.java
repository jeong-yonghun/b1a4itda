package com.kh.itda.recruitAndContest.controller;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;

import com.google.gson.Gson;
import com.kh.itda.common.MyFileRenamePolicy;
import com.kh.itda.recruitAndContest.model.service.Jyh_RecruitService;
import com.kh.itda.recruitAndContest.model.vo.Jyh_Attachment;
import com.oreilly.servlet.MultipartRequest;

@WebServlet("/jyhInsertImg.re")
public class Jyh_InsertImageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public Jyh_InsertImageServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("여기인가!!!!!!!!!?");
		
		//전달한 request가 multipart/form-data 방식으로 넘어온 데이터인지 확인하여 true, false 리턴
				if(ServletFileUpload.isMultipartContent(request)) {
					System.out.println("여기였다!!!!!!!!!!!!!!!!!!!!!");
					//전송 파일 용량 제한 : 10MB로 계산(제한하기)
					int maxSize = 1024 * 1024 * 10; //이렇게하면 10MB가 된다.
					
					//웹 서버 컨테이너(톰캣 내부의 web 폴더) 경로 추출
					String root = request.getSession().getServletContext().getRealPath("/"); // ->request.getSession().getServletContext()은 톰캣을 가리킨다.
					
					System.out.println("root : " + root); // root : D:\Dev\servlet\workspace\jspProject\web\라는 결과가 나왔다(학원기준)
					
					//파일 저장 경로(고객의 파일을 받아서 우리 컴퓨터(서버)에 저장하기 위해서 하는듯
					String savePath = root + "competition_uploadFiles/";
					
					//객체 생성 시 파일을 저장하고 그에 대한 정보를 가져오는 형태이다.
					//즉, 파일의 정보를 검사하여 저장하는 형태가 아닌, 저장한 다음 검사 후 삭제를 해야 한다.
					//이미지만 올라오도록 했는데 텍스트 파일이 올라오면 지워주는 것은 우리가 작성해줘야한다.
					//사용자가 올린 파일명을 그대로 저장하지 않는 것이 일반적이다.
					//1. 같은 파일명이 있는 경우 이전 파일을 덮어쓸 수 있다.
					//2. 한글 혹은 특수기호나 띄어쓰기 등이 파일 이름에 있는 경우 서버에 따라 문제가 발생할 수 있다.
					
					//DefaultFileRenamePolicy는 cos.jar 안에 존재하는 클래스로 FileRenamePolicy interface를 상속받아
					//구현한 클래스이다.
					//같은 파일 명이 존재하는지를 검사하고 있는 경우 파일명 뒤에 숫자를 붙여준다.
					//ex : aaa.zip, aaa1.zip, aaa2.zip
//					MultipartRequest multiRequest = 
//							new MultipartRequest(request, savePath, maxSize, "UTF-8", new DefaultFileRenamePolicy());
					
					MultipartRequest multiRequest = 
							new MultipartRequest(request, savePath, maxSize, "UTF-8", new MyFileRenamePolicy());
					
					//저장한 파일의 이름을 저장할 ArrayList 생성
					ArrayList<String> saveFiles = new ArrayList<String>();
					//원본 파일의 이름을 저장할 ArrayList 생성
					ArrayList<String> originFiles = new ArrayList<String>();
					
					//파일이 전송된 폼의 name값을 반환
					Enumeration<String> files = multiRequest.getFileNames();
					
					while(files.hasMoreElements()) {
						String name = files.nextElement(); //각 파일을 찾을 수 있도록 key 역할을 한다.thumbnailImg4,3,2,1 값이 나온다(이 프로젝트 기준) 역순으로 들어온다.
						
						System.out.println("name : " + name);
						
						saveFiles.add(multiRequest.getFilesystemName(name)); //역순으로 저장된다. 먼저 넣은 것이 나중에 나옴.
						originFiles.add(multiRequest.getOriginalFileName(name));
					}
					
					System.out.println("saveFiles : " + saveFiles);
					System.out.println("originFiles : " + originFiles);
					
					//multipartRequest객체에서 파일 외의 값을 가져올 수 도 있다.
					String multiTitle = multiRequest.getParameter("title");
					String multiContent = multiRequest.getParameter("content");
					
					System.out.println("multiTitle : " + multiTitle);
					System.out.println("multiContent : " + multiContent);
					
					
					//여러 개의 Attachment정보를 ArrayList에 담기
					ArrayList<Jyh_Attachment> fileList = new ArrayList<Jyh_Attachment>();
					//전송 순서 역순으로 파일을 list에 저장했기 때문에 반복문을 역으로 수행함.
					for(int i = originFiles.size() - 1; i >= 0; i--) {
						Jyh_Attachment at = new Jyh_Attachment();
						at.setFilePath(savePath);
						at.setOriginName(originFiles.get(i));
						at.setChangeName(saveFiles.get(i));
						
						fileList.add(at);
					}
					
					System.out.println("filesList" + fileList);
					System.out.println("123214343412312 ===== " + fileList.get(0).getChangeName());
					
					int result = new Jyh_RecruitService().insertImage(fileList);
					
					String url = "/itda/competition_uploadFiles/" + fileList.get(0).getChangeName();
					
					if(result > 0) {
						response.setContentType("application/json");
						response.setCharacterEncoding("UTF-8");
						
						new Gson().toJson(url, response.getWriter());
					} else {
						//실패시 저장된 사진 삭제
						for(int i = 0; i < saveFiles.size(); i++) {
							File failedFile = new File(savePath + saveFiles.get(i));
							failedFile.delete();
						}
						
						request.setAttribute("msg", "사진게시판 등록 실패!");
						request.getRequestDispatcher("views/common/errorPage.jsp").forward(request, response);
					}
				}
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
