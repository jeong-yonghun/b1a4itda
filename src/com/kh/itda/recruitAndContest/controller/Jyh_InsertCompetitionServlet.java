package com.kh.itda.recruitAndContest.controller;

import java.io.IOException;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kh.itda.member.model.vo.Jyh_ComMember;
import com.kh.itda.recruitAndContest.model.service.Jyh_RecruitService;

@WebServlet("/jyhInertCompetition.re")
public class Jyh_InsertCompetitionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public Jyh_InsertCompetitionServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		Jyh_ComMember loginUser= (Jyh_ComMember) request.getSession().getAttribute("loginUser");
		
		String title = request.getParameter("title");
		String contents = request.getParameter("summernote");
		String[] targetArr = request.getParameterValues("target-li");
	//	String[] positionArr = request.getParameterValues("position-li");
		String position = request.getParameter("position");
		String[] skillArr = request.getParameterValues("skill-li");
		String[] guideArr = request.getParameterValues("guide-li");
		String requirements = request.getParameter("summernote1");
		String[] linkArr = request.getParameterValues("link-li");
		String[] fileArr = request.getParameterValues("file-li");
		String[] privilegeArr = request.getParameterValues("privilege-li");
		String aStartDate = request.getParameter("AnnouncementDate1").replace("-", "");
		String aEndDate = request.getParameter("AnnouncementDate2").replace("-", "");
		String aReceiptStart = request.getParameter("receiptDate1").replace("-", "");
		String aReceiptEnd = request.getParameter("receiptDate2").replace("-", "");
		String progressStart = request.getParameter("progressDate1").replace("-", "");
		String progressEnd = request.getParameter("progressDate2").replace("-", "");
		String submissionDate = request.getParameter("submissionDate").replace("-", "");
		String imgSrc = request.getParameter("imgSrc");
		String userNo = loginUser.getComNo();
		
		String target = null;
//		String position = null;
		String skill = null;
		String guide = null;
		String link = null;
		String file = null;
		String privilege = null;
		
		if(targetArr != null) {
			target = String.join(",", targetArr);
		}
		/*if(positionArr != null) {
			position = String.join(",", positionArr);
		}*/
		if(skillArr != null) {
			skill = String.join(",", skillArr);
		}
		if(guideArr != null) {
			guide = String.join(",", guideArr);
		}
		if(linkArr != null) {
			link = String.join(",", linkArr);
		}
		if(fileArr != null) {
			file = String.join(",", fileArr);
		}
		if(privilegeArr != null) {
			privilege = String.join(",", privilegeArr);
		}
		
		System.out.println(aStartDate);
		System.out.println(aEndDate);
		System.out.println(aReceiptStart);
		System.out.println(aReceiptEnd);
		System.out.println(progressStart);
		System.out.println(progressEnd);
		System.out.println(submissionDate);
		
		HashMap<String, Object> competitionMap= new HashMap<String, Object>();
		
		competitionMap.put("title", title);
		competitionMap.put("contents", contents);
		competitionMap.put("targetArr", target);
		competitionMap.put("position", position);
		competitionMap.put("skillArr", skillArr);
		competitionMap.put("guideArr", guide);
		competitionMap.put("requirements", requirements);
		competitionMap.put("linkArr", link);
		competitionMap.put("fileArr", file);
		competitionMap.put("privilegeArr", privilege);
		competitionMap.put("aStartDate", aStartDate);
		competitionMap.put("aEndDate", aEndDate);
		competitionMap.put("aReceiptStart", aReceiptStart);
		competitionMap.put("aReceiptEnd", aReceiptEnd);
		competitionMap.put("progressStart", progressStart);
		competitionMap.put("progressEnd", progressEnd);
		competitionMap.put("submissionDate", submissionDate);
		competitionMap.put("imgSrc", imgSrc);
		competitionMap.put("userNo", userNo);
		
		int result = new Jyh_RecruitService().insertCompetition(competitionMap);
		
		String page = "";
		if (result > 0) {
			page = "views/company/companyMainPage/companyMainPage.jsp";
			response.sendRedirect(page);
		} else {
			page = "views/common/errorPage.jsp";
			request.setAttribute("msg", "공모전 등록하기 실패!");
			request.getRequestDispatcher(page).forward(request, response);
		}
		
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
