package com.kh.itda.recruitAndContest.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kh.itda.approval.model.vo.PageInfo;
import com.kh.itda.member.model.service.Jyh_MemberService;
import com.kh.itda.member.model.vo.Jyh_ComMember;
import com.kh.itda.query.AnnListPagingQuery;
import com.kh.itda.recruitAndContest.model.service.Jyh_RecruitService;

@WebServlet("/jyhSelectProgressHireList.re")
public class Jyh_SelectProgressHireListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public Jyh_SelectProgressHireListServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String dateCondition = request.getParameter("dateCondition");
		String selectCondition = request.getParameter("selectCondition");
		String annType = request.getParameter("annType");
		String annDivision = request.getParameter("annDivision");
		String chk = request.getParameter("chk");
		String aDivision = null;
		String status = null;
		StringBuilder changeQuery = null;
		StringBuilder changeListCount = null;
		
		if(chk == null) {
			chk = "1";
		}
		 
		
		System.out.println("ttttttttttttt ==" + dateCondition);
		
		if(annType.equals("1")) {
			aDivision = "'일반공고'";
		}else if(annType.equals("2")){
			aDivision = "'공모전'";
		}
		
		if(annDivision != null) {
			if(annDivision.equals("1")) {
				status = "'승인'";
			}else if(annDivision.equals("2")) {
				status = "'종료'";
			}else if(annDivision.equals("3")) {
				status = "'승인', '반려'";
			}
		}
		
		if(selectCondition == null) {
			selectCondition = " '%%'";
		}
		
		if(dateCondition == null) {
			dateCondition = ">= '0'";
		}
		
		if(chk.equals("1")) {
			if(selectCondition.charAt(2) == '%') {
				selectCondition = "COM_ID LIKE"+selectCondition+" OR COMPANY_NAME LIKE"+selectCondition
						+" OR A_TITLE LIKE"+selectCondition+" OR PHONE LIKE"+selectCondition
						+" OR COM_NAME LIKE"+selectCondition;
			}
			changeQuery = new AnnListPagingQuery().changeQueryAnn(dateCondition, selectCondition, status, aDivision);
			changeListCount = new AnnListPagingQuery().changeListCountAnn(dateCondition, selectCondition, status, aDivision);
			
			int result = new Jyh_RecruitService().changeQueryAnn(changeQuery, changeListCount);
		}/*else if(annType.equals("2") && chk.equals("1")) {
			if(selectCondition.charAt(2) == '%') {
				selectCondition = "COM_ID LIKE"+selectCondition+" OR COMPANY_NAME LIKE"+selectCondition
						+" OR A_TITLE LIKE"+selectCondition+" OR PHONE LIKE"+selectCondition
						+" OR COM_NAME LIKE"+selectCondition;
			}
			changeQuery = new AnnListPagingQuery().changeQueryCptt(dateCondition, selectCondition, status, aDivision);
			changeListCount = new AnnListPagingQuery().changeListCountCptt(dateCondition, selectCondition, status, aDivision);
			
			int result = new Jyh_RecruitService().changeQueryAnn(changeQuery, changeListCount);
		}*/
		
		
		System.out.println("여기야 여기 annType = " + annType);
		System.out.println("여기야 여기 changeListCount = " + changeListCount);
		System.out.println("여기야 여기 changeQuery = " + changeQuery);
		// ------------ 페이징 처리 후 --------------
		int currentPage; // 현재 페이지를 표시할 변수
		int limit; // 한 페이지에 게시글이 몇 개 보여질 것인지 표시
		int maxPage; // 전체 페이지에서 가장 마지막 페이지
		int startPage; // 한 번에 표시될 페이지가 시작할 페이지
		int endPage; // 한 번에 표시될 페이지가 끝나는 페이지

		currentPage = 1;

		// 전달받은 페이지가 있다면 전달값으로 currentPage 값 변경 (null이 아닐 때)
		if (request.getParameter("currentPage") != null) {
			currentPage = Integer.parseInt(request.getParameter("currentPage"));
		}

		// 한 페이지에 보여질 목록 갯수
		limit = 10;

		Jyh_RecruitService rs = new Jyh_RecruitService();
		int listCount = rs.getChangeListCount();

		System.out.println("getChangeListCount : " + listCount);

		maxPage = (int) ((double) listCount / limit + 0.9);

		startPage = (((int) ((double) currentPage / limit + 0.9)) - 1) * 10 + 1;

		endPage = startPage + 10 - 1;

		if (maxPage < endPage) {
			endPage = maxPage;
		}

		PageInfo pi = new PageInfo(currentPage, listCount, limit, maxPage, startPage, endPage);
		
		String pagingUrl = null;

		ArrayList<HashMap<String, Object>> list = new Jyh_RecruitService().selectAnnouncementList(pi);
		
		if (annType.equals("1")) {
			String page = "";
			
			if(annDivision.equals("1")) {
				pagingUrl = "/jyhSelectProgressHireList.re?annType=1&chk=2&annDivision="+annDivision+"&";
			}else if(annDivision.equals("2")) {
				pagingUrl = "/jyhSelectProgressHireList.re?annType=1&chk=2&annDivision="+annDivision+"&";
			}else if(annDivision.equals("3")) {
				pagingUrl = "/jyhSelectProgressHireList.re?annType=1&chk=2&annDivision="+annDivision+"&";
			}
			if (list != null) {
				System.out.println("일반공고 전체 리스트 및 검색기능 성공!!!!");
				if(annDivision.equals("1")) {
					page = "views/admin/jyh_admin_hireNoticeManage_proceeding.jsp";
				}else if(annDivision.equals("2")) {
					page = "views/admin/jyh_admin_hireNoticeManage_closed.jsp";
				}else if(annDivision.equals("3")) {
					page = "views/admin/jyh_admin_hireNoticeManage_ok_retuened.jsp";
				}
				request.setAttribute("list", list);
				request.setAttribute("pi", pi);
				request.setAttribute("pagingUrl", pagingUrl);
			} else {
				System.out.println("일반공고 전체 리스트 및 검색기능 실패!!!!!!!!!");
				page = "views/common/errorPage.jsp";
				request.setAttribute("msg", "일반공고 전체 리스트 및 검색기능 실패!");
			}
			request.getRequestDispatcher(page).forward(request, response);
		}else if(annType.equals("2")) {
			String page = "";
			if(annDivision.equals("1")) {
				pagingUrl = "/jyhSelectProgressHireList.re?annType=2&chk=2&annDivision="+annDivision+"&";
			}else if(annDivision.equals("2")) {
				pagingUrl = "/jyhSelectProgressHireList.re?annType=2&chk=2&annDivision="+annDivision+"&";
			}else if(annDivision.equals("3")) {
				pagingUrl = "/jyhSelectProgressHireList.re?annType=2&chk=2&annDivision="+annDivision+"&";
			}
			if (list != null) {
				System.out.println("기업멤버리스트 성공!!!!");
				if(annDivision.equals("1")) {
					page = "views/admin/jyh_admin_competitionNoticeManage_proceeding.jsp";
				}else if(annDivision.equals("2")) {
					page = "views/admin/jyh_admin_competitionNoticeManage_closed.jsp";
				}else if(annDivision.equals("3")) {
					page = "views/admin/jyh_admin_competitionNoticeManage_ok_retuened.jsp";
				}
				request.setAttribute("list", list);
				request.setAttribute("pi", pi);
				request.setAttribute("pagingUrl", pagingUrl);
			} else {
				System.out.println("기업멤버리스트 실패!!!!!!!!!");
				page = "views/common/errorPage.jsp";
				request.setAttribute("msg", "게시판 목록 조회 실패!");
			}
			request.getRequestDispatcher(page).forward(request, response);
		}
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
