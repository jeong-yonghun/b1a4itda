package com.kh.itda.recruitAndContest.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kh.itda.programmerRecruit.model.service.AnnonuceService;
import com.kh.itda.recruitAndContest.model.service.AnnouncementService;
import com.kh.itda.recruitAndContest.model.vo.Announcement;


@WebServlet("/selectUpdate.re")
public class UpdateHireServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
  
	
    public UpdateHireServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String ano = request.getParameter("ano");
		int val = Integer.parseInt(request.getParameter("val"));
		
		System.out.println(ano);
		System.out.println(val);
		
		Announcement an = new AnnouncementService().selectUpdateRe(ano,val);
		
		String page = "";
		
		if(an != null) {
			page = "views/company/programmerRecruit/updateHire.jsp";
			request.setAttribute("an", an);
			
			System.out.println(an);
		}else {
			
		}
		
		request.getRequestDispatcher(page).forward(request, response);
		
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
