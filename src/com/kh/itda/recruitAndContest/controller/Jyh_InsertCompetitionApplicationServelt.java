package com.kh.itda.recruitAndContest.controller;

import java.io.IOException;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.kh.itda.member.model.vo.Jyh_PerMember;
import com.kh.itda.recruitAndContest.model.service.Jyh_RecruitService;

@WebServlet("/jyhCompetitionApplication.re")
public class Jyh_InsertCompetitionApplicationServelt extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
    public Jyh_InsertCompetitionApplicationServelt() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		Jyh_PerMember loginUser= (Jyh_PerMember) request.getSession().getAttribute("loginUser");
		
		String aNo = request.getParameter("aNo");
		String userNo = request.getParameter("userNo");
		String career = request.getParameter("ex-career");
		String division = request.getParameter("division");
		
		HashMap<String, String> applicationHmap = new HashMap<String, String>();
		
		applicationHmap.put("userNo", userNo);
		applicationHmap.put("aNo", aNo);
		applicationHmap.put("career", career);
		applicationHmap.put("division", division);
		
		System.out.println(applicationHmap.get("userNo"));
		System.out.println(applicationHmap.get("aNo"));
		System.out.println(applicationHmap.get("career"));
		System.out.println(applicationHmap.get("division"));
		
		int result = new Jyh_RecruitService().insertApplication(applicationHmap);
		
		String page = "";
		if(result > 0) {
			System.out.println("공모전 신청 성공!!!!!!!!!!!!!!!!!!!!!!!!!!");
			page = "index.jsp";
			HttpSession session = request.getSession();
			session.setAttribute("loginUser", loginUser);
			response.sendRedirect(page);
		}else {
			System.out.println("공모전 신청  실패!!!!!!!!!!!!!!!!!!!!!!!!!!");
			page = "views/common/errorPage.jsp";
			request.setAttribute("msg", "공모전 신청 실패");
			request.getRequestDispatcher(page).forward(request, response);
		}
		
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
