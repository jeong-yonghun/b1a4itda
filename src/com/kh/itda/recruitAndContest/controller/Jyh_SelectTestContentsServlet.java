package com.kh.itda.recruitAndContest.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kh.itda.recruitAndContest.model.service.Jyh_RecruitService;

@WebServlet("/jyhSelectTestContents.re")
public class Jyh_SelectTestContentsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public Jyh_SelectTestContentsServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String aNo = request.getParameter("aNo");
		String userNo = request.getParameter("userNo");
		String career = request.getParameter("ex-career");
		String division = request.getParameter("division");
		
		String submitResultYN = new Jyh_RecruitService().submitResultYN(userNo, aNo);
		
		HashMap<String, Object> testContent = new Jyh_RecruitService().selectTestContent(userNo, aNo);

		System.out.println("testContent : " + testContent);
		System.out.println("submitResultYN : " + submitResultYN);

		String page = "";
		if (testContent != null) {
			System.out.println("테스트 요구사항 불러오기 성공!");
			page = "views/company/com_competition/jyh_competition_test.jsp";
			request.setAttribute("testContent", testContent);
			request.setAttribute("submitResultYN", submitResultYN);
		} else {
			System.out.println("테스트 요구사항 불러오기 실패!");
			page = "views/common/errorPage.jsp";
			request.setAttribute("msg", "테스트 요구사항 내용 불러오기 실패!");
		}
		request.getRequestDispatcher(page).forward(request, response);
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
