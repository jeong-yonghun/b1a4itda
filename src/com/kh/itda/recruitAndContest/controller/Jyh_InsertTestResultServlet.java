package com.kh.itda.recruitAndContest.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.kh.itda.member.model.vo.Jyh_PerMember;
import com.kh.itda.recruitAndContest.model.service.Jyh_RecruitService;

@WebServlet("/insertTestResult.re")
public class Jyh_InsertTestResultServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public Jyh_InsertTestResultServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		Jyh_PerMember loginUser= (Jyh_PerMember) request.getSession().getAttribute("loginUser");
		
		String userNo = request.getParameter("userNo");
		String aNo = request.getParameter("aNo");
		String github = request.getParameter("github");
		
		int result = new Jyh_RecruitService().insertTestResult(userNo, aNo, github);
		
		String page = "";
		if (result > 0) {
			page = "index.jsp";
			HttpSession session = request.getSession();
			session.setAttribute("loginUser", loginUser);
			response.sendRedirect(page);
		} else {
			page = "views/common/errorPage.jsp";
			request.setAttribute("msg", "테스트 결과 제출 실패!");
			request.getRequestDispatcher(page).forward(request, response);
		}
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
