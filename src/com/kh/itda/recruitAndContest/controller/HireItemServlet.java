package com.kh.itda.recruitAndContest.controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.kh.itda.recruitAndContest.model.service.AnnouncementService;
import com.kh.itda.recruitAndContest.model.vo.Item;

/**
 * Servlet implementation class HireItemServlet
 */
@WebServlet("/item.re")
public class HireItemServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public HireItemServlet() {
        super();
        // TODO Auto-generated constructor stub
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String no = request.getParameter("no");
		
		System.out.println(no);
		
		ArrayList<Item> list = new AnnouncementService().itemListAn(no);
		System.out.println(list);
		
		response.setContentType("application/json");
		//response.setContentType("UTF-8");
		response.setCharacterEncoding("UTF-8");
		
		new Gson().toJson(list, response.getWriter());
		
		
		
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
