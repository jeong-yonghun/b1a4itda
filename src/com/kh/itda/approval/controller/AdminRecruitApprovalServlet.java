package com.kh.itda.approval.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kh.itda.approval.model.service.ApprovalService;



/**
 * Servlet implementation class AdminRecruitApprovalServlet
 */
@WebServlet("/approvalRe.ad")
public class AdminRecruitApprovalServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdminRecruitApprovalServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String ano = request.getParameter("num");
		/*int num = Integer.parseInt(request.getParameter("num"));
		System.out.println(num);*/
		
		System.out.println(ano);
		
		
		int result = new ApprovalService().updateApprovalRe(ano);
		
		System.out.println(result);
		
		String page ="";
		
		if(result > 0) {
			
			page = "views/common/wjs_successPage.jsp";
			request.setAttribute("successCode", "updateRecruit");
			System.out.println("공고수정");
			
		}else {
			
		}
		
		request.getRequestDispatcher(page).forward(request, response);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
