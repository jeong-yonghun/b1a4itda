package com.kh.itda.approval.controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kh.itda.approval.model.service.ApprovalService;
import com.kh.itda.member.model.vo.Jyh_ComMember;


@WebServlet("/approval.com")
public class ApprovalCompanyServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ApprovalCompanyServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

    
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		int currentPage;		//현재 페이지를 표시할 변수
		int limit;				//한 페이지에 게시글이 몇 개 보여질 것인지 표시
		int maxPage;			//전체 페이지에서 가장 마지막 페이지
		int startPage;			//한 번에 표시될 페이지가 시작할 페이지
		int endPage;			//한 번에 표시될 페이지가 끝나는 페이지


		currentPage = 1;

		if(request.getParameter("currentPage") != null) {
			currentPage = Integer.parseInt(request.getParameter("currentPage"));
		}

		limit = 10;

		int listCount = new ApprovalService().getListCount();

		System.out.println(listCount);

		//총 페이지 수 계산
		//목록 수가 123개이면 페이지는 13페이지가 되어야 한다.
		//짜투리 목록이 최소 1개일 때, 1페이지가 추가되도록 계산
		maxPage = (int) ((double) listCount / limit + 0.9);

		//현제 페이지에 보여줄 시작 페이지 수 (10개씩 보여지게 할 경우)
		//아래 쪽 페이지 수가 10개씩 보여지게 한다면
		//1, 11, 21, 31, ...
		startPage = (((int) ((double) currentPage /limit + 0.9)) -1) * 10 + 1;

		//알 쪽에 보여질 마지막 페이지 수 (10,20,30, ...)
		endPage = startPage + 10 -1;

		//빈페이지가 발생할수 있음  맥스페이지가 13인데 endpage는 20까지 발생할수있음
		if(maxPage < endPage) {
			endPage = maxPage;
		}

		//페이지에 정보를 담은 생성자
		com.kh.itda.approval.model.vo.PageInfo pi = new com.kh.itda.approval.model.vo.PageInfo(currentPage, listCount, limit, maxPage, startPage, endPage);
		
		ArrayList<Jyh_ComMember> list = new ApprovalService().approvalMemberSelect(pi);
		System.out.println(list);
		
		String page = "";
		
		if(list != null) {
			
			page = "views/admin/admin_approval/company_approval.jsp";
			request.setAttribute("pi", pi);
			request.setAttribute("list", list);
			
		}else {
			
		}
		
		request.getRequestDispatcher(page).forward(request, response);
		
		
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
