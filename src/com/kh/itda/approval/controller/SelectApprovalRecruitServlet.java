package com.kh.itda.approval.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kh.itda.recruitAndContest.model.dao.AnnouncementDao;
import com.kh.itda.recruitAndContest.model.service.AnnouncementService;
import com.kh.itda.recruitAndContest.model.vo.Announcement;
import com.kh.itda.recruitAndContest.model.vo.Attachment;


@WebServlet("/SelectAp.re")
public class SelectApprovalRecruitServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    
    public SelectApprovalRecruitServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

    
    
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String AnnNum  = request.getParameter("num");
		
		
		Announcement announcement = new AnnouncementService().approvalSelect(AnnNum);
		
		Attachment at = new AnnouncementService().approvalSelectAttach(AnnNum);
		
		System.out.println(announcement);
		
		String page = "";
		
		if(announcement != null) {
			
			page = "views/admin/admin_approval/hire_approval_detail.jsp";
			request.setAttribute("an", announcement);
			request.setAttribute("at", at);
			
		}else {
			
		}
		
		request.getRequestDispatcher(page).forward(request, response);
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
