package com.kh.itda.approval.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kh.itda.approval.model.service.ApprovalService;


@WebServlet("/refuseRe.ad")
public class AdminRecruitRefuseServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
  
    public AdminRecruitRefuseServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		String ano = request.getParameter("ano");
		String text = request.getParameter("textarea");
		int num = Integer.parseInt(request.getParameter("num")); 
		
		System.out.println(num);
		
		System.out.println(text);

		int result = new ApprovalService().updateRefuseRe(ano,num,text);
		
		String page = "";
		
		if(result > 0) {
			System.out.println("수정");
			page = "views/common/wjs_successPage.jsp";
			request.setAttribute("successCode", "updateRecruit2");
			
		}else {
			
		}
		
		request.getRequestDispatcher(page).forward(request, response);
		
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
