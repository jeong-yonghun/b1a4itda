package com.kh.itda.approval.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kh.itda.approval.model.service.ApprovalService;
import com.kh.itda.member.model.vo.Jyh_ComMember;
import com.kh.itda.recruitAndContest.model.vo.Attachment;


@WebServlet("/selectAp.com")
public class SelectApprovalCompanyServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
  
    public SelectApprovalCompanyServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

    
    
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			
		String num = request.getParameter("num");
		
		System.out.println(num);
		
		Jyh_ComMember cm = new ApprovalService().approvalSelectCm(num);
		
		
		
		String page = "";
		
		if(cm != null) {
			System.out.println("조회완료");
			page = "views/admin/admin_approval/company_approval_detail.jsp";
			request.setAttribute("cm", cm);
			
		}else {
			
		}
		
		request.getRequestDispatcher(page).forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
