package com.kh.itda.approval.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kh.itda.approval.model.service.ApprovalService;


@WebServlet("/refuseCm.ad")
public class AdminCompanyRefuseServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
 
	
    public AdminCompanyRefuseServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

    
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String num = request.getParameter("num");
		
		System.out.println(num);
		
		String text = request.getParameter("textarea");
		
		System.out.println(text);
		
		int result = new ApprovalService().updateRefuseCm(num,text);
		
		String page = "";
		
		if(result > 0) {
			System.out.println("수정");
			page = "views/common/wjs_successPage.jsp";
			request.setAttribute("successCode", "updateCom2");
			
			
		}else {
			
		}
		
		request.getRequestDispatcher(page).forward(request, response);
	}
		

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
