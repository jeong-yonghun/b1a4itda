package com.kh.itda.approval.model.dao;

import static com.kh.itda.common.JDBCTemplate.close;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Properties;

import com.kh.itda.approval.model.vo.PageInfo;
import com.kh.itda.member.model.vo.Jyh_ComMember;
import com.kh.itda.recruitAndContest.model.dao.AnnouncementDao;
import com.kh.itda.recruitAndContest.model.vo.Announcement;

import oracle.net.aso.p;

public class ApprovalDao {
	
	private Properties prop = new Properties();
	
	public ApprovalDao() {
		
		String fileName = AnnouncementDao.class.getResource("/sql/approval/wjs-approval-query.properties").getPath();
		
		try {
			prop.load(new FileReader(fileName));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	

	public int getListCount(Connection con) {
		
		Statement stmt = null;
		ResultSet rset = null;
		
		int listCount = 0;
		
		String query = prop.getProperty("listCount");
		
		try {
			stmt = con.createStatement();
			rset = stmt.executeQuery(query);
			
			if(rset.next()) {
				listCount = rset.getInt(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			close(rset);
			close(stmt);
		}
		
		return listCount;
		
		
	}


	public ArrayList<Jyh_ComMember> approvalRecruitSelect(Connection con, PageInfo pi) {
		
		
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<Jyh_ComMember> list = null;
		
		String query = prop.getProperty("wjs_selectWithPaging");
		
		try {
			pstmt = con.prepareStatement(query);
			
			int startRow = (pi.getCurrentPage() - 1) * pi.getLimit() + 1;
			int endRow = startRow + pi.getLimit() -1;
			
			System.out.println(startRow);
			System.out.println(endRow);
			
			pstmt.setInt(1, startRow);
			pstmt.setInt(2, endRow);
			System.out.println("여기??");
			rset = pstmt.executeQuery();
			
			list = new ArrayList<>();
			
			while(rset.next()) {
				System.out.println("왜안들어왓 시바라");
				Jyh_ComMember cm = new Jyh_ComMember();
				
				cm.setComNo(rset.getString("COM_NO"));
				cm.setComId(rset.getString("COM_ID"));
				cm.setCompanyName(rset.getString("COMPANY_NAME"));
				cm.setComNum(rset.getString("COM_NUM"));
				cm.setPhone(rset.getString("PHONE"));
				cm.setEnrollDate(rset.getDate("ENROLL_DATE"));
				cm.setComAddress(rset.getString("STATUS"));
				
				System.out.println("cm" + cm);
				list.add(cm);
				System.out.println(list);
			}	
			
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			close(rset);
			close(pstmt);
		}
		
		return list;
		
	}



	public int updateApprovalRe(Connection con, String ano, String rscode) {
		
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("updateApprovalRe");
		
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, ano);
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			close(pstmt);
		}
		
		return result;
	}



	public String selectRsCode(Connection con, String ano) {
		
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		String rsCode = "";
		
		String query = prop.getProperty("selectRsCode");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, ano);
			System.out.println("ano :" +ano);
			
			rset = pstmt.executeQuery();
			
			while(rset.next()) {
				
				rsCode = rset.getString("ST_CODE");
				
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			close(rset);
			close(pstmt);
		}
		
		return rsCode;
	}



	public int updateApprovalRe2(Connection con, String ano, int rscode) {
		
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("updateApprovalRe2");
		
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, ano);
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			close(pstmt);
		}
		
		return result;
		
	}


	public Jyh_ComMember approvalSelectCm(Connection con, String num) {
		
		PreparedStatement pstmt = null;
		Jyh_ComMember cm = null;
		ResultSet rset = null;
		
		String query = prop.getProperty("approvalSelectCm");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, num);
			
			rset = pstmt.executeQuery();
			
			cm = new Jyh_ComMember();
			while(rset.next()) {
				
				cm.setComId(rset.getString("COM_ID"));
				cm.setCompanyName(rset.getString("COMPANY_NAME"));
				cm.setEmail(rset.getString("EMAIL"));
				cm.setPhone(rset.getString("PHONE"));
				cm.setEnrollDate(rset.getDate("ENROLL_DATE"));
				cm.setInfoPeriod(rset.getString("INFO_PERIOD"));
				cm.setComNum(rset.getString("COM_NUM"));
				cm.setNumEmp(rset.getString("NUM_EMP"));
				cm.setInvest(rset.getString("INVEST"));
				cm.setSales(rset.getString("SALES"));
				cm.setComNo(rset.getString("COM_NO"));
				
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			close(rset);
			close(pstmt);
		}
		
		return cm;
	}


	public int updateApprovalCm(Connection con, String num) {
		
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("updateApprovalCm");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, num);
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			close(pstmt);
		}
		
		return result;
	}


	public int updateRefuseCm(Connection con, String num) {
		
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("updateRefuseCm");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, num);
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			close(pstmt);
		}
		
		return result;
		
	}


	public int insertNotice(Connection con, String text, int num, String ano, String comNo) {
		
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("insertNotice2");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, text);
			pstmt.setString(2, comNo);
			pstmt.setString(3, ano);
			pstmt.setInt(4, num);
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			close(pstmt);
		}
		
		return result;
	}


	public String selectComNo(Connection con, String num1) {
		
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		String comNo = "";
		
		String query = prop.getProperty("selectComNo");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, num1);
			rset = pstmt.executeQuery();
			
			while(rset.next()) {
				comNo = rset.getString("COM_NO");
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			close(rset);
			close(pstmt);
		}
		
		
		return comNo;
	}


	public int updateRefuseRe(Connection con, String num) {
		
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("updateRefuseAn");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, num);
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			close(pstmt);
		}
		
		return result;
		
	}



	public int insertNotice(Connection con, String text, String num) {
		
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("insertNotice3");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, text);
			pstmt.setString(2, num);
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			close(pstmt);
		}
		
		return result;
		
		
	}



	public int insertNoticeAp(Connection con, String ano, String comNo) {
		
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("insertNoticeRe");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, comNo);
			pstmt.setString(2, ano);
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			close(pstmt);
		}
		
		return result;
		
	}
	
}
