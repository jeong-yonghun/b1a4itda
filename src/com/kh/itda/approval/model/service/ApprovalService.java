package com.kh.itda.approval.model.service;

import static com.kh.itda.common.JDBCTemplate.close;
import static com.kh.itda.common.JDBCTemplate.commit;
import static com.kh.itda.common.JDBCTemplate.getConnection;
import static com.kh.itda.common.JDBCTemplate.rollback;

import java.sql.Connection;
import java.util.ArrayList;

import com.kh.itda.approval.model.dao.ApprovalDao;
import com.kh.itda.approval.model.vo.PageInfo;
import com.kh.itda.member.model.vo.Jyh_ComMember;
import com.kh.itda.recruitAndContest.model.dao.AnnouncementDao;
import com.kh.itda.recruitAndContest.model.vo.Announcement;

public class ApprovalService {

	

	public int getListCount() {
		Connection con = getConnection();
		
		int listCount = new ApprovalDao().getListCount(con);
		
		close(con);
		
		return listCount;
	}
	

	public ArrayList<Jyh_ComMember> approvalMemberSelect(PageInfo pi) {
		
		Connection con = getConnection();
		
		ArrayList<Jyh_ComMember> list = new ApprovalDao().approvalRecruitSelect(con, pi);
		
		close(con);
		
		System.out.println(list);
		
		return list;
	}
	
	public int updateApprovalRe(String ano) {
		
		Connection con = getConnection();
		
		int result = 0;
		String rscode = new ApprovalDao().selectRsCode(con, ano);
		System.out.println(rscode);
		
		String comNo = new ApprovalDao().selectComNo(con,ano);
		
		int result1 = new ApprovalDao().updateApprovalRe(con,ano,rscode);
		
		int result2 = new ApprovalDao().insertNoticeAp(con , ano,comNo);
		
		
		if(result1 > 0 && result2 > 0) {
			commit(con);
			result = 1;
		}else {
			rollback(con);
		}
		
		close(con);
		
		return result;
	}

	public Jyh_ComMember approvalSelectCm(String num) {
		
		Connection con = getConnection();
		
		Jyh_ComMember cm = new ApprovalDao().approvalSelectCm(con, num);
		
		
		close(con);
		
		return cm;
	}

	public int updateApprovalCm(String num) {
		
		Connection con = getConnection();
		
		int result = new ApprovalDao().updateApprovalCm(con, num);
		
		if(result > 0) {
			commit(con);
		}else {
			rollback(con);
		}
		
		return result;
	}

	public int updateRefuseCm(String num, String text) {
		
		Connection con = getConnection();
		
		int result = 0;
		int result1 = new ApprovalDao().updateRefuseCm(con,num);
		
		int result2 = new ApprovalDao().insertNotice(con, text ,num);
		
		
		if(result1 > 0 && result2 > 0) {
			commit(con);
			result = 1;
		}else {
			rollback(con);
		}
		
		return result;
	}

	public int updateRefuseRe(String ano, int num, String text) {
		
		Connection con = getConnection();
		
		int result = 0;
		
		String comNo = new ApprovalDao().selectComNo(con,ano);
		
		int result1 = new ApprovalDao().updateRefuseRe(con,ano);
		
		int result2 = new ApprovalDao().insertNotice(con, text ,num , ano,comNo);
		
		
		if(result1 > 0 && result2 > 0) {
			commit(con);
			result = 1;
		}else {
			rollback(con);
		}
		
		return result;
		
	}

	
	

}
