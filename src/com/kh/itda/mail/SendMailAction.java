package com.kh.itda.mail;

import java.util.Properties;
import java.util.Random;

import javax.mail.Address;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class SendMailAction {

	public SendMailAction() {
	}

	public String SendMail(String email) {
		Properties p = new Properties(); // 정보를 담을 객체
		p.put("mail.smtp.host", "gmail-smtp.l.google.com"); // 네이버 SMTP

		p.put("mail.smtp.port", "465");
		p.put("mail.smtp.starttls.enable", "true");
		p.put("mail.smtp.auth", "true");
		p.put("mail.smtp.debug", "true");
		p.put("mail.smtp.socketFactory.port", "465");
		p.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		p.put("mail.smtp.socketFactory.fallback", "false");
		// SMTP 서버에 접속하기 위한 정보들

		// 사용자가 관리자에게 문의를 하는 구조 - 결국 나에게 내가 이메일을 보낸다
		int random = new Random().nextInt(8998)+1001;
		String random1 = Integer.toString(random);
		
		try {
			
			Authenticator auth = new Gmail();
			Session ses = Session.getInstance(p, auth);

			ses.setDebug(true);

			MimeMessage msg = new MimeMessage(ses); // 메일의 내용을 담을 객체
			msg.setSubject("IT'DA 회원가입 이메일 인증번호 안내"); // 제목

			Address fromAddr = new InternetAddress("b1a4itda@gmail.com");
			msg.setFrom(fromAddr); // 보내는 사람

			Address toAddr = new InternetAddress(email);
			msg.addRecipient(Message.RecipientType.TO, toAddr); // 받는 사람

			msg.setContent("IT'DA회원가입 인증 번호는 [ " + random1 + " ] 입니다."
										, "text/html;charset=UTF-8"); // 내용과 인코딩
/*			msg.setContent("<div style='width:100px; height:100px; background:black; color:white'>" + random1 + "</div>"
					, "text/html;charset=UTF-8"); // 내용과 인코딩
*/			Transport.send(msg); // 전송
		} catch (Exception e) {
			e.printStackTrace();
			// 오류 발생시 뒤로 돌아가도록
			System.out.println("error");
		}

		return random1;
	}
}