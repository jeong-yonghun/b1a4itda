package com.kh.itda.programmerRecruit.model.service;

import static com.kh.itda.common.JDBCTemplate.close;
import static com.kh.itda.common.JDBCTemplate.commit;
import static com.kh.itda.common.JDBCTemplate.getConnection;
import static com.kh.itda.common.JDBCTemplate.rollback;

import java.sql.Connection;
import java.util.ArrayList;

import com.kh.itda.programmerRecruit.model.dao.AnnounceDao;
import com.kh.itda.recruitAndContest.model.vo.Announcement;

public class AnnonuceService {

	public ArrayList<Announcement> selectAnnounList() {
		Connection con=getConnection();
		
		ArrayList<Announcement> list=new AnnounceDao().selectAnnounList(con);
		if(list!=null) {
			commit(con);
		}else {
			rollback(con);
		}
		close(con);
		return list;
	}

	public ArrayList<Announcement> searchSelectList(String location, String skill, String career, String duty) {
		Connection con=getConnection();
		
		ArrayList<Announcement> list=new AnnounceDao().searchSelectList(con,location,skill,career,duty);
		System.out.println("Dao 갔다온후 list:"+list);
		if(list!=null) {
			commit(con);
		}else {
			rollback(con);
		}
		close(con);
		return list;
	}

	public ArrayList<Announcement> PerSelectAnnounList(String perNo) {
		Connection con=getConnection();
		
		ArrayList<Announcement> list=new AnnounceDao().PerSelectList(con,perNo);
		System.out.println("Dao 갔다온후 list:"+list);
		if(list!=null) {
			commit(con);
		}else {
			rollback(con);
		}
		close(con);
		return list;
	}

	public ArrayList<Announcement> selectContestAnnounList() {
Connection con=getConnection();
		
		ArrayList<Announcement> list=new AnnounceDao().selectContestAnnounList(con);
		if(list!=null) {
			commit(con);
		}else {
			rollback(con);
		}
		close(con);
		return list;
	}

	
}
