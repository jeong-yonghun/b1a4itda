package com.kh.itda.programmerRecruit.model.dao;

import static com.kh.itda.common.JDBCTemplate.close;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Properties;

import com.kh.itda.addInfo.resume.model.dao.ResumeDao;
import com.kh.itda.addInfo.resume.model.vo.Resume;
import com.kh.itda.recruitAndContest.model.vo.Announcement;

public class AnnounceDao {
	private Properties prop=new Properties();
	
	public AnnounceDao() {
		String fileName=ResumeDao.class.getResource("/sql/recruit/cho-annouceList-query.properties").getPath();
		try {
			prop.load(new FileReader(fileName));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public ArrayList<Announcement> selectAnnounList(Connection con) {
		PreparedStatement pstmt=null;
		ResultSet rset=null;
		ArrayList<Announcement> list=new ArrayList<>();
		Announcement ann=null;
		String query=prop.getProperty("selectAnnouceList");
		
		try {
			pstmt=con.prepareStatement(query);
			pstmt.setString(1, "일반공고");
			rset=pstmt.executeQuery();
			while(rset.next()) {
				ann=new Announcement();
				ann.setaNo(rset.getString("A_NO"));
				ann.setaTitle(rset.getString("A_TITLE"));
				ann.setaStartDate(rset.getDate("A_START_DATE"));
				ann.setaEndDate(rset.getDate("A_END_DATE"));
				ann.setaReceiptStart(rset.getDate("A_RECEIPT_START"));
				ann.setaReceiptEnd(rset.getDate("A_RECEIPT_END"));
				ann.setaDivision(rset.getString("A_DIVISION"));
				ann.setComNo(rset.getString("COM_NO"));
				ann.setDuty(rset.getString("AA_DUTY"));
				ann.setCareer(rset.getString("AA_CAREER"));
				ann.setLocation(rset.getString("AA_LOCATION"));
				ann.setCom_nmae(rset.getString("COMPANY_NAME"));
				ann.setItem(rset.getString("ITEM"));
				ann.setSkill(rset.getString("A_SKILL"));
				ann.setChangeName(rset.getString("CHANGE_NAME"));
				System.out.println("공고목록:"+ann);
				list.add(ann);
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
			close(rset);
		}
		return list;
	}
	public ArrayList<Announcement> searchSelectList(Connection con, String location, String skill, String career,
			String duty) {
		PreparedStatement pstmt=null;
		ResultSet rset=null;
		ArrayList<Announcement> list=new ArrayList<>();
		Announcement ann=null;
		String query=prop.getProperty("searchSelectAnnouceList");
		System.out.println("검색값:"+location+" "+skill+" "+career+" "+duty);
		
		try {
			pstmt=con.prepareStatement(query);
			pstmt.setString(1, "일반공고");
			pstmt.setString(2, skill);
			pstmt.setString(3, location);
			pstmt.setString(4, duty);
			pstmt.setString(5, career);
			rset=pstmt.executeQuery();
			while(rset.next()) {
				ann=new Announcement();
				ann.setaNo(rset.getString("A_NO"));
				ann.setaTitle(rset.getString("A_TITLE"));
				ann.setaStartDate(rset.getDate("A_START_DATE"));
				ann.setaEndDate(rset.getDate("A_END_DATE"));
				ann.setaReceiptStart(rset.getDate("A_RECEIPT_START"));
				ann.setaReceiptEnd(rset.getDate("A_RECEIPT_END"));
				ann.setaDivision(rset.getString("A_DIVISION"));
				ann.setComNo(rset.getString("COM_NO"));
				ann.setDuty(rset.getString("AA_DUTY"));
				ann.setCareer(rset.getString("AA_CAREER"));
				ann.setLocation(rset.getString("AA_LOCATION"));
				ann.setCom_nmae(rset.getString("COMPANY_NAME"));
				ann.setItem(rset.getString("ITEM"));
				ann.setSkill(rset.getString("A_SKILL"));
				ann.setChangeName(rset.getString("CHANGE_NAME"));
				System.out.println("공고목록:"+ann);
				list.add(ann);
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
			close(rset);
		}
		return list;
	}
	public ArrayList<Announcement> PerSelectList(Connection con, String perNo) {
		PreparedStatement pstmt=null;
		ResultSet rset=null;
		ArrayList<Announcement> list=new ArrayList<>();
		Announcement ann=null;
		String query=prop.getProperty("PerSelectAnnouceList");
		
		try {
			pstmt=con.prepareStatement(query);
			pstmt.setString(1, "일반공고");
			pstmt.setString(2, perNo);
			rset=pstmt.executeQuery();
			while(rset.next()) {
				ann=new Announcement();
				ann.setaNo(rset.getString("A_NO"));
				ann.setaTitle(rset.getString("A_TITLE"));
				ann.setaStartDate(rset.getDate("A_START_DATE"));
				ann.setaEndDate(rset.getDate("A_END_DATE"));
				ann.setaReceiptStart(rset.getDate("A_RECEIPT_START"));
				ann.setaReceiptEnd(rset.getDate("A_RECEIPT_END"));
				ann.setaDivision(rset.getString("A_DIVISION"));
				ann.setComNo(rset.getString("COM_NO"));
				ann.setDuty(rset.getString("AA_DUTY"));
				ann.setCareer(rset.getString("AA_CAREER"));
				ann.setLocation(rset.getString("AA_LOCATION"));
				ann.setCom_nmae(rset.getString("COMPANY_NAME"));
				ann.setItem(rset.getString("ITEM"));
				ann.setSkill(rset.getString("A_SKILL"));
				ann.setChangeName(rset.getString("CHANGE_NAME"));
				System.out.println("공고목록:"+ann);
				list.add(ann);
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
			close(rset);
		}
		return list;
	}
	public ArrayList<Announcement> selectContestAnnounList(Connection con) {
		PreparedStatement pstmt=null;
		ResultSet rset=null;
		ArrayList<Announcement> list=new ArrayList<>();
		Announcement ann=null;
		String query=prop.getProperty("selectContestAnnouceList");
		
		try {
			pstmt=con.prepareStatement(query);
			pstmt.setString(1, "공모전공고");
			rset=pstmt.executeQuery();
			while(rset.next()) {
				ann=new Announcement();
				ann.setaNo(rset.getString("A_NO"));
				ann.setaTitle(rset.getString("A_TITLE"));
				ann.setaStartDate(rset.getDate("A_START_DATE"));
				ann.setaEndDate(rset.getDate("A_END_DATE"));
				ann.setaReceiptStart(rset.getDate("PROGRESS_START"));
				ann.setaReceiptEnd(rset.getDate("PROGRESS_END"));
				ann.setaDivision(rset.getString("A_DIVISION"));
				ann.setComNo(rset.getString("COM_NO"));
				/*ann.setDuty(rset.getString("AA_DUTY"));
				ann.setCareer(rset.getString("AA_CAREER"));
				ann.setLocation(rset.getString("AA_LOCATION"));
				ann.setCom_nmae(rset.getString("COMPANY_NAME"));
				ann.setItem(rset.getString("ITEM"));
				ann.setSkill(rset.getString("A_SKILL"));*/
				ann.setChangeName(rset.getString("CHANGE_NAME"));
				System.out.println("공고목록:"+ann);
				list.add(ann);
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
			close(rset);
		}
		return list;
	}
	

}
