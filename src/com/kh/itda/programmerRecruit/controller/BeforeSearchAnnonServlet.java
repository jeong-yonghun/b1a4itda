package com.kh.itda.programmerRecruit.controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kh.itda.addInfo.resume.model.service.ResumeService;
import com.kh.itda.addInfo.resume.model.vo.Resume;
import com.kh.itda.programmerRecruit.model.service.AnnonuceService;
import com.kh.itda.recruitAndContest.model.vo.Announcement;

/**
 * Servlet implementation class BeforeSearchAnnonServlet
 */
@WebServlet("/beforeSearch.anon")
public class BeforeSearchAnnonServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public BeforeSearchAnnonServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		ArrayList<Announcement> list=new AnnonuceService().selectAnnounList();
		System.out.println("공고목록 서블릿:"+list);
		String page="";
		if(list!=null) {
			page="views/person/programmerRecruit/cho_searchAnon.jsp";
			request.setAttribute("list", list);
		}else {
			page="views/common/errorPage.jsp";
			request.setAttribute("msg", "채용공고목록 조회 실패");
		}
		request.getRequestDispatcher(page).forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
