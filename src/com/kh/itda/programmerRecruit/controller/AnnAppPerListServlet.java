package com.kh.itda.programmerRecruit.controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kh.itda.addInfo.resume.model.service.ResumeService;
import com.kh.itda.addInfo.resume.model.vo.Resume;
import com.kh.itda.member.model.vo.Jyh_ComMember;
import com.kh.itda.member.model.vo.Jyh_PerMember;

/**
 * Servlet implementation class AnnAppPerListServlet
 */
@WebServlet("/appPerList.ap")
public class AnnAppPerListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AnnAppPerListServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		Jyh_ComMember loginUser = (Jyh_ComMember) request.getSession().getAttribute("loginUser");
		String aNo=request.getParameter("aNo");
		System.out.println("PER:"+aNo);
		ArrayList<Resume> list=new ResumeService().applyResumeSelect(aNo);
		
		System.out.println("공고 지원 이력서 list:"+list);
		
		String page="";
		if(list!=null) {
			page="views/company/companyMyPage/cho_perAperResume.jsp";
			request.setAttribute("list",list);
		}else {
			page="views/common/errorPage.jsp";
			request.setAttribute("msg","이력서목록조회 실패");
		}
		request.getRequestDispatcher(page).forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
