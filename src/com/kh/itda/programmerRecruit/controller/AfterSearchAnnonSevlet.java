package com.kh.itda.programmerRecruit.controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kh.itda.addInfo.resume.model.service.ResumeService;
import com.kh.itda.addInfo.resume.model.vo.Resume;
import com.kh.itda.programmerRecruit.model.service.AnnonuceService;
import com.kh.itda.recruitAndContest.model.vo.Announcement;

/**
 * Servlet implementation class AfterSearchAnnonSevlet
 */
@WebServlet("/afterSearchAnnon.sea")
public class AfterSearchAnnonSevlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AfterSearchAnnonSevlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String skillYN=request.getParameter("skillYN");
		String locationYN=request.getParameter("locationYN");
		String skill="N";
		String location="N";
		if(locationYN.equals("Y")) {
			location=request.getParameter("searchLocation");	
		}
		if(skillYN.equals("Y")) {
			skill=request.getParameter("searchSkill");
		}
		
		String career=request.getParameter("career");
		String duty=request.getParameter("duty");
		
		ArrayList<Announcement> list=new AnnonuceService().searchSelectList(location,skill,career,duty);
		//ArrayList<Resume> list=new ResumeService().selectList("B1");
		System.out.println("서블릿 검색후 공고 list:"+list);
		
		
		String page="";
		if(list!=null) {
			page="views/person/programmerRecruit/cho_searchAnon.jsp";
			request.setAttribute("list",list);
		}else {
			page="views/common/errorPage.jsp";
			request.setAttribute("msg","검색후목록조회 실패");
		}
		request.getRequestDispatcher(page).forward(request, response);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
