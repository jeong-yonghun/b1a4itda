package com.kh.itda.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;

// 11번

@WebFilter("/*") // 모든 요청에 대해서 필터를 거친다.(동작하면 여기부터 작동되고 넘어감) *표시를 했기 때문에.
public class CommonFilter implements Filter {

    public CommonFilter() {
    }

	public void destroy() {
	} 

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		
		//12번
		HttpServletRequest hRequest = (HttpServletRequest) request;
		
		if(hRequest.getMethod().equalsIgnoreCase("post")) {
			hRequest.setCharacterEncoding("UTF-8");
			System.out.println("인코딩 완료!");
			
		}
		
		chain.doFilter(hRequest, response); // 다음 필터로 request와 response를 넘겨주거나 다음 필터가 없으면 서블릿으로 요청을 넘겨주는 doFilter이다.
	}

	public void init(FilterConfig fConfig) throws ServletException {
	}

}
