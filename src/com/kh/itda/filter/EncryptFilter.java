package com.kh.itda.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;

import com.kh.itda.wrapper.LoginWrapper;


//회원가입 4번 (암호화 필터)
@WebFilter("*.me")
public class EncryptFilter implements Filter {

    public EncryptFilter() {
    }

	public void destroy() {
	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		
		HttpServletRequest hRequest = (HttpServletRequest) request; //서블릿에서 사용하는 httprequest로 다운캐스팅해서 사용해야한다.
		
		LoginWrapper lw = new LoginWrapper(hRequest); //request를 가공처리해서 lw에 담는다
		
		chain.doFilter(lw, response); //가공한 request인 lw를 request대신 사용한다.
		//회원가입 4번 끝 -> LoginWrapper로 넘어간다
	}

	public void init(FilterConfig fConfig) throws ServletException {
	}

}
