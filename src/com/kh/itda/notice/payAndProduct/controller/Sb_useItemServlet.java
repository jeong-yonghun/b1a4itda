package com.kh.itda.notice.payAndProduct.controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kh.itda.notice.payAndProduct.model.service.Sb_paymentService;
import com.kh.itda.notice.payAndProduct.model.vo.Sb_Payment;

/**
 * Servlet implementation class Sb_useItemServlet
 */
@WebServlet("/useItem.pay")
public class Sb_useItemServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Sb_useItemServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("useItemServlet 도착");
		String comNo = request.getParameter("comNo");
		System.out.println("comNo : " + comNo);
		ArrayList<Sb_Payment> list = new Sb_paymentService().useItemList(comNo);
		//ArrayList<Sb_Payment> list = new Sb_paymentService().getItemListCount(comNo);
		System.out.println("useItemList : " + list);
		
		String page= "";
		
		
		if(list != null) {
			page = "views/company/companyMyPage/sb_companyFirstPage.jsp";
			request.setAttribute("list", list);
		}else {
			page = "views/company/companyMainPage/companyMainPage.jsp";
			request.setAttribute("msg", "실패실패");
			request.getRequestDispatcher("views/company/companyMainPage/companyMainPage.jsp");
		}
		request.getRequestDispatcher(page).forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
