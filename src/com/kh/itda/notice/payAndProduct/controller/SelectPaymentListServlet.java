package com.kh.itda.notice.payAndProduct.controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kh.itda.notice.payAndProduct.model.service.Sb_paymentService;
import com.kh.itda.notice.payAndProduct.model.vo.Sb_Payment;

/**
 * Servlet implementation class SelectPaymentListServlet
 */
@WebServlet("/selectPaymentList.pay")
public class SelectPaymentListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SelectPaymentListServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ArrayList<Sb_Payment> list = new Sb_paymentService().selectList();
		System.out.println("list : " + list);
		
		String page = "";
		if(list != null) {
			System.out.println("if구문통과");
			page = "views/company/productIntroduce/sb_itemPayList2.jsp";
			request.setAttribute("list", list);
		} else {
			page = "views/company/companyMainPage/companyMainPage.jsp";
			request.setAttribute("msg", "결제리스트실패");
			request.getRequestDispatcher("views/company/companyMainPage/companyMainPage.jsp");
		}
		request.getRequestDispatcher(page).forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
