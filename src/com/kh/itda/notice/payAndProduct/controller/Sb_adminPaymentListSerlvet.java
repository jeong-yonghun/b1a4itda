package com.kh.itda.notice.payAndProduct.controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kh.itda.notice.payAndProduct.model.service.Sb_paymentService;
import com.kh.itda.notice.payAndProduct.model.vo.Sb_PageInfo;
import com.kh.itda.notice.payAndProduct.model.vo.Sb_Payment;


/**
 * Servlet implementation class Sb_adminPaymentListSerlvet
 */
@WebServlet("/adminPayList.pay")
public class Sb_adminPaymentListSerlvet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Sb_adminPaymentListSerlvet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("이동함");
		/*ArrayList<Sb_Payment> list = new Sb_paymentService().selectList();
		System.out.println("list : " + list);*/
		
		int currentPage;		//현재 페이지를 표시할 변수
		int limit;				//한 페이지에 표시될 게시물 수
		int maxPage;			//전체 페이지에서 가장 마지막 페이지
		int startPage;			//한 번에 표시될 페이지가 시작할 페이지
		int endPage;			//한 번에 표시될 페이지가 끝나는 페이지
		
		currentPage = 1;
		
		if(request.getParameter("currentPage") != null) {
			currentPage = Integer.parseInt(request.getParameter("currentPage"));
		}
		
		limit = 10;
		
		//maxPage를 계산하기 위해 필요하며, DB에서 게시물의 수를 조회해서 담아줘야함.
		Sb_paymentService ps = new Sb_paymentService();
		int listCount = ps.getListCount(); 
		System.out.println("listCount : " + listCount);
		
		maxPage = (int) ((double) listCount / limit + 0.9);
		
		startPage = (((int) ((double) currentPage / limit + 0.9)) -1) * 10 + 1;
		
		endPage = startPage + 10 -1;
		
		if(maxPage < endPage) {
			endPage = maxPage;
		}
		
		Sb_PageInfo pi = new Sb_PageInfo(currentPage, listCount, limit, maxPage, startPage, endPage);
		
		System.out.println("pi" + pi);
		
		ArrayList<Sb_Payment> list = new Sb_paymentService().selectList(pi);
		
		String page = "";
		if(list != null) {
			page = "views/admin/sb_adminMemberPaymentManage.jsp";
			request.setAttribute("list", list);
			request.setAttribute("pi", pi);
		}else {
			page = "views/common/errorPage.jsp";
			request.setAttribute("msg", "결제리스트조회실패!!");
		}
		request.getRequestDispatcher(page).forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
