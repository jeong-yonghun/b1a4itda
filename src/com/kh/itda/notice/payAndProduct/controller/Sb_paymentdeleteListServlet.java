package com.kh.itda.notice.payAndProduct.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kh.itda.notice.payAndProduct.model.service.Sb_paymentService;

/**
 * Servlet implementation class Sb_paymentdeleteListServlet
 */
@WebServlet("/delete.pay")
public class Sb_paymentdeleteListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Sb_paymentdeleteListServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("delete 서블릿 들어옴");
		String payno = request.getParameter("payno");
		String test = request.getParameter("test");
		System.out.println(payno);
		
		int result = new Sb_paymentService().updatePayDivision(payno);
		System.out.println(result);
	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
