package com.kh.itda.notice.payAndProduct.controller;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.GregorianCalendar;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;

import com.kh.itda.notice.payAndProduct.model.service.Sb_paymentService;
import com.kh.itda.notice.payAndProduct.model.vo.Sb_Payment;
import com.sun.org.apache.bcel.internal.generic.GETSTATIC;

import sun.util.calendar.Gregorian;

/**
 * Servlet implementation class Sb_paymentServlet
 */
@WebServlet("/test.it")
public class Sb_paymentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Sb_paymentServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String imp_uid = request.getParameter("imp_uid");
		String merchant_uid = request.getParameter("merchant_uid");
		String item3 = request.getParameter("item3");
		String select = request.getParameter("select");
		String amount = request.getParameter("amount");
		String date = request.getParameter("date");
		String id = request.getParameter("id");
		
		System.out.println(imp_uid);
		System.out.println(merchant_uid);
		System.out.println("item3 :" + item3);
		System.out.println(select);
		System.out.println(amount);
		System.out.println("date : " + date);
		System.out.println(id);
		
		java.sql.Date day = null;
		
		if(date != "") {
			day= java.sql.Date.valueOf(date);
		} else {
			day = new java.sql.Date(new GregorianCalendar().getTimeInMillis());
		}
		Sb_Payment sbPayment = new Sb_Payment();
		
		sbPayment.setImp_uid(imp_uid);
		sbPayment.setMerchant_uid(merchant_uid);
		sbPayment.setProductCode(item3);
		sbPayment.setPd_date(day);
		sbPayment.setCom_No(id);
		
		System.out.println(sbPayment);
		
		
		
		int result = new Sb_paymentService().insertpayment(sbPayment);
		System.out.println("Payresult : " + result);
		if(result > 0) {
			//사용자가 톰캣외부로 나갔다가 다시 재요청을 해야하는 상황이기 때문에 절대경로인 /itda/가 필요하다.
			//response.sendRedirect("/itda/selectOnePaymentList.pay");
			response.sendRedirect("/itda/useItem.pay");
			//response.sendRedirect("/itda/views/company/productIntroduce/sb_itemInformation.jsp");
		} else {
			request.setAttribute("msg", "결제리스트실패");
			request.getRequestDispatcher("views/company/companyMainPage/companyMainPage.jsp");
		}
		
//		String imp_key 		=	URLEncoder.encode('rest  API KEY', "UTF-8");
		//String imp_key = URLEncoder.encode("rest API KEY", "UTF-8");
		//String imp_secret	=	URLEncoder.encode("rest Secret KEY", "UTF-8");
		
		
		//토큰 생성 시작
		System.out.println("servlet");
		JSONObject json = new JSONObject();
		//Token token = new Token();
		json.put("imp_key","0155565158366532");
		json.put("imp_secret", "rau4NeSTqtq05WbFKyiq4FIg4syWscWTmrXaGQ46jv6psi4cQmDV2lc6OK3gW09js7u8Xyeh4lGwELJv");
		String token = "";
		try {
			token = new Token().getToken(request, response, json, "https://api.iamport.kr/users/getToken");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// 토큰 생성 끝
		
		
		
		System.out.println(token);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
