package com.kh.itda.notice.payAndProduct.model.service;

import java.sql.Connection;
import java.util.ArrayList;

import static com.kh.itda.common.JDBCTemplate.close;
import static com.kh.itda.common.JDBCTemplate.getConnection;
import static com.kh.itda.common.JDBCTemplate.commit;
import static com.kh.itda.common.JDBCTemplate.rollback;

import com.kh.itda.notice.payAndProduct.model.dao.Sb_paymentDao;
import com.kh.itda.notice.payAndProduct.model.vo.Sb_PageInfo;
import com.kh.itda.notice.payAndProduct.model.vo.Sb_Payment;

public class Sb_paymentService {

	public int insertpayment(Sb_Payment sbPayment) {
		Connection con = getConnection();
		System.out.println("Service : " + sbPayment);
		int result = new Sb_paymentDao().insertpayment(con, sbPayment);
		System.out.println("Serviceresult : " + result);
		if(result > 0) {
			commit(con);
		} else {
			rollback(con);
		}
		
		close(con);
				
		return result;
	}

	public ArrayList<Sb_Payment> selectList(Sb_PageInfo pi) {
		Connection con = getConnection();
		
		ArrayList<Sb_Payment> list = new Sb_paymentDao().selectList(con, pi);
		
		close(con);
		
		return list;
	}

	public ArrayList<Sb_Payment> selectOneList(String comNo, Sb_PageInfo pi) {
		Connection con = getConnection();
		
		ArrayList<Sb_Payment> list = new Sb_paymentDao().selectOneList(con, comNo, pi);
		
		close(con);
		System.out.println("service부분통과");
		return list;
		
	}

	public int getListCount(String comNo) {
		Connection con = getConnection();
		
		int listCount = new Sb_paymentDao().getListCount(con,comNo);
		
		close(con);
		
		
		return listCount;
	}

	public ArrayList<Sb_Payment> useItemList(String comNo) {
		Connection con = getConnection();
		
		ArrayList<Sb_Payment> list = new Sb_paymentDao().useItemList(con,comNo);
		
		/*if(list.subList(1, 1) != null)*/
		/*System.out.println("sublist : " + list.subList(1, 2));
		System.out.println("subnotList: " + list); */
		
		close(con);
		return list;
	}

	public ArrayList<Sb_Payment> getItemListCount(String comNo) {
		Connection con = getConnection();
		
		ArrayList<Sb_Payment> itemListCount = new Sb_paymentDao().getItemListCount(con, comNo);
		
		close(con);
		
		return itemListCount;
	}

	public int updatePayDivision(String payno) {
		Connection con = getConnection();
		
		int result = new Sb_paymentDao().updatePayDivision(con,payno);
		
		/*if(result > 0) {
			commit(con);
		}else {
			close(con);
		}*/
		commit(con);
		
		return result;
	}

	public int getListCount() {
		Connection con = getConnection();
		
		int listCount = new Sb_paymentDao().getListCount(con);
		
		close(con);
		
		return listCount;
	}

	public int updatePayadminDivision(String payno) {
		Connection con = getConnection();
		
		int result = new Sb_paymentDao().updatePayadminDivision(con,payno);
		
		if(result > 0) {
			commit(con);
		}else {
			rollback(con);
		}
		
		return result;
	}
	

}
