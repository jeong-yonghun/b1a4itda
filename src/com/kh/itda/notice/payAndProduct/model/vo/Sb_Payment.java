package com.kh.itda.notice.payAndProduct.model.vo;

import java.sql.Date;

public class Sb_Payment implements java.io.Serializable {
	private String imp_uid;
	private String merchant_uid;
	private String productCode;
	private int pd_price;
	private String pd_name;
	private String pd_validity;
	private Date pd_date;
	private String com_No;
	private int ab1;
	private int ab2;
	private int ab3;
	private int ab4;
	private int ab5;
	private int ab6;
	private int ab7;
	private String py_num;
	private String py_division;
	
	public Sb_Payment () {}

	public Sb_Payment(String imp_uid, String merchant_uid, String productCode, int pd_price, String pd_name,
			String pd_validity, Date pd_date, String com_No, int ab1, int ab2, int ab3, int ab4, int ab5, int ab6,
			int ab7, String py_num, String py_division) {
		super();
		this.imp_uid = imp_uid;
		this.merchant_uid = merchant_uid;
		this.productCode = productCode;
		this.pd_price = pd_price;
		this.pd_name = pd_name;
		this.pd_validity = pd_validity;
		this.pd_date = pd_date;
		this.com_No = com_No;
		this.ab1 = ab1;
		this.ab2 = ab2;
		this.ab3 = ab3;
		this.ab4 = ab4;
		this.ab5 = ab5;
		this.ab6 = ab6;
		this.ab7 = ab7;
		this.py_num = py_num;
		this.py_division = py_division;
	}

	public String getImp_uid() {
		return imp_uid;
	}

	public void setImp_uid(String imp_uid) {
		this.imp_uid = imp_uid;
	}

	public String getMerchant_uid() {
		return merchant_uid;
	}

	public void setMerchant_uid(String merchant_uid) {
		this.merchant_uid = merchant_uid;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public int getPd_price() {
		return pd_price;
	}

	public void setPd_price(int pd_price) {
		this.pd_price = pd_price;
	}

	public String getPd_name() {
		return pd_name;
	}

	public void setPd_name(String pd_name) {
		this.pd_name = pd_name;
	}

	public String getPd_validity() {
		return pd_validity;
	}

	public void setPd_validity(String pd_validity) {
		this.pd_validity = pd_validity;
	}

	public Date getPd_date() {
		return pd_date;
	}

	public void setPd_date(Date pd_date) {
		this.pd_date = pd_date;
	}

	public String getCom_No() {
		return com_No;
	}

	public void setCom_No(String com_No) {
		this.com_No = com_No;
	}

	public int getAb1() {
		return ab1;
	}

	public void setAb1(int ab1) {
		this.ab1 = ab1;
	}

	public int getAb2() {
		return ab2;
	}

	public void setAb2(int ab2) {
		this.ab2 = ab2;
	}

	public int getAb3() {
		return ab3;
	}

	public void setAb3(int ab3) {
		this.ab3 = ab3;
	}

	public int getAb4() {
		return ab4;
	}

	public void setAb4(int ab4) {
		this.ab4 = ab4;
	}

	public int getAb5() {
		return ab5;
	}

	public void setAb5(int ab5) {
		this.ab5 = ab5;
	}

	public int getAb6() {
		return ab6;
	}

	public void setAb6(int ab6) {
		this.ab6 = ab6;
	}

	public int getAb7() {
		return ab7;
	}

	public void setAb7(int ab7) {
		this.ab7 = ab7;
	}

	public String getPy_num() {
		return py_num;
	}

	public void setPy_num(String py_num) {
		this.py_num = py_num;
	}

	public String getPy_division() {
		return py_division;
	}

	public void setPy_division(String py_division) {
		this.py_division = py_division;
	}

	@Override
	public String toString() {
		return "Sb_Payment [imp_uid=" + imp_uid + ", merchant_uid=" + merchant_uid + ", productCode=" + productCode
				+ ", pd_price=" + pd_price + ", pd_name=" + pd_name + ", pd_validity=" + pd_validity + ", pd_date="
				+ pd_date + ", com_No=" + com_No + ", ab1=" + ab1 + ", ab2=" + ab2 + ", ab3=" + ab3 + ", ab4=" + ab4
				+ ", ab5=" + ab5 + ", ab6=" + ab6 + ", ab7=" + ab7 + ", py_num=" + py_num + ", py_division="
				+ py_division + "]";
	}
	
}
