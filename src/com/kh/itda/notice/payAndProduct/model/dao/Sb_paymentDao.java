package com.kh.itda.notice.payAndProduct.model.dao;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Properties;
import static com.kh.itda.common.JDBCTemplate.close;

import com.kh.itda.notice.payAndProduct.model.vo.Sb_PageInfo;
import com.kh.itda.notice.payAndProduct.model.vo.Sb_Payment;

public class Sb_paymentDao {
	private Properties prop = new Properties();
	
	public Sb_paymentDao() {
		String fileName = Sb_paymentDao.class.getResource("/sql/notice/sb-payment-query.properties").getPath();
		try {
			prop.load(new FileReader(fileName));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public int insertpayment(Connection con, Sb_Payment sbPayment) {
		PreparedStatement pstmt = null;
		int result = 0;
		String query = prop.getProperty("insertPayment");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, sbPayment.getCom_No());
			pstmt.setString(2, sbPayment.getProductCode());
			pstmt.setDate(3, sbPayment.getPd_date());
			pstmt.setString(4, sbPayment.getImp_uid());

			
			result = pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		System.out.println("Dao :" + result);
		return result;
	}

	public ArrayList<Sb_Payment> selectList(Connection con, Sb_PageInfo pi) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<Sb_Payment> list = null;
		
		String query = prop.getProperty("selectPaging");
		
		try {
				pstmt = con.prepareStatement(query);
				
				int startRow = (pi.getCurrentPage() - 1) * pi.getLimit() + 1;
				int endRow = startRow + pi.getLimit() - 1;
				
				pstmt.setInt(1, startRow);
				pstmt.setInt(2, endRow);
				
				
				rset = pstmt.executeQuery();
				
				list = new ArrayList<>();
				
				while(rset.next()) {
					Sb_Payment pay = new Sb_Payment();
					
					pay.setCom_No(rset.getString("COM_NO"));
					pay.setImp_uid(rset.getString("PY_CODE"));
					pay.setPd_date(rset.getDate("PY_DATE"));
					pay.setPd_name(rset.getString("PD_NAME"));
					pay.setPd_price(rset.getInt("PD_PRICE"));
					pay.setPd_validity(rset.getString("PD_VALIDITY"));
					pay.setPy_num(rset.getString("PY_NO"));
					pay.setPy_division(rset.getString("PY_DIVISION"));
					
					
					list.add(pay);
				
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return list;
	}

	public ArrayList<Sb_Payment> selectOneList(Connection con, String comNo, Sb_PageInfo pi) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		String query = prop.getProperty("selectOnePaging");
		ArrayList<Sb_Payment> list = null;
		System.out.println("dao부분 도착");
		
		try {
			pstmt = con.prepareStatement(query);
			
			int startRow = (pi.getCurrentPage() -1) * pi.getLimit() + 1;
			int endRow = startRow + pi.getLimit() -1;
			
			
			pstmt.setString(1, comNo);
			pstmt.setInt(2, startRow);
			pstmt.setInt(3, endRow);
			
			rset = pstmt.executeQuery();
			
			list = new ArrayList<>();
			
			while(rset.next()) {
				Sb_Payment pay = new Sb_Payment();
				//pay.setCom_No(rset.getString("COM_NO"));
				pay.setImp_uid(rset.getString("PY_NO"));
				pay.setMerchant_uid(rset.getString("COM_NO"));
				pay.setProductCode(rset.getString("PY_CODE"));
				pay.setPd_name(rset.getString("PD_NAME"));
				pay.setPd_price(rset.getInt("PD_PRICE"));
				pay.setPd_validity(rset.getString("PD_VALIDITY"));
				pay.setPd_date(rset.getDate("PY_DATE"));
				pay.setPy_division(rset.getString("PY_DIVISION"));
				
				list.add(pay);
				
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return list;
	}

	public int getListCount(Connection con, String comNo) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
		int listCount = 0;
		
		String query = prop.getProperty("listCount");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, comNo);
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				listCount = rset.getInt(1);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		return listCount;
	}

	public ArrayList<Sb_Payment> useItemList(Connection con, String comNo) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<Sb_Payment> list = null;
		
		String query = prop.getProperty("useItemList");
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setString(1, comNo);
			rset = pstmt.executeQuery();
			
			list = new ArrayList<>();
			while(rset.next()) {
				Sb_Payment pay = new Sb_Payment();
				//pay.setCom_No(rset.getString("COM_NO"));
				pay.setImp_uid(rset.getString("PY_NO"));
				pay.setMerchant_uid(rset.getString("COM_NO"));
				pay.setProductCode(rset.getString("PY_CODE"));
				pay.setPd_date(rset.getDate("PY_DATE"));
				pay.setPd_name(rset.getString("PD_NAME"));
				pay.setPd_price(rset.getInt("PD_PRICE"));
				pay.setPd_validity(rset.getString("PD_VALIDITY"));
				
				list.add(pay);
				
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return list;
	}

	public ArrayList<Sb_Payment> getItemListCount(Connection con, String comNo) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<Sb_Payment> list = null;
		String query = prop.getProperty("itemlistCount");
		
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setString(1, comNo);
			pstmt.setString(2, comNo);
			pstmt.setString(3, comNo);
			pstmt.setString(4, comNo);
			pstmt.setString(5, comNo);
			pstmt.setString(6, comNo);
			pstmt.setString(7, comNo);
			rset = pstmt.executeQuery();
			
			list = new ArrayList<>();
			while(rset.next()) {
				Sb_Payment pay = new Sb_Payment();
				pay.setAb1(rset.getInt(1));
				/*pay.setAb2(rset.getInt(2));
				pay.setAb3(rset.getInt(3));
				pay.setAb4(rset.getInt(4));
				pay.setAb5(rset.getInt(5));
				pay.setAb6(rset.getInt(6));
				pay.setAb7(rset.getInt(7));*/
				
				list.add(pay);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		return list;
		
	}

	public int updatePayDivision(Connection con, String payno) {
		PreparedStatement pstmt = null;
		int result = 0;
		System.out.println("dao");
		String query = prop.getProperty("updatePayDivision");
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, payno);
			
			result = pstmt.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return result;
	}

	public int getListCount(Connection con) {
		Statement stmt = null;
		ResultSet rset = null;
		
		int listCount = 0;
		
		String query = prop.getProperty("listAllCount");
		
		try {
			stmt = con.createStatement();
			rset = stmt.executeQuery(query);
			
			if(rset.next()) {
				listCount = rset.getInt(1);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(stmt);
			close(rset);
		}
		
		return listCount;
	}

	public int updatePayadminDivision(Connection con, String payno) {
		PreparedStatement pstmt = null;
		int result = 0;
		System.out.println("dao");
		String query = prop.getProperty("updatePayadminDivision");
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, payno);
			
			result = pstmt.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return result;
	}

}
