package com.kh.itda.query;

public class AnnListPagingQuery {
	
	public StringBuilder changeQueryAnn(String dateCondition, String selectCondition, String status, String aDivision) {
		SelectQueryMaker subQuery1 = new SelectQueryMaker.Builder()
				.select().column("A.COM_NO")
					.comma().column("M.COM_ID")
					.comma().column("M.COM_NAME")
					.comma().column("M.COMPANY_NAME")
					.comma().column("A.A_TITLE")
					.comma().column("M.PHONE")
					.comma().column("A.A_START_DATE")
					.comma().column("A.A_END_DATE")
					.comma().column("STATUS")
					.comma().column("A.A_NO")
					.comma().column("AR.RE_DATE")
					.comma().column("A.A_DIVISION")
					.empty()
					//.enter()
				.from().tableName("ANNOUNCEMENT").as("A")//.enter()
				.join().tableName("COM_MEMBER").as("M").on().equalCondition("A.COM_NO", "M.COM_NO")//.enter()
				.join().tableName("ANN_RECORD").as("AR").on().equalCondition("A.A_NO", "AR.A_NO")//.enter()
				.join().tableName("STATUS").as("S").on().equalCondition("AR.ST_CODE", "S.RS_CODE")//.enter()
				.where().openParenthesis().column("A.A_NO").comma().toNumber().openParenthesis().substr().openParenthesis().column("AR.RE_NO").comma().num("2").closeParenthesis().closeParenthesis().closeParenthesis().empty().in().openParenthesis()
					.select().column("A_NO").comma().max1().openParenthesis().toNumber().openParenthesis().substr().openParenthesis().column("AR.RE_NO").comma().num("2").closeParenthesis().closeParenthesis().closeParenthesis().empty()
					.from().tableName("ANN_RECORD")
					.groupBy().column("A_NO").closeParenthesis().empty()
				.and().column("S.STATUS").empty().in().openParenthesis().column(status).closeParenthesis().empty()
				.and().column("A_DIVISION").empty().equal().custom(aDivision).empty()
				.orderBy().toNumber().openParenthesis().substr().openParenthesis().column("A.A_NO").comma().num("2").closeParenthesis().closeParenthesis().desc()
				.build();
		
		SelectQueryMaker subQuery2 = new SelectQueryMaker.Builder()
				.select().column("ROWNUM").as("RNUM")
					.comma().column("COM_NO")
					.comma().column("COM_ID")
					.comma().column("COM_NAME")
					.comma().column("COMPANY_NAME")
					.comma().column("A_TITLE")
					.comma().column("PHONE")
					.comma().column("A_START_DATE")
					.comma().column("A_END_DATE")
					.comma().column("STATUS")
					.comma().column("A_NO")
					.comma().column("RE_DATE")
					.comma().column("A_DIVISION")
					.empty()
					//.enter()
				.from().subQuery(subQuery1)
				.where().trunc("SYSDATE").empty().operator("-").column("RE_DATE").empty().custom(dateCondition)
				.and().custom(selectCondition)
				.build();
		
		SelectQueryMaker mainQuery = new SelectQueryMaker.Builder()
				.select().column("RNUM")
					.comma().column("COM_NO")
					.comma().column("COM_ID")
					.comma().column("COM_NAME")
					.comma().column("COMPANY_NAME")
					.comma().column("A_TITLE")
					.comma().column("PHONE")
					.comma().column("A_START_DATE")
					.comma().column("A_END_DATE")
					.comma().column("STATUS")
					.comma().column("A_NO")
					.comma().column("RE_DATE")
					.comma().column("A_DIVISION")
					.empty()
					//.enter()
				.from().subQuery(subQuery2)//.enter()
				.where().columnName("RNUM").betweenAnd("?", "?")
				.build();
		
		
		System.out.println(mainQuery.getQuery());
		
		return mainQuery.getQuery();
	}
	
	
	public StringBuilder changeListCountAnn(String dateCondition, String selectCondition, String status, String aDivision) {
		
		SelectQueryMaker ListCount1 = new SelectQueryMaker.Builder()
				.select().column("A.COM_NO")
					.comma().column("M.COM_ID")
					.comma().column("M.COM_NAME")
					.comma().column("M.COMPANY_NAME")
					.comma().column("A.A_TITLE")
					.comma().column("M.PHONE")
					.comma().column("A.A_START_DATE")
					.comma().column("A.A_END_DATE")
					.comma().column("STATUS")
					.comma().column("A.A_NO")
					.comma().column("AR.RE_DATE")
					.comma().column("A.A_DIVISION")
					.empty()
					//.enter()
				.from().tableName("ANNOUNCEMENT").as("A")//.enter()
				.join().tableName("COM_MEMBER").as("M").on().equalCondition("A.COM_NO", "M.COM_NO")//.enter()
				.join().tableName("ANN_RECORD").as("AR").on().equalCondition("A.A_NO", "AR.A_NO")//.enter()
				.join().tableName("STATUS").as("S").on().equalCondition("AR.ST_CODE", "S.RS_CODE")//.enter()
				.where().openParenthesis().column("A.A_NO").comma().toNumber().openParenthesis().substr().openParenthesis().column("AR.RE_NO").comma().num("2").closeParenthesis().closeParenthesis().closeParenthesis().empty().in().openParenthesis()
					.select().column("A_NO").comma().max1().openParenthesis().toNumber().openParenthesis().substr().openParenthesis().column("AR.RE_NO").comma().num("2").closeParenthesis().closeParenthesis().closeParenthesis().empty()
					.from().tableName("ANN_RECORD")
					.where().column("A_DIVISION").empty().equal().column(aDivision).empty()
					.groupBy().column("A_NO").closeParenthesis().empty()
				.and().column("S.STATUS").empty().in().openParenthesis().column(status).closeParenthesis().empty()
				.orderBy().toNumber().openParenthesis().substr().openParenthesis().column("A.A_NO").comma().num("2").closeParenthesis().closeParenthesis().desc()
				.build();
		
		SelectQueryMaker ListCount2 = new SelectQueryMaker.Builder()
				.select().column("ROWNUM").as("RNUM")
				.comma().column("COM_NO")
				.comma().column("COM_ID")
				.comma().column("COM_NAME")
				.comma().column("COMPANY_NAME")
				.comma().column("A_TITLE")
				.comma().column("PHONE")
				.comma().column("A_START_DATE")
				.comma().column("A_END_DATE")
				.comma().column("STATUS")
				.comma().column("A_NO")
				.comma().column("RE_DATE")
				.comma().column("A_DIVISION")
				.empty()
				//.enter()
			.from().subQuery(ListCount1)
			.where().trunc("SYSDATE").empty().operator("-").column("RE_DATE").empty().custom(dateCondition)
			.and().custom(selectCondition)
			.build();
		
		SelectQueryMaker ListCount = new SelectQueryMaker.Builder()
				.select().column("COUNT(*)").empty()//.enter()
				.from().subQuery(ListCount2)
				.build();
		
		System.out.println(ListCount.getQuery());
		
		return ListCount.getQuery();
		
	}
	
	public StringBuilder changeQueryCptt(String dateCondition, String selectCondition, String status, String aDivision) {
		SelectQueryMaker subQuery1 = new SelectQueryMaker.Builder()
				.select().column("A.COM_NO")
					.comma().column("M.COM_ID")
					.comma().column("M.COM_NAME")
					.comma().column("M.COMPANY_NAME")
					.comma().column("A.A_TITLE")
					.comma().column("M.PHONE")
					.comma().column("A.A_START_DATE")
					.comma().column("A.A_END_DATE")
					.comma().column("STATUS")
					.comma().column("A.A_NO")
					.comma().column("AR.RE_DATE")
					.comma().column("A.A_DIVISION")
					.empty()
					//.enter()
				.from().tableName("ANNOUNCEMENT").as("A")//.enter()
				.join().tableName("COM_MEMBER").as("M").on().equalCondition("A.COM_NO", "M.COM_NO")//.enter()
				.join().tableName("ANN_RECORD").as("AR").on().equalCondition("A.A_NO", "AR.A_NO")//.enter()
				.join().tableName("STATUS").as("S").on().equalCondition("AR.ST_CODE", "S.RS_CODE")//.enter()
				.where().openParenthesis().column("A.A_NO").comma().toNumber().openParenthesis().substr().openParenthesis().column("AR.RE_NO").comma().num("2").closeParenthesis().closeParenthesis().closeParenthesis().empty().in().openParenthesis()
					.select().column("A_NO").comma().max1().openParenthesis().toNumber().openParenthesis().substr().openParenthesis().column("AR.RE_NO").comma().num("2").closeParenthesis().closeParenthesis().closeParenthesis().empty()
					.from().tableName("ANN_RECORD")
					.groupBy().column("A_NO").closeParenthesis().empty()
				.and().column("S.STATUS").empty().in().openParenthesis().custom(status).closeParenthesis().empty()
				.and().column("A_DIVISION").empty().equal().column(aDivision).empty()
				.orderBy().toNumber().openParenthesis().substr().openParenthesis().column("A.A_NO").comma().num("2").closeParenthesis().closeParenthesis().desc()
				.build();
		
		SelectQueryMaker subQuery2 = new SelectQueryMaker.Builder()
				.select().column("ROWNUM").as("RNUM")
					.comma().column("COM_NO")
					.comma().column("COM_ID")
					.comma().column("COM_NAME")
					.comma().column("COMPANY_NAME")
					.comma().column("A_TITLE")
					.comma().column("PHONE")
					.comma().column("A_START_DATE")
					.comma().column("A_END_DATE")
					.comma().column("STATUS")
					.comma().column("A_NO")
					.comma().column("RE_DATE")
					.comma().column("A_DIVISION")
					.empty()
					//.enter()
				.from().subQuery(subQuery1)
				.where().trunc("SYSDATE").empty().operator("-").column("RE_DATE").empty().custom(dateCondition)
				.and().custom(selectCondition)
				.build();
		
		SelectQueryMaker mainQuery = new SelectQueryMaker.Builder()
				.select().column("RNUM")
					.comma().column("COM_NO")
					.comma().column("COM_ID")
					.comma().column("COM_NAME")
					.comma().column("COMPANY_NAME")
					.comma().column("A_TITLE")
					.comma().column("PHONE")
					.comma().column("A_START_DATE")
					.comma().column("A_END_DATE")
					.comma().column("STATUS")
					.comma().column("A_NO")
					.comma().column("RE_DATE")
					.comma().column("A_DIVISION")
					.empty()
					//.enter()
				.from().subQuery(subQuery2)//.enter()
				.where().columnName("RNUM").betweenAnd("?", "?")
				.build();
		
		
		System.out.println(mainQuery.getQuery());
		
		return mainQuery.getQuery();
	}
	
	
	public StringBuilder changeListCountCptt(String dateCondition, String selectCondition, String status, String aDivision) {
		
		SelectQueryMaker ListCount1 = new SelectQueryMaker.Builder()
				.select().column("A.COM_NO")
					.comma().column("M.COM_ID")
					.comma().column("M.COM_NAME")
					.comma().column("M.COMPANY_NAME")
					.comma().column("A.A_TITLE")
					.comma().column("M.PHONE")
					.comma().column("A.A_START_DATE")
					.comma().column("A.A_END_DATE")
					.comma().column("STATUS")
					.comma().column("A.A_NO")
					.comma().column("AR.RE_DATE")
					.comma().column("A.A_DIVISION")
					.empty()
					//.enter()
				.from().tableName("ANNOUNCEMENT").as("A")//.enter()
				.join().tableName("COM_MEMBER").as("M").on().equalCondition("A.COM_NO", "M.COM_NO")//.enter()
				.join().tableName("ANN_RECORD").as("AR").on().equalCondition("A.A_NO", "AR.A_NO")//.enter()
				.join().tableName("STATUS").as("S").on().equalCondition("AR.ST_CODE", "S.RS_CODE")//.enter()
				.where().openParenthesis().column("A.A_NO").comma().toNumber().openParenthesis().substr().openParenthesis().column("AR.RE_NO").comma().num("2").closeParenthesis().closeParenthesis().closeParenthesis().empty().in().openParenthesis()
					.select().column("A_NO").comma().max1().openParenthesis().toNumber().openParenthesis().substr().openParenthesis().column("AR.RE_NO").comma().num("2").closeParenthesis().closeParenthesis().closeParenthesis().empty()
					.from().tableName("ANN_RECORD")
					.groupBy().column("A_NO").closeParenthesis().empty()
				.and().column("S.STATUS").empty().in().openParenthesis().column(status).closeParenthesis().empty()
				.and().column("A_DIVISION").empty().equal().custom(aDivision).empty()
				.orderBy().toNumber().openParenthesis().substr().openParenthesis().column("A.A_NO").comma().num("2").closeParenthesis().closeParenthesis().desc()
				.build();
		
		SelectQueryMaker ListCount2 = new SelectQueryMaker.Builder()
				.select().column("ROWNUM").as("RNUM")
				.comma().column("COM_NO")
				.comma().column("COM_ID")
				.comma().column("COM_NAME")
				.comma().column("COMPANY_NAME")
				.comma().column("A_TITLE")
				.comma().column("PHONE")
				.comma().column("A_START_DATE")
				.comma().column("A_END_DATE")
				.comma().column("STATUS")
				.comma().column("A_NO")
				.comma().column("RE_DATE")
				.comma().column("A_DIVISION")
				.empty()
				//.enter()
			.from().subQuery(ListCount1)
			.where().trunc("SYSDATE").empty().operator("-").column("RE_DATE").empty().custom(dateCondition)
			.and().custom(selectCondition)
			.build();
		
		SelectQueryMaker ListCount = new SelectQueryMaker.Builder()
				.select().column("COUNT(*)").empty()//.enter()
				.from().subQuery(ListCount2)
				.build();
		
		System.out.println(ListCount.getQuery());
		
		return ListCount.getQuery();
		
	}

}