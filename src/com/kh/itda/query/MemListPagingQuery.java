package com.kh.itda.query;

public class MemListPagingQuery {
	public StringBuilder changeQueryPer(String dateCondition, String selectCondition) {
		SelectQueryMaker subQuery1 = new SelectQueryMaker.Builder()
				.select().column("PER_NO")
					.comma().column("PER_ID")
					.comma().column("PER_NAME")
					.comma().column("PHONE")
					.comma().column("EMAIL")
					.comma().column("ENROLL_DATE")
					.comma().column("MODIFY_DATE")
					.comma().column("INFO_PERIOD")
					.comma().column("GENDER")
					.comma().column("BIRTHDAY")
					.empty()
					//.enter()
				.from().tableName("PER_MEMBER")//.enter()
				.where().custom(dateCondition)
				.and().custom(selectCondition)
				.orderBy().toNumber().openParenthesis().substr().openParenthesis().column("PER_NO").comma().num("2").closeParenthesis().closeParenthesis().desc()
				.build();
		
		SelectQueryMaker subQuery2 = new SelectQueryMaker.Builder()
				.select().column("ROWNUM").as("RNUM")
					.comma().column("PER_NO")
					.comma().column("PER_ID")
					.comma().column("PER_NAME")
					.comma().column("PHONE")
					.comma().column("EMAIL")
					.comma().column("ENROLL_DATE")
					.comma().column("MODIFY_DATE")
					.comma().column("INFO_PERIOD")
					.comma().column("GENDER")
					.comma().column("BIRTHDAY")
					.empty()
					//.enter()
				.from().subQuery(subQuery1)
				.build();
		
		
		SelectQueryMaker mainQuery = new SelectQueryMaker.Builder()
				.select().column("RNUM")
					.comma().column("PER_NO")
					.comma().column("PER_ID")
					.comma().column("PER_NAME")
					.comma().column("PHONE")
					.comma().column("EMAIL")
					.comma().column("ENROLL_DATE")
					.comma().column("MODIFY_DATE")
					.comma().column("INFO_PERIOD")
					.comma().column("GENDER")
					.comma().column("BIRTHDAY")
					.empty()
					//.enter()
				.from().subQuery(subQuery2)//.enter()
				.where().columnName("RNUM").betweenAnd("?", "?")
				.build();
		
		
		System.out.println(mainQuery.getQuery());
		
		return mainQuery.getQuery();
	}
	
	/*SELECT COUNT(*)
	FROM PER_MEMBER1 
	WHERE TRUNC(SYSDATE) - ENROLL_DATE <= 7 AND PER_NAME LIKE '%정용훈%' ORDER BY TO_NUMBER(SUBSTR(PER_NO, 2)) DESC;*/
	
	public StringBuilder changeListCountPer(String dateCondition, String selectCondition) {
		
		SelectQueryMaker ListCount = new SelectQueryMaker.Builder()
				.select().column("COUNT(*)").empty()//.enter()
				.from().tableName("PER_MEMBER")//.enter()
				.where().custom(dateCondition)
				.and().custom(selectCondition)
				.orderBy().toNumber().openParenthesis().substr().openParenthesis().column("PER_NO").comma().num("2").closeParenthesis().closeParenthesis().desc()
				.build();
		
		
		System.out.println(ListCount.getQuery());
		
		return ListCount.getQuery();
		
	}
	
	public StringBuilder changeQueryCom(String dateCondition, String selectCondition) {
		SelectQueryMaker subQuery1 = new SelectQueryMaker.Builder()
				.select().column("COM_NO")
					.comma().column("COM_ID")
					.comma().column("COM_NAME")
					.comma().column("PHONE")
					.comma().column("EMAIL")
					.comma().column("ENROLL_DATE")
					.comma().column("MODIFY_DATE")
					.comma().column("INFO_PERIOD")
					.comma().column("COM_NUM")
					.comma().column("COMPANY_NAME")
					.comma().column("COM_URL")
					.comma().column("COM_ADDRESS")
					.comma().column("NUM_EMP")
					.comma().column("INVEST")
					.comma().column("SALES")
					.comma().column("STATUS")
					.empty()
					//.enter()
				.from().tableName("COM_MEMBER").as("CM")//.enter()
				.join().tableName("MEM_STATUS").as("YMS").on().equalCondition("CM.COM_NO", "YMS.MEM_NO")//.enter()
				.join().tableName("STATUS").as("YS").on().equalCondition("YMS.RS_CODE", "YS.RS_CODE")//.enter()
				.where().openParenthesis().column("MEM_NO").comma().column("MEM_STATUS_NO").closeParenthesis().empty().in().openParenthesis()
					.select().column("MEM_NO").comma().max("MEM_STATUS_NO").empty()
					.from().tableName("MEM_STATUS")
					.where().column("MEM_NO").empty().like().prefixPattern("'B'")
					.groupBy().column("MEM_NO").closeParenthesis()
				.orderBy().toNumber().openParenthesis().substr().openParenthesis().column("COM_NO").comma().num("2").closeParenthesis().closeParenthesis().desc()
				.build();
		
		SelectQueryMaker subQuery2 = new SelectQueryMaker.Builder()
				.select().column("ROWNUM").as("RNUM")
					.comma().column("COM_NO")
					.comma().column("COM_ID")
					.comma().column("COM_NAME")
					.comma().column("PHONE")
					.comma().column("EMAIL")
					.comma().column("ENROLL_DATE")
					.comma().column("MODIFY_DATE")
					.comma().column("INFO_PERIOD")
					.comma().column("COM_NUM")
					.comma().column("COMPANY_NAME")
					.comma().column("COM_URL")
					.comma().column("COM_ADDRESS")
					.comma().column("NUM_EMP")
					.comma().column("INVEST")
					.comma().column("SALES")
					.comma().column("STATUS")
					.empty()
					//.enter()
				.from().subQuery(subQuery1)
				.where().custom(dateCondition)
				.and().custom(selectCondition)
				.build();
		
		
		SelectQueryMaker mainQuery = new SelectQueryMaker.Builder()
				.select().column("RNUM")
					.comma().column("COM_NO")
					.comma().column("COM_ID")
					.comma().column("COM_NAME")
					.comma().column("PHONE")
					.comma().column("EMAIL")
					.comma().column("ENROLL_DATE")
					.comma().column("MODIFY_DATE")
					.comma().column("INFO_PERIOD")
					.comma().column("COM_NUM")
					.comma().column("COMPANY_NAME")
					.comma().column("COM_URL")
					.comma().column("COM_ADDRESS")
					.comma().column("NUM_EMP")
					.comma().column("INVEST")
					.comma().column("SALES")
					.comma().column("STATUS")
					.empty()
					//.enter()
				.from().subQuery(subQuery2)//.enter()
				.where().columnName("RNUM").betweenAnd("?", "?")
				.build();
		
		
		System.out.println(mainQuery.getQuery());
		
		return mainQuery.getQuery();
	}

	
/*	SELECT COUNT(*)
	FROM COM_MEMBER1 CM
	JOIN MEM_STATUS YMS ON (CM.COM_NO = YMS.MEM_NO)
	JOIN STATUS YS ON (YMS.RS_CODE = YS.RS_CODE)
	WHERE (MEM_NO, MEM_STATUS_NO) IN (SELECT MEM_NO, MAX(MEM_STATUS_NO) 
	                                  FROM MEM_STATUS
	                                  WHERE MEM_NO LIKE 'B%'
	                                  GROUP BY MEM_NO)
	ORDER BY TO_NUMBER(SUBSTR(COM_NO,2));*/
	
	public StringBuilder changeListCountCom(String dateCondition, String selectCondition) {
		
		SelectQueryMaker ListCount1 = new SelectQueryMaker.Builder()
				.select().column("COM_NO")
					.comma().column("COM_ID")
					.comma().column("COM_NAME")
					.comma().column("PHONE")
					.comma().column("EMAIL")
					.comma().column("ENROLL_DATE")
					.comma().column("MODIFY_DATE")
					.comma().column("INFO_PERIOD")
					.comma().column("COM_NUM")
					.comma().column("COMPANY_NAME")
					.comma().column("COM_URL")
					.comma().column("COM_ADDRESS")
					.comma().column("NUM_EMP")
					.comma().column("INVEST")
					.comma().column("SALES")
					.comma().column("STATUS")
					.empty()
					//.enter()
				.from().tableName("COM_MEMBER").as("CM")//.enter()
				.join().tableName("MEM_STATUS").as("YMS").on().equalCondition("CM.COM_NO", "YMS.MEM_NO")//.enter()
				.join().tableName("STATUS").as("YS").on().equalCondition("YMS.RS_CODE", "YS.RS_CODE")//.enter()
				.where().openParenthesis().column("MEM_NO").comma().column("MEM_STATUS_NO").closeParenthesis().empty().in().openParenthesis()
					.select().column("MEM_NO").comma().max("MEM_STATUS_NO").empty()
					.from().tableName("MEM_STATUS")
					.where().column("MEM_NO").empty().like().prefixPattern("'B'")
					.groupBy().column("MEM_NO").closeParenthesis()
				.orderBy().toNumber().openParenthesis().substr().openParenthesis().column("COM_NO").comma().num("2").closeParenthesis().closeParenthesis().desc()
				.build();
		
		SelectQueryMaker ListCount = new SelectQueryMaker.Builder()
				.select().column("COUNT(*)").empty()//.enter()
				.from().subQuery(ListCount1)
				.where().custom(dateCondition)
				.and().custom(selectCondition)
				.build();
		
		System.out.println(ListCount.getQuery());
		
		return ListCount.getQuery();
	}

}
