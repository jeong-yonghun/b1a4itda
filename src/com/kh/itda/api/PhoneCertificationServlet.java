package com.kh.itda.api;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

@WebServlet("/phoneCertification.ph")
public class PhoneCertificationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public PhoneCertificationServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		
		String phone = request.getParameter("phone");
		
		System.out.println(phone);
		
		String randomVal = new Coolsms().sms(phone);
//		String randomVal = phone;
		
		
//		List<Board> replyList = new BoardService().insertReply(board);
		
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		
		new Gson().toJson(randomVal, response.getWriter());
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
