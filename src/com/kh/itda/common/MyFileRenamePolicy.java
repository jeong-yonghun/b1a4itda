package com.kh.itda.common;

import java.io.File;
import java.sql.Date;
import java.text.SimpleDateFormat;

import com.oreilly.servlet.multipart.FileRenamePolicy;

public class MyFileRenamePolicy implements FileRenamePolicy{

	@Override
	public File rename(File oldFile) {
		long currentTime = System.currentTimeMillis(); // 현재 시간을 반환1970.1.1 0:0:0 기준이다.
		
		System.out.println(currentTime);
		
		//파일의 이름을 겹치지 않게 바꿔주기 위해서!
		SimpleDateFormat ft = new SimpleDateFormat("yyyyMMddHHmmss");
		String now = ft.format(new Date(currentTime));
		int randomNumber = (int) (Math.random() * 100000);
		
		String name = oldFile.getName();
		String body = "";
		String ext = "";
		int dot = name.lastIndexOf("."); // .의 인덱스 위치를 찾는다.
		if(dot != -1) {//lastIndexOf를 찾지 못했을 경우 -1리턴
			//확장자가 있는 경우
			body = name.substring(0, dot);
			ext = name.substring(dot);
		} else {
			//확장자가 없는 경우
			body = name;
		}
		
		String fileName = now + randomNumber + ext;
		
		File newFile = new File(oldFile.getParent(), fileName);
		return newFile;
	}

}
