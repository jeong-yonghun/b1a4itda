package com.kh.itda.common.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kh.itda.common.model.service.IndexCountService;

/**
 * Servlet implementation class AdminCountComPerServlet
 */
@WebServlet("/adminComPerAll.co")
public class AdminCountComPerServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * 
     * 
     * @see HttpServlet#HttpServlet()
     */
    public AdminCountComPerServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		int result=new IndexCountService().ComPerAll();
		System.out.println("SEve:"+result);
		response.getWriter().print(result);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
		
	}

}
