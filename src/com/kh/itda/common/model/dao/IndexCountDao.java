package com.kh.itda.common.model.dao;

import static com.kh.itda.common.JDBCTemplate.close;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

import com.kh.itda.addInfo.resume.model.dao.ResumeDao;

public class IndexCountDao {
	private Properties prop=new Properties();

	public IndexCountDao() {
		String fileName=ResumeDao.class.getResource("/sql/indexCount.properties").getPath();
		try {
			prop.load(new FileReader(fileName));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public int Index1(Connection con) {
		PreparedStatement pstmt=null;
		ResultSet rset=null;
		int result=0;
		String result1="";
		String query=prop.getProperty("countAnonce");
		
		try {
			pstmt=con.prepareStatement(query);
			
			rset=pstmt.executeQuery();
			if(rset.next()) {
				result1=rset.getString("Count(*)");
			}
			result=Integer.parseInt(result1);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
		}
		
		return result;
	}
	public int Index2(Connection con) {
		PreparedStatement pstmt=null;
		ResultSet rset=null;
		int result=0;
		String result1="";
		String query=prop.getProperty("countPerson");
		
		try {
			pstmt=con.prepareStatement(query);
			
			rset=pstmt.executeQuery();
			if(rset.next()) {
				result1=rset.getString("Count(*)");
			}
			result=Integer.parseInt(result1);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
		}
		
		return result;
	}
	public int Index3(Connection con) {
		PreparedStatement pstmt=null;
		ResultSet rset=null;
		int result=0;
		String result1="";
		String query=prop.getProperty("countContest");
		
		try {
			pstmt=con.prepareStatement(query);
			
			rset=pstmt.executeQuery();
			if(rset.next()) {
				
				result1=rset.getString("Count(*)");
			}
			result=Integer.parseInt(result1);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
		}
		
		return result;	
	}
	public int ComPerAll(Connection con) {
		PreparedStatement pstmt=null;
		ResultSet rset=null;
		int result=0;
		String result1="";
		String query=prop.getProperty("countComPerAll");
		
		try {
			pstmt=con.prepareStatement(query);
			
			rset=pstmt.executeQuery();
			if(rset.next()) {
				result1=rset.getString("Count(*)");
			}
			result=Integer.parseInt(result1);
			System.out.println("re:"+result1);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
		}
		
		return result;
	}

}
