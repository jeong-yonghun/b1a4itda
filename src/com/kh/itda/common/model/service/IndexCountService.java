package com.kh.itda.common.model.service;

import static com.kh.itda.common.JDBCTemplate.close;
import static com.kh.itda.common.JDBCTemplate.commit;
import static com.kh.itda.common.JDBCTemplate.getConnection;
import static com.kh.itda.common.JDBCTemplate.rollback;

import java.sql.Connection;

import com.kh.itda.common.model.dao.IndexCountDao;

public class IndexCountService {

	public int Index1() {
		Connection con=getConnection();
		int result=new IndexCountDao().Index1(con);
		
		if(result>0) {
			commit(con);
		}else {
			rollback(con);
		}
		close(con);
		
		return result;
	}

	public int Index2() {
		Connection con=getConnection();
		int result=new IndexCountDao().Index2(con);
		
		if(result>0) {
			commit(con);
		}else {
			rollback(con);
		}
		close(con);
		
		return result;
	}

	public int Index3() {
		Connection con=getConnection();
		int result=new IndexCountDao().Index3(con);
		
		if(result>0) {
			commit(con);
		}else {
			rollback(con);
		}
		close(con);
		
		return result;
	}

	public int ComPerAll() {
		Connection con=getConnection();
		int result=new IndexCountDao().ComPerAll(con);
		
		if(result>0) {
			commit(con);
		}else {
			rollback(con);
		}
		close(con);
		
		return result;
	}

}
