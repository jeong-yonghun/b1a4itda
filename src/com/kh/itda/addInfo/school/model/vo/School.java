package com.kh.itda.addInfo.school.model.vo;

import java.sql.Date;

public class School {
	private String caName;//학력이름
	private String caNo;//학력번호
	private String perNo;//회원번호
	private Date caEnrollDate;//입학일
	private Date caGraduDate;//졸업일
	private String caMajor;//전공
	private String caDegree;//학위
	private String caGrades;//내학점
	private String caTopGrades;//최대학점
	private String etc;//기타
	
	
	public School() {}


	public School(String caName, String caNo, String perNo, Date caEnrollDate, Date caGraduDate, String caMajor,
			String caDegree, String caGrades, String caTopGrades, String etc) {
		super();
		this.caName = caName;
		this.caNo = caNo;
		this.perNo = perNo;
		this.caEnrollDate = caEnrollDate;
		this.caGraduDate = caGraduDate;
		this.caMajor = caMajor;
		this.caDegree = caDegree;
		this.caGrades = caGrades;
		this.caTopGrades = caTopGrades;
		this.etc = etc;
	}


	public String getCaName() {
		return caName;
	}


	public void setCaName(String caName) {
		this.caName = caName;
	}


	public String getCaNo() {
		return caNo;
	}


	public void setCaNo(String caNo) {
		this.caNo = caNo;
	}


	public String getPerNo() {
		return perNo;
	}


	public void setPerNo(String perNo) {
		this.perNo = perNo;
	}


	public Date getCaEnrollDate() {
		return caEnrollDate;
	}


	public void setCaEnrollDate(Date caEnrollDate) {
		this.caEnrollDate = caEnrollDate;
	}


	public Date getCaGraduDate() {
		return caGraduDate;
	}


	public void setCaGraduDate(Date caGraduDate) {
		this.caGraduDate = caGraduDate;
	}


	public String getCaMajor() {
		return caMajor;
	}


	public void setCaMajor(String caMajor) {
		this.caMajor = caMajor;
	}


	public String getCaDegree() {
		return caDegree;
	}


	public void setCaDegree(String caDegree) {
		this.caDegree = caDegree;
	}


	public String getCaGrades() {
		return caGrades;
	}


	public void setCaGrades(String caGrades) {
		this.caGrades = caGrades;
	}


	public String getCaTopGrades() {
		return caTopGrades;
	}


	public void setCaTopGrades(String caTopGrades) {
		this.caTopGrades = caTopGrades;
	}


	public String getEtc() {
		return etc;
	}


	public void setEtc(String etc) {
		this.etc = etc;
	}


	@Override
	public String toString() {
		return "School [caName=" + caName + ", caNo=" + caNo + ", perNo=" + perNo + ", caEnrollDate=" + caEnrollDate
				+ ", caGraduDate=" + caGraduDate + ", caMajor=" + caMajor + ", caDegree=" + caDegree + ", caGrades="
				+ caGrades + ", caTopGrades=" + caTopGrades + ", etc=" + etc + "]";
	}
	
	
}
