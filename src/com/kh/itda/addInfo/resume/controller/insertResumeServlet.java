package com.kh.itda.addInfo.resume.controller;

import java.io.File;
import java.io.IOException;
import java.sql.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.GregorianCalendar;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;

import com.kh.itda.addInfo.resume.model.service.ResumeService;
import com.kh.itda.addInfo.resume.model.vo.Resume;
import com.kh.itda.addInfo.school.model.vo.School;
import com.kh.itda.common.MyFileRenamePolicy;
import com.kh.itda.member.model.vo.Jyh_PerMember;
import com.kh.itda.member.model.vo.hyj_Attachment;
import com.kh.itda.member.model.vo.hyj_Previous;
import com.kh.itda.member.model.vo.hyj_awards;
import com.kh.itda.member.model.vo.hyj_certificate;
import com.kh.itda.member.model.vo.hyj_developmentinsert;
import com.kh.itda.member.model.vo.hyj_education;
import com.kh.itda.member.model.vo.hyj_volunteer;
import com.kh.itda.recruitAndContest.model.vo.Attachment;
import com.oreilly.servlet.MultipartRequest;

import oracle.sql.DATE;

/**
 * Servlet implementation class insertResumeServlet
 */
@WebServlet(name = "InsertResumeServlet", urlPatterns = { "/insertResume.re" })
public class insertResumeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public insertResumeServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
if(ServletFileUpload.isMultipartContent(request)) {
			
			int maxSize = 1024 * 1024 * 10;
			
			String root = request.getSession().getServletContext().getRealPath("/");
			
			System.out.println(root);
			
			
			String savePath = root + "uploadFiles/";
			
			MultipartRequest multiRequest = 
					new MultipartRequest(request, savePath, maxSize, "UTF-8", new MyFileRenamePolicy());
			
			
			
			
	         
		Jyh_PerMember loginUser=(Jyh_PerMember)request.getSession().getAttribute("loginUser");
		ArrayList<HashMap<String,Object>> list=new ArrayList<>();
		String rTitle=multiRequest.getParameter("resumeTitle");//이력서 제목
		String userName=multiRequest.getParameter("userName");//사용자 이름
		String userEmail=multiRequest.getParameter("userEmail");//이메일
		String userPhone=multiRequest.getParameter("userPhone");//번호
		String userBirth=multiRequest.getParameter("userBirth");//생년월일
		String gender=multiRequest.getParameter("hyj_gender");//성별
		String rHopeDuty=multiRequest.getParameter("job");//직군
		String[] skill=multiRequest.getParameterValues("skill-exp");//스킬
		String rSkill=String.join(",", skill);//주요기술
		String rContent=multiRequest.getParameter("rContent");//자기소개
		String rPublic=multiRequest.getParameter("publicOption");//공개여부
		String moneyYN=multiRequest.getParameter("moneyYN");
		
		
		//String portfolio=multiRequest.getParameter("portfolio");//포트폴리오
		java.sql.Date day0 = null;
		if(userBirth !=  "") {
			day0 = java.sql.Date.valueOf(userBirth);
			
		}else {
			day0 = new java.sql.Date(new GregorianCalendar().getTimeInMillis());
		}
		loginUser.setBirthDay(day0);
		loginUser.setGender(gender);
		loginUser.setEmail(userEmail);
		loginUser.setPhone(userPhone);
		
		Resume resume=new Resume();
		resume.setrTitle(rTitle);
		resume.setrContent(rContent);
		resume.setrSkill(rSkill);
		resume.setrHopeDuty(rHopeDuty);
		resume.setrPublic(rPublic);
		//System.out.println("moneyYN"+moneyYN);
		if(moneyYN != null) {
			if(moneyYN.equals("Y")) {
				String rHopeSalary=multiRequest.getParameter("hyj_mymoney-002");//연봉
				resume.setrHopeYearlySalary(rHopeSalary);
			}
		}
///////////////회사
		String comCheck=multiRequest.getParameter("companyYN");


		System.out.println("comCheck"+comCheck);
		HashMap<String,Object> comList=new HashMap<>();
		hyj_Previous c=null;
		java.sql.Date day = null;
		java.sql.Date day1 = null;
		if(comCheck != null) {
		if(comCheck.equals("Y")) {
				
				
				String[] comNo =multiRequest.getParameterValues("comNo");
				//System.out.println("comNo길이"+comNo.length);
				String[] hyj_backcompany=multiRequest.getParameterValues("hyj_backcompany");
				String[] hyj_companyjob=multiRequest.getParameterValues("hyj_companyjob");
				String[] hyj_joindate=multiRequest.getParameterValues("hyj_joindate");
				String[] hyj_Resignationdate1=multiRequest.getParameterValues("hyj_Resignationdate");
				String[] hyj_userstack=multiRequest.getParameterValues("hyj_userstack");
				String[] hyj_money=multiRequest.getParameterValues("hyj_money");
				for(int j=0;j<comNo.length;j++) {
					c=new hyj_Previous();
					String iString=Integer.toString(j);
					c.setPreviousNo(comNo[j]);
					c.setPreviousname(hyj_backcompany[j]);
					c.setPreviousjobname(hyj_companyjob[j]);
					if(hyj_joindate[j] !=  "") {
						day = java.sql.Date.valueOf(hyj_joindate[j]);
						
					}else {
						day = new java.sql.Date(new GregorianCalendar().getTimeInMillis());
					}
					c.setPreviousjoindate(day);
					if(hyj_Resignationdate1[j] !=  "") {
						day1 = java.sql.Date.valueOf(hyj_Resignationdate1[j]);
						
					}else {
						day1 = new java.sql.Date(new GregorianCalendar().getTimeInMillis());
					}
					c.setResignationdate(day1);
					c.setStack(hyj_userstack[j]);
					c.setMoney(hyj_money[j]);
					comList.put("company"+iString, c);
				}
		
				
				list.add(comList);
				System.out.println("받아오는회사값:"+comList);
			}
		}
		
///////////////////개발		
		String develCheck=multiRequest.getParameter("develYN");
		System.out.println("devleCheck"+develCheck);
		HashMap<String,Object> develList=new HashMap<>();
		hyj_developmentinsert d=null;
		java.sql.Date day2 = null;
		java.sql.Date day3 = null;
		if(develCheck != null) {
		if(develCheck.equals("Y")) {
			String[] develNo=multiRequest.getParameterValues("delveNo");
			String[] hyj_joindate1=multiRequest.getParameterValues("hyj_joindate");
			String[] hyj_Resignationdate1=multiRequest.getParameterValues("hyj_Resignationdate");
			String[] hyj_userstack1=multiRequest.getParameterValues("hyj_userstack");
			String[] hyj_userskill=multiRequest.getParameterValues("hyj_userskill");
			String[] hyj_usergit=multiRequest.getParameterValues("hyj_usergit");
			for(int i=0;i<develNo.length;i++) {
				String iString=Integer.toString(i);
				d=new hyj_developmentinsert();
				d.setDevelNo(develNo[i]);
				if(hyj_joindate1[i] !=  "") {
					day2 = java.sql.Date.valueOf(hyj_joindate1[i]);
					
				}else {
					day2 = new java.sql.Date(new GregorianCalendar().getTimeInMillis());
				}
				d.setJoindate(day2);
				if(hyj_Resignationdate1[i] !=  "") {
					day3 = java.sql.Date.valueOf(hyj_Resignationdate1[i]);
					
				}else {
					day3 = new java.sql.Date(new GregorianCalendar().getTimeInMillis());
				}
				d.setResignationdate(day3);
				d.setDuserstack(hyj_userstack1[i]);
				d.setDuserskill(hyj_userskill[i]);
				d.setUsergit(hyj_usergit[i]);
				develList.put("devel"+iString, d);
			}
			list.add(develList);
			System.out.println("받아오는개발이력값:"+develList);
		}
		}
//		
/////////////////////교육
		String eduCheck=multiRequest.getParameter("eduYN");
		System.out.println("eduCheck"+eduCheck);
		HashMap<String,Object> eduList=new HashMap<>();
		hyj_education e=null;
		java.sql.Date day4 = null;
		java.sql.Date day5 = null;
		if(eduCheck != null) {
		if(eduCheck.equals("Y")) {
			String[] eduNo=multiRequest.getParameterValues("eduNo");
			String[] eduName=multiRequest.getParameterValues("hyj_educationname");
			String[] hyj_educationstartday=multiRequest.getParameterValues("hyj_educationstartday");
			String[] hyj_educationlastday=multiRequest.getParameterValues("hyj_educationlastday");
			String[] hyj_educationstack=multiRequest.getParameterValues("hyj_educationstack");
			String[] hyj_educationkil=multiRequest.getParameterValues("hyj_educationkil");
			for(int i=0;i<eduNo.length;i++) {
				String iString=Integer.toString(i);
				e=new hyj_education();
				e.setEducationNo(eduNo[i]);
				e.setEducationname(eduName[i]);
				if(hyj_educationstartday[i] !=  "") {
					day4 = java.sql.Date.valueOf(hyj_educationstartday[i]);
					
				}else {
					day4 = new java.sql.Date(new GregorianCalendar().getTimeInMillis());
				}
				e.setEducationstartday(day4);
				if(hyj_educationlastday[i] !=  "") {
					day5 = java.sql.Date.valueOf(hyj_educationlastday[i]);
					
				}else {
					day5 = new java.sql.Date(new GregorianCalendar().getTimeInMillis());
				}
				e.setEducationlastday(day5);
				e.setEducationstack(hyj_educationstack[i]);
				e.setEducationskil(hyj_educationkil[i]);
				
				eduList.put("edu"+iString, e);
			}
			list.add(eduList);
			System.out.println("받아오는교육이력값:"+eduList);
		}
		}
///////////////////수상
		String awardCheck=multiRequest.getParameter("awardYN");
		System.out.println("awardCheck"+awardCheck);
		HashMap<String,Object> awardList=new HashMap<>();
		hyj_awards a=null;
		java.sql.Date day6 = null;
		if(awardCheck != null) {
		if(awardCheck.equals("Y")) {
			String[] awardNo=multiRequest.getParameterValues("awardNo");
			String[] hyj_Awardsname=multiRequest.getParameterValues("hyj_Awardsname");
			String[] hyj_Awardscontent=multiRequest.getParameterValues("hyj_Awardscontent");
			String[] hyj_Awardsmonth=multiRequest.getParameterValues("hyj_Awardsmonth");
			String[] hyj_AwardsAgency=multiRequest.getParameterValues("hyj_AwardsAgency");
			for(int i=0;i<awardNo.length;i++) {
				a=new hyj_awards(); 
				String iString=Integer.toString(i);
				a.setAwardNo(awardNo[i]);
				a.setAwardsname(hyj_Awardsname[i]);
				a.setAwardscontent(hyj_Awardscontent[i]);
				if(hyj_Awardsmonth[i] !=  "") {
					day6 = java.sql.Date.valueOf(hyj_Awardsmonth[i]);
					
				}else {
					day6 = new java.sql.Date(new GregorianCalendar().getTimeInMillis());
				}
				a.setAwardsmonth(day6);
				a.setAwardsagency(hyj_AwardsAgency[i]);
				awardList.put("award"+iString, a);
			}
			
			list.add(awardList);
			System.out.println("받아오는수상이력값:"+awardList);
		}
		}
/////////////////////////////활동
		String volCheck=multiRequest.getParameter("volYN");
		System.out.println("volCheck"+volCheck);
		HashMap<String,Object> volList=new HashMap<>();
		hyj_volunteer v=null;
		java.sql.Date day7 = null;
		java.sql.Date day8 = null;
		if(volCheck != null) {
		if(volCheck.equals("Y")) {
			String[] volNo=multiRequest.getParameterValues("volNo");
			String[] hyj_Volunteername=multiRequest.getParameterValues("hyj_Volunteername");
			String[] hyj_Volunteerfirst=multiRequest.getParameterValues("hyj_Volunteerfirst");
			String[] hyj_Volunteerlast=multiRequest.getParameterValues("hyj_Volunteerlast");
			String[] hyj_Volunteerlink=multiRequest.getParameterValues("hyj_Volunteerlink");
			String[] hyj_VolunteerExplanation=multiRequest.getParameterValues("hyj_VolunteerExplanation");
			for(int i=0;i<volNo.length;i++) {
				String iString=Integer.toString(i);
				v=new hyj_volunteer();
				v.setVolunteerNo(volNo[i]);
				v.setVolunteername(hyj_Volunteername[i]);
				if(hyj_Volunteerfirst[i] !=  "") {
					day7 = java.sql.Date.valueOf(hyj_Volunteerfirst[i]);	
				}else {
					day7 = new java.sql.Date(new GregorianCalendar().getTimeInMillis());
				}
				v.setVolunteerStartDate(day7);
				if(hyj_Volunteerlast[i] !=  "") {
					day8 = java.sql.Date.valueOf(hyj_Volunteerlast[i]);	
				}else {
					day8 = new java.sql.Date(new GregorianCalendar().getTimeInMillis());
				}
				v.setVolunteerendDate(day8);
				v.setVolunteerlink(hyj_Volunteerlink[i]);
				v.setVolunteerExplanation(hyj_VolunteerExplanation[i]);
				volList.put("volun"+iString, v);
			}
			list.add(volList);
			System.out.println("받아오는활동이력값:"+volList);
		}
		}

/////////////////////////////학력
		String schoolCheck=multiRequest.getParameter("schoolYN");
		System.out.println("schoolCheck"+schoolCheck);
		HashMap<String,Object> schoolList=new HashMap<>();
		School s=null;
		java.sql.Date day9 = null;
		java.sql.Date day10 = null;
		if(schoolCheck != null) {
		if(schoolCheck.equals("Y")) {
			String[] scNo=multiRequest.getParameterValues("scNo");
			String[] hyj_school=multiRequest.getParameterValues("hyj_school-001");
			String[] hyj_admission=multiRequest.getParameterValues("hyj_admission");
			String[] hyj_graduated=multiRequest.getParameterValues("hyj_graduated");
			String[] hyj_major=multiRequest.getParameterValues("hyj_major");
			String[] hyj_degree=multiRequest.getParameterValues("hyj_degree");
			String[] hyj_grades=multiRequest.getParameterValues("hyj_grades");
			String[] hyj_topgrades=multiRequest.getParameterValues("hyj_topgrades");
			String[] hyj_explanation=multiRequest.getParameterValues("hyj_explanation");
			for(int i=0;i<scNo.length;i++) {
				String iString=Integer.toString(i);
				s=new School();
				s.setCaNo(scNo[i]);
				s.setCaName(hyj_school[i]);
				if(hyj_admission[i] !=  "") {
					day9 = java.sql.Date.valueOf(hyj_admission[i]);	
				}else {
					day9 = new java.sql.Date(new GregorianCalendar().getTimeInMillis());
				}
				s.setCaEnrollDate(day9);
				if(hyj_graduated[i] !=  "") {
					day10 = java.sql.Date.valueOf(hyj_graduated[i]);	
				}else {
					day10 = new java.sql.Date(new GregorianCalendar().getTimeInMillis());
				}
				s.setCaGraduDate(day10);
				s.setCaMajor(hyj_major[i]);
				s.setCaDegree(hyj_degree[i]);
				s.setCaGrades(hyj_grades[i]);
				s.setCaTopGrades(hyj_topgrades[i]);
				s.setEtc(hyj_explanation[i]);
				schoolList.put("school"+iString, s);
			}
			System.out.println("받아오는학력 값:"+schoolList);
			list.add(schoolList);
		}
		}
		
		
///////////////////////////////자격증		
		String licenseCheck=multiRequest.getParameter("licenseYN");
		System.out.println("licenseCheck"+licenseCheck);
		HashMap<String,Object> licenseList=new HashMap<>();
		hyj_certificate l=null;
		java.sql.Date day11 = null;
		java.sql.Date day12 = null;
		if(licenseCheck != null) {
		if(licenseCheck.equals("Y")) {
			String[] liNo=multiRequest.getParameterValues("liNo");
			String[] hyj_certificatename=multiRequest.getParameterValues("hyj_certificatename");
			String[] hyj_certificatestart=multiRequest.getParameterValues("hyj_certificatestart");
			String[] hyj_certificateend=multiRequest.getParameterValues("hyj_certificateend");
			String[] hyj_certificateagency=multiRequest.getParameterValues("hyj_certificateagency");
			for(int i=0;i<liNo.length;i++) {
				String iString=Integer.toString(i);
				l=new hyj_certificate();
				l.setCertificateNo(liNo[i]);
				
				l.setCertificatename(hyj_certificatename[i]);
				if(hyj_certificatestart[i] !=  "") {
					day11 = java.sql.Date.valueOf(hyj_certificatestart[i]);	
				}else {
					day11 = new java.sql.Date(new GregorianCalendar().getTimeInMillis());
				}
				l.setCertificatestart(day11);
				if(hyj_certificateend[i] !=  "") {
					day12 = java.sql.Date.valueOf(hyj_certificateend[i]);	
				}else {
					day12 = new java.sql.Date(new GregorianCalendar().getTimeInMillis());
				}
				l.setCertificateend(day12);
				l.setCertificateagency(hyj_certificateagency[i]);
				licenseList.put("license"+iString, l);
			}
			System.out.println("받아오는자격증 값:"+schoolList);
			list.add(licenseList);
		}
		}
	

		/////////////////////////////파일
		String fileCheck=multiRequest.getParameter("fileYN");
		System.out.println("fileCheck"+fileCheck);
		HashMap<String,Object> fileList=new HashMap<>();
		//hyj_Attachment f=null;
		if(fileCheck != null) {
			if(fileCheck.equals("Y")) {
				//String[] file=multiRequest.getParameterValues("file");
				//for(int i=0;i<file.length;i++) {
				//f=new hyj_Attachment();
				//f.setOriginName(file[i]);
				//String iString=Integer.toString(i);
				//fileList.put("file"+iString, f);
				Enumeration<String> file = multiRequest.getFileNames();

				String name = file.nextElement();
				

				String saveFile =  multiRequest.getFilesystemName(name);
				String originFile = multiRequest.getOriginalFileName(name);
				Attachment at = new Attachment();
				at.setFilePath(savePath);
				at.setOriginName(originFile);
				at.setChangeName(saveFile);
				//}
				//list.add(fileList);
				System.out.println("받아오는파일이력값:"+fileList);
				int result=new ResumeService().insertResume(resume,list,loginUser,at);
				String page="";
				if(result>0) {
					//나중에 페이지 정해지면 작성
					System.out.println("파일넣었을때 안오류");
					page=request.getContextPath()+"/resumeList.re";
					response.sendRedirect(page);
				}else {
					
					
					File failedFile = new File(savePath + saveFile);
					failedFile.delete();
					System.out.println("파일넣었을때 오류");
					request.setAttribute("msg", "이력서작성 실패!");
					request.getRequestDispatcher("views/common/errorPage.jsp").forward(request, response);
				}
			}
		}else {
			int result=new ResumeService().insertResume(resume,list,loginUser);
			String page="";
			if(result>0) {
				//나중에 페이지 정해지면 작성
				System.out.println("파일 안넣었을때 안오류");
				page=request.getContextPath()+"/resumeList.re";
				response.sendRedirect(page);
			}else {
				System.out.println("파일 안넣었을때 오류");
				request.setAttribute("msg", "이력서작성 실패!");
				request.getRequestDispatcher("views/common/errorPage.jsp").forward(request, response);
			}
		}

	
		
	}
}
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
