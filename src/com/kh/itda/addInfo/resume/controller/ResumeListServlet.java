package com.kh.itda.addInfo.resume.controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.kh.itda.addInfo.resume.model.service.ResumeService;
import com.kh.itda.addInfo.resume.model.vo.Resume;
import com.kh.itda.member.model.vo.Jyh_PerMember;

/**
 * Servlet implementation class ResumeListServlet
 */
@WebServlet("/resumeList.re")
public class ResumeListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ResumeListServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		//로그인구현하면 사용하기
		Jyh_PerMember loginUser=(Jyh_PerMember)request.getSession().getAttribute("loginUser");
		
		
		ArrayList<Resume> list=new ResumeService().selectList(loginUser.getPerNo());
		//ArrayList<Resume> list=new ResumeService().selectList("B1");
		System.out.println("서블릿 이력서 list:"+list);
		
		
		String page="";
		if(list!=null) {
			page="views/person/personMyPage/personMyPageResume/cho_personMyPageResumeMain.jsp";
			request.setAttribute("list",list);
		}else {
			page="views/common/errorPage.jsp";
			request.setAttribute("msg","이력서목록조회 실패");
		}
		request.getRequestDispatcher(page).forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
