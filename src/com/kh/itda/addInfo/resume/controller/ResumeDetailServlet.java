package com.kh.itda.addInfo.resume.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kh.itda.addInfo.resume.model.service.ResumeService;
import com.kh.itda.member.model.vo.Jyh_PerMember;

/**
 * Servlet implementation class ResumeDetailServlet
 */
@WebServlet("/resumeDetail.re")
public class ResumeDetailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ResumeDetailServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		String Rno=request.getParameter("rNo");
		System.out.println("rno" +Rno);
		Jyh_PerMember loginUser=(Jyh_PerMember)request.getSession().getAttribute("loginUser");
		
		String userNo = null;
		System.out.println("aaaaaaaaaaa = " + loginUser);
		
		if(loginUser != null) {
			userNo = loginUser.getPerNo();
		}else {
			userNo = request.getParameter("userNo");
		}
		ArrayList<HashMap<String,Object>> resumeDetail=new ResumeService().DetailResume(Rno,userNo);
		
		System.out.println("이력서 내용들 서블릿에서:"+resumeDetail);
		String page="";
		if(resumeDetail!=null) {
			page="views/person/personMyPage/personMyPageResume/cho_personMyPageResumeDetail.jsp";
			request.setAttribute("resumeDetail", resumeDetail);
		}else {
			page="views/common/errorPage.jsp";
			request.setAttribute("msg", "이력서 디테일값 불러오기 실패");
		}
		request.getRequestDispatcher(page).forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
