package com.kh.itda.addInfo.resume.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kh.itda.addInfo.resume.model.service.ResumeService;

/**
 * Servlet implementation class ResumeDeleteServlet
 */
@WebServlet("/deleteResume.re")
public class ResumeDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ResumeDeleteServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String resumeNo=request.getParameter("rNo");
		System.out.println("번호"+resumeNo);
		int result=new ResumeService().deleteResume(resumeNo);
		
		String page="";
		if(result>0) {
			response.sendRedirect("/itda/resumeList.re");
			
		}else {
			page = "views/common/errorPage.jsp";
			request.setAttribute("msg", "이력서삭제 실패!");
			
			request.getRequestDispatcher(page).forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
