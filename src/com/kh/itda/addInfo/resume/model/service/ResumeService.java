package com.kh.itda.addInfo.resume.model.service;

import static com.kh.itda.common.JDBCTemplate.*;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;

import com.kh.itda.addInfo.portfolio.model.vo.Portfolio;
import com.kh.itda.addInfo.resume.model.dao.ResumeDao;
import com.kh.itda.addInfo.resume.model.vo.Resume;
import com.kh.itda.addInfo.school.model.vo.School;
import com.kh.itda.member.model.vo.Jyh_PerMember;
import com.kh.itda.member.model.vo.hyj_Attachment;
import com.kh.itda.member.model.vo.hyj_Previous;
import com.kh.itda.member.model.vo.hyj_awards;
import com.kh.itda.member.model.vo.hyj_certificate;
import com.kh.itda.member.model.vo.hyj_developmentinsert;
import com.kh.itda.member.model.vo.hyj_education;
import com.kh.itda.member.model.vo.hyj_volunteer;
import com.kh.itda.recruitAndContest.model.vo.Attachment;

import java.util.Map.Entry; 

public class ResumeService {

	public int insertResume(Resume resume,ArrayList<HashMap<String,Object>> list, Jyh_PerMember loginUser) {
		Connection con=getConnection();
		
		//이력서 테이블에 값 넣기
		int result=new ResumeDao().insertResume(con,resume,loginUser);
    	System.out.println("resumeDetailArrayList jsp파일에서" + list);
		ArrayList<hyj_Previous> companyCareer=new ArrayList<hyj_Previous>();
		ArrayList<hyj_education> eduCareer=new ArrayList<hyj_education>();
		ArrayList<hyj_developmentinsert> DevelCareer=new ArrayList<hyj_developmentinsert>();
		ArrayList<hyj_awards> award=new ArrayList<hyj_awards>();
		ArrayList<School> school=new ArrayList<School>();
		ArrayList<hyj_volunteer> volunte=new ArrayList<hyj_volunteer>();
		ArrayList<Portfolio> portfolio=new ArrayList<Portfolio>();
		ArrayList<hyj_certificate> license=new ArrayList<hyj_certificate>();
		ArrayList<hyj_Attachment> file= new ArrayList<hyj_Attachment>();
		
		
   /* for(int i = 0; i < list.size(); i++){
        //arraylist 사이즈 만큼 for문을 실행합니다.
        System.out.println("list 순서 " + i + "번쨰");
        for( Entry<String, Object> elem : list.get(i).entrySet() ){
            // list 각각 hashmap받아서 출력합니다.
            System.out.println( String.format("키 : %s, 값 : %s", elem.getKey(), elem.getValue()) );
           for(int j=0;j<list.size();j++){
        	   System.out.println("for문들어오는 지확인");
            	if(elem.getKey().equals("company"+j)){
            		companyCareer.add((hyj_Previous)elem.getValue());
            	}else if(elem.getKey().equals("edu"+j)){
            		eduCareer.add((hyj_education)elem.getValue());
            	}else if(elem.getKey().equals("devel"+j)){
            		DevelCareer.add((hyj_developmentinsert)elem.getValue());
            	}else if(elem.getKey().equals("award"+j)){
            		award.add((hyj_awards)elem.getValue());
            	}else if(elem.getKey().equals("school"+j)){
            		school.add((School)elem.getValue());
            	}else if(elem.getKey().equals("volun"+j)){
            		 volunte.add((hyj_volunteer)elem.getValue());
            	}else if(elem.getKey().equals("Portfolio"+j)){
            		portfolio.add((Portfolio)elem.getValue());
            	}else if(elem.getKey().equals("license"+j)){
            		license.add((hyj_certificate)elem.getValue());
            	}else if(elem.getKey().equals("file"+j)){
            		file.add((hyj_Attachment)elem.getValue());
            	}
           }
          // companyCareer=new ArrayList<hyj_Previous>();
           //ArrayList<hyj_Previous> companyCareer1= companyCareer<Object>;
           System.out.println("CompanyArray:"+companyCareer);
           System.out.println("eduArray:"+eduCareer);
           System.out.println("DevelArray:"+DevelCareer);
           System.out.println("awardArray:"+award);
           System.out.println("schoolArray:"+school);
           System.out.println("volunteArray:"+volunte);
           System.out.println("portfolioArray:"+portfolio);
           System.out.println("licenseArray:"+license);
           System.out.println("file"+file);
        	}
        }*/
		
		 for(int i = 0; i < list.size(); i++){
		        System.out.println("list 순서 " + i + "번쨰");
		       // int j = 0;
		        for( Entry<String, Object> elem : list.get(i).entrySet() ){
		            System.out.println( String.format("키 : %s, 값 : %s", elem.getKey(), elem.getValue()));
		            for(int j=0;j<4;j++) {
		            	//불안하지만 일단 4로 설정
		            	if(elem.getKey().equals("company"+j)){
		            		companyCareer.add((hyj_Previous)elem.getValue());
		            	}else if(elem.getKey().equals("edu"+j)){
		            		eduCareer.add((hyj_education)elem.getValue());
		            	}else if(elem.getKey().equals("devel"+j)){
		            		System.out.println("devel에 값담기는지 확인");
		            		DevelCareer.add((hyj_developmentinsert)elem.getValue());
		            	}else if(elem.getKey().equals("award"+j)){
		            		award.add((hyj_awards)elem.getValue());
		            	}else if(elem.getKey().equals("school"+j)){
		            		school.add((School)elem.getValue());
		            	}else if(elem.getKey().equals("volun"+j)){
		            		 volunte.add((hyj_volunteer)elem.getValue());
		            	}else if(elem.getKey().equals("Portfolio"+j)){
		            		portfolio.add((Portfolio)elem.getValue());
		            	}else if(elem.getKey().equals("license"+j)){
		            		license.add((hyj_certificate)elem.getValue());
		            	}else if(elem.getKey().equals("file"+j)){
		            		file.add((hyj_Attachment)elem.getValue());
		            	}
		            	//j++;
		            }
		          // companyCareer=new ArrayList<hyj_Previous>();
		           //ArrayList<hyj_Previous> companyCareer1= companyCareer<Object>;
		        	}
		        System.out.println("CompanyArray:"+companyCareer);
		        System.out.println("eduArray:"+eduCareer);
		        System.out.println("DevelArray:"+DevelCareer);
		        System.out.println("awardArray:"+award);
		        System.out.println("schoolArray:"+school);
		        System.out.println("volunteArray:"+volunte);
		        System.out.println("portfolioArray:"+portfolio);
		        System.out.println("licenseArray:"+license);
		        System.out.println("file"+file);
		        }
		
    
    
  //이력서 번호 받아오기
    
    	String resumeNo="";
  		if (result>0) {
  			resumeNo=new ResumeDao().selectCurrval(con);
  		}else {
  			System.out.println("ResumeService: 이력서 입력 실패!");
  		}
    
    
    	//회사 경력 값 받기
    	int insertCom=0;//이력서별 경력테이블에 값들어갔는지 확인용
    	String comNoNew="";//새로 값넣은 회사경력 고유번호
    	for(hyj_Previous c :companyCareer) {
    		if(c.getPreviousNo() != null) {
    			if(c.getPreviousNo().equals("0")) {
    				//회사 경력 테이블에서 새롭게 값 넣어주기 resultCom 1이면 값 잘 넣은거
    				System.out.println("c.getPreviousNo 새로운값: "+c.getPreviousNo());
    				int resultCom=new ResumeDao().insertCompanyCareer(con,c,loginUser);
    				if(resultCom>0) {
    					comNoNew=new ResumeDao().insertCompanyCareerCurrval(con);
    					System.out.println("service comNoNew:"+comNoNew);
    				}
    			}else {
    				comNoNew=c.getPreviousNo();
    			}
    			insertCom=new ResumeDao().insertResumeCareerCompany(con,comNoNew,resumeNo);
    			System.out.println("service insertCom"+insertCom);
    		}
    	}
    
    	//교육 경력 받기
    	int insertEdu=0;
    	String eduNoNew="";
    	for(hyj_education e: eduCareer) {
    		if(e.getEducationNo() != null) {
    			if(e.getEducationNo().equals("0")) {
    				int resultEdu=new ResumeDao().insertEduCareer(con,e,loginUser);
    				if(resultEdu>0) {
    					eduNoNew=new ResumeDao().insertEduCareerCurrval(con);
    					System.out.println("service eduNoNew:"+eduNoNew);
    				}
    				
    			}else {
					eduNoNew=e.getEducationNo();
				}
				insertEdu=new ResumeDao().insertResumeCareerEdu(con,eduNoNew,resumeNo);
				System.out.println("service insertEdu"+insertEdu);
    		}
    	}
    
    
    
		//개발경력 받기
    	int insertDevel=0;
    	String develNoNew="";
    	for(hyj_developmentinsert d: DevelCareer) {
    		if(d.getDevelNo() != null) {
    			if(d.getDevelNo().equals("0")) {
    				int resultDevel=new ResumeDao().insertDevelCareer(con,d,loginUser);
    				if(resultDevel>0) {
    					develNoNew=new ResumeDao().insertDevelCareerCurrval(con);
    					System.out.println("serveice develNoNew"+develNoNew);
    				}
    			}else {
    				develNoNew=d.getDevelNo();
    			}
    			insertDevel=new ResumeDao().insertResumeCareerDevel(con,develNoNew,resumeNo);
    			System.out.println("service insertDevel"+insertDevel);
    		}
    	}

    	//수상경력
    	int insertAward=0;
    	String awardNoNew="";
    	for(hyj_awards a: award) {
    		if(a.getAwardNo() != null) {
    			if(a.getAwardNo().equals("0")) {
    				int resultAward=new ResumeDao().insertAwardCareer(con,a,loginUser);
    				if(resultAward>0) {
    					awardNoNew=new ResumeDao().insertAwardCareerCurrval(con);
    					System.out.println("serveice awardNoNew"+awardNoNew);
    				}
    			}else{
    				awardNoNew=a.getAwardNo();
    			}
    			insertAward=new ResumeDao().insertResumeCareerAward(con,awardNoNew,resumeNo);
    		}
    	}
		
		//학력
    	int insertSchool=0;
    	String schoolNoNew="";
    	for(School s: school) {
    		if(s.getCaNo() != null) {
    			if(s.getCaNo().equals("0")) {
    				int resultSchool=new ResumeDao().insertSchoolCareer(con,s,loginUser);
    				System.out.println("지금확인:"+resultSchool);
    				if(resultSchool>0) {
    					schoolNoNew=new ResumeDao().insertSchoolCareerCurrval(con);
    					System.out.println("serveice schoolNoNew"+schoolNoNew);
    				}else {
    					System.out.println("학력새로운값 넣기 실패");
    				}
    			}else {
    				schoolNoNew=s.getCaNo();
    			}
    			insertSchool=new ResumeDao().insertReseumeCareerAward(con,schoolNoNew,resumeNo);
    		}
    	}
		
    	//활동
    	int insertVol=0;
    	String volNoNew="";
    	for(hyj_volunteer v: volunte) {
    		if(v.getVolunteerNo() != null) {
    			if(v.getVolunteerNo().equals("0")) {
    				int resultVol=new ResumeDao().insertVolCareer(con,v,loginUser);
    				if(resultVol>0) {
    					volNoNew=new ResumeDao().insertVolCareerCurrval(con);
    					System.out.println("serveice volNoNew"+volNoNew);
    				}
    			}else {
    				volNoNew=v.getVolunteerNo();
    			}
    			insertVol=new ResumeDao().insertReseumeCareerVolun(con,volNoNew,resumeNo);
    		}
    	}
		
    	
    	//자격증
    	int insertLie=0;
    	String lieNoNew="";
    	for(hyj_certificate l: license) {
    		if(l.getCertificateNo() != null) {
    			if(l.getCertificateNo().equals("0")) {
    				int resultLi=new ResumeDao().insertLieCareer(con,l,loginUser);
    				if(resultLi>0) {
    					lieNoNew=new ResumeDao().insertLieVareerCurrval(con);
    					System.out.println("serveice lieNoNew"+lieNoNew);
    				}
    			}else {
    				lieNoNew=l.getCertificateNo();
    			}
    			insertLie=new ResumeDao().insertResumeCareerLie(con,lieNoNew,resumeNo);
    		}
    	}
    	
    	//첨부파일 
//    	int intsertFile=0;
//    	String fileNoNew="";
//    	for(hyj_Attachment f: file) {
//    		if(f.getAt_no() != null) {
//    			if(f.getAt_no().equals("0")) {
//    				int resultFile=new ResumeDao().insertFile(con,l,loginUser);
//    				//if(resultFile>0)
//    			}
//    		}
//    	}
    	
		
		if(result>0) {
			commit(con);
			
		}else {
			rollback(con);
		}
		close(con);

		return result;
	}

	public ArrayList<Resume> selectList(String perId) {
		// TODO Auto-generated method stub
		Connection con=getConnection();
		
		ArrayList<Resume> list=new ResumeDao().selectList(con,perId);
		System.out.println("service list:"+list);
		
		if(list!=null) {
			commit(con);
		}else {
			rollback(con);
		}
		close(con);
		return list;
	}

	//작성전에 값 받아오기
	public ArrayList<HashMap<String, Object>> beforeInsertResume(String perNo) {
		Connection con=getConnection();
		
		ArrayList<HashMap<String,Object>> resumeDetail=new ResumeDao().beforeInsertSelectResume(con,perNo);
		resumeDetail=new ResumeDao().beforeInsertSelectCompanyCareer(con,perNo,resumeDetail);//회사경력
		resumeDetail=new ResumeDao().beforeInsertSelectEduCarreer(con,perNo,resumeDetail);//교육경력
		resumeDetail=new ResumeDao().beforeInsertSelectDevelCarreer(con,perNo,resumeDetail);//개발경력
		resumeDetail=new ResumeDao().beforeInsertSelectAward(con,perNo,resumeDetail);//수상경력
		resumeDetail=new ResumeDao().beforeInsertSelectSchool(con,perNo,resumeDetail);//학력
		resumeDetail=new ResumeDao().beforeInsertSelectPortfolio(con,perNo,resumeDetail);//포트폴리오
		resumeDetail=new ResumeDao().beforeInsertSelectVolunte(con, perNo, resumeDetail);//대외활동
		resumeDetail=new ResumeDao().beforeInsertSelectLicense(con, perNo, resumeDetail);//자격증
		
		
		System.out.println("이력서 내용들:"+resumeDetail);
		
		if(resumeDetail!=null) {
			commit(con);
		}else {
			rollback(con);
		}
		close(con);
			
		return resumeDetail;
	}

	public int deleteResume(String resumeNo) {
		Connection con =getConnection();
		int result=new ResumeDao().deleteResume(con,resumeNo);
		if(result>0) {
			commit(con);
		}else {
			rollback(con);
		}
		close(con);
		return result;
	}

	public ArrayList<HashMap<String, Object>> DetailResume(String rno,String loginUser) {
		Connection con=getConnection();
		
		ArrayList<HashMap<String,Object>> resumeDetail=new ResumeDao().beforeInsertSelectResume(con,loginUser);
		resumeDetail=new ResumeDao().DetailSelectResume(con,rno,resumeDetail);//이력서
		resumeDetail=new ResumeDao().DetailSelectCompanyCareer(con,rno,resumeDetail);//회사경력
		resumeDetail=new ResumeDao().DetailSelectEduCarreer(con,rno,resumeDetail);//교육경력
		resumeDetail=new ResumeDao().DetailSelectDevelCarreer(con,rno,resumeDetail);//개발경력
		resumeDetail=new ResumeDao().DetailSelectAward(con,rno,resumeDetail);//수상경력
		resumeDetail=new ResumeDao().DetailSelectSchool(con,rno,resumeDetail);//학력
		resumeDetail=new ResumeDao().DetailSelectVolunte(con, rno, resumeDetail);//대외활동
		resumeDetail=new ResumeDao().DetailSelectLicense(con, rno, resumeDetail);//자격증
		resumeDetail=new ResumeDao().DetailSelectAttach(con, rno, resumeDetail);//첨부파일
		
		
		System.out.println("이력서 내용들 Service:"+resumeDetail);
		
		if(resumeDetail!=null) {
			commit(con);
		}else {
			rollback(con);
		}
		close(con);
			
		return resumeDetail;
	}

	public int insertResume(Resume resume, ArrayList<HashMap<String, Object>> list, Jyh_PerMember loginUser,
			Attachment at) {
		
Connection con=getConnection();
		
		//이력서 테이블에 값 넣기
		int result=new ResumeDao().insertResume(con,resume,loginUser);
    	System.out.println("resumeDetailArrayList jsp파일에서" + list);
		ArrayList<hyj_Previous> companyCareer=new ArrayList<hyj_Previous>();
		ArrayList<hyj_education> eduCareer=new ArrayList<hyj_education>();
		ArrayList<hyj_developmentinsert> DevelCareer=new ArrayList<hyj_developmentinsert>();
		ArrayList<hyj_awards> award=new ArrayList<hyj_awards>();
		ArrayList<School> school=new ArrayList<School>();
		ArrayList<hyj_volunteer> volunte=new ArrayList<hyj_volunteer>();
		ArrayList<Portfolio> portfolio=new ArrayList<Portfolio>();
		ArrayList<hyj_certificate> license=new ArrayList<hyj_certificate>();
		ArrayList<hyj_Attachment> file= new ArrayList<hyj_Attachment>();
		
		
   
		
		 for(int i = 0; i < list.size(); i++){
		        System.out.println("list 순서 " + i + "번쨰");
		       // int j = 0;
		        for( Entry<String, Object> elem : list.get(i).entrySet() ){
		            System.out.println( String.format("키 : %s, 값 : %s", elem.getKey(), elem.getValue()));
		            for(int j=0;j<4;j++) {
		            	//불안하지만 일단 4로 설정
		            	if(elem.getKey().equals("company"+j)){
		            		companyCareer.add((hyj_Previous)elem.getValue());
		            	}else if(elem.getKey().equals("edu"+j)){
		            		eduCareer.add((hyj_education)elem.getValue());
		            	}else if(elem.getKey().equals("devel"+j)){
		            		System.out.println("devel에 값담기는지 확인");
		            		DevelCareer.add((hyj_developmentinsert)elem.getValue());
		            	}else if(elem.getKey().equals("award"+j)){
		            		award.add((hyj_awards)elem.getValue());
		            	}else if(elem.getKey().equals("school"+j)){
		            		school.add((School)elem.getValue());
		            	}else if(elem.getKey().equals("volun"+j)){
		            		 volunte.add((hyj_volunteer)elem.getValue());
		            	}else if(elem.getKey().equals("Portfolio"+j)){
		            		portfolio.add((Portfolio)elem.getValue());
		            	}else if(elem.getKey().equals("license"+j)){
		            		license.add((hyj_certificate)elem.getValue());
		            	}else if(elem.getKey().equals("file"+j)){
		            		file.add((hyj_Attachment)elem.getValue());
		            	}
		            	//j++;
		            }
		          // companyCareer=new ArrayList<hyj_Previous>();
		           //ArrayList<hyj_Previous> companyCareer1= companyCareer<Object>;
		        	}
		        System.out.println("CompanyArray:"+companyCareer);
		        System.out.println("eduArray:"+eduCareer);
		        System.out.println("DevelArray:"+DevelCareer);
		        System.out.println("awardArray:"+award);
		        System.out.println("schoolArray:"+school);
		        System.out.println("volunteArray:"+volunte);
		        System.out.println("portfolioArray:"+portfolio);
		        System.out.println("licenseArray:"+license);
		        System.out.println("file"+file);
		        }
		
    
    
  //이력서 번호 받아오기
    
    	String resumeNo="";
  		if (result>0) {
  			resumeNo=new ResumeDao().selectCurrval(con);
  		}else {
  			System.out.println("ResumeService: 이력서 입력 실패!");
  		}
    
    
    	//회사 경력 값 받기
    	int insertCom=0;//이력서별 경력테이블에 값들어갔는지 확인용
    	String comNoNew="";//새로 값넣은 회사경력 고유번호
    	for(hyj_Previous c :companyCareer) {
    		if(c.getPreviousNo() != null) {
    			if(c.getPreviousNo().equals("0")) {
    				//회사 경력 테이블에서 새롭게 값 넣어주기 resultCom 1이면 값 잘 넣은거
    				System.out.println("c.getPreviousNo 새로운값: "+c.getPreviousNo());
    				int resultCom=new ResumeDao().insertCompanyCareer(con,c,loginUser);
    				if(resultCom>0) {
    					comNoNew=new ResumeDao().insertCompanyCareerCurrval(con);
    					System.out.println("service comNoNew:"+comNoNew);
    				}
    			}else {
    				comNoNew=c.getPreviousNo();
    			}
    			insertCom=new ResumeDao().insertResumeCareerCompany(con,comNoNew,resumeNo);
    			System.out.println("service insertCom"+insertCom);
    		}
    	}
    
    	//교육 경력 받기
    	int insertEdu=0;
    	String eduNoNew="";
    	for(hyj_education e: eduCareer) {
    		if(e.getEducationNo() != null) {
    			if(e.getEducationNo().equals("0")) {
    				int resultEdu=new ResumeDao().insertEduCareer(con,e,loginUser);
    				if(resultEdu>0) {
    					eduNoNew=new ResumeDao().insertEduCareerCurrval(con);
    					System.out.println("service eduNoNew:"+eduNoNew);
    				}
    				
    			}else {
					eduNoNew=e.getEducationNo();
				}
				insertEdu=new ResumeDao().insertResumeCareerEdu(con,eduNoNew,resumeNo);
				System.out.println("service insertEdu"+insertEdu);
    		}
    	}
    
    
    
		//개발경력 받기
    	int insertDevel=0;
    	String develNoNew="";
    	for(hyj_developmentinsert d: DevelCareer) {
    		if(d.getDevelNo() != null) {
    			if(d.getDevelNo().equals("0")) {
    				int resultDevel=new ResumeDao().insertDevelCareer(con,d,loginUser);
    				if(resultDevel>0) {
    					develNoNew=new ResumeDao().insertDevelCareerCurrval(con);
    					System.out.println("serveice develNoNew"+develNoNew);
    				}
    			}else {
    				develNoNew=d.getDevelNo();
    			}
    			insertDevel=new ResumeDao().insertResumeCareerDevel(con,develNoNew,resumeNo);
    			System.out.println("service insertDevel"+insertDevel);
    		}
    	}

    	//수상경력
    	int insertAward=0;
    	String awardNoNew="";
    	for(hyj_awards a: award) {
    		if(a.getAwardNo() != null) {
    			if(a.getAwardNo().equals("0")) {
    				int resultAward=new ResumeDao().insertAwardCareer(con,a,loginUser);
    				if(resultAward>0) {
    					awardNoNew=new ResumeDao().insertAwardCareerCurrval(con);
    					System.out.println("serveice awardNoNew"+awardNoNew);
    				}
    			}else{
    				awardNoNew=a.getAwardNo();
    			}
    			insertAward=new ResumeDao().insertResumeCareerAward(con,awardNoNew,resumeNo);
    		}
    	}
		
		//학력
    	int insertSchool=0;
    	String schoolNoNew="";
    	for(School s: school) {
    		if(s.getCaNo() != null) {
    			if(s.getCaNo().equals("0")) {
    				int resultSchool=new ResumeDao().insertSchoolCareer(con,s,loginUser);
    				System.out.println("지금확인:"+resultSchool);
    				if(resultSchool>0) {
    					schoolNoNew=new ResumeDao().insertSchoolCareerCurrval(con);
    					System.out.println("serveice schoolNoNew"+schoolNoNew);
    				}else {
    					System.out.println("학력새로운값 넣기 실패");
    				}
    			}else {
    				schoolNoNew=s.getCaNo();
    			}
    			insertSchool=new ResumeDao().insertReseumeCareerAward(con,schoolNoNew,resumeNo);
    		}
    	}
		
    	//활동
    	int insertVol=0;
    	String volNoNew="";
    	for(hyj_volunteer v: volunte) {
    		if(v.getVolunteerNo() != null) {
    			if(v.getVolunteerNo().equals("0")) {
    				int resultVol=new ResumeDao().insertVolCareer(con,v,loginUser);
    				if(resultVol>0) {
    					volNoNew=new ResumeDao().insertVolCareerCurrval(con);
    					System.out.println("serveice volNoNew"+volNoNew);
    				}
    			}else {
    				volNoNew=v.getVolunteerNo();
    			}
    			insertVol=new ResumeDao().insertReseumeCareerVolun(con,volNoNew,resumeNo);
    		}
    	}
		
    	
    	//자격증
    	int insertLie=0;
    	String lieNoNew="";
    	for(hyj_certificate l: license) {
    		if(l.getCertificateNo() != null) {
    			if(l.getCertificateNo().equals("0")) {
    				int resultLi=new ResumeDao().insertLieCareer(con,l,loginUser);
    				if(resultLi>0) {
    					lieNoNew=new ResumeDao().insertLieVareerCurrval(con);
    					System.out.println("serveice lieNoNew"+lieNoNew);
    				}
    			}else {
    				lieNoNew=l.getCertificateNo();
    			}
    			insertLie=new ResumeDao().insertResumeCareerLie(con,lieNoNew,resumeNo);
    		}
    	}
    	int result1=0;
    	int insertFile=0;
    	String fileNoNew="";
    	if(resumeNo != null && result > 0) {
    		at.setAno(resumeNo);
    		result1 = new ResumeDao().insertAttachment(con,at);
    		if(result1>0) {
    			fileNoNew=new ResumeDao().insertFileCurrval(con);
				System.out.println("serveice fileNoNew"+fileNoNew);
			}
    		insertFile=new ResumeDao().insertResumeCareerFile(con,fileNoNew,resumeNo);
    	}
    	//첨부파일 
//    	int intsertFile=0;
//    	
//    		if(at.getAno() != null) {
//    				int resultFile=new ResumeDao().insertFile(con,at,loginUser);
//    				//if(resultFile>0)
//    			
//    		}
    	
    	
		int result2=0;
		if(result>0 && result1>0) {
			commit(con);
			result2 = 1;
		}else {
			rollback(con);
		}
		close(con);

		return result2;
	}

	public ArrayList<Resume> applyResumeSelect(String aNo) {
		// TODO Auto-generated method stub
		Connection con=getConnection();
		ArrayList<Resume> list=new ResumeDao().selectListPerANo(con,aNo);
		if(list != null) {
			commit(con);
			
		}else {
			rollback(con);
		}
		close(con);

		return list;
	}


}
