package com.kh.itda.addInfo.resume.model.dao;

import static com.kh.itda.common.JDBCTemplate.close;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;

import com.kh.itda.addInfo.portfolio.model.vo.Portfolio;
import com.kh.itda.addInfo.resume.model.vo.Resume;
import com.kh.itda.addInfo.school.model.vo.School;
import com.kh.itda.member.model.vo.Jyh_PerMember;
import com.kh.itda.member.model.vo.hyj_Attachment;
import com.kh.itda.member.model.vo.hyj_Previous;
import com.kh.itda.member.model.vo.hyj_addFile;
import com.kh.itda.member.model.vo.hyj_awards;
import com.kh.itda.member.model.vo.hyj_certificate;
import com.kh.itda.member.model.vo.hyj_developmentinsert;
import com.kh.itda.member.model.vo.hyj_education;
import com.kh.itda.member.model.vo.hyj_volunteer;
import com.kh.itda.recruitAndContest.model.vo.Attachment;

public class ResumeDao {
	private Properties prop=new Properties();
	
	public ResumeDao() {
		String fileName=ResumeDao.class.getResource("/sql/addInfo/resume/cho-resume-query.properties").getPath();
		try {
			prop.load(new FileReader(fileName));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public int insertResume(Connection con, Resume resume, Jyh_PerMember loginUser) {
		PreparedStatement pstmt=null;
		int result=0;
		String query=prop.getProperty("insertResume");
		
		try {
			pstmt=con.prepareStatement(query);
				
			
			pstmt.setString(1,loginUser.getPerNo());
			pstmt.setString(2,resume.getrHopeDuty());
			pstmt.setString(3, resume.getrHopeYearlySalary());
			pstmt.setString(4, resume.getrPublic());
			pstmt.setString(5, resume.getrTitle());
			pstmt.setString(6, resume.getrContent());
			pstmt.setString(7, resume.getrSkill());
	
			result=pstmt.executeUpdate();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
		}
		
		return result;
	}
	
	
	//내가쓴 이력서 조회
	public ArrayList<Resume> selectList(Connection con, String perId) {
		
		PreparedStatement pstmt=null;
		ResultSet rset=null;
		ArrayList<Resume> list=null;
		
		String query=prop.getProperty("selectResume");
		
		try {
			pstmt=con.prepareStatement(query);
			pstmt.setString(1, perId);
			
			rset=pstmt.executeQuery();
			
			list=new ArrayList<Resume>();
			while(rset.next()) {
				Resume r=new Resume();
				r.setrNo(rset.getString("R_NO"));
				r.setPerNo(rset.getString("PER_NO"));
				r.setrHopeDuty(rset.getString("R_HOPE_DUTY"));
				r.setrHopeYearlySalary(rset.getString("R_HOPE_YEARLY_SALAYR"));
				r.setrPublic(rset.getString("R_PUBLIC"));
				r.setrWriteDate(rset.getDate("R_WRITE_DATE"));
				r.setrUpdateDate(rset.getDate("R_UPDATE_DATE"));
				r.setrTitle(rset.getString("R_TITLE"));
				r.setrContent(rset.getString("R_CONTENT"));
				r.setrSkill(rset.getString("R_SKILL"));
				r.setrDelete(rset.getString("R_DELETE"));
				
				list.add(r);
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
			close(rset);
		}
		System.out.println("dao list:"+list);
		return list;
	}
	
	
	//이력서에 학력 집어 넣기
	public int insertResume(Connection con, School sc, Jyh_PerMember loginUser) {
		// TODO 학력 값 테이블에 입력
		PreparedStatement pstmt=null;
		int result=0;
		String query=prop.getProperty("insertSchool");
		
		try {
			
			
			
			pstmt=con.prepareStatement(query);
			pstmt.setString(1, sc.getCaName());
			pstmt.setDate(2, sc.getCaEnrollDate());
			pstmt.setDate(3, sc.getCaGraduDate());
			pstmt.setString(4, sc.getCaMajor());
			pstmt.setString(5, sc.getCaGrades());
			pstmt.setString(6, sc.getCaTopGrades());
			pstmt.setString(7, loginUser.getPerNo());
			pstmt.setString(8, sc.getEtc());
			pstmt.setString(9, sc.getCaDegree());
			
			result=pstmt.executeUpdate();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
		}
		
		
		System.out.println("Dao 학력insert:"+result);
		return result;
	}
	
	//이력서 고유번호 받아오기
	public String selectCurrval(Connection con) {
		Statement stmt=null;
		ResultSet rset=null;
		String rnum="";
		String query=prop.getProperty("selectCurrval");
		
		
		
		try {
			stmt=con.createStatement();
			rset=stmt.executeQuery(query);
			if(rset.next()) {
				rnum=rset.getString("currval");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(rset);
			close(stmt);
		}
		
		
		String rnum1="E"+rnum;
		return rnum1;
	}
	
	//이력서 값 받
	public ArrayList<HashMap<String, Object>> beforeInsertSelectResume(Connection con, String perNo) {
	     PreparedStatement pstmt = null;
         ResultSet rset = null;
         Jyh_PerMember member = null;
         ArrayList <HashMap<String,Object>> resumeDetail=null; 
         HashMap <String,Object> resumeDetail1= null;
         
         String query = prop.getProperty("selectMember");
         
         
         try {
            pstmt = con.prepareStatement(query);
            pstmt.setString(1, perNo);
            rset = pstmt.executeQuery();
            resumeDetail1=new HashMap<String,Object>();
            resumeDetail=new ArrayList <HashMap<String,Object>>();
            if(rset.next()) {
            	member=new Jyh_PerMember();
            member.setPerNo(rset.getString("PER_NO"));
            member.setPerId(rset.getString("PER_ID"));
            member.setPerPwd(rset.getString("PER_PWD"));
            member.setPerName(rset.getString("PER_NAME"));
            member.setPhone(rset.getString("PHONE"));
            member.setEmail(rset.getString("EMAIL"));
            member.setEnrollDate(rset.getDate("ENROLL_DATE"));
            member.setModifyDate(rset.getDate("MODIFY_DATE"));
            member.setInfoPeriod(rset.getString("INFO_PERIOD"));
            member.setGender(rset.getString("GENDER"));
            member.setBirthDay(rset.getDate("BIRTHDAY"));
            
            resumeDetail1.put("member", member);
            }
            resumeDetail.add(resumeDetail1);
            System.out.println("이력서 작성전에 멤버 값 받아오기DAO:"+resumeDetail);
            
         } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
         }finally {
            close(pstmt);
            close(rset);
         }
         return resumeDetail;
         
         
      }
	public ArrayList<HashMap<String, Object>> beforeInsertSelectCompanyCareer(Connection con, String perNo,
			ArrayList<HashMap<String,Object>> resumeDetail) {
		 PreparedStatement pstmt = null;
         ResultSet rset = null;
         //어레이리스트에 각각의 커리어를 저장하고 그 리스트를 hashMap담아줌
         HashMap <String,Object> resumeDetail1= null;
         hyj_Previous companyCareer=null;//회사경력
         String query_compayCareer=prop.getProperty("selectCompanyCareer");//?(회원번호)
         
         try {
			pstmt=con.prepareStatement(query_compayCareer);
			pstmt.setString(1, perNo);
			resumeDetail1=new HashMap<String,Object>();
			rset=pstmt.executeQuery();
			int i=0;
			
			while(rset.next()) {
				companyCareer=new hyj_Previous();
				
				companyCareer.setPreviousNo(rset.getString("CE_NO"));
				companyCareer.setPreviousjobname(rset.getString("CE_OCCUPATION"));
				companyCareer.setPreviousname(rset.getString("CE_PERVIOUSCOMPANY"));
				companyCareer.setPreviousjoindate(rset.getDate("CE_JOINDATE"));
				companyCareer.setResignationdate(rset.getDate("CE_LASTDATE"));
				companyCareer.setStack(rset.getString("CE_STACK"));
				companyCareer.setMoney(rset.getString("CE_MONEY"));
				
				resumeDetail1.put("companyCareer"+i, companyCareer);
				i++;
				
			}
			resumeDetail.add(resumeDetail1);
			System.out.println("dao에서 compnay 경력 받아와지나 확인"+resumeDetail1);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
			close(rset);
		}
         

		return resumeDetail;
	}
	public ArrayList<HashMap<String, Object>> beforeInsertSelectEduCarreer(Connection con, String perNo,
			ArrayList<HashMap<String,Object>> resumeDetail) {
		 PreparedStatement pstmt = null;
         ResultSet rset = null;
         hyj_education eduCareer=null;//교육경력
         String query_eduCareer=prop.getProperty("selectEduCareer");//?(PERNO)
         HashMap <String,Object> resumeDetail1= null;
         
         try {
			pstmt=con.prepareStatement(query_eduCareer);
			pstmt.setString(1, perNo);
			resumeDetail1=new HashMap<String,Object>();
			rset=pstmt.executeQuery();
			int i=0;
			
			while(rset.next()) {
				eduCareer=new hyj_education();
				
				eduCareer.setEducationNo(rset.getString("CA_NO"));
				eduCareer.setEducationname(rset.getString("CA_NAME"));
				eduCareer.setEducationstartday(rset.getDate("CA_STARTDATE"));
				eduCareer.setEducationlastday(rset.getDate("CA_ENDDATE"));
				eduCareer.setEducationstack(rset.getString("CA_STACK"));
				eduCareer.setEducationskil(rset.getString("CA_SKILL"));
				resumeDetail1.put("eduCareer"+i, eduCareer);
				i++;
			}
			resumeDetail.add(resumeDetail1);
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}finally {
				close(pstmt);
				close(rset);
			}
	         

			return resumeDetail;
	}
	public ArrayList<HashMap<String, Object>> beforeInsertSelectDevelCarreer(Connection con, String perNo,
			ArrayList<HashMap<String,Object>> resumeDetail) {
		// TODO Auto-generated method stub
		 PreparedStatement pstmt = null;
         ResultSet rset = null;
		 hyj_developmentinsert develCareer=null; //개발경력
		 HashMap <String,Object> resumeDetail1= null;
		 String query_develCareer=prop.getProperty("selectDevelCareer");//?(PERNO)

		 try {
			pstmt=con.prepareStatement(query_develCareer);
			pstmt.setString(1, perNo);
			resumeDetail1=new HashMap<String,Object>();
			rset=pstmt.executeQuery();
			int i=0;
			
			while(rset.next()) {
				develCareer=new hyj_developmentinsert();
				
				develCareer.setDevelNo(rset.getString("DP_NO"));
				develCareer.setJoindate(rset.getDate("DP_STARTDAY"));
				develCareer.setResignationdate(rset.getDate("DP_LASTDAY"));
				develCareer.setDuserstack(rset.getString("DP_CHARGE"));
				develCareer.setDuserskill(rset.getString("DP_SKILL"));
				develCareer.setUsergit(rset.getString("DP_GITURL"));
				
				resumeDetail1.put("DevelCareer"+i, develCareer);
				i++;
			}
			resumeDetail.add(resumeDetail1);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
			close(rset);
		}
         

		return resumeDetail;
	}
	public ArrayList<HashMap<String, Object>> beforeInsertSelectAward(Connection con, String perNo,
			ArrayList<HashMap<String,Object>> resumeDetail) {
		 PreparedStatement pstmt = null;
         ResultSet rset = null;
         hyj_awards award=null;//수상경력


         HashMap <String,Object> resumeDetail1= null;
		 String query_award=prop.getProperty("selectAward");//?(PERNO)

		 try {
			pstmt=con.prepareStatement(query_award);
			
			pstmt.setString(1, perNo);
			resumeDetail1=new HashMap<String,Object>();
			rset=pstmt.executeQuery();
			int i=0;
			
			while(rset.next()) {
				award=new hyj_awards();
				
				award.setAwardNo(rset.getNString("A_NO"));
				award.setAwardsname(rset.getString("A_NAME"));
				award.setAwardscontent(rset.getString("A_CONTENT"));
				award.setAwardsmonth(rset.getDate("A_DATE"));
				award.setAwardsagency(rset.getString("A_AGENCY"));
				
				resumeDetail1.put("award"+i, award);
				i++;
			}
			resumeDetail.add(resumeDetail1);
		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
			close(rset);
		}
         

		return resumeDetail;
	}
	public ArrayList<HashMap<String, Object>> beforeInsertSelectSchool(Connection con, String perNo,
			ArrayList<HashMap<String,Object>> resumeDetail) {
		 PreparedStatement pstmt = null;
         ResultSet rset = null;
         School school=null;//학력

         HashMap <String,Object> resumeDetail1= null;
		 String query_schoolCareer=prop.getProperty("selectSchoolCareer");//?
		 
		 try {
			pstmt=con.prepareStatement(query_schoolCareer);
			pstmt.setString(1, perNo);
			resumeDetail1=new HashMap<String,Object>();
			rset=pstmt.executeQuery();
			int i=0;
			
			while(rset.next()) {
				school=new School();
				
				school.setCaNo(rset.getString("SC_NO"));
				school.setCaName(rset.getString("SC_NAME"));
				school.setCaEnrollDate(rset.getDate("SC_ENROLL_DATE"));
				school.setCaGraduDate(rset.getDate("SC_GRADUATE_DATE"));
				school.setCaMajor(rset.getString("SC_MAJOR"));
				school.setCaDegree(rset.getString("SC_ETC"));//학위
				school.setCaGrades(rset.getString("SC_MYGRADEPOINT"));
				school.setCaTopGrades(rset.getString("SC_GRADEPOINT"));
				school.setEtc(rset.getString("SC_EXPLANATION"));
				
				resumeDetail1.put("school"+i, school);
				System.out.println("school dao:"+school);
				i++;
			}
			resumeDetail.add(resumeDetail1);
			System.out.println("school resumeDetail:"+resumeDetail1);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
			close(rset);
		}
         

		return resumeDetail;
	}
	
	
	public ArrayList<HashMap<String, Object>> beforeInsertSelectPortfolio(Connection con, String perNo,
			ArrayList<HashMap<String,Object>> resumeDetail) {
		 PreparedStatement pstmt = null;
         ResultSet rset = null;
         Portfolio portfolio=null;//포트폴리오
         
         HashMap <String,Object> resumeDetail1= null;
         String query_portfolio=prop.getProperty("selectPortFolio");//?(PERNO)
         
         try {
			pstmt=con.prepareStatement(query_portfolio);
			pstmt.setString(1, perNo);
			resumeDetail1=new HashMap<String,Object>();
			rset=pstmt.executeQuery();
			
			if(rset.next()) {
				portfolio=new Portfolio();
				portfolio.setPerNo(rset.getString("PER_NO"));
				portfolio.setTitle(rset.getString("P_TITLE"));
				portfolio.setContent(rset.getString("P_CONTENT"));
				System.out.println("dao 포트폴리오 객체:"+portfolio);
				resumeDetail1.put("Portfolio0", portfolio);
			}
			
			resumeDetail.add(resumeDetail1);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
			close(rset);
		}
         

		return resumeDetail;
	}
	public ArrayList<HashMap<String, Object>> beforeInsertSelectVolunte(Connection con, String perNo,
			ArrayList<HashMap<String,Object>> resumeDetail) {
		 PreparedStatement pstmt = null;
         ResultSet rset = null;
         hyj_volunteer volunte=null;//대외활동
         HashMap <String,Object> resumeDetail1= null;
         String query_volunte=prop.getProperty("selectVolunte");//?(PERNO)
         
         try {
			pstmt=con.prepareStatement(query_volunte);
			pstmt.setString(1, perNo);
			rset=pstmt.executeQuery();
			resumeDetail1=new HashMap<String,Object>();
			int i=0;
			
			while(rset.next()) {
				volunte=new hyj_volunteer();
				volunte.setVolunteerNo(rset.getString("SA_NO"));
				volunte.setVolunteername(rset.getString("SA_NAME"));
				volunte.setVolunteerStartDate(rset.getDate("SA_STARTDATE"));
				volunte.setVolunteerendDate(rset.getDate("SA_ENDDATE"));
				volunte.setVolunteerlink(rset.getString("SA_LINK"));
				volunte.setVolunteerExplanation(rset.getString("SA_EXPLAIN"));
				resumeDetail1.put("volunte"+i, volunte);
				i++;
			}
			resumeDetail.add(resumeDetail1);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
			close(rset);
		}
         

		return resumeDetail;
	}
	public ArrayList<HashMap<String, Object>> beforeInsertSelectLicense(Connection con, String perNo,
			ArrayList<HashMap<String, Object>> resumeDetail) {
		PreparedStatement pstmt = null;
        ResultSet rset = null;
        hyj_certificate license=null;//자격증
        HashMap <String,Object> resumeDetail1= null;
        String query_volunte=prop.getProperty("selectLicense");//?(PERNO)
        
        try {
			pstmt=con.prepareStatement(query_volunte);
			pstmt.setString(1, perNo);
			rset=pstmt.executeQuery();
			resumeDetail1=new HashMap<String,Object>();
			int i=0;
			 
			while(rset.next()) {
				license=new hyj_certificate();
				license.setCertificateNo(rset.getString("LI_NO"));
				license.setCertificatename(rset.getString("LI_NAME"));
				license.setCertificateagency(rset.getString("LI_ORGANIZATION"));
				license.setCertificatestart(rset.getDate("LI_GETDATE"));
				license.setCertificateend(rset.getDate("LI_EXPIREDATE"));
				resumeDetail1.put("license"+i, license);
				
				i++;
			}
			resumeDetail.add(resumeDetail1);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
			close(rset);
		}
        

		return resumeDetail;
	}
	public int deleteResume(Connection con, String resumeNo) {
		PreparedStatement pstmt=null;
		int result=0;
		String query=prop.getProperty("deleteResume");
		try {
			pstmt=con.prepareStatement(query);
			pstmt.setString(1, resumeNo);
			result=pstmt.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
		}
		System.out.println("deleteDao result:"+result);
		return result;
	}
	
	
	
	
	
	//////////////////////////////회사 경력
	public ArrayList<HashMap<String, Object>> DetailSelectCompanyCareer(Connection con, String rno,
			ArrayList<HashMap<String, Object>> resumeDetail) {
		PreparedStatement pstmt=null;
		ResultSet rset=null;
		hyj_Previous companyCareer=null;
		String query=prop.getProperty("detailSelectCompanyCareer");
		HashMap<String,Object> resumeDetail1=null;
		try {
			pstmt=con.prepareStatement(query);
			pstmt.setString(1, rno);
			rset=pstmt.executeQuery();
			resumeDetail1=new HashMap<String,Object>();
			int i=0;
			while(rset.next()) {
				System.out.println("test1 if문");
				companyCareer=new hyj_Previous();
				companyCareer.setPreviousNo(rset.getString("CE_NO"));
				companyCareer.setPreviousjobname(rset.getString("CE_OCCUPATION"));
				companyCareer.setPreviousname(rset.getString("CE_PERVIOUSCOMPANY"));
				companyCareer.setPreviousjoindate(rset.getDate("CE_JOINDATE"));
				companyCareer.setResignationdate(rset.getDate("CE_LASTDATE"));
				companyCareer.setStack(rset.getString("CE_STACK"));
				companyCareer.setMoney(rset.getString("CE_MONEY"));
				System.out.println("companDao:"+companyCareer);
				resumeDetail1.put("companyCareer"+i, companyCareer);
				i++;
				
			}
			System.out.println("Detail_dao에서 compnay 경력 받아와지나 확인"+resumeDetail1);
			//if(resumeDetail1!=null) {
				resumeDetail.add(resumeDetail1);								
			//}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
			close(rset);
		}
         

		return resumeDetail;
	}

		
	
		
	
	public ArrayList<HashMap<String, Object>> DetailSelectEduCarreer(Connection con, String rno,
			ArrayList<HashMap<String, Object>> resumeDetail) {
		 PreparedStatement pstmt = null;
         ResultSet rset = null;
         hyj_education eduCareer=null;//교육경력
         String query_eduCareer1=prop.getProperty("detailSelectEduCareer");//?(PERNO)
         HashMap <String,Object> resumeDetail1= null;
         
         try {
			pstmt=con.prepareStatement(query_eduCareer1);
			pstmt.setString(1, rno);
			resumeDetail1=new HashMap<String,Object>();
			rset=pstmt.executeQuery();
			int i=0;
			
			while(rset.next()) {
				eduCareer=new hyj_education();
				
				eduCareer.setEducationNo(rset.getString("CA_NO"));
				eduCareer.setEducationname(rset.getString("CA_NAME"));
				eduCareer.setEducationstartday(rset.getDate("CA_STARTDATE"));
				eduCareer.setEducationlastday(rset.getDate("CA_ENDDATE"));
				eduCareer.setEducationstack(rset.getString("CA_STACK"));
				eduCareer.setEducationskil(rset.getString("CA_SKILL"));
				resumeDetail1.put("eduCareer"+i, eduCareer);
				System.out.println("eduCareer:"+eduCareer);
				i++;
			}
			resumeDetail.add(resumeDetail1);
			System.out.println("Detail_dao에서 edu 경력 받아와지나 확인"+resumeDetail1);
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}finally {
				close(pstmt);
				close(rset);
			}
	         

			return resumeDetail;
	}
	
	
	//////////수상
	public ArrayList<HashMap<String, Object>> DetailSelectAward(Connection con, String rno,
			ArrayList<HashMap<String, Object>> resumeDetail) {
		PreparedStatement pstmt = null;
        ResultSet rset = null;
        hyj_awards award=null;//수상경력


        HashMap <String,Object> resumeDetail1= null;
		 String query_award=prop.getProperty("detailSelectAward");//?(PERNO)

		 try {
			pstmt=con.prepareStatement(query_award);
			
			pstmt.setString(1, rno);
			resumeDetail1=new HashMap<String,Object>();
			rset=pstmt.executeQuery();
			int i=0;
			
			while(rset.next()) {
				award=new hyj_awards();
				
				award.setAwardNo(rset.getNString("A_NO"));
				award.setAwardsname(rset.getString("A_NAME"));
				award.setAwardscontent(rset.getString("A_CONTENT"));
				award.setAwardsmonth(rset.getDate("A_DATE"));
				award.setAwardsagency(rset.getString("A_AGENCY"));
				
				resumeDetail1.put("award"+i, award);
				i++;
			}
			resumeDetail.add(resumeDetail1);
			System.out.println("Detail_dao에서 award 경력 받아와지나 확인"+resumeDetail1);
		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
			close(rset);
		}
        

		return resumeDetail;
	
	}
	public ArrayList<HashMap<String, Object>> DetailSelectDevelCarreer(Connection con, String rno,
			ArrayList<HashMap<String, Object>> resumeDetail) {
		 PreparedStatement pstmt = null;
         ResultSet rset = null;
		 hyj_developmentinsert develCareer=null; //개발경력
		 HashMap <String,Object> resumeDetail1= null;
		 String query_develCareer=prop.getProperty("detailSelectDevelCareer");//?(PERNO)

		 try {
			pstmt=con.prepareStatement(query_develCareer);
			pstmt.setString(1, rno);
			resumeDetail1=new HashMap<String,Object>();
			rset=pstmt.executeQuery();
			int i=0;
			
			while(rset.next()) {
				develCareer=new hyj_developmentinsert();
				
				develCareer.setDevelNo(rset.getString("DP_NO"));
				develCareer.setJoindate(rset.getDate("DP_STARTDAY"));
				develCareer.setResignationdate(rset.getDate("DP_LASTDAY"));
				develCareer.setDuserstack(rset.getString("DP_CHARGE"));
				develCareer.setDuserskill(rset.getString("DP_SKILL"));
				develCareer.setUsergit(rset.getString("DP_GITURL"));
				
				resumeDetail1.put("DevelCareer"+i, develCareer);
				i++;
			}
			resumeDetail.add(resumeDetail1);
			System.out.println("Detail_dao에서 devel 경력 받아와지나 확인"+resumeDetail1);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
			close(rset);
		}
         

		return resumeDetail;
	}
	public ArrayList<HashMap<String, Object>> DetailSelectSchool(Connection con, String rno,
			ArrayList<HashMap<String, Object>> resumeDetail) {
		 PreparedStatement pstmt = null;
         ResultSet rset = null;
         School school=null;//학력

         HashMap <String,Object> resumeDetail1= null;
		 String query_schoolCareer=prop.getProperty("detailSelectSchoolCareer");//?
		 
		 try {
			pstmt=con.prepareStatement(query_schoolCareer);
			pstmt.setString(1, rno);
			resumeDetail1=new HashMap<String,Object>();
			rset=pstmt.executeQuery();
			int i=0;
			
			while(rset.next()) {
				school=new School();
				
				school.setCaNo(rset.getString("SC_NO"));
				school.setCaName(rset.getString("SC_NAME"));
				school.setCaEnrollDate(rset.getDate("SC_ENROLL_DATE"));
				school.setCaGraduDate(rset.getDate("SC_GRADUATE_DATE"));
				school.setCaMajor(rset.getString("SC_MAJOR"));
				school.setCaDegree(rset.getString("SC_ETC"));//학위
				school.setCaGrades(rset.getString("SC_MYGRADEPOINT"));
				school.setCaTopGrades(rset.getString("SC_GRADEPOINT"));
				school.setEtc(rset.getString("SC_EXPLANATION"));
				
				resumeDetail1.put("school"+i, school);
				System.out.println("school dao:"+school);
				i++;
			}
			resumeDetail.add(resumeDetail1);
			System.out.println("detail 페이지에서 school resumeDetail:"+resumeDetail1);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
			close(rset);
		}
         

		return resumeDetail;
	}
	
	public ArrayList<HashMap<String, Object>> DetailSelectVolunte(Connection con, String rno,
			ArrayList<HashMap<String, Object>> resumeDetail) {
		 PreparedStatement pstmt = null;
         ResultSet rset = null;
         hyj_volunteer volunte=null;//대외활동
         HashMap <String,Object> resumeDetail1= null;
         String query_volunte=prop.getProperty("detailSelectVolunte");//?(PERNO)
         
         try {
			pstmt=con.prepareStatement(query_volunte);
			pstmt.setString(1, rno);
			rset=pstmt.executeQuery();
			resumeDetail1=new HashMap<String,Object>();
			int i=0;
			
			while(rset.next()) {
				volunte=new hyj_volunteer();
				volunte.setVolunteerNo(rset.getString("SA_NO"));
				volunte.setVolunteername(rset.getString("SA_NAME"));
				volunte.setVolunteerStartDate(rset.getDate("SA_STARTDATE"));
				volunte.setVolunteerendDate(rset.getDate("SA_ENDDATE"));
				volunte.setVolunteerlink(rset.getString("SA_LINK"));
				volunte.setVolunteerExplanation(rset.getString("SA_EXPLAIN"));
				resumeDetail1.put("volunte"+i, volunte);
				i++;
			}
			resumeDetail.add(resumeDetail1);
			System.out.println("detail 페이지에서 vol resumeDetail:"+resumeDetail1);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
			close(rset);
		}
         

		return resumeDetail;
	}
	public ArrayList<HashMap<String, Object>> DetailSelectLicense(Connection con, String rno,
			ArrayList<HashMap<String, Object>> resumeDetail) {
		PreparedStatement pstmt = null;
        ResultSet rset = null;
        hyj_certificate license=null;//자격증
        HashMap <String,Object> resumeDetail1= null;
        String query_volunte=prop.getProperty("detailSelectLicense");//?(PERNO)
        
        try {
			pstmt=con.prepareStatement(query_volunte);
			pstmt.setString(1, rno);
			rset=pstmt.executeQuery();
			resumeDetail1=new HashMap<String,Object>();
			int i=0;
			 
			while(rset.next()) {
				license=new hyj_certificate();
				license.setCertificateNo(rset.getString("LI_NO"));
				license.setCertificatename(rset.getString("LI_NAME"));
				license.setCertificateagency(rset.getString("LI_ORGANIZATION"));
				license.setCertificatestart(rset.getDate("LI_GETDATE"));
				license.setCertificateend(rset.getDate("LI_EXPIREDATE"));
				resumeDetail1.put("license"+i, license);
				
				i++;
			}
			resumeDetail.add(resumeDetail1);
			System.out.println("detail 페이지에서 license resumeDetail:"+resumeDetail1);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
			close(rset);
		}
        

		return resumeDetail;
	}
	public ArrayList<HashMap<String, Object>> DetailSelectResume(Connection con, String rno,
			ArrayList<HashMap<String, Object>> resumeDetail) {
		PreparedStatement pstmt=null;
		ResultSet rset=null;
		HashMap <String,Object> resumeDetail1= null;
		
		String query=prop.getProperty("DetailselectResume");
		
		try {
			pstmt=con.prepareStatement(query);
			pstmt.setString(1, rno);
			
			rset=pstmt.executeQuery();
			
			resumeDetail1=new HashMap<String,Object>();
			if(rset.next()) {
				Resume r=new Resume();
				r.setrNo(rset.getString("R_NO"));
				r.setPerNo(rset.getString("PER_NO"));
				r.setrHopeDuty(rset.getString("R_HOPE_DUTY"));
				r.setrHopeYearlySalary(rset.getString("R_HOPE_YEARLY_SALAYR"));
				r.setrPublic(rset.getString("R_PUBLIC"));
				r.setrWriteDate(rset.getDate("R_WRITE_DATE"));
				r.setrUpdateDate(rset.getDate("R_UPDATE_DATE"));
				r.setrTitle(rset.getString("R_TITLE"));
				r.setrContent(rset.getString("R_CONTENT"));
				r.setrSkill(rset.getString("R_SKILL"));
				r.setrDelete(rset.getString("R_DELETE"));
				
				resumeDetail1.put("resume", r);
				
			}
			resumeDetail.add(resumeDetail1);
			System.out.println("detail 페이지에서 resume resumeDetail:"+resumeDetail1);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
			close(rset);
		}
		return resumeDetail;
	}
	public ArrayList<HashMap<String, Object>> DetailSelectAttach(Connection con, String rno,
			ArrayList<HashMap<String, Object>> resumeDetail) {
		PreparedStatement pstmt=null;
		ResultSet rset=null;
		HashMap <String,Object> resumeDetail1= null;
		 hyj_Attachment f=null;
		String query=prop.getProperty("DetailselectAtt");
		
		try {
			pstmt=con.prepareStatement(query);
			pstmt.setString(1, rno);
			
			rset=pstmt.executeQuery();
			int i=0;
			resumeDetail1=new HashMap<String,Object>();
			while(rset.next()) {
				 f=new  hyj_Attachment();
				 f.setAt_no(rset.getString("ATT_NO"));
				f.setOriginName(rset.getString("ORIGIN_NAME"));
				f.setChangeName(rset.getString("CHANGE_NAME"));
				f.setFilePath(rset.getString("FILE_PATH"));
				f.setUploadDate(rset.getDate("UPLOAD_DATE"));
				f.setPerno(rset.getString("TB_NO"));
				f.setStatus(rset.getString("STATUS"));
				
				resumeDetail1.put("file"+i, f);
				i++;
			}
			resumeDetail.add(resumeDetail1);
			System.out.println("detail 페이지에서 file resumeDetail:"+resumeDetail1);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
			close(rset);
		}
		return resumeDetail;
	}
	public int insertCompanyCareer(Connection con,hyj_Previous c,Jyh_PerMember loginUser) {
		PreparedStatement pstmt=null;
		int result=0;
		String query=prop.getProperty("insertNewCampany");
		
		try {
			pstmt=con.prepareStatement(query);
				
			
			pstmt.setString(1,loginUser.getPerNo());
			pstmt.setString(2,c.getPreviousname());
			pstmt.setString(3, c.getPreviousjobname());
			pstmt.setDate(4, c.getPreviousjoindate());
			pstmt.setDate(5, c.getResignationdate());
			pstmt.setString(6, c.getStack());
			pstmt.setString(7, c.getMoney());
	
			result=pstmt.executeUpdate();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
		}
		
		return result;
	}
	public String insertCompanyCareerCurrval(Connection con) {
		Statement stmt=null;
		ResultSet rset=null;
		String rnum="";
		String query=prop.getProperty("CompanCareerCurrval");
		
		
		
		try {
			stmt=con.createStatement();
			rset=stmt.executeQuery(query);
			if(rset.next()) {
				rnum=rset.getString("currval");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(rset);
			close(stmt);
		}
		
		
		String rnum1="K"+rnum;
		return rnum1;
	}
	public int insertResumeCareerCompany(Connection con, String comNoNew, String resumeNo) {
		PreparedStatement pstmt=null;
		int result=0;
		String query=prop.getProperty("insertResumeCareerCampany");
		
		try {
			pstmt=con.prepareStatement(query);
				
			
			pstmt.setString(1,resumeNo);
			pstmt.setString(2,"COMPANYEXPERIENCE");
			pstmt.setString(3,comNoNew);
			
	
			result=pstmt.executeUpdate();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
		}
		
		return result;
	}
	public int insertEduCareer(Connection con, hyj_education ee, Jyh_PerMember loginUser) {
		PreparedStatement pstmt=null;
		int result=0;
		String query=prop.getProperty("insertNewEdu");
		
		try {
			pstmt=con.prepareStatement(query);
				
			
			pstmt.setString(1,loginUser.getPerNo());
			pstmt.setString(2,ee.getEducationname());
			pstmt.setDate(3, ee.getEducationstartday());
			pstmt.setDate(4, ee.getEducationlastday());
			pstmt.setString(5, ee.getEducationstack());
			pstmt.setString(6, ee.getEducationskil());
	
			result=pstmt.executeUpdate();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
		}
		
		return result;
	}
	public String insertEduCareerCurrval(Connection con) {
		Statement stmt=null;
		ResultSet rset=null;
		String rnum="";
		String query=prop.getProperty("EduCareerCurrval");
		
		
		
		try {
			stmt=con.createStatement();
			rset=stmt.executeQuery(query);
			if(rset.next()) {
				rnum=rset.getString("currval");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(rset);
			close(stmt);
		}
		
		
		String rnum1="L"+rnum;
		return rnum1;
	}
	public int insertResumeCareerEdu(Connection con, String eduNoNew, String resumeNo) {
		PreparedStatement pstmt=null;
		int result=0;
		String query=prop.getProperty("insertResumeCareerEdu");
		
		try {
			pstmt=con.prepareStatement(query);
				
			
			pstmt.setString(1,resumeNo);
			pstmt.setString(2,"CAREER");
			pstmt.setString(3,eduNoNew);
			
	
			result=pstmt.executeUpdate();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
		}
		
		return result;
	}
	public int insertDevelCareer(Connection con, hyj_developmentinsert d, Jyh_PerMember loginUser) {
		PreparedStatement pstmt=null;
		int result=0;
		String query=prop.getProperty("insertNewDevel");
		
		try {
			pstmt=con.prepareStatement(query);
				
			
			pstmt.setString(1,loginUser.getPerNo());
			pstmt.setDate(2, d.getJoindate());
			pstmt.setDate(3, d.getResignationdate());
			pstmt.setString(4, d.getDuserstack());
			pstmt.setString(5, d.getDuserskill());
			pstmt.setString(6, d.getUsergit());
	
			result=pstmt.executeUpdate();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
		}
		
		return result;
	}
	
	public String insertDevelCareerCurrval(Connection con) {
		Statement stmt=null;
		ResultSet rset=null;
		String rnum="";
		String query=prop.getProperty("DevelCareerCurrval");
		
		
		
		try {
			stmt=con.createStatement();
			rset=stmt.executeQuery(query);
			if(rset.next()) {
				rnum=rset.getString("currval");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(rset);
			close(stmt);
		}
		
		
		String rnum1="J"+rnum;
		return rnum1;
	}
	public int insertResumeCareerDevel(Connection con, String develNoNew, String resumeNo) {
		PreparedStatement pstmt=null;
		int result=0;
		String query=prop.getProperty("insertResumeCareerDevel");
		
		try {
			pstmt=con.prepareStatement(query);
				
			
			pstmt.setString(1,resumeNo);
			pstmt.setString(2,"DEVELOPMENT");
			pstmt.setString(3,develNoNew);
			
	
			result=pstmt.executeUpdate();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
		}
		
		return result;
	}
	public int insertAwardCareer(Connection con, hyj_awards a, Jyh_PerMember loginUser) {
		PreparedStatement pstmt=null;
		int result=0;
		String query=prop.getProperty("insertNewAward");
		
		try {
			pstmt=con.prepareStatement(query);
				
			
			pstmt.setString(1, a.getAwardsname());
			pstmt.setString(2, a.getAwardscontent());
			pstmt.setString(3, a.getAwardsagency());
			pstmt.setDate(4, a.getAwardsmonth());
			pstmt.setString(5,loginUser.getPerNo());
	
			result=pstmt.executeUpdate();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
		}
		
		return result;
	}
	public String insertAwardCareerCurrval(Connection con) {
		Statement stmt=null;
		ResultSet rset=null;
		String rnum="";
		String query=prop.getProperty("AwardCareerCurrval");
		
		
		
		try {
			stmt=con.createStatement();
			rset=stmt.executeQuery(query);
			if(rset.next()) {
				rnum=rset.getString("currval");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(rset);
			close(stmt);
		}
		
		
		String rnum1="I"+rnum;
		return rnum1;
	}
	public int insertResumeCareerAward(Connection con, String awardNoNew, String resumeNo) {
		PreparedStatement pstmt=null;
		int result=0;
		String query=prop.getProperty("insertResumeCareerDevel");
		
		try {
			pstmt=con.prepareStatement(query);
				
			
			pstmt.setString(1,resumeNo);
			pstmt.setString(2,"AWARD");
			pstmt.setString(3,awardNoNew);
			
	
			result=pstmt.executeUpdate();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
		}
		
		return result;
	}
	public int insertSchoolCareer(Connection con, School s, Jyh_PerMember loginUser) {
		PreparedStatement pstmt=null;
		int result=0;
		String query=prop.getProperty("insertNewSchool");
		
		try {
			pstmt=con.prepareStatement(query);
				
			
			pstmt.setString(1, s.getCaName());
			pstmt.setDate(2, s.getCaEnrollDate());
			pstmt.setDate(3, s.getCaGraduDate());
			pstmt.setString(4, s.getCaMajor());
			pstmt.setString(5, s.getCaGrades());
			pstmt.setString(6, s.getCaTopGrades());
			pstmt.setString(7,loginUser.getPerNo());
			pstmt.setString(8, s.getCaDegree());
			pstmt.setString(9, s.getEtc());
	
			result=pstmt.executeUpdate();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
		}
		
		return result;
	}
	public String insertSchoolCareerCurrval(Connection con) {
		Statement stmt=null;
		ResultSet rset=null;
		String rnum="";
		String query=prop.getProperty("SchoolCareerCurrval");
		
		
		
		try {
			stmt=con.createStatement();
			rset=stmt.executeQuery(query);
			if(rset.next()) {
				rnum=rset.getString("currval");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(rset);
			close(stmt);
		}
		
		
		String rnum1="H"+rnum;
		return rnum1;
	
	}
	public int insertReseumeCareerAward(Connection con, String schoolNoNew, String resumeNo) {
		PreparedStatement pstmt=null;
		int result=0;
		String query=prop.getProperty("insertResumeCareerDevel");
		
		try {
			pstmt=con.prepareStatement(query);
				
			
			pstmt.setString(1,resumeNo);
			pstmt.setString(2,"SCHOOL");
			pstmt.setString(3,schoolNoNew);
			
	
			result=pstmt.executeUpdate();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
		}
		
		return result;
	}
	public int insertVolCareer(Connection con, hyj_volunteer v, Jyh_PerMember loginUser) {
		PreparedStatement pstmt=null;
		int result=0;
		String query=prop.getProperty("insertNewVol");
		
		try {
			pstmt=con.prepareStatement(query);
				
			
			pstmt.setString(1,loginUser.getPerNo());
			pstmt.setString(2,v.getVolunteername());
			pstmt.setDate(3, v.getVolunteerStartDate());
			pstmt.setDate(4, v.getVolunteerendDate());
			pstmt.setString(5, v.getVolunteerlink() );
			pstmt.setString(6, v.getVolunteerExplanation());
	
			result=pstmt.executeUpdate();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
		}
		
		return result;
	}
	public String insertVolCareerCurrval(Connection con) {
		Statement stmt=null;
		ResultSet rset=null;
		String rnum="";
		String query=prop.getProperty("VolCareerCurrval");
		
		
		
		try {
			stmt=con.createStatement();
			rset=stmt.executeQuery(query);
			if(rset.next()) {
				rnum=rset.getString("currval");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(rset);
			close(stmt);
		}
		
		
		String rnum1="N"+rnum;
		return rnum1;
	}
	public int insertReseumeCareerVolun(Connection con, String volNoNew, String resumeNo) {
		PreparedStatement pstmt=null;
		int result=0;
		String query=prop.getProperty("insertResumeCareerDevel");
		
		try {
			pstmt=con.prepareStatement(query);
				
			
			pstmt.setString(1,resumeNo);
			pstmt.setString(2,"SIDEACT");
			pstmt.setString(3,volNoNew);
			
	
			result=pstmt.executeUpdate();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
		}
		
		return result;
	}
	public int insertLieCareer(Connection con, hyj_certificate l, Jyh_PerMember loginUser) {
		PreparedStatement pstmt=null;
		int result=0;
		String query=prop.getProperty("insertNewLi");
		
		try {
			pstmt=con.prepareStatement(query);
				
			
			pstmt.setString(1,loginUser.getPerNo());
			pstmt.setString(2,l.getCertificatename());
			pstmt.setDate(3, l.getCertificatestart());
			pstmt.setDate(4, l.getCertificateend());
			pstmt.setString(5, l.getCertificateagency() );
	
			result=pstmt.executeUpdate();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
		}
		
		return result;

	}
	public String insertLieVareerCurrval(Connection con) {
		Statement stmt=null;
		ResultSet rset=null;
		String rnum="";
		String query=prop.getProperty("LieCareerCurrval");
		
		
		
		try {
			stmt=con.createStatement();
			rset=stmt.executeQuery(query);
			if(rset.next()) {
				rnum=rset.getString("currval");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(rset);
			close(stmt);
		}
		
		
		String rnum1="M"+rnum;
		return rnum1;
	}
	public int insertResumeCareerLie(Connection con, String lieNoNew, String resumeNo) {
		PreparedStatement pstmt=null;
		int result=0;
		String query=prop.getProperty("insertResumeCareerDevel");
		
		try {
			pstmt=con.prepareStatement(query);
				
			
			pstmt.setString(1,resumeNo);
			pstmt.setString(2,"LICENSE");
			pstmt.setString(3,lieNoNew);
			
	
			result=pstmt.executeUpdate();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
		}
		
		return result;
	}
	public int insertAttachment(Connection con, Attachment at) {
		// TODO Auto-generated method stub
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("insertAttachment1");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, at.getOriginName());
			pstmt.setString(2, at.getChangeName());
			pstmt.setString(3, at.getFilePath());
			pstmt.setString(4, at.getAno());
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			close(pstmt);
		}
		

		return result;
	}
	public String insertFileCurrval(Connection con) {
		Statement stmt=null;
		ResultSet rset=null;
		String rnum="";
		String query=prop.getProperty("AttchCurrval");
		
		
		
		try {
			stmt=con.createStatement();
			rset=stmt.executeQuery(query);
			if(rset.next()) {
				rnum=rset.getString("currval");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(rset);
			close(stmt);
		}
		
		String rnum1="AT"+rnum;
		return rnum1;
	
	}
	public int insertResumeCareerFile(Connection con, String fileNoNew, String resumeNo) {
		PreparedStatement pstmt=null;
		int result=0;
		String query=prop.getProperty("insertResumeCareerDevel");
		
		try {
			pstmt=con.prepareStatement(query);
				
			
			pstmt.setString(1,resumeNo);
			pstmt.setString(2,"FILE");
			pstmt.setString(3,fileNoNew);
			
			
	
			result=pstmt.executeUpdate();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
		}
		
		return result;
	}
	public ArrayList<Resume> selectListPerANo(Connection con, String aNo) {
		PreparedStatement pstmt=null;
		ResultSet rset=null;
		ArrayList<Resume> list=null;
		
		String query=prop.getProperty("selectResumePerAno");
		
		try {
			pstmt=con.prepareStatement(query);
			pstmt.setString(1, aNo);
			
			rset=pstmt.executeQuery();
			
			list=new ArrayList<Resume>();
			while(rset.next()) {
				Resume r=new Resume();
				r.setrNo(rset.getString("R_NO"));
				r.setPerNo(rset.getString("PER_NO"));
				r.setrHopeDuty(rset.getString("R_HOPE_DUTY"));
				r.setrHopeYearlySalary(rset.getString("R_HOPE_YEARLY_SALAYR"));
				r.setrPublic(rset.getString("R_PUBLIC"));
				r.setrWriteDate(rset.getDate("R_WRITE_DATE"));
				r.setrUpdateDate(rset.getDate("R_UPDATE_DATE"));
				r.setrTitle(rset.getString("R_TITLE"));
				r.setrContent(rset.getString("R_CONTENT"));
				r.setrSkill(rset.getString("R_SKILL"));
				r.setrDelete(rset.getString("R_DELETE"));
				
				list.add(r);
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
			close(rset);
		}
		System.out.println("dao list:"+list);
		return list;
	}


}