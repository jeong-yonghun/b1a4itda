package com.kh.itda.addInfo.resume.model.vo;

import java.io.Serializable;
import java.sql.Date;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter @Getter @ToString @AllArgsConstructor
public class Resume implements Serializable {
	private String rNo;//이력서 고유번호
	private String perNo;//개인회원번호
	private String rHopeDuty;//희망직무
	private String rHopeYearlySalary;//희망연봉
	private String rPublic;//공개여부
	private Date rWriteDate;//작성일자
	private Date rUpdateDate;//수정일자
	private String rTitle;//제목
	private String rContent;//자기소개
	private String rSkill;//이력서별 스킬
	private String rDelete;//이력서 삭제유무

	
	//포트폴리오 주소
	private String portfolio;
	
	public Resume() {}

	public Resume(String rNo, String perNo, String rHopeDuty, String rHopeYearlySalary, String rPublic, Date rWriteDate,
			Date rUpdateDate, String rTitle, String rContent, String rSkill, String rDelete, String portfolio) {
		super();
		this.rNo = rNo;
		this.perNo = perNo;
		this.rHopeDuty = rHopeDuty;
		this.rHopeYearlySalary = rHopeYearlySalary;
		this.rPublic = rPublic;
		this.rWriteDate = rWriteDate;
		this.rUpdateDate = rUpdateDate;
		this.rTitle = rTitle;
		this.rContent = rContent;
		this.rSkill = rSkill;
		this.rDelete = rDelete;
		this.portfolio = portfolio;
	}

	public String getrNo() {
		return rNo;
	}

	public void setrNo(String rNo) {
		this.rNo = rNo;
	}

	public String getPerNo() {
		return perNo;
	}

	public void setPerNo(String perNo) {
		this.perNo = perNo;
	}

	public String getrHopeDuty() {
		return rHopeDuty;
	}

	public void setrHopeDuty(String rHopeDuty) {
		this.rHopeDuty = rHopeDuty;
	}

	public String getrHopeYearlySalary() {
		return rHopeYearlySalary;
	}

	public void setrHopeYearlySalary(String rHopeYearlySalary) {
		this.rHopeYearlySalary = rHopeYearlySalary;
	}

	public String getrPublic() {
		return rPublic;
	}

	public void setrPublic(String rPublic) {
		this.rPublic = rPublic;
	}

	public Date getrWriteDate() {
		return rWriteDate;
	}

	public void setrWriteDate(Date rWriteDate) {
		this.rWriteDate = rWriteDate;
	}

	public Date getrUpdateDate() {
		return rUpdateDate;
	}

	public void setrUpdateDate(Date rUpdateDate) {
		this.rUpdateDate = rUpdateDate;
	}

	public String getrTitle() {
		return rTitle;
	}

	public void setrTitle(String rTitle) {
		this.rTitle = rTitle;
	}

	public String getrContent() {
		return rContent;
	}

	public void setrContent(String rContent) {
		this.rContent = rContent;
	}

	public String getrSkill() {
		return rSkill;
	}

	public void setrSkill(String rSkill) {
		this.rSkill = rSkill;
	}

	public String getrDelete() {
		return rDelete;
	}

	public void setrDelete(String rDelete) {
		this.rDelete = rDelete;
	}

	public String getPortfolio() {
		return portfolio;
	}

	public void setPortfolio(String portfolio) {
		this.portfolio = portfolio;
	}

	@Override
	public String toString() {
		return "Resume [rNo=" + rNo + ", perNo=" + perNo + ", rHopeDuty=" + rHopeDuty + ", rHopeYearlySalary="
				+ rHopeYearlySalary + ", rPublic=" + rPublic + ", rWriteDate=" + rWriteDate + ", rUpdateDate="
				+ rUpdateDate + ", rTitle=" + rTitle + ", rContent=" + rContent + ", rSkill=" + rSkill + ", rDelete="
				+ rDelete + ", portfolio=" + portfolio + "]";
	}

	
	
	
	
	
	

	
	
}
