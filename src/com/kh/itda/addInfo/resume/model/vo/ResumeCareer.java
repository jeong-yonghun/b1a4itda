package com.kh.itda.addInfo.resume.model.vo;

public class ResumeCareer {

	private String Rno;//이력서 번호
	private String RcDivision;//구분
	private String CarNo;//커리어 번호
	public ResumeCareer(String rno, String rcDivision, String carNo) {
		super();
		Rno = rno;
		RcDivision = rcDivision;
		CarNo = carNo;
	}
	public String getRno() {
		return Rno;
	}
	public void setRno(String rno) {
		Rno = rno;
	}
	public String getRcDivision() {
		return RcDivision;
	}
	public void setRcDivision(String rcDivision) {
		RcDivision = rcDivision;
	}
	public String getCarNo() {
		return CarNo;
	}
	public void setCarNo(String carNo) {
		CarNo = carNo;
	}
	@Override
	public String toString() {
		return "ResumeCareer [Rno=" + Rno + ", RcDivision=" + RcDivision + ", CarNo=" + CarNo + "]";
	}
	
	
}
