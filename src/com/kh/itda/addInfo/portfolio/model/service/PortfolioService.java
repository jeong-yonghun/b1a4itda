package com.kh.itda.addInfo.portfolio.model.service;

import static com.kh.itda.common.JDBCTemplate.close;
import static com.kh.itda.common.JDBCTemplate.commit;
import static com.kh.itda.common.JDBCTemplate.getConnection;
import static com.kh.itda.common.JDBCTemplate.rollback;

import java.sql.Connection;

import com.kh.itda.addInfo.portfolio.model.dao.PortfolioDao;
import com.kh.itda.addInfo.portfolio.model.vo.Portfolio;

public class PortfolioService {

	public Portfolio selectPortfolio(String perNo) {
		// TODO Auto-generated method stub
		Connection con=getConnection();
		
		Portfolio pt=new PortfolioDao().selectPortfolio(con,perNo);
		
		if(pt!=null) {
			commit(con);
		}else {
			rollback(con);
		}
		close(con);
		return pt;
	}

}
