package com.kh.itda.addInfo.portfolio.model.vo;

public class Portfolio {
	private String perNo;
	private String title;
	private String content;
	
	public Portfolio() {}

	public Portfolio(String perNo, String title, String content) {
		super();
		this.perNo = perNo;
		this.title = title;
		this.content = content;
	}

	public String getPerNo() {
		return perNo;
	}

	public void setPerNo(String perNo) {
		this.perNo = perNo;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	@Override
	public String toString() {
		return "Portfolio [perNo=" + perNo + ", title=" + title + ", content=" + content + "]";
	}
	
	
	
	
}
