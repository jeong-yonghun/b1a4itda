package com.kh.itda.addInfo.portfolio.model.dao;

import static com.kh.itda.common.JDBCTemplate.close;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

import com.kh.itda.addInfo.portfolio.model.vo.Portfolio;
import com.kh.itda.addInfo.resume.model.dao.ResumeDao;

public class PortfolioDao {
private Properties prop=new Properties();
	
	public PortfolioDao() {
		String fileName=ResumeDao.class.getResource("/sql/addInfo/portfolio/cho-portfolio.properties").getPath();
		try {
			prop.load(new FileReader(fileName));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public Portfolio selectPortfolio(Connection con, String perNo) {
		PreparedStatement pstmt=null;
		ResultSet rset=null;
		Portfolio pt=null;
		String query=prop.getProperty("selectPortfolio");
		try {
			pstmt=con.prepareStatement(query);
			pstmt.setString(1, perNo);
			rset=pstmt.executeQuery();
			if(rset.next()) {
				pt=new Portfolio();
				pt.setPerNo(rset.getString("PER_NO"));
				pt.setTitle(rset.getString("P_TITLE"));
				pt.setContent(rset.getString("P_CONTENT"));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
			close(rset);
		}
		return pt;
	}
}
