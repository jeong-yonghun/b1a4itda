package com.kh.itda.addInfo.portfolio;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kh.itda.addInfo.portfolio.model.service.PortfolioService;
import com.kh.itda.addInfo.portfolio.model.vo.Portfolio;
import com.kh.itda.member.model.vo.Jyh_PerMember;

/**
 * Servlet implementation class PortfolioMainServlet
 */
@WebServlet("/portfolioMain")
public class PortfolioMainServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PortfolioMainServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		Jyh_PerMember loginUser=(Jyh_PerMember)request.getSession().getAttribute("loginUser");
		Portfolio pt=new PortfolioService().selectPortfolio(loginUser.getPerNo());
		
		String page="";
		if(pt!=null) {
			page="views/person/personMyPage/personMyPagePortfolio/cho_personMyPagePortfolio.jsp";
			request.setAttribute("portfolio", pt);
		}else {
			page="views/common/errorPage.jsp";
			request.setAttribute("msg","포트폴리오목록조회 실패");
		}
		request.getRequestDispatcher(page).forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
