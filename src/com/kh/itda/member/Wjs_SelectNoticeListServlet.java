package com.kh.itda.member;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.kh.itda.member.model.service.Wjs_MemberService;
import com.kh.itda.member.model.vo.Wjs_Notice;


@WebServlet("/notice.com")
public class Wjs_SelectNoticeListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
   
    public Wjs_SelectNoticeListServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

    
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String no = request.getParameter("no");
		
		System.out.println(no);
		
		ArrayList<Wjs_Notice> list = new Wjs_MemberService().selectNoticeList(no);
		
		
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		
		new Gson().toJson(list, response.getWriter());
		
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
