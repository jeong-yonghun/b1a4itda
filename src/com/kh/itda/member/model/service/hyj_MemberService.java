package com.kh.itda.member.model.service;



import static com.kh.itda.common.JDBCTemplate.close;
import static com.kh.itda.common.JDBCTemplate.commit;
import static com.kh.itda.common.JDBCTemplate.getConnection;
import static com.kh.itda.common.JDBCTemplate.rollback;


import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;

import com.kh.itda.member.model.dao.hyj_MemberDao;
import com.kh.itda.member.model.vo.Jyh_ComMember;
import com.kh.itda.member.model.vo.Jyh_PerMember;
import com.kh.itda.member.model.vo.hyj_Attachment;
import com.kh.itda.member.model.vo.hyj_Previous;
import com.kh.itda.member.model.vo.hyj_awards;
import com.kh.itda.member.model.vo.hyj_certificate;
import com.kh.itda.member.model.vo.hyj_developmentinsert;
import com.kh.itda.member.model.vo.hyj_education;
import com.kh.itda.member.model.vo.hyj_member;
import com.kh.itda.member.model.vo.hyj_moreinformation;
import com.kh.itda.member.model.vo.hyj_schoolcareer;
import com.kh.itda.member.model.vo.hyj_volunteer;



public class hyj_MemberService {

 
   public int pephoneid(hyj_member member) {
      Connection con = getConnection();
      
      int result = new hyj_MemberDao().idsearchphone(con, member);
      
      close(con);
      
      
      return result;
   }

   public int perpwemail(hyj_member member) {
      Connection con = getConnection();
      
      int result = new hyj_MemberDao().perpwemail(con, member);
      
      close(con);
      
      return result;
   }

   public int perpwphone(hyj_member member) {
      Connection con = getConnection();
      
      int result = new hyj_MemberDao().perpwphone(con, member);
      
      close(con);
      
      return result;
   }


   public int comidphone(Jyh_ComMember company) {
      Connection con = getConnection();
      
      int result = new hyj_MemberDao().comidphone(con, company);
      
      close(con);
      
      return result;
   }
   //기본정보수정업데이트메소드
   public int personupdate1(Jyh_PerMember member) {

      Connection con = getConnection();
      
      int result = new hyj_MemberDao().infomationupdate(con, member);
      
      if(result > 0) {
    	  commit(con);
    	  
      }else {
    	  rollback(con);
      }
      
      close(con);

      return result;

   }



   public int personinsert2(hyj_Previous member, Jyh_PerMember pERNO) {
      Connection con = getConnection();
      int result = 0;
      
      
      result = new hyj_MemberDao().Previoussearch(con, pERNO);
      
      
      if(result == 1) {
    	  result = new hyj_MemberDao().preupdate(con, member, pERNO);
      }else {
    	  result = new hyj_MemberDao().Previousinsert(con, member, pERNO);  
      }
      close(con);
      
      return result;

   }

   public int Developmentinsert(hyj_developmentinsert member, Jyh_PerMember pERNO) {
      Connection con = getConnection();
      int result = 0;
      
      result = new hyj_MemberDao().developmentsearch(con, pERNO);
      
      if(result == 1) {
    	  result = new hyj_MemberDao().develupdate(con, member, pERNO);
      }else {
    	  result = new hyj_MemberDao().developmentinsert(con, member, pERNO);
      }
      
      
      close(con);
      
      return result;

   }

   public int educationinsert(hyj_education member, Jyh_PerMember perno) {
      Connection con = getConnection();
      int result = 0;

      result = new hyj_MemberDao().educationsearch(con, perno);
       
      if(result == 1) {
    	  result = new hyj_MemberDao().educationupdate(con, member, perno);
      }else {
    	  result = new hyj_MemberDao().educationinsert(con, member, perno);
      }
      
      close(con);
      
      return result;

   }
   public int volunteerinsert(hyj_volunteer member, Jyh_PerMember perno) {
      Connection con = getConnection();
      int result = 0;
      
      result = new hyj_MemberDao().volunteerserch(con, perno);
      
      
      if(result == 1) {
    	  result = new hyj_MemberDao().volunteerupdate(con, member, perno);
      }else {
    	  result = new hyj_MemberDao().volunteerinsert(con, member, perno);
      }
      
      close(con);
      
      return result;

   }


   public int awardsinsert(hyj_awards member, Jyh_PerMember perno) {
      Connection con = getConnection();
      int result = 0;
      
      result = new hyj_MemberDao().awardsserch(con, perno);
       
      if(result == 1) {
    	  result = new hyj_MemberDao().awardsupdate(con, member, perno);
      }else {
    	  result = new hyj_MemberDao().awardsinsert(con, member, perno);
      }
      
      close(con);
      
      return result;
   }

   public int certificateinsert(hyj_certificate member, Jyh_PerMember pERNO) {
      Connection con = getConnection();
      int result = 0;
      
      result = new hyj_MemberDao().certificateserch(con, pERNO);
      
      if(result == 1) {
    	  result = new hyj_MemberDao().certificateupdate(con, member, pERNO);
      }else {
    	  result = new hyj_MemberDao().certificateinsert(con, member, pERNO);
      }
      
      close(con);
      
      return result;
   }

   public Jyh_PerMember oneidsearch(Jyh_PerMember member2) {
      Connection con = getConnection();
      
      Jyh_PerMember member = new hyj_MemberDao().oneidsearch(con, member2);
      
      close(con);
            
      return member;
   }

   public Jyh_ComMember comoneidsearch(Jyh_ComMember requestmember) {
      Connection con = getConnection();
      
      Jyh_ComMember member = new hyj_MemberDao().comoneidsearch(con, requestmember);
      
      close(con);
      
      return member;
   }

public int perpwsearch(Jyh_PerMember requestmember) {
		Connection con = getConnection();
		Jyh_PerMember member = null;
		int result = 0;
		
		member = new hyj_MemberDao().perpwsearch(con, requestmember);
		
		if(member != null) {
			result = new hyj_MemberDao().perpwupdate(con, member, requestmember);
			commit(con);
			System.out.println(result);
		}else {
			rollback(con);
		}
		
		
		close(con);
		
	return result;
}

public int compwsearch(Jyh_ComMember requestmember) {
	Connection con = getConnection();
	Jyh_ComMember member = null;
	int result = 0;
	
	member = new hyj_MemberDao().compwsearch(con, requestmember);
	
	System.out.println(member);
	
	if(member != null) {
		result = new hyj_MemberDao().compwupdate(con, member, requestmember);
		commit(con);
		System.out.println(result);
	}else {
		rollback(con);
	}
	
	
	close(con);
	
return result;
}


public int schoolinsert(hyj_schoolcareer member, Jyh_PerMember perno) {
	Connection con = getConnection();
    int result = 0;
    
    result = new hyj_MemberDao().schoolserch(con, perno);
    
    if(result == 1) {
  	  result = new hyj_MemberDao().schoolupdate(con, member, perno);
  	  
    }else {
  	  result = new hyj_MemberDao().schoolinsert(con, member, perno);
    }
    
    close(con);
    
    return result;
}


public Jyh_PerMember onephidsearch(Jyh_PerMember requestmember) {
	
    Connection con = getConnection();
    
    Jyh_PerMember member = null;

    member = new hyj_MemberDao().onephidsearch(con, requestmember);
    
    close(con);
          
    return member;
}

public Jyh_ComMember comphidsearch(Jyh_ComMember requestmember) {
	
    Connection con = getConnection();
    
    Jyh_ComMember member = null;

    member = new hyj_MemberDao().comphidsearch(con, requestmember);
    
    close(con);
          
    return member;
}

public int perphpwsearch(Jyh_PerMember requestmember) {
	Connection con = getConnection();
	Jyh_PerMember member = null;
	int result = 0;
	
	member = new hyj_MemberDao().perphpwsearch(con, requestmember);
	
	if(member != null) {
		result = new hyj_MemberDao().perphpwupdate(con, member, requestmember);
		commit(con);
		System.out.println(result);
	}
	
	
	close(con);
	
return result;
}

public int comphpwsearch(Jyh_ComMember requestmember) {
	Connection con = getConnection();
	Jyh_ComMember member = null;
	int result = 0;
	
	member = new hyj_MemberDao().comphpwsearch(con, requestmember);
	
	System.out.println(member);
	
	if(member != null) {
		result = new hyj_MemberDao().comphpwupdate(con, member, requestmember);
		commit(con);
		System.out.println(result);
	}
	
	
	close(con);
	
return result;
}

public ArrayList<HashMap<String, Object>> moreinformation(String userno) {
	Connection con = getConnection();

	ArrayList<HashMap<String, Object>> information = new hyj_MemberDao().searchmember(con, userno);
	
	information = new hyj_MemberDao().companycareer(con, userno, information);
	information = new hyj_MemberDao().educarrer(con, userno, information);
	information = new hyj_MemberDao().DevelCareer(con, userno, information);
	information = new hyj_MemberDao().Award(con, userno, information);
	information = new hyj_MemberDao().school(con, userno, information);
	information = new hyj_MemberDao().volunte(con, userno, information);
	information = new hyj_MemberDao().License(con, userno, information);
	
	if(information != null) {
		commit(con);
	}else {
		rollback(con);
	}
	close(con);
	
	
	
	return information;
}

public Jyh_PerMember personpwsearch(Jyh_PerMember requestmember) {
	
	Connection con = getConnection();
	
	Jyh_PerMember member = null;
	
	member = new hyj_MemberDao().personpwsearch(con, requestmember);
	
	
	close(con);
	
return member;
}

public Jyh_PerMember personpwphsearch(Jyh_PerMember requestmember) {
	Connection con = getConnection();
	
	Jyh_PerMember member = null;
	
	member = new hyj_MemberDao().personpwphsearch(con, requestmember);
	
	
	close(con);
	
return member;
}

public Jyh_ComMember companypwsearch(Jyh_ComMember requestmember) {
	
	Connection con = getConnection();
	
	Jyh_ComMember member = null;
	
	member = new hyj_MemberDao().companypwsearch(con, requestmember);
	
	close(con);
	
return member;
}

public Jyh_ComMember companyphpwsearch(Jyh_ComMember requestmember) {
	
	Connection con = getConnection();
	
	Jyh_ComMember member = null;
	
	member = new hyj_MemberDao().companyphpwsearch(con, requestmember);
	
	close(con);
	
return member;
}

public String skillinsert(String userno, String skill) {
	
	Connection con = getConnection();
	String skillcode = null;
	int result1 = 0;
	int result2 = 0;
	skillcode = new hyj_MemberDao().skillsearch(con, skill);
	
	if(skillcode != null) {
		result1 = new hyj_MemberDao().perskillsearch(con, skillcode, userno);
		System.out.println(result1);
		if(result1 == 0) {
			result2 = new hyj_MemberDao().skillinsert(con, skillcode, userno);
			commit(con);
		}else {
			rollback(con);
		}
    	}
		System.out.println(result1);
		System.out.println(skillcode);
	close(con);
	
return skillcode;
}

public String skilldelete(String userno, String skill) {
	
	Connection con = getConnection();
	
	String skillcode = null;
	
	int result1 = 0;
	int result2 = 0;
	
	skillcode = new hyj_MemberDao().skillsearch(con, skill);
	
	if(skillcode != null) {
		
		result1 = new hyj_MemberDao().perskillsearch(con, skillcode, userno);
		
		if(result1 == 0) {
			
			result2 = new hyj_MemberDao().skilldelete(con, skillcode, userno);
			
			commit(con);
			
		}else {
			
			rollback(con);
		}
    	}

	close(con);
	
return skillcode;
}

public int moreinfoupdate(Jyh_ComMember member) {
    Connection con = getConnection();
    
    int result = new hyj_MemberDao().cominfomationupdate(con, member);
    
    close(con);
    
    return result;
}

}