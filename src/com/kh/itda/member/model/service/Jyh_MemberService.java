package com.kh.itda.member.model.service;

import static com.kh.itda.common.JDBCTemplate.close;
import static com.kh.itda.common.JDBCTemplate.commit;
import static com.kh.itda.common.JDBCTemplate.getConnection;
import static com.kh.itda.common.JDBCTemplate.rollback;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;

import com.kh.itda.approval.model.vo.PageInfo;
import com.kh.itda.member.model.dao.Jyh_MemberDao;
import com.kh.itda.member.model.vo.Jyh_ComMember;
import com.kh.itda.member.model.vo.Jyh_PerMember;

public class Jyh_MemberService {

	public Jyh_PerMember loginCheckPer(Jyh_PerMember requestMember, String memChk) {
		Connection con = getConnection();//14번 common패키지에 JDBCTemplate만들기 후 임포트 설정 끝
		
		if(memChk.equals("1")) {
			Jyh_PerMember loginUser = new Jyh_MemberDao().Per_LoginCheck(con, requestMember);
			return loginUser;
		}
		close(con);
		
		return null;
	}

	public Jyh_ComMember loginCheckCom(Jyh_ComMember requestMember, String memChk) {
		Connection con = getConnection();//14번 common패키지에 JDBCTemplate만들기 후 임포트 설정 끝
		
		if(memChk.equals("2")) {
			Jyh_ComMember loginUser = new Jyh_MemberDao().Com_LoginCheck(con, requestMember);
			return loginUser;
		}
		close(con);
		
		return null;
	}

	public int insertMemberPer(Jyh_PerMember requestMember, String[] skillArr) {
		Connection con = getConnection();
		
		int result = 0;
		ArrayList<String> skillCodeArr = null;
		
		int result1 = new Jyh_MemberDao().insertMemberPer(con, requestMember);
		
		if(result1 > 0) {
			String perNo = new Jyh_MemberDao().selectCurrvalPer(con);
			if(perNo != null) {
				skillCodeArr = new Jyh_MemberDao().selectSkillCode(con, skillArr);
				if(skillCodeArr != null) {
					int result2 = new Jyh_MemberDao().insertPerSkill(con, skillCodeArr, perNo);
					if(result2 > 0) {
						int result3 = new Jyh_MemberDao().insertPerStatus(con, perNo);
						if(result3 > 0) {
							result = 1;
						}else {
							result = 0;
						}
					}
				}
			}
		} else {
			result = 0;
		}
		
		if(result > 0) {
			commit(con);
		}else {
			rollback(con);
		}
		
		close(con);
		
		return result;
	}

	public int insertMemberCom(Jyh_ComMember requestMember) {
		Connection con = getConnection();
		
		int result1 = new Jyh_MemberDao().insertMemberCom(con, requestMember);
		
		int result = 0;
		if(result1 > 0) {
			String comNo = new Jyh_MemberDao().selectCurrvalCom(con);
			if(comNo != null) {
				int result2 = new Jyh_MemberDao().insertComStatus(con, comNo);
				if(result2 > 0) {
					commit(con);
					result = 1;
				}else {
					rollback(con);
					result = 0;
				}
			}
		}else {
			rollback(con);
			result = 0;
		}
		
		close(con);
		
		return result;
	}

	public int getListCount(String memType) {
		Connection con = getConnection();
		
		int listCount = 0;
		
		if(memType.equals("1")) {
			listCount = new Jyh_MemberDao().getPerListCount(con);
		}else {
			listCount = new Jyh_MemberDao().getComListCount(con);
		}
		
		close(con);
		
		return listCount;
	}

	public ArrayList<Jyh_PerMember> selectListPer(PageInfo pi) {
		Connection con = getConnection();
		
		ArrayList<Jyh_PerMember> list = new Jyh_MemberDao().selectListPer(con, pi);
		
		close(con);
		
		return list;
	}

	public HashMap<String, Object> selectOnePer(String userNo) {
		Connection con = getConnection();
		
		HashMap<String, Object> perDetail = new HashMap<>();
		
		Jyh_PerMember perMember = new Jyh_MemberDao().selectOnePer(con, userNo);
		
		ArrayList<String> skillArr = new Jyh_MemberDao().selectPerSkill(con, userNo);
		
		perDetail.put("perMember", perMember);
		perDetail.put("skillArr", skillArr);
		
		close(con);
		
		return perDetail;
	}

	public ArrayList<HashMap<String, Object>> selectApplicationList(String userNo, PageInfo pi) {
		Connection con = getConnection();
		
		ArrayList<HashMap<String, Object>> list = new Jyh_MemberDao().selectApplicationList(con, userNo, pi);
		
		close(con);
		
		return list;
	}

	public ArrayList<HashMap<String, Object>> selectDocumentList(String userNo, PageInfo pi) {
		Connection con = getConnection();
		
		ArrayList<HashMap<String, Object>> list = new Jyh_MemberDao().selectDocumentList(con, userNo, pi);
		
		close(con);
		
		return list;
	}

	public ArrayList<HashMap<String, Object>> selectPauseList(PageInfo pi) {
		Connection con = getConnection();
		
		ArrayList<HashMap<String, Object>> list = new Jyh_MemberDao().selectPauseList(con, pi);
		
		close(con);
		
		return list;
	}

	public ArrayList<Jyh_ComMember> selectListCom(PageInfo pi) {
		Connection con = getConnection();
		
		ArrayList<Jyh_ComMember> list = new Jyh_MemberDao().selectListCom(con, pi);
		
		close(con);
		
		return list;
	}

	public Jyh_ComMember selectOneCom(String userNo) {
		Connection con = getConnection();
		
		Jyh_ComMember ComMember = new Jyh_MemberDao().selectOneCom(con, userNo);
		
		close(con);
		
		return ComMember;
	}

	public ArrayList<HashMap<String, Object>> selectAnnouncementList(String userNo, PageInfo pi) {
		Connection con = getConnection();
		
		ArrayList<HashMap<String, Object>> list = new Jyh_MemberDao().selectAnnouncementList(con, userNo, pi);
		
		close(con);
		
		return list;
	}

	public int getAnnListCount(String userNo) {
		Connection con = getConnection();
		
		int listAnnCount = new Jyh_MemberDao().getAnnListCount(con, userNo);
		
		close(con);
		
		return listAnnCount;
	}

	public int getApplicationListCount(String userNo) {
		Connection con = getConnection();
		
		int listAppliCount = new Jyh_MemberDao().getApplicationListCount(con, userNo);
		
		close(con);
		
		return listAppliCount;
	}

	public int getDocumentListCount(String userNo) {
		Connection con = getConnection();
		
		int listAppliCount = new Jyh_MemberDao().getDocumentListCount(con, userNo);
		
		close(con);
		
		return listAppliCount;
	}

	public int getPauseListCount() {
		Connection con = getConnection();
		
		int listCount = 0;
		
			listCount = new Jyh_MemberDao().getPauseListCount(con);
		
		close(con);
		
		return listCount;
	}

	public int getBlackListCount() {
		Connection con = getConnection();
		
		int listCount = 0;
		
			listCount = new Jyh_MemberDao().getBlackListCount(con);
		
		close(con);
		
		return listCount;
	}

	public ArrayList<HashMap<String, Object>> selectBlackList(PageInfo pi) {
		Connection con = getConnection();
		
		ArrayList<HashMap<String, Object>> list = new Jyh_MemberDao().selectBlackList(con, pi);
		
		close(con);
		
		return list;
	}

	public int getPaymentListCount(String userNo) {
		Connection con = getConnection();
		
		int listPaymentCount = new Jyh_MemberDao().getPaymentListCount(con, userNo);
		
		close(con);
		
		return listPaymentCount;
	}

	public ArrayList<HashMap<String, Object>> selectPaymentList(String userNo, PageInfo pi) {
		Connection con = getConnection();
		
		ArrayList<HashMap<String, Object>> list = new Jyh_MemberDao().selectPaymentList(con, userNo, pi);
		
		close(con);
		
		return list;
	}

	public ArrayList<Jyh_PerMember> searchPerList(PageInfo pi) {
		Connection con = getConnection();
		
		ArrayList<Jyh_PerMember> list = new Jyh_MemberDao().searchPerList(con, pi);
		
		close(con);
		
		return list;
	}

	public int getChangeListCount(String memType) {
		Connection con = getConnection();
		
		int listCount = 0;
		
		if(memType.equals("1")) {
			listCount = new Jyh_MemberDao().getChangeListCountPer(con);
		}else {
			listCount = new Jyh_MemberDao().getChangeListCountCom(con);
		}
		
		close(con);
		
		return listCount;
	}

	public ArrayList<Jyh_ComMember> searchComList(PageInfo pi) {
		Connection con = getConnection();
		
		ArrayList<Jyh_ComMember> list = new Jyh_MemberDao().searchComList(con, pi);
		
		close(con);
		
		return list;
	}

	public int changeQueryPer(StringBuilder changeQuery, StringBuilder changeListCount) {
		
		int result = new Jyh_MemberDao().changeQueryPer(changeQuery, changeListCount);
		
		return result;
	}

	public int changeQueryCom(StringBuilder changeQuery, StringBuilder changeListCount) {
		int result = new Jyh_MemberDao().changeQueryCom(changeQuery, changeListCount);
		
		return result;
	}

	public String selectId(String id) {
		Connection con = getConnection();
		
		String returnId = new Jyh_MemberDao().selectId(con, id);
		
		close(con);
		
		return returnId;
	}

	public Jyh_PerMember selectMemBasicInfo(String userNo) {
		Connection con = getConnection();
		
		Jyh_PerMember perMem = new Jyh_MemberDao().selectMemBasicInfo(con, userNo);
		
		close(con);
		
		return perMem;
	}

	public String loginCheckAdmin(String id, String pwd) {
		Connection con = getConnection();
		
		String admin = new Jyh_MemberDao().loginCheckAdmin(con, id, pwd);
		
		close(con);
		
		return admin;
	}

}