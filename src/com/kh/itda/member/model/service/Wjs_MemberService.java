package com.kh.itda.member.model.service;

import static com.kh.itda.common.JDBCTemplate.close;
import static com.kh.itda.common.JDBCTemplate.commit;
import static com.kh.itda.common.JDBCTemplate.getConnection;
import static com.kh.itda.common.JDBCTemplate.rollback;

import java.sql.Connection;
import java.util.ArrayList;

import com.kh.itda.addInfo.resume.model.vo.Resume;
import com.kh.itda.member.model.dao.Wjs_MemberDao;
import com.kh.itda.member.model.vo.Jyh_PerMember;
import com.kh.itda.member.model.vo.Wjs_Notice;
import com.kh.itda.member.model.vo.Wjs_favorite;
import com.kh.itda.recruitAndContest.model.vo.Announcement;
import com.kh.itda.recruitAndContest.model.vo.Attachment;

import oracle.net.ano.Ano;

public class Wjs_MemberService {

   public int deleteMember(Jyh_PerMember deleteMember) {
      
      Connection con = getConnection();
      
      int result = new Wjs_MemberDao().deleteMember(con, deleteMember);
      
      System.out.println("result" + result);
      
      if(result > 0) {
         commit(con);
      }else {
         rollback(con);
      }
      
      return result;
   }

   
   public int insertFavorite(Wjs_favorite fv) {
      
      Connection con = getConnection();
      
      int result = new Wjs_MemberDao().insertFavorite(con, fv);
      
      if(result > 0) {
         commit(con);
      }else {
         rollback(con);
      }
      
      return result;
   }

   public int deletetFavorite(Wjs_favorite fv){
      
      Connection con = getConnection();
      
      int result = new Wjs_MemberDao().deleteFavorite(con, fv);
      System.out.println("삭제");
      
      if(result > 0) {
         commit(con);
      }else {
         rollback(con);
      }
      
      return result;
   }

   public ArrayList<Announcement> favoriteListAn(Jyh_PerMember loginUser) {
      
      Connection con = getConnection();
      
      ArrayList<Announcement> list = new Wjs_MemberDao().favoriteListAn(con, loginUser);
      
      close(con);
      
      return list;
   }

   public ArrayList<Resume> selectResume(Jyh_PerMember loginUser) {
      
      Connection con = getConnection();
      
      ArrayList<Resume> list = new Wjs_MemberDao().selectResume(con, loginUser);
      
      close(con);
      
      return list;
   }

   public int surpportHire(String ano, String pno, String rno) {
      
      Connection con = getConnection();
      
      int result = new Wjs_MemberDao().surpportHire(con, ano, pno, rno);
      
      String apv = new Wjs_MemberDao().selectCurrval(con,ano,pno,rno);
      
      int result2 = new Wjs_MemberDao().surpportRecord(con, apv);
      
      if(result > 0 && result2 > 0) {
         commit(con);
      }else {
         rollback(con);
      }
      
      return result;
   }

   public Announcement selectHireEnd(String ano) {
      
      Connection con = getConnection();
      
      Announcement a = new Wjs_MemberDao().selectHireEnd(con, ano);
      
      close(con);
      
      return a;
   }

   public ArrayList<Wjs_Notice> selectNoticeList(String no) {
      
      Connection con = getConnection();
      
      ArrayList<Wjs_Notice> list = new Wjs_MemberDao().selectNoticeList(con,no);
      
      close(con);
      
      return list;
   }

   public Announcement selectAnnouncement(String num) {
      
      Connection con = getConnection();
      
      Announcement an = new Wjs_MemberDao().selectAnnouncement(con, num);
      
      close(con);
      
      return an;
   }


   public Wjs_Notice selectNoticeMsg(String no1) {
      
      Connection con = getConnection();
      
      Wjs_Notice notice = new Wjs_MemberDao().selectNoticeMsg(con, no1);
      
      close(con);
      
      return notice;
   }


   public ArrayList<Announcement> favoriteListAn2(Jyh_PerMember loginUser) {
      
      Connection con = getConnection();
      
      ArrayList<Announcement> list = new Wjs_MemberDao().favoriteListAn2(con, loginUser);
      
      close(con);
      
      return list;
      
   }


   public Wjs_favorite selectFavorite(String aNo, String mNo) {
      
      Connection con = getConnection();
      
      Wjs_favorite fv = new Wjs_MemberDao().selectFavorite(con, aNo,mNo);
      
      close(con);
      
      
      return fv;
   }


   public int deleteNotice(String nNo) {
      
      Connection con = getConnection();
      
      int result = new Wjs_MemberDao().deleteNotice(con, nNo);
      
      if(result > 0) {
         commit(con);
      }else {
         rollback(con);
      }
      
      return result;
   }

   

}