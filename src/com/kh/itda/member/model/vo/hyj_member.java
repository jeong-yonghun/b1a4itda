package com.kh.itda.member.model.vo;

import java.sql.Date;

public class hyj_member implements java.io.Serializable{
	
	private String perNo;
	private String perId;
	private String perPwd;
	private String perName;
	private String phone;
	private String email;
	private Date enrollDate;
	private Date modifyDate;
	private int warningCtn; 
	private int infoPeriod;
	private String birthday;
	private String gender;
	
	public hyj_member() {}

	public hyj_member(String perNo, String perId, String perPwd, String perName, String phone, String email,
			Date enrollDate, Date modifyDate, int warningCtn, int infoPeriod, String birthday, String gender) {
		super();
		this.perNo = perNo;
		this.perId = perId;
		this.perPwd = perPwd;
		this.perName = perName;
		this.phone = phone;
		this.email = email;
		this.enrollDate = enrollDate;
		this.modifyDate = modifyDate;
		this.warningCtn = warningCtn;
		this.infoPeriod = infoPeriod;
		this.birthday = birthday;
		this.gender = gender;
	}

	public String getPerNo() {
		return perNo;
	}

	public void setPerNo(String perNo) {
		this.perNo = perNo;
	}

	public String getPerId() {
		return perId;
	}

	public void setPerId(String perId) {
		this.perId = perId;
	}

	public String getPerPwd() {
		return perPwd;
	}

	public void setPerPwd(String perPwd) {
		this.perPwd = perPwd;
	}

	public String getPerName() {
		return perName;
	}

	public void setPerName(String perName) {
		this.perName = perName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getEnrollDate() {
		return enrollDate;
	}

	public void setEnrollDate(Date enrollDate) {
		this.enrollDate = enrollDate;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public int getWarningCtn() {
		return warningCtn;
	}

	public void setWarningCtn(int warningCtn) {
		this.warningCtn = warningCtn;
	}

	public int getInfoPeriod() {
		return infoPeriod;
	}

	public void setInfoPeriod(int infoPeriod) {
		this.infoPeriod = infoPeriod;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	@Override
	public String toString() {
		return "hyj_member [perNo=" + perNo + ", perId=" + perId + ", perPwd=" + perPwd + ", perName=" + perName
				+ ", phone=" + phone + ", email=" + email + ", enrollDate=" + enrollDate + ", modifyDate=" + modifyDate
				+ ", warningCtn=" + warningCtn + ", infoPeriod=" + infoPeriod + ", birthday=" + birthday + ", gender="
				+ gender + "]";
	}

	
	
	
}
