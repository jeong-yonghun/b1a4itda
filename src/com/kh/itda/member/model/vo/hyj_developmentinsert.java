package com.kh.itda.member.model.vo;

import java.sql.Date;

public class hyj_developmentinsert implements java.io.Serializable {
	
	private String develNo;
	private Date joindate;
	private Date resignationdate;
	private String duserstack;
	private String duserskill;
	private String usergit;
	
	public hyj_developmentinsert() {}

	public hyj_developmentinsert(String develNo, Date joindate, Date resignationdate, String duserstack,
			String duserskil1, String usergit) {
		super();
		this.develNo = develNo;
		this.joindate = joindate;
		this.resignationdate = resignationdate;
		this.duserstack = duserstack;
		this.duserskill = duserskil1;
		this.usergit = usergit;
	}

	public String getDevelNo() {
		return develNo;
	}

	public void setDevelNo(String develNo) {
		this.develNo = develNo;
	}

	public Date getJoindate() {
		return joindate;
	}

	public void setJoindate(Date joindate) {
		this.joindate = joindate;
	}

	public Date getResignationdate() {
		return resignationdate;
	}

	public void setResignationdate(Date resignationdate) {
		this.resignationdate = resignationdate;
	}

	public String getDuserstack() {
		return duserstack;
	}

	public void setDuserstack(String duserstack) {
		this.duserstack = duserstack;
	}

	public String getDuserskill() {
		return duserskill;
	}

	public void setDuserskill(String duserskil1) {
		this.duserskill = duserskil1;
	}

	public String getUsergit() {
		return usergit;
	}

	public void setUsergit(String usergit) {
		this.usergit = usergit;
	}

	@Override
	public String toString() {
		return "hyj_developmentinsert [develNo=" + develNo + ", joindate=" + joindate + ", resignationdate="
				+ resignationdate + ", duserstack=" + duserstack + ", duserskill=" + duserskill + ", usergit=" + usergit
				+ "]";
	}

	
	
	
	
	
}
