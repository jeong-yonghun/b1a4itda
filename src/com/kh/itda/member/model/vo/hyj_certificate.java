package com.kh.itda.member.model.vo;

import java.sql.Date;

public class hyj_certificate implements java.io.Serializable{
	//자격증
	private String certificateNo;
	private String certificatename;
	private Date certificatestart;
	private Date certificateend;
	private String certificateagency;
	
	public hyj_certificate() {}

	public hyj_certificate(String certificateNo, String certificatename, Date certificatestart, Date certificateend,
			String certificateagency) {
		super();
		this.certificateNo = certificateNo;
		this.certificatename = certificatename;
		this.certificatestart = certificatestart;
		this.certificateend = certificateend;
		this.certificateagency = certificateagency;
	}

	public String getCertificateNo() {
		return certificateNo;
	}

	public void setCertificateNo(String certificateNo) {
		this.certificateNo = certificateNo;
	}

	public String getCertificatename() {
		return certificatename;
	}

	public void setCertificatename(String certificatename) {
		this.certificatename = certificatename;
	}

	public Date getCertificatestart() {
		return certificatestart;
	}

	public void setCertificatestart(Date certificatestart) {
		this.certificatestart = certificatestart;
	}

	public Date getCertificateend() {
		return certificateend;
	}

	public void setCertificateend(Date certificateend) {
		this.certificateend = certificateend;
	}

	public String getCertificateagency() {
		return certificateagency;
	}

	public void setCertificateagency(String certificateagency) {
		this.certificateagency = certificateagency;
	}

	@Override
	public String toString() {
		return "hyj_certificate [certificateNo=" + certificateNo + ", certificatename=" + certificatename
				+ ", certificatestart=" + certificatestart + ", certificateend=" + certificateend
				+ ", certificateagency=" + certificateagency + "]";
	}


	

	
	
	
}
