package com.kh.itda.member.model.vo;

import java.sql.Date;

public class hyj_Attachment implements java.io.Serializable{
	private String at_no;
	private String originName;
	private String changeName;
	private String filePath;
	private Date uploadDate;
	private int fileLevel;
	private String status;
	private String perno;
	
	public hyj_Attachment() {}

	public hyj_Attachment(String at_no, String originName, String changeName, String filePath, Date uploadDate,
			int fileLevel, String status, String perno) {
		super();
		this.at_no = at_no;
		this.originName = originName;
		this.changeName = changeName;
		this.filePath = filePath;
		this.uploadDate = uploadDate;
		this.fileLevel = fileLevel;
		this.status = status;
		this.perno = perno;
	}

	public String getAt_no() {
		return at_no;
	}

	public void setAt_no(String at_no) {
		this.at_no = at_no;
	}

	public String getOriginName() {
		return originName;
	}

	public void setOriginName(String originName) {
		this.originName = originName;
	}

	public String getChangeName() {
		return changeName;
	}

	public void setChangeName(String changeName) {
		this.changeName = changeName;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public Date getUploadDate() {
		return uploadDate;
	}

	public void setUploadDate(Date uploadDate) {
		this.uploadDate = uploadDate;
	}

	public int getFileLevel() {
		return fileLevel;
	}

	public void setFileLevel(int fileLevel) {
		this.fileLevel = fileLevel;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getPerno() {
		return perno;
	}

	public void setPerno(String perno) {
		this.perno = perno;
	}

	@Override
	public String toString() {
		return "hyj_Attachment [at_no=" + at_no + ", originName=" + originName + ", changeName=" + changeName
				+ ", filePath=" + filePath + ", uploadDate=" + uploadDate + ", fileLevel=" + fileLevel + ", status="
				+ status + ", perno=" + perno + "]";
	}

	
	

	
	
}
