package com.kh.itda.member.model.vo;

import java.sql.Date;

public class Jyh_PerMember {
	
	private String perNo;
	private String perId;
	private String perPwd;
	private String perName;
	private String phone;
	private String email;
	private Date enrollDate;
	private Date modifyDate;
	private String infoPeriod;
	private String gender;
	private Date birthDay;
	private String status;
	private String reason;
	
	public Jyh_PerMember() {}

	public Jyh_PerMember(String perNo, String perId, String perPwd, String perName, String phone, String email,
			Date enrollDate, Date modifyDate, String infoPeriod, String gender, Date birthDay, String status,
			String reason) {
		super();
		this.perNo = perNo;
		this.perId = perId;
		this.perPwd = perPwd;
		this.perName = perName;
		this.phone = phone;
		this.email = email;
		this.enrollDate = enrollDate;
		this.modifyDate = modifyDate;
		this.infoPeriod = infoPeriod;
		this.gender = gender;
		this.birthDay = birthDay;
		this.status = status;
		this.reason = reason;
	}

	public String getPerNo() {
		return perNo;
	}

	public void setPerNo(String perNo) {
		this.perNo = perNo;
	}

	public String getPerId() {
		return perId;
	}

	public void setPerId(String perId) {
		this.perId = perId;
	}

	public String getPerPwd() {
		return perPwd;
	}

	public void setPerPwd(String perPwd) {
		this.perPwd = perPwd;
	}

	public String getPerName() {
		return perName;
	}

	public void setPerName(String perName) {
		this.perName = perName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getEnrollDate() {
		return enrollDate;
	}

	public void setEnrollDate(Date enrollDate) {
		this.enrollDate = enrollDate;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public String getInfoPeriod() {
		return infoPeriod;
	}

	public void setInfoPeriod(String infoPeriod) {
		this.infoPeriod = infoPeriod;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public Date getBirthDay() {
		return birthDay;
	}

	public void setBirthDay(Date birthDay) {
		this.birthDay = birthDay;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	@Override
	public String toString() {
		return "Jyh_PerMember [perNo=" + perNo + ", perId=" + perId + ", perPwd=" + perPwd + ", perName=" + perName
				+ ", phone=" + phone + ", email=" + email + ", enrollDate=" + enrollDate + ", modifyDate=" + modifyDate
				+ ", infoPeriod=" + infoPeriod + ", gender=" + gender + ", birthDay=" + birthDay + ", status=" + status
				+ ", reason=" + reason + "]";
	}

}
