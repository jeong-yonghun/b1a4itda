package com.kh.itda.member.model.vo;

import java.sql.Date;

public class Jyh_ComMember {
	
	private String comNo;
	private String comId;
	private String comPwd;
	private String comName;
	private String phone;
	private String email;
	private Date enrollDate;
	private Date modifyDate;
	private String infoPeriod;
	private String comNum;
	private String companyName;
	private String comUrl;
	private String comAddress;
	private String numEmp;
	private String invest;
	private String sales;
	private String status;
	private String reason;
	
	public Jyh_ComMember() {}

	public Jyh_ComMember(String comNo, String comId, String comPwd, String comName, String phone, String email,
			Date enrollDate, Date modifyDate, String infoPeriod, String comNum, String companyName, String comUrl,
			String comAddress, String numEmp, String invest, String sales, String status, String reason) {
		super();
		this.comNo = comNo;
		this.comId = comId;
		this.comPwd = comPwd;
		this.comName = comName;
		this.phone = phone;
		this.email = email;
		this.enrollDate = enrollDate;
		this.modifyDate = modifyDate;
		this.infoPeriod = infoPeriod;
		this.comNum = comNum;
		this.companyName = companyName;
		this.comUrl = comUrl;
		this.comAddress = comAddress;
		this.numEmp = numEmp;
		this.invest = invest;
		this.sales = sales;
		this.status = status;
		this.reason = reason;
	}

	public String getComNo() {
		return comNo;
	}

	public void setComNo(String comNo) {
		this.comNo = comNo;
	}

	public String getComId() {
		return comId;
	}

	public void setComId(String comId) {
		this.comId = comId;
	}

	public String getComPwd() {
		return comPwd;
	}

	public void setComPwd(String comPwd) {
		this.comPwd = comPwd;
	}

	public String getComName() {
		return comName;
	}

	public void setComName(String comName) {
		this.comName = comName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getEnrollDate() {
		return enrollDate;
	}

	public void setEnrollDate(Date enrollDate) {
		this.enrollDate = enrollDate;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public String getInfoPeriod() {
		return infoPeriod;
	}

	public void setInfoPeriod(String infoPeriod) {
		this.infoPeriod = infoPeriod;
	}

	public String getComNum() {
		return comNum;
	}

	public void setComNum(String comNum) {
		this.comNum = comNum;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getComUrl() {
		return comUrl;
	}

	public void setComUrl(String comUrl) {
		this.comUrl = comUrl;
	}

	public String getComAddress() {
		return comAddress;
	}

	public void setComAddress(String comAddress) {
		this.comAddress = comAddress;
	}

	public String getNumEmp() {
		return numEmp;
	}

	public void setNumEmp(String numEmp) {
		this.numEmp = numEmp;
	}

	public String getInvest() {
		return invest;
	}

	public void setInvest(String invest) {
		this.invest = invest;
	}

	public String getSales() {
		return sales;
	}

	public void setSales(String sales) {
		this.sales = sales;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	@Override
	public String toString() {
		return "Jyh_ComMember [comNo=" + comNo + ", comId=" + comId + ", comPwd=" + comPwd + ", comName=" + comName
				+ ", phone=" + phone + ", email=" + email + ", enrollDate=" + enrollDate + ", modifyDate=" + modifyDate
				+ ", infoPeriod=" + infoPeriod + ", comNum=" + comNum + ", companyName=" + companyName + ", comUrl="
				+ comUrl + ", comAddress=" + comAddress + ", numEmp=" + numEmp + ", invest=" + invest + ", sales="
				+ sales + ", status=" + status + ", reason=" + reason + "]";
	}

}
