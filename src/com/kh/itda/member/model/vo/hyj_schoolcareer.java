package com.kh.itda.member.model.vo;

import java.sql.Date;

public class hyj_schoolcareer {
	private String schoolno;
	private String schoolname;
	private String schooletc;
	private Date schoolenrolldate;
	private Date schoolgraduatedate;
	private String schoolmajor;
	private String schoolmypoint;
	private String schoolgradepoint;
	private String schoolexplanation;
	
	public hyj_schoolcareer() {}

	public hyj_schoolcareer(String schoolno, String schoolname, String schooletc, Date schoolenrolldate,
			Date schoolgraduatedate, String schoolmajor, String schoolmypoint, String schoolgradepoint,
			String schoolexplanation) {
		super();
		this.schoolno = schoolno;
		this.schoolname = schoolname;
		this.schooletc = schooletc;
		this.schoolenrolldate = schoolenrolldate;
		this.schoolgraduatedate = schoolgraduatedate;
		this.schoolmajor = schoolmajor;
		this.schoolmypoint = schoolmypoint;
		this.schoolgradepoint = schoolgradepoint;
		this.schoolexplanation = schoolexplanation;
	}

	public String getSchoolno() {
		return schoolno;
	}

	public void setSchoolno(String schoolno) {
		this.schoolno = schoolno;
	}

	public String getSchoolname() {
		return schoolname;
	}

	public void setSchoolname(String schoolname) {
		this.schoolname = schoolname;
	}

	public String getSchooletc() {
		return schooletc;
	}

	public void setSchooletc(String schooletc) {
		this.schooletc = schooletc;
	}

	public Date getSchoolenrolldate() {
		return schoolenrolldate;
	}

	public void setSchoolenrolldate(Date schoolenrolldate) {
		this.schoolenrolldate = schoolenrolldate;
	}

	public Date getSchoolgraduatedate() {
		return schoolgraduatedate;
	}

	public void setSchoolgraduatedate(Date schoolgraduatedate) {
		this.schoolgraduatedate = schoolgraduatedate;
	}

	public String getSchoolmajor() {
		return schoolmajor;
	}

	public void setSchoolmajor(String schoolmajor) {
		this.schoolmajor = schoolmajor;
	}

	public String getSchoolmypoint() {
		return schoolmypoint;
	}

	public void setSchoolmypoint(String schoolmypoint) {
		this.schoolmypoint = schoolmypoint;
	}

	public String getSchoolgradepoint() {
		return schoolgradepoint;
	}

	public void setSchoolgradepoint(String schoolgradepoint) {
		this.schoolgradepoint = schoolgradepoint;
	}

	public String getSchoolexplanation() {
		return schoolexplanation;
	}

	public void setSchoolexplanation(String schoolexplanation) {
		this.schoolexplanation = schoolexplanation;
	}

	@Override
	public String toString() {
		return "hyj_schoolcareer [schoolno=" + schoolno + ", schoolname=" + schoolname + ", schooletc=" + schooletc
				+ ", schoolenrolldate=" + schoolenrolldate + ", schoolgraduatedate=" + schoolgraduatedate
				+ ", schoolmajor=" + schoolmajor + ", schoolmypoint=" + schoolmypoint + ", schoolgradepoint="
				+ schoolgradepoint + ", schoolexplanation=" + schoolexplanation + "]";
	}
	
	
}
