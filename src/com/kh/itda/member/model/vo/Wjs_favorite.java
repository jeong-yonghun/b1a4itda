package com.kh.itda.member.model.vo;

public class Wjs_favorite {
	
	private int fno;
	private String favoriteDo;
	private String favoriteRe;
	
	public Wjs_favorite() {}

	public Wjs_favorite(int fno, String favoriteDo, String favoriteRe) {
		super();
		this.fno = fno;
		this.favoriteDo = favoriteDo;
		this.favoriteRe = favoriteRe;
	}

	public int getFno() {
		return fno;
	}

	public void setFno(int fno) {
		this.fno = fno;
	}

	public String getFavoriteDo() {
		return favoriteDo;
	}

	public void setFavoriteDo(String favoriteDo) {
		this.favoriteDo = favoriteDo;
	}

	public String getFavoriteRe() {
		return favoriteRe;
	}

	public void setFavoriteRe(String favoriteRe) {
		this.favoriteRe = favoriteRe;
	}

	@Override
	public String toString() {
		return "Wjs_favorite [fno=" + fno + ", favoriteDo=" + favoriteDo + ", favoriteRe=" + favoriteRe + "]";
	}
	
	
}
