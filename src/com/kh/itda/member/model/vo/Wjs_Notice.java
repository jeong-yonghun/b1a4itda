package com.kh.itda.member.model.vo;

public class Wjs_Notice {
	
	private String nNo;
	private String nMsg;
	private String mNo;
	private String aNo;
	private String status;
	private String divide;
	private int num;
	private String atitle;
	
	public Wjs_Notice() {}

	public String getnNo() {
		return nNo;
	}

	public void setnNo(String nNo) {
		this.nNo = nNo;
	}

	public String getnMsg() {
		return nMsg;
	}

	public void setnMsg(String nMsg) {
		this.nMsg = nMsg;
	}

	public String getmNo() {
		return mNo;
	}

	public void setmNo(String mNo) {
		this.mNo = mNo;
	}

	public String getaNo() {
		return aNo;
	}

	public void setaNo(String aNo) {
		this.aNo = aNo;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDivide() {
		return divide;
	}

	public void setDivide(String divide) {
		this.divide = divide;
	}

	public int getNum() {
		return num;
	}

	public void setNum(int num) {
		this.num = num;
	}

	public String getAtitle() {
		return atitle;
	}

	public void setAtitle(String atitle) {
		this.atitle = atitle;
	}

	public Wjs_Notice(String nNo, String nMsg, String mNo, String aNo, String status, String divide, int num,
			String atitle) {
		super();
		this.nNo = nNo;
		this.nMsg = nMsg;
		this.mNo = mNo;
		this.aNo = aNo;
		this.status = status;
		this.divide = divide;
		this.num = num;
		this.atitle = atitle;
	}

	@Override
	public String toString() {
		return "Wjs_Notice [nNo=" + nNo + ", nMsg=" + nMsg + ", mNo=" + mNo + ", aNo=" + aNo + ", status=" + status
				+ ", divide=" + divide + ", num=" + num + ", atitle=" + atitle + "]";
	}

		
	

	
	

}
