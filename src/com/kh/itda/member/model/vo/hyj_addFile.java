package com.kh.itda.member.model.vo;

import java.sql.Date;

public class hyj_addFile {

	private String addFileNo;//첨부파일고유번호
	private String FileName;//파일명
	private String FilePath;//파일경로
	private String FileSize;//파일사이즈
	private String FileForm;//파일확장자
	private Date uploadDate;//업로드 날짜
	
	public hyj_addFile() {};
	public hyj_addFile(String addFileNo, String fileName, String filePath, String fileSize, String fileForm,
			Date uploadDate) {
		super();
		this.addFileNo = addFileNo;
		FileName = fileName;
		FilePath = filePath;
		FileSize = fileSize;
		FileForm = fileForm;
		this.uploadDate = uploadDate;
	}
	public String getAddFileNo() {
		return addFileNo;
	}
	public void setAddFileNo(String addFileNo) {
		this.addFileNo = addFileNo;
	}
	public String getFileName() {
		return FileName;
	}
	public void setFileName(String fileName) {
		FileName = fileName;
	}
	public String getFilePath() {
		return FilePath;
	}
	public void setFilePath(String filePath) {
		FilePath = filePath;
	}
	public String getFileSize() {
		return FileSize;
	}
	public void setFileSize(String fileSize) {
		FileSize = fileSize;
	}
	public String getFileForm() {
		return FileForm;
	}
	public void setFileForm(String fileForm) {
		FileForm = fileForm;
	}
	public Date getUploadDate() {
		return uploadDate;
	}
	public void setUploadDate(Date uploadDate) {
		this.uploadDate = uploadDate;
	}
	@Override
	public String toString() {
		return "hyj_addFile [addFileNo=" + addFileNo + ", FileName=" + FileName + ", FilePath=" + FilePath
				+ ", FileSize=" + FileSize + ", FileForm=" + FileForm + ", uploadDate=" + uploadDate + "]";
	}
	
	
	
	
}
