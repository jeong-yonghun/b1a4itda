package com.kh.itda.member.model.vo;

import java.sql.Date;

public class hyj_education implements java.io.Serializable{

	private String educationNo;
	private String educationname;
	private Date educationstartday;
	private Date educationlastday;
	private String educationstack;
	private String educationskil;
	
	public hyj_education() {}

	public hyj_education(String educationNo, String educationname, Date educationstartday, Date educationlastday,
			String educationstack, String educationskil) {
		super();
		this.educationNo = educationNo;
		this.educationname = educationname;
		this.educationstartday = educationstartday;
		this.educationlastday = educationlastday;
		this.educationstack = educationstack;
		this.educationskil = educationskil;
	}

	public String getEducationNo() {
		return educationNo;
	}

	public void setEducationNo(String educationNo) {
		this.educationNo = educationNo;
	}

	public String getEducationname() {
		return educationname;
	}

	public void setEducationname(String educationname) {
		this.educationname = educationname;
	}

	public Date getEducationstartday() {
		return educationstartday;
	}

	public void setEducationstartday(Date educationstartday) {
		this.educationstartday = educationstartday;
	}

	public Date getEducationlastday() {
		return educationlastday;
	}

	public void setEducationlastday(Date educationlastday) {
		this.educationlastday = educationlastday;
	}

	public String getEducationstack() {
		return educationstack;
	}

	public void setEducationstack(String educationstack) {
		this.educationstack = educationstack;
	}

	public String getEducationskil() {
		return educationskil;
	}

	public void setEducationskil(String educationskil) {
		this.educationskil = educationskil;
	}

	@Override
	public String toString() {
		return "hyj_education [educationNo=" + educationNo + ", educationname=" + educationname + ", educationstartday="
				+ educationstartday + ", educationlastday=" + educationlastday + ", educationstack=" + educationstack
				+ ", educationskil=" + educationskil + "]";
	}

	

	
	
	
	
	
}
