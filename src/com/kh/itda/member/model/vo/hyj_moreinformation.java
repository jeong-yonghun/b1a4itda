package com.kh.itda.member.model.vo;

public class hyj_moreinformation implements java.io.Serializable{
	private String birthday;
	private String gender;
	
	public hyj_moreinformation() {}

	public hyj_moreinformation(String birthday, String gender) {
		super();
		this.birthday = birthday;
		this.gender = gender;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	@Override
	public String toString() {
		return "hyj_moreinformation [birthday=" + birthday + ", gender=" + gender + "]";
	}
	
	
	
}
