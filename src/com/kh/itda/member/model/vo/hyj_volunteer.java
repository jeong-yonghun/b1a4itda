package com.kh.itda.member.model.vo;

import java.sql.Date;

public class hyj_volunteer implements java.io.Serializable{
	
	private String volunteerNo;
	private String volunteername;
	private Date volunteerStartDate;
	private Date volunteerendDate;
	private String volunteerlink;
	private String volunteerExplanation;
	
	public hyj_volunteer() {}

	public hyj_volunteer(String volunteerNo, String volunteername, Date volunteerStartDate, Date volunteerendDate,
			String volunteerlink, String volunteerExplanation) {
		super();
		this.volunteerNo = volunteerNo;
		this.volunteername = volunteername;
		this.volunteerStartDate = volunteerStartDate;
		this.volunteerendDate = volunteerendDate;
		this.volunteerlink = volunteerlink;
		this.volunteerExplanation = volunteerExplanation;
	}

	public String getVolunteerNo() {
		return volunteerNo;
	}

	public void setVolunteerNo(String volunteerNo) {
		this.volunteerNo = volunteerNo;
	}

	public String getVolunteername() {
		return volunteername;
	}

	public void setVolunteername(String volunteername) {
		this.volunteername = volunteername;
	}

	public Date getVolunteerStartDate() {
		return volunteerStartDate;
	}

	public void setVolunteerStartDate(Date volunteerStartDate) {
		this.volunteerStartDate = volunteerStartDate;
	}

	public Date getVolunteerendDate() {
		return volunteerendDate;
	}

	public void setVolunteerendDate(Date volunteerendDate) {
		this.volunteerendDate = volunteerendDate;
	}

	public String getVolunteerlink() {
		return volunteerlink;
	}

	public void setVolunteerlink(String volunteerlink) {
		this.volunteerlink = volunteerlink;
	}

	public String getVolunteerExplanation() {
		return volunteerExplanation;
	}

	public void setVolunteerExplanation(String volunteerExplanation) {
		this.volunteerExplanation = volunteerExplanation;
	}

	@Override
	public String toString() {
		return "hyj_volunteer [volunteerNo=" + volunteerNo + ", volunteername=" + volunteername
				+ ", volunteerStartDate=" + volunteerStartDate + ", volunteerendDate=" + volunteerendDate
				+ ", volunteerlink=" + volunteerlink + ", volunteerExplanation=" + volunteerExplanation + "]";
	}

	

	
	
	
	

}
