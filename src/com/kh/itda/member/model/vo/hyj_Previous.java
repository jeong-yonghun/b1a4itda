package com.kh.itda.member.model.vo;

import java.sql.Date;

public class hyj_Previous implements java.io.Serializable{
	private String PreviousNo;
	private String Previousname;//회사명
	private String Previousjobname;//직군
	private Date Previousjoindate;
	private Date Resignationdate;
	private String stack;
	private String money;
	
	public hyj_Previous() {}

	public hyj_Previous(String previousNo, String previousname, String previousjobname, Date previousjoindate,
			Date resignationdate, String stack, String money) {
		super();
		PreviousNo = previousNo;
		Previousname = previousname;
		Previousjobname = previousjobname;
		Previousjoindate = previousjoindate;
		Resignationdate = resignationdate;
		this.stack = stack;
		this.money = money;
	}

	public String getPreviousNo() {
		return PreviousNo;
	}

	public void setPreviousNo(String previousNo) {
		PreviousNo = previousNo;
	}

	public String getPreviousname() {
		return Previousname;
	}

	public void setPreviousname(String previousname) {
		Previousname = previousname;
	}

	public String getPreviousjobname() {
		return Previousjobname;
	}

	public void setPreviousjobname(String previousjobname) {
		Previousjobname = previousjobname;
	}

	public Date getPreviousjoindate() {
		return Previousjoindate;
	}

	public void setPreviousjoindate(Date previousjoindate) {
		Previousjoindate = previousjoindate;
	}

	public Date getResignationdate() {
		return Resignationdate;
	}

	public void setResignationdate(Date resignationdate) {
		Resignationdate = resignationdate;
	}

	public String getStack() {
		return stack;
	}

	public void setStack(String stack) {
		this.stack = stack;
	}

	public String getMoney() {
		return money;
	}

	public void setMoney(String money) {
		this.money = money;
	}

	@Override
	public String toString() {
		return "hyj_Previous [PreviousNo=" + PreviousNo + ", Previousname=" + Previousname + ", Previousjobname="
				+ Previousjobname + ", Previousjoindate=" + Previousjoindate + ", Resignationdate=" + Resignationdate
				+ ", stack=" + stack + ", money=" + money + "]";
	}

	

	

	
	
	
}
