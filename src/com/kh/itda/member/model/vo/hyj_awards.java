package com.kh.itda.member.model.vo;

import java.sql.Date;

public class hyj_awards implements java.io.Serializable{
	
	private String awardNo;
	private String awardsname;
	private String awardscontent;
	private Date awardsmonth;
	private String awardsagency;
	
	public hyj_awards() {}

	public hyj_awards(String awardNo, String awardsname, String awardscontent, Date awardsmonth, String awardsagency) {
		super();
		this.awardNo = awardNo;
		this.awardsname = awardsname;
		this.awardscontent = awardscontent;
		this.awardsmonth = awardsmonth;
		this.awardsagency = awardsagency;
	}

	public String getAwardNo() {
		return awardNo;
	}

	public void setAwardNo(String awardNo) {
		this.awardNo = awardNo;
	}

	public String getAwardsname() {
		return awardsname;
	}

	public void setAwardsname(String awardsname) {
		this.awardsname = awardsname;
	}

	public String getAwardscontent() {
		return awardscontent;
	}

	public void setAwardscontent(String awardscontent) {
		this.awardscontent = awardscontent;
	}

	public Date getAwardsmonth() {
		return awardsmonth;
	}

	public void setAwardsmonth(Date awardsmonth) {
		this.awardsmonth = awardsmonth;
	}

	public String getAwardsagency() {
		return awardsagency;
	}

	public void setAwardsagency(String awardsagency) {
		this.awardsagency = awardsagency;
	}

	@Override
	public String toString() {
		return "hyj_awards [awardNo=" + awardNo + ", awardsname=" + awardsname + ", awardscontent=" + awardscontent
				+ ", awardsmonth=" + awardsmonth + ", awardsagency=" + awardsagency + "]";
	}

	

	
	
	
	
	
}
