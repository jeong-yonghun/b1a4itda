package com.kh.itda.member.model.dao;

import static com.kh.itda.common.JDBCTemplate.close;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Properties;

import com.kh.itda.addInfo.resume.model.vo.Resume;
import com.kh.itda.member.model.vo.Jyh_PerMember;
import com.kh.itda.member.model.vo.Wjs_Notice;
import com.kh.itda.member.model.vo.Wjs_favorite;
import com.kh.itda.recruitAndContest.model.vo.Announcement;

public class Wjs_MemberDao {
	
	private Properties prop = new Properties();
	
	public Wjs_MemberDao() {
		
		String fileName = Wjs_MemberDao.class.getResource("/sql/member/wjs-member-query.properties").getPath();
		
		try {
			prop.load(new FileReader(fileName));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	

	public int deleteMember(Connection con, Jyh_PerMember deleteMember) {
		
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("deleteMember");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, deleteMember.getPerNo());
			
			 result = pstmt.executeUpdate();
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			close(pstmt);
		}
		
		
		return result;
	}


	public int insertFavorite(Connection con, Wjs_favorite fv) {
		
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("insertFavorite");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, fv.getFavoriteDo());
			pstmt.setString(2, fv.getFavoriteRe());
			
			result = pstmt.executeUpdate();
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			close(pstmt);
		}
		
		return result;
	}


	public int deleteFavorite(Connection con, Wjs_favorite fv) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("deleteFavorite");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, fv.getFavoriteDo());
			pstmt.setString(2, fv.getFavoriteRe());
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			close(pstmt);
		}
		
		return result;
	}


	public ArrayList<Announcement> favoriteListAn(Connection con, Jyh_PerMember loginUser) {
		PreparedStatement pstmt = null;
		ArrayList<Announcement> list = null;
		ResultSet rset = null;
		
		String query = prop.getProperty("favoriteListAn");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, loginUser.getPerNo());
			rset = pstmt.executeQuery();
			
			list = new ArrayList<>();
			while(rset.next()) {
				
				Announcement a = new Announcement();
				a.setaTitle(rset.getString("A_TITLE"));
				a.setaReceiptStart(rset.getDate("A_RECEIPT_START"));
				a.setaReceiptEnd(rset.getDate("A_RECEIPT_END"));
				a.setStatus(rset.getString("STATUS"));
				a.setaNo(rset.getString("A_NO"));
				
				list.add(a);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			close(pstmt);
			close(rset);
		}
		
		
		return list;
	}


	public ArrayList<Resume> selectResume(Connection con, Jyh_PerMember loginUser) {
		
		PreparedStatement pstmt = null;
		ArrayList<Resume> list = null;
		ResultSet rset = null;
		
		String query = prop.getProperty("selectResume");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, loginUser.getPerNo());
			
			rset = pstmt.executeQuery();
			
			list = new ArrayList<>();
			
			while(rset.next()) {
				Resume r = new Resume();
				r.setrTitle(rset.getString("R_TITLE"));
				r.setrUpdateDate(rset.getDate("R_UPDATE_DATE"));
				r.setPerNo(rset.getString("PER_NO"));
				r.setrNo(rset.getString("R_NO"));
				
				list.add(r);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			close(pstmt);
			close(rset);
		}
		
		return list;
	}


	public int surpportHire(Connection con, String ano, String pno, String rno) {
		
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("surpportHire");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, pno);
			pstmt.setString(2, rno);
			pstmt.setString(3, ano);
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			close(pstmt);
		}
		
		return result;
	}


	public Announcement selectHireEnd(Connection con, String ano) {
		
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		Announcement a = null;
		
		String query = prop.getProperty("selectHireEnd");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, ano);
			rset = pstmt.executeQuery();
			
			while(rset.next()) {
				a = new Announcement();
				a.setDuty(rset.getString("AA_DUTY"));
				a.setCom_nmae(rset.getString("COM_NAME"));
				
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			close(rset);
			close(pstmt);
		}
		
		return a;
	}


	public ArrayList<Wjs_Notice> selectNoticeList(Connection con, String no) {
		
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<Wjs_Notice> list = null;
		
		String query = prop.getProperty("selectNoticeList");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, no);
			
			rset = pstmt.executeQuery();
			list = new ArrayList<>();
			while(rset.next()) {
				Wjs_Notice n = new Wjs_Notice();
				n.setnNo(rset.getString("N_NO"));
				n.setnMsg(rset.getString("N_MSG"));
				n.setmNo(rset.getString("M_NO"));
				n.setStatus(rset.getString("STATUS"));
				n.setDivide(rset.getString("DIVIDE"));
				n.setaNo(rset.getString("A_NO"));
				list.add(n);
				System.out.println("n :" + n);
			}
			
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			close(rset);
			close(pstmt);
		}
		
		
		return list;
	}


	public Announcement selectAnnouncement(Connection con, String num) {
		
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		Announcement an = null;
		
		String query = prop.getProperty("selectAnnouncement");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, num);
			
			rset = pstmt.executeQuery();
			
			an = new Announcement();
			while(rset.next()) {
				an.setaNo(rset.getString("A_NO"));
				an.setaTitle(rset.getString("A_TITLE"));
				an.setCom_nmae(rset.getString("COM_NAME"));
				an.setDuty(rset.getString("AA_DUTY"));
				an.setCareer(rset.getString("AA_CAREER"));
				an.setScale(rset.getString("AA_SCALE"));
				an.setService(rset.getString("AA_SERVICE"));
				an.setaReceiptStart(rset.getDate("A_RECEIPT_START"));
				an.setaReceiptEnd(rset.getDate("A_RECEIPT_END"));
				an.setLocation(rset.getString("AA_LOCATION"));
				an.setIntroduce(rset.getString("AA_INTRODUCE"));
				an.setSkill(rset.getString("A_SKILL"));
				an.setEligibility(rset.getString("AA_ELIGIBILITY"));
				an.setPreferential(rset.getString("AA_PREFERENTIAL"));
				an.setPresentation(rset.getString("AA_PRESENTATION"));
				an.setProcess(rset.getString("AA_PROCESS"));
				an.setCom_url(rset.getString("COM_URL"));
				an.setNum_emp(rset.getString("NUM_EMP"));
				an.setInvest(rset.getString("INVEST"));
				an.setSales(rset.getString("SALES"));
				an.setaDivision(rset.getString("CHANGE_NAME"));
				
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			close(rset);
			close(pstmt);
		}
		
		return an;
	}



	public Wjs_Notice selectNoticeMsg(Connection con, String no1) {
		
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		Wjs_Notice n = null;
		
		String query = prop.getProperty("selectNoticeMsg");
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, no1);
			System.out.println("no :" +no1);
			
			rset = pstmt.executeQuery();
			
			n = new Wjs_Notice();
			
			while(rset.next()) {
				n.setnNo(rset.getString("N_NO"));
				n.setnMsg(rset.getString("N_MSG"));
				n.setmNo(rset.getString("M_NO"));
				n.setStatus(rset.getString("STATUS"));
				n.setNum(rset.getInt("NUM"));
				n.setDivide(rset.getString("DIVIDE"));
				n.setaNo(rset.getString("A_NO"));
				n.setAtitle(rset.getString("A_TITLE"));
			}
			System.out.println(n);
			
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			close(rset);
			close(pstmt);
		}
		
		return n;
	}



	public ArrayList<Announcement> favoriteListAn2(Connection con, Jyh_PerMember loginUser) {
		PreparedStatement pstmt = null;
		ArrayList<Announcement> list = null;
		ResultSet rset = null;
		
		String query = prop.getProperty("favoriteListAn2");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, loginUser.getPerNo());
			rset = pstmt.executeQuery();
			
			list = new ArrayList<>();
			while(rset.next()) {
				
				Announcement a = new Announcement();
				a.setaTitle(rset.getString("A_TITLE"));
				a.setaReceiptStart(rset.getDate("A_RECEIPT_START"));
				a.setaReceiptEnd(rset.getDate("A_RECEIPT_END"));
				a.setStatus(rset.getString("STATUS"));
				a.setaNo(rset.getString("A_NO"));
				
				list.add(a);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			close(pstmt);
			close(rset);
		}
		
		
		return list;
		
	}



	public Wjs_favorite selectFavorite(Connection con, String aNo, String mNo) {
		
		PreparedStatement pstmt = null;
		Wjs_favorite fv = null;
		ResultSet rset = null;
		
		String query = prop.getProperty("selectFavorite");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, mNo);
			pstmt.setString(2, aNo);
			rset = pstmt.executeQuery();
			
			fv = new Wjs_favorite();
			while(rset.next()) {
				fv.setFno(rset.getInt("F_NO"));
			}
			
			System.out.println("fv" + fv.getFno());
			
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			close(rset);
			close(pstmt);
		}
		
		
		return fv;
	}



	public int surpportRecord(Connection con, String ano) {
		
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("surpportRecord");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, ano);
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			close(pstmt);
		}
		
		return result;
	}



	public String selectCurrval(Connection con, String ano, String pno, String rno) {
		
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		String apv = "";
		
		String query = prop.getProperty("selectCurrvalApv");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, ano);
			pstmt.setString(2, pno);
			pstmt.setString(3, rno);
			
			rset = pstmt.executeQuery();
			
			
			while(rset.next()) {
				apv = rset.getString("APV_NO");
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			close(rset);
			close(pstmt);
		}
		
		return apv;
	}



	public int deleteNotice(Connection con, String nNo) {
		
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("deleteNotice");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, nNo);
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			close(pstmt);
		}
		
		return result;
	}

}
