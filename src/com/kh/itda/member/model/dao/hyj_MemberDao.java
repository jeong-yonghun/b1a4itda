package com.kh.itda.member.model.dao;



import static com.kh.itda.common.JDBCTemplate.close;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;

import com.kh.itda.addInfo.school.model.vo.School;
import com.kh.itda.member.model.vo.Jyh_ComMember;
import com.kh.itda.member.model.vo.Jyh_PerMember;
import com.kh.itda.member.model.vo.hyj_Attachment;
import com.kh.itda.member.model.vo.hyj_Previous;
import com.kh.itda.member.model.vo.hyj_awards;
import com.kh.itda.member.model.vo.hyj_certificate;
import com.kh.itda.member.model.vo.hyj_developmentinsert;
import com.kh.itda.member.model.vo.hyj_education;
import com.kh.itda.member.model.vo.hyj_member;
import com.kh.itda.member.model.vo.hyj_moreinformation;
import com.kh.itda.member.model.vo.hyj_schoolcareer;
import com.kh.itda.member.model.vo.hyj_volunteer;


public class hyj_MemberDao {
   
private Properties prop = new Properties();
   
   public hyj_MemberDao() {
      String fileName = hyj_MemberDao.class.getResource("/sql/member/hyj-member-query.properties").getPath();
      
      try {
         prop.load(new FileReader(fileName));
         
      } catch (IOException e) {
         e.printStackTrace();
      }
   }

  

   public int idsearchphone(Connection con, hyj_member member) {
      PreparedStatement pstmt = null;
      ResultSet rset = null;
      int result = 0;
      
      String query = prop.getProperty("phoneidsearch");
      
      try {
         pstmt = con.prepareStatement(query);
         pstmt.setString(1, member.getPerName());
         pstmt.setString(2, member.getPhone());
         
         rset = pstmt.executeQuery();
         
         if(rset.next()) {
            result = rset.getInt(1);
         }
               

      } catch (SQLException e) {
         // TODO Auto-generated catch block
         e.printStackTrace();
      }finally {
         close(pstmt);
         close(rset);
      }

      
      return result;
   }

   public int perpwemail(Connection con, hyj_member member) {
      PreparedStatement pstmt = null;
      ResultSet rset = null;
      int result = 0;
      
      String query = prop.getProperty("perpwemailsearch");
      
      try {
         pstmt = con.prepareStatement(query);
         pstmt.setString(1, member.getPerName());
         pstmt.setString(2, member.getPerId());
         pstmt.setString(3, member.getEmail());
         
         rset = pstmt.executeQuery();
         
         if(rset.next()) {
            result = rset.getInt(1);
         }
         

         
      } catch (SQLException e) {
         // TODO Auto-generated catch block
         e.printStackTrace();
      }finally {
         close(pstmt);
         close(rset);
      }

      return result;
   }

   public int perpwphone(Connection con, hyj_member member) {
      PreparedStatement pstmt = null;
      ResultSet rset = null;
      int result = 0;
      
      String query = prop.getProperty("perpwphonesearch");
      
      try {
         pstmt = con.prepareStatement(query);
         pstmt.setString(1, member.getPerName());
         pstmt.setString(2, member.getPerId());
         pstmt.setString(3, member.getPhone());
         
         rset = pstmt.executeQuery();
         
         if(rset.next()) {
            result = rset.getInt(1);
         }
         
         
         
      } catch (SQLException e) {
         // TODO Auto-generated catch block
         e.printStackTrace();
      }finally {
         close(pstmt);
         close(rset);
      }
   
      return result;
   }



   public int comidphone(Connection con, Jyh_ComMember company) {
      PreparedStatement pstmt = null;
      ResultSet rset = null;
      int result = 0;
      
      String query = prop.getProperty("comidphone");
      
      try {
         pstmt = con.prepareStatement(query);
         pstmt.setString(1, company.getComName());
         pstmt.setString(2, company.getPhone());
         
         rset = pstmt.executeQuery();
         
         if(rset.next()) {
            result = rset.getInt(1);
         }
      
      } catch (SQLException e) {
         // TODO Auto-generated catch block
         e.printStackTrace();
      }finally {
         close(pstmt);
         close(rset);
      }
      return result;
   }

   public int Previousinsert(Connection con, hyj_Previous member, Jyh_PerMember pERNO) {
      PreparedStatement pstmt = null;
      int result = 0;
      
      String query = prop.getProperty("Previousinsert");
      
      try {
       
         pstmt = con.prepareStatement(query);
         pstmt.setString(1, pERNO.getPerNo());
         pstmt.setString(2, member.getPreviousname());
         pstmt.setString(3, member.getPreviousjobname());
         pstmt.setDate(4, member.getPreviousjoindate());
         pstmt.setDate(5, member.getResignationdate());
         pstmt.setString(6, member.getStack());
         pstmt.setString(7, member.getMoney());

    
         result = pstmt.executeUpdate();
      
      } catch (SQLException e) {
         e.printStackTrace();
      }finally{
         close(pstmt);
      }
      
      return result;
   }

   
   public int moreinformationinsert(Connection con, Jyh_PerMember member) {
      PreparedStatement pstmt = null;
      int result = 0;
      
      String query = prop.getProperty("moreinfomationinsert");
      
      try {
         pstmt = con.prepareStatement(query);
         pstmt.setString(1, member.getGender());
         pstmt.setDate(2, member.getBirthDay());
         pstmt.setString(3, member.getPerNo());

         
         result = pstmt.executeUpdate();
         System.out.println(result);
      } catch (SQLException e) {
         // TODO Auto-generated catch block
         e.printStackTrace();
      }finally{
         close(pstmt);
      }
      
      return result;
   }

      public int infomationupdate(Connection con, Jyh_PerMember member) {
      PreparedStatement pstmt = null;
      int result = 0;
      
      String query = prop.getProperty("informationupdate");
      
      try {
         pstmt = con.prepareStatement(query);
         pstmt.setString(1, member.getPerName());
         pstmt.setString(2, member.getPhone());
         pstmt.setString(3, member.getEmail());
         pstmt.setDate(4, member.getBirthDay());
         pstmt.setString(5, member.getGender());
         pstmt.setString(6, member.getPerNo());

         
         result = pstmt.executeUpdate();
         System.out.println(result);
      } catch (SQLException e) {
      
         e.printStackTrace();
      }finally{
         close(pstmt);
      }
      
      return result;
   }

   public int developmentinsert(Connection con, hyj_developmentinsert member, Jyh_PerMember pERNO) {
      PreparedStatement pstmt = null;
      int result = 0;
      
      String query = prop.getProperty("developmentinsert1");
      
      try {
         pstmt = con.prepareStatement(query);
         pstmt.setString(1, pERNO.getPerNo());
         pstmt.setDate(2, member.getJoindate());
         pstmt.setDate(3, member.getResignationdate());
         pstmt.setString(4, member.getDuserstack());
         pstmt.setString(5, member.getDuserskill());
         pstmt.setString(6, member.getUsergit());
         
         result = pstmt.executeUpdate();

      } catch (SQLException e) {
         e.printStackTrace();
      }finally{
         close(pstmt);
      }
      
      return result;
   }

      public int educationinsert(Connection con, hyj_education member, Jyh_PerMember perno) {
      PreparedStatement pstmt = null;
      int result = 0;
      
      String query = prop.getProperty("educationinsert");
      
      try {
         pstmt = con.prepareStatement(query);
         pstmt.setString(1, perno.getPerNo());
         pstmt.setString(2, member.getEducationname());
         pstmt.setDate(3, member.getEducationstartday());
         pstmt.setDate(4, member.getEducationlastday());
         pstmt.setString(5, member.getEducationstack());
         pstmt.setString(6, member.getEducationskil());
         

         result = pstmt.executeUpdate();

      } catch (SQLException e) {
         e.printStackTrace();
      }finally{
         close(pstmt);
      }
      
      return result;
   }

   public int volunteerinsert(Connection con, hyj_volunteer member, Jyh_PerMember perno) {
      PreparedStatement pstmt = null;
      int result = 0;
      
      String query = prop.getProperty("volunteerinsert");
      
      try {
         pstmt = con.prepareStatement(query);
         pstmt.setString(1, perno.getPerNo());
         pstmt.setString(2, member.getVolunteername());
         pstmt.setDate(3, member.getVolunteerStartDate());
         pstmt.setDate(4, member.getVolunteerendDate());
         pstmt.setString(5, member.getVolunteerlink());
         pstmt.setString(6, member.getVolunteerExplanation());
         

         result = pstmt.executeUpdate();

      } catch (SQLException e) {

         e.printStackTrace();
      }finally{
         close(pstmt);
      }
      
      return result;
   }

 

 

   public int awardsinsert(Connection con, hyj_awards member, Jyh_PerMember perno) {
      PreparedStatement pstmt = null;
      int result = 0;
      
      String query = prop.getProperty("awardsinsert");
      
      try {
         pstmt = con.prepareStatement(query);
         pstmt.setString(1, member.getAwardsname());
         pstmt.setString(2, member.getAwardscontent());
         pstmt.setString(3, member.getAwardsagency());
         pstmt.setDate(4, member.getAwardsmonth());
         pstmt.setString(5, perno.getPerNo());
         
         result = pstmt.executeUpdate();
         
      } catch (SQLException e) {
     
         e.printStackTrace();
      }finally {
         close(pstmt);
      }

      return result;
   }

   public int certificateinsert(Connection con, hyj_certificate member, Jyh_PerMember pERNO) {
      PreparedStatement pstmt = null;
      int result = 0;
      
      String query = prop.getProperty("certificateinsert");
      
      try {
         pstmt = con.prepareStatement(query);
         pstmt.setString(1, pERNO.getPerNo());
         pstmt.setString(2, member.getCertificatename());
         pstmt.setDate(3, member.getCertificatestart());
         pstmt.setDate(4, member.getCertificateend());
         pstmt.setString(5, member.getCertificateagency());
      
         result = pstmt.executeUpdate();
         
      } catch (SQLException e) {
         e.printStackTrace();
      }finally {
         close(pstmt);
      }
      
      
      return result;
   }

   public Jyh_PerMember oneidsearch(Connection con, Jyh_PerMember member2) {
      PreparedStatement pstmt = null;
      ResultSet rset = null;
      Jyh_PerMember member = null;
      
      String query = prop.getProperty("oneidsearch");
      
      try {
         
         pstmt = con.prepareStatement(query);
         
         pstmt.setString(1, member2.getPerName());
         pstmt.setString(2, member2.getEmail());
         
         rset = pstmt.executeQuery();
         
         if(rset.next()) {
            member = new Jyh_PerMember();
            member.setPerNo(rset.getString("PER_NO"));
            member.setPerId(rset.getString("PER_ID"));
            member.setPerPwd(rset.getString("PER_PWD"));
            member.setPerName(rset.getString("PER_NAME"));
            member.setPhone(rset.getString("PHONE"));
            member.setEmail(rset.getString("EMAIL"));
            member.setEnrollDate(rset.getDate("ENROLL_DATE"));
            member.setModifyDate(rset.getDate("MODIFY_DATE"));
            member.setInfoPeriod(rset.getString("INFO_PERIOD"));
            member.setGender(rset.getString("GENDER"));
            member.setBirthDay(rset.getDate("BIRTHDAY"));
            
            
         }
         
         
      } catch (SQLException e) {
      
         e.printStackTrace();
      } finally {
    	  close(pstmt);
    	  close(rset);
      }

      return member;
   }

   public Jyh_ComMember comoneidsearch(Connection con, Jyh_ComMember requestmember) {
      PreparedStatement pstmt = null;
      ResultSet rset = null;
      Jyh_ComMember member = null;
      
      String query = prop.getProperty("comoneidsearch");
      
      try {
         pstmt = con.prepareStatement(query);
         pstmt.setString(1, requestmember.getComName());
         pstmt.setString(2, requestmember.getEmail());
         
         rset = pstmt.executeQuery();
         
         if(rset.next()) {
            member = new Jyh_ComMember();
            
            member.setComNo(rset.getNString("COM_NO"));
            member.setComId(rset.getString("COM_ID"));
            member.setComPwd(rset.getString("COM_PWD"));
            member.setComName(rset.getString("COM_NAME"));
            member.setPhone(rset.getString("PHONE"));
            member.setEmail(rset.getString("EMAIL"));
            member.setEnrollDate(rset.getDate("ENROLL_DATE"));
            member.setModifyDate(rset.getDate("MODIFY_DATE"));
            member.setInfoPeriod(rset.getString("INFO_PERIOD"));
            member.setComNum(rset.getString("COM_NUM"));
            member.setCompanyName(rset.getString("COM_NAME"));
            member.setComUrl(rset.getString("COM_URL"));
            member.setComAddress(rset.getString("COM_ADDRESS"));
            member.setNumEmp(rset.getString("NUM_EMP"));
            member.setInvest(rset.getString("INVEST"));
            member.setSales(rset.getString("SALES"));
   
         }
         

      } catch (SQLException e) {
         // TODO Auto-generated catch block
         e.printStackTrace();
      }finally {
    	  close(pstmt);
    	  close(rset);
      }
      
 
      
      return member;
   }



       public Jyh_PerMember perpwsearch(Connection con, Jyh_PerMember requestmember) {
    	   
    	   PreparedStatement pstmt = null;
    	   ResultSet rset = null;
    	   Jyh_PerMember member = null;
    	   
    	   String query = prop.getProperty("perpwsearch");
    	   
    	   
    	   try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, requestmember.getPerName());
			pstmt.setString(2, requestmember.getEmail());
			pstmt.setString(3, requestmember.getPerId());
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
	            member = new Jyh_PerMember();
	            
	            member.setPerNo(rset.getString("PER_NO"));
	            member.setPerId(rset.getString("PER_ID"));
	            member.setPerPwd(rset.getString("PER_PWD"));
	            member.setPerName(rset.getString("PER_NAME"));
	            member.setPhone(rset.getString("PHONE"));
	            member.setEmail(rset.getString("EMAIL"));
	            member.setEnrollDate(rset.getDate("ENROLL_DATE"));
	            member.setModifyDate(rset.getDate("MODIFY_DATE"));
	            member.setInfoPeriod(rset.getString("INFO_PERIOD"));
	            member.setGender(rset.getString("GENDER"));
	            member.setBirthDay(rset.getDate("BIRTHDAY"));
				
			}
			System.out.println(member);
    	   } catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
			close(rset);
		}
    	   
    	   
    	   
	return member;
}



	public int perpwupdate(Connection con, Jyh_PerMember member, Jyh_PerMember requestmember) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("perpwupdate");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, requestmember.getPerPwd());
			pstmt.setString(2, member.getPerNo());
			
			result = pstmt.executeUpdate();

			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
		}
		
		
		return result;
	}



	public Jyh_ComMember compwsearch(Connection con, Jyh_ComMember requestmember) {
 	   PreparedStatement pstmt = null;
 	   ResultSet rset = null;
 	   Jyh_ComMember member = null;
 	   
 	  String query = prop.getProperty("compwsearch");
 	  
 	  
 	  try {
		pstmt = con.prepareStatement(query);
		pstmt.setString(1, requestmember.getComName());
		pstmt.setString(2, requestmember.getEmail());
		pstmt.setString(3, requestmember.getComId());
		
		rset = pstmt.executeQuery();
		
		if(rset.next()) {
            member = new Jyh_ComMember();
            
            member.setComNo(rset.getNString("COM_NO"));
            member.setComId(rset.getString("COM_ID"));
            member.setComPwd(rset.getString("COM_PWD"));
            member.setComName(rset.getString("COM_NAME"));
            member.setPhone(rset.getString("PHONE"));
            member.setEmail(rset.getString("EMAIL"));
            member.setEnrollDate(rset.getDate("ENROLL_DATE"));
            member.setModifyDate(rset.getDate("MODIFY_DATE"));
            member.setInfoPeriod(rset.getString("INFO_PERIOD"));
            member.setComNum(rset.getString("COM_NUM"));
            member.setCompanyName(rset.getString("COMPANY_NAME"));
            member.setComUrl(rset.getString("COM_URL"));
            member.setComAddress(rset.getString("COM_ADDRESS"));
            member.setNumEmp(rset.getString("NUM_EMP"));
            member.setInvest(rset.getString("INVEST"));
            member.setSales(rset.getString("SALES"));
			
		}
		System.out.println(member);
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}finally {
		close(pstmt);
		close(rset);
	}
 	return member;
	}



	public int compwupdate(Connection con, Jyh_ComMember member, Jyh_ComMember requestmember) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("compwupdate");
		
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, requestmember.getComPwd());
			pstmt.setString(2, member.getComNo());
			
			result = pstmt.executeUpdate();
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
		}
		
		
		return result;
	}





	public int Previoussearch(Connection con, Jyh_PerMember pERNO) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int result = 0;
		
		String query = prop.getProperty("premembersearch");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, pERNO.getPerNo());
			
			rset = pstmt.executeQuery();
		
			if(rset.next()) {
				result = rset.getInt(1);
			}
			
			
		} catch (SQLException e) {
			
			e.printStackTrace();
		}finally {
			close(pstmt);
			close(rset);
		}
		
		
		
		return result;
	}

	public int preupdate(Connection con, hyj_Previous member, Jyh_PerMember pERNO) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("preupdate");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, member.getPreviousname());
			pstmt.setString(2, member.getPreviousjobname());
			pstmt.setDate(3, member.getPreviousjoindate());
			pstmt.setDate(4, member.getResignationdate());
			pstmt.setString(5, member.getStack());
			pstmt.setString(6, member.getMoney());
			pstmt.setString(7, pERNO.getPerNo());
			
			result = pstmt.executeUpdate();

			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
		}
		
		
		return result;
	}



	public int developmentsearch(Connection con, Jyh_PerMember pERNO) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int result = 0;
		
		String query = prop.getProperty("developmentsearch");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, pERNO.getPerNo());
			
			rset = pstmt.executeQuery();
		
			if(rset.next()) {
				result = rset.getInt(1);
			}
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			close(pstmt);
			close(rset);
		}
		return result;
	}



	public int develupdate(Connection con, hyj_developmentinsert member, Jyh_PerMember pERNO) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("develupdate");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setDate(1, member.getJoindate());
			pstmt.setDate(2, member.getResignationdate());
			pstmt.setString(3, member.getDuserstack());
			pstmt.setString(4, member.getDuserskill());
			pstmt.setString(5, member.getUsergit());
			pstmt.setString(6, pERNO.getPerNo());
			
			result = pstmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			close(pstmt);
		}

		return result;
	}



	public int educationsearch(Connection con, Jyh_PerMember perno) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int result = 0;
		
		String query = prop.getProperty("educationsearch");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, perno.getPerNo());
			
			rset = pstmt.executeQuery();
		
			if(rset.next()) {
				result = rset.getInt(1);
			}
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			close(pstmt);
			close(rset);
		}
		return result;
	}



	public int educationupdate(Connection con, hyj_education member, Jyh_PerMember perno) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("educationpdate");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, member.getEducationname());
			pstmt.setDate(2, member.getEducationstartday());
			pstmt.setDate(3, member.getEducationlastday());
			pstmt.setString(4, member.getEducationstack());
			pstmt.setString(5, member.getEducationskil());
			pstmt.setString(6, perno.getPerNo());
			
			result = pstmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			close(pstmt);
		}

		return result;
	}



	public int volunteerserch(Connection con, Jyh_PerMember perno) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int result = 0;
		
		String query = prop.getProperty("volunteerserch");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, perno.getPerNo());
			
			rset = pstmt.executeQuery();
		
			if(rset.next()) {
				result = rset.getInt(1);
			}
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			close(pstmt);
			close(rset);
		}
		return result;
	}



	public int volunteerupdate(Connection con, hyj_volunteer member, Jyh_PerMember perno) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("volunteerupdate");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, member.getVolunteername());
			pstmt.setDate(2, member.getVolunteerStartDate());
			pstmt.setDate(3, member.getVolunteerendDate());
			pstmt.setString(4, member.getVolunteerlink());
			pstmt.setString(5, member.getVolunteerExplanation());
			pstmt.setString(6, perno.getPerNo());
			
			result = pstmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			close(pstmt);
		}

		return result;
	}



	public int awardsserch(Connection con, Jyh_PerMember perno) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int result = 0;
		
		String query = prop.getProperty("awardsserch");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, perno.getPerNo());
			
			rset = pstmt.executeQuery();
		
			if(rset.next()) {
				result = rset.getInt(1);
			}
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			close(pstmt);
			close(rset);
		}
		return result;
	}



	public int awardsupdate(Connection con, hyj_awards member, Jyh_PerMember perno) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("awardsupdate");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, member.getAwardsname());
			pstmt.setString(2, member.getAwardscontent());
			pstmt.setString(3, member.getAwardsagency());
			pstmt.setDate(4, member.getAwardsmonth());
			pstmt.setString(5, perno.getPerNo());
			
			result = pstmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			close(pstmt);
		}

		return result;
	}



	public int certificateserch(Connection con, Jyh_PerMember pERNO) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int result = 0;
		
		String query = prop.getProperty("certificateserch");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, pERNO.getPerNo());
			
			rset = pstmt.executeQuery();
		
			if(rset.next()) {
				result = rset.getInt(1);
			}
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			close(pstmt);
			close(rset);
		}
		return result;
	}



	public int certificateupdate(Connection con, hyj_certificate member, Jyh_PerMember pERNO) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("certificateupdate");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, member.getCertificatename());
			pstmt.setDate(2, member.getCertificatestart());
			pstmt.setDate(3, member.getCertificateend());
			pstmt.setString(4, member.getCertificateagency());
			pstmt.setString(5, pERNO.getPerNo());
			
			result = pstmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			close(pstmt);
		}

		return result;
	}



	public int schoolserch(Connection con, Jyh_PerMember perno) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int result = 0;
		
		String query = prop.getProperty("schoolserch");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, perno.getPerNo());
			
			rset = pstmt.executeQuery();
		
			if(rset.next()) {
				result = rset.getInt(1);
			}
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			close(pstmt);
			close(rset);
		}
		return result;
	}



	public int schoolupdate(Connection con, hyj_schoolcareer member, Jyh_PerMember perno) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("schoolupdate");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, member.getSchoolname());
			pstmt.setDate(2, member.getSchoolenrolldate());
			pstmt.setDate(3, member.getSchoolgraduatedate());
			pstmt.setString(4, member.getSchoolmajor());
			pstmt.setString(5, member.getSchoolmypoint());
			pstmt.setString(6, member.getSchoolgradepoint());
			pstmt.setString(7, member.getSchooletc());
			pstmt.setString(8, member.getSchoolexplanation());
			pstmt.setString(9, perno.getPerNo());
			
			result = pstmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			close(pstmt);
		}

		return result;
	}



	public int schoolinsert(Connection con, hyj_schoolcareer member, Jyh_PerMember perno) {
	      PreparedStatement pstmt = null;
	      int result = 0;
	      
	      String query = prop.getProperty("schoolinsert");
	      
	      try {
	         pstmt = con.prepareStatement(query);
	         pstmt.setString(1, member.getSchoolname());
	         pstmt.setDate(2, member.getSchoolenrolldate());
	         pstmt.setDate(3, member.getSchoolgraduatedate());
	         pstmt.setString(4, member.getSchoolmajor());
	         pstmt.setString(5, member.getSchoolmypoint());
	         pstmt.setString(6, member.getSchoolgradepoint());
	         pstmt.setString(7, perno.getPerNo());
	         pstmt.setString(8, member.getSchooletc());
	         pstmt.setString(9, member.getSchoolexplanation());
	         result = pstmt.executeUpdate();

	      } catch (SQLException e) {
	         e.printStackTrace();
	      }finally{
	         close(pstmt);
	      }
	      
	      return result;
	}



	public int insertThumbnailContent(Connection con, Jyh_PerMember member) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("insertThumb");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, member.getPerNo());
			

			result = pstmt.executeUpdate();

		} catch (SQLException e) {
	
			e.printStackTrace();
		}finally {
			close(pstmt);
		}

		return result;
	}



	public int selectCurrval(Connection con) {
		Statement stmt = null;
		ResultSet rset = null;
		int pno = 0;
		
		String query = prop.getProperty("selectCurrval");
		
		try {
			stmt = con.createStatement();
			rset = stmt.executeQuery(query);
			
			if(rset.next()) {
				pno = rset.getInt("currval");
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(stmt);
			close(rset);
		}
		
		return pno;
	}



	public int insertAttachment(Connection con, Jyh_PerMember member, hyj_Attachment hyj_Attachment) {
		PreparedStatement pstmt = null;
		int result = 0;
		String query = prop.getProperty("insertAttachment");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, member.getPerNo());
			pstmt.setString(2, hyj_Attachment.getOriginName());
			pstmt.setString(3, hyj_Attachment.getChangeName());
			pstmt.setString(4, hyj_Attachment.getFilePath());
			pstmt.setDate(5, hyj_Attachment.getUploadDate());
			
			result = pstmt.executeUpdate();
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
		}

		return result;
	}



	public Jyh_PerMember onephidsearch(Connection con, Jyh_PerMember requestmember) {
		 PreparedStatement pstmt = null;
	      ResultSet rset = null;
	      Jyh_PerMember member = null;
	      
	      String query = prop.getProperty("onephidsearch");
	      
	      try {
	         
	         pstmt = con.prepareStatement(query);
	         
	         pstmt.setString(1, requestmember.getPerName());
	         pstmt.setString(2, requestmember.getPhone());
	         
	         rset = pstmt.executeQuery();
	         
	         if(rset.next()) {
	            member = new Jyh_PerMember();
	            member.setPerNo(rset.getString("PER_NO"));
	            member.setPerId(rset.getString("PER_ID"));
	            member.setPerPwd(rset.getString("PER_PWD"));
	            member.setPerName(rset.getString("PER_NAME"));
	            member.setPhone(rset.getString("PHONE"));
	            member.setEmail(rset.getString("EMAIL"));
	            member.setEnrollDate(rset.getDate("ENROLL_DATE"));
	            member.setModifyDate(rset.getDate("MODIFY_DATE"));
	            member.setInfoPeriod(rset.getString("INFO_PERIOD"));
	            member.setGender(rset.getString("GENDER"));
	            member.setBirthDay(rset.getDate("BIRTHDAY"));
	            
	            
	         }
	         
	        
	      } catch (SQLException e) {
	      
	         e.printStackTrace();
	      } finally {
	    	  close(pstmt);
	    	  close(rset);
	      }

	      return member;
	}



	public Jyh_ComMember comphidsearch(Connection con, Jyh_ComMember requestmember) {
		  PreparedStatement pstmt = null;
	      ResultSet rset = null;
	      Jyh_ComMember member = null;
	      
	      String query = prop.getProperty("comphidsearch");
	      
	      try {
	         
	         pstmt = con.prepareStatement(query);
	         
	         pstmt.setString(1, requestmember.getComName());
	         pstmt.setString(2, requestmember.getPhone());
	         
	         rset = pstmt.executeQuery();
	         
	         if(rset.next()) {
	             member = new Jyh_ComMember();
	             
	             member.setComNo(rset.getNString("COM_NO"));
	             member.setComId(rset.getString("COM_ID"));
	             member.setComPwd(rset.getString("COM_PWD"));
	             member.setComName(rset.getString("COM_NAME"));
	             member.setPhone(rset.getString("PHONE"));
	             member.setEmail(rset.getString("EMAIL"));
	             member.setEnrollDate(rset.getDate("ENROLL_DATE"));
	             member.setModifyDate(rset.getDate("MODIFY_DATE"));
	             member.setInfoPeriod(rset.getString("INFO_PERIOD"));
	             member.setComNum(rset.getString("COM_NUM"));
	             member.setCompanyName(rset.getString("COMPANY_NAME"));
	             member.setComUrl(rset.getString("COM_URL"));
	             member.setComAddress(rset.getString("COM_ADDRESS"));
	             member.setNumEmp(rset.getString("NUM_EMP"));
	             member.setInvest(rset.getString("INVEST"));
	             member.setSales(rset.getString("SALES"));
	            
	            
	         }
	         
	        
	      } catch (SQLException e) {
	      
	         e.printStackTrace();
	      } finally {
	    	  close(pstmt);
	    	  close(rset);
	      }

	      return member;
	}



	public Jyh_PerMember perphpwsearch(Connection con, Jyh_PerMember requestmember) {
		   PreparedStatement pstmt = null;
    	   ResultSet rset = null;
    	   Jyh_PerMember member = null;
    	   
    	   String query = prop.getProperty("perphpwsearch");
    	   
    	   
    	   try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, requestmember.getPerName());
			pstmt.setString(2, requestmember.getPhone());
			pstmt.setString(3, requestmember.getPerId());
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
	            member = new Jyh_PerMember();
	            
	            member.setPerNo(rset.getString("PER_NO"));
	            member.setPerId(rset.getString("PER_ID"));
	            member.setPerPwd(rset.getString("PER_PWD"));
	            member.setPerName(rset.getString("PER_NAME"));
	            member.setPhone(rset.getString("PHONE"));
	            member.setEmail(rset.getString("EMAIL"));
	            member.setEnrollDate(rset.getDate("ENROLL_DATE"));
	            member.setModifyDate(rset.getDate("MODIFY_DATE"));
	            member.setInfoPeriod(rset.getString("INFO_PERIOD"));
	            member.setGender(rset.getString("GENDER"));
	            member.setBirthDay(rset.getDate("BIRTHDAY"));
				
			}
			System.out.println(member);
    	   } catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
			close(rset);
		}
    	   
    	   
    	   
	return member;
	}



	public int perphpwupdate(Connection con, Jyh_PerMember member, Jyh_PerMember requestmember) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("perphpwupdate");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, requestmember.getPerPwd());
			pstmt.setString(2, member.getPerNo());
			
			result = pstmt.executeUpdate();

			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
		}
		
		
		return result;
	}



	public Jyh_ComMember comphpwsearch(Connection con, Jyh_ComMember requestmember) {
		   PreparedStatement pstmt = null;
	 	   ResultSet rset = null;
	 	   Jyh_ComMember member = null;
	 	   
	 	  String query = prop.getProperty("comphpwsearch");
	 	  
	 	  
	 	  try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, requestmember.getComName());
			pstmt.setString(2, requestmember.getPhone());
			pstmt.setString(3, requestmember.getComId());
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
	            member = new Jyh_ComMember();
	            
	            member.setComNo(rset.getNString("COM_NO"));
	            member.setComId(rset.getString("COM_ID"));
	            member.setComPwd(rset.getString("COM_PWD"));
	            member.setComName(rset.getString("COM_NAME"));
	            member.setPhone(rset.getString("PHONE"));
	            member.setEmail(rset.getString("EMAIL"));
	            member.setEnrollDate(rset.getDate("ENROLL_DATE"));
	            member.setModifyDate(rset.getDate("MODIFY_DATE"));
	            member.setInfoPeriod(rset.getString("INFO_PERIOD"));
	            member.setComNum(rset.getString("COM_NUM"));
	            member.setCompanyName(rset.getString("COMPANY_NAME"));
	            member.setComUrl(rset.getString("COM_URL"));
	            member.setComAddress(rset.getString("COM_ADDRESS"));
	            member.setNumEmp(rset.getString("NUM_EMP"));
	            member.setInvest(rset.getString("INVEST"));
	            member.setSales(rset.getString("SALES"));
				
			}
			System.out.println(member);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
			close(rset);
		}
	 	return member;
	}



	public int comphpwupdate(Connection con, Jyh_ComMember member, Jyh_ComMember requestmember) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("comphpwupdate");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, requestmember.getComPwd());
			pstmt.setString(2, member.getComNo());
			
			result = pstmt.executeUpdate();

			
		} catch (SQLException e) {
		
			e.printStackTrace();
		}finally {
			close(pstmt);
		}
		
		
		return result;
	}



	public hyj_Previous moreinformation(Connection con, String userno, hyj_Previous preoinfor1) {
		 PreparedStatement pstmt = null;
         ResultSet rset = null;
         hyj_Previous member = null;
         
         String query = prop.getProperty("selectPrevious");
         
         
         try {
            pstmt = con.prepareStatement(query);
            pstmt.setString(1, userno);
            
            rset = pstmt.executeQuery();

            if(rset.next()) {
            	
            member=new hyj_Previous();
            member.setPreviousNo(rset.getString("CE_NO"));
            member.setPreviousname(rset.getString("CE_PERVIOUSCOMPANY"));
            member.setPreviousjobname(rset.getString("CE_OCCUPATION"));
            member.setPreviousjoindate(rset.getDate("CE_JOINDATE"));
            member.setResignationdate(rset.getDate("CE_LASTDATE"));
            member.setStack(rset.getString("CE_STACK"));
            member.setMoney(rset.getString("CE_MONEY"));
            
            }

            System.out.println(member);
         } catch (SQLException e) {
          
            e.printStackTrace();
         }finally {
            close(pstmt);
            close(rset);
         }
         return member;
	}



	public ArrayList<HashMap<String, Object>> Previous(Connection con, String userno,
			ArrayList<HashMap<String, Object>> inforlist) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		hyj_Previous Previous = null;
		HashMap<String, Object> inforlist1 = null;
		
		String query = prop.getProperty("selectPrevious");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, userno);
			rset = pstmt.executeQuery();
			int i = 0;
			inforlist1 = new HashMap<String, Object>();
			
			if(rset.next()) {
				Previous = new hyj_Previous();
				
				Previous.setPreviousNo(rset.getString("CE_NO"));
				Previous.setPreviousname(rset.getString("CE_PERVIOUSCOMPANY"));
				Previous.setPreviousjobname(rset.getString("CE_OCCUPATION"));
				Previous.setPreviousjoindate(rset.getDate("CE_JOINDATE"));
				Previous.setResignationdate(rset.getDate("CE_LASTDATE"));
				Previous.setStack(rset.getString("CE_STACK"));
				Previous.setMoney(rset.getString("CE_MONEY"));
				
				inforlist1.put("companyCareer"+i, Previous);
				i++;
			}
			
			inforlist.add(inforlist1);
			
		} catch (SQLException e) {
			
			e.printStackTrace();
		}finally {
			close(pstmt);
			close(rset);
		}
		
		
		
		return inforlist;
	}



	public ArrayList<HashMap<String, Object>> companycareer(Connection con, String userno,
			ArrayList<HashMap<String, Object>> information) {
		 PreparedStatement pstmt = null;
         ResultSet rset = null;
         //어레이리스트에 각각의 커리어를 저장하고 그 리스트를 hashMap담아줌
         HashMap <String,Object> resumeDetail1 = null;
         hyj_Previous companyCareer = null;
         String query_compayCareer = prop.getProperty("selectPrevious");
         
         try {
			pstmt=con.prepareStatement(query_compayCareer);
			pstmt.setString(1, userno);
			resumeDetail1=new HashMap<String,Object>();
			rset=pstmt.executeQuery();
			int i = 0;
			
			if(rset.next()) {
				companyCareer=new hyj_Previous();
				
				companyCareer.setPreviousNo(rset.getString("CE_NO"));
				companyCareer.setPreviousname(rset.getString("CE_PERVIOUSCOMPANY"));
				companyCareer.setPreviousjobname(rset.getString("CE_OCCUPATION"));
				companyCareer.setPreviousjoindate(rset.getDate("CE_JOINDATE"));
				companyCareer.setResignationdate(rset.getDate("CE_LASTDATE"));
				companyCareer.setStack(rset.getString("CE_STACK"));
				companyCareer.setMoney(rset.getString("CE_MONEY"));
				
				resumeDetail1.put("companyCareer"+i, companyCareer);
				i++;
				
				information.add(resumeDetail1);
			}
			
			
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
			close(rset);
		}
         

		return information;
	}



	public ArrayList<HashMap<String, Object>> educarrer(Connection con, String userno,
			ArrayList<HashMap<String, Object>> information) {
		PreparedStatement pstmt = null;
        ResultSet rset = null;
        hyj_education eduCareer=null;
        String query_eduCareer=prop.getProperty("selectEduCareer");
        HashMap <String,Object> resumeDetail1= null;
        
        
        try {
			pstmt=con.prepareStatement(query_eduCareer);
			pstmt.setString(1, userno);
			resumeDetail1=new HashMap<String,Object>();
			rset=pstmt.executeQuery();
			int i = 0;
			
			if(rset.next()) {
				eduCareer=new hyj_education();
				
				eduCareer.setEducationNo(rset.getString("CA_NO"));
				eduCareer.setEducationname(rset.getString("CA_NAME"));
				eduCareer.setEducationstartday(rset.getDate("CA_STARTDATE"));
				eduCareer.setEducationlastday(rset.getDate("CA_ENDDATE"));
				eduCareer.setEducationstack(rset.getString("CA_STACK"));
				eduCareer.setEducationskil(rset.getString("CA_SKILL"));
				resumeDetail1.put("eduCareer"+i, eduCareer);
				i++;
			}
				
			information.add(resumeDetail1);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}finally {
				close(pstmt);
				close(rset);
			}
	         

			return information;
	}



	public ArrayList<HashMap<String, Object>> DevelCareer(Connection con, String userno,
			ArrayList<HashMap<String, Object>> information) {
		 PreparedStatement pstmt = null;
         ResultSet rset = null;
		 hyj_developmentinsert develCareer=null; //개발경력
		 HashMap <String,Object> resumeDetail1= null;
		 String query_develCareer=prop.getProperty("selectDevelCareer");//?(PERNO)

		 try {
			pstmt=con.prepareStatement(query_develCareer);
			pstmt.setString(1, userno);
			resumeDetail1=new HashMap<String,Object>();
			rset=pstmt.executeQuery();
			int i = 0;
			
			if(rset.next()) {
				develCareer=new hyj_developmentinsert();
				
				develCareer.setDevelNo(rset.getString("DP_NO"));
				develCareer.setJoindate(rset.getDate("DP_STARTDAY"));
				develCareer.setResignationdate(rset.getDate("DP_LASTDAY"));
				develCareer.setDuserstack(rset.getString("DP_CHARGE"));
				develCareer.setDuserskill(rset.getString("DP_SKILL"));
				develCareer.setUsergit(rset.getString("DP_GITURL"));
				
				resumeDetail1.put("DevelCareer"+i, develCareer);
				i++;
			}
			
			information.add(resumeDetail1);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
			close(rset);
		}
         

		return information;
	}



	public ArrayList<HashMap<String, Object>> Award(Connection con, String userno,
			ArrayList<HashMap<String, Object>> information) {
		 PreparedStatement pstmt = null;
         ResultSet rset = null;
         hyj_awards award=null;//수상경력
         HashMap <String,Object> resumeDetail1= null;
         
		 String query_award=prop.getProperty("selectAward");

		 try {
			pstmt=con.prepareStatement(query_award);
			
			pstmt.setString(1, userno);
			resumeDetail1=new HashMap<String,Object>();
			rset=pstmt.executeQuery();
			int i = 0;
			
			if(rset.next()) {
				award=new hyj_awards();
				
				award.setAwardNo(rset.getNString("A_NO"));
				award.setAwardsname(rset.getString("A_NAME"));
				award.setAwardscontent(rset.getString("A_CONTENT"));
				award.setAwardsmonth(rset.getDate("A_DATE"));
				award.setAwardsagency(rset.getString("A_AGENCY"));
				
				resumeDetail1.put("award"+i, award);
				i++;
			}
		
			information.add(resumeDetail1);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
			close(rset);
		}
         

		return information;
	}



	public ArrayList<HashMap<String, Object>> school(Connection con, String userno,
			ArrayList<HashMap<String, Object>> information) {
		PreparedStatement pstmt = null;
        ResultSet rset = null;
        hyj_schoolcareer school=null;//학력

        HashMap <String,Object> resumeDetail1= null;
		 String query_schoolCareer=prop.getProperty("selectSchoolCareer");
		 
		 try {
			pstmt=con.prepareStatement(query_schoolCareer);
			pstmt.setString(1, userno);
			resumeDetail1=new HashMap<String,Object>();
			rset=pstmt.executeQuery();
			int i = 0;
			
			if(rset.next()) {
				school=new hyj_schoolcareer();
				
				school.setSchoolno(rset.getString("SC_NO"));
				school.setSchoolname(rset.getString("SC_NAME"));
				school.setSchoolenrolldate(rset.getDate("SC_ENROLL_DATE"));
				school.setSchoolgraduatedate(rset.getDate("SC_GRADUATE_DATE"));
				school.setSchoolmajor(rset.getString("SC_MAJOR"));
				school.setSchooletc(rset.getString("SC_ETC"));//학위
				school.setSchoolmypoint(rset.getString("SC_MYGRADEPOINT"));
				school.setSchoolgradepoint(rset.getString("SC_GRADEPOINT"));
				school.setSchoolexplanation(rset.getString("SC_EXPLANATION"));
				
				resumeDetail1.put("school"+i, school);
				System.out.println("school dao:"+school);
				i++;
			}
			information.add(resumeDetail1);
			System.out.println("school resumeDetail:"+resumeDetail1);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
			close(rset);
		}
        

		return information;
	}



	public ArrayList<HashMap<String, Object>> volunte(Connection con, String userno,
			ArrayList<HashMap<String, Object>> information) {
		 PreparedStatement pstmt = null;
         ResultSet rset = null;
         hyj_volunteer volunte=null;//대외활동
         HashMap <String,Object> resumeDetail1= null;
         String query_volunte=prop.getProperty("selectVolunte");
         
         try {
			pstmt=con.prepareStatement(query_volunte);
			pstmt.setString(1, userno);
			rset=pstmt.executeQuery();
			resumeDetail1=new HashMap<String,Object>();
			int i = 0;
			
			if(rset.next()) {
				volunte = new hyj_volunteer();
				volunte.setVolunteerNo(rset.getString("SA_NO"));
				volunte.setVolunteername(rset.getString("SA_NAME"));
				volunte.setVolunteerStartDate(rset.getDate("SA_STARTDATE"));
				volunte.setVolunteerendDate(rset.getDate("SA_ENDDATE"));
				volunte.setVolunteerlink(rset.getString("SA_LINK"));
				volunte.setVolunteerExplanation(rset.getString("SA_EXPLAIN"));
				resumeDetail1.put("volunte"+i, volunte);
				i++;
			}
			information.add(resumeDetail1);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
			close(rset);
		}
         

		return information;
	}



	public ArrayList<HashMap<String, Object>> License(Connection con, String userno,
			ArrayList<HashMap<String, Object>> information) {
		PreparedStatement pstmt = null;
        ResultSet rset = null;
        hyj_certificate license=null;//자격증
        HashMap <String,Object> resumeDetail1= null;
        String query_volunte=prop.getProperty("selectLicense");//?(PERNO)
        
        try {
			pstmt=con.prepareStatement(query_volunte);
			pstmt.setString(1, userno);
			rset=pstmt.executeQuery();
			resumeDetail1=new HashMap<String,Object>();
			int i = 0;
			if(rset.next()) {
				license=new hyj_certificate();
				license.setCertificateNo(rset.getString("LI_NO"));
				license.setCertificatename(rset.getString("LI_NAME"));
				license.setCertificatestart(rset.getDate("LI_GETDATE"));
				license.setCertificateend(rset.getDate("LI_EXPIREDATE"));
				license.setCertificateagency(rset.getString("LI_ORGANIZATION"));
				resumeDetail1.put("license"+i, license);
				i++;
				
			}
			information.add(resumeDetail1);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
			close(rset);
		}
        

		return information;
	}



	public Jyh_PerMember personpwsearch(Connection con, Jyh_PerMember requestmember) {
		PreparedStatement pstmt = null;
  	   ResultSet rset = null;
  	   Jyh_PerMember member = null;
  	   
  	   
  	   String query = prop.getProperty("perpwsearch");
  	   
  	   
  	   try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, requestmember.getPerName());
			pstmt.setString(2, requestmember.getEmail());
			pstmt.setString(3, requestmember.getPerId());
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
	            member = new Jyh_PerMember();
	            
	            member.setPerNo(rset.getString("PER_NO"));
	            member.setPerId(rset.getString("PER_ID"));
	            member.setPerPwd(rset.getString("PER_PWD"));
	            member.setPerName(rset.getString("PER_NAME"));
	            member.setPhone(rset.getString("PHONE"));
	            member.setEmail(rset.getString("EMAIL"));
	            member.setEnrollDate(rset.getDate("ENROLL_DATE"));
	            member.setModifyDate(rset.getDate("MODIFY_DATE"));
	            member.setInfoPeriod(rset.getString("INFO_PERIOD"));
	            member.setGender(rset.getString("GENDER"));
	            member.setBirthDay(rset.getDate("BIRTHDAY"));
				
	         
			}
			System.out.println(member);
  	   } catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
			close(rset);
		}
  	   
  	   
  	   
	return member;
	}



	public Jyh_PerMember personpwphsearch(Connection con, Jyh_PerMember requestmember) {
		 PreparedStatement pstmt = null;
	  	   ResultSet rset = null;
	  	   Jyh_PerMember member = null;
	  	   
	  	   
	  	   String query = prop.getProperty("perphpwsearch");
	  	   
	  	   
	  	   try {
				pstmt = con.prepareStatement(query);
				pstmt.setString(1, requestmember.getPerName());
				pstmt.setString(2, requestmember.getPhone());
				pstmt.setString(3, requestmember.getPerId());
				
				rset = pstmt.executeQuery();
				
				if(rset.next()) {
		            member = new Jyh_PerMember();
		            
		            member.setPerNo(rset.getString("PER_NO"));
		            member.setPerId(rset.getString("PER_ID"));
		            member.setPerPwd(rset.getString("PER_PWD"));
		            member.setPerName(rset.getString("PER_NAME"));
		            member.setPhone(rset.getString("PHONE"));
		            member.setEmail(rset.getString("EMAIL"));
		            member.setEnrollDate(rset.getDate("ENROLL_DATE"));
		            member.setModifyDate(rset.getDate("MODIFY_DATE"));
		            member.setInfoPeriod(rset.getString("INFO_PERIOD"));
		            member.setGender(rset.getString("GENDER"));
		            member.setBirthDay(rset.getDate("BIRTHDAY"));
					
		         
				}
				System.out.println(member);
	  	   } catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}finally {
				close(pstmt);
				close(rset);
			}
	  	   
	  	   
	  	   
		return member;
	}



	public Jyh_ComMember companypwsearch(Connection con, Jyh_ComMember requestmember) {
		   PreparedStatement pstmt = null;
	 	   ResultSet rset = null;
	 	   Jyh_ComMember member = null;
	 	   
	 	  String query = prop.getProperty("compwsearch");

	 	  try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, requestmember.getComName());
			pstmt.setString(2, requestmember.getEmail());
			pstmt.setString(3, requestmember.getComId());
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
	            member = new Jyh_ComMember();
	            
	            member.setComNo(rset.getNString("COM_NO"));
	            member.setComId(rset.getString("COM_ID"));
	            member.setComPwd(rset.getString("COM_PWD"));
	            member.setComName(rset.getString("COM_NAME"));
	            member.setPhone(rset.getString("PHONE"));
	            member.setEmail(rset.getString("EMAIL"));
	            member.setEnrollDate(rset.getDate("ENROLL_DATE"));
	            member.setModifyDate(rset.getDate("MODIFY_DATE"));
	            member.setInfoPeriod(rset.getString("INFO_PERIOD"));
	            member.setComNum(rset.getString("COM_NUM"));
	            member.setCompanyName(rset.getString("COMPANY_NAME"));
	            member.setComUrl(rset.getString("COM_URL"));
	            member.setComAddress(rset.getString("COM_ADDRESS"));
	            member.setNumEmp(rset.getString("NUM_EMP"));
	            member.setInvest(rset.getString("INVEST"));
	            member.setSales(rset.getString("SALES"));
				
			}
			System.out.println(member);
		} catch (SQLException e) {

			e.printStackTrace();
		}finally {
			close(pstmt);
			close(rset);
		}
	 	return member;
	}



	public Jyh_ComMember companyphpwsearch(Connection con, Jyh_ComMember requestmember) {
		PreparedStatement pstmt = null;
	 	   ResultSet rset = null;
	 	   Jyh_ComMember member = null;
	 	   
	 	  String query = prop.getProperty("comphpwsearch");

	 	  try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, requestmember.getComName());
			pstmt.setString(2, requestmember.getPhone());
			pstmt.setString(3, requestmember.getComId());
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
	            member = new Jyh_ComMember();
	            
	            member.setComNo(rset.getNString("COM_NO"));
	            member.setComId(rset.getString("COM_ID"));
	            member.setComPwd(rset.getString("COM_PWD"));
	            member.setComName(rset.getString("COM_NAME"));
	            member.setPhone(rset.getString("PHONE"));
	            member.setEmail(rset.getString("EMAIL"));
	            member.setEnrollDate(rset.getDate("ENROLL_DATE"));
	            member.setModifyDate(rset.getDate("MODIFY_DATE"));
	            member.setInfoPeriod(rset.getString("INFO_PERIOD"));
	            member.setComNum(rset.getString("COM_NUM"));
	            member.setCompanyName(rset.getString("COMPANY_NAME"));
	            member.setComUrl(rset.getString("COM_URL"));
	            member.setComAddress(rset.getString("COM_ADDRESS"));
	            member.setNumEmp(rset.getString("NUM_EMP"));
	            member.setInvest(rset.getString("INVEST"));
	            member.setSales(rset.getString("SALES"));
				  
	            
			}
			System.out.println(member);
		} catch (SQLException e) {

			e.printStackTrace();
		}finally {
			close(pstmt);
			close(rset);
		}
	 	return member;
	}



	public ArrayList<HashMap<String, Object>> personuser(Connection con, String userno) {
		 PreparedStatement pstmt = null;
         ResultSet rset = null;
         Jyh_PerMember member = null;
         ArrayList <HashMap<String,Object>> resumeDetail=null; 
         HashMap <String,Object> resumeDetail1= null;
         
         String query = prop.getProperty("selectMember");
         
         
         try {
            pstmt = con.prepareStatement(query);
            pstmt.setString(1, userno);
            rset = pstmt.executeQuery();
            resumeDetail1=new HashMap<String,Object>();
            resumeDetail=new ArrayList <HashMap<String,Object>>();
            if(rset.next()) {
            	member=new Jyh_PerMember();
            member.setPerNo(rset.getString("PER_NO"));
            member.setPerId(rset.getString("PER_ID"));
            member.setPerPwd(rset.getString("PER_PWD"));
            member.setPerName(rset.getString("PER_NAME"));
            member.setPhone(rset.getString("PHONE"));
            member.setEmail(rset.getString("EMAIL"));
            member.setEnrollDate(rset.getDate("ENROLL_DATE"));
            member.setModifyDate(rset.getDate("MODIFY_DATE"));
            member.setInfoPeriod(rset.getString("INFO_PERIOD"));
            member.setGender(rset.getString("GENDER"));
            member.setBirthDay(rset.getDate("BIRTHDAY"));
            
            resumeDetail1.put("member", member);
            }
            resumeDetail.add(resumeDetail1);
            System.out.println("이력서 작성전에 멤버 값 받아오기DAO:"+resumeDetail);
            
         } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
         }finally {
            close(pstmt);
            close(rset);
         }
         return resumeDetail;
	}






	public String skillsearch(Connection con, String skill) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		String skillcode = null;
		
		String query = prop.getProperty("checkboxselect");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, skill);
			rset = pstmt.executeQuery();
			if(rset.next()) {
				skillcode = rset.getString(1);
			}
			

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(rset);
			close(pstmt);
		}
		
		
		return skillcode;
	}



	public int skillinsert(Connection con, String skillcode, String userno) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("checkboxinsert");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, userno);
			pstmt.setString(2, skillcode);
			
			result = pstmt.executeUpdate();
			

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
		}
		
		
		return result;
	}



	public int perskillsearch(Connection con, String skillcode, String userno) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int result = 0;
		
		String query = prop.getProperty("perskillsearch");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, userno);
			pstmt.setString(2, skillcode);
			
			rset = pstmt.executeQuery();
			if(rset.next()) {
				result = rset.getInt(1);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
			close(rset);
		}
		
		return result;
	}



	public int skilldelete(Connection con, String skillcode, String userno) {
		PreparedStatement pstmt = null;
		
		int result = 0;
		
		String query = prop.getProperty("perskilldelete");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, userno);
			pstmt.setString(2, skillcode);
			
			result = pstmt.executeUpdate();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
			
		}
		
		return result;
	}



	public int cominfomationupdate(Connection con, Jyh_ComMember member) {
		   PreparedStatement pstmt = null;
		     int result = 0;
		      
		      String query = prop.getProperty("cominformationupdate");
		      
		      try {
		         pstmt = con.prepareStatement(query);
		         pstmt.setString(1, member.getComName());
		         pstmt.setString(2, member.getPhone());
		         pstmt.setString(3, member.getEmail());
		         pstmt.setString(4, member.getCompanyName());
		         pstmt.setString(5, member.getComUrl());
		         pstmt.setString(6, member.getNumEmp());
		         pstmt.setString(7, member.getInvest());
		         pstmt.setString(8, member.getSales());
		         pstmt.setString(9, member.getComNo());
		     
		         result = pstmt.executeUpdate();

		      } catch (SQLException e) {
		      
		         e.printStackTrace();
		      }finally{
		         close(pstmt);
		      }
		      
		      return result;
	}



	public ArrayList<HashMap<String, Object>> searchmember(Connection con, String userno) {
		   PreparedStatement pstmt = null;
	         ResultSet rset = null;
	         Jyh_PerMember member = null;
	         ArrayList <HashMap<String,Object>> resumeDetail=null; 
	         HashMap <String,Object> resumeDetail1= null;
	         
	         String query = prop.getProperty("selectMember");
	         
	         
	         try {
	            pstmt = con.prepareStatement(query);
	            pstmt.setString(1, userno);
	            rset = pstmt.executeQuery();
	            resumeDetail1=new HashMap<String,Object>();
	            resumeDetail=new ArrayList <HashMap<String,Object>>();
	            if(rset.next()) {
	            	member=new Jyh_PerMember();
	            member.setPerNo(rset.getString("PER_NO"));
	            member.setPerId(rset.getString("PER_ID"));
	            member.setPerPwd(rset.getString("PER_PWD"));
	            member.setPerName(rset.getString("PER_NAME"));
	            member.setPhone(rset.getString("PHONE"));
	            member.setEmail(rset.getString("EMAIL"));
	            member.setEnrollDate(rset.getDate("ENROLL_DATE"));
	            member.setModifyDate(rset.getDate("MODIFY_DATE"));
	            member.setInfoPeriod(rset.getString("INFO_PERIOD"));
	            member.setGender(rset.getString("GENDER"));
	            member.setBirthDay(rset.getDate("BIRTHDAY"));
	            
	            resumeDetail1.put("member", member);
	            }
	            resumeDetail.add(resumeDetail1);
	            System.out.println("이력서 작성전에 멤버 값 받아오기DAO:"+resumeDetail);
	            
	         } catch (SQLException e) {
	            // TODO Auto-generated catch block
	            e.printStackTrace();
	         }finally {
	            close(pstmt);
	            close(rset);
	         }
	         return resumeDetail;
	}
}