package com.kh.itda.member.model.dao;

import static com.kh.itda.common.JDBCTemplate.close;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;

import com.kh.itda.approval.model.vo.PageInfo;
import com.kh.itda.member.model.vo.Jyh_ComMember;
import com.kh.itda.member.model.vo.Jyh_PerMember;

public class Jyh_MemberDao {
	
	private Properties prop = new Properties();
	
	String fileName = Jyh_MemberDao.class.getResource("/sql/member/jyh-member-query.properties").getPath();
	public Jyh_MemberDao() {
		
		try {
			prop.load(new FileReader(fileName));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public Jyh_PerMember Per_LoginCheck(Connection con, Jyh_PerMember requestMember) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		Jyh_PerMember loginUser = null;
		
		String query = prop.getProperty("loginSelectPer"); //18번 끝
		
		//20번 시작
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, requestMember.getPerId());
			pstmt.setString(2, requestMember.getPerPwd());
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				loginUser = new Jyh_PerMember();
				
				loginUser.setPerNo(rset.getString("PER_NO"));
				loginUser.setPerId(rset.getString("PER_ID"));
				loginUser.setPerPwd(rset.getString("PER_PWD"));
				loginUser.setPerName(rset.getString("PER_NAME"));
				loginUser.setPhone(rset.getString("PHONE"));
				loginUser.setEmail(rset.getString("EMAIL"));
				loginUser.setEnrollDate(rset.getDate("ENROLL_DATE"));
				loginUser.setModifyDate(rset.getDate("MODIFY_DATE"));
				loginUser.setInfoPeriod(rset.getString("INFO_PERIOD"));
				loginUser.setGender(rset.getString("GENDER"));
	            loginUser.setBirthDay(rset.getDate("BIRTHDAY"));
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(rset);
			close(pstmt);
		}
		
		return loginUser;//20번 끝
		
	}

	public Jyh_ComMember Com_LoginCheck(Connection con, Jyh_ComMember requestMember) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		Jyh_ComMember loginUser = null;
		
		String query = prop.getProperty("loginSelectCom"); //18번 끝
		
		//20번 시작
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, requestMember.getComId());
			pstmt.setString(2, requestMember.getComPwd());
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				loginUser = new Jyh_ComMember();
				
				loginUser.setComNo(rset.getString("COM_NO"));
				loginUser.setComId(rset.getString("COM_ID"));
				loginUser.setComPwd(rset.getString("COM_PWD"));
				loginUser.setComName(rset.getString("COM_NAME"));
				loginUser.setPhone(rset.getString("PHONE"));
				loginUser.setEmail(rset.getString("EMAIL"));
				loginUser.setEnrollDate(rset.getDate("ENROLL_DATE"));
				loginUser.setModifyDate(rset.getDate("MODIFY_DATE"));
				loginUser.setInfoPeriod(rset.getString("INFO_PERIOD"));
				loginUser.setComNum(rset.getString("COM_NUM"));
				loginUser.setCompanyName(rset.getString("COMPANY_NAME"));
				loginUser.setComUrl(rset.getString("COM_URL"));
				loginUser.setComAddress(rset.getString("COM_ADDRESS"));
				loginUser.setNumEmp(rset.getString("NUM_EMP"));
				loginUser.setInvest(rset.getString("INVEST"));
				loginUser.setSales(rset.getString("SALES"));
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(rset);
			close(pstmt);
		}
		
		return loginUser;//20번 끝
		
	}

	public int insertMemberPer(Connection con, Jyh_PerMember requestMember) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("insertMemberPer");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, requestMember.getPerId());
			pstmt.setString(2, requestMember.getPerPwd());
			pstmt.setString(3, requestMember.getPerName());
			pstmt.setString(4, requestMember.getPhone());
			pstmt.setString(5, requestMember.getEmail());
			pstmt.setString(6, requestMember.getInfoPeriod());
			
			result = pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}

		return result;
	}

	public int insertMemberCom(Connection con, Jyh_ComMember requestMember) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("insertMemberCom");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, requestMember.getComId());
			pstmt.setString(2, requestMember.getComPwd());
			pstmt.setString(3, requestMember.getComName());
			pstmt.setString(4, requestMember.getPhone());
			pstmt.setString(5, requestMember.getEmail());
			pstmt.setString(6, requestMember.getInfoPeriod());
			pstmt.setString(7, requestMember.getComNum());
			pstmt.setString(8, requestMember.getCompanyName());
			pstmt.setString(9, requestMember.getComUrl());
			pstmt.setString(10, requestMember.getComAddress());
			
			result = pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}

		return result;
	}

	public int getPerListCount(Connection con) {
		Statement stmt = null;
		ResultSet rset = null;
		
		int listCount = 0;
		
		String query = prop.getProperty("perListCount");
		
		try {
			stmt = con.createStatement();
			rset = stmt.executeQuery(query);
			
			if(rset.next()) {
				listCount = rset.getInt(1);
			}
			
			System.out.println(listCount);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(stmt);
			close(rset);
		}
		
		return listCount;
	}

	public ArrayList<Jyh_PerMember> selectListPer(Connection con, PageInfo pi) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<Jyh_PerMember> list = null;
		
		String query = prop.getProperty("perSelectListWithPaging");
		
		try {
			pstmt = con.prepareStatement(query);
			
			int startRow = (pi.getCurrentPage() - 1) * pi.getLimit() + 1; //시작 페이지 -> 번호 무조건 10단위로 만든다음 +1을 시켜주는 계산식
			int endRow = startRow + pi.getLimit() - 1; //끝 페이지 -> 번호 무조건 10단위로 만든다음 -1을 시켜주는 계산식
			
			pstmt.setInt(1, startRow);
			pstmt.setInt(2, endRow);
			
			rset = pstmt.executeQuery();
			
			list = new ArrayList<>();
			
			while(rset.next()) {
				Jyh_PerMember pm = new Jyh_PerMember();
				pm.setPerNo(rset.getString("PER_NO"));
				pm.setPerId(rset.getString("PER_ID"));
				pm.setPerName(rset.getString("PER_NAME"));
				pm.setPhone(rset.getString("PHONE"));
				pm.setEmail(rset.getString("EMAIL"));
				pm.setEnrollDate(rset.getDate("ENROLL_DATE"));
				pm.setModifyDate(rset.getDate("MODIFY_DATE"));
				pm.setInfoPeriod(rset.getString("INFO_PERIOD"));
				pm.setGender(rset.getString("GENDER"));
				pm.setBirthDay(rset.getDate("BIRTHDAY"));
				
				
				list.add(pm);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return list;
	}

	public Jyh_PerMember selectOnePer(Connection con, String userNo) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		Jyh_PerMember perMember = null;
		
		String query = prop.getProperty("selectOnePer");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, userNo);
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				perMember = new Jyh_PerMember();
				
				perMember.setPerNo(rset.getString("PER_NO"));
				perMember.setPerId(rset.getString("PER_ID"));
				perMember.setPerName(rset.getString("PER_NAME"));
				perMember.setPhone(rset.getString("PHONE"));
				perMember.setEmail(rset.getString("EMAIL"));
				perMember.setEnrollDate(rset.getDate("ENROLL_DATE"));
				perMember.setModifyDate(rset.getDate("MEM_CHANGEDATE"));
				perMember.setInfoPeriod(rset.getString("INFO_PERIOD"));
				perMember.setStatus(rset.getString("STATUS"));
				perMember.setReason(rset.getString("MEM_REASON"));
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		 
		return perMember;
	}

	public ArrayList<HashMap<String, Object>> selectApplicationList(Connection con, String userNo, PageInfo pi) {
		PreparedStatement pstmt = null;
		ArrayList<HashMap<String, Object>> list = null;
		HashMap<String, Object> hmap = null;
		ResultSet rset = null;
		
		String query = prop.getProperty("selectApplicationList");
		
		try {
			pstmt = con.prepareStatement(query);
			int startRow = (pi.getCurrentPage() - 1) * pi.getLimit() + 1; //시작 페이지 -> 번호 무조건 10단위로 만든다음 +1을 시켜주는 계산식
			int endRow = startRow + pi.getLimit() - 1; //끝 페이지 -> 번호 무조건 10단위로 만든다음 -1을 시켜주는 계산식
			
			pstmt.setString(1, userNo);
			pstmt.setInt(2, startRow);
			pstmt.setInt(3, endRow);
			
			rset = pstmt.executeQuery();
			
			list = new ArrayList<HashMap<String, Object>>();
			
			while(rset.next()) {
				hmap = new HashMap<String, Object>();
				
				hmap.put("rnum", rset.getInt("RNUM"));
				hmap.put("perNo", rset.getString("PER_NO"));
				hmap.put("comNo", rset.getString("COM_NO"));
				hmap.put("rNo", rset.getString("R_NO"));
				hmap.put("aNo", rset.getString("A_NO"));
				hmap.put("companyName", rset.getString("COMPANY_NAME"));
				hmap.put("aDivision", rset.getString("A_DIVISION"));
				hmap.put("rTitle", rset.getString("R_TITLE"));
				hmap.put("aTitle", rset.getString("A_TITLE"));
				hmap.put("apvVolDate", rset.getDate("APV_VOL_DATE"));
				hmap.put("status", rset.getString("STATUS"));
				
				list.add(hmap);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(rset);
			close(pstmt);
		}
		
		return list;
	}

	public ArrayList<HashMap<String, Object>> selectDocumentList(Connection con, String userNo, PageInfo pi) {
		PreparedStatement pstmt = null;
		ArrayList<HashMap<String, Object>> list = null;
		HashMap<String, Object> hmap = null;
		ResultSet rset = null;
		
		String query = prop.getProperty("selectDocumentList");
		
		try {
			pstmt = con.prepareStatement(query);
			int startRow = (pi.getCurrentPage() - 1) * pi.getLimit() + 1; //시작 페이지 -> 번호 무조건 10단위로 만든다음 +1을 시켜주는 계산식
			int endRow = startRow + pi.getLimit() - 1; //끝 페이지 -> 번호 무조건 10단위로 만든다음 -1을 시켜주는 계산식
			
			pstmt.setString(1, userNo);
			pstmt.setInt(2, startRow);
			pstmt.setInt(3, endRow);
			
			rset = pstmt.executeQuery();
			
			list = new ArrayList<HashMap<String, Object>>();
			
			while(rset.next()) {
				hmap = new HashMap<String, Object>();
				
				hmap.put("rnum", rset.getInt("RNUM"));
				hmap.put("perNo", rset.getString("PER_NO"));
				hmap.put("rNo", rset.getString("R_NO"));
				hmap.put("rTitle", rset.getString("R_TITLE"));
				
				list.add(hmap);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(rset);
			close(pstmt);
		}
		
		return list;
	}

	public ArrayList<HashMap<String, Object>> selectPauseList(Connection con, PageInfo pi) {
		PreparedStatement pstmt = null;
		ArrayList<HashMap<String, Object>> list = null;
		HashMap<String, Object> hmap = null;
		ResultSet rset = null;
		
		String query = prop.getProperty("selectPauseList");
		
		try {
			pstmt = con.prepareStatement(query);
			int startRow = (pi.getCurrentPage() - 1) * pi.getLimit() + 1; //시작 페이지 -> 번호 무조건 10단위로 만든다음 +1을 시켜주는 계산식
			int endRow = startRow + pi.getLimit() - 1; //끝 페이지 -> 번호 무조건 10단위로 만든다음 -1을 시켜주는 계산식
			
			pstmt.setInt(1, startRow);
			pstmt.setInt(2, endRow);
			
			rset = pstmt.executeQuery();
			
			list = new ArrayList<HashMap<String, Object>>();
			
			while(rset.next()) {
				hmap = new HashMap<String, Object>();
				
				hmap.put("rnum", rset.getInt("RNUM"));
				hmap.put("perNo", rset.getString("PER_NO"));
				hmap.put("perId", rset.getString("PER_ID"));
				hmap.put("perName", rset.getString("PER_NAME"));
				hmap.put("email", rset.getString("EMAIL"));
				hmap.put("memReason", rset.getString("MEM_REASON"));
				hmap.put("memChangeDate", rset.getDate("MEM_CHANGEDATE"));
				hmap.put("releaseDate", rset.getString("RELEASE_DATE"));
				
				list.add(hmap);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(rset);
			close(pstmt);
		}
		
		return list;
	}

	public ArrayList<Jyh_ComMember> selectListCom(Connection con, PageInfo pi) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<Jyh_ComMember> list = null;
		
		String query = prop.getProperty("comSelectListWithPaging");
		
		try {
			pstmt = con.prepareStatement(query);
			
			int startRow = (pi.getCurrentPage() - 1) * pi.getLimit() + 1; //시작 페이지 -> 번호 무조건 10단위로 만든다음 +1을 시켜주는 계산식
			int endRow = startRow + pi.getLimit() - 1; //끝 페이지 -> 번호 무조건 10단위로 만든다음 -1을 시켜주는 계산식
			
			pstmt.setInt(1, startRow);
			pstmt.setInt(2, endRow);
			
			rset = pstmt.executeQuery();
			
			list = new ArrayList<>();
			
			while(rset.next()) {
				Jyh_ComMember cm = new Jyh_ComMember();
				cm.setComNo(rset.getString("COM_NO"));
				cm.setComId(rset.getString("COM_ID"));
				cm.setComName(rset.getString("COM_NAME"));
				cm.setPhone(rset.getString("PHONE"));
				cm.setEmail(rset.getString("EMAIL"));
				cm.setEnrollDate(rset.getDate("ENROLL_DATE"));
				cm.setModifyDate(rset.getDate("MODIFY_DATE"));
				cm.setInfoPeriod(rset.getString("INFO_PERIOD"));
				cm.setComNum(rset.getString("COM_NUM"));
				cm.setCompanyName(rset.getString("COMPANY_NAME"));
				cm.setComUrl(rset.getString("COM_URL"));
				cm.setComAddress(rset.getString("COM_ADDRESS"));
				cm.setNumEmp(rset.getString("NUM_EMP"));
				cm.setInvest(rset.getString("INVEST"));
				cm.setSales(rset.getString("SALES"));
				cm.setStatus(rset.getString("STATUS"));
				
				list.add(cm);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return list;
	}

	public Jyh_ComMember selectOneCom(Connection con, String userNo) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		Jyh_ComMember comMember = null;
		
		String query = prop.getProperty("selectOneCom");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, userNo);
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				comMember = new Jyh_ComMember();
				
				comMember.setComNo(rset.getString("COM_NO"));
				comMember.setComId(rset.getString("COM_ID"));
				comMember.setComName(rset.getString("COM_NAME"));
				comMember.setPhone(rset.getString("PHONE"));
				comMember.setEmail(rset.getString("EMAIL"));
				comMember.setEnrollDate(rset.getDate("ENROLL_DATE"));
				comMember.setModifyDate(rset.getDate("MEM_CHANGEDATE"));
				comMember.setInfoPeriod(rset.getString("INFO_PERIOD"));
				comMember.setComNum(rset.getString("COM_NUM"));
				comMember.setCompanyName(rset.getString("COMPANY_NAME"));
				comMember.setComUrl(rset.getString("COM_URL"));
				comMember.setComAddress(rset.getString("COM_ADDRESS"));
				comMember.setNumEmp(rset.getString("NUM_EMP"));
				comMember.setInvest(rset.getString("INVEST"));
				comMember.setSales(rset.getString("SALES"));
				comMember.setStatus(rset.getString("STATUS"));
				comMember.setReason(rset.getString("MEM_REASON"));
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		 
		return comMember;
	}

	public ArrayList<HashMap<String, Object>> selectAnnouncementList(Connection con, String userNo, PageInfo pi) {
		PreparedStatement pstmt = null;
		ArrayList<HashMap<String, Object>> list = null;
		HashMap<String, Object> hmap = null;
		ResultSet rset = null;
		
		String query = prop.getProperty("selectAnnouncementList");
		
		try {
			pstmt = con.prepareStatement(query);
			int startRow = (pi.getCurrentPage() - 1) * pi.getLimit() + 1; //시작 페이지 -> 번호 무조건 10단위로 만든다음 +1을 시켜주는 계산식
			int endRow = startRow + pi.getLimit() - 1; //끝 페이지 -> 번호 무조건 10단위로 만든다음 -1을 시켜주는 계산식
			
			pstmt.setString(1, userNo);
			pstmt.setString(2, userNo);
			pstmt.setInt(3, startRow);
			pstmt.setInt(4, endRow);
			
			rset = pstmt.executeQuery();
			
			list = new ArrayList<HashMap<String, Object>>();
			
			while(rset.next()) {
				hmap = new HashMap<String, Object>();
				
				hmap.put("rnum", rset.getInt("RNUM"));
				hmap.put("reNo", rset.getString("RE_NO"));
				hmap.put("aNo", rset.getString("A_NO"));
				hmap.put("aTitle", rset.getString("A_TITLE"));
				hmap.put("aDivision", rset.getString("A_DIVISION"));
				hmap.put("comNo", rset.getString("COM_NO"));
				hmap.put("reDate", rset.getDate("RE_DATE"));
				hmap.put("status", rset.getString("STATUS"));
				hmap.put("ctn", rset.getString("CTN"));
				
				list.add(hmap);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(rset);
			close(pstmt);
		}
		
		return list;
	}

	public int getAnnListCount(Connection con, String userNo) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
		int listCount = 0;
		
		String query = prop.getProperty("AnnListCount");
		
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setString(1, userNo);
			pstmt.setString(2, userNo);
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				listCount = rset.getInt(1);
			}
			
			System.out.println(listCount);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return listCount;
	}

	public int getComListCount(Connection con) {
		Statement stmt = null;
		ResultSet rset = null;
		
		int listCount = 0;
		
		String query = prop.getProperty("comListCount");
		
		try {
			stmt = con.createStatement();
			rset = stmt.executeQuery(query);
			
			if(rset.next()) {
				listCount = rset.getInt(1);
			}
			
			System.out.println(listCount);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(stmt);
			close(rset);
		}
		
		return listCount;
	}

	public int getApplicationListCount(Connection con, String userNo) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
		int listAppliCount = 0;
		
		String query = prop.getProperty("applicationListCount");
		
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setString(1, userNo);
		//	pstmt.setString(2, userNo);
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				listAppliCount = rset.getInt(1);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return listAppliCount;
	}

	public int getDocumentListCount(Connection con, String userNo) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
		int listAppliCount = 0;
		
		String query = prop.getProperty("documentListCount");
		
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setString(1, userNo);
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				listAppliCount = rset.getInt(1);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return listAppliCount;
	}

	public int getPauseListCount(Connection con) {
		Statement stmt = null;
		ResultSet rset = null;
		
		int listCount = 0;
		
		String query = prop.getProperty("pauseListCount");
		
		try {
			stmt = con.createStatement();
			rset = stmt.executeQuery(query);
			
			if(rset.next()) {
				listCount = rset.getInt(1);
			}
			
			System.out.println(listCount);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(stmt);
			close(rset);
		}
		
		return listCount;
	}

	public int getBlackListCount(Connection con) {
		Statement stmt = null;
		ResultSet rset = null;
		
		int listCount = 0;
		
		String query = prop.getProperty("blackListCount");
		
		try {
			stmt = con.createStatement();
			rset = stmt.executeQuery(query);
			
			if(rset.next()) {
				listCount = rset.getInt(1);
			}
			
			System.out.println(listCount);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(stmt);
			close(rset);
		}
		
		return listCount;
	}

	public ArrayList<HashMap<String, Object>> selectBlackList(Connection con, PageInfo pi) {
		PreparedStatement pstmt = null;
		ArrayList<HashMap<String, Object>> list = null;
		HashMap<String, Object> hmap = null;
		ResultSet rset = null;
		
		String query = prop.getProperty("selectBlackList");
		
		try {
			pstmt = con.prepareStatement(query);
			int startRow = (pi.getCurrentPage() - 1) * pi.getLimit() + 1; //시작 페이지 -> 번호 무조건 10단위로 만든다음 +1을 시켜주는 계산식
			int endRow = startRow + pi.getLimit() - 1; //끝 페이지 -> 번호 무조건 10단위로 만든다음 -1을 시켜주는 계산식
			
			pstmt.setInt(1, startRow);
			pstmt.setInt(2, endRow);
			
			rset = pstmt.executeQuery();
			
			list = new ArrayList<HashMap<String, Object>>();
			
			while(rset.next()) {
				hmap = new HashMap<String, Object>();
				
				hmap.put("rnum", rset.getInt("RNUM"));
				hmap.put("comNo", rset.getString("COM_NO"));
				hmap.put("comId", rset.getString("COM_ID"));
				hmap.put("companyName", rset.getString("COMPANY_NAME"));
				hmap.put("email", rset.getString("EMAIL"));
				hmap.put("comNum", rset.getString("COM_NUM"));
				hmap.put("memReason", rset.getString("MEM_REASON"));
				hmap.put("memChangeDate", rset.getDate("MEM_CHANGEDATE"));
				hmap.put("releaseDate", rset.getString("RELEASE_DATE"));
				
				
				list.add(hmap);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(rset);
			close(pstmt);
		}
		
		return list;
	}

	public int getPaymentListCount(Connection con, String userNo) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
		int listAppliCount = 0;
		
		String query = prop.getProperty("paymentListCount");
		
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setString(1, userNo);
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				listAppliCount = rset.getInt(1);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return listAppliCount;
	}

	public ArrayList<HashMap<String, Object>> selectPaymentList(Connection con, String userNo, PageInfo pi) {
		PreparedStatement pstmt = null;
		ArrayList<HashMap<String, Object>> list = null;
		HashMap<String, Object> hmap = null;
		ResultSet rset = null;
		
		String query = prop.getProperty("selectPaymentList");
		
		try {
			pstmt = con.prepareStatement(query);
			int startRow = (pi.getCurrentPage() - 1) * pi.getLimit() + 1; //시작 페이지 -> 번호 무조건 10단위로 만든다음 +1을 시켜주는 계산식
			int endRow = startRow + pi.getLimit() - 1; //끝 페이지 -> 번호 무조건 10단위로 만든다음 -1을 시켜주는 계산식
			
			pstmt.setString(1, userNo);
			pstmt.setInt(2, startRow);
			pstmt.setInt(3, endRow);
			
			rset = pstmt.executeQuery();
			
			list = new ArrayList<HashMap<String, Object>>();
			
			while(rset.next()) {
				hmap = new HashMap<String, Object>();
				
				hmap.put("rnum", rset.getInt("RNUM"));
				hmap.put("pyNo", rset.getString("PY_NO"));
				hmap.put("comNo", rset.getString("COM_NO"));
				hmap.put("pdName", rset.getString("PD_NAME"));
				hmap.put("companyName", rset.getString("COMPANY_NAME"));
				hmap.put("phone", rset.getString("PHONE"));
				hmap.put("pyDate", rset.getString("PY_DATE"));
				hmap.put("pdPrice", rset.getInt("PD_PRICE"));
				hmap.put("pyDivision", rset.getString("PY_DIVISION"));
				hmap.put("reDate", rset.getString("RE_DATE"));
				
				list.add(hmap);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(rset);
			close(pstmt);
		}
		
		return list;
	}

	public ArrayList<String> selectSkillCode(Connection con, String[] skillArr) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<String> skillCodeArr = null;
		
		String query = prop.getProperty("selectSkillCode");
		
		try {
			pstmt = con.prepareStatement(query);
			
			skillCodeArr = new ArrayList<String>();
			for(int i = 0; i < skillArr.length; i++) {
				pstmt.setString(1, skillArr[i]);
				rset = pstmt.executeQuery();
				if(rset.next()) {
					skillCodeArr.add(rset.getString(1));
				}
			}
			
			System.out.println("skillCodeArr == " + skillCodeArr);
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return skillCodeArr;
	}

	public int insertPerSkill(Connection con, ArrayList<String> skillCodeArr, String perNo) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("insertPerSkill");
		
		try {
			pstmt = con.prepareStatement(query);
			
			for(int i = 0; i < skillCodeArr.size(); i++) {
				pstmt.setString(1, perNo);
				pstmt.setString(2, skillCodeArr.get(i));
				result = pstmt.executeUpdate();
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}

		return result;
	}

	public String selectCurrvalPer(Connection con) {
		Statement stmt = null;
		ResultSet rset = null;
		String perNo = null;
		
		String query = prop.getProperty("selectCurrvalPer");
		
		try {
			stmt = con.createStatement();
			rset = stmt.executeQuery(query);
			
			if(rset.next()) {
				perNo = "A" + rset.getInt(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(stmt);
			close(rset);
		}
		
		return perNo;
	}

	public ArrayList<Jyh_PerMember> searchPerList(Connection con, PageInfo pi) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<Jyh_PerMember> list = null;
		
		String query = prop.getProperty("changeQueryPer");
		
		try {
			pstmt = con.prepareStatement(query);
			
			int startRow = (pi.getCurrentPage() - 1) * pi.getLimit() + 1; //시작 페이지 -> 번호 무조건 10단위로 만든다음 +1을 시켜주는 계산식
			int endRow = startRow + pi.getLimit() - 1; //끝 페이지 -> 번호 무조건 10단위로 만든다음 -1을 시켜주는 계산식
			
			pstmt.setInt(1, startRow);
			pstmt.setInt(2, endRow);
			
			rset = pstmt.executeQuery();
			
			list = new ArrayList<>();
			
			while(rset.next()) {
				Jyh_PerMember pm = new Jyh_PerMember();
				pm.setPerNo(rset.getString("PER_NO"));
				pm.setPerId(rset.getString("PER_ID"));
				pm.setPerName(rset.getString("PER_NAME"));
				pm.setPhone(rset.getString("PHONE"));
				pm.setEmail(rset.getString("EMAIL"));
				pm.setEnrollDate(rset.getDate("ENROLL_DATE"));
				pm.setModifyDate(rset.getDate("MODIFY_DATE"));
				pm.setInfoPeriod(rset.getString("INFO_PERIOD"));
				pm.setGender(rset.getString("GENDER"));
				pm.setBirthDay(rset.getDate("BIRTHDAY"));
				
				
				list.add(pm);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return list;
	}

	public int getChangeListCountPer(Connection con) {
		Statement stmt = null;
		ResultSet rset = null;
		
		int listCount = 0;

		String query = prop.getProperty("changeListCountPer");
		
		try {
			stmt = con.createStatement();
			rset = stmt.executeQuery(query);
			
			if(rset.next()) {
				listCount = rset.getInt(1);
			}
			
			System.out.println(listCount);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(stmt);
			close(rset);
		}
		
		return listCount;
	}

	public int getChangeListCountCom(Connection con) {
		Statement stmt = null;
		ResultSet rset = null;
		
		int listCount = 0;
		
		String query = prop.getProperty("changeListCountCom");
		
		try {
			stmt = con.createStatement();
			rset = stmt.executeQuery(query);
			
			if(rset.next()) {
				listCount = rset.getInt(1);
			}
			
			System.out.println(listCount);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(stmt);
			close(rset);
		}
		
		return listCount;
	}

	public ArrayList<Jyh_ComMember> searchComList(Connection con, PageInfo pi) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<Jyh_ComMember> list = null;
		
		String query = prop.getProperty("changeQueryCom");
		
		try {
			pstmt = con.prepareStatement(query);
			
			int startRow = (pi.getCurrentPage() - 1) * pi.getLimit() + 1; 
			int endRow = startRow + pi.getLimit() - 1;
			
			pstmt.setInt(1, startRow);
			pstmt.setInt(2, endRow);
			
			rset = pstmt.executeQuery();
			
			list = new ArrayList<>();
			
			while(rset.next()) {
				Jyh_ComMember cm = new Jyh_ComMember();
				cm.setComNo(rset.getString("COM_NO"));
				cm.setComId(rset.getString("COM_ID"));
				cm.setComName(rset.getString("COM_NAME"));
				cm.setPhone(rset.getString("PHONE"));
				cm.setEmail(rset.getString("EMAIL"));
				cm.setEnrollDate(rset.getDate("ENROLL_DATE"));
				cm.setModifyDate(rset.getDate("MODIFY_DATE"));
				cm.setInfoPeriod(rset.getString("INFO_PERIOD"));
				cm.setComNum(rset.getString("COM_NUM"));
				cm.setCompanyName(rset.getString("COMPANY_NAME"));
				cm.setComUrl(rset.getString("COM_URL"));
				cm.setComAddress(rset.getString("COM_ADDRESS"));
				cm.setNumEmp(rset.getString("NUM_EMP"));
				cm.setInvest(rset.getString("INVEST"));
				cm.setSales(rset.getString("SALES"));
				cm.setStatus(rset.getString("STATUS"));
				
				list.add(cm);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return list;
	}

	public int changeQueryPer(StringBuilder changeQuery, StringBuilder changeListCount) {
		int result = 0;
		
		prop.setProperty("changeQueryPer", changeQuery.toString());
		prop.setProperty("changeListCountPer", changeListCount.toString());
		
		OutputStream stream = null;
		try {
			stream = new FileOutputStream(fileName);
			prop.store(stream, "Server Info");
			
			result = 1;
			
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				stream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		return result;
	}

	public int changeQueryCom(StringBuilder changeQuery, StringBuilder changeListCount) {
		int result = 0;
		
		prop.setProperty("changeQueryCom", changeQuery.toString());
		prop.setProperty("changeListCountCom", changeListCount.toString());
		
		OutputStream stream = null;
		try {
			stream = new FileOutputStream(fileName);
			prop.store(stream, "Server Info");
			
			result = 1;
			
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				stream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		return result;
	}

	public String selectId(Connection con, String id) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		String returnId = null;
		
		String query = prop.getProperty("selectId");
		
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setString(1, id);
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				returnId = rset.getString("MEM_ID");
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return returnId;
	}

	public String selectCurrvalCom(Connection con) {
		Statement stmt = null;
		ResultSet rset = null;
		String comNo = null;
		
		String query = prop.getProperty("selectCurrvalCom");
		
		try {
			stmt = con.createStatement();
			rset = stmt.executeQuery(query);
			
			if(rset.next()) {
				comNo = "B" + rset.getInt(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(stmt);
			close(rset);
		}
		
		return comNo;
	}

	public int insertComStatus(Connection con, String comNo) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("insertComStatus");
		
		try {
			pstmt = con.prepareStatement(query);
			
				pstmt.setString(1, comNo);
				result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}

		return result;
	}

	public int insertPerStatus(Connection con, String perNo) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("insertPerStatus");
		
		try {
			pstmt = con.prepareStatement(query);
			
				pstmt.setString(1, perNo);
				result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}

		return result;
	}

	public ArrayList<String> selectPerSkill(Connection con, String userNo) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<String> skillArr = null;
		
		String query = prop.getProperty("selectPerSkill");
		
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setString(1, userNo);
			rset = pstmt.executeQuery();
			
			skillArr = new ArrayList<>();
			
			while(rset.next()) {
				String skill = rset.getString("SKILL_NAME");
				
				skillArr.add(skill);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}

		return skillArr;
	}

	public Jyh_PerMember selectMemBasicInfo(Connection con, String userNo) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		Jyh_PerMember pm = null;
		
		String query = prop.getProperty("selectMemBasicInfo");
		
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setString(1, userNo);
			rset = pstmt.executeQuery();
			
			pm = new Jyh_PerMember();
			
			if(rset.next()) {
				pm.setPerNo(rset.getString("PER_NO"));
				pm.setPerName(rset.getString("PER_NAME"));
				pm.setPhone(rset.getString("PHONE"));
				pm.setEmail(rset.getString("EMAIL"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}

		return pm;
	}

	public String loginCheckAdmin(Connection con, String id, String pwd) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		String admin = null;
		
		String query = prop.getProperty("loginCheckAdmin");
		
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setString(1, id);
			pstmt.setString(2, pwd);
			rset = pstmt.executeQuery();
			
			
			if(rset.next()) {
				admin = rset.getString("ADMIN_NO");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}

		return admin;
	}

}