package com.kh.itda.member.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.kh.itda.member.model.service.Wjs_MemberService;
import com.kh.itda.member.model.vo.Wjs_favorite;


@WebServlet("/selectFv.an")
public class FavoriteAnnounceSelectServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
   
    public FavoriteAnnounceSelectServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		/*String star = request.getParameter("star");*/
		String aNo = request.getParameter("aNo");
		String mNo = request.getParameter("mNo");
		String star =	"";
		
		/*System.out.println(star);*/
		System.err.println("ANO : " +aNo);
		System.out.println("MNO :" + mNo);
		
		Wjs_favorite fv = new Wjs_MemberService().selectFavorite(aNo, mNo);
		
		if(fv.getFno() > 0) {
			star = "star2";
			System.out.println("if : " +fv.getFno());
			response.setContentType("application/json");
			//response.setContentType("UTF-8");
			response.setCharacterEncoding("UTF-8");
			
			new Gson().toJson(star, response.getWriter());
		}else {
			star = "star";
			System.out.println("else :" + fv.getFno());
			response.setContentType("application/json");
			//response.setContentType("UTF-8");
			response.setCharacterEncoding("UTF-8");
			
			new Gson().toJson(star, response.getWriter());
		}
		
		
		
		
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
