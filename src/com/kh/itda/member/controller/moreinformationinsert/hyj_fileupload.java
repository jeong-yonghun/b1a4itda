package com.kh.itda.member.controller.moreinformationinsert;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;

import com.kh.itda.common.MyFileRenamePolicy;
import com.kh.itda.member.model.service.hyj_MemberService;
import com.kh.itda.member.model.vo.Jyh_PerMember;
import com.kh.itda.member.model.vo.hyj_Attachment;
import com.oreilly.servlet.MultipartRequest;

/**
 * Servlet implementation class hyj_fileupload
 */
@WebServlet("/personinsert10.pe")
public class hyj_fileupload extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public hyj_fileupload() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		/*String title = request.getParameter("title");
		System.out.println(title);*/
		String perno = request.getParameter("perno");
		
		//mutiPart/form-data로 전송하는 경우에는 기존처럼 request.getparameter로 값을 받을수없다.
		//cos.jar가 파일도 받고 폼의 다른값들도 받아주는 역활을 한다.
		//com.orelilly.servlet의 약자이다.
		
		
		//전달한 request가 multipart/form-data 방식으로 넘어온 데이터인지 확인하여 true, false 리턴
		if(ServletFileUpload.isMultipartContent(request)) {
			//전송 파일 용량 제한 : 10MB로 계싼
			//1024 * 1024 == 1MB
			int maxSize = 1024 * 1024 * 10; 
			
			//웹 서버 컨테이너(톰캣 내부의 web 폴더) 경로 추출
			String root = request.getSession().getServletContext().getRealPath("/");
			
			
			System.out.println("root : " + root);
			
			//파일저장경로
			String savePath = root + "uploadFiles/";
			
			//multopart 객체를 생성
			//DefaultFileRenamePolicy : 이름이 같다면 이름을 자동으로 변경해주는 구문
			//객체 생성시 파일을 저장하고 그에 대한 정보를 가져오는 형태이다.
			//즉, 파일의 정보를 검사하여 형태가 아닌, 저장한 다음 검사후 삭제를 해야한다.
			
			//사용자가 올린 파일명을 그대로 저장하지 않는것이 일반적이다.
			//1. 같은 파일명이 있는경우 이전파일을 덮어쓸수 있다.
			//2. 한글 혹은은 특수기호나 띄어쓰기 등이 파일이름에 있는 경우 서버에 문제가 발생할수있다.
			
			
			//DefaultFileRenamePolicy는 cos.jar안에 존재하는 클래스로 FilRenamePolicy interface를 상속받아
			//구현한 클래스이다.
			//같은 파일명이 존재하는지를 검사하고 있는 경우 파일명뒤에 숫자를 붙여준다.
			//ex : aaa.zip, aaa1.zip, aaa2.zip
/*			MultipartRequest multiRequest = 
					new MultipartRequest(request, savePath, maxSize, "UTF-8", new DefaultFileRenamePolicy());
		}*/
			MultipartRequest multiRequest = 
					new MultipartRequest(request, savePath, maxSize, "UTF-8", new MyFileRenamePolicy());
			
			//저장할 파일의 이름을 저장할 arrayList 생성
			ArrayList<String> saveFiles = new ArrayList<String>();
			
			//원본파일의 이름을 저장할 arrayList생성
			ArrayList<String> originFiles = new ArrayList<String>();
			
			//파일이 전송된 폼의 name값을 반환
			Enumeration<String> files = multiRequest.getFileNames();
			//네임이름을 가져온후 files저장한다음 
			//while문으로 값을 꺼내어서
			//name에 넣어줌
			while(files.hasMoreElements()) {
				String name = files.nextElement();
				
				System.out.println("name : " + name);
				
				//저장된 파일의 이름을 가져올떄 사용하는방법
				saveFiles.add(multiRequest.getFilesystemName(name));
				//넘어온 파일의 원본 이름을 알수가있다.
				originFiles.add(multiRequest.getOriginalFileName(name));
			}
			
			System.out.println("saveFile : " + saveFiles);
			System.out.println("originFiles : " + originFiles);
			
			//multipartREquest객체에서 파일 외의 값을 가져올수도 있다.
			String multiTitle = multiRequest.getParameter("title");
			String multiContent = multiRequest.getParameter("content");
			
			
			System.out.println("multiTitle : " + multiTitle);
			System.out.println("multiContent : " + multiContent);
			
			//Board객체 생성
			Jyh_PerMember member = new Jyh_PerMember();
			member.setPerNo(perno);
			
			//여러개의 Attachment정보를 arrayList에 담기
			ArrayList<hyj_Attachment> fileList = new ArrayList<hyj_Attachment>();
			
			//전송순서 역순으로 파일을 list에 저장했기 떄문에 반복문을 역으로 수행함
			for(int i = originFiles.size() - 1; i >= 0; i--) {
				hyj_Attachment at = new hyj_Attachment();
				at.setFilePath(savePath);
				at.setOriginName(originFiles.get(i));
				at.setChangeName(saveFiles.get(i));
				
				//역으로 순으로 하면 넣을떄 마지막사진이 0번째
				//꺼낼때는 역순
				if(i == originFiles.size() - 1) {
					//대표사진 0번쨰 사진을넣어줌
					
					at.setFileLevel(0);
				}else {
					//1 그밖에 사진들을 넣어줌
					at.setFileLevel(1);
				}
				

				fileList.add(at);
			}
			
			System.out.println("fileList : " + fileList);
		
			
			int result = new hyj_MemberService().insertThumbnail(member, fileList);
		
			if(result > 0) {
				response.sendRedirect(request.getContextPath() + "/selectList.tn");
			}else {
				//실패시 저장된 사진 삭제
				//사진을 저장되어있는 상태이기때문에 실패하면 지워줘야함
				for(int i = 0; i < saveFiles.size(); i++) {
					File failedFile = new File(savePath + saveFiles.get(i));
					//원본으로 지우면안되고
					//변경된걸로 지워야함
					//과부화 막기위해 꼭 지워줘야함
					failedFile.delete();
				}
				
				request.setAttribute("msg", "사진게시판등록실패!!");	
				request.getRequestDispatcher("views/common/errorPage.jsp").forward(request, response);
			}
			
		
		}
		
		
		
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
