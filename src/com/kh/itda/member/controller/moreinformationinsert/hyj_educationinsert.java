package com.kh.itda.member.controller.moreinformationinsert;

import java.io.IOException;
import java.util.GregorianCalendar;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.kh.itda.member.model.service.hyj_MemberService;
import com.kh.itda.member.model.vo.Jyh_PerMember;
import com.kh.itda.member.model.vo.hyj_education;
import com.kh.itda.member.model.vo.hyj_member;
import com.kh.itda.member.model.vo.hyj_moreinformation;

/**
 * Servlet implementation class hyj_personupdateServlet
 */
@WebServlet("/personinsert4.pe")
public class hyj_educationinsert extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public hyj_educationinsert() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//vo에 값 넣어줘야함
		String educationname = request.getParameter("hyj_educationname");
		String educationstartday = request.getParameter("hyj_educationstartday");
		String educationlastday = request.getParameter("hyj_educationlastday");
		String educationstack = request.getParameter("hyj_educationstack");
		String educationskil = request.getParameter("hyj_educationkil");
		String PERNO = request.getParameter("perno");
		Jyh_PerMember perno = new Jyh_PerMember();
		hyj_education education = new hyj_education();
		int result = 0;
			
		java.sql.Date day1 = null; 
		java.sql.Date day2 = null;
		
		if(educationstartday != "") {
		 day1 = java.sql.Date.valueOf(educationstartday);
		}else {
	  	 day1 = new java.sql.Date(new GregorianCalendar().getTimeInMillis());
		}
		
		if(educationlastday != "") {
		 day2 = java.sql.Date.valueOf(educationlastday); 
		}else {
		 day2 = new java.sql.Date(new GregorianCalendar().getTimeInMillis()); 
		}
		
		
		
		education.setEducationname(educationname);
		education.setEducationstartday(day1);
		education.setEducationlastday(day2);
		education.setEducationstack(educationstack);
		education.setEducationskil(educationskil);
		perno.setPerNo(PERNO);
		
		result = new hyj_MemberService().educationinsert(education, perno);
		
		String page = "";
		
		if(result > 0) {
			HttpSession session = request.getSession();
			session.setAttribute("education", education);
			page = request.getContextPath() + "/moreinfomation.pe";
			response.sendRedirect(page);
			
		}else {
			request.setAttribute("msg", "실패!");
			request.getRequestDispatcher("views/common/errorPage.jsp").forward(request, response);
		}
		

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
