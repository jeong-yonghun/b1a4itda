package com.kh.itda.member.controller.moreinformationinsert;

import java.io.IOException;
import java.lang.reflect.Member;
import java.util.GregorianCalendar;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.kh.itda.member.model.service.hyj_MemberService;
import com.kh.itda.member.model.vo.Jyh_PerMember;
import com.kh.itda.member.model.vo.hyj_education;
import com.kh.itda.member.model.vo.hyj_member;
import com.kh.itda.member.model.vo.hyj_moreinformation;
import com.kh.itda.member.model.vo.hyj_schoolcareer;

/**
 * Servlet implementation class hyj_personupdateServlet
 */
@WebServlet("/personinsert9.pe")
public class hyj_schoolinsert extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public hyj_schoolinsert() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//vo에 값 넣어줘야함
		String schoolname = request.getParameter("hyj_schoolname");
		String schoolmaster = request.getParameter("hyj_gobun");
		String startday = request.getParameter("hyj_admission");
		String endday = request.getParameter("hyj_graduated");
		String major = request.getParameter("hyj_major");
		String mygrade = request.getParameter("hyj_grades");
		String topgrade = request.getParameter("hyj_topgrades");
		String explanation = request.getParameter("hyj_explanation3");
		String PERNO = request.getParameter("perno");
		hyj_schoolcareer schoolcareer = new hyj_schoolcareer();
		Jyh_PerMember perno = new Jyh_PerMember();
		System.out.println(schoolmaster);
		int result = 0;
			
		java.sql.Date day1 = null; 
		java.sql.Date day2 = null;
		
		if(startday != "") {
		 day1 = java.sql.Date.valueOf(startday);
		}else {
	  	 day1 = new java.sql.Date(new GregorianCalendar().getTimeInMillis());
		}
		
		if(endday != "") {
			 day2 = java.sql.Date.valueOf(endday); 
		}else {
			 day2 = new java.sql.Date(new GregorianCalendar().getTimeInMillis()); 
		}
		
		
		
		
		
		schoolcareer.setSchoolname(schoolname);
		schoolcareer.setSchooletc(schoolmaster);
		schoolcareer.setSchoolenrolldate(day1);
		schoolcareer.setSchoolgraduatedate(day2);
		schoolcareer.setSchoolmajor(major);
		schoolcareer.setSchoolmypoint(mygrade);
		schoolcareer.setSchoolgradepoint(topgrade);
		schoolcareer.setSchoolexplanation(explanation);
		perno.setPerNo(PERNO);
		
		
		result = new hyj_MemberService().schoolinsert(schoolcareer, perno);
		
		String page = "";
		
		if(result > 0) {
			HttpSession session = request.getSession();
			session.setAttribute("schoolcareer", schoolcareer);
			page = request.getContextPath() + "/moreinfomation.pe";
			response.sendRedirect(page);
			
		}else {
			request.setAttribute("msg", "실패!");
			request.getRequestDispatcher("views/common/errorPage.jsp").forward(request, response);
		}
		

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
