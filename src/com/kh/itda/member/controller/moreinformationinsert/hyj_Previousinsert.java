package com.kh.itda.member.controller.moreinformationinsert;

import java.io.IOException;
import java.util.GregorianCalendar;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.kh.itda.member.model.service.hyj_MemberService;
import com.kh.itda.member.model.vo.Jyh_PerMember;
import com.kh.itda.member.model.vo.hyj_Previous;

/**
 * Servlet implementation class hyj_personupdateServlet
 */
@WebServlet("/personinsert2.pe")
public class hyj_Previousinsert extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public hyj_Previousinsert() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		  String companyname = request.getParameter("hyj_backcompany"); 
		  String jobname = request.getParameter("hyj_companyjob"); 
		  String previousjoindate = request.getParameter("hyj_joindate"); 
		  String resignationdate = request.getParameter("hyj_Resignationdate"); 
		  String stack = request.getParameter("hyj_userstack"); 
		  String money = request.getParameter("hyj_money-001"); 
		  String perno = request.getParameter("perno"); 
		  hyj_Previous Previous = new hyj_Previous();
		  Jyh_PerMember PERNO = new Jyh_PerMember();
		  int result = 0;
	
		  java.sql.Date day1 = null; 
		  java.sql.Date day2 = null;
		  
		  if(previousjoindate != "") { 
			  day1 = java.sql.Date.valueOf(previousjoindate);
		  }else { 
			  day1 = new java.sql.Date(new GregorianCalendar().getTimeInMillis());
		  }
		  
		  if(resignationdate != "") { 
			  day2 = java.sql.Date.valueOf(resignationdate); 
		  }else { 
			  day2 = new java.sql.Date(new GregorianCalendar().getTimeInMillis()); 
		  }
		  
		
		  Previous.setPreviousname(companyname);
		  Previous.setPreviousjobname(jobname);
		  Previous.setPreviousjoindate(day1);
		  Previous.setResignationdate(day2);
		  Previous.setStack(stack);
		  Previous.setMoney(money); 
		  PERNO.setPerNo(perno);
		 
		  System.out.println(Previous);
		  System.out.println(PERNO);
		 
		 result = new hyj_MemberService().personinsert2(Previous, PERNO);
		  
		  String page = "";
		  
		  if(result > 0) { 
				HttpSession session = request.getSession();
				session.setAttribute("Previous", Previous);
				page = request.getContextPath() + "/moreinfomation.pe";
				response.sendRedirect(page);
				
			}else {
				request.setAttribute("msg", "실패!");
				request.getRequestDispatcher("views/common/errorPage.jsp").forward(request, response);
		   }
		  

		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
