package com.kh.itda.member.controller.moreinformationinsert;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLEncoder;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;

import com.kh.itda.member.model.service.hyj_MemberService;
import com.kh.itda.member.model.vo.Jyh_PerMember;

/**
 * Servlet implementation class hyj_checkboxinsert
 */
@WebServlet("/checkboxinsert.pe")
public class hyj_checkboxinsert extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public hyj_checkboxinsert() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Jyh_PerMember loginUser = (Jyh_PerMember) request.getSession().getAttribute("loginUser");
		String skill = request.getParameter("skill");
		String userno = null;
		String member = null;
		
		
		if(loginUser != null) {
			userno = loginUser.getPerNo();
		}else {
			userno = request.getParameter("userNo");
		}
		
		System.out.println(userno);
		System.out.println(skill);
		
		
		member = new hyj_MemberService().skillinsert(userno, skill);

		 System.out.println(member);
	     JSONObject result  = new JSONObject();
	     result.put("data", URLEncoder.encode(member, "UTF-8"));
	     response.setContentType("application/json");
	     PrintWriter out = response.getWriter();
	     System.out.println(result);
	     out.print(result.toString());
	     out.flush();
	     out.close();  

		
		
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
