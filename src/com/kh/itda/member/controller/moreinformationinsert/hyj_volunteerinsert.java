package com.kh.itda.member.controller.moreinformationinsert;

import java.io.IOException;
import java.util.GregorianCalendar;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.kh.itda.member.model.service.hyj_MemberService;
import com.kh.itda.member.model.vo.Jyh_PerMember;
import com.kh.itda.member.model.vo.hyj_member;
import com.kh.itda.member.model.vo.hyj_moreinformation;
import com.kh.itda.member.model.vo.hyj_volunteer;

/**
 * Servlet implementation class hyj_personupdateServlet
 */
@WebServlet("/personinsert5.pe")
public class hyj_volunteerinsert extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public hyj_volunteerinsert() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String volunteername = request.getParameter("hyj_Volunteername");
		String volunteerfirst = request.getParameter("hyj_Volunteerfirst");
		String volunteerlast = request.getParameter("hyj_Volunteerlast");
		String volunteerlink = request.getParameter("hyj_Volunteerlink");
		String volunteerExplanation = request.getParameter("hyj_VolunteerExplanation");
		String PERNO = request.getParameter("perno");
		Jyh_PerMember perno = new Jyh_PerMember();
		hyj_volunteer volunteer = new hyj_volunteer();
		int result = 0;
		
		java.sql.Date day1 = null; 
		java.sql.Date day2 = null;
		
		if(volunteerfirst != "") { 
		 day1 = java.sql.Date.valueOf(volunteerfirst);
		}else { 
	 	 day1 = new java.sql.Date(new GregorianCalendar().getTimeInMillis());
		}
		
		if(volunteerlast != "") { 
		day2 = java.sql.Date.valueOf(volunteerlast); 
		}else { 
	    day2 = new java.sql.Date(new GregorianCalendar().getTimeInMillis()); 
		}
		
		
		
		volunteer.setVolunteername(volunteername);
		volunteer.setVolunteerStartDate(day1);
		volunteer.setVolunteerendDate(day2);
		volunteer.setVolunteerlink(volunteerlink);
		volunteer.setVolunteerExplanation(volunteerExplanation);
		perno.setPerNo(PERNO);

		result = new hyj_MemberService().volunteerinsert(volunteer, perno);
		
		String page = "";
		
		if(result > 0) {
			HttpSession session = request.getSession();
			session.setAttribute("volunteer", volunteer);
			page = request.getContextPath() + "/moreinfomation.pe";
			response.sendRedirect(page);
			
		}else {
			request.setAttribute("msg", "실패!");
			request.getRequestDispatcher("views/common/errorPage.jsp").forward(request, response);
		}

		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
