package com.kh.itda.member.controller.moreinformationinsert;

import java.io.IOException;
import java.util.GregorianCalendar;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.kh.itda.member.model.service.hyj_MemberService;
import com.kh.itda.member.model.vo.Jyh_PerMember;
import com.kh.itda.member.model.vo.hyj_developmentinsert;

/**
 * Servlet implementation class hyj_personupdateServlet
 */
@WebServlet("/personinsert3.pe")
public class hyj_Developmentinsert extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public hyj_Developmentinsert() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//vo에 값 넣어줘야함
		String joindate = request.getParameter("hyj_joindate1");
		String Resignationdate = request.getParameter("hyj_Resignationdate1");
		String stack = request.getParameter("hyj_userstack1");
		String skill = request.getParameter("hyj_userskil1");
		String giturl = request.getParameter("hyj_usergit");
		String perno = request.getParameter("perno");
		hyj_developmentinsert develop = new hyj_developmentinsert();
		Jyh_PerMember PERNO = new Jyh_PerMember();
		int result = 0;
		
		java.sql.Date day1 = null;
		java.sql.Date day2 = null;
		
		if(joindate != "") {
			day1 = java.sql.Date.valueOf(joindate);
		}else {
			day1 = new java.sql.Date(new GregorianCalendar().getTimeInMillis());
		}
		
		if(Resignationdate != "") {
			day2 = java.sql.Date.valueOf(Resignationdate);
		}else {
			day2 = new java.sql.Date(new GregorianCalendar().getTimeInMillis()); 
		}
		
		
		
		
		develop.setJoindate(day1);
		develop.setResignationdate(day2);
		develop.setDuserstack(stack);
		develop.setDuserskill(skill);
		develop.setUsergit(giturl);
		PERNO.setPerNo(perno);
		

		result = new hyj_MemberService().Developmentinsert(develop, PERNO);
		
		String page = "";
		
		if(result > 0) {
			HttpSession session = request.getSession();
			session.setAttribute("develop", develop);
			page = request.getContextPath() + "/moreinfomation.pe";
			response.sendRedirect(page);
			
		}else {
			request.setAttribute("msg", "실패!");
			request.getRequestDispatcher("views/common/errorPage.jsp").forward(request, response);
		}
		

		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
