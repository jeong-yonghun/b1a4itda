package com.kh.itda.member.controller.moreinformationinsert;

import java.io.IOException;
import java.util.GregorianCalendar;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.kh.itda.member.model.service.hyj_MemberService;
import com.kh.itda.member.model.vo.Jyh_PerMember;
import com.kh.itda.member.model.vo.hyj_awards;
import com.kh.itda.member.model.vo.hyj_member;
import com.kh.itda.member.model.vo.hyj_moreinformation;

/**
 * Servlet implementation class hyj_personupdateServlet
 */
@WebServlet("/personinsert7.pe")
public class hyj_awardinsert extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public hyj_awardinsert() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//vo에 값 넣어줘야함
		String awardsname = request.getParameter("hyj_Awardsname1");
		String awardscontent = request.getParameter("hyj_Awardscontent");
		String awardsmonth = request.getParameter("hyj_Awardsmonth");
		String awardsAgency = request.getParameter("hyj_AwardsAgency1");
		String PERNO = request.getParameter("perno");
		Jyh_PerMember perno = new Jyh_PerMember();
		hyj_awards awards = new hyj_awards();
		int result = 0;

		  java.sql.Date day1 = null; 
		  
		  if(awardsmonth != "") { 
			  day1 = java.sql.Date.valueOf(awardsmonth);
		  }else { 
			  day1 = new java.sql.Date(new GregorianCalendar().getTimeInMillis());
		  }
		
		  awards.setAwardsname(awardsname);
		  awards.setAwardscontent(awardscontent);
		  awards.setAwardsmonth(day1);
		  awards.setAwardsagency(awardsAgency);
		perno.setPerNo(PERNO);
		
		result = new hyj_MemberService().awardsinsert(awards, perno);


		
		 String page = "";
		
		 if(result > 0) {
			 	HttpSession session = request.getSession();
				session.setAttribute("awards", awards);
				page = request.getContextPath() + "/moreinfomation.pe";
				response.sendRedirect(page);
				
			}else {
				request.setAttribute("msg", "실패!");
				request.getRequestDispatcher("views/common/errorPage.jsp").forward(request, response);
		 }
		
		

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
