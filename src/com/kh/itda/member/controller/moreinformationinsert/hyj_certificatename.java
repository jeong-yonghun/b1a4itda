package com.kh.itda.member.controller.moreinformationinsert;

import java.io.IOException;
import java.util.GregorianCalendar;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.kh.itda.member.model.service.hyj_MemberService;
import com.kh.itda.member.model.vo.Jyh_PerMember;
import com.kh.itda.member.model.vo.hyj_awards;
import com.kh.itda.member.model.vo.hyj_certificate;
import com.kh.itda.member.model.vo.hyj_member;
import com.kh.itda.member.model.vo.hyj_moreinformation;

/**
 * Servlet implementation class hyj_personupdateServlet
 */
@WebServlet("/personinsert8.pe")
public class hyj_certificatename extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public hyj_certificatename() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String certificatename = request.getParameter("hyj_certificatename");
		String certificatestart = request.getParameter("hyj_certificatestart");
		String certificateend = request.getParameter("hyj_certificateend");
		String certificateagency = request.getParameter("hyj_certificateagency");
		String perno = request.getParameter("perno");
		Jyh_PerMember PERNO = new Jyh_PerMember();
		hyj_certificate certificate = new hyj_certificate();
		int result = 0;
		
		  java.sql.Date day1 = null; 
		  java.sql.Date day2 = null;
		  
		  if(certificatestart != "") { 
			  day1 = java.sql.Date.valueOf(certificatestart);
		  }else { 
			  day1 = new java.sql.Date(new GregorianCalendar().getTimeInMillis());
		  }
		  
		  
		  if(certificateend != "") { 
			  day2 = java.sql.Date.valueOf(certificateend); 
		  }else { 
			  day2 = new java.sql.Date(new GregorianCalendar().getTimeInMillis()); 
		  }
		  
		
		
		  certificate.setCertificatename(certificatename);
		  certificate.setCertificatestart(day1);
		  certificate.setCertificateend(day2);
		  certificate.setCertificateagency(certificateagency);
		  PERNO.setPerNo(perno);

		result = new hyj_MemberService().certificateinsert(certificate, PERNO);

		 String page = "";
		
		 if(result > 0) {
			 	HttpSession session = request.getSession();
				session.setAttribute("certificate", certificate);
				page = request.getContextPath() + "/moreinfomation.pe";
				response.sendRedirect(page);
				
			}else {
				request.setAttribute("msg", "실패!");
				request.getRequestDispatcher("views/common/errorPage.jsp").forward(request, response);
		 }
		
		
		
		
		

		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
