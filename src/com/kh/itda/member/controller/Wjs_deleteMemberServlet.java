package com.kh.itda.member.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.kh.itda.member.model.service.Wjs_MemberService;
import com.kh.itda.member.model.vo.Jyh_PerMember;

/**
 * Servlet implementation class Wjs_deleteMemberServlet
 */
@WebServlet("/deleteMember.per")
public class Wjs_deleteMemberServlet extends HttpServlet {
   private static final long serialVersionUID = 1L;
       

    public Wjs_deleteMemberServlet() {
        super();
        // TODO Auto-generated constructor stub
    }


   protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      
      /*String perId = request.getParameter();*/
      
      /*deleteMember.setPerId(perId);*/
      
      Jyh_PerMember deleteMember = (Jyh_PerMember) request.getSession().getAttribute("loginUser");
      
      
      int result = new Wjs_MemberService().deleteMember(deleteMember);
      
      
      String page = "";
      
      if(result > 0) {
         
         HttpSession session = request.getSession();
         session.invalidate();
         
         
         page = "views/common/wjs_successPage.jsp";
         request.setAttribute("successCode", "deleteMember");
         System.out.println("탈퇴완료");   
         
      }else {
         
      }
      
      request.getRequestDispatcher(page).forward(request, response);
      
   }

   
   protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      // TODO Auto-generated method stub
      doGet(request, response);
   }

}