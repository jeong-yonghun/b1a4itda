package com.kh.itda.member.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kh.itda.member.model.service.Wjs_MemberService;
import com.kh.itda.recruitAndContest.model.vo.Announcement;

@WebServlet("/surpportEnd.re")
public class Wjs_HireSurpportEndServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	
    public Wjs_HireSurpportEndServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

    
	
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String ano = request.getParameter("ano");
		String pno = request.getParameter("pno");
		String rno = request.getParameter("rno");
		
		System.out.println(ano);
		
		System.out.println("pno :"+pno);
		System.out.println("rno :"+rno);
		
		int result = new Wjs_MemberService().surpportHire(ano,pno,rno);
		
		Announcement a = new Wjs_MemberService().selectHireEnd(ano);
		String duty = a.getDuty();
		String name = a.getCom_nmae();
		System.out.println(duty);
		System.out.println(name);
		
		String page="";
		
		if(result > 0) {
			System.out.println("지원완료");
			/*response.sendRedirect("views/person/programmerRecruit/hireSurpportEnd.jsp?duty="+ duty +"&name="+ name);*/
			page = "views/person/programmerRecruit/hireSurpportEnd.jsp";
			request.setAttribute("duty", duty);
			request.setAttribute("name", name);
		}else {
			
		}
		request.getRequestDispatcher(page).forward(request, response);
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
