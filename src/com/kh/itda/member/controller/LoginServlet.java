package com.kh.itda.member.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.kh.itda.member.model.service.Jyh_MemberService;
import com.kh.itda.member.model.vo.Jyh_ComMember;
import com.kh.itda.member.model.vo.Jyh_PerMember;

@WebServlet("/login.me")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public LoginServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String memChk = request.getParameter("memChk");
		
		System.out.println(memChk);
		
		if(memChk.equals("1")) {
			
			String perId = request.getParameter("perId");
			String perPwd = request.getParameter("perPwd");
			
			System.out.println(perId);
			System.out.println(perPwd);
			
			//12번
			Jyh_PerMember requestMember = new Jyh_PerMember();
			requestMember.setPerId(perId);
			requestMember.setPerPwd(perPwd);
			
			Jyh_PerMember loginUser = new Jyh_MemberService().loginCheckPer(requestMember, memChk);
			
			System.out.println("loginUser : " + loginUser);
			
			//21번 시작
			if(loginUser != null) {
				HttpSession session = request.getSession();
				session.setAttribute("loginUser", loginUser);
				session.setAttribute("memChk", memChk);
				
				response.sendRedirect("index.jsp");
			} else {
				request.setAttribute("msg", "로그인 실패!");
				request.getRequestDispatcher("views/common/errorPage.jsp").forward(request, response);
			}//21번 끝 -> 로그인 부분 완료
		}else if(memChk.equals("2")){
			String perId = request.getParameter("perId");
			String perPwd = request.getParameter("perPwd");
			
			System.out.println(perId);
			System.out.println(perPwd);
			
			//12번
			Jyh_ComMember requestMember = new Jyh_ComMember();
			requestMember.setComId(perId);
			requestMember.setComPwd(perPwd);
			
			Jyh_ComMember loginUser = new Jyh_MemberService().loginCheckCom(requestMember, memChk);
			
			System.out.println("loginUser : " + loginUser);
			
			//21번 시작
			if(loginUser != null) {
				HttpSession session = request.getSession();
				session.setAttribute("loginUser", loginUser);
				session.setAttribute("memChk", memChk);
				
				
				response.sendRedirect("/itda/views/company/companyMainPage/companyMainPage.jsp");
			} else {
				request.setAttribute("msg", "로그인 실패!");
				request.getRequestDispatcher("views/common/errorPage.jsp").forward(request, response);
			}//21번 끝 -> 로그인 부분 완료
		}
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
