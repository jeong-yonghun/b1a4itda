package com.kh.itda.member.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kh.itda.member.model.service.hyj_MemberService;
import com.kh.itda.member.model.vo.Jyh_PerMember;
import com.kh.itda.member.model.vo.hyj_Previous;

/**
 * Servlet implementation class hyj_moreinfomation
 */
@WebServlet("/moreinfomation.pe")
public class hyj_moreinfomation extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public hyj_moreinfomation() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Jyh_PerMember loginUser = (Jyh_PerMember) request.getSession().getAttribute("loginUser");

		System.out.println("들어옴");
		String userno = null;
		
		System.out.println(loginUser);
		
		if(loginUser != null) {
			userno = loginUser.getPerNo();
		}else {
			userno = request.getParameter("userNo");
		}
		
		
		ArrayList<HashMap<String,Object>> resumeDetail = new hyj_MemberService().moreinformation(userno);
		
		String page = "";
		System.out.println("보내질값 확인" + resumeDetail);
		if(resumeDetail != null) {
			page = "views/person/personMyPage/personMoreinformation/hyj_personMoreinformation_detail.jsp";
			request.setAttribute("resumeDetail", resumeDetail);
	
		}else {
			page="views/person/personMyPage/personMoreinformation/hyj_personMoreinformation_detail.jsp";
			request.setAttribute("msg", "실패");
		}
		request.getRequestDispatcher(page).forward(request, response);
		
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
