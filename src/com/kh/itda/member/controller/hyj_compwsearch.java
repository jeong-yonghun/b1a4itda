package com.kh.itda.member.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kh.itda.member.model.service.hyj_MemberService;
import com.kh.itda.member.model.vo.Jyh_ComMember;
import com.kh.itda.member.model.vo.Jyh_PerMember;

/**
 * Servlet implementation class hyj_compwsearch
 */
@WebServlet("/compwsearch.me")
public class hyj_compwsearch extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public hyj_compwsearch() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		System.out.println("들옴");
		String num = request.getParameter("valnum");
		System.out.println(num);
		
		if(num.equals("1")) {
		String comname = request.getParameter("comname1");
		String comid = request.getParameter("comid1");
		String comemail = request.getParameter("comemail");
		String userpwd = request.getParameter("perPwd1");
		Jyh_ComMember requestmember = new Jyh_ComMember();
		int result = 0;
		
		
		requestmember.setComName(comname);
		requestmember.setEmail(comemail);
		requestmember.setComId(comid);
		requestmember.setComPwd(userpwd);
		System.out.println(requestmember);
		result = new hyj_MemberService().compwsearch(requestmember);
		System.out.println(result);
		String page = "";
		if(result > 0 ) {
			page = "index.jsp";
			request.setAttribute("successCode", "searchMember");

		}else {
			page = "index.jsp";
			request.setAttribute("msg", "error");
		}
		request.getRequestDispatcher(page).forward(request, response);
		
	}else {
		
		String comname = request.getParameter("comname2");
		String comid = request.getParameter("comid2");
		String comphone = request.getParameter("comphone");
		String userpwd = request.getParameter("perPwd1");
		Jyh_ComMember requestmember = new Jyh_ComMember();
		int result = 0;
		
		
		requestmember.setComName(comname);
		requestmember.setPhone(comphone);
		requestmember.setComId(comid);
		requestmember.setComPwd(userpwd);
		System.out.println(requestmember);
		result = new hyj_MemberService().comphpwsearch(requestmember);
		System.out.println(result);
		String page = "";
		if(result > 0 ) {
			page = "index.jsp";
			request.setAttribute("successCode", "searchMember");

		}else {
			page = "index.jsp";
			request.setAttribute("msg", "error");
		}
		request.getRequestDispatcher(page).forward(request, response);

	}
		
		
	}
		
	

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
