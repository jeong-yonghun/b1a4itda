package com.kh.itda.member.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLEncoder;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;

import com.kh.itda.member.model.service.hyj_MemberService;
import com.kh.itda.member.model.vo.Jyh_PerMember;


/**
 * Servlet implementation class hyj_idsearch
 */
@WebServlet("/idsearch.pe")
public class hyj_idsearch extends HttpServlet {
   private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public hyj_idsearch() {
        super();
        // TODO Auto-generated constructor stub
    }

   /**
    * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
    */
   protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      String name = request.getParameter("username");
      String email = request.getParameter("useremail");
      Jyh_PerMember member = new Jyh_PerMember();
      Jyh_PerMember requestmember = new Jyh_PerMember();
      
      requestmember.setPerName(name);
      requestmember.setEmail(email);
      
      member = new hyj_MemberService().oneidsearch(requestmember);
      
      JSONObject result  = new JSONObject();
      result.put("userid", URLEncoder.encode(member.getPerId(), "UTF-8"));
      response.setContentType("application/json");
      PrintWriter out = response.getWriter();
      System.out.println(result);
      out.print(result.toString());
      out.flush();
      out.close();  
      }
   

   /**
    * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
    */
   protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      // TODO Auto-generated method stub
      doGet(request, response);
   }

}