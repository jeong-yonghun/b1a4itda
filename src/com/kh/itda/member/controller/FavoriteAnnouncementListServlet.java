package com.kh.itda.member.controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kh.itda.member.model.service.Wjs_MemberService;
import com.kh.itda.member.model.vo.Jyh_PerMember;
import com.kh.itda.recruitAndContest.model.vo.Announcement;


@WebServlet("/favoritelist.an")
public class FavoriteAnnouncementListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
   
    public FavoriteAnnouncementListServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

    
    

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		Jyh_PerMember loginUser = (Jyh_PerMember) request.getSession().getAttribute("loginUser");
		
		ArrayList<Announcement> list = new Wjs_MemberService().favoriteListAn(loginUser);
		
		ArrayList<Announcement> list2 = new Wjs_MemberService().favoriteListAn2(loginUser);
		
		System.out.println(list);
		System.out.println(list2);
		
		
		String page = "";
		if(list != null) {
			page="views/person/personMyPage/personMyPageFavorites/personMyPageFavorites.jsp";
			request.setAttribute("list", list);
			request.setAttribute("list2", list2);
			
		}else {
			
		}
		
		request.getRequestDispatcher(page).forward(request, response);
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
