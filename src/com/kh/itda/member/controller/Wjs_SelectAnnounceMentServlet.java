package com.kh.itda.member.controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kh.itda.member.model.service.Wjs_MemberService;
import com.kh.itda.recruitAndContest.model.vo.Announcement;

/**
 * Servlet implementation class Wjs_SelectAnnounceMentServlet
 */
@WebServlet("/selectAn.me")
public class Wjs_SelectAnnounceMentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
  
    public Wjs_SelectAnnounceMentServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String num = request.getParameter("num");
		
		Announcement an = new Wjs_MemberService().selectAnnouncement(num);
		
		String page = "";
		
		
		if(an != null) {
			page= "views/person/programmerRecruit/hireEnrollComplete.jsp";
			request.setAttribute("an", an);
			
		}else {
			
			
		}
		
		request.getRequestDispatcher(page).forward(request, response);
		
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
