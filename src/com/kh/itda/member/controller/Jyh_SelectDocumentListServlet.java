package com.kh.itda.member.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kh.itda.approval.model.vo.PageInfo;
import com.kh.itda.member.model.service.Jyh_MemberService;

@WebServlet("/jyhDocumentList.me")
public class Jyh_SelectDocumentListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public Jyh_SelectDocumentListServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String userNo = request.getParameter("userNo");
		
		System.out.println("지원현황보기!!! = " + userNo);
		
		//------------ 페이징 처리 후 --------------
		int currentPage;		//현재 페이지를 표시할 변수
		int limit;				//한 페이지에 게시글이 몇 개 보여질 것인지 표시
		int maxPage;			//전체 페이지에서 가장 마지막 페이지
		//현재 화면이 1~10페이지 버튼이 있으면 1이 시작, 10이 끝
		//만약 넘어가서 11~20페이지 버튼이 보이면 11이 시작 20이 끝
		//만약 11~13까지 있으면 13이 마지막이 되어야한다.
		int startPage;			//한 번에 표시될 페이지가 시작할 페이지 
		int endPage;			//한 번에 표시될 페이지가 끝나는 페이지
		
		//게시판은 1페이지부터 시작함
		currentPage = 1;
		
		//전달받은 페이지가 있다면 전달값으로 currentPage 값 변경 (null이 아닐 때)
		if(request.getParameter("currentPage") != null) {
			currentPage = Integer.parseInt(request.getParameter("currentPage"));
		}
		
		//한 페이지에 보여질 목록 갯수
		limit = 10;
		
		Jyh_MemberService ms = new Jyh_MemberService();
		int listCount = ms.getDocumentListCount(userNo);
		
		System.out.println("listCount : " + listCount);
		
		//총 페이지 수 계산
		//목록 수가 123개 이면 페이지 수는 13페이지가 되어야 한다.(나는 97개니까 10페이지)
		//짜투리 목록이 최소 1개일 때, 1페이지가 추가되도록 계산
		maxPage = (int) ((double) listCount / limit + 0.9);
		
		//현재 페이지에 보여줄 시작 페이지 수 (10개씩 보여지게 할 경우)
		//아래쪽 페이지 수가 10개씩 보여지게 한다면
		//1, 11, 21, 31
		startPage = (((int) ((double) currentPage / limit + 0.9)) - 1) * 10 + 1;
		
		//아래쪽에 보여질 마지막 페이지 수(10, 20, 30, ...)
		endPage = startPage + 10 - 1;
		
		if(maxPage < endPage) {
			endPage = maxPage;
		}
		
		PageInfo pi = new PageInfo(currentPage, listCount, limit, maxPage, startPage, endPage);
		
		
		//보드와 어태치먼트 조인해서 사용한다.
		//어레이 리스트에는 해쉬맵 타입만 들어올 수 있도록 하고
		//해쉬맵은 모든 객체를 담을 수 있도록 오브젝트 타입으로 제네릭설정한다.
		//ArrayList<HashMap<String, Object>> list = new Jyh_MemberService().selectApplicationList(userNo, pi);
		ArrayList<HashMap<String, Object>> list = new Jyh_MemberService().selectDocumentList(userNo, pi);
		//System.out.println(list.get(0));
				
		System.out.println("Document list : " + list);
				
		String page = "";
		if(list != null) {
			System.out.println("이력서/포트폴리오 불러오기 성공!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
			page = "views/admin/jyh_adminMember_per_resume.jsp";
			request.setAttribute("list", list);
			request.setAttribute("pi", pi);
			request.setAttribute("userNo", userNo);
		} else {
			page = "views/common/errorPage.jsp";
			request.setAttribute("msg", "이력서/포트폴리오 불러오기 실패!");
		}
		request.getRequestDispatcher(page).forward(request, response);
		
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
