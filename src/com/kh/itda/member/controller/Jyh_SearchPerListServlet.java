package com.kh.itda.member.controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kh.itda.approval.model.vo.PageInfo;
import com.kh.itda.member.model.service.Jyh_MemberService;
import com.kh.itda.member.model.vo.Jyh_ComMember;
import com.kh.itda.member.model.vo.Jyh_PerMember;
import com.kh.itda.query.MemListPagingQuery;

@WebServlet("/jyhSearchPerList.me")
public class Jyh_SearchPerListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public Jyh_SearchPerListServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String dateCondition = request.getParameter("dateCondition");
		String selectCondition = request.getParameter("selectCondition");
		String memType = request.getParameter("memType");
		String chk = request.getParameter("chk");
		
		StringBuilder changeQuery = null;
		StringBuilder changeListCount = null;
		
		if(chk == null) {
			chk = "1";
		}
		
		if(memType.equals("1") && chk.equals("1")) {
			if(selectCondition.charAt(2) == '%') {
				selectCondition = "PER_ID LIKE"+selectCondition+" OR PER_NAME LIKE"+selectCondition
						+" OR PHONE LIKE"+selectCondition+" OR ENROLL_DATE LIKE"+selectCondition
						+" OR MODIFY_DATE LIKE"+selectCondition+" OR EMAIL LIKE"+selectCondition;
			}
			changeQuery = new MemListPagingQuery().changeQueryPer(dateCondition, selectCondition);
			changeListCount = new MemListPagingQuery().changeListCountPer(dateCondition, selectCondition);
			
			int result = new Jyh_MemberService().changeQueryPer(changeQuery, changeListCount);
		}else if(memType.equals("2") && chk.equals("1")) {
			if(selectCondition.charAt(2) == '%') {
				selectCondition = "COM_ID LIKE"+selectCondition+" OR COM_NAME LIKE"+selectCondition
						+" OR PHONE LIKE"+selectCondition+" OR ENROLL_DATE LIKE"+selectCondition
						+" OR COMPANY_NAME LIKE"+selectCondition+" OR STATUS LIKE"+selectCondition
						+" OR EMAIL LIKE"+selectCondition+" OR MODIFY_DATE LIKE"+selectCondition;
			}
			changeQuery = new MemListPagingQuery().changeQueryCom(dateCondition, selectCondition);
			changeListCount = new MemListPagingQuery().changeListCountCom(dateCondition, selectCondition);
			
			int result = new Jyh_MemberService().changeQueryCom(changeQuery, changeListCount);
		}
		
		
		System.out.println("여기야 여기 memType = " + memType);
		System.out.println("여기야 여기 changeListCount = " + changeListCount);
		System.out.println("여기야 여기 changeQuery = " + changeQuery);
		// ------------ 페이징 처리 후 --------------
		int currentPage; // 현재 페이지를 표시할 변수
		int limit; // 한 페이지에 게시글이 몇 개 보여질 것인지 표시
		int maxPage; // 전체 페이지에서 가장 마지막 페이지
		int startPage; // 한 번에 표시될 페이지가 시작할 페이지
		int endPage; // 한 번에 표시될 페이지가 끝나는 페이지

		currentPage = 1;

		// 전달받은 페이지가 있다면 전달값으로 currentPage 값 변경 (null이 아닐 때)
		if (request.getParameter("currentPage") != null) {
			currentPage = Integer.parseInt(request.getParameter("currentPage"));
		}

		// 한 페이지에 보여질 목록 갯수
		limit = 10;

		Jyh_MemberService ms = new Jyh_MemberService();
		int listCount = ms.getChangeListCount(memType);

		System.out.println("getChangeListCount : " + listCount);

		maxPage = (int) ((double) listCount / limit + 0.9);

		startPage = (((int) ((double) currentPage / limit + 0.9)) - 1) * 10 + 1;

		endPage = startPage + 10 - 1;

		if (maxPage < endPage) {
			endPage = maxPage;
		}

		PageInfo pi = new PageInfo(currentPage, listCount, limit, maxPage, startPage, endPage);
		
		String pagingUrl = null;

		if (memType.equals("1")) {
			ArrayList<Jyh_PerMember> list = new Jyh_MemberService().searchPerList(pi);
//			System.out.println(list.get(0).getPerId());
			String page = "";
			pagingUrl = "/jyhSearchPerList.me?memType=1&chk=2&";
			if (list != null) {
				System.out.println("개인멤버리스트 검색기능 성공!!!!");
				page = "views/admin/jyh_adminMember_per_list.jsp";
				request.setAttribute("list", list);
				request.setAttribute("pi", pi);
				request.setAttribute("pagingUrl", pagingUrl);
			} else {
				System.out.println("개인멤버리스트 실패!!!!!!!!!");
				page = "views/common/errorPage.jsp";
				request.setAttribute("msg", "개인멤버리스트 검색기능 실패!");
			}
			request.getRequestDispatcher(page).forward(request, response);
		}else if(memType.equals("2")) {
			ArrayList<Jyh_ComMember> list = new Jyh_MemberService().searchComList(pi);
			System.out.println(list.get(0).getComId());
			String page = "";
			pagingUrl = "/jyhSearchPerList.me?memType=2&chk=2&";
			if (list != null) {
				System.out.println("기업멤버리스트 성공!!!!");
				page = "views/admin/jyh_adminMember_com_list.jsp";
				request.setAttribute("list", list);
				request.setAttribute("pi", pi);
				request.setAttribute("pagingUrl", pagingUrl);
			} else {
				System.out.println("기업멤버리스트 실패!!!!!!!!!");
				page = "views/common/errorPage.jsp";
				request.setAttribute("msg", "게시판 목록 조회 실패!");
			}
			request.getRequestDispatcher(page).forward(request, response);
		}

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
