package com.kh.itda.member.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLEncoder;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;

import com.kh.itda.member.model.service.hyj_MemberService;
import com.kh.itda.member.model.vo.Jyh_PerMember;

/**
 * Servlet implementation class hyj_pwsearch
 */
@WebServlet("/pwsearch.me")
public class hyj_pwsearch extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public hyj_pwsearch() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		System.out.println("들옴");
		String num = request.getParameter("valnum");
		System.out.println(num);
	
		
		if(num.equals("1")) {

		String username = request.getParameter("username1");
		String userid = request.getParameter("userid1");
		String useremail = request.getParameter("useremail");
		String userpwd = request.getParameter("perPwd1");
		Jyh_PerMember requestmember = new Jyh_PerMember();
		int result = 0;

		requestmember.setPerName(username);
		requestmember.setPerId(userid);
		requestmember.setEmail(useremail);
		requestmember.setPerPwd(userpwd);

		System.out.println(requestmember);
		
		result = new hyj_MemberService().perpwsearch(requestmember);
		
		String page = "";
		if(result > 0 ) {
			page = "views/common/jyh_login.jsp";
			request.setAttribute("successCode", "searchMember");

		}else {
			page = "views/common/jyh_login.jsp";
			request.setAttribute("msg", "error");
		}
		request.getRequestDispatcher(page).forward(request, response);
		
		}else if (num.equals("2")){
			String username2 = request.getParameter("username2");
			String userid2 = request.getParameter("userid2");
			String phone2 = request.getParameter("phone");
			String userpwd2 = request.getParameter("perPwd1");
			Jyh_PerMember requestmember2 = new Jyh_PerMember();
			int result2 = 0;

			requestmember2.setPerName(username2);
			requestmember2.setPerId(userid2);
			requestmember2.setPhone(phone2);
			requestmember2.setPerPwd(userpwd2);

			System.out.println(requestmember2);
			
			result2 = new hyj_MemberService().perphpwsearch(requestmember2);
			
			String page = "";
			if(result2 > 0 ) {
				page = "views/common/jyh_login.jsp";
				request.setAttribute("successCode", "searchMember");

			}else {
				page = "views/common/jyh_login.jsp";
				request.setAttribute("msg", "error");

			}
			request.getRequestDispatcher(page).forward(request, response);
			
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
