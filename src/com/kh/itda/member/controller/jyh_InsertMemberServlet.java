package com.kh.itda.member.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kh.itda.member.model.service.Jyh_MemberService;
import com.kh.itda.member.model.vo.Jyh_ComMember;
import com.kh.itda.member.model.vo.Jyh_PerMember;

@WebServlet("/jyhinsert.me")
public class jyh_InsertMemberServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public jyh_InsertMemberServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		System.out.println("서블릿들어왔다!!");
		
		String memType = request.getParameter("memTypeChk");
		System.out.println("개인은 1번 기업은 2번 = " + memType);
		int result = 0;
		
		if(memType.equals("1")) {
			
			String perId = request.getParameter("perId");
			String perPwd = request.getParameter("perPwd1");
			String perName = request.getParameter("perName");
			String phone = request.getParameter("phone");
			String email = request.getParameter("email");
			String infoPeriod = request.getParameter("infoPeriod");
			String[] skillArr = request.getParameterValues("skill-exp");
			
			Jyh_PerMember requestMember = new Jyh_PerMember();
			requestMember.setPerId(perId);
			requestMember.setPerPwd(perPwd);
			requestMember.setPerName(perName);
			requestMember.setPhone(phone);
			requestMember.setEmail(email);
			requestMember.setInfoPeriod(infoPeriod);
			
			System.out.println("insert request member : " + requestMember);
			System.out.println("skillArr : " + skillArr);
			
			result = new Jyh_MemberService().insertMemberPer(requestMember,skillArr);
			
		}else if(memType.equals("2")) {
			String comId = request.getParameter("id-com");
			String comPwd = request.getParameter("comPwd1");
			String comName = request.getParameter("name-com");
			String phone = request.getParameter("phone");
			String email = request.getParameter("email");
			String infoPeriod = request.getParameter("infoPeriod");
			String comNum = request.getParameter("comNumTxt");
			String companyName = request.getParameter("comname");
			String comUrl = request.getParameter("com-url");
			String add1 = request.getParameter("com-add1");
			String add2 = request.getParameter("com-add2");
			String add3 = request.getParameter("com-add3");
			String add4 = request.getParameter("com-add4");
			String address = add1 + "&" + add2 + "&" + add3 + "&" + add4;
			
			System.out.println("사업자번호 : " + comNum);
			
			Jyh_ComMember requestMember = new Jyh_ComMember();
			requestMember.setComId(comId);
			requestMember.setComPwd(comPwd);
			requestMember.setComName(comName);
			requestMember.setPhone(phone);
			requestMember.setEmail(email);
			requestMember.setInfoPeriod(infoPeriod);
			requestMember.setComNum(comNum);
			requestMember.setCompanyName(companyName);
			requestMember.setComUrl(comUrl);
			requestMember.setComAddress(address);
			
			System.out.println("insert request member : " + requestMember);
			
			result = new Jyh_MemberService().insertMemberCom(requestMember);
		}
		
		String page = "";
		if(result > 0) {
			System.out.println("회원가입 성공!!!!!!!!!!!!!!!!!!!!!!!!!!");
			page = "views/common/jyh_login.jsp";
			response.sendRedirect(page);
		}else {
			System.out.println("회원가입 실패!!!!!!!!!!!!!!!!!!!!!!!!!!");
			page = "views/common/errorPage.jsp";
			request.setAttribute("msg", "회원 가입 실패");
			request.getRequestDispatcher(page).forward(request, response);
		}
		
		
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
