package com.kh.itda.member.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.kh.itda.member.model.service.Wjs_MemberService;
import com.kh.itda.member.model.vo.Wjs_favorite;

/**
 * Servlet implementation class FavoriteAnnouncementServlet
 */
@WebServlet("/favorite.an")
public class FavoriteAnnouncementServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
  
    public FavoriteAnnouncementServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

    

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String star = request.getParameter("star");
		String aNo = request.getParameter("aNo");
		String mNo = request.getParameter("mNo");
		
		System.out.println(star);
		System.err.println(aNo);
		System.out.println(mNo);
		
		Wjs_favorite fv = new Wjs_favorite();
		fv.setFavoriteRe(aNo);
		fv.setFavoriteDo(mNo);
		int result = 0;
			
		
		if(star.equals("star")) {
			result = new Wjs_MemberService().insertFavorite(fv);
			
		}else {
			
			result = new Wjs_MemberService().deletetFavorite(fv);
		}
		
		System.out.println(result);
		
		response.setContentType("application/json");
		//response.setContentType("UTF-8");
		response.setCharacterEncoding("UTF-8");
		
		new Gson().toJson(star, response.getWriter());
		
		
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
