package com.kh.itda.member.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kh.itda.member.model.service.Jyh_MemberService;
import com.kh.itda.member.model.vo.Jyh_ComMember;
import com.kh.itda.member.model.vo.Jyh_PerMember;

@WebServlet("/jyhSelectOne.me")
public class Jyh_SelectOneMemberServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public Jyh_SelectOneMemberServlet() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		System.out.println("들왔다!!!");

		String userNo = request.getParameter("userNo");
		String memType = request.getParameter("memType");
		System.out.println("userNo = " + userNo);
		System.out.println("memType = " + memType);

		if (memType.equals("1")) {
			HashMap<String, Object> perDetail = new Jyh_MemberService().selectOnePer(userNo);
			
			Jyh_PerMember pm = (Jyh_PerMember) perDetail.get("perMember");
			ArrayList<String> skillArr = (ArrayList<String>) perDetail.get("skillArr");


			String page = "";
			if (pm != null && skillArr != null) {
				page = "views/admin/jyh_adminMember_per_detail.jsp";
				request.setAttribute("perMember", pm);
				request.setAttribute("skillArr", skillArr);
				request.setAttribute("userNo", userNo);
			} else {
				page = "views/common/errorPage.jsp";
				request.setAttribute("msg", "개인회원검색 실패!");
			}
			request.getRequestDispatcher(page).forward(request, response);
		}else if(memType.equals("2")) {
			Jyh_ComMember cm = new Jyh_MemberService().selectOneCom(userNo);

			System.out.println("comMember detail : " + cm);

			String page = "";
			if (cm != null) {
				page = "views/admin/jyh_adminMember_com_detail.jsp";
				request.setAttribute("comMember", cm);
			} else {
				page = "views/common/errorPage.jsp";
				request.setAttribute("msg", "기업회원검색 실패!");
			}
			request.getRequestDispatcher(page).forward(request, response);
		}


	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
