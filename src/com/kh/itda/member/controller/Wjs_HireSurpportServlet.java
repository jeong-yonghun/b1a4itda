package com.kh.itda.member.controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kh.itda.addInfo.resume.model.vo.Resume;
import com.kh.itda.member.model.service.Wjs_MemberService;
import com.kh.itda.member.model.vo.Jyh_PerMember;


@WebServlet("/surpport.re")
public class Wjs_HireSurpportServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
   
    public Wjs_HireSurpportServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

    
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		Jyh_PerMember loginUser = (Jyh_PerMember) request.getSession().getAttribute("loginUser");
		
		String ano = request.getParameter("no");
		String tt = request.getParameter("tt");
		
		
		
		
		System.out.println(loginUser);
		
		ArrayList<Resume> list = new Wjs_MemberService().selectResume(loginUser);
		
		System.out.println(list);
		
		String page = "";
		
		if(list != null) {
			
			page="views/person/programmerRecruit/hireSurpport.jsp";
			request.setAttribute("list", list);
			request.setAttribute("ano", ano);
			request.setAttribute("tt", tt);
			
		}else {
			
		}
		
		request.getRequestDispatcher(page).forward(request, response);
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
