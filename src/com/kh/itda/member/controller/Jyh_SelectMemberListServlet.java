package com.kh.itda.member.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kh.itda.approval.model.vo.PageInfo;
import com.kh.itda.member.model.service.Jyh_MemberService;
import com.kh.itda.member.model.vo.Jyh_ComMember;
import com.kh.itda.member.model.vo.Jyh_PerMember;

@WebServlet("/jyhselectlist.me")
public class Jyh_SelectMemberListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public Jyh_SelectMemberListServlet() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String memType = request.getParameter("memType");
		System.out.println("memType = " + memType);
		// ------------ 페이징 처리 후 --------------
		int currentPage; // 현재 페이지를 표시할 변수
		int limit; // 한 페이지에 게시글이 몇 개 보여질 것인지 표시
		int maxPage; // 전체 페이지에서 가장 마지막 페이지
		// 현재 화면이 1~10페이지 버튼이 있으면 1이 시작, 10이 끝
		// 만약 넘어가서 11~20페이지 버튼이 보이면 11이 시작 20이 끝
		// 만약 11~13까지 있으면 13이 마지막이 되어야한다.
		int startPage; // 한 번에 표시될 페이지가 시작할 페이지
		int endPage; // 한 번에 표시될 페이지가 끝나는 페이지

		// 게시판은 1페이지부터 시작함
		currentPage = 1;

		// 전달받은 페이지가 있다면 전달값으로 currentPage 값 변경 (null이 아닐 때)
		if (request.getParameter("currentPage") != null) {
			currentPage = Integer.parseInt(request.getParameter("currentPage"));
		}

		// 한 페이지에 보여질 목록 갯수
		limit = 10;

		Jyh_MemberService ms = new Jyh_MemberService();
		int listCount = ms.getListCount(memType);

		System.out.println("listCount : " + listCount);

		// 총 페이지 수 계산
		// 목록 수가 123개 이면 페이지 수는 13페이지가 되어야 한다.(나는 97개니까 10페이지)
		// 짜투리 목록이 최소 1개일 때, 1페이지가 추가되도록 계산
		maxPage = (int) ((double) listCount / limit + 0.9);

		// 현재 페이지에 보여줄 시작 페이지 수 (10개씩 보여지게 할 경우)
		// 아래쪽 페이지 수가 10개씩 보여지게 한다면
		// 1, 11, 21, 31
		startPage = (((int) ((double) currentPage / limit + 0.9)) - 1) * 10 + 1;

		// 아래쪽에 보여질 마지막 페이지 수(10, 20, 30, ...)
		endPage = startPage + 10 - 1;

		if (maxPage < endPage) {
			endPage = maxPage;
		}

		PageInfo pi = new PageInfo(currentPage, listCount, limit, maxPage, startPage, endPage);
		
		String pagingUrl = null;

		if (memType.equals("1")) {
			ArrayList<Jyh_PerMember> list = new Jyh_MemberService().selectListPer(pi);
			System.out.println(list.get(0).getPerId());
			String page = "";
			pagingUrl = "/jyhselectlist.me?memType=1&";
			if (list != null) {
				System.out.println("개인멤버리스트 성공!!!!");
				page = "views/admin/jyh_adminMember_per_list.jsp";
				request.setAttribute("list", list);
				request.setAttribute("pi", pi);
				request.setAttribute("pagingUrl", pagingUrl);
			} else {
				System.out.println("개인멤버리스트 실패!!!!!!!!!");
				page = "views/common/errorPage.jsp";
				request.setAttribute("msg", "게시판 목록 조회 실패!");
			}
			request.getRequestDispatcher(page).forward(request, response);
		}else if(memType.equals("2")) {
			ArrayList<Jyh_ComMember> list = new Jyh_MemberService().selectListCom(pi);
			pagingUrl = "/jyhselectlist.me?memType=2&";
			System.out.println(list.get(0).getComId());
			String page = "";
			if (list != null) {
				System.out.println("기업멤버리스트 성공!!!!");
				page = "views/admin/jyh_adminMember_com_list.jsp";
				request.setAttribute("list", list);
				request.setAttribute("pi", pi);
				request.setAttribute("pagingUrl", pagingUrl);
			} else {
				System.out.println("기업멤버리스트 실패!!!!!!!!!");
				page = "views/common/errorPage.jsp";
				request.setAttribute("msg", "게시판 목록 조회 실패!");
			}
			request.getRequestDispatcher(page).forward(request, response);
		}


	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
