package com.kh.itda.member.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLEncoder;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;

import com.kh.itda.member.model.service.hyj_MemberService;
import com.kh.itda.member.model.vo.Jyh_ComMember;

/**
 * Servlet implementation class hyj_comphidserarch
 */
@WebServlet("/phidserch.co")
public class hyj_comphidserarch extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public hyj_comphidserarch() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		  String comnamephone = request.getParameter("comnamephone");
	      String comphone = request.getParameter("comphone");
	      Jyh_ComMember member = new Jyh_ComMember();
	      Jyh_ComMember requestmember = new Jyh_ComMember();
	      requestmember.setComName(comnamephone);
	      requestmember.setPhone(comphone);
	      System.out.println(requestmember);
	      member = new hyj_MemberService().comphidsearch(requestmember);
	      System.out.println(member);
	      
	      JSONObject result  = new JSONObject();
	      result.put("userid", URLEncoder.encode(member.getComId(), "UTF-8"));
	      response.setContentType("application/json");
	      PrintWriter out = response.getWriter();
	      System.out.println(result);
	      out.print(result.toString());
	      out.flush();
	      out.close();   
	      }
	

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
