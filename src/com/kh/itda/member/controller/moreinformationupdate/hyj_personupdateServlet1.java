package com.kh.itda.member.controller.moreinformationupdate;

import java.io.IOException;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.kh.itda.member.model.service.hyj_MemberService;
import com.kh.itda.member.model.vo.Jyh_PerMember;
import com.kh.itda.member.model.vo.hyj_member;
import com.kh.itda.member.model.vo.hyj_moreinformation;

/**
 * Servlet implementation class hyj_personupdateServlet
 */
@WebServlet("/personupdate1.pe")
public class hyj_personupdateServlet1 extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public hyj_personupdateServlet1() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String name = request.getParameter("hyj_username");
		String email = request.getParameter("hyj_email");
		String phone = request.getParameter("hyj_phone");
		String birthday = request.getParameter("hyj_birthday");
		String gender = request.getParameter("hyj_gender");
		String perno = request.getParameter("perno");
		Jyh_PerMember member = new Jyh_PerMember();
		HashMap<String, Object> hmap = null;
		
		
		java.sql.Date day = null;
	
		if(birthday != "") {
			day = java.sql.Date.valueOf(birthday);
		}else {
			day = new java.sql.Date(new GregorianCalendar().getTimeInMillis());
		}
		
		member.setPerName(name);
		member.setEmail(email);
		member.setPhone(phone);
		member.setBirthDay(day);
		member.setGender(gender);
		member.setPerNo(perno);
			System.out.println(member);
			
			int result = new hyj_MemberService().personupdate1(member);

			String page = "";
			if(result >0){
				HttpSession session = request.getSession();
				session.setAttribute("loginUser", member);
				page = request.getContextPath() + "/moreinfomation.pe";
				response.sendRedirect(page);

			}else{
				request.setAttribute("msg", "실패!");
				request.getRequestDispatcher("views/common/errorPage.jsp").forward(request, response);
			}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
