package com.kh.itda.member.controller;


import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kh.itda.member.model.service.Jyh_MemberService;
import com.kh.itda.member.model.vo.Jyh_PerMember;

@WebServlet("/jyhBasicInfo.me")
public class Jyh_SelectMemBasicInfoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public Jyh_SelectMemBasicInfoServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		String position = request.getParameter("position");
		String aNo = request.getParameter("aNo");
		String userNo = request.getParameter("userNo");
		String division = request.getParameter("division");
		
		Jyh_PerMember perMem = new Jyh_MemberService().selectMemBasicInfo(userNo);
		
		System.out.println("perMem : " + perMem);
		System.out.println("postion : " + position);
		System.out.println("aNo : " + aNo);
		
		String page = "";
		if (perMem != null) {
			page = "views/company/com_competition/jyh_competition_application.jsp";
			request.setAttribute("perMem", perMem);
			request.setAttribute("aNo", aNo);
			request.setAttribute("position", position);
			request.setAttribute("division", division);
		} else {
			page = "views/common/errorPage.jsp";
			request.setAttribute("msg", "공모전 내용 불러오기 실패!");
		}
		request.getRequestDispatcher(page).forward(request, response);
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
