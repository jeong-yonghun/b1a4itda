package com.kh.itda.member.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.kh.itda.member.model.service.Jyh_MemberService;

@WebServlet("/jyhIdCheck.me")
public class Jyh_IdCheckServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public Jyh_IdCheckServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String id = request.getParameter("id");
		
		System.out.println(id);
		
		String returnId = new Jyh_MemberService().selectId(id);
		
		System.out.println("들어옴1");
		
		String result = null;
		
		
		if(returnId == null) {
			result = "ok";
		}else {
			result = "no";
		}
		
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		new Gson().toJson(result, response.getWriter());
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
