package com.kh.itda.member.controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.kh.itda.member.model.service.Wjs_MemberService;
import com.kh.itda.member.model.vo.Wjs_Notice;


@WebServlet("/notice2.com")
public class Wjs_selectNoticeMsgServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
   
    public Wjs_selectNoticeMsgServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

    
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String no1 = request.getParameter("no1");
		
		System.out.println(no1);
		
		Wjs_Notice notice = new Wjs_MemberService().selectNoticeMsg(no1);
		
		System.out.println("notice :" + notice);
		
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		
		new Gson().toJson(notice, response.getWriter());
		
		
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
