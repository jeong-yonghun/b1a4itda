package com.kh.itda.member.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLEncoder;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;

import com.kh.itda.member.model.service.hyj_MemberService;
import com.kh.itda.member.model.vo.Jyh_PerMember;

/**
 * Servlet implementation class hyj_personpwphone
 */
@WebServlet("/personpwphone.pe")
public class hyj_personpwphone extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public hyj_personpwphone() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String username = request.getParameter("username");
		String userid = request.getParameter("userid");
		String phone = request.getParameter("phone");
		Jyh_PerMember requestmember = new Jyh_PerMember();
		Jyh_PerMember member = new Jyh_PerMember();
	
		
		System.out.println("username : " + username);
		System.out.println("userid : " + userid);
		System.out.println("useremail : " + phone);
		
		requestmember.setPerName(username);
		requestmember.setPerId(userid);
		requestmember.setPhone(phone);
		
		
		member = new hyj_MemberService().personpwphsearch(requestmember);
		
		
	      JSONObject result  = new JSONObject();
	      result.put("userid", URLEncoder.encode(member.getPerId(), "UTF-8"));
	      response.setContentType("application/json");
	      PrintWriter out = response.getWriter();
	      System.out.println(result);
	      out.print(result.toString());
	      out.flush();
	      out.close(); 
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
