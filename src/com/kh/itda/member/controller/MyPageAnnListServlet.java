package com.kh.itda.member.controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kh.itda.member.model.vo.Jyh_ComMember;
import com.kh.itda.member.model.vo.Jyh_PerMember;
import com.kh.itda.programmerRecruit.model.service.AnnonuceService;
import com.kh.itda.recruitAndContest.model.vo.Announcement;

/**
 * Servlet implementation class MyPageAnnListServlet
 */
@WebServlet("/selectAnnPer.li")
public class MyPageAnnListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MyPageAnnListServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		Jyh_ComMember loginUser = (Jyh_ComMember) request.getSession().getAttribute("loginUser");
		System.out.println("ComNo: "+loginUser.getComNo());
		ArrayList<Announcement> list=new AnnonuceService().PerSelectAnnounList(loginUser.getComNo());
		
		System.out.println("서블릿 채용공고관리 리스트  list:"+list);
		
		String page="";
		if(list!=null) {
			page="views/company/companyMyPage/cho_companyAnnList.jsp";
			request.setAttribute("list",list);
		}else {
			page="views/common/errorPage.jsp";
			request.setAttribute("msg","기업 마이페이지 공고목록조회 실패");
		}
		request.getRequestDispatcher(page).forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
