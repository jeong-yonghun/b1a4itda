package com.kh.itda.member.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kh.itda.member.model.service.hyj_MemberService;
import com.kh.itda.member.model.vo.Jyh_ComMember;

/**
 * Servlet implementation class hyj_companymoreinformation
 */
@WebServlet("/companymoreinformation.co")
public class hyj_companymoreinformation extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public hyj_companymoreinformation() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String companyname = request.getParameter("hyj_com");
		String comusername = request.getParameter("hyj_comusername");
		String comuserphone = request.getParameter("hyj_phone");
		String comemail = request.getParameter("hyj_email");
		String cominvest = request.getParameter("hyj_invest");
		String comemp = request.getParameter("hyj_personcnum");
		String sales = request.getParameter("hyj_money");
		String url = request.getParameter("hyj_url");
		String comno = request.getParameter("comno");
		Jyh_ComMember member = new Jyh_ComMember();
		int result = 0;
		
		member.setCompanyName(companyname);
		member.setComName(comusername);
		member.setPhone(comuserphone);
		member.setEmail(comemail);
		member.setInvest(cominvest);
		member.setNumEmp(comemp);
		member.setSales(sales);
		member.setComUrl(url);
		member.setComNo(comno);
	
		System.out.println("member : " + member);
		result = new hyj_MemberService().moreinfoupdate(member);
		String page = "";
		if(result > 0){
			page ="views/company/companyMyPage/companyMyPageinformation/hyj_companyMyPage_information.jsp";
			request.setAttribute("ok", "성공");
		}else{
			page = "views/company/companyMyPage/companyMyPageinformation/hyj_companyMyPage_information.jsp";
			request.setAttribute("msg", "실패");
		}
		request.getRequestDispatcher(page).forward(request, response);
		
			
		}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
