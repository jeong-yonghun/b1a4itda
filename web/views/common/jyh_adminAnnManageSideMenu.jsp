<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="ko">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <style>
        #jyh_nav-div{
            height:1000px;
        }
        #jyh_side-menu-outline p{
            margin:10px;
        }
        #jyh_side-menu-outline ul{
            list-style:none; 
            padding-left:5px;
        }
        #jyh_side-menu-outline{
            width:75%;
            margin:auto;
            border:1px solid gray;
        }
        #jyh_side-menu-per, #jyh_side-menu-com{
            width:80%;
            margin:auto;
        }
        #jyh_side-menu-com{
            margin-top:50px;
        }
    </style>
 </head>
</body>
             <div style="height:75px; width:280px;">
                    </div>
            <div id=jyh_nav-div>
        <div id="jyh_side-menu-outline">
            <div id="jyh_side-menu-per">
                <h3>채용공고</h3>
                <hr style="margin-right:auto; margin-left:auto; margin-top:5px;">
                <ul>
                    <li style="font-weight:bold;">채용공고정보
                        <ul style="font-weight:400;">
                            <li id="hireProceeding"><p>- 진행중인공고</p></li>
                            <li id="hireEnd"><p>- 마감된공고</p></li>
                            <li id="hireOkAndReturn"><p>- 승인/반려공고</p></li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div id="jyh_side-menu-com">
                <h3>공모전</h3>
                <hr style="margin-right:auto; margin-left:auto; margin-top:5px;">
                <ul>
                    <li style="font-weight:bold;">공모전정보
                        <ul style="font-weight:400;">
                            <li id="competitionProceeding"><p>- 진행중인공모전</p></li>
                            <li id="competitionEnd"><p>- 마감된공모전</p></li>
                            <li id="competitionOkAndReturn"><p>- 승인/반려공모전</p></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    
    <script>
    	$("#hireProceeding").click(function(){
    		location.href = "<%=request.getContextPath()%>/jyhSelectProgressHireList.re?annType=1&annDivision=1";
    	});
    	$("#hireEnd").click(function(){
    		location.href = "<%=request.getContextPath()%>/jyhSelectProgressHireList.re?annType=1&annDivision=2";
    	});
    	$("#hireOkAndReturn").click(function(){
    		location.href = "<%=request.getContextPath()%>/jyhSelectProgressHireList.re?annType=1&annDivision=3";
    	});
    	$("#competitionProceeding").click(function(){
    		location.href = "<%=request.getContextPath()%>/jyhSelectProgressHireList.re?annType=2&annDivision=1";
    	});
    	$("#competitionEnd").click(function(){
    		location.href = "<%=request.getContextPath()%>/jyhSelectProgressHireList.re?annType=2&annDivision=2";
    	});
    	$("#competitionOkAndReturn").click(function(){
    		location.href = "<%=request.getContextPath()%>/jyhSelectProgressHireList.re?annType=2&annDivision=3";
    	});
    </script>
</body>
</html>