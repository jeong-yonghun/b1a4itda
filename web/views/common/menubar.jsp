<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="com.kh.itda.member.model.vo.Jyh_PerMember"%>
<%
	Jyh_PerMember loginUser = (Jyh_PerMember) session.getAttribute("loginUser");
%>
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script> -->
	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script> 
<script src="//code.jquery.com/jquery.min.js"></script>

 <style>

        /*----------------------------------------- 메뉴바 style 영역 시작 -------------------------------------  */
        .head-blue-text-bold{
            color:#5A84F1;
            font-weight: bold;
            text-align: center;
          
        }

         
        .head{
            margin-left: auto;
            margin-right: auto;
            width:1400px;
            height:100px;
            text-align: center;
            
        }
        .head1{
            height:60%;
            margin:0px;
            padding:0px;
            text-align: center;
            
        }

        .head1-1{
            height:100%;  
            float:left;
            text-align: center;
           ;
        }
        .head1-1-empty{
            width:7.5%; 
            text-align: center;
            color:black;
        }
        .head1-2-logo{
            width:7.5%; 
            text-align: center;
            
        }
        .head1-3-search{
            width:25%;
            text-align: center;
            
        }
        input[name=menubar-search]{
            height:86%; 
            width:60%; 
            float: left;
            border:solid 2px; 
            border-color: #5A84F1;
            margin-left: 5px;
            margin-top: 6px;
            text-align: center;
           
        }
        .head1-1-3-menubar-search{
            height:50%; 
            margin-top:3.86%;
            text-align: center;
           
        }
        .search-img-space{
         border:solid 2px;
          border-color: #5A84F1;
           width:10%;
            height:87%; 
            background: #5A84F1;
             float:left;
              margin-top: 6px;
              text-align: center;
           
        }
        .search-img{
            width: 100%; 
            max-width: 25px; 
            vertical-align: middle;
            text-align: center;
           
        }

        .head1-4-empty{
            width:45%;
            text-align: center;
           
        }
        .head1-5{
            width:15%;
            text-align: center;
            
        }
        .head1-5-1{
            width:100%; 
            height:50%;  
            float:left; 
            margin-top:15px;
            text-align: center;
           
        }
        .head1-5-1-alarm-img-space{
            width:25%; 
            height:100%;  
            float:left; 
            margin-left:auto; 
            margin-right:auto;
            text-align: center;
            
        }
        .alarm-img{
            width: 100%; 
            max-width: 30px; 
            vertical-align: middle;
            text-align: center;
            
        }
        .head1-5-2-mypage-label-space{
            width:40%; 
            height:21.6px;  
            float:left; 
            margin-left:auto; 
            margin-right:auto; 
            margin-top:2.2px;
            text-align: center;
            
        }        
        .head1-5-3-login-label-space{
            width:35%; 
            height:21.6px; 
            float:left; 
            margin-left:auto; 
            margin-right:auto; 
            margin-top:4.2px;
            text-align: center;
           
        }
        
        .head2{
            height:40%; 
            background-color:#4876EF;
            text-align: center;
            
            
        }
        .head2-common{
            height:100%; 
            float:left;
            text-align: center;
            
        }
        .head2-1{
            width:30%;
            text-align: center;
            
        }
        .head2-2{
            width:40%;
            text-align: center;
            
        }
        .head2-3{
            width:30%;
            text-align: center;
            
        }

        .head2-text{
            font-weight: bold; 
            color:white; 
            width:80%; 
            height:50%; 
            margin-left:auto; 
            margin-right:auto; 
            margin-top:10px;
            text-align: center;
           
        }
        .head2-2-text1{
            width:30%;
            text-align: center;
            
        }
        .head2-2-text2{
            width:25%; 
            margin-left:23px; 
            margin-right:23px;
            text-align: center;
            
        }
        .head2-2-text3{
            width:35%;
            text-align: center;
            
        }

        /*----------------------------------------- 메뉴바 style 영역 끝 -------------------------------------  */


    </style>

    <header class="head"> <!-- 메뉴바 시작 -->
        <div class="head1">
            <div class="head1-1 head1-1-empty" >
            </div><!-- 빈 공간 -->

            <div class="head1-1 head1-2-logo" onclick="goIndi()" ><img src="/itda/image/logo.PNG" style="width:100%; height: 100%;"></div><!-- 로고 들어갈 자리 -->

            <div class="head1-1 head1-3-search"><!-- 검색 들어갈 자리 시작 -->
                <div class="head1-1-3-menubar-search">
                    <input type="text" name="menubar-search" maxlength="100">
                    <div class="search-img-space"><!-- 돋보기 이미지 시작 -->
                            <img src="/itda/image/searchIcon.PNG" style="width: 100%; max-width: 25px; vertical-align: middle"  />
                    </div><!-- 돋보기 이미지 끝 -->
                </div>
            </div><!-- 검색 들어갈 자리 끝 -->

            <div class="head1-1 head1-4-empty"></div><!-- 빈공간 -->

            <div class="head1-1 head1-5"><!-- 알림, 마이페이지, 로그인, 로그아웃 자리 시작 -->

                <div class="head1-5-1">
                    <div class="head1-5-1-alarm-img-space"><!-- 알림 이미지 시작 -->
                         <img src="/itda/image/bell.PNG" style="width: 100%; max-width: 30px; vertical-align: middle" />
                    </div><!-- 알림 이미지 끝 -->
                    <div class="head1-5-2-mypage-label-space" >
                        <% if(loginUser == null){ %>
                        	<label class="head-blue-text-bold" onclick="gocompany()"style="margin-top:2px;" >기업페이지</label>
    					<%}else{ %>
    						<label class="head-blue-text-bold" onclick="goMyPage()">My Page</label>
    					<%}%>
                    </div>
                    <div class="head1-5-3-login-label-space">
                        <% if(loginUser == null){ %>
                        	<label id="nickName" onclick="goLogin()" class="head-blue-text-bold">로그인</label>
    					<%}else{ %>
    						<label id="nickName" onclick="logout()" class="head-blue-text-bold">로그아웃</label>
    					<%}%>
                    </div>
                </div>
				
            </div><!-- 알림, 마이페이지, 로그인, 로그아웃 자리 끝 -->
            <script>
            //0211조지연이 임시로 만듬               
            	function goMyPage(){
            		location.href="/itda/views/person/personMyPage/personMyPageResume/hyj_personmainpage.jsp";
            	}
            	function goIndi(){
            		console.log("test");
            		location.href="/itda/index.jsp";
            	}
            	function goLogin(){
            		location.href="/itda/views/common/jyh_login.jsp";
            	}
            	function logout(){
            		location.href="<%=request.getContextPath()%>/logout.me";
            	}
            	function gocompany(){
            		location.href="/itda/views/company/companyMainPage/companyMainPage.jsp";
            	}
            	function goCompetition(){
            		location.href="/itda/selectCompetitionList.re";
            	}
 
            </script>

        </div>
        <div class="head2">
            <div class="head2-common head2-1"></div>
            <div class="head2-common head2-2">
                <div class="head2-common head2-2-text1">
                   <div class="head2-text" onclick="goProgrammer()">
                        <p style="margin:4px;">개발자 채용</p>
                    </div>
                </div>  
                <div class="head2-common head2-2-text2">
                    <div class="head2-text" onclick="goCompetition()">
                        <p style="margin:4px;">공모전</p>
                    </div>
                </div>
                <div class="head2-common head2-2-text3">
                    <div class="head2-text">
                        <p style="margin:4px;">개발자 커뮤니티</p>
                    </div>
                </div>  
            </div>
            <div class="head2-common head2-3"></div>
        </div>
         <script type="text/javascript">
           function goProgrammer(){
              location.href="<%=request.getContextPath()%>/beforeSearch.anon";
           }
        </script>
    </header><!-- 메뉴바 끝 -->
