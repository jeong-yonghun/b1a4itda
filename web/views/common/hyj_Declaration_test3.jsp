<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<style>
#all {
	width: 1000px;
	height: 1000px;
	margin-left: auto;
	margin-right: auto;
	margin-top: 4%;
}

#bba {
	border-top: 1px solid black;
	font-family: 'Noto Sans KR', sans-serif;
}

#baa {
	border-top: 1px solid black;
	font-family: 'Noto Sans KR', sans-serif;
}

.sideCustomerCenter {
	border: 1px solid blue;
	width: 150px;
}
.inquiryBoard {
	width: 700px;
	margin-left: 20px;
}
.board{
	width: 700px;
	text-align: center;
	
    border-collapse: collapse;
	
}
td {
	height: 30px;
}

#btnArea {
	width: 150px;
	margin-left: auto;
	margin-right: auto;
}

.btn1 {
	background-color: blue;
	color: white;
	border-radius: 5px;
}

.btn2 {
	background-color: gray;
	color: white;
	border-radius: 5px;
}
.hyj_tableline{
	border-bottom: 2px solid black;
}
.hyj_td1{
	border-bottom: 1px solid #444444;
    padding: 10px;
}

</style>
</head>
<body>
	<%@ include file="../common/menubar.jsp"%>
	<div id="all">
		<div class="sideCustomerCenter" style="float: left;">
			<table>
				<tr>
					<td>
						<table>
							<tr>
								<td
									 style="font-family: 'Noto Sans KR', sans-serif;">고객센터</td>
							</tr>
							<tr>
								<td  id="baa">FAQ</td>
							</tr>
							<tr>
								<td id="bba">문의하기</td>
							</tr>
							<tr>
								<td id="bba">신고</td>
							</tr>
							<tr>
								<td id="bba">- 신고접수</td>
                            </tr>
                            <tr>
                                <td id="bba">- 신고진행조회</td>
                            </tr>
						</table>
					</td>
				</tr>
			</table>
		</div>
		<form>
			<div class="inquiryBoard" style="float: left;">
				<div style="font-weight: bold; font-size: 24px; color:rgb(90, 132, 241);">신고진행조회</div>
				<hr style="border: 2px solid rgb(90, 132, 241);">
				<table class="board">
					<tr>
						<th>글번호</th>
						<th>제목</th>
						<th>신고일자</th>
						<th>답변여부</th>
					</tr>
					<tr>
						<td class="hyj_td1">1</td>
						<td class="hyj_td1">허위채용공고를 올린b1a4회사를 신고합니다</td>
						<td class="hyj_td1">2020.02.01</td>
						<td class="hyj_td1">답변대기중</td>
					
					</tr>
					<tr>
						<td class="hyj_td1">2</td>
						<td class="hyj_td1">사기</td>
						<td class="hyj_td1">2020.05.05</td>
						<td class="hyj_td1">답변완료</td>
					</tr>
				</table>
			</div>
		</form>
	</div>
	<%@ include file="../common/cho_footer.jsp"%>
</body>
</html>