<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<style>
#all {
	width: 1000px;
	height: 1000px;
	margin-left: auto;
	margin-right: auto;
	margin-top: 4%;
}

#bba {
	border-top: 1px solid black;
	font-family: 'Noto Sans KR', sans-serif;
}

#baa {
	border-top: 1px solid black;
	font-family: 'Noto Sans KR', sans-serif;
}

.sideCustomerCenter {
	border: 1px solid blue;
	width: 150px;
}
.inquiryBoard {
	width: 700px;
	margin-left: 20px;
}

td {
	height: 30px;
}

#btnArea {
	width: 150px;
	margin-left: auto;
	margin-right: auto;
}

.btn1 {
	background-color: blue;
	color: white;
	border-radius: 5px;
}

.btn2 {
	background-color: gray;
	color: white;
	border-radius: 5px;
}
#hyj_textarea{
	resize: none;
	width: 700px;
	height: 500px;
	
}
</style>
</head>
<body>
	<div id="all">
		<div class="sideCustomerCenter" style="float: left;">
			<table>
				<tr>
					<td>
						<table>
							<tr>
								<td
									width: 100px; style="font-family: 'Noto Sans KR', sans-serif;">고객센터</td>
							</tr>
							<tr>
								<td id="baa">FAQ</td>
							</tr>
							<tr>
								<td id="bba">문의하기</td>
							</tr>
							<tr>
								<td id="bba">신고</td>
							</tr>
							<tr>
								<td id="bba">- 신고접수</td>
                            </tr>
                            <tr>
                                <td id="bba">- 신고진행조회</td>
                            </tr>
						</table>
					</td>
				</tr>
			</table>
		</div>
		<form>
			<div class="inquiryBoard" style="float: left;">
				<div style="font-weight: bold; font-size: 24px; color:rgb(90, 132, 241);">신고접수</div>
				<hr style="border: 2px solid rgb(90, 132, 241);">
				<table class="board">
					<tr>
						<td><p style="font-weight: bold; font-size: 20px;">* 이용약관안내</p></td>
					</tr>
					<tr>
						<td><textarea id="hyj_textarea" readonly="readonly" disabled>
								
1. 연락처는 필요한 경우에만 공개로 설정하십시오.
사람인에서는 전화번호, 주소, e-메일, 휴대폰 번호, 홈페이지 주소를 각각 별도로 공개 설정할 수 있습니다.
비공개로 설정해두시면, 기업에서 유료서비스를 이용하여도 연락처를 확인할 수 없습니다. 모든 연락처를 비공개로 해주셔도, 기업에서 입사지원 요청은 보낼 수 있습니다.
또한, 연락처를 비공개로 설정해두신 경우에도 입사지원을 하시게 되면 지원하신 기업의 인사담당자는 모든 연락처를 볼 수 있습니다.
따라서, 꼭 필요한 연락처만 공개 설정해두시면 개인정보를 보호할 수 있습니다.

2. 취업이 되었을 경우에는 구직상태를 재직중으로 변경하시고 이력서를 비공개로 지정하십시오.
취업이 된 경우에도, 이력서가 공개 설정되어 있으면 기업으로부터 연락이 올 수 있습니다. 이력서를 비공개로 해두시면, 기업회원이 인재정보에서 회원님의 이력서를 검색할 수 없게 됩니다.
취업이 되셨으면, 개인서비스 > 회원정보 수정에서 구직상태를 재직중으로 변경하시고, 취업으로 구직활동을 중단하실 경우에는 반드시 이력서를 비공개로 지정하십시오.

3. 채용공고 제목이 명확하지 않거나 채용공고 내용이 충실하지 않은 경우, 지원하지 마십시오.
제목에는 구체적인 채용분야가 명확하게 기재된 경우가 일반적입니다.
불량기업일 경우, 채용공고 제목을 명확하게 기재하지 않습니다.
- 참신하고 유능한 인재를 모십니다
- 끈기 있고,성실한 패기 있는 인재를 모집합니다.
- 업계의 최고가 될 인재를 모십니다.
- 고액연봉을 꿈꾸십니까
- 억대연봉 꿈이 아닙니다
- 업계최고의 급여, 업계최고의 대우....
- 전문OO를 꿈꾸시는 분
- 월 000만 보장
- 사세확장으로 직원 대채용
- 즉시 출근 가능
위와 같이 구직자를 현혹하는 듯한 추상적인 제목은 불량기업들이 자주 사용하는 방법이므로, 특별히 유의하셔야 합니다. 최근에는 제목과 내용이 다른 경우도 많이 있으므로, 제목이 분명하더라도 내용을 반드시 꼼꼼히 검토해보신 후 지원하셔야 합니다.
또한 채용공고의 내용이 충실히 작성되지 않았을 경우에도 입사지원에 앞서 주의하셔야 합니다. 채용공고 내용에는 상세한 지원자격, 채용분야, 복리후생이 기재되어 있어야 합니다. 또한, 회사명, 인사담당자명이 정확히 명시되어 있어야 합니다. 지원하시기 전에 반드시 기업 정보를 검토해보시고 회사 홈페이지도 방문해 보신 후 지원하시기 바랍니다.
사람인 고객센터에서는 매일 올라오는 수천건의 채용정보를 일일히 검토해서 잘못된 정보를 삭제조치하고 있습니다. 불량기업이나, 허위 과장 광고라고 판단되실 때에는 사람인 고객센터로 신고해주시기 바랍니다.

4. 입사지원 요청이 들어왔을 때, 채용정보와 기업정보를 반드시 확인 후 지원하십시오.
이력서를 공개로 설정해 놓으시면, 회원님의 이력서가 인재정보에 노출되어 이틀동안 기업회원으로부터 입사지원 요청을 받을 수 있습니다. 기업회원이 유료서비스를 이용하면, 이틀이 지난 이력서에도 입사지원 요청을 할 수 있으며, 연락처 또한 확인 할 수 있게 됩니다.
입사지원 요청이 들어왔을 때에는 반드시 해당 채용정보를 클릭해서, 자신과 맞는 정보인지 확인하셔야 합니다. 또한, 채용정보와 기업정보를 꼼꼼히 읽어보셔서, 혹시 불량기업이거나 허위 과장 광고는 아닌지 검토하신 후, 지원하시기 바랍니다. 개인회원 여러분의 판단으로 지원하지 않으셔도 되므로, 원치 않는 채용정보 일 경우에는 지원하지 마십시오.
사람인 고객센터에서는 불량기업일 경우, 입사지원 요청 및 연락처 확인을 하지 못하도록 사전에 일일히 확인조치를 취하고 있습니다. 불량기업이나, 허위 과장 광고라고 판단되실 때에는 사람인 고객센터로 신고해주시기 바랍니다.

5. 채용분야과 상관없는 업종 및 직종이 선택된 채용정보에 지원하지 마십시오.
사람인은 기업의 채용공고 등록시 업종 1개, 직종 3개까지 선택할 수 있습니다.
이것은 채용시 여러 부문의 채용이 동시에 발생하는 경우에 효과적으로 활용할 수 있도록 하기 위함입니다.
그러나, 일부 기업의 경우 이를 악용하여 채용분야와 상관없이 특정 직종에 노출하기 위한 목적으로 채용분야과 상관없는 인기 직종이나 업종을 지정하는 경우가 있습니다. 이런 경우에는 허위 과장 광고이므로 지원하지 마십시오.
사람인 고객센터에서는 꾸준히 이러한 경우에 대응하기 위해 실시간으로 필터링을 하고 있지만 현실적으로 모든 사례에 대한 필터링은 어렵습니다. 채용분야와 업직종 선택이 일치하지 않은 경우, 사람인 고객센터로 신고해주시기 바랍니다.

6. 동일 기업명으로 여러 채용공고가 등록된 경우를 조심하십시오.
전국적으로 많은 지사를 통해 상시적으로 채용이 이루어지고 있는 기업들이 많습니다.
사람인은 이 경우 기업명과 함께 구체적인 지사명을 함께 밝혀야만 채용공고 등록이 가능하도록 하고 있습니다.
동일 기업명으로 채용공고가 다수 등록되어 있으면서 지사명이 구체적으로 밝혀져 있지 않을 경우 회원님의 주의가 요망됩니다.

7. 지원자격 제한이 '사실상 없는' 경우에도 조심하셔야 합니다.
지원자격을 완화하는 것은 분명 필요한 것이지만 간혹 학력, 연령, 경력, 근무지역 등 모든 조건과 상관없이(무관) 채용을 진행하는 채용공고가 있습니다.
이는 개인회원의 주의가 필요한 채용공고입니다. 이러한 채용공고의 경우 입사지원하는 개인회원의 신상정보 획득을 목적으로 허위 채용공고를 게재했을 가능성이 있습니다.

8. 접수 서류에 주민등록등본을 요구할 경우, 주의가 필요합니다.
최근들어 정상적인 채용시 서류전형 단계에서 주민등록등본을 요구하는 사례가 과거에 비해 현저히 줄어들었지만, 여전히 이러한 관례를 악용하여 개인정보 수집을 목적으로 입사지원시 이를 요구하는 채용기업들이 있습니다.
주민등록등본의 경우 채용 확정후에 제출하는 것이 가장 일반적이므로 입사지원시 제출을 요구할 경우 주의가 필요합니다.

9. 사람인 커뮤니티와 취업 자료실을 충분히 활용하시기 바랍니다.
사람인의 커뮤니티와 취업자료실(자료통)에는 취업 선배 또는 동일한 처지의 구직자들의 면접경험담과 지원시 주의해야할 기업, 주의사항 등의 많은 정보들이 공유되고 있습니다. 이 곳을 통해 여러분이 조심해야 하는 '취업 요주의 기업'에 대한 정보를 공유하실 수 있습니다.

						</textarea></td>

					</tr>
				</table>
				<div id="fileArea">
					<input type="file" id="thumbnailImg" name="thumbnailImg"
						onchange="loadImg(this, 1);">
				</div>
				<div id="btnArea">
					<button class="btn1" type="submit">신고하기</button>
					<button class="btn2" onclick="hyj_gohome();">메인으로</button>
				</div>
			</div>
		</form>
	</div>
	<%@ include file="../common/cho_footer.jsp"%>
	<script>
        $(function(){
            $("#fileArea").hide();

            $("#contentImgArea").click(function(){
   				$("#thumbnailImg").click();
   			});
        });

		function hyj_gohome(){
			location.href="<%=request.getContextPath()%>/index.jsp";
		};

    </script>
</body>
</html>