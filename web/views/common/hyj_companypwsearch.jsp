<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.0.min.js" ></script>
    <script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <style>
        .login-msg-head1:hover, .login-msg-head2:hover{
           color:#5A84F1;
           cursor:pointer;
           text-decoration: underline;
           
        }
        div{
            text-align: center;
            color:black;
        }

        a:link { 
            text-decoration: none;
            color: #7A7A7A;
        }
        a:visited { 
            text-decoration: none;
            color: #7A7A7A;
        }

        input::placeholder {
            color:rgb(180, 179, 179);
            font-weight: bold;
            font-size: 12pt;
        }

        .article1{
            height:15%;
        }
        .article2{
            height:10%;
     
        }
        .article3{
            height:60%;
        }
        .article4{
        	margin-top:327px;
        	margin-left:-30px;
            height:60%;
            
        } 
        #contents{
            margin-left: auto;
            margin-right: auto;
            width:1400px;
            height:550px;
        }
        nav{
            width:20%;
            height:100%;
            float:left;
        }
        #section{
            width:60%;
            height:100%;
            float:left;
        }

        .login-msg{
            font-weight: bold;
            font-size:9pt;
            margin: 0px;
        }

        .login-space1, .login-space2{
            height:100%;
            width:55%;
            margin-left: auto;
            margin-right: auto;
            background: rgb(255, 255, 255);
        }
        .login-space-empty{
            width:5%;
            height:100%;
            float:left;
        }
        .login-space-main{
            width:90%;
            height:100%;
            float:left;

        }
        .member-type-wrap{
            width:100%;
            height:30%;
        }
        .member-type-choice-wrap{
            width:100%;
            height:70%;
            padding-top:1.5%;
        }
        .member-type-per{
            width:49%;
            height:100%;
            float:left;
            border:0.5px solid gray;
        }
        .member-type-per:hover{
            color:#5A84F1;
            cursor:pointer;
        }
        .member-type-per:visited{
            background-color: rgb(115, 115, 255);
        }
        .member-type-com{
            width:50%;
            height:100%;
            float:left;
            border:0.5px solid gray;
        }
        .member-type-com:hover{
            color:#5A84F1;
            cursor:pointer;
        }
        .login-form{
            width:100%;
            height:40%;
            background: black;
            float: left;
        }
        .login-box{
            width:82%;
            height:100%;
            margin:0px;
            float:left;
            border:thin;
            background:red;
        }
        #id, #pwd, #comname2, #comname1, #comphone, #email, #comid1, #comid2 ,#comemail{
            height:98%;
            width:120%;
            padding-left:5px;
            background: rgb(255, 255, 255);
        }
        aside{
            width:20%;
            height:100%;
            float:left;
        }
        footer{
            margin-left: auto;
            margin-right: auto;
            width:1400px;
            height:100px;
        }
        #hyj_compw_email, #hyj_compw_phone{
            background: rgb(72, 118, 239);
            border-radius: 5px;
            color: black;
            width: 100px;
            height: 50px;
            margin-top: 15px;
        }
       	#hyj_idsearch-email{
       	   width: 410px;
           height: 140px;
       	}
       #hyj_idsearch-phone{
       	   width: 410px;
           height: 140px;
           margin-top: -231px;
           margin-left: 23px;
       	}
    	#popupDiv, #popupDiv2 {  /* 팝업창 css */
          top : 0px;
          position: absolute;
          background: white;
          width: 500px;
          height: 350px;
          display: none; 
       }
       #pwpopupdiv, #pwpopupdiv3{
          top : 0px;
          position: absolute;
          background: white;
          width: 500px;
          height: 350px;
          display: none; 
           
       }
       ul {
            list-style:none;
            margin:0;
            padding:0;
        }

        li {
            float: left;
            margin-right: 10px;
        }
       #popup_mask, #popup_mask2 { /* 팝업 배경 css */
           position: fixed;
           width: 100%;
           height: 1000px;
           top: 0px;
           left: 0px;
            display: none; 
            background-color:#000;
            opacity: 0.8;
       }
       #popup_mask1, #popup_mask3{
           position: fixed;
           width: 100%;
           height: 1000px;
           top: 0px;
           left: 0px;
            display: none; 
            background-color:#000;
            opacity: 0.8;
      
       }
       
           #phone-sms, #phone-sms2{
            width:98%;
            height:35px;
            padding:0px 0 0 5px;
            font-size: 15pt;
            border: 1px solid #5A84F1;
            border-radius: 5px;
        }
        .phone-sms, .phone-sms2{
            width:40%;
            height:31px;
            padding:4px 0 0 5px;
            font-size: 15pt;
            border: 1px solid #5A84F1;
            border-radius: 5px;
        }
        #phone-sms-btn, #phone-sms-btn1, #phone-sms-btn2, #phone-sms-btn3, #popCloseBtn, #popCloseBtn2{
           background:#5A84F1;
            color:white;
            font-weight:500;
            font-size: 12pt;
            border-radius: 5px;
            border:0;
            outline:0;
            width:95px;
            height:37px;
        }
        
        #newpwbtn, #newpwbtn3{
        width:100px;
        height:30px;
        }
    </style>
</head>
<body>

	<%@ include file="../common/menubar.jsp" %>
    <div id="contents"><!-- 전체 화면 div 시작 -->

        <nav id="nav"></nav>

        <div id="section">
            <div class="article1"></div>
			<form id="updateForm" action="<%=request.getContextPath() %>/compwsearch.me" method="POST">
            <div class="article2"><!-- 로그인 메시지 시작 -->
                <table style="background: rgb(255, 255, 255); margin-left: 320px; margin-right: auto; font-size: 20px;">
                        <tr>
                            <td>
                                <h3 class="login-msg-head1">개인</h3>
                            </td>
                            <td>
                                &nbsp;&nbsp;&nbsp;&nbsp;
                            </td>
                            <td>
                                <h3 class="login-msg-head2">기업</h3>
                            </td>
                        </tr>
                 </table>
            </div><!-- 로그인 메시지 끝 -->
            <hr style="margin:20px 0px 20px">
            <div class="article3" id="article3">
                <div class="login-space1">
                    <div class="login-space-empty"></div>
                    <div class="login-space-main">
                        <div class="member-type-wrap">
                            <div class="member-type-choice-wrap">
                                <div class="member-type-per">
                                    <p style="margin:24px;" id="hyj_emailsearch" class="hyj_emailsearch">이메일로 찾기</p>
                                </div>
                                <div class="member-type-com">
                                    <p style="margin:24px;" id="hyj_phonesearch" class="hyj_phonesearch">휴대폰번호로 찾기</p>
                                </div>
                            </div>
                        </div>
                        <div id="hyj_idsearch-email">
                        <div style="float: left; color: gray; font-size: 16px;" id="hyj_username1">이름</div>
                       	 <div class="login-form">
                          	  <div class="login-box">
                          	      <input type="text" id="comname1" name="comname1" class="txt_tool" placeholder="이름" value="">
                          	      <input type="hidden" value="1" name="valnum">
                       	     </div>
                      	  </div>
                        <div style="float: left; color: gray; font-size: 16px;" id="hyj_userid1">아이디</div>
                        <div class="login-form">
                            <div class="login-box">
                                <input type="text" id="comid1" name="comid1" class="txt_tool" placeholder="아이디" value="">
                            </div>
                        </div>
                        <div style="float: left; color: gray; font-size: 16px;" id="hyj_useremail" name="hyj_useremail">이메일</div>
                        <div class="login-form">
                            <div class="login-box">
                                <input type="email" id="comemail" name="comemail" class="txt_tool" placeholder="www.naver.com" value="">
                         </div>
                             <input id="hyj_compw_email" type="button" name="emailbtn" value="인증번호">
                <div style="margin-top: 10px;">이미계정이 있으신가요?
                    <div style="color: blue;" id="login" onclick="login();">[로그인]</div>
                </div>
                <hr style="margin:20px 0px 20px">
                        </div>
                    </div>
                </div>
                </div>
            </div>
            <div id ="popup_mask" ></div>
          <div id="popupDiv">
             <div class="phone-sms-area" style="width:100%; height:100%; position: relative;">
                <h4 align="center" style="margin:auto; margin-top:45px;">인증번호 발송 안내</h4>
                <hr style="width:30%;">
                <div style="width:100%; height:39px; margin-top:40px; margin-bottom:40px;">
                <ul style="display: table; margin: auto; margin-left:7%; padding:0; float:left; width:76%;">
                   <li><h4 style="margin:auto;  margin-top:10%; color:gray; vertical-align:middle">인증번호 입력</h4></li>
                   <li><input id="phone-sms" type="text" name="phoneSms" placeholder="인증번호"  style="vertical-align:middle;"></li>
                </ul>
                   <span style="width:20%;"><p id="timer" style="color:red;  margin:0; margin-top:8.7px; width:10%; height:50%; float:left;">2:59</p></span>
                 </div>
                 <div style="width:100%; height:50px; color:gray; text-align:center; margin-bottom:30px;">
                    <h5 id="timerOver1">제한 시간 안에 인증번호를 입력해주세요.<br>시간 초과 시 창이 닫힙니다.</h5>
                    <h5 id="timerOver2" style="display:none; color:red;">인증번호가 맞지 않습니다.</h5>
                 </div>
                 <div id="btn-area" style="width:40%; height:38px; margin:auto; text-align:center;" >
                    <input id="phone-sms-btn" type="button" name="phone-sms-btn" value="확인">
                    <input id="popCloseBtn" type="button" name="popCloseBtn" value="닫기" style="background:gray;">
                 </div>
              </div>
          </div>
    
    
     <div id="popup_mask1"></div>
          <div id="pwpopupdiv" style="width:400px; height:250px; magin:auto; border:2px solid gray;">
          	<div class="pwseach">
          		<table style="height:250px; width:400px;">
          			<tr>
          				<th style="text-align:center; border-bottom:1px solid gray;" colspan="2">새 비밀번호 입력</th>
          			</tr>
          			<tr>
          				<td>새 비밀번호 : </td>
          				<td><input type="password" name="perPwd1" id="perPwd1" placeholder="비밀번호입력"></td>
          			</tr>
          			<tr>
          				<td>새 비밀번호 확인 : </td>
          				<td><input type="password" name="perPwd2" id="perPwd2" placeholder="비밀번호입력"></td>
          			</tr>
					<tr>
						<td colspan="2" style="border-top:1px solid gray;"><button type="submit" value="확인" id="newpwbtn" name="newpwbtn">확인</button>
						<input type="hidden" value="1" name="valnum"></td>
					</tr>
					        
          		</table>
          	</div>
          </div>
        </form>
            
           
            <!--------------------------------------------------휴대전화---------------------------------------->
            <form id="updateForm" action="<%=request.getContextPath() %>/compwsearch.me" method="POST">
            <div class="article4" id="article4" >
            	   <div class="login-space2">
                    <div class="login-space-main">
                      
    				<div id="hyj_idsearch-phone">
                        <div style="float: left; color: gray; font-size: 16px;" id="hyj_username2">이름</div>
                        <input type="hidden" value="2" name="valnum">
                        <div class="login-form">
                            <div class="login-box">
                                <input type="text" id="comname2" name="comname2" class="txt_tool" placeholder="이름" value="">
                            </div>
                        </div>
                        <div style="float: left; color: gray; font-size: 16px;" id="hyj_userid2">아이디</div>
                        <div class="login-form">
                            <div class="login-box">
                                <input type="text" id="comid2" name="comid2" class="txt_tool" placeholder="아이디" value="">
                            </div>
                        </div>
                        <div style="float: left; color: gray; font-size: 16px;" id="hyj_userephone" name="hyj_userephone">핸드폰번호</div>
                        <div class="login-form">
                            <div class="login-box">
                                <input type="text" id="comphone" name="comphone" class="txt_tool" placeholder="010-0000-1111" value="">
                            </div>
                            <input id="hyj_compw_phone" type="button" name="emailbtn" value="인증번호">
                <div style="margin-top: 10px;">이미계정이 있으신가요?
                    <div style="color: blue;" id="login"  onclick="login();">[로그인]</div>
                </div>
                <hr style="margin:20px 0px 20px">
                        </div>
                    </div>
                </div>
                </div>

            </div>
             

	
	</div>
        <aside></aside>
        
    </div><!-- 전체 화면 div 끝 -->
    
     <div id ="popup_mask2" ></div>
          <div id="popupDiv2">
             <div class="phone-sms-area2" style="width:100%; height:100%; position: relative;">
                <h4 align="center" style="margin:auto; margin-top:45px;">인증번호 발송 안내</h4>
                <hr style="width:30%;">
                <div style="width:100%; height:39px; margin-top:40px; margin-bottom:40px;">
                <ul style="display: table; margin: auto; margin-left:7%; padding:0; float:left; width:76%;">
                   <li><h4 style="margin:auto;  margin-top:10%; color:gray; vertical-align:middle">인증번호 입력</h4></li>
                   <li><input id="phone-sms2" type="text" name="phoneSms2" placeholder="인증번호"  style="vertical-align:middle;"></li>
                </ul>
                   <span style="width:20%;"><p id="timer2" style="color:red;  margin:0; margin-top:8.7px; width:10%; height:50%; float:left;">2:59</p></span>
                 </div>
                 <div style="width:100%; height:50px; color:gray; text-align:center; margin-bottom:30px;">
                    <h5 id="timerOver12">제한 시간 안에 인증번호를 입력해주세요.<br>시간 초과 시 창이 닫힙니다.</h5>
                    <h5 id="timerOver22" style="display:none; color:red;">인증번호가 맞지 않습니다.</h5>
                 </div>
                 <div id="btn-area2" style="width:40%; height:38px; margin:auto; text-align:center;" >
                    <input id="phone-sms-btn2" type="button" name="phone-sms-btn2" value="확인">
                    <input id="popCloseBtn2" type="button" name="popCloseBtn2" value="닫기" style="background:gray;">
                 </div>
              </div>
          </div>
  
            
              <div id="popup_mask3"></div>
          <div id="pwpopupdiv3" style="width:400px; height:250px; magin:auto; border:2px solid gray;">
          	<div class="pwseach3">
          		<table style="height:250px; width:400px;">
          			<tr>
          				<th style="text-align:center; border-bottom:1px solid gray;" colspan="2">새 비밀번호 입력</th>
          			</tr>
          			<tr>
          				<td>새 비밀번호 : </td>
          				<td><input type="password" name="perPwd1" id="perPwd1" placeholder="비밀번호입력"></td>
          			</tr>
          			<tr>
          				<td>새 비밀번호 확인 : </td>
          				<td><input type="password" name="perPwd2" id="perPwd2" placeholder="비밀번호입력"></td>
          			</tr>
					<tr>
						<td colspan="2" style="border-top:1px solid gray;"><button type="submit" value="확인" id="newpwbtn3" name="newpwbtn3">확인</button>
						<input type="hidden" value="2" name="valnum"></td>
					</tr>
					        
          		</table>
          	</div>
          	
          </div>
		 </form>
    
    
	<script>
   	function login(){
   		location.href="<%=request.getContextPath()%>/views/common/jyh_login.jsp";
   	}
	
	
	
    var randomVal;
    //이벤트 컨트롤을위한 변수
    var searchpw = 0;
    //값을 보내기 위한 선언
		function idsearch(){
			$("#idserachform").submit();
		};
	//한페이지  hide show
		$(function(){
			$("#hyj_idsearch-phone").hide();
			$("#hyj_phonesearch").click(function(){
				$("#hyj_idsearch-email").hide();
				$("#hyj_idsearch-phone").show();
			})
			$("#hyj_emailsearch").click(function(){
				$("#hyj_idsearch-phone").hide();
				$("#hyj_idsearch-email").show();
			});
		});
		//개인 / 기업 페이지를 이동하기위한
		$(document).ready(function(){
			$(".login-msg-head1").on("click", function(){
				location.href="<%=request.getContextPath()%>/views/common/hyj_personpwsearch.jsp";
			})
		})
		
		
		//==============================================================================================
		$("#phone-sms-btn").on("click", function(){ // 인증번호 팝업창의 확인 버튼을 눌렀을 경우
                           
         var userSms = $("#phone-sms").val();
                           
         if(userSms == randomVal){          //인증번호가 같은경우
            console.log("같다");
            clearInterval(timer);
            min = 2;
            sec = 59;
            $("#popup_mask").css("display","none"); //팝업창 뒷배경 display none
            $("#popupDiv").css("display","none"); //팝업창 display none
            $("body").css("overflow","auto"); //body 스크롤바 생성
            $("#timer").show();
            $("#timerOver1").show();
            $("#timerOver2").hide();
              alert("인증성공");
              searchpw = 1;
            $("#phone-sms").val("");
            if(searchpw > 0 ){
                    	
            $("#pwpopupdiv").css({
              "top": (($(window).height()-$("#popupDiv").outerHeight())/2+$(window).scrollTop())+"px",
              "left": (($(window).width()-$("#popupDiv").outerWidth())/2+$(window).scrollLeft())+"px"
               //팝업창을 가운데로 띄우기 위해 현재 화면의 가운데 값과 스크롤 값을 계산하여 팝업창 CSS 설정
               });   	
  			$("#popup_mask1").css("display", "block");
  			$("#pwpopupdiv").css("display", "block");
            $("#newpwbtn").on("click", function(){
                      		
               var newpassword1 = $("#newpassword1").val();
               var newpassword2 = $("#newpassword2").val();

               console.log(newpassword1);
               console.log(newpassword2);
              //값을 입력받기 위한 간단한 알림
            if(newpassword1 == ""){
           
               $("#newpassword1").focus();
               
            }else if(newpassword2 == ""){
             
             $("#newpassword2").focus();
             
            }else if(newpassword1 == newpassword2){
          	$("#popup_mask1").css("display", "none");
          	$("#pwpopupdiv").css("display", "none");				
            }
			})
			}
            }else{                        //인증번호가 같은 경우
            console.log("다르다");
            $("#timerOver2").show();
            $("#timerOver1").hide();
            }
			});
                        
		
		///////////////////////////////////////////////////////////////////////////////
		  $("#phone-sms-btn2").on("click", function(){ // 인증번호 팝업창의 확인 버튼을 눌렀을 경우
			  searchpw = 0;
        var userSms1 = $("#phone-sms2").val();
                       
         if(userSms1 == randomVal){          //인증번호가 다를 경우
          console.log("같다");
          
           min = 2;
           sec = 59;
            $("#popup_mask2").css("display","none"); //팝업창 뒷배경 display none
               $("#popupDiv2").css("display","none"); //팝업창 display none
               $("body").css("overflow","auto"); //body 스크롤바 생성
             $("#timer2").show();
             $("#timerOver12").show();
            $("#timerOver22").hide();
               alert("인증성공");
               searchpw = 1;
               if(searchpw > 0){
                   $("#pwpopupdiv3").css({
                       "top": (($(window).height()-$("#popupDiv2").outerHeight())/2+$(window).scrollTop())+"px",
                       "left": (($(window).width()-$("#popupDiv2").outerWidth())/2+$(window).scrollLeft())+"px"
                       //팝업창을 가운데로 띄우기 위해 현재 화면의 가운데 값과 스크롤 값을 계산하여 팝업창 CSS 설정
                    }); 
               	
						$("#popup_mask3").css("display", "block");
						$("#pwpopupdiv3").css("display", "block");

	

               	$("#newpwbtn3").on("click", function(){
               		
                     	var newpassword1 = $("#perPwd1").val();
                   	var newpassword2 = $("#perPwd2").val();

                   	console.log(newpassword1);
                   	console.log(newpassword2);
               		if(newpassword1 == ""){
               		
               			$("#perPwd1").focus();
               		}else if(newpassword2 == ""){
               		
               			$("#perPwd2").focus();
               		}else if(newpassword1 == newpassword2){
   						$("#popup_mask3").css("display", "none");
   						$("#pwpopupdiv3").css("display", "none");
              		}
              	})
              }
             }else{                        //인증번호가 같은 경우
             console.log("다르다");
           $("#timerOver22").show();
          $("#timerOver12").hide();
         }
          
      });
      
      //=========================버튼 클릭시 유효성검사===========================
         //--------------------------------------이메일 인증 시작-------------------------------------------
         $("#hyj_compw_email").on("click", function(){
         
          var getMail = RegExp(/^[A-Za-z0-9_\.\-]+@[A-Za-z0-9\-]+\.[A-Za-z0-9\-]+/);
          var email = $("#comemail").val();
          var compwem = 0;
          min = 2;
          sec = 59;

          if($("#comname1").val() == ""){
             alert("이름입력바랍니다");
             $("#comname1").focus();
             
          }else if($("#comemail").val() == ""){
             alert("이메일을 입력바랍니다");
             $(email).focus();
             
          }else if(!getMail.test($("#comemail").val())){
             
                 $(email).val("");
                 $(email).focus();
                
          }else if(getMail.test($("#comemail").val())){
        	  compwem = 1;
        	  if(compwem > 0){
                  var comname = $("#comname1").val();
                  var comid = $("#comid1").val();
                  var comemail = $("#comemail").val();
                  console.log(comname);
                  console.log(comid);
                  console.log(comemail);
                  
                  $.ajax({
                      async:false,
                      url:"/itda/compwemail.co",
                      data:{
                    	  comname:comname,
                    	  comemail:comemail,
                    	  comid:comid
                      },
                      type:"POST",
                      success:function(data){
                    	  alert("고객정보확인")
                          if(getMail.test(email)){
                              var timer = setInterval(function(){
                                 if(sec > 10){
                                    $("#timer").text(String(min) + ":" + String(sec));
                                    sec--;
                                 }else if(sec >= 0 ){
                                    $("#timer").text(String(min) + ":0" + String(sec));
                                    sec--;
                                 }else if(min != 0){
                                    min--;
                                    sec = 60;
                                 }else{
                                    clearInterval(timer);
                                    min = 2;
                                    sec = 59;
                                    $("#popup_mask").css("diplay", "none");
                                          $("#popupDiv").css("display","none"); //팝업창 display none
                                          $("body").css("overflow","auto");//body 스크롤바 생성
                                          $("#timer").show();
                                          $("#timerOver1").show();
                                         $("#timerOver2").hide();
                                       }
                                    }, 1000);
                               $("#popupDiv").css({
                                       "top": (($(window).height()-$("#popupDiv").outerHeight())/2+$(window).scrollTop())+"px",
                                       "left": (($(window).width()-$("#popupDiv").outerWidth())/2+$(window).scrollLeft())+"px"
                                       //팝업창을 가운데로 띄우기 위해 현재 화면의 가운데 값과 스크롤 값을 계산하여 팝업창 CSS 설정
                                    }); 
                                   
                                   $("#popup_mask").css("display","block"); //팝업 뒷배경 display block
                                   $("#popupDiv").css("display","block"); //팝업창 display block
                                   
                                   $("body").css("overflow","hidden");//body 스크롤바 없애기
                               
                               $("#popCloseBtn").click(function(event){
                                   $("#popup_mask").css("display","none"); //팝업창 뒷배경 display none
                                   $("#popupDiv").css("display","none"); //팝업창 display none
                                   $("body").css("overflow","auto");//body 스크롤바 생성
                                   $("#timer").show();
                                   $("#timerOver1").show();
                                  $("#timerOver2").hide();
                                  clearInterval(timer);
                               });
                             //메일 ajax
                             $.ajax({
                               url: "/itda/sendMail.se",
                               data: {
                                  email: email
                               },
                               type: "post",
                               success: function(data) {
                                  
                                  randomVal = data;
                                  console.log(data);
                                  
                               },
                               error: function(error) {
                                  console.log(error);
                               } 
                            });
                           }
                    
                    
                      },
                      error:function(error){
                         alert("없는 회원입니다!");
                         console.log(error);
                      }
                      
                   })
        	  }
        	  
          }
           
         });
            //--------------------------------------이메일 인증 끝-------------------------------------------
            //--------------------------------------성공한 인증 후 회원 아이디 불러오기---------------------------
            
 
         
            
      
            //--------------------------------------휴대폰 인증 시작-------------------------------------------
                		  var min = 2;
                        var sec = 59;
                        var timerId = null;
                        var compwph = 0;

                        

                		$("#hyj_compw_phone").on("click", function(){  // 핸드폰의 인증번호 버튼을 눌렀을 경우
                			var regExp =/(01[016789])([1-9]{1}[0-9]{2,3})([0-9]{4})$/;
                			var phone = $("#comphone").val();
                			
                	          if($("#comname2").val() == ""){
                	              alert("이름입력바랍니다");
                	              $("#comname2").focus();
                	              
                	           }else if($("#comphone").val() == ""){
                	              alert("핸드폰번호를 입력바랍니다");
                	              $("#comphone").focus();
                	              
                	           }else if($("#comid2").val() == ""){
                 	              alert("아이디를 입력바랍니다");
                	              $("#comid2").focus();
                	           }else if(regExp.test(phone)){
                	        	   compwph = 1;
                	        	   if(compwph > 0){
                	        		   var comname = $("#comname2").val();
            	                       var comid = $("#comid2").val();
            	                       var comphone = $("#comphone").val();
            	                       console.log(comname);
            	                       console.log(comid);
            	                       console.log(comphone);
                	                   $.ajax({
                	                       async:false,
                	                       url:"/itda/compwphone.me",
                	                       data:{
                	                     	  comname:comname,
                	                     	  comphone:comphone,
                	                     	  comid:comid
                	                       },
                	                       type:"POST",
                	                       success:function(data){
                	                     	  alert("고객정보확인")
 
                	        	        		if(regExp.test(phone)){
                	        	        			
                	        	        			function timer2(){
                	                                	if(sec > 10){
                	                   	        			$("#timer2").text(String(min) + ":" + String(sec));
                	                   	        			sec--;
                	                   	        			timerId=setTimeout(timer2,1000);
                	                   	        		}else if(sec >= 0){
                	                   	        			$("#timer2").text(String(min) + ":0" + String(sec));
                	                   	        			sec--;
                	                   	        			timerId=setTimeout(timer2,1000);
                	                   	        		}else if(min != 0){
                	                   	        			min--;
                	                   	        			sec = 60;
                	                   	        			timerId=setTimeout(timer2,1000);
                	                   	        		}else{
                	                   	        			min = 2;
                	                   	        			sec = 59;
                	                   	        			$("#popup_mask2").css("display","none"); //팝업창 뒷배경 display none
                	                           	            $("#popupDiv2").css("display","none"); //팝업창 display none
                	                           	            $("body").css("overflow","auto");//body 스크롤바 생성
                	                           	            $("#timer2").show();
                	                           	            $("#timerOver12").show();
                	                                   		$("#timerOver22").hide();
                	                   	        		}
                	                                }
                	        	        			
                	        	        			timer2();
                	                	             $("#popupDiv2").css({
                	                	                "top": (($(window).height()-$("#popupDiv2").outerHeight())/2+$(window).scrollTop())+"px",
                	                	                "left": (($(window).width()-$("#popupDiv2").outerWidth())/2+$(window).scrollLeft())+"px"
                	                	                //팝업창을 가운데로 띄우기 위해 현재 화면의 가운데 값과 스크롤 값을 계산하여 팝업창 CSS 설정
                	                	             }); 
                	                	            
                	                	            $("#popup_mask2").css("display","block"); //팝업 뒷배경 display block
                	                	            $("#popupDiv2").css("display","block"); //팝업창 display block
                	                	            
                	                	            $("body").css("overflow","hidden");//body 스크롤바 없애기
                	                	        
                	                	        $("#popCloseBtn2").click(function(event){
                	                	            $("#popup_mask2").css("display","none"); //팝업창 뒷배경 display none
                	                	            $("#popupDiv2").css("display","none"); //팝업창 display none
                	                	            $("body").css("overflow","auto");//body 스크롤바 생성
                	                	            $("#timer2").show();
                	                	            $("#timerOver12").show();
                	                        		$("#timerOver22").hide();
                	                        		clearTimeout(timerId);
                	                        		min = 2;
                	        	        			sec = 59;
                	        	        			okSign.hide();
                	                        	   /*  clearInterval(timer1); */
                	                	        });
                	                	        	
                	                			$.ajax({
                	            					url: "/itda/phoneCertification.ph",
                	            					data: {
                	            						phone: phone
                	            					},
                	            					type: "post",
                	            					success: function(data) {
                	            						
                	            						randomVal = data;
                	            						console.log(data);
                	            						
                	            					},
                	            					error: function(error) {
                	            						console.log(error);
                	            					} 
                	            				});
                	                			
                	                        	}else{
                	                        		alert("핸드폰 형식에 맞춰서 작성해주세요");
                	                        		
                	                        		}
                	                     
                	                       },
                	                       error:function(error){
                	                          alert("없는 회원입니다!");
                	                          console.log(error);
                	                       }
                	                       
                	                    })
                	        	   }
                	        	   
                	           }
  	
                	});
              //--------------------------------------휴대폰 인증 끝-------------------------------------------
   
		
		
		
		
	</script>	

		<div style="margin-top:100px;"><%@ include file="cho_footer.jsp" %></div>

</body>
</html>