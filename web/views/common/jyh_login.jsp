<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
<head>
    <meta charset="UTF-8">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <title>Document</title>
    <style>
        div{
            text-align: center;
            color:black;
        }

        a:link { 
            text-decoration: none;
            color: #7A7A7A;
        }
        a:visited { 
            text-decoration: none;
            color: #7A7A7A;
        }

        input::placeholder {
            color:rgb(180, 179, 179);
            font-weight: bold;
            font-size: 12pt;
        }

        .article1{
            height:15%;
        }
        .article2{
            height:10%;
        }
        .article3{
            height:60%;
        }
      /*   .article4{
            height:15%;
            background:red;
        } */


        #contents{
            margin-left: auto;
            margin-right: auto;
            width:1400px;
            height:550px;
        }
        nav{
            width:20%;
            height:100%;
            float:left;
        }
        section{
            width:60%;
            height:100%;
            float:left;
        }

        .login-msg-head{
            margin:0px; 
            padding-top:0.5%;
        }
        .login-msg{
            font-weight: bold;
            font-size:9pt;
            margin: 0px;
        }
        .login-msg1{
            color:#C2C2C2;
        }
        .login-msg2{
            color:#4876EF;
        }
        .login-msg3{
            color:#505050;
        }
        .login-space{
            height:100%;
            width:55%;
            float:left;
        }
        .login-space-empty{
            width:5%;
            height:100%;
            float:left;
        }
        .login-space-main{
            width:90%;
            height:100%;
            float:left;
        }
        .member-type-wrap{
            width:100%;
            height:30%;
        }
        .member-type-choice-wrap{
            width:100%;
            height:70%;
            padding-top:1.5%;
        }
        .member-type-per{
            width:49%;
            height:100%;
            float:left;
            border:0.5px solid gray;
        }
        .member-type-per:hover{
            color:#5A84F1;
            cursor:pointer;
        }
        .member-type-com{
            width:50%;
            height:100%;
            float:left;
            border:0.5px solid gray;
            border-left:none;
        }
        .member-type-com:hover{
            color:#5A84F1;
            cursor:pointer;
        }

        .login-form{
            width:100%;
            height:100%;
            margin:5% 0% 2%;
        }
        #loginForm{
        	width:100%;
            height:40%;
        }
        .login-box{
            width:70%;
            height:46.5%;
            margin:0px;
            float:left;
            border:thin;
        }
        #id, #pwd{
            height:98%;
            width:97%;
            padding-left:5px;
            margin:0px;
        }
        .btn-login{
            height:97%;
            width:30%;
            border: none;
            color: #fff;
            font-size: 16px;
            font-weight: bold;
            cursor: pointer;
            background:#4394F0;
        }
        .signup-forgotten{
            width:100%;
            height:10%;
        }
        .find-id-pwd{
            float:left;
            padding-left:10px;
        }
        .signup{
            float:right;
            padding-right:10px;
        }

        .ad-space{
            height:100%;
            width:45%;
            float:left;
        }
        .ad-text-space{
            margin-top:20%;
        }
        .ad-text1{
            font-weight:bold;
            font-size:28pt;
            color:#505050;
        }
        .ad-text1-1{
            font-weight:bold;
            font-size:28pt;
            color: #4394F0;
        }
        .ad-text2{
            font-weight:bold;
            font-size:18pt;
            color:#505050;
        }
        .ad-text3{
            font-weight:bold;
            font-size:18pt;
            color: #4394F0;
        }
        .ad-text4{
            font-weight:bold;
            font-size:18pt;
            color:#505050;
        }

        aside{
            width:20%;
            height:100%;
            float:left;
        }
        footer{
            margin-left: auto;
            margin-right: auto;
            width:1400px;
            height:100px;
        }
    </style>
</head>
<body>

	<%@ include file="../common/menubar.jsp" %>
	
    <div id="contents"><!-- 전체 화면 div 시작 -->

        <nav id="nav"></nav>

        <section id="section">
            <div class="article1"></div>
            <div class="article2"><!-- 로그인 메시지 시작 -->
                <h3 class="login-msg-head">로그인이 필요한 서비스 입니다.</h3>
                <span class="login-msg login-msg1">IT</span><span class="login-msg login-msg2">'DA</span><span class="login-msg login-msg3">의</span>
                <span class="login-msg login-msg3">서비스를 이용하고 싶으신가요? 지금 바로 회원가입 해주세요!</span>
            </div><!-- 로그인 메시지 끝 -->
            <hr style="margin:20px 0px 20px">
            <div class="article3">
                <div class="login-space">
                    <div class="login-space-empty"></div>
                    <div class="login-space-main">
                        <div class="member-type-wrap">
                            <div class="member-type-choice-wrap">
                                <div class="member-type-per">
                                    <p style="margin:24px 24px 0px 24px;">개인회원</p>
                                    <hr style='width:50%; border:1px solid #5A84F1;'>
                                </div>
                                <div class="member-type-com">
                                    <p style="margin:24px 24px 0px 24px;">기업회원</p>
                                    <hr style='width:50%; border:1px solid #5A84F1; display:none;'>
                                </div>
                            </div>
                        </div>
                        <form id="loginForm" action="<%= request.getContextPath() %>/login.me" method="post">
                        <div id="memChk"><input type="text" name="memChk" id="memNum" value="1" style="display:none;"></div>
                        <div class="login-form">
                            <div class="login-box">
                                <input type="text" id="id" name="perId" class="txt_tool" placeholder="아이디" value="">
                            </div>
                            <div class="login-box">
                                <input type="password" id="pwd" name="perPwd" class="txt_tool" placeholder="비밀번호" value="">
                            </div>
                            <button type="submit" class="btn-login">로그인</button>
                        </div>
                        </form>
                        <p class="signup-forgotten">
                            <a class="find-id-pwd" href="hyj_personidsearch.jsp">아이디 찾기</a> <span style="float:left; font-size:11pt; color: #ccc; margin:0px 5px 0px 15px;"> | </span>
                            <a class="find-id-pwd" href="hyj_personpwsearch.jsp">비밀번호 찾기</a>
                            <a class="signup" href="jyh_signUp.jsp">회원 가입</a>
                        </p>
                    </div>
                    <div class="login-space-empty"></div>
                </div>
                <div class="ad-space">
                    <div class="ad-text-space">
                        <span class="ad-text1">기업</span><span class="ad-text2">과</span><span class="ad-text1">사람</span><span class="ad-text2">을</span><span class="ad-text1-1">잇다</span><br>
                        <span class="ad-text3">IT 전문</span>
                        <span class="ad-text4">채용 서비스</span><br>
                        <img src="/itda/image/logo.PNG"style="height:30%; width:30%;">
                    </div>
                </div>
            </div>
            <hr style="margin:20px 0px 20px">
            <div class="article4"></div>
             
        </section>

        <aside></aside>

    </div><!-- 전체 화면 div 끝 -->

		<%@ include file="cho_footer.jsp" %>
		<script>
		$(function(){
      		$("#memNum").hide();
      		$(".member-type-per").click(function(){
      			$("#memNum").val("1");
      			console.log($("#memNum").val())
      			$(this).children("hr").show();
      			$(".member-type-com").children("hr").hide();
      		});
      		$(".member-type-com").click(function(){
      			$("#memNum").val("2");
      			console.log($("#memNum").val())
      			$(this).children("hr").show();
      			$(".member-type-per").children("hr").hide();
      		});
      	});
		</script>
</body>
</html>