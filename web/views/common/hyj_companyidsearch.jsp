<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.0.min.js" ></script>
    <script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <style>
        .login-msg-head1:hover, .login-msg-head2:hover{
        color:#5A84F1;
        cursor:pointer;
        text-decoration: underline; 
        }
        div{
            text-align: center;
            color:black;
        }

        a:link { 
            text-decoration: none;
            color: #7A7A7A;
        }
        a:visited { 
            text-decoration: none;
            color: #7A7A7A;
        }

        input::placeholder {
            color:rgb(180, 179, 179);
            font-weight: bold;
            font-size: 12pt;
        }

        .article1{
            height:15%;
        }
        .article2{
            height:10%;
     			
        }
        .article3{
            height:60%;
        }
        .article4{
            height:60%;
            
        } 
        #contents{
            margin-left: auto;
            margin-right: auto;
            width:1400px;
            height:550px;
        }
        nav{
            width:20%;
            height:100%;
            float:left;
        }
        #section{
            width:60%;
            height:100%;
            float:left;
        }

        .login-msg{
            font-weight: bold;
            font-size:9pt;
            margin: 0px;
        }

        .login-space1, .login-space2{
            height:100%;
            width:55%;
            margin-left: auto;
            margin-right: auto;
            background: rgb(255, 255, 255);
        }
        .login-space-empty{
            width:5%;
            height:100%;
            float:left;
        }
        .login-space-main{
            width:90%;
            height:100%;
            float:left;

        }
        .member-type-wrap{
            width:100%;
            height:30%;
        }
        .member-type-choice-wrap{
            width:100%;
            height:70%;
        
        }
        .member-type-per{
            width:49%;
            height:100%;
            float:left;
            border:0.5px solid gray;
        }
        .member-type-per:hover{
            color:#5A84F1;
            cursor:pointer;
        }
        .member-type-per:visited{
            background-color: rgb(115, 115, 255);
        }
        .member-type-com{
            width:50%;
            height:100%;
            float:left;
            border:0.5px solid gray;
        }
        .member-type-com:hover{
             color:white;
            cursor:pointer;
            background:#5A84F1;
            text-decoration: underline;
        }
        .login-form{
            width:100%;
            height:40%;
            background: black;
            float: left;
        }
        .member-type-per:hover{
            color:white;
            cursor:pointer;
            background:#5A84F1;
            text-decoration: underline;
        }
        .login-box{
            width:82%;
            height:100%;
            margin:0px;
            float:left;
            border:thin;
            background:red;
        }
        #id, #pwd, #comnameemail, #comnamephone, #comphone, #comemail{
            height:98%;
            width:120%;
            padding-left:5px;
            background: rgb(255, 255, 255);
        }
        aside{
            width:20%;
            height:100%;
            float:left;
        }
        footer{
            margin-left: auto;
            margin-right: auto;
            width:1400px;
            height:100px;
        }
        #hyj_comid_email, #hyj_comid_phone{
            background: rgb(72, 118, 239);
            border-radius: 5px;
            color: black;
            width: 100px;
            height: 50px;
            margin-top: 15px;
        }
          #hyj_idsearch-email{
             width: 410px;
           height: 140px;
          }
       #hyj_idsearch-phone{
             width: 410px;
           height: 140px;
           margin-top: -231px;
           margin-left: 23px;
          }
        #popupDiv, #popupDiv1 {  /* 팝업창 css */
          top : 0px;
          position: absolute;
          background: white;
          width: 500px;
          height: 350px;
          display: none; 
       }
        ul {
            list-style:none;
            margin:0;
            padding:0;
        }
        li {
            float: left;
            margin-right: 10px;
        }
       #popup_mask, #popup_mask1{ /* 팝업 배경 css */
           position: fixed;
           width: 100%;
           height: 1000px;
           top: 0px;
           left: 0px;
            display: none; 
            background-color:#000;
            opacity: 0.8;
       }
           #phone-sms, #phone-sms1{
            width:98%;
            height:35px;
            padding:0px 0 0 5px;
            font-size: 15pt;
            border: 1px solid #5A84F1;
            border-radius: 5px;
        }
        .phone-sms, .phone-sms1{
            width:40%;
            height:31px;
            padding:4px 0 0 5px;
            font-size: 15pt;
            border: 1px solid #5A84F1;
            border-radius: 5px;
        }
                #phone-sms-btn, #phone-sms-btn1, #popCloseBtn, #popCloseBtn1{
           background:#5A84F1;
            color:white;
            font-weight:500;
            font-size: 12pt;
            border-radius: 5px;
            border:0;
            outline:0;
            width:95px;
            height:37px;
        }
    </style>
</head>
<body>

   
   <%@ include file="../common/menubar.jsp" %>
    <div id="contents"><!-- 전체 화면 div 시작 -->

        <nav id="nav"></nav>

        <div id="section">
            <div class="article1"></div>
            <div class="article2"><!-- 로그인 메시지 시작 -->
                <table style="background: rgb(255, 255, 255); margin-left: auto; margin-right: auto; font-size: 20px;">
                        <tr>
                            <td>
                                <h3 class="login-msg-head1">개인</h3>
                            </td>
                            <td>
                                &nbsp;&nbsp;&nbsp;&nbsp;
                            </td>
                            <td>
                                <h3 class="login-msg-head2">기업</h3>
                            </td>
                        </tr>
                 </table>
            </div><!-- 로그인 메시지 끝 -->
            <hr style="margin:20px 0px 20px">
            <div class="article3" id="article3">
                <div class="login-space1">
                    <div class="login-space-empty"></div>
                    <div class="login-space-main">
                        <div class="member-type-wrap">
                            <div class="member-type-choice-wrap">
                                <div class="member-type-per">
                                    <p style="margin:24px;" id="hyj_emailsearch" class="hyj_emailsearch">이메일로 찾기</p>
                                </div>
                                <div class="member-type-com">
                                    <p style="margin:24px;" id="hyj_phonesearch" class="hyj_phonesearch">휴대폰번호로 찾기</p>
                                </div>
                            </div>
                        </div>
                        <div id="hyj_idsearch-email">
                        <div style="float: left; color: gray; font-size: 16px;" id="hyj_comname">이름</div>
                        <div class="login-form">
                            <div class="login-box">
                                <input type="text" id="comnameemail" name="comnameemail" class="txt_tool" placeholder="이름" value="">
                            </div>
                        </div>
                        <div style="float: left; color: gray; font-size: 16px;" id="hyj_comemail" name="hyj_comemail">이메일</div>
                        <div class="login-form">
                            <div class="login-box">
                                <input type="email" id="comemail" name="comemail" class="txt_tool" placeholder="aaa@naver.co.kr" value="">
                            </div>
                            <input id="hyj_comid_email" type="button" name="emailbtn" value="인증번호">
                <div style="margin-top: 10px;">이미계정이 있으신가요?
                    <div style="color: blue;" id="login(); ">[로그인]</div>
                </div>
                <hr style="margin:20px 0px 20px">
                        </div>
                    </div>
                </div>
                </div>
            </div>
           
            
            <!--------------------------------------------------휴대전화---------------------------------------->
            <div class="article4" id="article4" >
                  <div class="login-space2">
                    <div class="login-space-main">
                      
                <div id="hyj_idsearch-phone">
                        <div style="float: left; color: gray; font-size: 16px;" id="hyj_comname1">이름</div>
                        <div class="login-form">
                            <div class="login-box">
                                <input type="text" id="comnamephone" name="comnamephone" class="txt_tool" placeholder="이름" value="">
                            </div>
                        </div>
                        <div style="float: left; color: gray; font-size: 16px;" id="hyj_userephone" name="hyj_userephone">핸드폰번호</div>
                        <div class="login-form">
                            <div class="login-box">
                                <input type="text" id="comphone" name="comphone" class="txt_tool" placeholder="010-0000-1111" value="">
                            </div>
                            <input id="hyj_comid_phone" type="button" name="phonebtn" value="인증번호">
                <div style="margin-top: 10px;">이미계정이 있으신가요?
                    <div style="color: blue;" id="login(); "><로그인></div>
                </div>
                <hr style="margin:20px 0px 20px">
                        </div>
                    </div>
                </div>
                </div>

            </div>
             

  
   </div>
        <aside></aside>
        
    </div><!-- 전체 화면 div 끝 -->
      <div id ="popup_mask" ></div>
          <div id="popupDiv">
             <div class="phone-sms-area" style="width:100%; height:100%; position: relative;">
                <h4 align="center" style="margin:auto; margin-top:45px;">인증번호 발송 안내</h4>
                <hr style="width:30%;">
                <div style="width:100%; height:39px; margin-top:40px; margin-bottom:40px;">
                <ul style="display: table; margin: auto; margin-left:7%; padding:0; float:left; width:76%;">
                   <li><h4 style="margin:auto;  margin-top:10%; color:gray; vertical-align:middle">인증번호 입력</h4></li>
                   <li><input id="phone-sms" type="text" name="phoneSms" placeholder="인증번호"  style="vertical-align:middle;"></li>
                </ul>
                   <span style="width:20%;"><p id="timer" style="color:red;  margin:0; margin-top:8.7px; width:10%; height:50%; float:left;">2:59</p></span>
                 </div>
                 <div style="width:100%; height:50px; color:gray; text-align:center; margin-bottom:30px;">
                    <h5 id="timerOver1">제한 시간 안에 인증번호를 입력해주세요.<br>시간 초과 시 창이 닫힙니다.</h5>
                    <h5 id="timerOver2" style="display:none; color:red;">인증번호가 맞지 않습니다.</h5>
                 </div>
                 <div id="btn-area" style="width:40%; height:38px; margin:auto; text-align:center;" >
                    <input id="phone-sms-btn" type="button" name="phone-sms-btn" value="확인">
                    <input id="popCloseBtn" type="button" name="popCloseBtn" value="닫기" style="background:gray;">
                 </div>
              </div>
          </div>
          
          
           <div id ="popup_mask1" ></div>
          <div id="popupDiv1">
             <div class="phone-sms-area1" style="width:100%; height:100%; position: relative;">
                <h4 align="center" style="margin:auto; margin-top:45px;">인증번호 발송 안내</h4>
                <hr style="width:30%;">
                <div style="width:100%; height:39px; margin-top:40px; margin-bottom:40px;">
                <ul style="display: table; margin: auto; margin-left:7%; padding:0; float:left; width:76%;">
                   <li><h4 style="margin:auto;  margin-top:10%; color:gray; vertical-align:middle">인증번호 입력</h4></li>
                   <li><input id="phone-sms1" type="text" name="phoneSms" placeholder="인증번호"  style="vertical-align:middle;"></li>
                </ul>
                   <span style="width:20%;"><p id="timer" style="color:red;  margin:0; margin-top:8.7px; width:10%; height:50%; float:left;">2:59</p></span>
                 </div>
                 <div style="width:100%; height:50px; color:gray; text-align:center; margin-bottom:30px;">
                    <h5 id="timerOver11">제한 시간 안에 인증번호를 입력해주세요.<br>시간 초과 시 창이 닫힙니다.</h5>
                    <h5 id="timerOver21" style="display:none; color:red;">인증번호가 맞지 않습니다.</h5>
                 </div>
                 <div id="btn-area1" style="width:40%; height:38px; margin:auto; text-align:center;" >
                    <input id="phone-sms-btn1" type="button" name="phone-sms-btn" value="확인">
                    <input id="popCloseBtn1" type="button" name="popCloseBtn" value="닫기" style="background:gray;">
                 </div>
              </div>
          </div>
          
          
          
          
          
          
          
          
    
   <script>
   
  	function login(){
   		location.href="<%=request.getContextPath()%>/views/common/jyh_login.jsp";
   	}
   
    var randomVal;
    
    //인증성공시 컨트롤할수있게 선언
    var searchid = 0;
    
      //버튼 클릭시 정보를 보낼수있도록 submit
      function idsearch(){
         $("#idserachform").submit();
      };
      
	  //hide and show로 버튼 클릭시 이미지가 나오게 하는 방법
      $(function(){
         $("#hyj_idsearch-phone").hide();
         $("#hyj_phonesearch").click(function(){
            $("#hyj_idsearch-email").hide();
            $("#hyj_idsearch-phone").show();
         })
         $("#hyj_emailsearch").click(function(){
            $("#hyj_idsearch-phone").hide();
            $("#hyj_idsearch-email").show();
         });
      });
	
      //개인 / 기업 페이지 이동
      $(document).ready(function(){
         $(".login-msg-head1").on("click", function(){
            location.href="<%=request.getContextPath()%>/views/common/hyj_personidsearch.jsp";
         })
      })
      
      //인증팝업창에서 확인 버튼 클릭시 이벤트
      $("#phone-sms-btn").on("click", function(){ // 인증번호 팝업창의 확인 버튼을 눌렀을 경우
                           
      var userSms = $("#phone-sms").val();
                           
      if(userSms == randomVal){ //인증번호가 다를 경우
         console.log("같다");
         clearInterval(timer);
         min = 2;
         sec = 59;
         $("#popup_mask").css("display","none"); //팝업창 뒷배경 display none
         $("#popupDiv").css("display","none"); //팝업창 display none
         $("body").css("overflow","auto"); //body 스크롤바 생성
         $("#timer").show();
         $("#timerOver1").show();
         $("#timerOver2").hide();
         alert("인증성공"); //인증선공 알림창
         searchid = 1;  //인증성공이벤트를 위한 변수
         $("#phone-sms").val("");
                    
      if(searchid > 0 ){
    	 //입력한 이메일과 이름값을 가져옴
         var comnameemail = $("#comnameemail").val();
         var comemail = $("#comemail").val();
                
         console.log(comnameemail);
         console.log(comemail);
         	//고객정보를 찾아 띄우기위한 ajax
         	//입력한 정보를 바탕으로 고객정보를 조회하고
         	//조회한 결과에 따라 성공 실패를 선언함
              $.ajax({
                      async:false,
                      url:"/itda/idsearch.co",
                      type:"POST",
                      data:{
                      		comnameemail:comnameemail,
                            comemail:comemail
                           },
                           
                      success:function(data){
                      var result = decodeURIComponent(data.userid);
                      alert("고객님의 아이디는 : " + result + " 입니다!");
                           },
                           
                      error:function(error){
                      alert("없는 회원입니다!");
                           }
                       })
                    }
                 }else{      //인증번호가 다른경우
                 console.log("다르다");
               $("#timerOver2").show();
               $("#timerOver1").hide();
             }
          });
      
		 ////////////////////////////////////////////////////////////////////
      
    	  $("#phone-sms-btn1").on("click", function(){ // 인증번호 팝업창의 확인 버튼을 눌렀을 경우
          
          var userSms1 = $("#phone-sms1").val();
                         
           if(userSms1 == randomVal){          //인증번호가 다를 경우
            console.log("같다");
            
             min = 2;
             sec = 59;
              $("#popup_mask1").css("display","none"); //팝업창 뒷배경 display none
                 $("#popupDiv1").css("display","none"); //팝업창 display none
                 $("body").css("overflow","auto"); //body 스크롤바 생성
               $("#timer1").show();
               $("#timerOver11").show();
              $("#timerOver21").hide();
                 alert("인증성공");
                 searchid = 1;
                  
                  if(searchid > 0){
                     var comnamephone = $("#comnamephone").val();
                     var comphone = $("#comphone").val();
              
                     
                
                     $.ajax({
                        async: false,
                        url:"/itda/phidserch.co",
                        data:{
                        	comnamephone:comnamephone,
                        	comphone:comphone
                        },
                        type:"POST",
                        success:function(data){
                     	 var result =
                       		  decodeURIComponent(data.userid);
              
                          alert("회원님의 아이디는 : " + result + "입니다!");
                      
                      
                        },
                        error:function(error){
                           alert("없는 회원입니다!");
                           console.log(error);
                        }
                        
                        
                     })
                  }
    
               }else{                        //인증번호가 같은 경우
               console.log("다르다");
             $("#timerOver21").show();
            $("#timerOver11").hide();
           }
            
        });
      
      
      
      
      
      //=========================버튼 클릭시 유효성검사===========================
      //--------------------------------------이메일 인증 시작-------------------------------------------
         //이메일과 형식이 맞을경구 이벤트
         //인증번호 클릭시 유효성검사
         $("#hyj_comid_email").on("click", function(){
         //이메일형식에 맞추어 입력할수 있게하는 설정
          var getMail = RegExp(/^[A-Za-z0-9_\.\-]+@[A-Za-z0-9\-]+\.[A-Za-z0-9\-]+/);
          var coemid = 0;
         //유저가 입력한 이메일 값
          var email = $("#comemail").val();
          min = 2;
          sec = 59;
          
          
          //이름
          if($("#comnameemail").val() == ""){
             alert("이름입력바랍니다");
             $("#comnameemail").focus();
          //이메일
          }else if($("#comemail").val() == ""){
             alert("이메일을 입력바랍니다");
             $("#comemail").focus();
          //이메일형식과 입력값을 비교
          }else if(!getMail.test($("#comemail").val())){
                 $(email).val("");
                 $(email).focus();
          }
            
          var comnameemail = $("#comnameemail").val();
          var comemail = $("#comemail").val();

          $.ajax({
             async: false,
             url:"/itda/idsearch.co",
             data:{
            	 comnameemail:comnameemail,
             	comemail:comemail
             },
             type:"POST",
             success:function(data){
            	 alert("회원조회가 완료되었습니다");
            	 coemid = 1;
            	 
            	 if(coemid > 0){
            		 
                     if(getMail.test(email)){
                   	  //타이머 설정
                          var timer = setInterval(function(){
                             if(sec > 10){
                                $("#timer").text(String(min) + ":" + String(sec));
                                sec--;
                             }else if(sec >= 0 ){
                                $("#timer").text(String(min) + ":0" + String(sec));
                                sec--;
                             }else if(min != 0){
                                min--;
                                sec = 60;
                             }else{
                                clearInterval(timer);
                                min = 2;
                                sec = 59;
                                $("#popup_mask").css("diplay", "none");
                                      $("#popupDiv").css("display","none"); //팝업창 display none
                                      $("body").css("overflow","auto");//body 스크롤바 생성
                                      $("#timer").show();
                                      $("#timerOver1").show();
                                     $("#timerOver2").hide();
                                   }
                                }, 1000);
                           $("#popupDiv").css({
                                   "top": (($(window).height()-$("#popupDiv").outerHeight())/2+$(window).scrollTop())+"px",
                                   "left": (($(window).width()-$("#popupDiv").outerWidth())/2+$(window).scrollLeft())+"px"
                                   //팝업창을 가운데로 띄우기 위해 현재 화면의 가운데 값과 스크롤 값을 계산하여 팝업창 CSS 설정
                                }); 
                               
                               $("#popup_mask").css("display","block"); //팝업 뒷배경 display block
                               $("#popupDiv").css("display","block"); //팝업창 display block
                               
                               $("body").css("overflow","hidden");//body 스크롤바 없애기
                           
                           $("#popCloseBtn").click(function(event){
                               $("#popup_mask").css("display","none"); //팝업창 뒷배경 display none
                               $("#popupDiv").css("display","none"); //팝업창 display none
                               $("body").css("overflow","auto");//body 스크롤바 생성
                               $("#timer").show();
                               $("#timerOver1").show();
                              $("#timerOver2").hide();
                              clearInterval(timer);
                           });
                               
                         //메일인증보내는  ajax
                         $.ajax({
                           url: "/itda/sendMail.se",
                           data: {
                              email: email
                           },
                           type: "post",
                           success: function(data) {
                              
                              randomVal = data;
                              console.log(data);
                              
                           },
                           error: function(error) {
                              console.log(error);
                           } 
                        });
                	   }
 
            	 }
/*           	 var result =
            		  decodeURIComponent(data.userid);
               alert("회원님의 아이디는 : " + result + "입니다!"); */
           
           
             },
             error:function(error){
                alert("없는 회원입니다!");
                console.log(error);
             }
             
             
          })


        });
            //--------------------------------------이메일 인증 끝-------------------------------------------

            //--------------------------------------휴대폰 인증 시작-------------------------------------------
              		   var min = 2;
                        var sec = 59;

                        var timerId = null;

                        

                		$("#hyj_comid_phone").on("click", function(){  // 핸드폰의 인증번호 버튼을 눌렀을 경우
                			var regExp =/(01[016789])([1-9]{1}[0-9]{2,3})([0-9]{4})$/;
                			var phone = $("#comphone").val();
                			
                			
                	          if($("#comnamephone").val() == ""){
                	              alert("이름입력바랍니다");
                	              $("#comnamephone").focus();
                	              
                	           }else if($("#comphone").val() == ""){
                	              alert("핸드폰번호를 입력바랍니다");
                	              $("#comphone").focus();
                	              
                	           }

                	          var comnamephone = $("#comnamephone").val();
                              var comphone = $("#comphone").val();
                              var coidph = 0;
 
                              $.ajax({
                                 async: false,
                                 url:"/itda/phidserch.co",
                                 data:{
                                	 comnamephone:comnamephone,
                                 	comphone:comphone
                                 },
                                 type:"POST",
                                 success:function(data){
                                	 alert("인증완료되었습니다")
                                	 coidph = 1;
                                	 if(coidph > 0){
                                		 
                     	        		if(regExp.test(phone)){
                    	        			
                    	        			function timer1(){
                                            	if(sec > 10){
                               	        			$("#timer1").text(String(min) + ":" + String(sec));
                               	        			sec--;
                               	        			timerId=setTimeout(timer1,1000);
                               	        		}else if(sec >= 0){
                               	        			$("#timer1").text(String(min) + ":0" + String(sec));
                               	        			sec--;
                               	        			timerId=setTimeout(timer1,1000);
                               	        		}else if(min != 0){
                               	        			min--;
                               	        			sec = 60;
                               	        			timerId=setTimeout(timer1,1000);
                               	        		}else{
                               	        			min = 2;
                               	        			sec = 59;
                               	        			$("#popup_mask1").css("display","none"); //팝업창 뒷배경 display none
                                       	            $("#popupDiv1").css("display","none"); //팝업창 display none
                                       	            $("body").css("overflow","auto");//body 스크롤바 생성
                                       	            $("#timer1").show();
                                       	            $("#timerOver11").show();
                                               		$("#timerOver21").hide();
                               	        		}
                                            }
                    	        			
                    	        			timer1();
                            	             $("#popupDiv1").css({
                            	                "top": (($(window).height()-$("#popupDiv1").outerHeight())/2+$(window).scrollTop())+"px",
                            	                "left": (($(window).width()-$("#popupDiv1").outerWidth())/2+$(window).scrollLeft())+"px"
                            	                //팝업창을 가운데로 띄우기 위해 현재 화면의 가운데 값과 스크롤 값을 계산하여 팝업창 CSS 설정
                            	             }); 
                            	            
                            	            $("#popup_mask1").css("display","block"); //팝업 뒷배경 display block
                            	            $("#popupDiv1").css("display","block"); //팝업창 display block
                            	            
                            	            $("body").css("overflow","hidden");//body 스크롤바 없애기
                            	        
                            	        $("#popCloseBtn1").click(function(event){
                            	            $("#popup_mask1").css("display","none"); //팝업창 뒷배경 display none
                            	            $("#popupDiv1").css("display","none"); //팝업창 display none
                            	            $("body").css("overflow","auto");//body 스크롤바 생성
                            	            $("#timer1").show();
                            	            $("#timerOver11").show();
                                    		$("#timerOver21").hide();
                                    		clearTimeout(timerId);
                                    		min = 2;
                    	        			sec = 59;
                    	        			okSign.hide();
                                    	   /*  clearInterval(timer1); */
                            	        });
                            	        	
                            			$.ajax({
                        					url: "/itda/phoneCertification.ph",
                        					data: {
                        						phone: phone
                        					},
                        					type: "post",
                        					success: function(data) {
                        						
                        						randomVal = data;
                        						console.log(data);
                        						
                        					},
                        					error: function(error) {
                        						console.log(error);
                        					} 
                        				});
                            			
                                    	}else{
                                    		alert("핸드폰 형식에 맞춰서 작성해주세요");
                                    		
                                    		}

                                	 }
/*                               	 var result =
                                		  decodeURIComponent(data.userid);
                       
                                   alert("회원님의 아이디는 : " + result + "입니다!"); */
                                 },
                                 error:function(error){
                                    alert("없는 회원입니다!");
                                    console.log(error);
                                 } 
                              })
                	});
              //--------------------------------------휴대폰 인증 끝-------------------------------------------
   
      
      
      
      
      
   </script>   

      <%@ include file="cho_footer.jsp" %>

</body>
</html>