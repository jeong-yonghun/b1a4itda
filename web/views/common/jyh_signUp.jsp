<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <script src="https://t1.daumcdn.net/mapjsapi/bundle/postcode/prod/postcode.v2.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <style>
        a:link { 
            text-decoration: none;
            color: #7A7A7A;
        }
        a:visited { 
            text-decoration: none;
            color: #7A7A7A;
        }
        .article1{
            width:840px; 
            height:75px;
        }
        .article2{
            width:840px; 
            height:75px;
            text-align: center;
        }
        #contents{
            margin-left: auto;
            margin-right: auto;
            width:1400px;
            height:1500px;
        }
        #nav{
            width:280px;
        }
        #aside{
            width:280px;
        }
        footer{
            margin-left: auto;
            margin-right: auto;
            width:1400px;
            height:100px;
        }
        .member-type-choice-wrap{
            width:50%;
            height:70%;
            margin:auto;
            padding-top:2%;
        }
        .member-type-per, .member-type-com{
            width:50%;
            height:100%;
            float:left;
            font-size: 20pt;
            font-weight: bold;
            color: #505050;
        }
        .member-type-per:hover, .member-type-com:hover{
            color:#5A84F1;
            cursor:pointer;
        }
        #companyFile-btn:hover, input[type=button]:hover, .comNoType:hover{
        	cursor:pointer;
        }
        /* .member-type-com:hover{
            color:#5A84F1;
            cursor:pointer;
        } */
        .comNoType{
        	border:1px solid #5A84F1;
        	border-radius: 5px;
        	padding:5px;
        }

        .signup-from{
            width:798px;
            margin:auto;
        }
        .signup-info-per, .signup-info-com{
            width:60%;
            margin:auto;
        }
        .info-area{
            height:65px;
        }
        #com-add2-area{
            height:37px;
        }
        .info-div{
            float:left;
            color:#505050;
            font-weight:550;
            font-size:13pt;
            margin-bottom:5px;
        }
        .info-div1{
            color:#505050;
            font-weight:550;
            font-size:13pt;
            margin-bottom:5px;
        }
        .jyh_info-div{
            color:#505050;
            font-weight:550;
            font-size:13pt;
            margin-bottom:5px;
        }
        .info-input, #phone-sms{
            width:98%;
            height:35px;
            padding:0px 0 0 5px;
            font-size: 15pt;
            border: 1px solid #5A84F1;
            border-radius: 5px;
        }
        .com-add-div{
            width:100%;
            float:left;
            color:#505050;
            font-weight:550;
            font-size:13pt;
            margin-bottom:5px;
        }
        .com-add1-area, .com-add3-area{
            margin-bottom:10px;
        }
        .com-add2-area{
            margin-bottom:5px;
        }
        .info-input-com-add-code, .phone-sms{
            width:40%;
            height:31px;
            padding:4px 0 0 5px;
            font-size: 15pt;
            border: 1px solid #5A84F1;
            border-radius: 5px;
        }
        .info-input-btn-per, .info-input-btn-com{
            width:80%;
            height:31px;
            padding-left:5px;
            padding:4px 0 0 5px;
            float:left;
            border: 1px solid #5A84F1;
            border-radius: 5px;
        }
        .info-input::placeholder, .info-input-btn-per::placeholder, .info-input-btn-com::placeholder, .info-input-com-add-code::placeholder {
            color:rgb(180, 179, 179);
            font-weight: bold;
            font-size: 15pt;
        }
        
        .info-input-btn-com1::placeholder{
        	color:rgb(180, 179, 179);
            font-weight: bold;
            font-size: 15pt;
            text-align:center;
        }
        .jyh_btn-com-file-ref{
        	font-weight: bold;
            font-size: 15pt;
            text-align:center;
        }
        
        .jyh_btn-identify, .jyh_btn-com-num-ref{
            width:18.1%;
            height:37px;
            float:right;
            background:#5A84F1;
            color:white;
            font-weight:500;
            font-size: 12pt;
            border-radius: 5px;
            border:0;
            outline:0;
        }
        .jyh_btn-com-file-ref{
        	width:80%;
            height:37px;
            float:left;
            background:white;
            color:#5A84F1;
            font-weight:500;
            font-size: 12pt;
            border-radius: 5px;
            border:1px solid #5A84F1;
            outline:0;
        }
        .jyh_btn-com-file-ref1{
        	width:80%;
            height:37px;
            float:left;
            background:white;
            color:rgb(180, 179, 179);
            font-weight: bold;
            font-size: 15pt;
            border-radius: 5px;
            border:1px solid #5A84F1;
            outline:0;
        }
        table{
            margin:auto;
            border-collapse:collapse;
            border-spacing:0;
        }
        td{
            margin:0;
            padding:0;
        }
        .jyh_skill-exp-slide{
            text-align: left;
            margin-left:auto;
            margin-right:auto;
            margin-bottom:10px;
            width:80%;
            border:1px solid #5A84F1;
            font-weight: bold;
            font-size: 18px;
            color:#5A84F1;
        }
        .downtriangle-img{
            width:25px;
            float:right;
        }

        .jyh_skill-exp-content{
            width:640.39px;
            margin-left:auto;
            margin-right:auto;
            margin-bottom:10px;
            display:none;
        }
        .jyh_skill-exp-content > table{
            text-align:left;
            font-size:15pt;
            font-weight:bold;
        }
        input[name=skill-exp], #agree{
            width:18px;
            height:18px;
        }
        .jyh_java > table{
            width:120%;
            height:300px;
        }
        .jyh_mobile-app > table{
            width:130%;
            height:100px;
        }
        .jyh_php-asp > table{
            width:113%;
            height:150px;
        }
        .jyh_net > table{
            width:114%;
            height:150px;
        }
        .jyh_javascript > table{
            width:118%;
            height:100px;
        }
        .jyh_c > table{
            width:115%;
            height:150px;
        }
        .jyh_db > table{
            width:118%;
            height:150px;
        }
        .jyh_publisher > table{
            width:113%;
            height:150px;
        }

        #info-check{
            width:60%;
            margin:auto;
            text-align: center;
        }

        #submitBtn{
            background:#5A84F1;
            color:white;
            font-weight:bold;
            border-radius: 5px;
            font-size: 18pt;
            border:0;
            outline:0;
        }
        
        #popupDiv {  /* 팝업창 css */
    		top : 0px;
    		position: absolute;
    		background: white;
    		width: 500px;
    		height: 350px;
    		display: none; 
    	}
    
    	#popup_mask { /* 팝업 배경 css */
        	position: fixed;
        	width: 100%;
        	height: 1000px;
        	top: 0px;
        	left: 0px;
         	display: none; 
         	background-color:#000;
         	opacity: 0.8;
    	}
    	
    	ul {
            list-style:none;
            margin:0;
            padding:0;
        }

        li {
            float: left;
            margin-right: 10px;
        }
        #phone-sms-btn, #phone-sms-btn1, #popCloseBtn{
        	background:#5A84F1;
            color:white;
            font-weight:500;
            font-size: 12pt;
            border-radius: 5px;
            border:0;
            outline:0;
            width:95px;
            height:37px;
        }
    </style>
</head>
<body>
<%@ include file="../common/menubar.jsp" %>
        <table>
            <tr>
                <td id="nav">
            </td>
            <td>
                    <div class="article1"></div>
                    <div class="article2"><!-- 로그인 메시지 시작 -->
                        <div class="member-type-choice-wrap ">
                            <div class="member-type-per" style="border-bottom:2px solid #5A84F1">
                                <p id="member-type-per-id" style="margin:8px; margin-bottom:0px;">개인회원</p>
                            </div>
                            <div class="member-type-com">
                                <p id="member-type-com-id" style="margin:8px; margin-bottom:0px;">기업회원</p>
                            </div>
                        </div>
                    </div><!-- 로그인 메시지 끝 -->
                    <hr style="margin:5px 0px 20px ">
        
                    <!-- ------------------------------개인 회원가입-------------------------------- -->
        
        
                    <div class="signup-from change-per">
                        <form id="insertPer" class="perForm" action="<%= request.getContextPath() %>/jyhinsert.me" method="post" >
                        <input class="info-input memTypeChk" type="text" name="memTypeChk" value ="1" style="display:none;">
                        <div class="signup-info-per">
                            <div class="info-area name-area-per">
                                <div class="info-div name-div-per">이름</div>
                                <input id="perName" class="info-input" type="text" name="perName" placeholder="이름(실명)">
                            </div><br>
                            <div class="info-area id-area-per">
                                <div class="info-div id-div-per">아이디</div>
                                <input id="perId" class="info-input" type="text" name="perId" placeholder="아이디(5자리 이상)">
                                 <div id="okIdPer" class="okIdPer" style="width:100%; text-align:right; float:right; color:gray;"></div>
                            </div><br>
                            <div class="info-area pwd1-area">
                                <div class="info-div pwd1-div-per">비밀번호</div>
                                <input id="perPwd1" class="info-input" type="password" name="perPwd1" placeholder="비밀번호">
                            </div><br>
                            <div class="info-area pwd2-area">
                                <div class="info-div pwd2-div-per">비밀번호 확인</div>
                                <input id="perPwd2" class="info-input" type="password" name="perPwd2" placeholder="비밀번호 확인">
                            </div><br>
                            <div class="info-area phone-area" style="margin-bottom:10px;">
                                <div class="info-div phone-div-per">핸드폰 번호</div>
                                <input id="phone" class="info-input-btn-per info-input" type="text" name="phone" placeholder="휴대폰 번호(-제외)">
                                <input id="perPhoneBtn" type="button" name="phone-btn"class="jyh_btn-identify jyh_btn-phone-per" value="인증번호">
                                <div id="okPhone" class="okPhonePer" style="width:32%; float:right; display:none; color:#5A84F1;">인증성공</div>
                            </div><br>
                            <div class="info-area email-area">
                                <div class="info-div email-div">이메일 주소</div>
                                <input id="email" class="info-input-btn-per info-input" type="text" name="email" placeholder="이메일 주소">
                                <input id="perEmailBtn" type="button" name="email-btn" class="jyh_btn-identify jyh_btn-email-per" value="인증번호">
                                <div id="okEmail" class="okEmailPer" style="width:32%; float:right; display:none; color:#5A84F1;">인증성공</div>
                            </div><br>
                        </div>
                        
					
                        <div class="skill-exp">
                                    <h1 style="text-align: center;">스킬 & 경험 선택</h1>
                                
                                <div class="jyh_skill-exp-slide jyh_java-checkbox">JAVA
                                    <img class="downtriangle-img" src="/itda/image/downtriangle.png"/>
                                </div>
                                <div class="jyh_skill-exp-content jyh_java">
                                    <h1 style="color:#505050;">JAVA</h1>
                                        <hr>
                                        <table>
                                            <tr>
                                                <td><input type="checkbox" name="skill-exp" id="FrontEnd" value="Front-End"><label for="FrontEnd">Front-End</label></td>
                                                <td><input type="checkbox" name="skill-exp" id="BackEnd" value="Back-End"><label for="BackEnd">Back-End</label></td>
                                                <td><input type="checkbox" name="skill-exp" id="Java" value="Java"><label for="Java">Java</label></td>
                                            </tr>
                                            <tr>
                                                <td><input type="checkbox" name="skill-exp" id="Mlplatform" value="Mlplatform"><label for="Mlplatform">Mlplatform</label></td>
                                                <td><input type="checkbox" name="skill-exp" id="Nexacro" value="Nexacro"><label for="Nexacro">Nexacro</label></td>
                                                <td><input type="checkbox" name="skill-exp" id="Proframe" value="Proframe"><label for="Proframe">Proframe</label></td>
                                            </tr>
                                            <tr>
                                                <td><input type="checkbox" name="skill-exp" id="Sencha" value="Sencha"><label for="Sencha">Sencha</label></td>
                                                <td><input type="checkbox" name="skill-exp" id="Trustform" value="Trustform"><label for="Trustform">Trustform</label></td>
                                                <td><input type="checkbox" name="skill-exp" id="Tuxedo" value="Tuxedo"><label for="Tuxedo">Tuxedo</label></td>
                                            </tr>
                                            <tr>
                                                <td><input type="checkbox" name="skill-exp" id="DevOn" value="DevOn"><label for="DevOn">DevOn</label></td>
                                                <td><input type="checkbox" name="skill-exp" id="Thymeleaf" value="Thymeleaf"><label for="Thymeleaf">Thymeleaf</label></td>
                                                <td><input type="checkbox" name="skill-exp" id="Spring" value="Spring"><label for="Spring">Spring</label></td>
                                            </tr>
                                            <tr>
                                                <td><input type="checkbox" name="skill-exp" id="Pro*C" value="Pro*C"><label for="Pro*C">Pro*C</label></td>
                                                <td><input type="checkbox" name="skill-exp" id="Xplatform" value="Xplatform"><label for="Xplatform">Xplatform</label></td>
                                                <td><input type="checkbox" name="skill-exp" id="Maven" value="Maven"><label for="Maven">Maven</label></td>
                                            </tr>
                                            <tr>
                                                <td><input type="checkbox" name="skill-exp" id="Jenklns" value="Jenklns"><label for="Jenklns">Jenklns</label></td>
                                                <td><input type="checkbox" name="skill-exp" id="Gauce" value="Gauce"><label for="Gauce">Gauce</label></td>
                                                <td></td>
                                            </tr>
                                        </table>
                                </div>
                                <div class="jyh_skill-exp-slide jyh_mobile-app-checkbox">Mobile App
                                    <img class="downtriangle-img" src="/itda/image/downtriangle.png"/>
                                </div>
                                <div class="jyh_skill-exp-content jyh_mobile-app">
                                        <h1 style="color:#505050;">Mobile App</h1>
                                        <hr>
                                        <table>
                                            <tr>
                                                <td><input type="checkbox" name="skill-exp" id="Hybrid" value="Hybrid"><label for="Hybrid">Hybrid</label></td>
                                                <td><input type="checkbox" name="skill-exp" id="Android" value="Android"><label for="Android">Android</label></td>
                                                <td><input type="checkbox" name="skill-exp" id="iOS(Object-C)" value="iOS(Object-C)"><label for="iOS(Object-C)">iOS(Object-C)</label></td>
                                            </tr>
                                            <tr>
                                                <td><input type="checkbox" name="skill-exp" id="IoT" value="IoT"><label for="IoT">IoT</label></td>
                                                <td><input type="checkbox" name="skill-exp" id="NeiOS(Swift) xacro" value="iOS(Swift) "><label for="iOS(Swift) ">iOS(Swift) </label></td>
                                                <td><input type="checkbox" name="skill-exp" id="WebView" value="WebView"><label for="WebView">WebView</label></td>
                                            </tr>
                                        </table>
                                </div>
        
                                <div class="jyh_skill-exp-slide jyh_php-asp-checkbox">PHP / ASP
                                    <img class="downtriangle-img" src="/itda/image/downtriangle.png"/>
                                </div>
                                <div class="jyh_skill-exp-content jyh_php-asp">
                                        <h1 style="color:#505050;">PHP / ASP</h1>
                                        <hr>
                                        <table>
                                            <tr>
                                                <td><input type="checkbox" name="skill-exp" id="PHP" value="PHP"><label for="PHP">PHP</label></td>
                                                <td><input type="checkbox" name="skill-exp" id="Laravel" value="Laravel"><label for="Laravel">Laravel</label></td>
                                                <td><input type="checkbox" name="skill-exp" id="Codeigniter" value="Codeigniter"><label for="Codeigniter">Codeigniter</label></td>
                                            </tr>
                                            <tr>
                                                <td><input type="checkbox" name="skill-exp" id="ASP" value="ASP"><label for="ASP">ASP</label></td>
                                                <td><input type="checkbox" name="skill-exp" id="ZendFramWork" value="ZendFramWork"><label for="ZendFramWork">Zend FramWork</label></td>
                                                <td><input type="checkbox" name="skill-exp" id="Symfony" value="Symfony"><label for="Symfony">Symfony</label></td>
                                            </tr>
                                            <tr>
                                                <td><input type="checkbox" name="skill-exp" id="WordPress" value="WordPress"><label for="WordPress">WordPress</label></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                        </table>
                                </div>
        
                                <div class="jyh_skill-exp-slide jyh_net-checkbox">.NET
                                    <img class="downtriangle-img" src="/itda/image/downtriangle.png"/>
                                </div>
                                <div class="jyh_skill-exp-content jyh_net">
                                        <h1 style="color:#505050;">.NET</h1>
                                        <hr>
                                        <table>
                                            <tr>
                                                <td><input type="checkbox" name="skill-exp" id="ASP.net" value="ASP.net"><label for="ASP.net">ASP.net</label></td>
                                                <td><input type="checkbox" name="skill-exp" id="C" value="C"><label for="C">C</label></td>
                                                <td><input type="checkbox" name="skill-exp" id="C++" value="C++"><label for="C++">C++</label></td>
                                            </tr>
                                            <tr>
                                                <td><input type="checkbox" name="skill-exp" id="OpenGL" value="OpenGL"><label for="OpenGL">OpenGL</label></td>
                                                <td><input type="checkbox" name="skill-exp" id="DevExpress" value="DevExpress"><label for="DevExpress">DevExpress</label></td>
                                                <td><input type="checkbox" name="skill-exp" id="VBA" value="VBA"><label for="VBA">VBA</label></td>
                                            </tr>
                                            <tr>
                                                <td><input type="checkbox" name="skill-exp" id="C#" value="C#"><label for="C#">C#</label></td>
                                                <td><input type="checkbox" name="skill-exp" id="MFC" value="MFC"><label for="MFC">MFC</label></td>
                                                <td></td>
                                            </tr>
                                        </table>
                                </div>
        
                                <div class="jyh_skill-exp-slide jyh_javascript-checkbox">JavaScript
                                    <img class="downtriangle-img" src="/itda/image/downtriangle.png"/>
                                </div>
                                <div class="jyh_skill-exp-content jyh_javascript">
                                        <h1 style="color:#505050;">JavaScript</h1>
                                        <hr>
                                        <table>
                                            <tr>
                                                <td><input type="checkbox" name="skill-exp" id="node.js" value="node.js"><label for="node.js">node.js</label></td>
                                                <td><input type="checkbox" name="skill-exp" id="AngularJS" value="AngularJS"><label for="AngularJS">AngularJS</label></td>
                                                <td><input type="checkbox" name="skill-exp" id="React.js" value="React.js"><label for="React.js">React.js</label></td>
                                            </tr>
                                            <tr>
                                                <td><input type="checkbox" name="skill-exp" id="JavaScript" value="JavaScript"><label for="JavaScript">JavaScript</label></td>
                                                <td><input type="checkbox" name="skill-exp" id="Vue.js" value="Vue.js"><label for="Vue.js">Vue.js</label></td>
                                                <td><input type="checkbox" name="skill-exp" id="JQuery" value="JQuery"><label for="JQuery">JQuery</label></td>
                                            </tr>
                                        </table>
                                </div>
        
                                <div class="jyh_skill-exp-slide jyh_c-checkbox">C / C++
                                    <img class="downtriangle-img" src="/itda/image/downtriangle.png"/>
                                </div>
                                <div class="jyh_skill-exp-content jyh_c">
                                        <h1 style="color:#505050;">C / C++</h1>
                                        <hr>
                                        <table>
                                            <tr>
                                                <td><input type="checkbox" name="skill-exp" id="Server" value="Server"><label for="Server">Server</label></td>
                                                <td><input type="checkbox" name="skill-exp" id="UNIX" value="UNIX"><label for="UNIX">UNIX</label></td>
                                                <td><input type="checkbox" name="skill-exp" id="Embedded" value="Embedded"><label for="Embedded">Embedded</label></td>
                                            </tr>
                                            <tr>
                                                <td><input type="checkbox" name="skill-exp" id="Qt" value="Qt"><label for="Qt">Qt</label></td>
                                                <td><input type="checkbox" name="skill-exp" id="MachineVision" value="MachineVision"><label for="MachineVision">MachineVision</label></td>
                                                <td><input type="checkbox" name="skill-exp" id="MetalLab" value="MetalLab"><label for="MetalLab">MetalLab</label></td>
                                            </tr>
                                            <tr>
                                                <td><input type="checkbox" name="skill-exp" id="Firmware" value="Firmware"><label for="Firmware">Firmware</label></td>
                                                <td><input type="checkbox" name="skill-exp" id="LabVIEW" value="LabVIEW"><label for="LabVIEW">LabVIEW</label></td>
                                                <td><input type="checkbox" name="skill-exp" id="Aduino" value="Aduino"><label for="Aduino">Aduino</label></td>
                                            </tr>
                                        </table>
                                </div>
        
                                <div class="jyh_skill-exp-slide jyh_db-checkbox">DB
                                    <img class="downtriangle-img" src="/itda/image/downtriangle.png"/>
                                </div>
                                <div class="jyh_skill-exp-content jyh_db">
                                        <h1 style="color:#505050;">DB</h1>
                                        <hr>
                                        <table>
                                            <tr>
                                                <td><input type="checkbox" name="skill-exp" id="Oracle" value="Oracle"><label for="Oracle">Oracle</label></td>
                                                <td><input type="checkbox" name="skill-exp" id="MySQL" value="MySQL"><label for="MySQL">MySQL</label></td>
                                                <td><input type="checkbox" name="skill-exp" id="MariaDB" value="MariaDB"><label for="MariaDB">MariaDB</label></td>
                                            </tr>
                                            <tr>
                                                <td><input type="checkbox" name="skill-exp" id="MSSQL" value="MSSQL"><label for="MSSQL">MSSQL</label></td>
                                                <td><input type="checkbox" name="skill-exp" id="Tibero" value="Tibero"><label for="Tibero">Tibero</label></td>
                                                <td><input type="checkbox" name="skill-exp" id="CUBRID" value="CUBRID"><label for="CUBRID">CUBRID</label></td>
                                            </tr>
                                            <tr>
                                                <td><input type="checkbox" name="skill-exp" id="MongoDB" value="MongoDB"><label for="MongoDB">MongoDB</label></td>
                                                <td><input type="checkbox" name="skill-exp" id="Postgresql" value="Postgresql"><label for="Postgresql">Postgresql</label></td>
                                                <td></td>
                                            </tr>
                                        </table>
                                </div>
        
                                <div class="jyh_skill-exp-slide jyh_publisher-checkbox">Publisher
                                    <img class="downtriangle-img" src="/itda/image/downtriangle.png"/>
                                </div>
                                <div class="jyh_skill-exp-content jyh_publisher">
                                        <h1 style="color:#505050;">Publisher</h1>
                                        <hr>
                                        <table>
                                            <tr>
                                                <td><input type="checkbox" name="skill-exp" id="HTML5" value="HTML5"><label for="HTML5">HTML5</label></td>
                                                <td><input type="checkbox" name="skill-exp" id="ActionScript" value="ActionScript"><label for="ActionScript">ActionScript</label></td>
                                                <td><input type="checkbox" name="skill-exp" id="CSS" value="CSS"><label for="CSS">CSS</label></td>
                                            </tr>
                                            <tr>
                                                <td><input type="checkbox" name="skill-exp" id="Flash" value="Flash"><label for="Flash">Flash</label></td>
                                                <td><input type="checkbox" name="skill-exp" id="웹접근성" value="웹접근성"><label for="웹접근성">웹접근성</label></td>
                                                <td><input type="checkbox" name="skill-exp" id="Git" value="Git"><label for="Git">Git</label></td>
                                            </tr>
                                            <tr>
                                                <td><input type="checkbox" name="skill-exp" id="웹표준" value="웹표준"><label for="웹표준">웹표준</label></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                        </table>
                                </div>
                        </div>
                        	<input class="infoPeriod" type="text" name="infoPeriod" style="display:none;">
        				</form>
                    </div>
        
                    <!-- ------------------------------기업 회원가입-------------------------------- -->
                    
                    <form id="uploadForm" class="comForm">
    					<input id="companyFile" type="file" name="file" style="display:none;" onchange="loadFile(); "/>
    					<button type="button" id="uploadBtn" onchange="loadFile();" style="display:none;">Save</button>
					</form>
        
                    <div class="signup-from change-com" style="display: none;">
                        <form id="insertCom" class="comForm"  action="<%= request.getContextPath() %>/jyhinsert.me" method="post">
                        <div class="signup-info-com">
                        <input id="comNumTxt" type="text" name="comNumTxt" style="display:none;"/>
                        <input class="info-input memTypeChk" type="text" name="memTypeChk" style="display:none;">
                        	<div id="comNoTypeChk" style="width:100%; text-align:center; margin-top:30px; margin-bottom:20px;">
                        	<div class="info-div1 com-num-div" style="margin-bottom:15px;">사업자 등록 조회</div>
                        	<input type="radio" name="comNoType1" id="comNo1" checked style="display: none;"><label class="comNoType comNoType1" for="comNo1" style="color:#5A84F1;">번호로 조회하기</label>
                        	<input type="radio" name="comNoType2" id="comNo2" style="display: none;"><label class="comNoType comNoType2" for="comNo2" style=" margin-left:30px;">파일로 조회하기</label>
                        	</div>
                            <div class="info-area1 com-num-area1" style="width:100%; margin-bottom:30px;">
                                <input id="companyNum" class="info-input-btn-com info-input-btn-com1" type="text" name="companynum" placeholder="사업자 등록 번호(-제외)" style="font-size: 15pt;">
                                <input id="companyNum-btn" type="button" value="조회" class="jyh_btn-com-num-ref jyh_btn-com-num">
                            </div>
                            <div class="info-area1 com-num-area2"  style="display:none; width:100%; margin-bottom:30px;">
                                <input id="companyFile-btn" class="jyh_btn-com-file-ref jyh_btn-com-file-ref1" type="button" value="사업자 등록증 이미지 파일 선택" name="companyFile-btn">
                                <input id="companyFile-search-btn" type="button" value="조회" class="jyh_btn-com-num-ref jyh_btn-com-num" style="float:right;">
                            </div><div id="okComNo" style="width:33%; float:right; display:none; color:#5A84F1;">인증성공</div><br><br>
                            <div class="info-area id-area-com">
                                <div class="info-div id-div-com">아이디</div>
                                <input id="comId" class="info-input" type="text" name="id-com" placeholder="아이디">
                                <div id="okIdCom" class="okIdCom" style="width:100%; text-align:right; float:right; color:gray;"></div>
                            </div><br>
                            <div class="info-area pwd1-area-com">
                                <div class="info-div pwd1-div-com">비밀번호</div>
                                <input id="comPwd1" class="info-input" type="password" name="comPwd1" placeholder="비밀번호">
                            </div><br>
                            <div class="info-area pwd2-area-com">
                                <div class="info-div pwd2-div-com">비밀번호 확인</div>
                                <input id="comPwd2" class="info-input" type="password" name="comPwd2" placeholder="비밀번호 확인">
                            </div><br>
                            <div class="info-area name-area-com">
                                <div class="info-div name-div-com">담당자 이름</div>
                                <input id="comName" class="info-input" type="text" name="name-com" placeholder="담당자 이름(실명)">
                            </div><br>
                            <div class="info-area phone-area-com">
                                <div class="info-div phone-div">담당자 핸드폰 번호</div>
                                <input id="comPhone" class="info-input-btn-com info-input" type="text" name="phone" placeholder="담당자 휴대폰 번호(-제외)">
                                <input id="comPhoneBtn" type="button" name="phone-btn" value="인증번호" class="jyh_btn-identify jyh_btn-phone-com">
                                <div id="okPhone" class="okPhoneCom" style="width:32%; float:right; display:none; color:#5A84F1;">인증성공</div>
                            </div><br>
                            <div class="info-area email-area-com">
                                <div class="info-div email-div-com">기업 이메일 주소</div>
                                <input id="comEmail" class="info-input-btn-com info-input" type="text" name="email" placeholder="기업 이메일 주소">
                                <input id="comEmailBtn" type="button" name="email-btn" value="인증번호" class="jyh_btn-identify jyh_btn-email-com">
                                <div id="okEmail" class="okEmailPer" style="width:32%; float:right; display:none; color:#5A84F1;">인증성공</div>
                            </div><br>
                            <div class="info-area com-name-area">
                                <div class="info-div id-div-com">기업명</div>
                                <input id="companyName" class="info-input" type="text" name="comname" placeholder="기업명">
                            </div><br>
                            <div class="info-area com-url-area">
                                <div class="info-div com-url-div">회사 홈페이지</div>
                                <input id="homePage" class="info-input" type="url" name="com-url" placeholder="https://website.co.kr/">
                            </div><br>
                            <div class="info-area com-add1-area">
                                <div class="info-div com-add-div">주소</div>
                                <input id="jyh_add1" class="info-input-com-add-code" type="text" name="com-add1" placeholder="우편번호" style="float:left;">
                                <input id="addressBtn" type="button" name="address-btn" value="우편번호"  onclick="sample6_execDaumPostcode()" class="jyh_btn-com-num-ref jyh_btn-com-num" style="float:left; margin-left:3px;">
                            </div>
                            <div class="info-area com-add2-area" id="com-add2-area">
                                <input id="jyh_add2" class="info-input" type="text" name="com-add2" placeholder="시/도/군">
                            </div>
                            <div class="info-area com-add3-area" id="com-add3-area">
                                <div class="jyh_info-div com-add3-div">상세주소</div>
                                <input id="jyh_add3" class="info-input" type="text" name="com-add3" placeholder="상세주소" style="width:55%; float:left;">
                                <input id="jyh_add4" class="info-input" type="text" name="com-add4" placeholder="참고항목" style="width:42%; float:right;">
                            </div><br>
                            <!-- <div class="info-area com-add3-area" id="com-add3-area">
                                    <div class="info-div com-add3-div">회사의 주요 기술 스택</div>
                                    <input class="info-input" type="text" name="com-skill" placeholder="기술 스택 입력 ex) java">
                                </div><br> -->
                        </div>
                        	<input class="infoPeriod" type="text" name="infoPeriod" style="display:none;">
                        </form>
                    </div>
                        <div id="info-check">
                        	<form id="insertYear" class="comForm perForm">
                            <div id="radio-check" style="margin-bottom:10px;">
                                <label>정보보유기간</label>
                                <input id="infoPeriod" type="radio" name="radio-check" value="1년"><label>1년</label>
                                <input type="radio" name="radio-check" value="3년"><label>3년</label>
                                <input type="radio" name="radio-check" value="5년"><label>5년</label>
                                <input type="radio" name="radio-check" value="탈퇴시"><label>탈퇴시</label>
                            </div>
                            </form>
                            <div id="agree-check" style="margin-bottom:10px;">
                                <span><input style="vertical-align:-3px;" type="checkbox" name="agree" id="agree" value="agree"></span><span><label >이용약관 및 개인정보 처리방침에 동의합니다.</label></span>
                            </div>
                            <input type="button" name="submit-btn" value="회원가입" disabled="disabled" style="width:350px; background:gray; height:40px;" id="submitBtn">
                            <p>
                                <a href="#">이용약관</a>
                                <a href="#">개인정보 처리방침</a>
                                <a href="#">FAQ/문의</a>
                            </p>
                            <hr>
                            <span>이미 계정이 있으신가요?</span> <a href="#"><로그인></a>
                        </div>
            </td>
            <td id="aside">
            </td>
            </tr>
        </table>
            <hr style="width:838px; margin:auto;">
            
             <div id ="popup_mask" ></div> <!-- 팝업 배경 DIV -->
    
    		<div id="popupDiv"> <!-- 팝업창 -->
    			<div class="phone-sms-area" style="width:100%; height:100%; position: relative;">
    				<h4 align="center" style="margin:auto; margin-top:45px;">인증번호 발송 안내</h4>
    				<hr style="width:30%;">
    				<div style="width:100%; height:39px; margin-top:40px; margin-bottom:40px;">
    				<ul style="display: table; margin: auto; margin-left:7%; padding:0; float:left; width:76%;">
    					<li><h4 style="margin:0; margin-top:10%; color:gray; vertical-align:middle">인증번호 입력</h4></li>
    					<li><input id="phone-sms" type="text" name="phoneSms" placeholder="인증번호"  style="vertical-align:middle;"></li>
    				</ul>
    					<span style="width:20%;"><p id="timer" style="color:red;  margin:0; margin-top:8.7px; width:10%; height:50%; float:left;">2:59</p></span>
        			</div>
        			<div style="width:100%; height:50px; color:gray; text-align:center; margin-bottom:30px;">
        				<h5 id="timerOver1">제한 시간 안에 인증번호를 입력해주세요.<br>시간 초과 시 창이 닫힙니다.</h5>
        				<h5 id="timerOver2" style="display:none; color:red;">인증번호가 맞지 않습니다.</h5>
        			</div>
        			<div id="btn-area" style="width:40%; height:38px; margin:auto; text-align:center;" >
        				<input id="phone-sms-btn" type="button" name="phone-sms-btn" value="확인">
        				<input id="popCloseBtn" type="button" name="popCloseBtn" value="닫기" style="background:gray;">
        			</div>
        		</div>
    		</div>
            
        <%@ include file="cho_footer.jsp" %>
        
            <script>
                var memberTypeChk = 1;
                var randomVal = "초기값";
                var fileName;
                var fileValue;
                
                function loadFile(){
          			fileValue = $("#companyFile").val().split("\\");
          			fileName = fileValue[fileValue.length-1];
          				$("#companyFile-btn").val(fileName);
          		}
                
                    $(function(){
                    	
                    	$("#comNo1").click(function(){
                    		$(".com-num-area1").show();
                    		$(".com-num-area2").hide();
                    		$(".comNoType1").css("color", "#5A84F1");
                    		$(".comNoType2").css("color", "black");
                    	});
                    	$("#comNo2").click(function(){
                    		$(".com-num-area1").hide();
                    		$(".com-num-area2").show();
                    		$(".comNoType1").css("color", "black");
                    		$(".comNoType2").css("color", "#5A84F1");
                    	});
                    	
                    	
                    	$("#companyFile").hide();
                    	$("#companyFile-btn").click(function(){
                  			$("#companyFile").click();
                  			console.log($("#companyFile").val());
                  			
                  		});
                    	$("#companyFile-search-btn").click(function(){
                  			$("#uploadBtn").click();
                  			
                  		});

                    	fileValue = $("#companyFile").val().split("\\");
              			fileName = fileValue[fileValue.length-1]; // 파일명
              			
                    	console.log(fileName);
                    	console.log($("#companyFile").val());
              			
              			
                    	
                    	
                    	
                        $(".jyh_skill-exp-content").slideUp();
                        
                        $(".jyh_skill-exp-slide").click(function(){
                            //$(this).next("p").slideDown();
                            
                            $(this).next("div").slideToggle(500, function(){
                            });
                        });
                        
                        $(".change-com").hide();
                        $(".member-type-per").click(function() { //개인회원 탭을 눌렀을 경우
                        	memberTypeChk = 1;
                            $("input:not(input[type=button], input[type=radio], input[type=checkbox], .memTypeChk, .comNumTxt)").val("");
                            $("#okPhone").hide();
                            $("#okEmail").hide();
                            $(".change-com").hide();
                            $(".change-per").show();
                            $(".memTypeChk").val("1");
                            $("#companyFile-btn").val("사업자 등록증 이미지 파일 선택");
                            $("#companyNum").val("");
                            $("input[name=radio-check]").prop("checked", false);
                            $("#agree").prop("checked", false);
                            $("#okIdCom").text("");
                            $("#okIdPer").text("");
                            //$(this).append("<hr style='background:#5A84F1; width:60%; margin-top:0;'>");
                            $(this).css("border-bottom","2px solid #5A84F1")
                            $(".member-type-com").css("border-bottom","none")
                        });
                        $(".member-type-com").click(function() { //기업회원 탭을 눌렀을 경우
                        	memberTypeChk = 2;
                            $("input:not(input[type=button], input[type=radio], input[type=checkbox], .memTypeChk, .comNumTxt)").val("");
                            $("#okPhone").hide();
                            $("#okEmail").hide();
                            $(".change-per").hide();
                            $(".change-com").show();
                            $(".memTypeChk").val("2");
                            $("input[name=radio-check]").prop("checked", false);
                            $("#agree").prop("checked", false);
                            $("#okIdCom").text("");
                            $("#okIdPer").text("");
                            $(this).css("border-bottom","2px solid #5A84F1")
                            $(".member-type-per").css("border-bottom","none")
                        });
                        
                        var chk=0;
                        
                        $("#submitBtn").click(function() {
                              var getMail = RegExp(/^[A-Za-z0-9_\.\-]+@[A-Za-z0-9\-]+\.[A-Za-z0-9\-]+/);
                              var getCheck= RegExp(/^[a-zA-Z0-9]{4,12}$/);
                              var getName= RegExp(/^[가-힣]+$/);
                              var fmt = RegExp(/^\d{6}[1234]\d{6}$/); //형식 설정
                              var buf = new Array(13); //주민등록번호 배열
                              var infoPeriod = $('#infoPeriod');
                              chk=0;
                             
                         	  if(memberTypeChk == 1){

                              //이름 유효성
                              if (!getName.test($("#perName").val())) {
                                alert("이름이 올바르지 않습니다.");
                                $("#perName").val("");
                                $("#perName").focus();
                                chk=1;
                              }
                         
                              //아이디 유효성 검사
                              else if(!getCheck.test($("#perId").val())){
                                alert("아이디가 올바르지 않습니다.");
                                $("#perId").val("");
                                $("#perId").focus();
                                chk=1;
                              }
                         
                              //비밀번호
                              else if(!getCheck.test($("#perPwd1").val())) {
                              alert("비밀번호가 올바르지 않습니다.");
                              $("#perPwd1").val("");
                              $("#perPwd1").focus();
                              chk=1;
                              }
                         
                              //아이디랑 비밀번호랑 같은지
                              else if ($("#perId").val()==($("#perPwd1").val())) {
                              alert("비밀번호가 아이디와 같습니다.");
                              $("#perPwd1").val("");
                              $("#perPwd1").focus();
                              chk=1;
                            }
                         
                              //비밀번호 똑같은지
                              else if($("#perPwd1").val() != ($("#perPwd2").val())){ 
                              alert("비밀번호가 같지 않습니다.");
                              $("#perPwd1").val("");
                              $("#perPwd2").val("");
                              $("#perPwd1").focus();
                              chk=1;
                             }
                              //약관 동의
                              else if($("#agree").is(":checked") == false){
                             	alert("약관에 동의해주세요");
                                chk=1;
                              }
                              //핸드폰 인증여부
                              else if($(".okPhonePer").css("display") == "none"){
                            	 alert("핸드폰 인증을 진행해주세요");
                             	 $("#phone").focus();
                             	 chk=1;
                              }
                              //핸드폰 인증여부
                              else if($(".okEmailPer").css("display") == "none"){
                            	  alert("이메일 인증을 진행해주세요");
                             	  $("#email").focus();
                             	  chk=1;
                              }
                              //정보보유기간 선택여부
                              else if($(':radio[name="radio-check"]:checked').length < 1){
                              	 alert('정보보유기간을 선택해주세요');                        
                              	 infoPeriod.focus();
                              	 event.preventDefault();
                              	 chk=1;
                          	  }
                          //-------------------------------------------------------
                          
                         	 }else if(memberTypeChk == 2){
                         		 
                         		 //이름 유효성
                                 if (!getName.test($("#comName").val())) {
                                   alert("이름이 올바르지 않습니다.");
                                   $("#comName").val("");
                                   $("#comName").focus();
                                   chk=1;
                                 }
                                 
                                 //아이디 유효성 검사
                                 else if(!getCheck.test($("#comId").val())){
                                   alert("아이디가 올바르지 않습니다.");
                                   $("#comId").val("");
                                   $("#comId").focus();
                                   chk=1;
                                 }
                            
                                 //비밀번호
                                 else if(!getCheck.test($("#comPwd1").val())) {
                                 alert("비밀번호가 올바르지 않습니다");
                                 $("#comPwd1").val("");
                                 $("#comPwd1").focus();
                                 chk=1;
                                 }
                            
                                 //아이디랑 비밀번호랑 같은지
                                 else if ($("#comId").val()==($("#comPwd1").val())) {
                                 alert("비밀번호가 아이디와 같습니다.");
                                 $("#comPwd1").val("");
                                 $("#comPwd1").focus();
                                 chk=1;
                               }
                            
                                 //비밀번호 똑같은지
                                 else if($("#comPwd1").val() != ($("#comPwd2").val())){ 
                                 alert("비밀번호가 같지 않습니다.");
                                 $("#comPwd1").val("");
                                 $("#comPwd2").val("");
                                 $("#comPwd1").focus();
                                 chk=1;
                                }
                         		 //약관 동의
                                 else if($("#agree").is(":checked") == false){
                                	 alert("약관에 동의해주세요");
                                     chk=1;
                                 }
                                 //사업자등록번호 인증여부
                                 else if($("#okComNo").css("display") == "none"){
                               		alert("핸드폰 인증을 진행해주세요");
                               		if($("#comNo1").css("display") != "none"){
                               			$("#comNo1").focus();
                               		}else{
                               			$("#comNo2").focus();
                               		}
                               		chk=1;
                                 }
                                 //핸드폰 인증여부
                                 else if($(".okPhoneCom").css("display") == "none"){
                               		alert("핸드폰 인증을 진행해주세요");
                               		$("#comPhone").focus();
                               		chk=1;
                                 }
                                 //핸드폰 인증여부
                                 else if($(".okEmailCom").css("display") == "none"){
                               		alert("이메일 인증을 진행해주세요");
                               		$("#comEmail").focus();
                               		chk=1;
                                 }
                         		 //정보보유기간 선택여부
                              	 else if($(':radio[name="radio-check"]:checked').length < 1){
                              	 	alert('정보보유기간을 선택해주세요');                        
                              	 	infoPeriod.focus();
                              	 	event.preventDefault();
                              	 	chk=1;
                          		 }
                         	 } 
                         	 
                              
                              
                         	 
                              
                              if(chk != 1 && memberTypeChk == "1"){
                            	 // alert("개인");
								 <%--  $.post("<%= request.getContextPath() %>/jyhinsert.me", $(".perForm").serialize(), function(data){
									  console.log("성공했다.ㅇㄹㄴㅇㄹㅇㄴㄹㄴ");
									  alert("ggggg");
									  location.href = "/itda/views/common/jyh_login.jsp";
								  }); --%>
                            	   $("#insertPer").submit();
                              }else if(chk != 1 && memberTypeChk == "2"){
                            	  //alert("기업");
                            	 <%--  $.post("<%= request.getContextPath() %>/jyhinsert.me", $(".comForm").serialize(), function(data){
                            		  
                            	  }); --%>
                            	  $("#insertCom").submit();
                              }
                          });
                        
                        
                        //-----------------------------------------------------------------
                        
                        $("#phone-sms-btn").on("click", function(){ // 인증번호 팝업창의 확인 버튼을 눌렀을 경우
                        	
                        	var userSms = $("#phone-sms").val();
                        	
                        	if(userSms == randomVal){ 			//인증번호가 같은 경우
                        		console.log("같다");
                        		//clearInterval(timer);
        	        			min = 2;
        	        			sec = 59;
        	        			$("#popup_mask").css("display","none"); //팝업창 뒷배경 display none
                	            $("#popupDiv").css("display","none"); //팝업창 display none
                	            $("body").css("overflow","auto"); //body 스크롤바 생성
                	            $("#timer").show();
                	            $("#timerOver1").show();
        	        			$("#timerOver2").hide();
        	        			okSign.show();
        	        			$("#phone-sms").val("");
        	        			clearTimeout(timerId);
                        	}else{								//인증번호가 다를 경우
                        		console.log("다르다");
                        		$("#timerOver2").show();
                        		$("#timerOver1").hide();
                        		okSign.hide();
                        	}
                        	
                        });
                        
                        //--------------------------------------핸드폰 sms 인증 시작-------------------------------------------
                        
                        var min = 2;
                        var sec = 59;
                        var okSign;
                        var division;
                        var timerId = null;

                        

                		$("#perPhoneBtn, #comPhoneBtn").on("click", function(){  // 핸드폰의 인증번호 버튼을 눌렀을 경우
                			var regExp =/(01[016789])([1-9]{1}[0-9]{2,3})([0-9]{4})$/;
                			var phone = $(this).parent().children("input:first-of-type").val();
                			okSign = $(this).parent().children("div:last-of-type");
                			division = $(this).attr('name');
                			
                            console.log($(this).attr('name'));
        	        		console.log($("#perPhoneBtn").attr('name'));
        	        		if(regExp.test(phone)){
        	        			
        	        			function timer(){
                                	if(sec > 10){
                   	        			$("#timer").text(String(min) + ":" + String(sec));
                   	        			sec--;
                   	        			timerId=setTimeout(timer,1000);
                   	        		}else if(sec >= 0){
                   	        			$("#timer").text(String(min) + ":0" + String(sec));
                   	        			sec--;
                   	        			timerId=setTimeout(timer,1000);
                   	        		}else if(min != 0){
                   	        			min--;
                   	        			sec = 60;
                   	        			timerId=setTimeout(timer,1000);
                   	        		}else{
                   	        			min = 2;
                   	        			sec = 59;
                   	        			$("#popup_mask").css("display","none"); //팝업창 뒷배경 display none
                           	            $("#popupDiv").css("display","none"); //팝업창 display none
                           	            $("body").css("overflow","auto");//body 스크롤바 생성
                           	            $("#timer").show();
                           	            $("#timerOver1").show();
                                   		$("#timerOver2").hide();
                   	        		}
                                }
        	        			
        	        			timer();
                	             $("#popupDiv").css({
                	                "top": (($(window).height()-$("#popupDiv").outerHeight())/2+$(window).scrollTop())+"px",
                	                "left": (($(window).width()-$("#popupDiv").outerWidth())/2+$(window).scrollLeft())+"px"
                	                //팝업창을 가운데로 띄우기 위해 현재 화면의 가운데 값과 스크롤 값을 계산하여 팝업창 CSS 설정
                	             }); 
                	            
                	            $("#popup_mask").css("display","block"); //팝업 뒷배경 display block
                	            $("#popupDiv").css("display","block"); //팝업창 display block
                	            
                	            $("body").css("overflow","hidden");//body 스크롤바 없애기
                	        
                	        $("#popCloseBtn").click(function(event){
                	            $("#popup_mask").css("display","none"); //팝업창 뒷배경 display none
                	            $("#popupDiv").css("display","none"); //팝업창 display none
                	            $("body").css("overflow","auto");//body 스크롤바 생성
                	            $("#timer").show();
                	            $("#timerOver1").show();
                        		$("#timerOver2").hide();
                        		clearTimeout(timerId);
                        		min = 2;
        	        			sec = 59;
        	        			okSign.hide();
                        	//	clearInterval(timer);
                	        });
                	        	
                			$.ajax({
            					url: "/itda/phoneCertification.ph",
            					data: {
            						phone: phone
            					},
            					type: "post",
            					success: function(data) {
            						
            						randomVal = data;
            						console.log(data);
            						
            					},
            					error: function(error) {
            						console.log(error);
            					} 
            				});
                			
                        	}else{
                        		alert("핸드폰 형식에 맞춰서 작성해주세요");
                        		$(this).parent().children("input:first-of-type").val("");
                        		$(this).parent().children("input:first-of-type").focus();
                            	chk=1;
                        		}
                			
                	});
                		
                		//--------------------------------------핸드폰 sms 인증 끝-------------------------------------------
                		
                        //--------------------------------------이메일 인증 시작-------------------------------------------
                        
                        $("#perEmailBtn, #comEmailBtn").on("click", function(){  // 핸드폰의 인증번호 버튼을 눌렀을 경우
                        	var getMail = RegExp(/^[A-Za-z0-9_\.\-]+@[A-Za-z0-9\-]+\.[A-Za-z0-9\-]+/);
                			var email = $(this).parent().children("input:first-of-type").val();
                			okSign = $(this).parent().children("div:last-of-type");
                			division = $(this).attr('name');
                			
                            console.log($(this).attr('name'));
        	        		console.log($("#perPhoneBtn").attr('name'));
        	        		
        	        		if(getMail.test(email)){
        	        			function timer(){
                                	if(sec > 10){
                   	        			$("#timer").text(String(min) + ":" + String(sec));
                   	        			sec--;
                   	        			timerId=setTimeout(timer,1000);
                   	        		}else if(sec >= 0){
                   	        			$("#timer").text(String(min) + ":0" + String(sec));
                   	        			sec--;
                   	        			timerId=setTimeout(timer,1000);
                   	        		}else if(min != 0){
                   	        			min--;
                   	        			sec = 60;
                   	        			timerId=setTimeout(timer,1000);
                   	        		}else{
                   	        			min = 2;
                   	        			sec = 59;
                   	        			$("#popup_mask").css("display","none"); //팝업창 뒷배경 display none
                           	            $("#popupDiv").css("display","none"); //팝업창 display none
                           	            $("body").css("overflow","auto");//body 스크롤바 생성
                           	            $("#timer").show();
                           	            $("#timerOver1").show();
                                   		$("#timerOver2").hide();
                   	        		}
                                }
        	        			
        	        			timer();
                	        	
                	             $("#popupDiv").css({
                	                "top": (($(window).height()-$("#popupDiv").outerHeight())/2+$(window).scrollTop())+"px",
                	                "left": (($(window).width()-$("#popupDiv").outerWidth())/2+$(window).scrollLeft())+"px"
                	                //팝업창을 가운데로 띄우기 위해 현재 화면의 가운데 값과 스크롤 값을 계산하여 팝업창 CSS 설정
                	             }); 
                	            
                	            $("#popup_mask").css("display","block"); //팝업 뒷배경 display block
                	            $("#popupDiv").css("display","block"); //팝업창 display block
                	            
                	            $("body").css("overflow","hidden");//body 스크롤바 없애기
                	        
                	        $("#popCloseBtn").click(function(event){
                	            $("#popup_mask").css("display","none"); //팝업창 뒷배경 display none
                	            $("#popupDiv").css("display","none"); //팝업창 display none
                	            $("body").css("overflow","auto");//body 스크롤바 생성
                	            $("#timer").show();
                	            $("#timerOver1").show();
                        		$("#timerOver2").hide();
                        		clearInterval(timer);
                	        });
                	        	
                			$.ajax({
            					url: "/itda/sendMail.se",
            					data: {
            						email: email
            					},
            					type: "post",
            					success: function(data) {
            						
            						randomVal = data;
            						console.log(data);
            						console.log(randomVal);
            						
            					},
            					error: function(error) {
            						console.log(error);
            					} 
            				});
                			
                        	}else{
                        		alert("이메일 형식에 맞춰서 작성해주세요");
                        		$(this).parent().children("input:first-of-type").val("");
                        		$(this).parent().children("input:first-of-type").focus();
                            	chk=1;
                        		}
                			
                	});
                		var gg;
                		//--------------------------------------이메일 인증 끝-------------------------------------------
                		
                		//--------------------------------------사업자 등록 번호로 인증 시작-------------------------------------------
                		$("#companyNum-btn").click(function(){
                				
                			var comNum = $("#companyNum").val();
                			
                			console.log(comNum);
                		
                			   $.ajax({
                			       url: "https://business.api.friday24.com/closedown/" + comNum,
                			       type: 'GET',
                			       async:false,
                			       headers: { 'Authorization' : 'Bearer cLckfJ3Aor1O49s52GzR' },
                			       dataType : 'json',
                			       success: function (data) {
                			    	   $('#okComNo').show();
                			    	   $('#okComNo').text("인증성공");
                                       $('#okComNo').css("color","#5A84F1");
                			    	   $('#comNumTxt').val(data.bizNum);
                			    	  // alert("성공");
                			    	   console.log(data);
                			    	   gg = data.bizNum;
                			 //   	   alert("사업자번호 : " + gg);
                			 //        alert(JSON.stringify(data));
                			       },
                			       error: function(){
                			    	 $('#okComNo').text("인증실패");
                   			    	 $('#okComNo').css("color","red");
                			         //alert("올바른 형식이 아닙니다.");
                			       }
                			});
                			   
                			   console.log(gg);
                			   
                			   if(gg != undefined){
                			 		var url = "http://www.ftc.go.kr/bizCommPop.do?wrkr_no="+gg;
                	 				window.open(url, "bizCommPop", "width=750, height=700;");
                			   }
                			  
                		});
                		
                		//--------------------------------------사업자 등록 번호로 인증 끝-------------------------------------------
                 
                    
                  //--------------------------------------사업자 등록 이미지로 인증 시작-------------------------------------------
                    var aa;
                    
                    $(function(){
                    	 
                        $('#uploadBtn').on('click', function(){
                            uploadFile();
                        });
                     
                    });
                    
                    
					function uploadFile(){
                        
                        var form = $('#uploadForm')[0];
                        var formData = new FormData(form);
                        var comNum1;
                        var state;
                        
                     
                        $.ajax({
                            url : 'https://ocr.api.friday24.com/business-license',
                            type : 'POST',
                            async:false,
                            headers: { 'Authorization' : 'Bearer cLckfJ3Aor1O49s52GzR' },
                            data : formData,
                            contentType : false,
                            processData : false,
                            success: function (data) {
                            $('#okComNo').show();
                            $('#okComNo').text("인증성공");
                            $('#okComNo').css("color","#5A84F1");
         			    	console.log(data);
         			    	comNum1 = data.license.bizNum;
         		//	    	alert("사업자번호 : " + aa);
         		//	        alert(JSON.stringify(data));
         			       },
         			       error: function(){
         			    	  /* $('#okComNo').hide();
         			    	  alert("올바른 파일이 아닙니다."); */
         			    	 $('#okComNo').text("인증실패");
         			    	 $('#okComNo').css("color","red");
         			       }
                        });
                        
                        $.ajax({
         			       url: "https://business.api.friday24.com/closedown/" + comNum1,
         			       type: 'GET',
         			       async:false,
         			       headers: { 'Authorization' : 'Bearer cLckfJ3Aor1O49s52GzR' },
         			       dataType : 'json',
         			       success: function (data) {
         			    	   $('#comNumTxt').val(data.bizNum);
         			    	   console.log(data);
         			    	   state = data.state;
         			 //   	   alert("사업자번호 : " + gg);
         			 //        alert(JSON.stringify(data));
         			       },
         			       error: function(){
         			    	// $('#okComNo').hide();
         			        // alert("올바른 형식이 아닙니다.");
         			    	 $('#okComNo').text("인증실패");
          			    	 $('#okComNo').css("color","red");
         			       }
         				});
                        
                        if(comNum1 != undefined && state != "close"){
        			 		var url = "http://www.ftc.go.kr/bizCommPop.do?wrkr_no="+comNum1;
        	 				window.open(url, "bizCommPop", "width=750, height=700;");
        			   }
                    }
					
					//--------------------------------------사업자 등록 이미지로 인증 끝-------------------------------------------
                    });
                </script>
                
                <script>
                function sample6_execDaumPostcode() {
                	
                    new daum.Postcode({
                        oncomplete: function(data) {
                            // 팝업에서 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분.

                            // 각 주소의 노출 규칙에 따라 주소를 조합한다.
                            // 내려오는 변수가 값이 없는 경우엔 공백('')값을 가지므로, 이를 참고하여 분기 한다.
                            var addr = ''; // 주소 변수
                            var extraAddr = ''; // 참고항목 변수

                            //사용자가 선택한 주소 타입에 따라 해당 주소 값을 가져온다.
                            if (data.userSelectedType === 'R') { // 사용자가 도로명 주소를 선택했을 경우
                                addr = data.roadAddress;
                            } else { // 사용자가 지번 주소를 선택했을 경우(J)
                                addr = data.jibunAddress;
                            }

                            // 사용자가 선택한 주소가 도로명 타입일때 참고항목을 조합한다.
                            if(data.userSelectedType === 'R'){
                                // 법정동명이 있을 경우 추가한다. (법정리는 제외)
                                // 법정동의 경우 마지막 문자가 "동/로/가"로 끝난다.
                                if(data.bname !== '' && /[동|로|가]$/g.test(data.bname)){
                                    extraAddr += data.bname;
                                }
                                // 건물명이 있고, 공동주택일 경우 추가한다.
                                if(data.buildingName !== '' && data.apartment === 'Y'){
                                    extraAddr += (extraAddr !== '' ? ', ' + data.buildingName : data.buildingName);
                                }
                                // 표시할 참고항목이 있을 경우, 괄호까지 추가한 최종 문자열을 만든다.
                                if(extraAddr !== ''){
                                    extraAddr = ' (' + extraAddr + ')';
                                }
                                // 조합된 참고항목을 해당 필드에 넣는다.
                                document.getElementById("jyh_add4").value = extraAddr;
                            
                            } else {
                                document.getElementById("jyh_add4").value = '';
                            }

                            // 우편번호와 주소 정보를 해당 필드에 넣는다.
                            document.getElementById('jyh_add1').value = data.zonecode;
                            document.getElementById("jyh_add2").value = addr;
                            // 커서를 상세주소 필드로 이동한다.
                            document.getElementById("jyh_add3").focus();
                        }
                    }).open();
                }
				</script>
				
				<script>
					var agreeChk = 1;
					$("#agree").click(function(){
						if($("#agree").is(":checked") == true) {
							$("#submitBtn").css("background","#5A84F1");
							$('#submitBtn').attr('disabled', false);
						}else{
							$("#submitBtn").css("background","gray");
							$('#submitBtn').attr('disabled', true);
						}
						
					});
					
					$("input[name=radio-check]").click(function(){
						$(".infoPeriod").val($(this).val());
					});
					
					
					$(document).ready(function(){
						var getMail = RegExp(/^[A-Za-z0-9_\.\-]+@[A-Za-z0-9\-]+\.[A-Za-z0-9\-]+/);
                        var getCheck= RegExp(/^[a-zA-Z0-9]{4,12}$/);
                        var getName= RegExp(/^[가-힣]+$/);
                        var fmt = RegExp(/^\d{6}[1234]\d{6}$/); //형식 설정
                        var buf = new Array(13); //주민등록번호 배열
						var checkAjaxSetTimeout;
						    $('#perId').keyup(function(){
						     //   clearTimeout(checkAjaxSetTimeout);
						     //   checkAjaxSetTimeout = setTimeout(function(){
						        if ( $('#perId').val().length > 4 ) {
						            var id = $('#perId').val();
						            // ajax 실행
						            $.ajax({
						                type : 'POST',
						                url : '/itda/jyhIdCheck.me',
						                data:
						                {
						                    id: id
						                },
						                success : function(result) {
						                    //console.log(result);
						                    if (result == "ok") {
						                    	if(!getCheck.test($("#perId").val())){
						                    		$("#okIdPer").text("사용 불가능한 아이디 입니다.");
							                        $("#okIdPer").css("color", "red");
					                              }else{
						                       		$("#okIdPer").text("사용 가능한 아이디 입니다.");
						                        	$("#okIdPer").css("color", "#5A84F1");
					                              }
						                    } else {
						                        $("#okIdPer").text("사용 불가능한 아이디 입니다.");
						                        $("#okIdPer").css("color", "red");
						                    }
						                }
						            }); // end ajax
						            }else if ( $('#perId').val().length < 5 && $('#perId').val().length > 0) {
						            	$("#okIdPer").text("5자리 이상 입력해주세요");
				                        $("#okIdPer").css("color", "red");
						            }else{
						            	$("#okIdPer").text("아이디를 입력해주세요");
				                        $("#okIdPer").css("color", "gray");
						            }
						    //    },1000);//end setTimeout
						    }); // end keyup
						    
						    $('#comId').keyup(function(){
							     //   clearTimeout(checkAjaxSetTimeout);
							     //   checkAjaxSetTimeout = setTimeout(function(){
							        if ( $('#comId').val().length > 4 ) {
							            var id = $('#comId').val();
							            // ajax 실행
							            $.ajax({
							                type : 'POST',
							                url : '/itda/jyhIdCheck.me',
							                data:
							                {
							                    id: id
							                },
							                success : function(result) {
							                    //console.log(result);
							                    if (result == "ok") {
							                    	if(!getCheck.test($("#comId").val())){
							                    		$("#okIdCom").text("사용 불가능한 아이디 입니다.");
								                        $("#okIdCom").css("color", "red");
						                              }else{
							                       		$("#okIdCom").text("사용 가능한 아이디 입니다.");
							                        	$("#okIdCom").css("color", "#5A84F1");
						                              }
							                    } else {
							                        $("#okIdCom").text("사용 불가능한 아이디 입니다.");
							                        $("#okIdCom").css("color", "red");
							                    }
							                }
							            }); // end ajax
							            }else if ( $('#comId').val().length < 5 && $('#comId').val().length > 0) {
							            	$("#okIdCom").text("5자리 이상 입력해주세요");
					                        $("#okIdCom").css("color", "red");
							            }else{
							            	$("#okIdCom").text("아이디를 입력해주세요");
					                        $("#okIdCom").css("color", "gray");
							            }
							    //    },1000);//end setTimeout
							    }); // end keyup
						});
				</script>
                
</body>
</html>