<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
	String admin = (String) session.getAttribute("admin");
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<title>Insert title here</title>
 <style>

        /*----------------------------------------- 메뉴바 style 영역 시작 -------------------------------------  */
        .head-blue-text-bold{
            color:#5A84F1;
            font-weight: bold;
            margin:10px;
        }

         
        header{
            margin-left: auto;
            margin-right: auto;
            width:1400px;
            height:100px;
        }
        .head1{
            height:60%;
            margin:0px;
            padding:0px;
        }

        .head1-1{
            height:100%;  
            float:left;
        }
        .head1-1-empty{
            width:7.5%; 
        }
        .head1-2-logo{
            width:7.5%; 
        }
        .head1-3-search{
            width:25%; 
        }
        .search-img{
            width: 100%; 
            max-width: 25px; 
            vertical-align: middle
        }

        .head1-4-empty{
            width:85%;
        }
        .head1-5{
            width:15%;
        }
        .head1-5-1{
            width:180px; 
            height:50%;  
            float:left; 
            margin-top:15px;
        }
        .head1-5-2-mypage-label-space{
            width:90px; 
            height:30px;  
            float:right; 
            margin-left:auto; 
            margin-right:auto; 
            margin-top:15px;
        }        
        .head1-5-3-login-label-space{
            width:80px; 
            height:30px; 
            float:right; 
            margin-left:auto; 
            margin-right:auto; 
            margin-top:14px;
        }
        
        .head2{
            height:40%; 
            background-color:#4876EF;
            
        }
        .head2-common{
            height:100%; 
            float:left;
            text-align: center;
        }
        .head2-1{
            width:20%;
        }
        .head2-2{
            width:60%;
        }
        .head2-3{
            width:20%;
        }

        .head2-text{
            font-weight: bold; 
            color:white; 
            width:80%; 
            height:50%; 
            margin-left:auto; 
            margin-right:auto; 
            margin-top:10px;
        }
        .head2-2-text{
            width:140px;
        }

        /*----------------------------------------- 메뉴바 style 영역 끝 -------------------------------------  */

    </style>
</head>
<body>
    <header class="head"> <!-- 메뉴바 시작 -->
        <div class="head1">
            <div class="head1-1 head1-1-empty" ></div><!-- 빈 공간 -->

            <div class="head1-1 head1-2-logo"><img src="/itda/image/logo.PNG" style="width:100%; height: 100%;"></div><!-- 로고 들어갈 자리 -->

            <div class="head1-1 head1-4-empty">

                    <div class="head1-5-2-mypage-label-space">
                    	<%if(admin != null){ %>
                        <label class="head-blue-text-bold">로그아웃</label>
                        <%}else{ %>
                        	<label class="head-blue-text-bold">로그인</label>
                        <%} %>
                    </div>
                    <div class="head1-5-3-login-label-space">
                    <%if(admin != null){ %>
                            <label class="head-blue-text-bold">admin</label>
                        <%}%>
                        </div>
            </div><!-- 빈공간 -->


        </div>
        <div class="head2">
            <div class="head2-common head2-1"></div>
            <div class="head2-common head2-2">
                <div class="head2-common head2-2-text">
                    <div class="head2-text">
                        <p id="mem-manage" style="margin:4px;">회원관리</p>
                    </div>
                </div>  
                <div class="head2-common head2-2-text">
                    <div class="head2-text">
                        <p id="hire-manage" style="margin:4px;">채용관리</p>
                    </div>
                </div>
                <div class="head2-common head2-2-text">
                    <div class="head2-text">
                        <p id="report-manage" style="margin:4px;">신고관리</p>
                    </div>
                </div>  
                <div class="head2-common head2-2-text">
                    <div class="head2-text">
                        <p id="question-manage" style="margin:4px;">고객문의</p>
                    </div>
                </div>  
                <div class="head2-common head2-2-text">
                    <div class="head2-text">
                        <p id="Approve-manage" style="margin:4px;">승인관리</p>
                    </div>
                </div>
                <div class="head2-common head2-2-text">
                    <div class="head2-text">
                        <p id="payment-manage" style="margin:4px;">결제관리</p>
                    </div>
                </div>  
            </div>
            <div class="head2-common head2-3"></div>
        </div>
    </header><!-- 메뉴바 끝   -->

	<script>
		$("#mem-manage").click(function(){
     		location.href = "<%=request.getContextPath()%>/jyhselectlist.me?memType=1";
    	});
		$("#hire-manage").click(function(){
     		location.href = "<%=request.getContextPath()%>/jyhSelectProgressHireList.re?annType=1&annDivision=1";
    	});
		$("#payment-manage").click(function(){
     		location.href = "<%=request.getContextPath()%>/adminPayList.pay";
    	});
		$("#Approve-manage").click(function(){
			location.href="<%=request.getContextPath()%>/approval.com";
    	});
	</script>
</body>
</html>