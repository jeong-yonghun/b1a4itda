<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="ko">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <style>
        #jyh_nav-div{
            height:1000px;
        }
        #jyh_side-menu-outline p{
            margin:10px;
        }
        #jyh_side-menu-outline ul{
            list-style:none; 
            padding-left:5px;
        }
        #jyh_side-menu-outline{
            width:75%;
            margin:auto;
            border:1px solid gray;
        }
        #jyh_side-menu-per, #jyh_side-menu-com{
            width:80%;
            margin:auto;
        }
        #jyh_side-menu-com{
            margin-top:50px;
        }
    </style>
 </head>
</body>
            <div id=jyh_nav-div>
        <div id="jyh_side-menu-outline">
            <div id="jyh_side-menu-per">
                <h3>개인회원</h3>
                <hr style="margin-right:auto; margin-left:auto; margin-top:5px;">
                <ul>
                    <li style="font-weight:bold;">개인회원정보
                        <ul style="font-weight:400;">
                            <li id="perList"><p>- 회원 정보 조회</p></li>
                            <li id="pauseList"><p>- 유저 관리</p></li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div id="jyh_side-menu-com">
                <h3>기업회원</h3>
                <hr style="margin-right:auto; margin-left:auto; margin-top:5px;">
                <ul>
                    <li style="font-weight:bold;">기업회원정보
                        <ul style="font-weight:400;">
                            <li id="comList"><p>- 회원 정보 조회</p></li>
                            <li id="billingHistory"><p>- 결제내역 관리</p></li>
                            <li id="blackList"><p>- 블랙리스트 관리</p></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    
    <script>
    	$("#perList").click(function(){
    		location.href = "<%=request.getContextPath()%>/jyhselectlist.me?memType=1";
    	});
    	$("#pauseList").click(function(){
    		location.href = "<%=request.getContextPath()%>/jyhPauseList.me";
    	});
    	$("#comList").click(function(){
    		location.href = "<%=request.getContextPath()%>/jyhselectlist.me?memType=2";
    	});
    	$("#blackList").click(function(){
    		location.href = "<%=request.getContextPath()%>/jyhBlackList.me";
    	});
    </script>
</body>
</html>