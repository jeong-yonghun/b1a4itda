<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>
    <style>
        div{
            text-align: center;
            color:black;
        }
        input::placeholder {
            color:rgb(180, 179, 179);
            font-weight: bold;
            font-size: 12pt;
        }
        #contents{
            margin-left: auto;
            margin-right: auto;
            width:1400px;
            height:700px;
            background: red;
        }
        nav{
            width:20%;
            height:100%;
            float:left;
            background: white;
        }
        section{
            width:60%;
            height:100%;
            float:left;
        }
        aside{
            width:20%;
            height:100%;
            float:left;
            background: black;
        }
        footer{
            margin-left: auto;
            margin-right: auto;
            width:1400px;
            height:100px;
        }
        #hyj_boardsearch{
            width:250px;
            height:20px;
            border: 2px solid rgb(116, 116, 255);
            border-radius: 10px;
            float: left;
        }
        #hyj_boardsearchbtn{
            float: left;
            width:70px;
            height:25px;
            border-radius: 10px;
            border: 2px solid rgb(116, 116, 255);
        }
        #hyj_newboard{
            
            width:150px;
            height:25px;
            border-radius: 10px;
            border: 2px solid rgb(116, 116, 255);
            background: white;
        }
        #hyj_boardsearchlist1, #hyj_boardsearchlist2, #hyj_boardsearchlist3, #hyj_boardsearchlist4{
            width:70px;
            height:25px;
            background: rgb(170, 170, 255);
        }
        #hyj_textline{
            width:20%;
            height:8%;
            background: rgb(57, 57, 97);
        }
        #hyj_boardtitle{
            border: 1px solid black;
            margin-left: auto;
            margin-right: auto;
            
            width:40%;
            height:8%; 
           
        }
        #hyj_boardno{
          
            border: 1px solid black;
        }
        #hyj_boardusername{
            width:25%;
            height:8%; 
            border: 1px solid black;
          
        }
        #hyj_boardviews{
            width:15%;
            height:8%; 
            border: 1px solid black;
        
        }
        #hyj_boardday{
            width:15%;
            height:8%; 
            border: 1px solid black;
       

        }

    </style>
</head>
<body>


	
    <div id="contents"><!-- 전체 화면 div 시작 -->
        <nav id="nav">nav</nav><!-- nav -->
        <table style="width:60%; height:120%; float:left; background: rgb(105, 76, 76);">
                <tr>
                    <td>
                        <table style="width:100%; height:100%; background: rgb(255, 255, 255);">
                            <tr >
                                <td></td>
                                <td><input type="text" placeholder="검색어 입력" id="hyj_boardsearch">
                                <button id="hyj_boardsearchbtn">검색</button></td>
                                <td><button id="hyj_newboard">새글쓰기</button></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                <button id="hyj_boardsearchlist1">최신순</button>
                                <button id="hyj_boardsearchlist2">추천순</button>
                                <button id="hyj_boardsearchlist3">댓글순</button>
                                <button id="hyj_boardsearchlist4">조회순</button></td>
                            </tr>
                            <tr>
                                
                                <th style="border-top: 2px solid black;"><div style="width: 150px; background: red; border-top: 2px solid blackl;">글번호</div></th>
                                <th id="hyj_boardtitle">제목</th>
                                <th id="hyj_boardusername">작성자</th>
                                <th id="hyj_boardviews">조회수</th>
                                <th id="hyj_boardday">작성일</th>
                               
                            </tr>
                            <tr>
                                <td style="border:1px solid black"></td>
                                <td style="border:1px solid black"></td>
                                <td style="border:1px solid black"></td>
                            </tr>
                            <tr>
                                <td  style="border:1px solid black"></td>
                                <td style="border:1px solid black"></td>
                                <td style="border:1px solid black"></td>
                            </tr>
                            <tr>
                                <td style="border:1px solid black"></td>
                                <td style="border:1px solid black"></td>
                                <td style="border:1px solid black"></td>
                            </tr>
                            <tr>
                                <td style="border:1px solid black"></td>
                                <td style="border:1px solid black"></td>
                                <td style="border:1px solid black"></td>
                            </tr>
                            <tr>
                                <td style="border:1px solid black"></td>
                                <td style="border:1px solid black"></td>
                            </tr>
                            <tr>
                                <td style="border:1px solid black"></td>
                                <td style="border:1px solid black"></td>
                            </tr>
                            <tr>
                                <td style="border:1px solid black"></td>
                                <td style="border:1px solid black"></td>
                            </tr>
                            <tr>
                                <td style="border:1px solid black"></td>
                                <td style="border:1px solid black"></td>
                            </tr>
                            <tr>
                                <td style="border:1px solid black"></td>
                                <td style="border:1px solid black"></td>
                            </tr>
                            <tr>
                                <td style="border:1px solid black"></td>
                                <td style="border:1px solid black"></td>
                            </tr>
                            <tr>
                                <td style="border:1px solid black"></td>
                                <td style="border:1px solid black"></td>
                            </tr>
                            <tr>
                                <td style="border:1px solid black"></td>
                                <td style="border:1px solid black"></td>
                            </tr>
                        </table>
                    </td>
                </tr>
        </table>
        <aside>aside</aside><!-- aside -->
    </div><!-- 전체 화면 div 끝 -->
		

</body>
</html>