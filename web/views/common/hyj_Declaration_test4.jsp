<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<style>
#all {
	width: 1000px;
	height: 1000px;
	margin-left: auto;
	margin-right: auto;
	margin-top: 4%;
}

#bba {
	border-top: 1px solid black;
	font-family: 'Noto Sans KR', sans-serif;
}

#baa {
	border-top: 1px solid black;
	font-family: 'Noto Sans KR', sans-serif;
}

.sideCustomerCenter {
	border: 1px solid blue;
	width: 150px;
}
.inquiryBoard {
	width: 700px;
	margin-left: 20px;
}

td {
	height: 30px;
}

#btnArea {
	width: 150px;
	margin-left: auto;
	margin-right: auto;
}

.btn1 {
	background-color: blue;
	color: white;
	border-radius: 5px;
}

.btn2 {
	background-color: gray;
	color: white;
	border-radius: 5px;
}
#hyj_btn{
width: 150px;
height: 50px;
margin-top: 50px;
margin-left: 150px;
margin-right: 150px;


}
</style>
</head>
<body>
	<%@ include file="../common/menubar.jsp"%>
	<div id="all">
		<div class="sideCustomerCenter" style="float: left;">
			<table>
				<tr>
					<td>
						<table>
							<tr>
								<td
									 style="font-family: 'Noto Sans KR', sans-serif;">고객센터</td>
							</tr>
							<tr>
								<td  id="baa">FAQ</td>
							</tr>
							<tr>
								<td id="bba">문의하기</td>
							</tr>
							<tr>
								<td id="bba">신고</td>
							</tr>
							<tr>
								<td id="bba">- 신고접수</td>
                            </tr>
                            <tr>
                                <td id="bba">- 신고진행조회</td>
                            </tr>
						</table>
					</td>
				</tr>
			</table>
		</div>
		<form>
			<div class="inquiryBoard" style="float: left;">
				<div style="font-weight: bold; font-size: 24px; color:rgb(90, 132, 241);">신고진행조회</div>
				<hr style="border: 2px solid rgb(90, 132, 241);">
				
				<table class="board">
					
					<tr style="background: rgb(255, 255, 255);">
						<td >
							<div style="font-size:20px;font-weight: bold;  border-bottom: 2px solid rgb(216, 216, 216);">신고내용</div>
							<table style="width: 700px; height: 50px; background: rgb(255, 255, 255);">

								<tr>
										<td style="width: 15%; background: skyblue;">제목 : </td>
										<td style=" border-bottom: 2px solid rgb(216, 216, 216);">허위 채용 공고를 올린 B1A4회사를 신고합니다!</td>
								</tr>
								<tr>
										<td style="width: 15%; background: skyblue;">내용 : </td>
										<td style=" border-bottom: 2px solid rgb(216, 216, 216);">허위 채용 공고를 올린 B1A4회사를 신고합니다!
											이상을 위하여 끓는 찬미를 것이다. 얼마나 끓는 간에 몸이 이상을 인간이 때까지 부패뿐이다. 무엇을 피부가 그들의 쓸쓸한 이상 뼈 만천하의 
											사랑의 않는 것이다. 굳세게 그들의 오아이스도 가치를 두손을 그들은 만물은 같이, 아니다. 사랑의 광야에서 속에 동산에는 희망의 것이다. 주며, 같이, 
											가는 가진 인류의 쓸쓸하랴? 무엇이 아니더면, 생생하며, 그들은 수 철환하였는가? 역사를 품에 불어 이것을 소리다.이것은 않는 것이다. 곳으로 싸인 꽃 
											약동하다.</td>
								</tr>
								<tr>
										<td style="width: 15%; background: skyblue;">첨부파일 : </td>
										<td><input type="file" name="hyj_files"></td>
								</tr>
								</table>
								<div>&nbsp;</div>
								<div style="font-size:20px;font-weight: bold;  border-bottom: 2px solid rgb(216, 216, 216);">답변내용</div>
								<table>
									<tr>
										<td style="width: 15%; background: skyblue;">제목 : </td>
										<td style=" border-bottom: 2px solid rgb(216, 216, 216);">신고하신내용의 답변 내용입니다</td>
								</tr>
								<tr>
										<td style="width: 15%; background: skyblue;">내용 : </td>
										<td style=" border-bottom: 2px solid rgb(216, 216, 216);">신고해주신 내용확인하였습니다
											해당기업에대한 불건전한정보를 파악하여 조치완료하였습니다
											이상을 위하여 끓는 찬미를 것이다. 얼마나 끓는 간에 몸이 이상을 인간이 때까지 부패뿐이다. 무엇을 피부가 그들의 쓸쓸한 이상 뼈 만천하의 
											사랑의 않는 것이다. 굳세게 그들의 오아이스도 가치를 두손을 그들은 만물은 같이, 아니다. 사랑의 광야에서 속에 동산에는 희망의 것이다. 주며, 같이, 
											가는 가진 인류의 쓸쓸하랴? 무엇이 아니더면, 생생하며, 그들은 수 철환하였는가? 역사를 품에 불어 이것을 소리다.이것은 않는 것이다. 곳으로 싸인 꽃 
											약동하다.
											</td>
								</tr>
								<tr>
										<td></td>
										<td><button id="hyj_btn">확인</button></td>
								</tr>

								</table>
              			    </td>
					</tr>

				</table>
	

			</div>
		</form>
	</div>
	<%@ include file="../common/cho_footer.jsp"%>
	<script>
		function hyj_btn(){
			location.href="<%=request.getContextPath()%>/index.jsp";
			
		}
	</script>
</body>
</html>