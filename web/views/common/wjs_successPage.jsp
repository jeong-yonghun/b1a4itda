<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<% String successCode = (String) request.getAttribute("successCode"); %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
</head>
<body>
	<script>
		$(function(){
			var successCode ='<%= successCode%>';
			var alertMessage = "";
			var movePath = "";
			
			switch(successCode){
			
			case "HireEnroll" :
				alertMessage="공고등록 신청이 완료되었습니다."
				movePath = "views/company/companyMainPage/companyMainPage.jsp";
				break;
				
			case "deleteMember" : 
				alertMessage="회원탈퇴 되었습니다.";
				movePath = "/itda/index.jsp";
				break;
				
			case "updateRecruit" : 
				alertMessage="공고를 승인하였습니다.";
				movePath = "/itda/approval.re";
				break;
			
			case "updateRecruit2" : 
				alertMessage="공고를 반려하였습니다.";
				movePath = "/itda/approval.re";
				break;
				
			case "updateCom" : 
				alertMessage="회원가입을 승인하였습니다.";
				movePath = "/itda/approval.com";
				break;
				
			case "updateCom2" : 
				alertMessage="회원가입을 반려하였습니다.";
				movePath = "/itda/approval.com";
				break;
				
			
				
			}
			
			alert(alertMessage);
			location.href = movePath;
		});
	</script>

</body>
</html>