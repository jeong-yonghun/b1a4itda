<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.util.*"%>
    <%@page import="com.kh.itda.addInfo.school.model.vo.*" %>
    <%@page import="com.kh.itda.addInfo.portfolio.model.vo.*" %>
    <%@page import="com.kh.itda.addInfo.resume.model.vo.*" %>
    <%@page import="com.kh.itda.member.model.vo.*" %>
    <%@page import="java.util.Map.Entry" %>
    <%
    ArrayList<HashMap<String,Object>> resumeDetail=(ArrayList<HashMap<String,Object>>) request.getAttribute("resumeDetail");
	System.out.println("resumeDetailArrayList jsp파일에서" + resumeDetail);
	ArrayList<Jyh_PerMember> member=new ArrayList<Jyh_PerMember>();
	ArrayList<hyj_Previous> companyCareer=new ArrayList<hyj_Previous>();
	ArrayList<hyj_education> eduCareer=new ArrayList<hyj_education>();
	ArrayList<hyj_developmentinsert> DevelCareer=new ArrayList<hyj_developmentinsert>();
	ArrayList<hyj_awards> award=new ArrayList<hyj_awards>();
	ArrayList<School> school=new ArrayList<School>();
	ArrayList<hyj_volunteer> volunte=new ArrayList<hyj_volunteer>();
	ArrayList<Portfolio> portfolio=new ArrayList<Portfolio>();
	ArrayList<hyj_certificate> license=new ArrayList<hyj_certificate>();
	ArrayList<Resume> resume=new ArrayList<Resume>();
	ArrayList<hyj_Attachment> file= new ArrayList<hyj_Attachment>();
	
	
for(int i = 0; i < resumeDetail.size(); i++){
    //arraylist 사이즈 만큼 for문을 실행합니다.
    System.out.println("list 순서 " + i + "번쨰");
    for( Entry<String, Object> elem : resumeDetail.get(i).entrySet() ){
        // list 각각 hashmap받아서 출력합니다.
        System.out.println( String.format("키 : %s, 값 : %s", elem.getKey(), elem.getValue()) );
       for(int j=0;j<resumeDetail.size();j++){
    	   System.out.println("for문들어오는 지확인");
        	if(elem.getKey().equals("companyCareer"+j)){
        		companyCareer.add((hyj_Previous)elem.getValue());
        	}else if(elem.getKey().equals("eduCareer"+j)){
        		eduCareer.add((hyj_education)elem.getValue());
        	}else if(elem.getKey().equals("DevelCareer"+j)){
        		DevelCareer.add((hyj_developmentinsert)elem.getValue());
        	}else if(elem.getKey().equals("award"+j)){
        		award.add((hyj_awards)elem.getValue());
        	}else if(elem.getKey().equals("school"+j)){
        		school.add((School)elem.getValue());
        	}else if(elem.getKey().equals("volunte"+j)){
        		 volunte.add((hyj_volunteer)elem.getValue());
        	}else if(elem.getKey().equals("Portfolio"+j)){
        		portfolio.add((Portfolio)elem.getValue());
        	}else if(elem.getKey().equals("license"+j)){
        		license.add((hyj_certificate)elem.getValue());
        	}else if(elem.getKey().equals("member")){
        		member.add((Jyh_PerMember)elem.getValue());
        	}else if(elem.getKey().equals("resume")){
        		resume.add((Resume)elem.getValue());
        	}else if(elem.getKey().equals("file"+j)){
        		file.add((hyj_Attachment)elem.getValue());
        	}
       }
      // companyCareer=new ArrayList<hyj_Previous>();
       //ArrayList<hyj_Previous> companyCareer1= companyCareer<Object>;
       System.out.println("CompanyArray:"+companyCareer);
       System.out.println("eduArray:"+eduCareer);
       System.out.println("DevelArray:"+DevelCareer);
       System.out.println("awardArray:"+award);
       System.out.println("schoolArray:"+school);
       System.out.println("volunteArray:"+volunte);
       System.out.println("portfolioArray:"+portfolio);
       System.out.println("licenseArray:"+license);
       System.out.println("resume:"+resume);
       System.out.println("file:"+file);
       
      
     
      
       
    }
}
    %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <style>
        .cho_resume_section{
            border-bottom: 1px solid #C4C4C4;
        }
        .cho_resumeSkillDiv{
           display: inline-block;
            background-color:#EEB0D1;
            border:1px solid white; 
            float:left;"
        }
        #resume_com{
           
            margin-left: auto;
            margin-right: auto;
            width: 1200px;
            border-collapse: separate;
  			border-spacing: 0 100px;
  			
        }
        .cho_left_td{
            width: 20%;
            border-bottom: 1px solid #C4C4C4;
            background-color:#777E80;
            color:white;
            
            text-align:center;
        }
        .cho_right_td{
            width: 78%;
            border-bottom: 1px solid #C4C4C4;
             padding-top:20px;
             padding-left:40px;
             background-color:#F1F3F7;
        }
        .cho_right_tr{
            vertical-align: top;
        }
        .cho_resume_section{
        	padding:18px;
        }
       
        .resume_Com_Table_botton{
            float: right;
            width: 210px;
            height: 60px;
            border-radius: 0.5pc;
            background-color: #5A84F1;
            color: white;
            font-size: 15pt;
           
        }
       
         #bookMarkIcon{
        	width: 25px;
        	height: 25px
        }
    </style>
</head>
<body>
    
    <table id="resume_com">
        <tr class="cho_left_tr">
            <!-- 오른쪽 영역 개인정보, 기술 스택, 직무 분야 -->
            <td class="cho_rright_td" style="vertical-align: top;">
                <div id="personalInfo" class="cho_resume_section">
                    <!-- 이름 작성부분 -->
                    <% Jyh_PerMember mem=member.get(0); %>
                    <h1><img id="bookMarkIcon" src="/itda/image/bookMarkIconDisable.png" onclick="goBookMark()"><%=mem.getPerName() %></h1>
                    <!-- 연락처, 이메일, 깃허브, 포트폴리오 -->
                    <h3>
                    	    연락처: <%=mem.getPhone() %><br>
                    	    이메일: <%=mem.getEmail() %><br>
                   	     성별: <%=mem.getGender() %><br>
                   	     생년월일: <%=mem.getBirthDay()%>
                    </h3>
					
                </div>
                <%
				Resume r=resume.get(0);
                	String[] skill=r.getrSkill().split(",");
                %>
                <div class="cho_resume_section">
                    <h3>기술 스택: <%=skill.length%></h3>
                    <div id="skill_stack">
                        <!-- 반복문으로 기술별div만들어주기 -->
                        <table style="width: 80%;">
                            <tr>
                                <td>기술</td>
                                <td>스택</td>
                            </tr>
                            <tr>
                            	<td colspan="2">
                            	<%for(int i=0;i<skill.length;i++) {%>
                            	<div style="background-color:#DBD5EA;width:80px;text-align:center; float:left; border:1px solid white;"><%=skill[i] %></div>
                            	<%} %>
                            	</td>
                            </tr>
                        </table>
                    </div>
                    
                </div>
                <div class="cho_resume_section">
                    <h3>직무 분야</h3>
                    <div>
                        <!-- 반복문으로 직무 분야별 div만들어주기 -->
                        <table>
                            <tr>
                            	<td style="color:#FF766B; font-size:20px;">
                            		<%=r.getrHopeDuty() %>
                            	</td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="cho_resume_section">
                <h3>자기소개</h3>
                <%=r.getrContent() %>
                </div>
                
            </td>
            
            <!-- 왼쪽영역 프로젝트 교육 등등등 값이 없을 때는 if 문으로 조건 걸고 hide쓰기-->
            <td>
                <table style="width: 98%;">
                    <!-- 회사 경력 줄 -->
                    <%if(companyCareer.size()>0) {%>
                    <%for(hyj_Previous com:companyCareer) {%>
                    <tr class="cho_right_tr">
                        <td class="cho_left_td"><h3>회사경력</h3></td>
                        <td class="cho_right_td">
                            <!-- 회사경력div 갯수만큼 for문으로 영역주기 -->
                            <div>
                            <table><tr><td>
                                <h4>회사명: <%=com.getPreviousname() %></h4>
                                </td></tr>
                                <tr><td>
                                <h5>직군: <%=com.getPreviousjobname() %></h5>
                                </td></tr>
                                <tr><td>
                            	   <h5> 입사일:<input type="date" value="<%=com.getPreviousjoindate() %>" disabled></h5>
                            	 </td></tr>
                            	 <tr><td>
                            	  <h5>  퇴사일:<input type="date" value="<%=com.getResignationdate() %>" disabled></h5>
                            	 </td></tr>
                                <!-- 사용스택 -->
                                <tr><td>
                                <h5>사용스택</h5>
                              <% String[] stack=com.getStack().split(",");
                                for(int i=0;i<stack.length;i++){ %>
                                <div class="cho_resumeSkillDiv"><%=stack[i]%></div>
                                    <%} %>
                                 </td></tr>
                               <tr><td>
                              	  <h5>연봉:<%=com.getMoney()%>(만원)</h5>
                              	 </td></tr>
                              </table>
                            </div>
                        </td>
                    </tr>
                    <%}} %>
                    <!-- 개발 경력 줄 -->
                      <%if(DevelCareer.size()>0) {%>
                    <%for(hyj_developmentinsert d: DevelCareer) {%>
                    <tr class="cho_right_tr">
                        <td class="cho_left_td"><h3>개발경력</h3></td>
                        <td class="cho_right_td">
                            <div>
                                <table><tr><td>
                                <h5>담당업무: <%=d.getDuserstack() %></h5>
                                </td></tr>
                                <tr><td>
                                 <h5>개발시작일자:<input type="date" value="<%=d.getJoindate() %>" disabled></h5>
                                 </td></tr>
                                 <tr><td>
                                  	  <h5>개발종료일자:<input type="date" value="<%=d.getResignationdate() %>" disabled></h5>
                                  </td></tr>
                                    <!-- 사용스택 -->
                                    <tr><td>
                                <h5>사용스택</h5>
                              <% String[] stack=d.getDuserskill().split(",");
                                for(int i=0;i<stack.length;i++){ %>
                                <div class="cho_resumeSkillDiv"><%=stack[i]%></div>
                                    <%} %>
                                 </td></tr>
                                    </table>
                            </div>
                        </td>
                    </tr>
                    <%} }%>
                    <!-- 수상 경력 -->
                    <%if(award.size()>0) {%>
                    <%for(hyj_awards a: award) {%>
                    <tr class="cho_right_tr">
                        <td class="cho_left_td"><h3>수상경력</h3></td>
                        <td class="cho_right_td">
                            <div>
                                <h4>수상명: <%=a.getAwardsname() %></h4>
                                <h5>수상날짜:<input type="date" value=""<%=a.getAwardsmonth() %> disabled></h5>
                                <h5>수상기관: <%=a.getAwardsagency() %></h5>
                            </div>
                        </td>
                    </tr>
                    <%}} %>
                    
                    
                    <!-- <tr class="cho_right_tr">
                        <td class="cho_left_td"><h3>개인 <br>프로젝트</h3></td>
                        <td class="cho_right_td">
                            프로젝트 div영역 갯수만큼 for문으로 영역주기
                            <div>
                                <h4>프로젝트 제목</h4>
                                <h5>2002</h5>
                                기술 스택 div영역 기술 갯수 for문으로
                                <div>
                                    <div class="cho_resumeSkillDiv">java</div>
                                    <div class="cho_resumeSkillDiv">java</div>
                                    <div class="cho_resumeSkillDiv">java</div>
                                    <div class="cho_resumeSkillDiv">java</div>
                                </div>
                                <h5>상세 설명</h5>
                                <div>asdfasdf</div>
                            </div>
                        </td>
                    </tr> -->
                    <!-- 교육  -->
                      <%if(eduCareer.size()>0) {%>
                    <%for(hyj_education e: eduCareer){ %>
                    <tr class="cho_right_tr">
                        <td class="cho_left_td"><h3>교육경력</h3></td>
                        <td class="cho_right_td">
                            <!-- 학력 갯수만큼 반복문 돌력서 div 추가하기 -->
                            <div>
                            	<table><tr><td>
                                <h4>교육 기관명: <%=e.getEducationname() %> </h4>
                                </td></tr>
                                <tr><td>
                                <h5>입학일:<input type="date" value="<%=e.getEducationstartday() %>" disabled></h5></td></tr>
                                <tr><td>
                                 	   <h5>졸업일:<input type="date" disabled value="<%=e.getEducationlastday()%>"></h5>
                                 </td></tr>
                                 <tr><td>
                                 <h5> 담당업무: <%=e.getEducationstack() %></h5>
                                 </td></tr>
                                 <tr><td>
                                    <h5>사용 스킬:<%=e.getEducationskil() %></h5>
                                  </td></tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <%}} %>
                    
                     <!--학력 -->
                      <%if(school.size()>0) {%>
                     <%for(School s: school){ %>
                     <tr class="cho_right_tr">
                        <td class="cho_left_td"><h3>학력</h3></td>
                        <td class="cho_right_td">
                            <!-- 학력 갯수만큼 반복문 돌력서 div 추가하기 -->
                            <div>
                            <table>
                            <tr><td>
                                <h4>학교명: <%=s.getCaName() %> </h4>
                                </td></tr>
                                <tr><td>
                                <h5>입학일:<input type="date" value="<%=s.getCaEnrollDate() %>" disabled></h5>
                                </td></tr>
                                <tr><td>
                                 	<h5> 졸업일:<input type="date" value="<%=s.getCaGraduDate() %>" disabled></h5>
                                 </td></tr>
                                 <tr><td>
                                 <h5> 전공: <%=s.getCaMajor() %></h5>
                                 </td></tr>
                                 <tr><td>
                                  	<h5>학위: <%=s.getCaDegree() %></h5>
                                  	</td></tr>
                                  	
                                  	<tr><td>
                                  	  <h5>내학점:<%=s.getCaGrades() %>  최대학점:<%=s.getCaTopGrades() %></h5>
                                  	  </td></tr>
                                  	  <tr><td>
                                  	 <h5> 기타</h5>
                                  	 </td></tr>
                                  	 <tr><td>
                                    <div style="width: 80%;margin-bottom:20px;">
                                      	  <%=s.getEtc() %>
                                    </div>
                                    </td></tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <%}} %>
                    <!-- 첨부파일 -->
                    <%if( file.size()>0) {%>
                    <%for(hyj_Attachment at: file) {%>
                    <tr class="cho_right_tr">
                        <td class="cho_left_td"><h3>첨부파일</h3></td>
                        <td class="cho_right_td">
                            <!-- 첨부파일 갯수만큼 for문으로 추가해야함 -->
                            <div>
                                <h5>첨부파일: <%=at.getChangeName() %></h5>
                            </div>
                        </td>
                    </tr>
                    <%} }%>
                     <!-- 희망연봉 -->
                      <%if( r.getrHopeYearlySalary() != null) {%>
                     <tr class="cho_right_tr">
                        <td class="cho_left_td"><h3>희망연봉</h3></td>
                        <td class="cho_right_td">
                            <div>
                                <h5>희망연봉: <%=r.getrHopeYearlySalary() %></h5>
                            </div>
                        </td>
                    </tr>
                    <%} %>
                
                    <!--활동 -->
                    <%if( volunte.size()>0) {%>
                    <%for(hyj_volunteer v: volunte) {%>
                    <tr class="cho_right_tr">
                        <td class="cho_left_td"><h3>활동</h3></td>
                        <td class="cho_right_td">
                            <!-- 활동 갯수만큼 반복문 돌력서 div 추가하기 -->
                            <div>
                            <table>
                            <tr><td>
                                <h4>활동명: <%=v.getVolunteername() %> </h4>
                                </td></tr>
                                <tr><td>
                                <h5>시작일:<input type="date" value="<%=v.getVolunteerStartDate() %>" disabled></h5>
                                </td></tr>
                                <tr><td>
                                  	 <h5> 종료일:<input type="date" value="<%=v.getVolunteerendDate() %>" disabled></h5>
                                  	 </td></tr>
                                  	 <tr><td>
                               	    <h5> 링크: <input type="text" value="<%=v.getVolunteerlink()%>" disabled></h5>
                               	    </td></tr>
                               	  
                                </table>
                            </div>
                        </td>
                    </tr>
                    <%} }%>
                    <!-- 닫기 버튼 -->
                    <tr>
                        <td>
                        <button onclick="goBack()" class="resume_Com_Table_botton">닫기</button>
                        </td>
                    </tr>
                </table>

            </td>
        </tr>

    </table>
    <script type="text/javascript">
    	function goBack(){
    		location.href="<%= request.getContextPath() %>/resumeList.re";
    	}
    
    </script>
</body>
</html>