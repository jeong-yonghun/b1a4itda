<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<script>
	<src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<style>

/*----------------------------------------- 메뉴바 style 영역 시작 -------------------------------------  */
.head-blue-text-bold {
	color: #5A84F1;
	font-weight: bold;
}


header {
	margin-left: auto;
	margin-right: auto;
	width: 1400px;
	height: 100px;
}


.head1 {
	height: 60%;
	margin: 0px;
	padding: 0px;
}

.head1-1 {
	height: 100%;
	float: left;
}

.head1-1-empty {
	width: 7.5%;
}

.head1-2-logo {
	width: 7.5%;
}

.head1-3-search {
	width: 25%;
}

input[name=menubar-search] {
	height: 80%;
	width: 60%;
	float: left;
}

.head1-1-3-menubar-search {
	height: 50%;
	margin-top: 3.86%;
}

.search-img-space {
	border: solid;
	width: 10%;
	height: 85%;
	background: blue;
	float: left;
}

.search-img {
	width: 100%;
	max-width: 25px;
	vertical-align: middle
}

.head1-4-empty {
	width: 45%;
}

.head1-5 {
	width: 15%;
}

.head1-5-1 {
	width: 100%;
	height: 50%;
	float: left;
	margin-top: 15px;
}

.head1-5-1-alarm-img-space {
	width: 30%;
	height: 100%;
	float: left;
	margin-left: auto;
	margin-right: auto;
}

.alarm-img {
	width: 100%;
	max-width: 30px;
	vertical-align: middle;
}

.head1-5-2-mypage-label-space {
	width: 35%;
	height: 21.6px;
	float: left;
	margin-left: auto;
	margin-right: auto;
	margin-top: 2.2px;
}

.head1-5-3-login-label-space {
	width: 35%;
	height: 21.6px;
	float: left;
	margin-left: auto;
	margin-right: auto;
	margin-top: 4.2px;
}

.head2 {
	height: 40%;
	background-color: #4876EF;
}

.head2-common {
	height: 100%;
	float: left;
}

.head2-1 {
	width: 30%;
}

.head2-2 {
	width: 40%;
}

.head2-3 {
	width: 30%;
}

.head2-text {
	font-weight: bold;
	color: white;
	width: 80%;
	height: 50%;
	margin-left: auto;
	margin-right: auto;
	margin-top: 10px;
}

.head2-2-text1 {
	width: 30%;
}

.head2-2-text2 {
	width: 25%;
	margin-left: 23px;
	margin-right: 23px;
}

.head2-2-text3 {
	width: 35%;
}

/*----------------------------------------- 메뉴바 style 영역 끝 -------------------------------------  */
.article1 {
	width: 840px;
	height: 75px;
}

.article2 {
	width: 840px;
	height: 75px;
	text-align: center;
}
#article3 {
	width: 840px;
	height: 75px;
}

#nav {
	width: 280px;
    
}
#section{
    height: 900px;

}

#aside {
	width: 280px;

}

footer {
	margin-left: auto;
	margin-right: auto;
	width: 1400px;
	height: 100px;
}

#info-check {
	width: 60%;
	margin: auto;
	text-align: center;
}

#next {
	background: #5A84F1;
	color: white;
	font-weight: bold;
	border-radius: 5px;
	font-size: 12pt;
	border: 0;
	outline: 0;
    width: 58px;
    height: 40px;
    margin-left: 380px;
    margin-top: 50px;
}


#tr {
	height: 50px
}

#wjs_jiwon {
	margin:auto;
    border-collapse:collapse; 
    border-spacing:0;
}

table td{
	padding:0px;
}

#wjs_back{
    width: 80px;
    height: 40px;
    color: #4876EF;
}
#wjs_choice{
    text-align: center;
    height: 70px;
   
}
#wjs_p{
    padding-top: 30px;
    font-size: 24px;
    font-weight: bold;
}

#wjs_company{
   
    height: 50px;
    text-align: center;
}

#wjs_p2{
    font-size: 18px;
    color: rgb(189, 188, 188);
}
#wjs_p3{
   
  margin: 0 auto;
  width: 500px;
  margin-left: 30px;

}
#wjs_resume{
    margin-left: 30px;
    height: 200px;
    margin-top: 30px;
    border: 1px solid rgb(197, 197, 197);
}

#wjs_p4{
    font-size: 20px;
   
}

#wjs_memberResume{
    height: 200px;
    
    text-align: center;
}
#wjs_preview{
    margin-top: 10px;
    margin-left: 30px;
    color: #4876EF;
    border: 0;
    outline: 0;
    float:right;
    background-color: white;
}
#wjs_img{
    margin-top: 70px;
    margin-left: 30px;
}
#wjs_img2{
    margin-top: 70px;
   
}
#wjs_img3{
    margin-top: 70px;
    margin-left: 50px;
}
</style>
</head>
<body>
	<%@include file="/views/common/menubar.jsp" %>
	<!-- 메뉴바 끝 -->


	<table id="wjs_jiwon">
		<tr>
			<td id="nav"></td>
			<td id="section" style="vertical-align: 0px;">
				<div class="article1"></div>
					<!-- 로그인 메시지 시작 -->

				<!-- 로그인 메시지 끝 -->
				

                <div id="wjs_back">
                    돌아가기
                </div>

                <div style="text-align: center;"><img src="/itda/image/wjs/miri.PNG"></div>

                <div id="wjs_choice">
                    <p id="wjs_p">이력서 선택</p>
                </div>
                <div id="wjs_company">
                    <p id="wjs_p2">회사 이름 채용 내용</p>
                </div>
        

                <div id="wjs_resume">
                    <div id="wjs_img" style="float: left;">
                        
                        <img src="/itda/image/wjs/resume2.PNG" style="width: 40px; height: 50px; float: left;">
                    </div>
                    <div id="wjs_img2" style="float: left;">
                        
                        <p id="wjs_p3" style="font-weight: bold;">이력서 이름</p>
                        <p id="wjs_p3">수정날짜 불러오기</p>
                    </div>
                    <div id="wjs_img3" style="float: left;">
                        <button id="wjs_preview">미리보기</button>
                    </div>
              
                    
                </div>
                
                <br><bR><br>    
                <hr>
                <button id="next">다음</button>
                <div id="article3"></div>

			</td>
			<td id="aside"></td>
		</tr>
	</table>
	

	<%@ include file="../../common/cho_footer.jsp"%>
	
	

</body>
</html>