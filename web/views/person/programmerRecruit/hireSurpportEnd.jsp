<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
	String duty = (String) request.getAttribute("duty");
%>
<%
	String name = (String) request.getAttribute("name");
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<style>

/*----------------------------------------- 메뉴바 style 영역 시작 -------------------------------------  */
.head-blue-text-bold {
	color: #5A84F1;
	font-weight: bold;
}


header {
	margin-left: auto;
	margin-right: auto;
	width: 1400px;
	height: 100px;
}

.head1 {
	height: 60%;
	margin: 0px;
	padding: 0px;
}

.head1-1 {
	height: 100%;
	float: left;
}

.head1-1-empty {
	width: 7.5%;
}

.head1-2-logo {
	width: 7.5%;
}

.head1-3-search {
	width: 25%;
}

input[name=menubar-search] {
	height: 80%;
	width: 60%;
	float: left;
}

.head1-1-3-menubar-search {
	height: 50%;
	margin-top: 3.86%;
}

.search-img-space {
	border: solid;
	width: 10%;
	height: 85%;
	background: blue;
	float: left;
}

.search-img {
	width: 100%;
	max-width: 25px;
	vertical-align: middle
}

.head1-4-empty {
	width: 45%;
}

.head1-5 {
	width: 15%;
}

.head1-5-1 {
	width: 100%;
	height: 50%;
	float: left;
	margin-top: 15px;
}

.head1-5-1-alarm-img-space {
	width: 30%;
	height: 100%;
	float: left;
	margin-left: auto;
	margin-right: auto;
}

.alarm-img {
	width: 100%;
	max-width: 30px;
	vertical-align: middle;
}

.head1-5-2-mypage-label-space {
	width: 35%;
	height: 21.6px;
	float: left;
	margin-left: auto;
	margin-right: auto;
	margin-top: 2.2px;
}

.head1-5-3-login-label-space {
	width: 35%;
	height: 21.6px;
	float: left;
	margin-left: auto;
	margin-right: auto;
	margin-top: 4.2px;
}

.head2 {
	height: 40%;
	background-color: #4876EF;
}

.head2-common {
	height: 100%;
	float: left;
}

.head2-1 {
	width: 30%;
}

.head2-2 {
	width: 40%;
}

.head2-3 {
	width: 30%;
}

.head2-text {
	font-weight: bold;
	color: white;
	width: 80%;
	height: 50%;
	margin-left: auto;
	margin-right: auto;
	margin-top: 10px;
}

.head2-2-text1 {
	width: 30%;
}

.head2-2-text2 {
	width: 25%;
	margin-left: 23px;
	margin-right: 23px;
}

.head2-2-text3 {
	width: 35%;
}

/*----------------------------------------- 메뉴바 style 영역 끝 -------------------------------------  */
.article1 {
	width: 840px;
	height: 75px;
}

.article2 {
	width: 840px;
	height: 75px;
	text-align: center;
}
#article3 {
	width: 840px;
	height: 75px;
}

#nav {
	width: 280px;
    
}
#section{
    height: 900px;

}

#aside {
	width: 280px;

}

footer {
	margin-left: auto;
	margin-right: auto;
	width: 1400px;
	height: 100px;
}

#info-check {
	width: 60%;
	margin: auto;
	text-align: center;
}

#next {
	background: #5A84F1;
	color: white;
	font-weight: bold;
	border-radius: 5px;
	font-size: 12pt;
	border: 0;
	outline: 0;
    width: 300px;
    height: 40px;
   	text-align: center;
   	
   
   
}
#list {
	background: #D8D8D8;
	color: black;
	font-weight: bold;
	border-radius: 5px;
	font-size: 12pt;
	border: 0;
	outline: 0;
    width: 300px;
    height: 40px;
   	text-align: center;
   	
   
   
}


#tr {
	height: 50px
}

#wjs_jiwon {
	margin:auto;
    border-collapse:collapse; 
    border-spacing:0;
}

table td{
	padding:0px;
}

#wjs_back{
    width: 80px;
    height: 40px;
    color: #4876EF;
}

#wjs_jiwonEnd{
	margin-left: auto;
	margin-right: auto;
	border: 1px solid rgb(172, 172, 172);
	width: 500px;
	height: 700px;
	border-radius: 10px;

}
#wjs_btn1{
	width: 500px;
	height: 100px;
	text-align: center;
}

#wjs_check{
	width: 60px;
	height: 80px;
	margin-left: 230px;
	margin-top: 40px;
}

#wjs_text{
	margin-top: 30px;
	text-align: center;
}
#wjs_table{
	text-align: center;
	height: 50px;
	vertical-align: auto;
	margin-left: auto;
	margin-right: auto;
}

#wjs_td{
	width: 100px;
	color: darkgray;
	text-align: left;
}
#wjs_td2{
	width: 280px;
}
</style>
</head>
<body>
	<%@include file="/views/common/menubar.jsp" %>
	<!-- 메뉴바 끝 -->


	<table id="wjs_jiwon">
		<tr>
			<td id="nav"></td>
			<td id="section" style="vertical-align: 0px;">
				<div class="article1"></div>
					<!-- 로그인 메시지 시작 -->

				<!-- 로그인 메시지 끝 -->
				

				<div id="wjs_jiwonEnd">
					
					<img src="/itda/image/wjs/end.png" id="wjs_check">

					<div id="wjs_text">
						<p style="font-weight: bold; font-size: 25px; text-align: center;">지원이 완료되었습니다.</p>
						
						<hr align="center" style="border: solid 1px #D8D8D8; width: 80%;">
						<table id="wjs_table">	
							<tr style="height: 60px;">
								<td id="wjs_td">기업명</td>
								<td id="wjs_td2" style="font-weight: bold"><%=name %></td>
							</tr>
							<tr>
								<td id="wjs_td">포지션</td>
								<td id="wjs_td2" style="font-weight: bold"><%=duty %></td>
							</tr>
						</table>
						<br>
						<hr align="center" style="border: solid 1px #D8D8D8; width: 80%;">
						
											
					<br><br>
					<p style="font-size: 16px;">지원이 완료되었습니다. 채용담당자의 검토 결과는 <br>
					IDTA 웹사이트 또는 이메일을 통해 안내해드립니다.<br>
					제출하신 이력서는 아래버튼을 통해 확인하실 수 있습니다.</p>
				<br><br>	
                <button id="next">지원 상태보기</button>
                <br><br>
                <button id="list" onclick="list();">채용 공고목록보기</button>		
                
                <br>			
			</div>
			</div>
     
                <div id="article3"></div>

			</td>
			<td id="aside"></td>
		</tr>
	</table>
	

	<%@ include file="../../common/cho_footer.jsp"%>
	<script>
	
	function list(){
		location.href ="<%=request.getContextPath()%>/beforeSearch.anon";
	}
	

</script>


</body>
</html>