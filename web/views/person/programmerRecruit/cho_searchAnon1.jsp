<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@page import="java.util.*" %>
    <%@page import="com.kh.itda.recruitAndContest.model.vo.* "%>
    <%
    	ArrayList<Announcement> ann=(ArrayList<Announcement>)request.getAttribute("list");
    
    %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>



    <style>
        #searchTable{
            width: 1400px;
            margin-left: auto;
            margin-right: auto;
            margin-bottom:50px
        }
        
        #searchMainDiv{
            margin-left: auto;
            margin-right: auto;
            width: 75%;
           
        }
        #searchValue{
        	 margin-left: auto;
            margin-right: auto;
            width: 80%;
        
        }
    
        .searchAno_skill{
            height:31px;
            padding-left:31px;
           /* background-image:#fff url("../image/searchSkill.png") no-repeat;*/
            background:#fff url('/itda/image/searchSkill.png') no-repeat;
        }
         .searchAno_address{
            height:31px;
            padding-left:31px;
           /* background-image:#fff url("../image/searchSkill.png") no-repeat;*/
            background:#fff url('/itda/image/searchAddress.PNG') no-repeat;
        }
        .section {
	overflow: hidden;
	padding: 20px;
	width: 1160px;
	margin-top: 0px;
	border: 1px solid lightgray;
	/* border-top: 1px; */
	/* border-bottom: 1px; */
}


 .col-sm-3{
			margin-bottom:10px; 
       		border:solid 1px #939292;
       		 height:300px; 
       		 width:260px;
		}
		.col-sm-2{
			height: 200px;
			margin-bottom:10px;
			border:solid 1px #939292;
			height:230px;
		}
		.col-sm-1{
			height: 100px;
		}
		.sb_menubar_update{
			width:1400 
			border-top: 1px solid gray; 
			border-bottom: 1px solid gray;
		}
		#searchButton{
            
            width: 50px;
            height: 40px;
            border-radius: 0.5pc;
            background-color: #5A84F1;
            color: white;
            font-size: 12pt;
       }
      
       .col-sm-3:hover{
       	border:solid 3px #5A84F1;
       }
       .col-sm-2:hover{
       	border:solid 3px #5A84F1;
       }
       .col-sm-1:hover{
       	border:solid 3px #5A84F1;
       }
       .hideDiv{
        	display:none;
        } 
    </style>
    
    
   
</head>
<body>
<%@include file="/views/common/menubar.jsp" %>
<%@include file="/views/person/person_common/sb_programmerHireMenubar.jsp" %>

<form action="<%= request.getContextPath() %>/afterSearchAnnon.sea" method="post">
<script type="text/javascript">
    $(function(){
    
    	var oldVal;
		 $(".searchAno_skill").on("propertychange change keyup paste input", function() {
			    var currentVal = $(this).val();
			    if(currentVal == oldVal) {
			        return;
			    }
			 console.log("hoope");
			    oldVal = currentVal;
			    
			    $("#skillNY").prop("checked",true);
			    
			});
		 
		 var oldVal1;
		 $(".searchAno_address").on("propertychange change keyup paste input", function() {
			    var currentVal = $(this).val();
			    if(currentVal == oldVal1) {
			        return;
			    }
			 console.log("hoope");
			    oldVal1 = currentVal;
			    
			    $("#locationNY").prop("checked",true);
			    
			});
	    	 

		      
		    });
			  		 
 
    
    </script>
<script type="text/javascript">
$(function() {
var availableTags = [
	"DevOn","Front-End","Gauce","Java","Jenkins","Maven","Miplatform","Nexacro","Pro*C","Proframe",
	"Sencha","Spring","Thymeleaf","TrustForm","Tuxedo","Xplatform","Android","Hybrid","iOS(Object-C)",
	"iOS(Swift)","IoT","WebView","ASP","Codeigniter","Laravel","PHP","ZendFramework","Symfony","WordPress",
	"C","C++","ASP.net","C#","MFC","OpenGL","DevExpress","VBA","javaScript","node.js","AngularJs","React.js",
	"Vue.js","jQuery","Embedded","Firmware","LabVIEW","MachineVision","Server","UNIX","MetaLab","Qt",
	"Aduino","CUBRID","MariaDB","MongoDB","MSSQL","MySQL","Oracle","Postgresql","Tibero","ActionScript",
	"CSS","Flash","Git","HTML5","웹접근성","웹표준","MobileApp",".NET","DB","Publisher"
]; 

$("#tags").autocomplete({
  source: availableTags


	});
});

</script>

  <table id="searchTable">
  	
    <tr>
        <!-- 광고역역 -->
        <td colspan="3">
            <div style="width: 1400px;height: 200px;">

            </div>
        </td>
    </tr>
    <tr>
        <td colspan="3">
           
            <div id="selectDiv" style="height: 100px;width: 1400px; background-color: #F0F0F0;">
                <div id="searchMainDiv">
                	<table style=" margin-right:auto;margin-left:auto;">
                	<tr>
                		<td>
                		<div  class="hideDiv">
							<input type="radio" name="skillYN" id="skillYN"  value="N"  checked="checked"><label></label>
							<input type="radio" name="skillYN" id="skillNY"value="Y"><label></label>
						</div>
						
                    		<input class="searchAno_skill" type="text" id="tags"  name="searchSkill" placeholder="기술 태크 검색"  autocomplete='on' style="float:left;margin: 15px;">
                    	</td>
                    	<td>
                    	<div  class="hideDiv">
							<input type="radio" name="locationYN" id="locationYN"  value="N"  checked="checked"><label></label>
							<input type="radio" name="locationYN" id="locationNY"value="Y"><label></label>
						</div>
                    		<input class="searchAno_address"  type="text" name="searchLocation" placeholder="지역 검색 (시/군/구)"style="float:left;margin: 15px;">
                    	</td>
                    <td>
                    <select name="career" style="width: 100px;height: 32px;margin: 15px;float:left;">
                        <option value="N">경력</option>
                        <option value="신입">신입</option>
                        <option value="1년">1년</option>
                        <option value="2년">2년</option>
                        <option value="3년">3년</option>
                        <option value="4년">4년</option>
                        <option value="5년">5년</option>
                        <option value="6년">6년</option>
                        <option value="7년">7년</option>
                        <option value="8년">8년</option>
                        <option value="9년">9년</option>
                    </select>
                    </td>
                    <td>
                    <select name="duty" style="width: 100px;height: 32px;margin: 15px;float:left;">
                        <option value="N">직무</option>
                        <option value="웹 프로그래머">웹 프로그래머</option>
                        <option value="DBA">DBA,데이터베이스</option>
                        <option value="응용 프로그래머">응용 프로그래머</option>
                        <option value="웹 기획">웹 기획PM</option>
                        <option value="시스템 프로그래머">시스템 프로그래머</option>
                        <option value="게임">게임</option>
                        <option value="웹 디자인">웹 디자인</option>
                        <option value="HTML">HTML,퍼블리싱 UI개발</option>
                        <option value="웹 마케팅">웹 마케팅</option>
                       
                    </select>
                    </td>
                    <td>
                    	<button id="searchButton" type="submit" >검색</button>
                    </td>
                    </tr>
                    <tr>
                    <td colspan="5">
                    	<div id="searchValue">
                     <!-- for문으로 div를 반복해서 값을 넣어줌 -->
                     
                     	
                 	</div>
                    </td>
                    </tr>
                    </table>
                 </div>
                 
            </div>
        </td>
    </tr>
    <tr>
        <td id="searchSied" style="width:180px;">
       
        </td>
        <td id="searchMain">
           
        
	<!-- 부트 스트랩쓴거 -->

	<div class="container-fluid">
				
				<div class="row">
				<table>
				<tr class="top">
				<td>
				<%for(Announcement a:ann){
	 %>
					<div class="col-sm-3" class="gonggo1" onclick="gonggoClick('<%=a.getaNo() %>')" >
					<div style="width: 280px;margin-left: auto;margin-right: auto;">
					<%if(a.getChangeName()!=null){ %>
						<img src="<%=request.getContextPath() %>/uploadFiles/<%=a.getChangeName() %>" style="width: 230px;height: 150px;margin-top: 6px;">
						<%}else{ %>
						<img src="image/AnnNoIMg.png" style="width: 230px;height: 150px;margin-top: 6px;">
						<%} %>
						<%String[] location=a.getLocation().split(" "); 
						String topSi="";
						for(int i=0;i<3;i++){
							topSi+=location[i];
							topSi+=" ";
						}
						if(a.getaTitle().length()>17){
						String[] topTitle=a.getaTitle().split("");
						 String toptitle1="";
						for(int i=0;i<14;i++){
							toptitle1+=topTitle[i];
						}
						System.out.println(toptitle1);%> 
						
						<h4 style="font-weight: 700;"><%=topTitle %>
						<%}else{%>
							<h4 style="font-weight: 700;"><%=a.getaTitle() %>
						<% }
						%>
						<input type="hidden" name="a_no" value="<%=a.getaNo()%>">
						<input type="hidden" name="a_skill" value="<%=a.getSkill() %>">
						<input type="hidden" name="a_career" value="<%=a.getCareer() %>"> 
						<input type="hidden" name="a_address" value="<%=a.getLocation()%>">
						</h4>
						<h5><%=topSi%></h5>
						<h5><%=a.getCom_nmae() %></h5>
			</div>
		</div>
		</td></tr>	
		<%}%>
		
	
	</table>
				</div>
			  </div>
	
	
        </td>
        <td id="searchAside" style="width:180px;">

        </td>
    </tr>
	
  </table>
  </form>
  <script type="text/javascript">
  	function gonggoClick(num){
  			location.href="<%=request.getContextPath()%>/selectAn.me?num="+num;
  			/* alert("클릭"); */
  	}
  	
  
  </script>
    <%@include file="/views/common/cho_footer.jsp" %>
    
</body>
</html>