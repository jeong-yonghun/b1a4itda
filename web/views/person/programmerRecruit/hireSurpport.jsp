<%@page import="com.kh.itda.addInfo.resume.model.vo.Resume"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
	ArrayList<Resume> list = (ArrayList) request.getAttribute("list");
%>
<%
	String ano = (String) request.getAttribute("ano");
%>
<%
	String tt = (String) request.getAttribute("tt");
%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<style>

/*----------------------------------------- 메뉴바 style 영역 시작 -------------------------------------  */
.head-blue-text-bold {
	color: #5A84F1;
	font-weight: bold;
}


header {
	margin-left: auto;
	margin-right: auto;
	width: 1400px;
	height: 100px;
}

.head1 {
	height: 60%;
	margin: 0px;
	padding: 0px;
}

.head1-1 {
	height: 100%;
	float: left;
}

.head1-1-empty {
	width: 7.5%;
}

.head1-2-logo {
	width: 7.5%;
}

.head1-3-search {
	width: 25%;
}

input[name=menubar-search] {
	height: 80%;
	width: 60%;
	float: left;
}

.head1-1-3-menubar-search {
	height: 50%;
	margin-top: 3.86%;
}

.search-img-space {
	border: solid;
	width: 10%;
	height: 85%;
	background: blue;
	float: left;
}

.search-img {
	width: 100%;
	max-width: 25px;
	vertical-align: middle
}

.head1-4-empty {
	width: 45%;
}

.head1-5 {
	width: 15%;
}

.head1-5-1 {
	width: 100%;
	height: 50%;
	float: left;
	margin-top: 15px;
}

.head1-5-1-alarm-img-space {
	width: 30%;
	height: 100%;
	float: left;
	margin-left: auto;
	margin-right: auto;
}

.alarm-img {
	width: 100%;
	max-width: 30px;
	vertical-align: middle;
}

.head1-5-2-mypage-label-space {
	width: 35%;
	height: 21.6px;
	float: left;
	margin-left: auto;
	margin-right: auto;
	margin-top: 2.2px;
}

.head1-5-3-login-label-space {
	width: 35%;
	height: 21.6px;
	float: left;
	margin-left: auto;
	margin-right: auto;
	margin-top: 4.2px;
}

.head2 {
	height: 40%;
	background-color: #4876EF;
}

.head2-common {
	height: 100%;
	float: left;
}

.head2-1 {
	width: 30%;
}

.head2-2 {
	width: 40%;
}

.head2-3 {
	width: 30%;
}

.head2-text {
	font-weight: bold;
	color: white;
	width: 80%;
	height: 50%;
	margin-left: auto;
	margin-right: auto;
	margin-top: 10px;
}

.head2-2-text1 {
	width: 30%;
}

.head2-2-text2 {
	width: 25%;
	margin-left: 23px;
	margin-right: 23px;
}

.head2-2-text3 {
	width: 35%;
}

/*----------------------------------------- 메뉴바 style 영역 끝 -------------------------------------  */
.article1 {
	width: 840px;
	height: 75px;
}

.article2 {
	width: 840px;
	height: 75px;
	text-align: center;
}
#article3 {
	width: 840px;
	height: 75px;
}

#nav {
	width: 280px;
    
}
#section{
    height: 900px;

}

#aside {
	width: 280px;

}

footer {
	margin-left: auto;
	margin-right: auto;
	width: 1400px;
	height: 100px;
}

#info-check {
	width: 60%;
	margin: auto;
	text-align: center;
}

#next {
	background: #5A84F1;
	color: white;
	font-weight: bold;
	border-radius: 5px;
	font-size: 13pt;
	border: 0;
	outline: 0;
    width: 70px;
    height: 40px;
    margin-left: 400px;
    margin-top: 50px;
}


#tr {
	height: 50px
}

#wjs_jiwon {
	margin:auto;
    border-collapse:collapse; 
    border-spacing:0;
}

table td{
	padding:0px;
}


#wjs_back{
    width: 80px;
    height: 40px;
    color: #4876EF;
    font-weight: bold;
    float:right;
    margin-right: 50px;
    vertical-align: top;
    margin-top: 70px;
}
#wjs_choice{
    text-align: center;
    height: 100px;
   
}
#wjs_p{
    padding-top: 30px;
    font-size: 24px;
    font-weight: bold;
}

#wjs_company{
   
    height: 70px;
    text-align: center;
}

#wjs_p2{
    font-size: 18px;
    color: rgb(189, 188, 188);
}
#wjs_p3{
    float: left;
    margin-left: 20px;
    color: #4876EF;
    font-weight: bold;

}
#wjs_pp{
    float: left;
    margin-left: 30px;
    color: black;

}
#wjs_resume{
    margin-left: 30px;
    height: 50px;
    margin-top: 30px;
}

#wjs_p4{
    font-size: 20px;
   
}

#wjs_memberResume{
    height: 200px;
    
    text-align: center;
}
#wjs_resume1{
    margin-left: 30px;
    height: 70px;
    margin-top: 30px;
}
#wjs_resume2{
    margin-left: 30px;
    height: 70px;
    margin-top: 30px;
}
#select{
	float: right; text-align: center; height: 33px; margin-top:10px; width:70px;  margin-right: 10px;
	border: 0;
	outline: 0;
	color: #0431B4;
	font-weight: bold;
	font-size: 13px;
}
#hr{
	border: solid 1px #F2F2F2;
}
.div{
	height: 700px;
}
#wjs_img3{
    margin-top: 70px;
    margin-left: 50px;
}
#wjs_preview{
    margin-top: 10px;
    margin-left: 30px;
    color: #4876EF;
    border: 0;
    outline: 0;
    float:right;
    background-color: white;
    font-weight: bold;
}
</style>
</head>
<body>
		<%@include file="/views/common/menubar.jsp" %>
	<!-- 메뉴바 끝 -->


	<table id="wjs_jiwon">
		<tr>
			<td id="nav" style="vertical-align: top;">
			
				<div id="wjs_back">
                  	   돌아가기
                </div>
			</td>
			<td id="section" style="vertical-align: 0px;">
				<div class="article1"></div>
					<!-- 로그인 메시지 시작 -->

				
				<!-- 로그인 메시지 끝 -->

                <div style="text-align: center;"><img id="wjs_logo" src="/itda/image/wjs/jiwon.PNG"></div>

                <div id="wjs_choice">
                    <p id="wjs_p">이력서 선택</p>
                </div>
                <div id="wjs_company">
                    <p id="wjs_p2"><%=tt %></p>
                </div>
                <hr id="hr">
				
                <div id="wjs_resume"  class="div1">
                    <img src="/itda/image/wjs/resume.PNG" style="width: 40px; height: 50px; float: left;">
                    <p id="wjs_p3" onclick="location.href = '<%=request.getContextPath()%>/beforeinsert.re'"> 새로운 이력서 등록</p>
                
                <br><br><br><br>
    			<hr id="hr">            
	               
                <br>
                <%if(list != null){
                	
                %>	
                <%for(Resume r : list){ %>
                <div id="wjs_resume1" >
                     <div id="wjs_img" style="float: left;">
                         
                         <img src="/itda/image/wjs/resume2.PNG" style="width: 40px; height: 50px; float: left;">
                     </div>
                     <div id="wjs_img2" style="float: left;">
                         <div style="width: 100px; float: left">
                         <p id="wjs_pp" style="font-weight: bold;"><%=r.getrTitle() %></p>
                         </div>
                         <div style="width: 200px; float: left"></div>
                         <p id="wjs_pp"><%=r.getrUpdateDate() %>에 업데이트 됨</p>
                     </div>
                     <button id="select" class="select">선택</button>
                    
                         <input type="hidden" id="pno" value="<%=r.getPerNo()%>">
                         <input type="hidden" id="rno" value="<%=r.getrNo()%>">
                 </div>	
                <%} 	
                }else{
                %>	
                <div id="wjs_memberResume">
                    <p id="wjs_p4">등록된 이력서가 없습니다. 새로운 이력서를 작성해주세요.</p>
                </div>
                
                <%	
                }
                %>
                </div>
                <br><br>
                <form id="surpport" action="<%=request.getContextPath() %>/surpportEnd.re" method="post">
                 <div id="wjs_resume1" class="div2" style="display: none;">
                    <div id="wjs_img" style="float: left;">
                        <img src="/itda/image/wjs/resume2.PNG" style="width: 40px; height: 50px; float: left;">
                    </div>
                    <div id="wjs_img2" style="float: left;">
                            
                        <p id="wjs_p3" class="wjs_name" style="font-weight: bold;">이력서 이름</p>
                        <p id="wjs_p3" class="wjs_update">수정날짜 불러오기</p>
                         <input type="hidden" id="ha" name="ano" value=<%=ano%>>
                         <input type="hidden" id="rno" name="rno" value="">
                         <input type="hidden" id="pno" name="pno" value="">
                    </div>
                        <div id="wjs_preview" style="float: right; margin-top: 20px;" onclick="miri();">미리보기</div>
                </div>
				</form>

                <button id="next" >다음</button>
				<br><br>
                <div id="article3"></div>

			</td>
			<td id="aside"></td>
		</tr>
	</table>

	<%@ include file="../../common/cho_footer.jsp"%>

<script>
	
	
	var rno = "";
	
	var num = 1;
	var num2 = 1;
	var name ="";
	var pno = "";
	var rno = "";
	var update ="";
	
	$(".div1 button").eq(0).click(function(){
		name = $(".div1").eq(0).children().children().children().children("p").eq(0).text();
		pno = $(".div1 input").eq(0).val();
		rno = $(".div1 input").eq(1).val();
		update = $(".div1").eq(0).children().children().children("p").eq(0).text();
		$(".wjs_name").text(name);
		$(".wjs_update").text(update);
		$(".hu").val(update);
		$(".ht").val(name);
		$("input[type=hidden][name=pno]").val(pno);
		$("input[type=hidden][name=rno]").val(rno);
		console.log($("#pno").val(pno));
		console.log($("#rno").val(rno));
		console.log(<%=ano%>);
		$(".select").val(rno);
		
		
	});
	
	function miri(){
		window.open("<%=request.getContextPath()%>/resumeDetail.re?rNo="+rno, "_blank|_self"); 
	}
	
	$(".div1 button").eq(1).click(function(){
		name = $(".div1").eq(0).children().children().children().children("p").eq(1).text();
		update = $(".div1").eq(0).children().children().children("p").eq(1).text();
		rno = $(".div1 input").eq(3).val();
		pno = $(".div1 input").eq(4).val();
		console.log(name);
		console.log(update);
		$(".wjs_name").text(name);
		$(".wjs_update").text(update);
		$(".hu").val(update);
		$(".ht").val(name);
		$("input[type=hidden][name=pno]").val(pno);
		$("input[type=hidden][name=rno]").val(rno);
		console.log(pno);
		console.log(rno);
		$(".select").val(rno);
	});
	$(".div1 button").eq(2).click(function(){
		name = $(".div1").eq(0).children().children().children().children("p").eq(2).text();
		update = $(".div1").eq(0).children().children().children("p").eq(2).text();
		rno = $(".div1 input").eq(5).val();
		pno = $(".div1 input").eq(6).val();
		$(".wjs_name").text(name);
		$(".wjs_update").text(update);
		$(".hu").val(update);
		$(".ht").val(name);
		$("input[type=hidden][name=pno]").val(pno);
		$("input[type=hidden][name=rno]").val(rno);
		console.log(pno);
		console.log(rno);
		$(".select").val(rno);
	});
	
	
	$("#next").click(function() {
        if(num < 2){
            $(".div" + num).hide();
            num++;
		    $("#wjs_logo").attr("src", "/itda/image/wjs/miri.PNG");
		    $("#wjs_p").text("미리보기");
		    $(".div" + num).show();
		    
		} else if(num == 2){
			location.href = "<%=request.getContextPath()%>/surpportEnd.re";
			$("#surpport").submit();
		} 
        if(num == 1){
            $("#back").hide();
        }else{
            $("#back").show();
        }
	});
	
	$(".select").click(function(){
		
		rno = $(this).val();
		if($(this).val() == rno){
			$(this).css("background", "#819FF7");
			$(this).css("color", "white");
		}else{
			$(this).css("background", "#F0F0F0");
		}
	});
	
	<%-- $("#wjs_resume").click(function(){
		
		location.href = "<%=request.getContextPath()%>/beforeinsert.re";
		
	});
	 --%>
	
		
</script>

</body>
</html>