<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" import="com.kh.itda.member.model.vo.Jyh_PerMember"%>
	<%
	
	
	%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<title>Insert title here</title>

<style>
#cho_userInfo {
	width: 200px;
	height: 300px;
	border: 1px #CCCCCC solid;
	border-radius: 1pc;
	margin-left: auto;
	margin-right: auto;
	text-align: center;
	font-family: 'Noto Sans KR', sans-serif;
	margin-top: 20px;
}

#cho_personImgDiv {
	width: 60px;
	height: 60px;
	border: solid #5A84F1 1px;
	border-radius: 10pc;
	margin-right: auto;
	margin-left: auto;
	margin-top: 15px;
}

#cho_userImg {
	width: 100%;
	height: 100%;
	border-radius: 10pc;
	margin-right: auto;
	margin-left: auto;
}

#cho_deletemember {
	margin-top: 70px;
	color: red;
}

#cho_sideMenubar {
	width: 200px;
	height: 400px;
	border: 2px solid #5A84F1;
	margin-left: auto;
	margin-right: auto;
	margin-top: 30px;
	font-family: 'Noto Sans KR', sans-serif;
}

#cho_sideMenubarTable {
	width: 180px;
	height: 90%;
	margin-left: auto;
	margin-right: auto;
}

#cho_sideMenubarTableTHaedP {
	font-size: 18px;
	font-weight: bold;
}

.sideSubTitle {
	font-size: 16px;
	font-weight: bold;
	border-top: 1px solid #C4C4C4;
	padding-top: 10px;
	padding-bottom: 10px;
}

.sideSide {
	padding-left: 20px;
}

#wjs_li {
	height: 50px;
}

input[type=checkbox] {
	display: none;
}

input[type=checkbox]+label {
	display: inline-block;
	cursor: pointer;
	position: relative;
	padding-left: 25px;
	margin-right: 15px;
	font-size: 13px;
}

input[type=checkbox]+label:before {
	content: "";
	display: inline-block;
	width: 20px;
	height: 20px;
	margin-right: 10px;
	position: absolute;
	left: 0;
	bottom: 1px;
	background-color: #ccc;
	border-radius: 2px;
	box-shadow: inset 0px 1px 1px 0px rgba(0, 0, 0, .3), 0px 1px 0px 0px
		rgba(255, 255, 255, .8);
}

input[type=checkbox]:checked+label:before {
	content: "\2713";
	text-shadow: 1px 1px 1px rgba(0, 0, 0, .2);
	font-size: 18px;
	font-weight: 800;
	color: #fff;
	background: #2f87c1;
	text-align: center;
	line-height: 18px;
}

#wjs_div {
	width: 570px;
	height: 90px;
	background: #FDD3C9;
	margin-left: auto;
	margin-right: auto;
	padding-left: 10px;
	border-radius: 8px;
	border: 1px solid darkgray;
}
</style>
</head>
<body>
	<!-- 사이드에 회원 정보창 -->
	<div id="cho_userInfo">
		<div id="cho_personImgDiv">
			<img id="cho_userImg" src="image/hyj_test.jpg">
		</div>
		<!-- 오류는 뜨지만 작동은 잘됨 -->
		<h3 id="cho_userName"><%=loginUser.getPerName() %></h3>
		<h4 id="cho_phone"><%=loginUser.getPhone() %></h4>
		<h4 id="cho_email"><%=loginUser.getEmail() %></h4>
		<h5 id="cho_github"></h5>
		<!--  <p id="cho_deletemember" onclick="deletemember()">회원탈퇴</p> -->
		<button type="button" class="btn btn-primary btn-lg"
			style="background: white; color: red; font-size: 13px; outline: 0; border: 0;"
			data-toggle="modal" data-target="#myModal">회원탈퇴</button>
	</div>

	<!-- 개인마이페이지 사이드 메뉴바 미완성!!!!!!!!!!!!!!!!클릭할때 색바뀌는거하기-->
	<div id="cho_sideMenubar">
		<table id="cho_sideMenubarTable">

			<tr id="cho_sideMenubarTableTHaed">
				<td><p id="cho_sideMenubarTableTHaedP">My Page</p></td>
			</tr>
			<tr>
				<td class="sideSubTitle" id="cho_dateil" onclick="goDateil()">나의
					상세정보</td>
			</tr>
			<tr>
				<td class="sideSubTitle">이력서 관리</td>
			</tr>
			<tr>
				<td class="sideSide" id="cho_resumewri" onclick="goResumeWriter()">-
					이력서 작성하기</td>
			</tr>
			<tr>
				<td class="sideSide" id="cho_resume" onclick="goResu()">- 이력서
					열람</td>
			</tr>
			<tr>
				<td class="sideSubTitle">지원 관리</td>
			</tr>
			<tr>
				<td class="sideSide" id="cho_contest" onclick="goContestApp()">-
					공모전 지원</td>
			</tr>
			<tr>
				<td class="sideSide" id="cho_recruit" onclick="goRecruit()">-
					채용 지원</td>
			</tr>
			<tr>
				<td class="sideSide" id="cho_proposed" onclick="goProposed()">-
					제안받기</td>
			</tr>
			<tr>
				<td class="sideSubTitle" id="cho_portfolio" onclick="goPortfoli()">포트폴리오</td>
			</tr>
			<tr>
				<td class="sideSubTitle" id="cho_writed" onclick="goWrited()">내가
					쓴글</td>
			</tr>
			<tr>
				<td class="sideSubTitle" id="cho_bookMark" onclick="goBookMark()">나의
					즐겨찾기</td>
			</tr>
		</table>



	</div>
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title" id="myModalLabel" style="font-weight: bold;">회원 탈퇴</h4>
				</div>
				<div class="modal-body">
					<br>
					<p>아이디 님의 itda 계정 삭제를 선택하셨습니다.</p>
					<br> <br>
					<ul>
						<li id="wjs_li">고객님의 이름, 이메일, 프로필 사진, 한 줄 소개, 웹사이트, 활동 지역,
							소속, 이력서를 포함한 모든 개인정보를 삭제합니다.</li>
						<li id="wjs_li">삭제한 정보는 복구할 수 없습니다.</li>
						<li id="wjs_li">북마크, 관심 태그, 점수, 활동 내용이 모두 삭제합니다.</li>
						<li id="wjs_li">질문/답변 등의 게시물은 "탈퇴한 사용자"의 게시물로 남습니다.</li>
						<li id="wjs_li">재가입은 3개월뒤에 가능합니다</li>
					</ul>

					<div id="wjs_div">
						<br> <input onclick="wjs_check();" type="checkbox"
							id="divECI_ISDVSAVE" class="checkbox-style"
							style="margin-left: 30px;"><label for="divECI_ISDVSAVE">약관을
							동의합니다</label>
						<div style="font-weight: bold;">계정을 삭제하면 되돌릴 수 없으며, 삭제한 데이터를
							복구할 수 없음을 이해했습니다.</div>
					</div>

					<br> <br>

					<h4>탈퇴사유 작성</h4>
					<br>

					<!-- <div class="btn-group" role="group" aria-label="...">
						<button type="button" class="btn btn-default" id="wjs_ph" >휴대폰
							인증</button>
						<button type="button" class="btn btn-default" id="wjs_em">이메일
							인증</button>
					</div>-->
					<div>
						<textarea rows="5" cols="75" style="resize: none"></textarea>
					</div>

					<!-- <div class="well well-lg" id="wjs_phone" style="height: 100px;" >
						<div class="info-area phone-area">
							<div class="info-div phone-div-per">휴대폰 번호</div>
							<input class="info-input-btn-per" type="text" name="phone-per"
								placeholder="휴대폰 번호(-제외)">
							<button class="jyh_btn-identify jyh_btn-phone-per">인증번호</button>
						</div> -->
						<br>

<!-- 
					</div>
					<div class="well well-lg" id="wjs_email" style="display: none; height: 100px;">
					<div class="info-area email-area" >
						<div class="info-div email-div">이메일 주소</div>
						<input class="info-input-btn-per" type="text" name="email-per"
							placeholder="이메일 주소">
						<button class="jyh_btn-identify jyh_btn-email-per">인증번호</button>
					</div>
					</div> -->


				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">취소</button>
					<button type="button" id="wjs_delete" class="btn btn-primary"
						onclick="" disabled="disabled" style="background-color: #FF4500;">회원탈퇴</button>
				</div>
			</div>
		</div>



	</div>
	<script>
		function goDateil() {
			var menu = $('#cho_dateil');
			menu.css("color", "#5A84F1");
			location.href = "/itda/views/person/personMyPage/personMoreinformation/hyj_personMoreinformation_detail.jsp";
		}
		function goResumeWriter() {
			var menu = $('#cho_resumewri');
			menu.css("color", "#5A84F1");
			location.href = "<%=request.getContextPath()%>/beforeinsert.re";
		}
		function goResu() {
			var menu = $('#cho_resume');
			menu.css("color", "#5A84F1");
			location.href="<%=request.getContextPath()%>/resumeList.re";
		}
		function goContestApp() {
			var menu = $('#cho_contest');
			menu.css("color", "#5A84F1");
			// location.href="";
		}
		function goRecruit() {
			var menu = $('#cho_recruit');
			menu.css("color", "#5A84F1");
			// location.href="";
		}
		function goProposed() {
			var menu = $('#cho_proposed');
			menu.css("color", "#5A84F1");
			// location.href="";
		}
		function goPortfoli() {
			var menu = $('#cho_portfolio');
			menu.css("color", "#5A84F1");
			location.href = "<%=request.getContextPath()%>/portfolioMain";
		}
		function goWrited() {
			var menu = $('#cho_writed');
			menu.css("color", "#5A84F1");
			 location.href="/itda/views/person/personMyPage/personMyPageWrite/sb_personMyWrite.jsp";
		}
		function goBookMark() {
			var menu = $('#cho_bookMark');
			menu.css("color", "#5A84F1");
			// location.href="";
		}

		function wjs_check() {
			$("#wjs_delete").removeAttr("disabled");
		}

		$("#wjs_em").click(function() {
			$("#wjs_phone").css("display", "none");
			$("#wjs_email").css("display", "block");
		})
		$("#wjs_ph").click(function() {
			$("#wjs_email").css("display", "none");
			$("#wjs_phone").css("display", "block");
		})
		$("#wjs_delete").click(function(){
			location.href="<%=request.getContextPath()%>/deleteMember.per";
		});
	</script>
</body>
</html>