<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="com.kh.itda.member.model.vo.hyj_member"%>

<!DOCTYPE html>
<html lang="ko">

<head>
    <meta charset="UTF-8">
    <title>Document</title>

    <style>
        #phone{
         width:69%;
         height:100%;
       
         text-align: left;
         float:left;
         color: gray;
        
        }
        #cho_middleMenubar{
        margin-left: auto;
            margin-right: auto;
           width:1400px;
           height:220px;
           float:center;
       }
       .cho_h3{
           text-align: center;
           font-weight: bold;
           font-family: 'Noto Sans KR', sans-serif;
           color: #7a7a7a;
       }
       .cho_div{
           background-color: white;
       }
       .cho_menubar{
         border-top:solid #d8d8d8;
         border-bottom:solid #d8d8d8;
       }
       .cho_menu{
          
           height: 95%;
           width:14%;
           float:left;
       }
         div{
            text-align: center;
            color:black;
        }
        header{
            margin-left: auto;
            margin-right: auto;
            width:1400px;
            height:100px;
        }
        #contents{
            margin-left: auto;
            margin-right: auto;
            width:1400px;
            height:1400px;
            background-color: rgb(255, 255, 255);
        }

        section{
            width:80%;
            height:100%;
            background-color: rgb(255, 255, 255);
            float:left;
        }


        #footerArea{
        margin-left: auto;
            margin-right: auto;
            width:1400px;
            height:150px;  
       }
        #hyj_mypagemenu{
            font-weight: bold;
            color:rgb(0, 0, 0); 
            width:95%;
            height:50%;
            margin-left:auto; 
            margin-right:auto;
            margin-top:15px; 
            font-size: 16px; 
            background:rgb(255, 255, 255); 
            border: 1px solid black;
        }
        #hyj_mypagemenuback{
            width:14.287%;
            height:100%;
            float:left;
            background:rgb(255, 255, 255);
        }
        #hyj_mydetails{
            width:45%; 
            height:850px;
           
            display : inline-block; 
            margin-top:3%;
        }
        #hyj_phone, #hyj_job, #hyj_career, #hyj_money, #hyj_skil, #hyj_certificate, #hyj_project, #hyj_Storage, #hyj_file, #hyj_Education{
            width:100%;
            height:9.78%;
           
            border: 2px solid rgb(133, 133, 133);
            border-radius: 5px;
        }
        #hyj_textarea{
            margin-left: 10px;
            margin-top: 10px;
            text-align: left;
            font-size: 20px;
            font-weight: bold;
            width:96%;
            height:30%;
            color: black;
            background: skyblue;
            border: 2px solid rgb(214, 214, 214);
        }
        #hyj_textareainput1, #hyj_textareainput2, #hyj_textareainput3, #hyj_textareainput4,
        #hyj_textareainput5, #hyj_textareainput6, #hyj_textareainput7, #hyj_textareainput8,
        #hyj_textareainput9{
            margin-left: 20px; 
            margin-top: 10px; 
            text-align: left; 
            font-size: 18px; 
            width:96%;
            height:45%;
            color:gray;
        }
        #hyj_btn{
            margin-top:18px;
            width:20%;
            height:40%;
            color:white;
            background:rgb(50, 46, 255); 
            border: 2px solid rgb(165, 165, 165);
            display: inline-block; 
            font-size: 20px; 
            font-weight: bold;
        }

        
    </style>
</head>
<body>
  
<%@include file="/views/common/menubar.jsp" %>



    <div id="contents" class="test1"><!-- 전체 화면 div 시작 -->

        
        <section id="section" class="test1">
            <div id="cho_middleMenubar" >
                <div class="cho_div" style="height:150px; background:rgb(255, 255, 255);"><img id="hyj_image" src="/itda/image/logo.PNG"></div>
                <div class="cho_menubar"style="height:60px;"><!-- 개인 마이페이지 상단 탭 영역 설정 -->
                    <div class="cho_white" style="width:15%; height:100%; float:left;"></div>
                    <div  class="cho_center"style="width:70%; height:100%; float:left; ">
                        <div class="cho_menu" id="menu1" onclick="goHome()" >
                               <h3 class="cho_h3"> 나의 HOME</h3>
                        </div>
                        <div class="cho_menu" id="menu2" onclick="goDetail()">         
                               <h3 class="cho_h3"> 나의 상세정보</h3>
                        </div>
                        <div class="cho_menu" id="menu3" onclick="goResume()" >
                             <h3 class="cho_h3">이력서관리</h3>
                        </div>
                        <div class="cho_menu" id="menu4" onclick="goVolunteer()">
                             <h3 class="cho_h3">지원 관리</h3>
                        </div>
                        <div class="cho_menu" id="menu5" onclick="goPortfolio()" >
                             <h3 class="cho_h3">포트폴리오</h3>
                        </div>
                        <div class="cho_menu" id="menu6" onclick="goMywriting()" >
                             <h3 class="cho_h3">내가쓴글</h3>
                        </div>
                        <div class="cho_menu" id="menu7" onclick="goFavorites()">
                            <h3 class="cho_h3">나의즐겨찾기</h3>
                        </div>
                    </div>
                 </div>
            <div style="height:85%; background:rgb(255, 255, 255);">
                <div style="width:60%; height:7%; background:rgb(255, 255, 255); display: inline-block;">
                    <p style="text-align: left; margin-top: 40px; font-size: 24px; font-weight: bold;">나의 상세정보</p></div>
                    <hr style="width:65%; border: 2px solid rgb(57, 70, 255);">
                    <div style="height:85%; ">
                        <div id="hyj_mydetails">
                        <table style="border:2px solid gray; width:100%; height:10%;">
                        	<tr>
                        		<td style="border:2px solid gray; font-weight: bold; background:skyblue; text-align: left; font-size:20px"   colspan="2">연락처</td>
                        	</tr>
                        	<tr>
                        		<td style="width:20%; text-align: left;">휴대폰번호 : </td>
                        		<td style="width:100%; text-align: left;"><%= loginUser.getPhone() %></td>
                        		
                        	</tr>
                        	<tr>
                        		<td style="width:20%; text-align: left;">이메일 : </td>
                        		<td style="width:100%; text-align: left;"><%= loginUser.getEmail() %></td>
                        	</tr> 
                        </table>
                        <table style="border:2px solid gray; width:100%; height:10%;">
                        	<tr>
                        	<td style="border:2px solid gray; font-weight: bold; background:skyblue; text-align: left; font-size:20px"   colspan="2">회사경력</td>
                        	</tr>
                        	<tr>
                        		<td style="width:20%; text-align: left;">회사경력 : </td>
                        		<td style="width:100%; text-align: left;"></td>
                        	</tr>
                        </table>
                       
                        <table style="border:2px solid gray; width:100%; height:10%;">
                        	<tr>
                        		<td style="border:2px solid gray; font-weight: bold; background:skyblue; text-align: left; font-size:20px"   colspan="2">교육경력</td>
                        	</tr>
                        	<tr>
                        		<td style="width:20%; text-align: left;">교육경력 : </td>
                        		<td style="width:100%; text-align: left;"></td>
                        	</tr>
                        </table>
                      
                            <table style="border:2px solid gray; width:100%; height:10%;">
                        	<tr>
                        		<td style="border:2px solid gray; font-weight: bold; background:skyblue; text-align: left; font-size:20px"   colspan="2">개발경력</td>
                        	</tr>
                        	<tr>
                        		<td style="width:20%; text-align: left;">개발경력 : </td>
                        		<td style="width:100%; text-align: left;"></td>
                        	</tr>
                        </table>
                        
                            <table style="border:2px solid gray; width:100%; height:10%;">
                        	<tr>
                        		<td style="border:2px solid gray; font-weight: bold; background:skyblue; text-align: left; font-size:20px"   colspan="2">봉사활동</td>
                        	</tr>
                        	<tr>
                        		<td style="width:20%; text-align: left;">봉사활동 : </td>
                        		<td style="width:100%; text-align: left;"></td>
                        	</tr>
                        </table>
                        
                            <table style="border:2px solid gray; width:100%; height:10%;">
                        	<tr>
                        		<td style="border:2px solid gray; font-weight: bold; background:skyblue; text-align: left; font-size:20px"   colspan="2">주요기술</td>
                        	</tr>
                        	<tr>
                        		<td style="width:20%; text-align: left;">주요기술 : </td>
                        		<td style="width:100%; text-align: left;"></td>
                        	</tr>
                        </table>
                                               
                            <table style="border:2px solid gray; width:100%; height:10%;">
                        	<tr>
                        		<td style="border:2px solid gray; font-weight: bold; background:skyblue; text-align: left; font-size:20px"   colspan="2">수상경력</td>
                        	</tr>
                        	<tr>
                        		<td style="width:20%; text-align: left;">수상경력 : </td>
                        		<td style="width:100%; text-align: left;"></td>
                        	</tr>
                        </table>
                                              
                            <table style="border:2px solid gray; width:100%; height:10%;">
                        	<tr>
                        		<td style="border:2px solid gray; font-weight: bold; background:skyblue; text-align: left; font-size:20px"   colspan="2">자격증</td>
                        	</tr>
                        	<tr>
                        		<td style="width:20%; text-align: left;">자격증 : </td>
                        		<td style="width:100%; text-align: left;"></td>
                        	</tr>
                        </table>
                                                
                            <table style="border:2px solid gray; width:100%; height:10%;">
                        	<tr>
                        		<td style="border:2px solid gray; font-weight: bold; background:skyblue; text-align: left; font-size:20px"   colspan="2">학력</td>
                        	</tr>
                        	<tr>
                        		<td style="width:20%; text-align: left;">학력 : </td>
                        		<td style="width:100%; text-align: left;"></td>
                        	</tr>
                        </table>
                        

                        </div> 
                        <br>
                        <button id="hyj_btn" onclick="hyj_test1();">상세정보 수정하기</button>
                    </div>
                    
            </div>
        </section>


    </div><!-- 전체 화면 div 끝 -->

   <%@ include file="/views/common/cho_footer.jsp" %>

	<script>
		function hyj_test1(){
			location.href="<%=request.getContextPath()%>/views/person/personMyPage/personMoreinformation/hyj_personMoreinformation_detail.jsp";
		}
	
	</script>




</body>
</html>