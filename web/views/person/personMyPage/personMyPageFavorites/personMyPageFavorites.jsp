<%@page import="com.kh.itda.recruitAndContest.model.vo.Announcement"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
	ArrayList<Announcement> list = (ArrayList) request.getAttribute("list");
%>
<%
	ArrayList<Announcement> list2 = (ArrayList) request.getAttribute("list2");
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <style>
       
       
        .wjs_mainTable{
            margin-left: auto;
            margin-right: auto;
            width: 1400px;
            height:1000px;
        }
     
        
        
        .wjs_tablefavorites{
            width: 840px;
          	vertical-align: top;
            
        }
        
        .wjs_div1{
            border-top: 6px solid #5A84F1;
            height: 50px;
            vertical-align: top;
        }
        
        .wjs_div2{
            border-top: 6px solid #5A84F1;
            height: 10px;
            vertical-align: top;
        }
        
        

  
       #wjs_table2,#wjs_table3,#wjs_table1{
       		width: 900px;
       		align-content: center;
       		
       }
       #wjs_name{
       		width: 200px;
       		text-align: center;
       }
        #wjs_oner{
       		width: 300px;
       		text-align: center;
       }
        #wjs_favorites{
       		width: 400px;
       		border: 1px solid black;
       		text-align: center;
       		
       }
       
       #wjs_sort{
       		float: right;
       	 background-color: rgba(0,0,0,0);
       	 padding: 5px;
       	 margin-bottom: 6px;


       }
       #sort1{
       	 margin-right:-4px;
       	 border: 0;
       	 outline: 0;
       	 background: white;
       }
       #sort2{
       	  margin-left:-3px;
       	   border: 0;
       	 outline: 0;
       	 background: white;
       }
       #wjs_time{
       		width: 300px;
       		text-align: center;
       		height: 40px;
       }
       #wjs_result{
       		width: 300px;
       		text-align: center;
       }
       #wjs_status{
       		width: 100px;
       		
       		text-align: center;
       }
       #wjs_time0{
       		width: 200px;
       		
       		text-align: center;
       }
        #wjs_time1{
       		width: 200px;
       		
       		text-align: center;
       }
       #wjs_result1{
       		width: 200px;
       		
       		text-align: center;
       }
       #wjs_trr{
       		border: 1px solid #E6E6E6;
       }
       .wjs_td{
       		font-weight: bold;
       }
      
    </style>
</head>
<body>

<%@include file="/views/common/menubar.jsp" %>
<%@include file="/views/person/personMyPage/cho_personMyPageMenubar.jsp" %>




    <table class="wjs_mainTable">
        <tr class="portfolioMainTable_tr">
            <td class="wjs_nav" style="width:280px; vertical-align:top">
                <!-- nav 개인프로필 영역이랑 사이드 메뉴바인클루드 하기 -->
              	<%@include file="/views/person/person_common/cho_sideMenubarAndUserInfo.jsp" %>
            </td>
            <td class="wjs_tablefavorites">
                <h1 style="font-weight: bold;">나의 즐겨찾기</h1>
             
                
                
                <br><br>
                
                
                <h4 style="font-weight: bold;">채용공고 즐겨찾기</h4>
                <div class="wjs_div2"></div>
                <div id="wjs_sort">
                	<button id="sort1">최신순</button>
                	<button id="sort2">등록순</button>	
                </div>
                <table id="wjs_table2">
                	<tr>
                		<td id="wjs_name" class="wjs_td">공고 제목</td>
                		<td id="wjs_time" class="wjs_td">접수기간</td>
                		<td id="wjs_result" class="wjs_td">결과 발표기간</td>
                		<td id="wjs_status" class="wjs_td">상태</td>
                	</tr>
                	<% for(Announcement a :list){ %>
						<tr id="wjs_trr">
							<input type="hidden" value="<%= a.getaNo() %>">
							<td id="wjs_name" class="wjs_td"><%= a.getaTitle() %></td>
							<td  id="wjs_time" class="wjs_td"><%= a.getaReceiptStart() %></td>
							<td id="wjs_result" class="wjs_td"><%= a.getaReceiptEnd() %></td>
							<td id="wjs_status" class="wjs_td"><%= a.getStatus() %></td>
						</tr>
						<% } %>
                </table>
                
               
                <br><br> 
                
               <h4 style="font-weight: bold;">공모전공고 즐겨찾기</h4>
               <div class="wjs_div2"></div>
                <div id="wjs_sort">
                	<button id="sort1">최신순</button>
                	<button id="sort2">등록순</button>	
                </div>
                <table id="wjs_table3">
                	<tr>
                		<td id="wjs_name" class="wjs_td">공고 제목</td>
                		<td id="wjs_time0" class="wjs_td">접수기간</td>
                		<td id="wjs_time1" class="wjs_td">공고기간</td>
                		<td id="wjs_status" class="wjs_td">상태</td>
                	</tr>
                	<% for(Announcement a :list2){ %>
						<tr id="wjs_trr">
							<input type="hidden" value="<%= a.getaNo() %>">
							<td id="wjs_name" class="wjs_td"><%= a.getaTitle() %></td>
							<td  id="wjs_time" class="wjs_td"><%= a.getaReceiptStart() %></td>
							<td id="wjs_result" class="wjs_td"><%= a.getaReceiptEnd() %></td>
							<td id="wjs_status" class="wjs_td"><%= a.getStatus() %></td>
						</tr>
						<% } %>
                </table>
             
             
		
                <br><br><br><br><br><br>

			 </td>
             
			 <td class="wjs_aside" style="width:280px;">
            
            </td>
	    </tr>
    </table>








<%@include file="/views/common/cho_footer.jsp" %>

</body>
<script>
	$("#wjs_table2 td").click(function(){
	 var num = $(this).parent().children("input").val();
	 console.log(num);
	location.href= "<%=request.getContextPath() %>/selectAn.me?num="+num;
});

</script>


</html>