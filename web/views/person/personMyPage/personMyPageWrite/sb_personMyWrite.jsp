<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<style>
	.abc{
            margin-left: auto;
            margin-right: auto;
            width: 1400px;
            height:1000px;
        }
         .container{
           width: 840px;
           height: 800px;
           /* margin-top:60px; */
       }
       .faqTable {
       		border: 1px solid black;
       }
       .communityTable {
       		border: 1px solid black;
       }
       .commentTable {
       		border: 1px solid black;
       }
       .hr1 {
        	width: 830px;
        	background-color: #5A84F1;
        	height: 5px;
        	margin-right: 320px
       }
</style> 
</head>
<body>
<%@include file="/views/common/menubar.jsp" %>
<%@include file="/views/person/personMyPage/cho_personMyPageMenubar.jsp" %>
	<table class="abc">
		<tr>
			<td class="portfolioMainTable_tr_td_nav" style="width:280px; vertical-align:top">
				<%@include file="/views/person/person_common/cho_sideMenubarAndUserInfo.jsp" %>
			</td>
			<td><label style="text-align: right; margin-left:20px; font-size: 30px; font-weight: bold; ">내가쓴글</label><hr class="hr1"><div class="container">
                        <ul class="nav nav-tabs">
                          <li class="active1"><a href="#">FAQ</a></li>
                          <li class="active2"><a href="#">개발자커뮤니티 게시글</a></li>
                          <li class="active3"><a href="#">댓글</a></li>
                        </ul>
                        <div class="faqBoard">
                        	<table class="faqTable">
                        		<tr>
                        			<th width="300px" style="text-align: center">질문내용</th>
                        			<th width="300px" style="text-align: center">작성날짜</th>
                        			<th width="300px" style="text-align: center">답변</th>
                        		</tr>
                        		<%-- <% for(Notice n : list) { %>
                        		<tr>
                        			<td><%= n.getNno() %></td>
                        			<td><%= n.getnTitle() %></td>
                        			<td><%= n.getnCount() %></td>
                        		</tr>
                        		<% }  %> --%>
                        	</table>
                        </div>
                        <div class="communityBoard">
                        	<table class="communityTable">
                        		<tr>
                        			<th width="300px" style="text-align: center">게시글제목</th>
                        			<th width="300px" style="text-align: center">작성날짜</th>
                        			<th width="300px" style="text-align: center">좋아요</th>
                        			<th width="300px" style="text-align: center">댓글</th>
                        			<th width="300px" style="text-align: center">조회수</th>
                        		</tr>
                        	</table>
                        </div>
                        <div class="comment">
                        	<table class="commentTable">
                        		<tr>
                        			<td width="300px" style="text-align: center">게시글 제목</td>
                        			<td width="300px" style="text-align: center">작성날짜</td>
                        			<td width="300px" style="text-align: center">좋아요</td>
                        			<td width="300px" style="text-align: center">댓글날짜</td>
                        		</tr>
                        	</table>
                        </div>
                        
                      </div><hr>
                      </td>
			
		</tr>
	</table>
	<script>
	$(function() {
		$(".communityBoard").hide();
	    $(".comment").hide();
	    $(".faqBoard").show();
		$(".active1").click(function() {
	     $(".communityBoard").hide();
	     $(".comment").hide();
	     $(".faqBoard").show();
		 });
		$(".active2").click(function() {
			$(".communityBoard").show();
	    	$(".faqBoard").hide();
	    	$(".comment").hide();
		});
		$(".active3").click(function() {
			$(".comment").show();
	    	$(".faqBoard").hide();
	    	$(".communityBoard").hide();
		});
	});
	</script>
</body>
</html>