<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<style>
.resumeViewsTable {
	margin-left: auto;
	margin-right: auto;
	width: 1400px;
	
	height: 1500px;
}



.resumeViewTable {
	width: 840px;
	vertical-align: top;
}

.resumeViews_section_div {
	border-top: 6px solid #5A84F1;
	height: 150px;
}

.resumeUpdate_botton {
	float: right;
	width: 210px;
	height: 60px;
	border-radius: 0.5pc;
	background-color: #5A84F1;
	color: white;
	font-size: 15pt;
	margin-bottom: 10px;
}

.resumeUpdate_botton1 {
	float: right;
	width: 210px;
	height: 60px;
	border-radius: 0.5pc;
	color: #5A84F1;
	background-color: white;
	font-size: 15pt;
	margin-bottom: 10px;
}

.container {
	width: 840px;
}

.resumeViews_section {
	
}

#resumeUpdate_Date {
	width: 210px;
	height: 40px;
	border: 1px solid #7A7A7A;
	margin-left: 30px;
	margin-right: 80px;
	margin-bottom: 10px;
	margin-top: 20px;
}

#resumeViewsUpdate {
	margin-left: 30px;
	margin-right: 80px;
}

.resumeViews_section {
	margin-left: auto;
	margin-right: auto;
	margin-top: 20px;
	width: 700px;
	border: 2px solid #CCCCCC;
}

.th_section {
	background-color: #F6F6F6;
}
</style>
</head>
<body>
<%@include file="/views/common/menubar.jsp" %>
<%@include file="/views/person/personMyPage/cho_personMyPageMenubar.jsp" %>
	<table class="resumeViewsTable">
		<tr class="portfolioMainTable_tr">
			<td class="portfolioMainTable_tr_td_nav" style="width: 280px; vertical-align: top;">
				<!-- nav 개인프로필 영역이랑 사이드 메뉴바인클루드 하기 -->
				<%@include file="/views/person/person_common/cho_sideMenubarAndUserInfo.jsp" %>
			</td>
			<td class="resumeViewTable">
				<h2 style="font-weight: bold;">이력서 관리</h2>
				<h3 style="font-weight: bold;">이력서 제목자리</h3>
				<div class="resumeViews_section_div">

					<div class="resumeViews_section">
						<table style="width: 95%; margin-left: auto; margin-right: auto;">
							<tr>
								<td>
									<!-- 이름 -->
									<h3>정용훈</h3>
								</td>
								<td>
									<!-- 생년영역 -->
									<h3 style="color: #7A7A7A;">1991년생</h3>
								</td>
							</tr>
							<tr>
								<td>
									<p style="font-size: 18px;">휴대폰번호:010-0000-0000</p>
								</td>
								<td>
									<p style="font-size: 18px;">이메일:asdf@asdf.com</p>
								</td>
							</tr>
							<tr>
								<td colspan="2" style="margin-bottom: 20px; padding-top: 40px;">
									<table
										style="width: 98%; margin-left: auto; margin-right: auto; border: 1px solid #7a7a7a;">
										<tr class="th_section">
											<th>직군</th>
											<th>주요기술</th>
											<th>경력사항</th>
											<th>희망연봉</th>
											<th>학력</th>
										</tr>
										<tr>
											<td>
												<!-- 직군영역 -->
											</td>
											<td>
												<!-- 주요기술 -->
											</td>
											<td>
												<!-- 경력사항 -->
											</td>
											<td>
												<!-- 희망연봉 -->
											</td>
											<td>
												<!-- 학력 -->
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td colspan="2"><h3>개발 경력</h3></td>
							</tr>
							<tr>
								<td colspan="2">
									<table
										style="width: 98%; margin-left: auto; margin-right: auto; border: 1px solid #7a7a7a;">
										<tr class="th_section">
											<th>개발 경력명</th>
											<th>담당 업무</th>
											<th>개발 기간</th>
											<th>사용 스택</th>
										</tr>
										<!-- for문으로 있는 만큼 출력 -->
										<tr>
											<td>
												<!-- 개발 경력명 -->
											</td>
											<td>
												<!-- 담당업무 -->
											</td>
											<td>
												<!-- 개발 기간 -->
											</td>
											<td>
												<!-- 사용 스택 -->
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td colspan="2"><h3>회사 경력</h3></td>
							</tr>
							<tr>
								<td colspan="2">
									<table
										style="width: 98%; margin-left: auto; margin-right: auto; border: 1px solid #7a7a7a;">
										<tr class="th_section">
											<th>회사명</th>
											<th>직군</th>
											<th>근무 기간</th>
											<th>사용 스택</th>
											<th>연봉</th>
										</tr>
										<tr>
											<td>
												<!-- 회사명 -->
											</td>
											<td>
												<!-- 직군 -->
											</td>
											<td>
												<!-- 근무 기간 -->
											</td>
											<td>
												<!-- 사용 스택 -->
											</td>
											<td>
												<!-- 연봉 -->
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td colspan="2"><h3>개인 프로젝트</h3></td>
							</tr>
							<tr>
								<td colspan="2">
									<table
										style="width: 98%; margin-left: auto; margin-right: auto; border: 1px solid #7a7a7a;">
										<tr class="th_section">
											<th>프로젝트 제목</th>
											<th>제작 년도</th>
											<th>사용 스택</th>
											<th>상세 설명</th>
										</tr>
										<tr>
											<td>
												<!-- 프로젝트 제목 -->
											</td>
											<td>
												<!-- 제작 년도 -->
											</td>
											<td>
												<!-- 사용 스택 -->
											</td>
											<td>
												<!-- 상세 설명 -->
											</td>
										</tr>

									</table>
								</td>
							</tr>
							<tr>
								<td colspan="2"><h3>저장소</h3></td>
							</tr>
							<tr>
								<td colspan="2">
									<table
										style="width: 98%; margin-left: auto; margin-right: auto; border: 1px solid #7a7a7a;">
										<tr class="th_section">
											<th>링크</th>
											<td><input type="text" style="width: 90%;"></td>
										</tr>

									</table>
								</td>
							</tr>
							<tr>
								<td colspan="2"><h3>학력</h3></td>
							</tr>
							<tr>
								<td colspan="2">
									<table
										style="width: 98%; margin-left: auto; margin-right: auto; border: 1px solid #7a7a7a;">
										<tr class="th_section">
											<th>학교/기관명</th>
											<th>입학일</th>
											<th>졸업일</th>
											<th>전공/학위</th>
											<th>내학점/최대학점</th>
											<th>기타</th>

										</tr>
										<tr>
											<td>
												<!-- 학교/기관 -->
											</td>
											<td>
												<!-- 입학일 -->
											</td>
											<td>
												<!-- 졸업일 -->
											</td>
											<td>
												<!-- 전공/학위 -->
											</td>
											<td>
												<!-- 내학점/최대학점 -->
											</td>
											<td>
												<!-- 기타 -->
											</td>
										</tr>
									</table>
							<tr>
								<td colspan="2"><h3>첨부파일</h3></td>
							</tr>
							<tr>
								<td colspan="2">
									<table
										style="width: 98%; margin-left: auto; margin-right: auto; border: 1px solid #7a7a7a;">
										<tr class="th_section">
											<th>파일</th>
											<td><input type="text" style="width: 90%;"></td>
										</tr>

									</table>
								</td>
							</tr>
							<tr>
								<td colspan="2"><h3>희망연봉</h3></td>
							</tr>
							<tr>
								<td colspan="2">
									<table
										style="width: 98%; margin-left: auto; margin-right: auto; border: 1px solid #7a7a7a;">
										<tr class="th_section">
											<th>금액</th>
											<td><input type="text" style="width: 90%;"></td>
										</tr>

									</table>
								</td>
							</tr>
							<tr>
								<td colspan="2"><h3>포트폴리오</h3></td>
							</tr>
							<tr>
								<td colspan="2">
									<table
										style="width: 98%; margin-left: auto; margin-right: auto; border: 1px solid #7a7a7a;">
										<tr class="th_section">
											<th>링크</th>
											<td><input type="text" style="width: 90%;"></td>
										</tr>
									</table>

								</td>
							</tr>
							<tr>
								<td colspan="2"><h3>활동</h3></td>
							</tr>
							<tr>
								<td colspan="2">
									<table
										style="width: 98%; margin-left: auto; margin-right: auto; border: 1px solid #7a7a7a;">
										<tr class="th_section">
											<th>활동명</th>
											<th>시작일</th>
											<th>종료일</th>
											<th>링크</th>
											<th>설명</th>
										</tr>
										<tr>
											<td>
												<!-- 활동명 -->
											</td>
											<td>
												<!-- 시작일 -->
											</td>
											<td>
												<!-- 종료일 -->
											</td>
											<td>
												<!-- 링크 -->
											</td>
											<td>
												<!-- 설명 -->
											</td>

										</tr>
									</table>
							<tr>
						</table>
					</div>
				</div>



			</td>


			<td class="portfolioMainTable_tr_td_aside"
				style="width: 280px; vertical-align: top;">
				<div class="resumeViews_section_div" style="margin-top: 107px;">
					<div id="resumeUpdate_Date">
						<p>최근 수정일:</p>
					</div>
					<button class="resumeUpdate_botton" id="resumeViewsUpdate">이력서
						수정하기</button>
					<button class="resumeUpdate_botton1" id="resumeViewsUpdate">인쇄하기</button>
					<button class="resumeUpdate_botton1" id="resumeViewsUpdate">PDF로
						저장하기</button>
				</div>
			</td>


		</tr>



	</table>

<%@include file="/views/common/cho_footer.jsp" %>
</body>
</html>