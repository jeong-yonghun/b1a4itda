
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%
    /* 수상격력 등등 추가 사항 부분에 for문 <%% 이걸 쓰고  추가 버튼 눌렀을 떄 생성되게 하기*/
    
    %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <style>
       
        .resumeWriMainTable{
            margin-left: auto;
            margin-right: auto;
            width: 1400px;
            height:1500px;
        }
        .resumeWriMainTable_tr_td_section{
            width: 840px;
        }
        .resumeWriMainTable_tr_td_aside{
            width:280px;
            vertical-align:top ;
            position: fixed;
        }
        
        .resumeWriMainTable_tr_td_section_div1{
            border-top: 6px solid #5A84F1;
            height: 150px;
        }
        .resumeWriMainTable_tr_td_section_div2{
            border: 6px solid #5A84F1;
            height: 550px;
        }
        .resumeWriMainTable_tr_td_section_botton{
            float: right;
            width: 210px;
            height: 60px;
            border-radius: 0.5pc;
            background-color: #5A84F1;
            color: white;
            font-size: 15pt;
        }
       .resumeWricontainer{
           width: 840px;
       }
       #resumeTitle{
           width: 600px;
           height: 50px;
           
           border: 1px solid #C0BCBC ;
       }
       #cho_publicButton{
          border-radius: 0%;
          border: 1px solid #C0BCBC;
       }
       #cho_privateButton{
          border-radius: 0%;
          border: 1px solid #C0BCBC;
       }
       .btn-lg{
           border: solid 1px #C0BCBC ;
           width: 50px;
          
           
       }

       .cho_resumeAsideMenubarTable_thead_title{
            font-size: 18px;
         font-weight: bold;
       }
       .asideBtn{
           border: solid 1px #5A84F1;
           border-radius: 8px;
           background-color: white;
           height: 20px;
           
       }
       #cho_resumeSave{
           background-color: #5A84F1;
           color: white;
           width: 40px;
           height: 30px;
       }
       #cho_resumeAsideMenubarTable{
           border: 1px solid #F6F6F6;
           margin: 30px;
           background-color: #F6F6F6;
           width: 150px;
           height: 300px;
		}
		#hyj_btn1, #hyj_btn2{
		margin-top:50px;
		width: 150px;
		height: 70px;
		border: 2px solid rgb(137, 104, 255);
		font-size: 18px;
        font-weight: bold;
        background:white;
		}
		.hyj_imagecontainer{
            overflow: hidden;
            display: flex;
            align-items: center;
            justify-content: center;
            height:100px;
			width: auto;
			height: auto;
    		max-width: 150px;
    		max-height: 85px;
    		border:1px solid black;
    		
    		
		}

    </style>
</head>
<body>
<%@include file="/views/common/menubar.jsp" %>
<%@include file="/views/person/personMyPage/cho_personMyPageMenubar.jsp" %>
    <table class="resumeWriMainTable">
        
        <tr class="resumeWriMainTable_tr">
            <td class="resumeWriMainTable_tr_td_nav" style="width:280px; vertical-align:top;">
                <!-- nav 개인프로필 영역이랑 사이드 메뉴바인클루드 하기 -->
                <%@include file="/views/person/person_common/cho_sideMenubarAndUserInfo.jsp" %>
            </td>
            <td class="resumeWriMainTable_tr_td_section" style="vertical-align:top;">
            	<table style="margin-top:30px;width:70%; height:10%; background:gray;  border-radius: 20px; ">
            		<tr>
            			<td style="font-size:100px; text-align: center;">0</td>
            			<td style="font-size:100px; text-align: center;">0</td>
            			<td style="font-size:100px; text-align: center;">0</td>
            			<td style="font-size:100px; text-align: center;">0</td>
            			<td style="font-size:100px; text-align: center;">0</td>
            		</tr>
            		<tr>
            			<td style="text-align: center;">나의 이력서</td>
            			<td style="text-align: center;">나의 공모전</td>
            			<td style="text-align: center;">나의 입사지원</td>
            			<td style="text-align: center;">포트폴리오</td>
            			<td style="text-align: center;">즐겨찾기</td>
            		</tr>
            	</table>
            	<table>
            		<tr>
            			<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            			<td><button id="hyj_btn1">이력서 작성</button></td>
            			<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            			<td><button id="hyj_btn2">포트폴리오 작성</button></td>
            		</tr>
            	</table>
            	<table style="width: 60%; height:20px;  margin-top:50px;">
            		<tr>
            			<td style="width: 150px; height:20px; border:2.5px solid rgb(137, 104, 255); font-size:24px; color:rgb(137, 104, 255); font-weight: bold; text-align: center;">맞춤채용</td>
            			<td style="border-bottom:2.5px solid rgb(137, 104, 255);"></td>
            		</tr>
            	</table>
            	<table style="margin-top: 50px;">
            		<tr>
            			<td>
            			<img class="hyj_imagecontainer" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAUEAAACdCAMAAAAdWzrjAAABVlBMVEX///8APXYBRYwAOXAASIwASJAARYgAPHP///0AOHEAPnsBS5T8//8BQX4BRosAQ4LB1uoCN3MAOWcAH10BS5YALm+91OKywtT///oAN3v2/////P/k8viXssw7YZEAQozD1d6nvc0ALWjw//8AJ1cAAEgAPX0ANnsAO30ALmsAJmUAKF7j6vQFN2kAADUAMFsAAD9pjLEAHFP//PIAAEsAEkoAADnCzNwATo8ASpzR3ueVsM4AACsAF0ve7vOHoLZ8jaVXbYaInrnc9f6fxdouZ58qUYMATqAWUo5KcpvP5fEiUH4APIw6Y49tjq1WfaEALHpOZ5Y5ZYaIr89riq1mfpUnRGcONFUyTWmWpbWxtsSQmqdXcYBSaHXa3uaEn6uFl7gAGERkip+ludcAF1sAJUIlQGIADlimxc9wiZM1UGpheJYVQGhYb5ZFYnj//ueBpLUhTmigQPt0AAAP80lEQVR4nO2d/1vayBbGh+8DHaWgxXESCxhEKyAWVJRdCWC3d6vVvetWLLW31a61rdq9vf//L/ecBKxIgKitNXXe1seQzBwzn5yZcxKSDCFSUlJSUlJSUlJSUlJSUlJSUlJSUlJSUlJSUlJSUlJSt1LUkPHbboX+hZlp81I7wdqGHSiFcmyuohBmUwZBbtVaThVCiVmG2jVnSJiH5cZb/w1ktFQhHDBybq8KE+gxzGILGgBL3NDldgPKM2cS5FSIS+45LRbBA3sQZMDiKiDwSDqTIFUYLxbFWuz+/fv4Y0uxskqKFs2FVcCBaTHbls4saopTCSpIUDxZTsfjcfew247iK/7w7/d7NJcysf5b2h2P+23Zaiv9Qlw2+NwWcTj05Mm413tvdPSePY36lkdXwjFi+FxHqxmMZ2ztqd8HtpZtWjP1r6G1q/X+HypjxMKgGnOvjF5GPi9SXMdeR1sBw4zA+DH70H0pYy25VcocR9BI6hinMb/vntmMTlfrtcpcF/99nSqiM54onKsP3d6Omj2ZdTqhW+XCYQTReyAXZKT8dGX0cl0OBMxXnqroeGf2jHFMexb3XdqYcwlCFs3J+u++0Xteu2PgOYTLz39bU9ppHyMtgG7wuisAHI07jyCeAShMVDZW/uX1jvoMdTLqXnXv/Kqwd+U3tZ03I0EmtD+ejy57Vzpq+nrJ+QQxiABA/+io1+vtaJfPN/qV4Pmmfi0DRYBV/JnasoVRRWgvplfuwdoLxvoQPFtyFsHWjuKp3NqmH1txjqDPos3WGEZ9Ye/zZxpnisIFxlFtaxoKer/W6k2vW3GMxY5BSMwIQKm6HR+9RCu75PXG/9AEGBJMoWxr5hqWZhxG0LyipT2bWR6+FkCv9/ljUQQ35Ar763k4fFcIMoOfAgDvea8uJDjsXXm+pXGqUPLX9LLXd3VrziNImHgc9w2PXqPRoGGoPr0PAyqZGB/2+q5ubNhxBCHzeJz2+YZ9w9cD6FteHp3eF+zJtHd55RpHw2EEjdxNHw9DIuP3X4dgazQc/2tsOuBf8fmvcTicRRAA0vVx87LS8HlZXHbqveX89a7OC2N2qlyw7B8Hgj+ai30hwcT0NyRoWWVQnU6CM3eWYO86NkG3CEofvC5B5/qgWxK8gm6jDzqqFzNIp4HgcLfc/vCwe9gPS36/31wFC37vsH8oDE0OG+u8fouaVsbAkNvXGim/0jpb6jhw3oADCfq75fYPQcPDFvIOx+P+If+wRaVeGvaH/cCp9eHscFgrHlSJY7LBvgTB1fzheLeex9OA8RL8kKAfjkZofHp8enp6fAYFS+OmzhZaH/79p3DQTR/9CEK7IxsPLbS5vfl02h8OXwqi2x/cmrAn1Unf1PUl6I88EdbVhFaPhC+F0O2OfLQ5uDFe5D/HOOhPbzGru64457D6GXTlS4yF8ZlNYfMGJAAIkcQpbtiXYCRJi7xL2DT49dfMJQhCWJ95TIr29klRFDxM37Xd306M9yII0XY82ccTfhmHeOp22yM45A4DQae41aVkEhzv8pkwEpzuSzDS8lRJ0MoHMUrYIGi7G//kBEli2iJvHkRwwiDot0q5uyUJWuiXSBg7uyR4DR+0B++nJ0iv44NDads+GBp/fIPNukH1JJg2CfYWEgzZ7sXDobvng+HBvdg+QZd/6Ocm+CA81KWwK/ygL8FH4fTQULq7opWAYPonJkgTDywaPZBgxChlm2DkzhEc+pYEA36XJHhRE0F78M4Ivrh7BNMP+sViiCT2CbriASD4U+paBO0jlAQtNDEZthmIJUFr/QLZTEgSbBMMDYUuKjzIBx+l012V2hq6YPAnJwgZtQWEtD2Cpo/1RNlBkLbu2R4gZz3gzvBZsD4E+2QzkwDGNQhd2yfToeALZosfxQfsrR5YvqXqTTD0LQmCwAe5+V3foH1SlMu/3eLH6YYIukJpIFgk1BYaBR/yc8x3dTfTi12udHoTn3m3866PYpGiG94ghevopnwQCk7GbA6E3FFPuH9XgueyHVcwFPFUx+xJdc737W2Cj9rtDLWRAJtAaMA5CRRyhVxnlayWWsvG/5Ar8sCWRv7UnDMMtghOuroVCrgeDSQYsKh4bYUmVfOFP45QP4KhgQQDgZAkKAleU7eRoEsSlATtEcwgwe8BMDTrJIKEcoUkZgPdCnkGEXQhwfNVopHxQGQ6FAh4PMGQhUmbussEXTNRz3gucGG1JGgtS4LpaCBSz3juIEGPoQD2v/ZSyBOcTVq+m9EUEAx6PC1UZpVAKBL1ZNYmUrlAsG3m68Yznd9gtcWRBIPdigwkGARMHVU8kUjUlakI/cAYCq+qO0WwswoQzKVns5zpmTtEkJ/rxR1y5ZBgb7V8sLNSMOCZzHLCqplgBDcFgxaWBymSUZ30TUlPgoHBBLvq5IKB3GSWUCJ2Mia8KxFMOenJxN4EPbnIQIK5zhrBHKxCgpRpiBC7uSTYU9iLcxertAlSJmqZq/XhO0Xwog+eI0iJVstjMJEEe6oPQfNtaOp26oqRxFkEKe0bSfp8T5IBYhcRAtPZrGmX07VtiMhXIBhEgo4JxjBgcRqbjUD+Z/wLtn4iwahBsHda1iLYLh4xUrnc6cyISZAwRtTGZBrDcS7qMTdH+vwE2yUinpTqoHei4yuUSWIk0q1gZGYAQaO5Fyp5xmcrZ0XYWsOMyB4L+70146xeTBhliUK0WwCn0J8gUvZ01IE144UzgnB0KruTBkIL+70FGbWTAEJDhfrycKRbhcWjSh+C5XwB1VGjMJJarGlnhoHDemP1YqmBOjxhTvrK3Xh7nhqzVLbfXBssa1UlUTp7qbJ5i4yasLbdR/cF3qN0M62/vnAKEdbrZrO+jlCkVq3kitJ+O79R23r+nAGinAnqnEsLvW6nGnSblcUMTdSYW6izEr9CUMX3mjomFktJSUlJSV1J3Jioz5jREE/rCMY/YT6GYKYoDM5rzXzC/IXTfsFWo965ONkKxszMXc5WF/G5ByOTKZ6VpefmISIUS7QWz2yZs9QZrx40dg2vG93KpKa8p7IiSzRJURHNV6SkcyVbTXCu0YROaLa+t7ffTGiUKlxtvv6sV6hSeqtAgpZMQsOJXoJGqnv1ul7fq1dVypKf/lNNmKn3G12HDXv6HstW38BnWqorvFLfe7u/X9/DIpROQD1dryd4Scfp2Ig+xv6u19/iujHyqkkVwtW3ZYXrX/T6/iucxy2xr+v79forZnsWwu+u8q8fGRO1gwon2mqT6HMaSSy91DgjzVWVlBfzwdzB/IkoKtnowubDqXyZjE2VobFHBwBh7dckUUhlcfJRajIzufhGO5k63c7MJZkClhOrmfzso3wuvxaDNWCwuiBI7EMq/yiXP9AYPubwdCSVSaUKX0h9FS8fKPl3opbKBGfzu6mq9vmIKIy8WfibiNxIZvLfC7kyF+8O8/lMPvX69hCkrKATUYkWnnAam4qR+gIQPBz5QsC/UkDw8G+hqdWFJBWv38cEKe9uC/V9k/DsaiHGyZPFdaJQpghW2xRC8FdTOiPq9vuKMUOHEOrRDqymibmP2D3fvgfHnooJRRjTYIEL/++lYPCR8b2UirPKRD/BeblIQhkYRE42cLKt8ockF7l3oijWj/6Eo5BaE4qmCeVHgzun2jElsYPTHUKbBZXUpwQQfJivGARpeXGMF4tq7jVRd3fgfI9sFSrk+ITQZDSqc7LVENBKDo2vbULbWe0Uz79i80kc+OBsVhy9wz9Rmh9Dj6sXWDExlThHcLsh8HYxTvWMCqOdyL3AE/OP8zGcLe9ZA9+cV1lNchb8hDOIVgtZUs3h3DuKcsUZLL+LmvOCVI/1vCCvtwnR5zVSnn+Sr0EvRh+cH4MxT2wfK9nDCaYwhl14PyrIyR9bx4RsA3eIQwzqHuPUTsfb4HIiW2jiSs65OPpEBBwS8EHoc/qiELGFBMaEIk5nqpCNhrELQDClQnAS0S286fzjUgLYkZOXGH0qq0A/UgVqbOdojVQjmlnDeoLVH6LyVIJE9fLSusjXoZVL0It/LU9MlUyCi2MYJl9vsPXFJE46V5qK8dJCWYlOlFazamrCiMnQpcAHi+BDJzhNmwoDQxGf+aICXBvnDTw8qYI2U4KUDk/ewmIFfA+89fg02ZxojhFSB4IQ4Xffge+R5HwCPJsBQQUHWRgHjz4nEqV3H/YprT54B9X3xO0ZBwkRu3oWula+WZmHwFoHjywvvWHQv5Bg4hCaVyS1DQbjEU6um5i/T7WpZHm+IubGEr9m2/lFbZsUhXZ6gh0sO6KbCQgVRy2C0dOj00Y0o5HSyGnjaXR3HfsxJ5u5FASSGhCczRY1pgSr2P2BICY7nxs4deibxSRjR5nF1YPMC4WT6mSjET1tiFvUiymp1ZJ5lexsJ9EPjHFwvkzW55oT5jhIWFHUNkjlQ5MoRdgWo2yjqucYOX7RzJP2K1KBIGeiUcPpJStA0LyYhQQhWCfmk5rQtPoB+OBUwnxQybgatrGxpqoVlZM6DLwwWpx2EgROogK+z47+qFROMmtwtKqwqzBc4CDQ46WwNy5KJqKbJ4R8zD38Bz6aBGO4q4/zRi8GRxAbn0kFuqbyXzoxXwZHON2sUvIl+nAHUknTjBlJjjegp8O40GTUeL069mLoiTgOQtKsrzJSWkgwc2Zpg2CDFCHLxqiVVeDv5KtoKwnjChSCsMQ5Ky+MUYZks1Nv4WBUoyp4t0JvkQ8SVi6MwBBXyYy8JW0fXIc0OZqCnU0AQcjsDj4RrfFSY1T78xQ8plQYKRXJeuFwgrdfplrbxtOUaqoM3frtfOwcQfAm8EGDYEEgQeOMBR/X5EgQIGH4XsIvYRLzTZyrzBgHofhBGXOqqTLXolXw2Z3VdcqqUXQ9Gw/V3qCY2J3PQptefoC2GeNgYg5iIUmuIsHCzoRefZ9fZyI5t/3x47OFJqFKNh9VId17WaicOQIQBBTrmVzyfn2xJrhxmmaOgwzSmzE8PdMLMA4ebjUn9GaTGQRfNiDjwaAiGoUvsS/5gIrv/U0uYSRBYxOx/cNtwTTwQTjSi1twjDJ6U9e/jN2iOY0hWXm9gTlEM69Ad6qvMlpeKlMCyV2U8XIqnxrJn9zHs9Tk6dJc/gsA5OyfGnbC6v9E6zy5NQ6CLz1dXDo8USmEYuOcubGDLBNLrxDo3i7jpclUanY2Mwf+DCX+OcbzauiUJFtbnJrajgk8cx6Dv49PI5aeLs6hMcpeVjEw7aSyZA/qZzKrNUwVfyy4r2JcwGkvV4RqTDaJO4y5GcNFzlRV1QRjRY53YK1nVfAunExc4MUFgalZyxNU1bhUACN/Bb8gN32TElXjxtd/+CQsGIPER1UUsAkm8cIBThRvXviHjdksw+SaGH8fr1uYxrCAakzbJlT4gdADwk58mzqylJSUlJSUlJSUlJSUlJSUlJSUlJSUlJSUlJSUlJSUlJSUlGP0fw3DCB9TllneAAAAAElFTkSuQmCC">
            			</td>
            			<td>
            				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            			</td>
            			<td>
            			<img class="hyj_imagecontainer" src="https://t1.kakaocdn.net/kakaocorp/corp_thumbnail/Kakao.png">
            			</td>
            			<td>
            				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            			</td>
            			<td>
            			<img class="hyj_imagecontainer" src="https://www.hyundai.co.kr/upload/image/mm100/mm100-0-2500.jpg">
            			</td>
            			<td>
            				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            			</td>
            			
            			<td>
            			<img class="hyj_imagecontainer" src="https://pbs.twimg.com/profile_images/933218943669452800/gFTQXBZt_400x400.jpg">
            			</td>
            		</tr>
            		<tr>
            			<td>
							채용공고제목
            			</td>
            			<td>
            				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            			</td>
            			<td>
                    	        채용공고제목
            			</td>
            			<td>
            				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            			</td>
            			<td>
                      	      채용공고제목
            			</td>
            			<td>
            				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            			</td>
            			
            			<td>
                           	 채용공고제목
            			</td>
                    </tr>
            		<tr>
            			<td>
							NC 소프트
            			</td>
            			<td>
            				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            			</td>
            			<td>
                            KAKAO
            			</td>
            			<td>
            				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            			</td>
            			<td>
                       	     현대
            			</td>
            			<td>
            				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            			</td>
            			
            			<td>
                      	      롯데
            			</td>
                    </tr>
                    <tr>
            			<td>
							서울
            			</td>
            			<td>
            				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            			</td>
            			<td>
                            	서울
            			</td>
            			<td>
            				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            			</td>
            			<td>
                         	   울산
            			</td>
            			<td>
            				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            			</td>
            			
            			<td>
                           	 부산
            			</td>
                    </tr>
            	</table>
            </td>
        </tr>
    
    </table>
<%@include file="/views/common/cho_footer.jsp" %>
</body>
</html>