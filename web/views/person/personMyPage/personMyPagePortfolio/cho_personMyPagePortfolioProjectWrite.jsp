<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
    <style>
        #projectWrite{
            width: 700px;
            height: 900px;
            text-align: center;
            margin-left: auto;
            margin-right: auto;
            border: 1px solid red;
        }
        .td_subtitle{
            width: 200px;
            
        }
        .td_input{
            width: 490px;
        }
        .portfolioProjectWriteTable_botton{
            float: right;
            width: 210px;
            height: 60px;
            border-radius: 0.5pc;
            background-color: #5A84F1;
            color: white;
            font-size: 15pt;
           
        }
    </style>
</head>
<body>
	<form>
    <table id="projectWrite">
        <tr>
            <td colspan="2">
                <h1>프로젝트 등록</h1>
                <hr>
            </td>
        </tr>
        <tr>
            <td class="td_subtitle">
                <h3>제목</h3>
            </td>
            <td class="td_input">
                <input type="text">
            </td>
        </tr>
        <tr>
            <td class="td_subtitle">
                <h3>개발기간</h3>
            </td>
            <td class="td_input">
                <input type="date">
            </td>
        </tr>
        <tr>
            <td class="td_subtitle">
                <h3>주요언어</h3>
            </td>
            <td class="td_input">
                <input type="text">
            </td>
        </tr>
        <tr>
            <td class="td_subtitle">
                <h3>한줄 소개</h3>
            </td>
            <td class="td_input">
                <input type="text">
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <h3>상세정보</h3>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <textarea cols="80" rows="20"></textarea>
            </td>
        </tr>
        <tr>
            <td class="td_subtitle">
                <h3>실행화면 사진</h3>
            </td>
            <td class="td_input">
                <input type="file">
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <input type="button" value="파일추가">
            </td>
        </tr>
        <tr>
            <td class="td_subtitle">
                <h3>코드 사진</h3>
            </td>
            <td class="td_input">
                <input type="file">
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <input type="button" value="파일추가">
            </td>
        </tr>
        <tr>
            <td class="td_subtitle">
                <h3>파일 첨부</h3>
            </td>
            <td class="td_input">
                <input type="file">
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <input type="button" value="파일추가">
            </td>
        </tr>
        <tr>
            <td class="td_subtitle">
                <h3>gitHub</h3>
            </td>
            <td class="td_input">
                <input type="file">
            </td>
        </tr>
        <tr><td colspan="2">
            <input class="portfolioProjectWriteTable_botton" type="submit"  value="등록하기" style="margin-right: 250px;"onclick="goSaveAndBack()">
        </td></tr>
    </table>
    </form>
    <script type="text/javascript">
    	function goSaveAndBack(){
    		location.href="cho_personMyPagePortfolioWrite.jsp";
    	}
    </script>

</body>
</html>