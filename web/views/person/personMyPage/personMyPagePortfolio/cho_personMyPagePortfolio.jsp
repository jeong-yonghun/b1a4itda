<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.util.*"%>
     <%@page import="com.kh.itda.addInfo.school.model.vo.*" %>
     <%@page import="com.kh.itda.addInfo.portfolio.model.vo.*" %>
     <%Portfolio portfolio=(Portfolio) request.getAttribute("portfolio"); %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <style>
       
        .portfolioMainTable{
            margin-left: auto;
            margin-right: auto;
           width: 1400px;
            height:1000px;
        }
     
        
        .portfolioMainTable_tr_td_section{
            
              width:800px;
        }
        
        .portfolioMainTable_tr_td_section_div1{
            border-top: 6px solid #5A84F1;
            height: 150px;
        }
        .portfolioMainTable_tr_td_section_div2{
            border-top: 6px solid #5A84F1;
            height: 580px;
        }
        .portfolioMainTable_tr_td_section_botton{
            float: right;
            width: 210px;
            height: 60px;
            border-radius: 0.5pc;
            background-color: #5A84F1;
            color: white;
            font-size: 15pt;
        }
       .container{
          width: 840px;
           margin-top:30px;
       }
       .portfolio_link_section{
       margin-left: auto;
       margin-right: auto; 
       margin-top: 20px;
       height: 100px;
       width: 600px;
       border:2px solid #CCCCCC;
       border-radius: 1pc;
       }
       .portfolioMainTable_tr_td_aside{
       	width:280px; 
       }
    </style>
</head>
<body>

<%@include file="/views/common/menubar.jsp" %>
<%@include file="/views/person/personMyPage/cho_personMyPageMenubar.jsp" %>





    <table class="portfolioMainTable">
        <tr class="portfolioMainTable_tr">
            <td class="portfolioMainTable_tr_td_nav" style="width:280px;vertical-align:top;">
                <!-- nav 개인프로필 영역이랑 사이드 메뉴바인클루드 하기 -->
              <%@include file="/views/person/person_common/cho_sideMenubarAndUserInfo.jsp"%> 
            </td>
            <td class="portfolioMainTable_tr_td_section" style="width:840px;">
                <h1 style="font-weight: bold;">포트폴리오</h1>
                <div class="portfolioMainTable_tr_td_section_div1">
                    
                    <div class="portfolio_link_section">
                    <%if(portfolio.getTitle()==null){ %>
                        <h1 class="portfolio_link_section_h1" style="text-align:center ;color: #CCCCCC;">포트폴리오를 작성해주세요</h1>
                        <%}else{ %>
                        <table style="margin-top:20px;margin-left:auto;margin-right:auto;"><tr>
                        <td><h3 style="color: #7c7c7c;">포트폴리오URL: </h3></td>
                        <td style="padding-top:15px;"><input type="text" value=" <%=request.getContextPath()%>/portfolioDetail?perNo=<%=loginUser.getPerNo()%>"
                        style="width:300px; height:40px; font-size:20px; border:white;" readonly></td>
                        </tr></table>
                        <%} %>
                    </div>
                </div>
                <%if(portfolio==null){ %>
                <button class="portfolioMainTable_tr_td_section_botton" id="portfolioWrite"style="margin-left:30px; margin-right:80px;"
                onclick="goWrite()">포트폴리오 작성하기</button>
               <%}else{ %>
                <button class="portfolioMainTable_tr_td_section_botton" id="portfolioUpdate"style="margin-left:30px; margin-right:80px;"
                onclick="goUpdate('<%=loginUser.getPerNo()%>')">포트폴리오 수정하기</button>
                <button class="portfolioMainTable_tr_td_section_botton" id="portfolioView" style="margin-left:30px;"
                 onclick="goView('<%=loginUser.getPerNo()%>')">포트폴리오 보기</button>
               <%} %> 
                <button class="portfolioMainTable_tr_td_section_botton" id="portfolioSample" onclick="goSample()">포트폴리오 샘플보기</button>

                <br><br><br><br>


                <!--포트폴리오 예시 부분-->
                <h1 style="font-weight: bold;">포트폴리오 예시</h1>
                <div class="portfolioMainTable_tr_td_section_div2">
                    <div>
                        <ul class="nav nav-tabs">
                          <li class="active1"><a href="#">HEAD</a></li>
                          <li class="active2"><a href="#">BODY</a></li>
                          <li class="active3"><a href="#">FOOTER</a></li>
                        </ul>
                        <br>
                        <img src="/itda/image/portfolioHead.PNG" class="portfolioHead" style="width:750px;">
                        <img src="/itda/image/portfolioBody.PNG" class="portfolioBody"style="width:750px;">
                        <img src="/itda/image/portfolioFooter.PNG" class="portfolioFooter" style="width:750px;">
                        
                      </div>
                    </div>
				 </td>
			 <td class="portfolioMainTable_tr_td_aside">
            	
           
            </td>
	    </tr>
    </table>
<script>
function goSample(){
	location.href="<%=request.getContextPath()%>/views/person/personMyPage/personMyPagePortfolio/cho_personMyPagePortfolioSample.jsp";
}
function goWrite(){
	location.href="<%=request.getContextPath()%>/views/person/personMyPage/personMyPagePortfolio/cho_personMyPagePortfolioWrite.jsp";
}
function goView(perNo){
	
}
function goUpdate(perNo){
	
}

$(function() {
	$(".portfolioBody").hide();
    $(".portfolioFooter").hide();
    $(".portfolioHead").show();
	$(".active1").click(function() {
     $(".portfolioBody").hide();
     $(".portfolioFooter").hide();
     $(".portfolioHead").show();
	 });
	$(".active2").click(function() {
		$(".portfolioBody").show();
    	$(".portfolioHead").hide();
    	$(".portfolioFooter").hide();
	});
	$(".active3").click(function() {
		$(".portfolioFooter").show();
    	$(".portfolioHead").hide();
    	$(".portfolioBody").hide();
	});
});

</script>










<%@include file="/views/common/cho_footer.jsp" %>



</body>
</html>