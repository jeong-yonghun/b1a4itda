<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

<style>
  
        .portfolioMainTable{
            margin-left: auto;
            margin-right: auto;
            width: 1400px;
            height:2000px;
        }
        
        .portfolioMainTable_tr_td_section{
            width: 840px;
        }
       
        .portfolioMainTable_tr_td_section_botton{
            float: right;
            width: 210px;
            height: 60px;
            border-radius: 0.5pc;
            background-color: #5A84F1;
            color: white;
            font-size: 15pt;
        }
       .container{
           width: 840px;
       }
       .portfolioMainTable_tr_td_section_write{
           border-top: 6px solid #5A84F1;
            height: 150px;
       }
       .jyh_skill-exp-slide{
            text-align: left;
            margin-left:auto;
            margin-right:auto;
            margin-bottom:10px;
            width:80%;
            border:1px solid #5A84F1;
            font-weight: bold;
            font-size: 18px;
            color:#5A84F1;
        }
        .downtriangle-img{
            width:25px;
            float:right;
        }
        .jyh_skill-exp-content{
            width:640.39px;
            margin-left:auto;
            margin-right:auto;
            margin-bottom:10px;
            display:none;
        }
        .jyh_skill-exp-content > table{
            text-align:left;
            font-size:15pt;
            font-weight:bold;
        }
        input[name=skill-exp]{
            width:18px;
            height:18px;
        }
        .jyh_java > table{
            width:120%;
            height:300px;
        }
        .jyh_mobile-app > table{
            width:130%;
            height:100px;
        }
        .jyh_php-asp > table{
            width:113%;
            height:150px;
        }
        .jyh_net > table{
            width:114%;
            height:150px;
        }
        .jyh_javascript > table{
            width:118%;
            height:100px;
        }
        .jyh_c > table{
            width:115%;
            height:150px;
        }
        .jyh_db > table{
            width:118%;
            height:150px;
        }
        .jyh_publisher > table{
            width:113%;
            height:150px;
        }
        .portfolioWriteTable_botton{
            float: right;
            width: 210px;
            height: 60px;
            border-radius: 0.5pc;
            background-color: #5A84F1;
            color: white;
            font-size: 15pt;
           
        }
        #projectWrite{
            width: 700px;
            height: 900px;
            text-align: center;
            margin-left: auto;
            margin-right: auto;
            border: 1px solid red;
        }
        .td_subtitle{
            width: 200px;
            
        }
        .td_input{
            width: 490px;
        }
        .portfolioProjectWriteTable_botton{
            float: right;
            width: 210px;
            height: 60px;
            border-radius: 0.5pc;
            background-color: #5A84F1;
            color: white;
            font-size: 15pt;
           
        }
    
    </style>

</head>
<body>

<%@include file="/views/common/menubar.jsp" %>
<%@include file="/views/person/personMyPage/cho_personMyPageMenubar.jsp" %>

 <table class="portfolioMainTable">
        <tr class="portfolioMainTable_tr">
            <td class="portfolioMainTable_tr_td_nav" style="width:280px; vertical-align:top;">
                <!-- nav 개인프로필 영역이랑 사이드 메뉴바인클루드 하기 -->
              	<%@include file="/views/person/person_common/cho_sideMenubarAndUserInfo.jsp" %>
            </td>
            <td class="portfolioMainTable_tr_td_section" style="vertical-align:top;">
                <h1 style="font-weight: bold;">포트폴리오 작성</h1>
                <div class="portfolioMainTable_tr_td_section_write">
                  <form  action="<%= request.getContextPath() %>/insertPortfolio" method="post">
                   <table id="portfolio_write_Table" style="margin-left: auto;margin-right: auto;">
                       <tr>
                           <td>
                                <h3>포트폴리오 제목</h3>
                           </td>
                       </tr>
                       <tr>
                           <td>
                               <input type="text" name="portfolio_title" placeholder="포트폴리오 제목을 입력하세요."style="width:700px; height:40px">
                           </td>
                       </tr>
                       <tr>
                           <td>
                         	      제목 부분 배경
                           </td>
                       </tr>
                       <tr>
                        <td>
                            <input type="file" name="portfolio_title_backimg">
                        </td>
                       </tr>
                       <tr>
                           <td>
                               <h3>자기소개</h3>
                           </td>
                       </tr>
                       <tr>
                           <td>
                                <p><textarea id="introSelf" name="portfolio_content"rows="30" cols="30" style="width: 700px;" placeholder="자기소개를 입력하세요."></textarea></p>
                           </td>
                       </tr>
                       <tr>
                           <td>
                               <h3>자기소개 배경</h3>
                           </td>
                       </tr>
                       <tr>
                           <td>
                               <input type="file" name="portfolio_content_backimg">
                           </td>
                       </tr>
                       
                       
                       <tr>
                           <td>
                               <h3>스킬 배경</h3>
                           </td>
                       </tr>
                       <tr>
                           <td>
                               <input type="file" name="portfolio_skill_backimg">
                           </td>
                       </tr>
                       <tr>
                           <td>
                                <h3>프로젝트 배경</h3>
                           </td>
                       </tr>
                       <tr>
                           <td>
                               <input type="file" name="portfolio_project_backimg">
                               <br>
                           </td>
                       </tr>
                     
                       <tr>
                           <td>
                               <h3 style="display: inline-block;">프로젝트 등록</h3>
                               <button style="margin-left: 20px;" type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myLargeModal">+</button> 
								<!-- onclick="goProjectWrite();" --> 
                           </td>
                       </tr>
                       <tr>
                            <td>
                              <input class="portfolioWriteTable_botton" type="button" style="margin-left: 40px; margin-right: 120px;" value="미리보기">
                              <input class="portfolioWriteTable_botton" type="submit"  value="저장하기">
                            </td>
                       </tr>
                   </table>
                 </form>
                </div>
            </td>
            <td class="portfolioMainTable_tr_td_aside" style="width:280px;vertical-align:top;margin-top:200px;">
                <div style="border: 2px solid #CCCCCC; width: 260px;margin-left:10px;">
                    <h3 style="color: #CCCCCC;">프로젝트 작성필수 요소</h3>
                    <h4 style="color: #CCCCCC;">프로젝트중 자신이 맡은 주요기능을 실행화면과 함께 코드를 작성해주세요. 이는 필수 입니다.</h4>
                    <img src="/itda/image/cho_portfolio/projectImg.PNG" style="width:240px;height:170px;">
                    <img src="/itda/image/cho_portfolio/projectCode.PNG" style="width:240px;height:170px;">
                    <h4>출처:<br>https://dl0312.github.io/geon-lee/</h4>
                </div>
            </td>
        </tr>
    </table>
    <!-- Modal -->
<div class="modal fade" id="myLargeModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">프로젝트 등록</h4>
      </div>
      <div class="modal-body">
       <form>
    <table id="projectWrite">
        <tr>
            <td colspan="3">
                <h1>프로젝트 등록</h1>
                <hr>
            </td>
        </tr>
        <tr>
            <td class="td_subtitle">
                <h3>제목</h3>
            </td>
            <td class="td_input" colspan="2">
                <input type="text" name="project_title">
            </td>
        </tr>
        <tr>
            <td class="td_subtitle">
                <h3>개발기간</h3>
            </td>
            <td class="td_input" >
                <input type="date" name="project_startDate"> ~ 
            </td>
            <td><input type="date" name="project_endDate"></td>
        </tr>
        <tr>
            <td class="td_subtitle">
                <h3>주요언어</h3>
            </td>
            <td class="td_input" colspan="2">
                <input type="text" name="project_skill">
            </td>
        </tr>
        <tr>
            <td class="td_subtitle">
                <h3>한줄 소개</h3>
            </td>
            <td class="td_input" colspan="2">
                <input type="text" name="project_intro">
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <h3>상세정보</h3>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <textarea cols="80" rows="20"></textarea>
            </td>
        </tr>
        <tr>
            <td class="td_subtitle">
                <h3>실행화면 사진</h3>
            </td>
            <td class="td_input" colspan="2">
                <input type="file" name="project_exe_img">
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <input type="button" value="파일추가">
            </td>
        </tr>
        <tr>
            <td class="td_subtitle">
                <h3>코드 사진</h3>
            </td>
            <td class="td_input" colspan="2">
                <input type="file" name="project_code_img">
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <input type="button" value="파일추가">
            </td>
        </tr>
        <tr>
            <td class="td_subtitle">
                <h3>파일 첨부</h3>
            </td>
            <td class="td_input" colspan="2">
                <input type="file" name="other_file">
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <input type="button" value="파일추가">
            </td>
        </tr>
        <tr>
            <td class="td_subtitle">
                <h3>gitHub</h3>
            </td>
            <td class="td_input" colspan="2">
                <input type="text" name="github_url">
            </td>
        </tr>

    </table>

      </div>
      <div class="modal-footer">

        <button type="button" class="btn btn-default" data-dismiss="modal">취소</button>
        <button type="submit" class="btn btn-primary" >등록하기</button>
      </div>
    </div>
  </div>
</div>
	</form>
    <script type="text/javascript">
    	function goProjectWrite(){
    		location.href="cho_personMyPagePortfolioProjectWrite.jsp";
    	}
    </script>
     
    <%@include file="/views/common/cho_footer.jsp" %>
</body>
</html>
