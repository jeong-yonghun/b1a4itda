<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <style>
       
        .portfolioMainTable{
            margin-left: auto;
            margin-right: auto;
            width: 1400px;
            height:2500px;
            border: 1px solid black;
            text-align: center;
        }
        #portfolioButtonPart{
            height: 10%;
        }
       .cl{
           border: 1px solid black;
       }
       #portfolioSampleCheckButton{
            margin-left: 600px;
            width: 210px;
            height: 60px;
            border-radius: 0.5pc;
            background-color: #5A84F1;
            color: white;
            font-size: 15pt;
       }
       #portfolioTitlePart{
           background-image:url('/itda/image/cho_portfolio/title1.gif');
           background-size: 1400px;
           background-repeat: no-repeat;
       }
       .portfolioTitlePart_h1{
           
           font-size: 100px;
           color: white;
       }
       #portfolioIntroPart{
           background-color: #E9ECF3;
           background-size: 1400px;
           background-repeat: no-repeat;
       }
       #portfolioSkillPart{
           background-image:url('dog.png');
           background-size: 1400px;
           background-repeat: no-repeat;
           background-color:#F1F3F7;
       }
       #portfolioProjectPart{
           background-image:url('dog.png');
           background-size: 1400px;
           background-repeat: no-repeat;
           background-color:#9FF3D4;
           
       }
    </style>
</head>
<body>


    <table class="portfolioMainTable">
        <tr class="cl" id="portfolioTitlePart">
            <td>
                <h1 class="portfolioParth_h1" style="color:#5A84F1;">1.포트폴리오 제목 부분입니다.</h1>
            </td>
        </tr>
        <tr class="cl" id="portfolioIntroPart">
            <td>
                <div style="display: inline-block;width: 30%;border: solid 1px red;margin-top: 0px;"><h1>ABOUT ME</h1></div>
                <div style="display:inline-block;width: 65%;border: 1px solid red;height: 300px;margin-top: 0px;"> 
                    <h1 class="portfolioParth_h1" style="color:#5A84F1;">2.포트폴리오 자기소개 부분입니다.</h1>
                </div>        
            </td>
        </tr>
        <tr class="cl" id="portfolioSkillPart">
            <td>
                <table style="margin-left: auto;margin-right: auto;">
                    <tr>
                        <td>
                            <h1 style="border: 1px red solid">Skills</h1>
                            <br> <h1 style="color:#5A84F1;">3.포트폴리오 스킬부분입니다.</h1>
                            
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <!-- 반복문으로 div들 입력 받은 만큼출력해주기 -->
                            <div style="width:80px;height:25px; background-color:pink;display: inline-block;">JAVA</div>
                             <div style="width:100px;height:25px; background-color:red;display: inline-block;">JAVA Script</div>
                              <div style="width:80px;height:25px; background-color:blue;display: inline-block;">HTML</div>
                              <div style="width:80px;height:25px; background-color:yellow;display: inline-block;">CSS</div>
                        </td>
                    </tr>
                </table>

            </td>
        </tr>
        <tr class="cl" id="portfolioProjectPart">
            <td>
                <table style="width: 85%;margin-left: auto;margin-right: auto;height: 100px;">
                    <tr>
                        <td>
                        	<h1>PROJECT</h1>
                        	<br>
                        	<h1 style="color:#5A84F1;">4.포트폴리오 프로젝트 부분입니다.</h1>
                            <!-- 반복문으로 섬네일 가로버전으로 생성할수 있게 하기 -->
                            <div style="width:80%; border:1px red solid;margin-left: auto;margin-right: auto;">
                                <div style="display: inline-block;width: 30%;">
                                    이미지
                                    <img src="/itda/image/cho_portfolio/project.png" style="height:250px;">
                                </div>
                                <div style="display: inline-block;width: 65%;">제목이랑 대략적인 내용</div>
                            
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr class="cl" id="portfolioButtonPart">
            <td>
                <button id="portfolioSampleCheckButton" style="margin-right: auto;margin-left: auto;"
                onclick="goback()">확인</button>
            </td>
        </tr>
    </table>
<script>
function goback(){
    location.href="cho_personMyPagePortfolio.jsp";
}

</script>




</body>
</html>