<!-- 개인회원 상세보기 페이지 입니다. -->
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="com.kh.itda.member.model.vo.Jyh_PerMember, java.util.ArrayList"%>
<%
	Jyh_PerMember perMember = (Jyh_PerMember) request.getAttribute("perMember");
	ArrayList<String> skillArr = (ArrayList<String>) request.getAttribute("skillArr");
%>
<!DOCTYPE html>
<html lang="ko">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <style>
        a:link { 
            text-decoration: none;
            color: black;
        }
        a:visited { 
            text-decoration: none;
            color: black;
        }
        .jyh_article1{
            width:840px; 
            height:75px;
        }
        .jyh_article2{
            width:840px; 
            height:35px;
            text-align: center;
        }
        #jyh_nav{
            width:280px;
            height:1000px;
        }
        #jyh_aside{
            width:280px;
        }
        footer{
            margin-left: auto;
            margin-right: auto;
            width:1400px;
            height:100px;
        }

        #jyh_outline-table, #td2 table{
            margin:auto; 
            border-collapse:collapse; 
            border-spacing:0;
            padding:0px;
        }
        #jyh_outline-table td{
            padding:0px;
        }

        .jyh_per-top-btn{
            float: left;
            margin-right:50px;
            margin-left:10px;
        }
        #jyh_detail-info tr td{
        	/* padding:2.5px 10px 2.5px 10px; */
        	padding:10px 10px 10px 10px;
        }
        #jyh_detail-info tr{
        	/* border-top:1px solid gray;
        	border-bottom:1px solid gray; */
        }
    </style>
</head>
<body>
    <%@ include file="../common/jyh_adminMenubar.jsp" %>

<table id="jyh_outline-table">
    <tr>
        <td id="jyh_nav" style="vertical-align:top;">
                <div class="jyh_article1" style="width:100%; vertical-align:top;" ></div>
            <%@ include file="../common/jyh_adminMemManageSideMenu.jsp" %>
    </td>
    <td id="td2" style="width:840px; vertical-align:top;">
            <div class="jyh_article1"></div>
            <div class="jyh_article2">
                <div id="jyh_per-top-btn1" class="jyh_per-top-btn">회원 상세정보<hr style="background:#5A84F1; margin-top:2px;"></div>
                <div id="jyh_per-top-btn2" class="jyh_per-top-btn">지원 이력</div>
                <div id="jyh_per-top-btn3" class="jyh_per-top-btn">이력서/포트폴리오</div>
            </div>
                <hr style="background:gray;">
            <!-- <div id="section1"> -->
                <div id="jyh_detail-outline" style="margin-bottom:10px; float:left; width:100%; height:100%;">
                    <div id="jyh_detail-table-outline1" style="float:left; width:60%;">
                    <table id="jyh_detail-info"style="width:100%;">
                        <tr>
                            <td>아이디</td>
                            <td><span><%= perMember.getPerId() %></span></td>
                        </tr>
                        <tr>
                            <td>이름</td>
                            <td><span><%= perMember.getPerName() %></span></td>
                        </tr>
                        <tr>
                            <td>이메일</td>
                            <td><span><%= perMember.getEmail() %></span></td>
                        </tr>
                        <tr>
                            <td>휴대전화</td>
                            <td><span><%= perMember.getPhone() %></span></td>
                        </tr>
                        <tr>
                            <td>가입일</td>
                            <td><span><%= perMember.getEnrollDate() %></span></td>
                        </tr>
                        <% if(perMember.getStatus().equals("탈퇴")) {%>
                        <tr>
                            <td>탈퇴일</td>
                            <td><span><%= perMember.getModifyDate() %></span></td>  
                        </tr>
                        <%} else if(perMember.getStatus().equals("정지")) {%>
                        <tr>
                            <td>정지일</td>
                            <td><span><%= perMember.getModifyDate() %></span></td>  
                        </tr>
                        <%} %>
                        <tr>
                            <td>정보보유기간</td>
                            <td><span><%= perMember.getInfoPeriod() %></span></td>
                        </tr>
                        <tr>
                            <td>회원상태</td>
                            <% if(perMember.getStatus().equals("탈퇴")) {%>
                            <td><span><%= perMember.getStatus() %></span></td>
                            <%} else if(perMember.getStatus().equals("정지")) {%>
                            <td><span><%= perMember.getStatus() %></span></td>
                            <%} else{%>
                            <td><span>승인</span></td>
                            <%} %>
                        </tr>
                    </table>
               
               		<h2>스킬</h2>
                    <div style="margin-top:20px; width:504px; word-break:break-all;" >
                    
                    <% for(int i = 0; i < skillArr.size(); i++) { %>
                    	<div style="border:1px solid gray; background:#C4C4C4; border-radius:5px; margin:5px; padding:2px; float:left;"><%= skillArr.get(i) %></div>
                    <%} %>
                    
                    </div>

                   <!--  <table style="border:1px solid blue; margin-top:20px; width:100%;" >
                            <caption>관심 분야</caption>
                            <tr>
                                <td><input type="checkbox" name="interests"><lable>관심분야데이터</lable></td>
                                <td><input type="checkbox" name="interests"><lable>관심분야데이터</lable></td>
                                <td><input type="checkbox" name="interests"><lable>관심분야데이터</lable></td>
                                <td><input type="checkbox" name="interests"><lable>관심분야데이터</lable></td>
                                <td><input type="checkbox" name="interests"><lable>관심분야데이터</lable></td>
                            </tr>
                            <tr>
                                <td><input type="checkbox" name="interests"><lable>관심분야데이터</lable></td>
                                <td><input type="checkbox" name="interests"><lable>관심분야데이터</lable></td>
                                <td><input type="checkbox" name="interests"><lable>관심분야데이터</lable></td>
                                <td><input type="checkbox" name="interests"><lable>관심분야데이터</lable></td>
                                <td><input type="checkbox" name="interests"><lable>관심분야데이터</lable></td>
                            </tr>
                            <tr>
                                <td><input type="checkbox" name="interests"><lable>관심분야데이터</lable></td>
                                <td><input type="checkbox" name="interests"><lable>관심분야데이터</lable></td>
                                <td><input type="checkbox" name="interests"><lable>관심분야데이터</lable></td>
                                <td><input type="checkbox" name="interests"><lable>관심분야데이터</lable></td>
                                <td><input type="checkbox" name="interests"><lable>관심분야데이터</lable></td>
                            </tr>
                        </table> -->
                    </div>
                    <div id="jyh_detail-table-photo-outline" style="float:right; width:40%; height:100%;">
                    <div id="jyh_detail-table-photo-inline" style="width:100%; height:200px;">
                        <img>
                    </div>
                </div>
            </div>
            
            <!-- </div> -->
            <div style="width:100%;float:left;">
            	<% if(perMember.getStatus().equals("탈퇴")) {%>
                <h3>탈퇴사유</h3>
                <hr style="width:100%; margin:auto; border:0.5px solid gray;">
                <div style="margin-top:20px; width:504px; word-break:break-all;" >
                	<%= perMember.getReason() %>
                </div>
            	<%} %>
            </div>
        </td>
    <td id="jyh_aside">
    </td>
    </tr>
</table>
    
    
    <%@ include file="../common/cho_footer.jsp" %>
    
    <script>
    	$("#jyh_per-top-btn1").click(function(){
         	location.href = "<%=request.getContextPath()%>/jyhSelectOne.me?userNo=<%=perMember.getPerNo()%>&memType=1";
        });
    	
    	$("#jyh_per-top-btn2").click(function(){
        	location.href = "<%=request.getContextPath()%>/jyhApplicationList.me?userNo=<%=perMember.getPerNo()%>&memType=1";
        });
    	
    	$("#jyh_per-top-btn3").click(function(){
         	location.href = "<%=request.getContextPath()%>/jyhDocumentList.me?userNo=<%=perMember.getPerNo()%>&memType=1";
        });
    </script>
   
</body>
</html>