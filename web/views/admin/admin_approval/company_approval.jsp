
<%@page import="com.kh.itda.member.model.vo.Jyh_ComMember"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import=" java.util.ArrayList"%>
<%@ page import="com.kh.itda.approval.model.vo.*"%> 
<% 
ArrayList<Jyh_ComMember> list = (ArrayList) request.getAttribute("list");
PageInfo pi = (PageInfo) request.getAttribute("pi");
int listCount = pi.getListCount();
int currentPage = pi.getCurrentPage();
int maxPage = pi.getMaxPage();
int startPage = pi.getStartPage();
int endPage = pi.getEndPage();
%>    
    
<!DOCTYPE html>
<html lang="ko">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <style>
        a:link { 
            text-decoration: none;
            color: black;
        }
        a:visited { 
            text-decoration: none;
            color: black;
        }
        .wjs_article1{
            width:840px; 
            height:75px;
        }
        .wjs_article2{
            width:840px; 
            height:35px;
            text-align: center;
        }
        #wjs_nav{
            width:280px;
        }
        #wjs_nav-div{
            height:1000px;
        }
        #wjs_side-menu-per, #jyh_side-menu-com{
            width:80%;
            margin:auto;
        }
        #wjs_side-menu-com{
            margin-top:50px;
        }
        #wjs_side-menu-outline p{
            margin:10px;
        }
        #wjs_side-menu-outline ul{
            list-style:none; 
            padding-left:5px;
        }
        #wjs_side-menu-outline{
            width:75%;
            margin:auto;
            border:1px solid gray;
        }

        #wjs_aside{
            width:280px;
        }
        footer{
            margin-left: auto;
            margin-right: auto;
            width:1400px;
            height:100px;
        }
        #section{
            height:1000px;
        }

        #wjs_outline-table{
            margin:auto; 
            border-collapse:collapse; 
            border-spacing:0;
        }
        #wjs_outline-table td:nth-of-type(1n){
            margin:0;
            padding:0;
        }

        #wjs_search-per{
            padding:5px;
            text-align: left;
            font-weight:bold;
            font-size: 15pt;
        }
        #wjs_page-descript-per{
            width:100%;
            height:100px;
        }
        #wjs_page-descript-per ul{
            margin:0;
            width:85%;
        }
        #wjs_page-descript-per p:nth-of-type(1n){
            margin:8px;
        }
        #wjs_modal_open_btn{
            width:85px;
            height:30px;
            float:left;
            font-weight:500;
            font-size: 12pt;
            border-radius: 5px;
            border:0;
            outline:0;
            margin-top:8px;
        }
        #wjs_modal{
            width:840px;
            display: none;
        }

        #wjs_modal {
            position:relative;
            width:100%;
            height:100%;
            z-index:1;
            }

        #wjs_modal h2 {
            margin:0;   
        }


        #wjs_modal .wjs_modal_content {
            width:500px;
            height:330px;
            margin:100px auto;
            padding:20px 10px;
            background:#fff;
            border:2px solid #666;
        } 
        .wjs_modal_content div:first-child{
            width:100%; 
            height:32px;
        }
        #wjs_modal .wjs_modal_layer {
            position:fixed;
            top:0;
            left:0;
            width:100%;
            height:100%;
            background:rgba(0, 0, 0, 0.5);
            z-index:-1;
        }  
        #wjs_word-list > table{
            margin:auto;
        }
        #wjs_word-list p:nth-of-type(1n){
            padding:10px;
            margin:0;
        }
        #wjs_modal_close_btn, #wjs_modal_next_btn, #wjs_modal_back_btn{
            width:85px;
            height:30px;
            float:right;
            font-weight:500;
            font-size: 12pt;
            border-radius: 5px;
            border:0;
            outline:0;
            margin-top:2px;
        }
        #wjs_per-list {
            border-collapse: collapse;
            border: 1px solid #168;
            width:100%;
        }  
        #wjs_per-list th {
            color: #168;
            background: #f0f6f9;
            text-align: center;
        }
        #wjs_per-list th, #wjs_per-list td {
            padding: 10px;
            /* border: 1px solid #ddd; */
        }
        #wjs_per-list th:first-child, #jyh_per-list td:first-child {
            border-left: 0;
        }
        #wjs_per-list th:last-child, #jyh_per-list td:last-child {
            border-right: 0;
        }
        #wjs_per-list tr td:first-child{
            text-align: center;
        }
        .wjs_search-filter{
            border:1px solid gray;
            border-radius: 5px;
        }
        .wjs_search-select{
            height:24px;
        }
        .wjs_search-line, #search{
            vertical-align: middle;
            margin-left:5px;
        }
        .wjs_search-radius{
            border-radius: 5px;
            border: 1px solid gray;
        }
        #search{
            width:200px;
            height:20px;
        }
    </style>
</head>
<body>
<%@ include file="../../common/jyh_adminMenubar.jsp" %>
<table id="wjs_outline-table">
    <tr>
        <td id="wjs_nav">
            <div id=wjs_nav-div>
        <div id="wjs_side-menu-outline">
            <div id="wjs_side-menu-per">
                <h3>기업회원 승인관리</h3>
                <hr style="margin-right:auto; margin-left:auto; margin-top:5px; border: solid 2px #58ACFA;">
                
            </div>
            <div id="wjs_side-menu-per">
                <h3 id="recruit">채용 공고 승인관리</h3>
                <hr style="margin-right:auto; margin-left:auto; margin-top:5px;">
                <!-- <ul>
                    <li style="font-weight:bold;">기업회원정보
                        <ul style="font-weight:400;">
                            <li><p>- 기업회원 승인관리</p></li>
                            <li><p>- 채용공고 승인관리</p></li>
                            <li><p>- 공모전 공고 승인관리</p></li>
                        </ul>
                    </li>
                </ul> -->
            </div>
            <div id="wjs_side-menu-per">
                <h3 onclick="location.href='<%=request.getContextPath() %>/approval.re2'">공모전  승인관리</h3>
                <hr style="margin-right:auto; margin-left:auto; margin-top:5px;">
                
            </div>
        </div>
            
        </div>
    </td>
    <td align="top">
            <div class="wjs_article1"></div>
            <div class="wjs_article2">
                <p id="wjs_search-per">기업가입 승인관리</p>
            </div>
            <hr style="margin-bottom:0;">
            <div id="section">
        <div id="wjs_page-descript-per">
            <ul id="wjs_page-descript-per1" style="float:left;">
                    <li><p>사업자번호를 확인하고 약관에 근거하여 승인해주는 페이지입니다</p></li>
                    <li><p>가입신청 후 2 ~ 3 영업일 안에 확인 후 처리를 해주시기바랍니다</p></li>
                    <li><p>반려됬을시 사유를 적어 주시기 바랍니다</p></li>
                </ul>
            <button type="button" id="wjs_modal_open_btn">단어확인</button>
        </div>
                                     <!-- 모달 창 시작 -->
            <div id="wjs_modal">
                    <div class="wjs_modal_content">
                       <div><h2 style="float:left; margin:0px;">부적절한 단어</h2>
                            <button type="button" id="wjs_modal_close_btn">닫기</button>
                        </div>
                       <hr>
                       <div id="wjs_word-list">
                           <table border="1" align="center">
                               <tr>
                                   <td><p>바</p></td>
                                   <td><p>소믈리에</p></td>
                                   <td><p>노래바</p></td>
                                   <td><p>섹시빠</p></td>
                                   <td><p>강남빠</p></td>
                                </tr>
                               <tr>
                                   <td><p>빠</p></td>
                                   <td><p>웨이터</p></td>
                                   <td><p>노래빠</p></td>
                                   <td><p>강남바</p></td>
                                   <td><p>강남BAR</p></td>
                                </tr>
                               <tr>
                                   <td><p>노래BAR</p></td>
                                   <td><p>황윤종</p></td>
                                   <td><p>째즈빠</p></td>
                                   <td><p>남성전용</p></td>
                                   <td><p>비키니</p></td>
                                </tr>
                               <tr>
                                   <td><p>호빠</p></td>
                                   <td><p>섹시</p></td>
                                   <td><p>빠텐더</p></td>
                                   <td><p>여성빠</p></td>
                                   <td><p>여성전용빠</p></td>
                                </tr>
                               <tr>
                                   <td><p>여성전용바</p></td>
                                   <td><p>모던빠</p></td>
                                   <td><p>모던바</p></td>
                                   <td><p>모던BAR</p></td>
                                   <td><p>바밤BAR</p></td>
                                </tr>
                           </table>
                       </div>
                       <hr>

                       <button type="button" id="wjs_modal_next_btn">다음</button>
                       <button type="button" id="wjs_modal_back_btn" style="display: none;">이전</button>
                    </div>
                   
                    <div class="wjs_modal_layer"></div>
                </div>
                                                <!-- 모달 창 끝 -->

                <div id="wjs_searchbar-outline">
                    <div id="wjs_search_filter" style="float: right; margin-bottom:10px;">
                        <span class="wjs_search-line"><label>가입일</label></span>
                        <span class="wjs_search-line jyh_search-filter jyh_search-radius">1주일이내</span>
                        <span class="wjs_search-line jyh_search-filter jyh_search-radius">1개월이내</span>
                        <span class="wjs_search-line jyh_search-filter jyh_search-radius">6개월이내</span>
                        <select class="wjs_search-line jyh_search-select jyh_search-radius">
                            <option>전체검색</option>
                            <option>전체검색1</option>
                            <option>전체검색2</option>
                            <option>전체검색3</option>
                        </select>
                        <input type="text" class="wjs_search-radius" id="search" maxlength="50">
                        <button class="wjs_search-radius">확인</button>
                    </div>
                </div>

                <table id="wjs_per-list" border="1" align="center">
                    <tr>
                        <th><input type="checkbox" name="wjs_per-list-check"></th>
                        <th>가입번호</th>
                        <th>회사명</th>
                        <th>아이디</th>
                        <th>사업자등록번호</th>
                        <th>담당자휴대전화</th>
                        <th>가입신청일</th>
                        <th>처리상태</th>
                    </tr>
                    
                    <% for(Jyh_ComMember c : list){ %>
						<tr>
							<%-- <input type="hidden" value="<%= a.getBid() %>"> --%>
							<td><input type="checkbox" name="wjs_per-list-check"></td>
							<td><%= c.getComNo() %></td>
							<td><%= c.getCompanyName() %></td>
							<td><%= c.getComId() %></td>
							<td><%= c.getComNum() %></td>
							<td><%= c.getPhone() %></td>
							<td><%= c.getEnrollDate() %></td>
							<td><%= c.getComAddress() %></td>
						</tr>
						<% } %>
                </table>
                <hr style="margin-right:auto; margin-left:auto; margin-top:10px;">

                <div class="pagingArea" align="center">
                  <button
							onclick="location.href='<%=request.getContextPath() %>/approval.re?currentPage=1'"><<</button>
						<% if(currentPage <= 1) { %>
						<button disabled><</button>
						<%}else{ %>
						<button
							onclick="location.href='<%=request.getContextPath()%>/approval.re?currentPage=<%=currentPage -1 %>'"><</button>
						<% } %>

						<% for(int p = startPage; p <= endPage; p++){ 
									if(p == currentPage){         
       					  %>
						<button disabled><%=p %></button>
						<%
									}else {
      				    %>
						<button
							onclick="location.href='<%=request.getContextPath() %>/approval.re?currentPage=<%=p %>'"><%=p %></button>
						<%
								}
       					%>
						<%} %>

						<%if(currentPage >= maxPage) { %>
						<button disabled>></button>
						<% } else { %>
						<button
							onclick="location.href='<%=request.getContextPath() %>/approval.re?currentPage=<%=currentPage +1 %>'">></button>
						<%} %>

						<button
							onclick="location.href='<%=request.getContextPath() %>/approval.re?currentPage=<%=maxPage %>'">>></button>
                </div>
            </div>
                <hr style="margin-right:auto; margin-left:auto; margin-top:10px;">
    </td>
    <td id="wjs_aside">
    </td>
    </tr>
</table>
    
    
    <%@ include file="../../common/cho_footer.jsp" %>
    <script>
            $("#wjs_modal_open_btn").click(function(){
                $("#wjs_modal").attr("style", "display:block");
            });
           
             $("#wjs_modal_close_btn").click(function(){
                $("#wjs_modal").attr("style", "display:none");
            });  
             
             $("#recruit").click(function(){
            	 location.href="<%=request.getContextPath()%>/approval.re";
             })
             
             
              $("#wjs_per-list td").click(function(){
            	 var num = $(this).parent().children().eq(1).text();
    			 console.log(num);
    			 location.href= "<%=request.getContextPath() %>/selectAp.com?num="+num;
             });
             
        </script>>
</html>