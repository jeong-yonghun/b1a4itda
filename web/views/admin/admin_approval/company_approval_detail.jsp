<%@page import="com.kh.itda.member.model.vo.Jyh_ComMember"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
	Jyh_ComMember cm = (Jyh_ComMember) request.getAttribute("cm");
%>    
 
<!DOCTYPE html>
<html lang="ko">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<head>
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
합쳐지고 최소화된 최신 CSS
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">

부가적인 테마
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">

합쳐지고 최소화된 최신 자바스크립트
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script> -->

    <meta charset="UTF-8">
    <title>Document</title>
    <style>
        a:link { 
            text-decoration: none;
            color: black;
        }
        a:visited { 
            text-decoration: none;
            color: black;
        }
        .wjs_article1{
            width:840px; 
            height:75px;
        }
        .wjs_article2{
            width:840px; 
            height:35px;
            text-align: center;
        }
        #wjs_nav{
            width:280px;
        }
        #wjs_nav-div{
            height:1000px;
          
        }
        #wjs_side-menu-per, #wjs_side-menu-com{
            width:80%; 
            margin:auto;
        }
        #wjs_side-menu-com{
            margin-top:50px;
        }
        #wjs_side-menu-outline p{
            margin:10px;
        }
        #wjs_side-menu-outline ul{
            list-style:none; 
            padding-left:5px;
        }
        #wjs_side-menu-outline{
        	margin-top:80px;
        	margin-left:40px;
            width:75%;
           
            border:1px solid gray;
        }

        #wjs_aside{
            width:280px;
        }
        footer{
            margin-left: auto;
            margin-right: auto;
            width:1400px;
            height:100px;
        }
        #section{
            height:400px;

        }

        #wjs_outline-table{
            margin:auto; 
            border-collapse:collapse; 
            border-spacing:0;
        }
        #wjs_outline-table td:nth-of-type(1n){
            margin:0;
            padding:0;
        }

        #wjs_search-per{
            padding:5px;
            text-align: left;
            font-weight:bold;
            font-size: 15pt;
        }
        #wjs_page-descript-per{
            width:100%;
            height:100px;
        }
        #wjs_page-descript-per ul{
            margin:0;
            width:85%;
        }
        #wjs_page-descript-per p:nth-of-type(1n){
            margin:8px;
        }
        #wjs_modal_open_btn{
            width:85px;
            height:30px;
            float:left;
            font-weight:500;
            font-size: 12pt;
            border-radius: 5px;
            border:0;
            outline:0;
            margin-top:8px;
        }
        #wjs_modal{
            width:840px;
            display: none;
        }

        #wjs_modal {
            position:relative;
            width:100%;
            height:100%;
            z-index:1;
            }

        #wjs_modal h2 {
            margin:0;   
        }


        #wjs_modal .wjs_modal_content {
            width:500px;
            height:330px;
            margin:100px auto;
            padding:20px 10px;
            background:#fff;
            border:2px solid #666;
        } 
        .wjs_modal_content div:first-child{
            width:100%; 
            height:32px;
        }
        #wjs_modal .wjs_modal_layer {
            position:fixed;
            top:0;
            left:0;
            width:100%;
            height:100%;
            background:rgba(0, 0, 0, 0.5);
            z-index:-1;
        }  
        #wjs_word-list > table{
            margin:auto;
        }
        #wjs_word-list p:nth-of-type(1n){
            padding:10px;
            margin:0;
        }
        #wjs_modal_close_btn, #wjs_modal_next_btn, #wjs_modal_back_btn{
            width:85px;
            height:30px;
            float:right;
            font-weight:500;
            font-size: 12pt;
            border-radius: 5px;
            border:0;
            outline:0;
            margin-top:2px;
        }
        #wjs_per-list {
            border-collapse: collapse;
            border: 1px solid #168;
            width:100%;
        }  
        #wjs_per-list th {
            color: #168;
            background: #f0f6f9;
            text-align: center;
        }
        #wjs_per-list th, #wjs_per-list td {
            padding: 10px;
            /* border: 1px solid #ddd; */
        }
        #wjs_per-list th:first-child, #jyh_per-list td:first-child {
            border-left: 0;
        }
        #wjs_per-list th:last-child, #jyh_per-list td:last-child {
            border-right: 0;
        }
        #wjs_per-list tr td:first-child{
            text-align: center;
        }
        .wjs_search-filter{
            border:1px solid gray;
            border-radius: 5px;
        }
        .wjs_search-select{
            height:24px;
        }
        .wjs_search-line, #search{
            vertical-align: middle;
            margin-left:5px;
        }
        .wjs_search-radius{
            border-radius: 5px;
            border: 1px solid gray;
        }
        #search{
            width:200px;
            height:20px;
        }
        
        #wjs_tb{
        	margin: 0 auto;
        }
        
        #wjs_td{
        	width: 250px;
        	height: 35px;
        	text-align: left;
        }
        
        #wjs_td2{
        	text-align: right;
        }
        
        #wjs_btn1{
        	width: 120px;
        	height: 50px;
        	outline: 0;
        	 background: #4876EF;
    		border: 0;
   			 outline: 0;
   			 color: white;
   			 font-size: 20px;
   			 font-weight: bold;  			 
   			 border-radius: 5px;
   			 margin-left: 280px;
        	
        }
        #wjs_btn2{
        	width: 120px;
        	height: 50px;
        	outline: 0;
        	 background: rgb(167, 167, 167);
    		border: 0;
   			 outline: 0;
   			 color: white;
   			 font-size: 20px;
   			 font-weight: bold;  			 
   			 border-radius: 5px;
   			 margin-bottom: 5px;
   			 
        }
        td{
        	font-weight: bold;
        }
        #mes{
			margin-left: 210px;
			margin-top: 40px;
			width: 100px;	
			height: 30px;
			margin-left: 210px;
			margin-top: 40px;
			width: 100px;	
			height: 30px;
        	width:90px;
        	height: 30px;
        	background: #4876EF;
        	color:white;
        	font-weight: bold;
        	margin-top: 50px;
        	outline: 0;
    }
        }
        #wjs_side{
        	font-size: 14px;
        	font-weight: bold;
        	
        }
    </style>
</head>
<body>
<%@ include file="../../common/jyh_adminMenubar.jsp" %>
<table id="wjs_outline-table">
		<tr>
			<td id="wjs_nav">
				<div id=wjs_nav-div>
					<div id="wjs_side-menu-outline">
						<div id="wjs_side-menu-per">
							<h3 style="font-size: 18px; font-weight: bold" onclick="location.href='<%=request.getContextPath() %>/approval.com'">기업회원 승인관리</h3>
							<hr
								style="margin-right: auto; margin-left: auto; margin-top: 5px;">

						</div>
						<div id="wjs_side-menu-per">
							<h3 onclick="location.href='<%=request.getContextPath() %>/approval.re'" id="wjs_side" style="font-size: 18px;font-weight: bold;">채용 공고 승인관리</h3>
							<hr
								style="margin-right: auto; margin-left: auto; margin-top: 5px;">
						</div>
						<div id="wjs_side-menu-per">
							<h3 onclick="location.href='<%=request.getContextPath() %>/approval.re2'" style="font-size: 18px;font-weight: bold;">공모전 승인관리</h3>
							<hr
								style="margin-right: auto; margin-left: auto; margin-top: 5px;">

						</div>
					</div>
				</div>
			</td>
    <td align="top" style="vertical-align: top;">
            <div class="wjs_article1"></div>
            <div class="wjs_article2">
                <p id="wjs_search-per">기업가입 승인관리</p>
            </div>
            <hr style="margin-bottom:0;">
            <div id="section">
            <br><br>
            	<table id="wjs_tb">
            		<tr>
            			<td id="wjs_td">아이디</td>
            			<td id="wjs_td2"><%=cm.getComId() %></td>
            		</tr>
            		<tr>
            			<td id="wjs_td">회사명</td>
            			<td id="wjs_td2"><%=cm.getCompanyName() %></td>
            		</tr>
            		<tr>
            			<td id="wjs_td">이메일</td>
            			<td id="wjs_td2"><%=cm.getEmail() %></td>
            		</tr>
            		<tr>
            			<td id="wjs_td">휴대전화</td>
            			<td id="wjs_td2"><%=cm.getPhone() %></td>
            		</tr>
            		<tr>
            			<td id="wjs_td">가입일</td>
            			<td id="wjs_td2"><%=cm.getEnrollDate() %></td>
            		</tr>
            		<!-- <tr>
            			<td id="wjs_td">가입상태</td>
            			<td id="wjs_td2">가입상태</td>
            		</tr> -->
            		<tr>
            			<td id="wjs_td">정보보유기간</td>
            			<td id="wjs_td2"><%=cm.getInfoPeriod() %></td>
            		</tr>
            		<tr>
            			<td id="wjs_td">사업자등록번호</td>
            			<td id="wjs_td2"><%=cm.getComNum() %></td>
            		</tr>
            		<%-- <tr>
            			<td id="wjs_td">사원수</td>
            			<td id="wjs_td2"><%=cm.getNumEmp() %></td>
            		</tr>
            		<tr>
            			<td id="wjs_td">투자금액</td>
            			<td id="wjs_td2"><%=cm.getInvest() %></td>
            		</tr>
            		<tr>
            			<td id="wjs_td">매출액</td>
            			<td id="wjs_td2"><%=cm.getSales() %></td>
            		</tr> --%>
            	
            	</table>
            	
            	<!-- <div>
            		<img src="" style="border: 1px solid black; width: 300px;height: 250px; float: left; margin-left: 80px;">
            	</div> -->
            	<br>


<div class="modal fade bd-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="margin-right: 90px; margin-top: 100px;">
  <div class="modal-dialog modal-sm">
    <div class="modal-content" style="width: 500px;height: 300px; margin-right: 100px;">
    <form id="wjs_form" action="<%=request.getContextPath()%>/refuseCm.ad">
      <h4 style="margin-left: 10px; margin-top: 30px;">반려 사유 작성</h4>
      <hr>
      <p style="margin-left: 10px;">문의내용 : </p>
      <textarea id="textarea" name="textarea" rows="4" cols="55" style="text-align: center; margin-left: 10px; resize: none;"></textarea>
      <input type="hidden" id="num" name="num" value="<%=cm.getComNo() %>">
      <button id="mes">답변전송</button>
    </form>
    </div>
  </div>
</div>
            
            </div>
            	
            	<button id="wjs_btn1">가입승인</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            	<button type="button" id="wjs_btn2" class="btn btn-primary" data-toggle="modal" data-target=".bd-example-modal-sm">반려</button>
      
    </td>
    <td id="wjs_aside">
    </td>
    </tr>
</table>
    
    
    <%@ include file="../../common/cho_footer.jsp" %>

</body>
<script>
	var num = '<%=cm.getComNo()%>';
	$("#wjs_btn1").click(function(){
		location.href="<%=request.getContextPath()%>/approvalCm.ad?num="+num;
	});
	
	$("#mes").click(function(){
		<%-- location.href="<%=request.getContextPath()%>/refuseCm.ad?num="+num; --%>
		$("#wjs_form").submit();
	});

</script>
</html>