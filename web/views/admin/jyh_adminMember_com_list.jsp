<!-- 기업회원 리스트 페이지 입니다. -->
<%@ page language="java" contentType="text/html; charset=UTF-8"
pageEncoding="UTF-8" import="java.util.*, com.kh.itda.member.model.vo.*, com.kh.itda.approval.model.vo.*"%>
<%
	ArrayList<Jyh_ComMember> list = (ArrayList<Jyh_ComMember>) request.getAttribute("list");
	PageInfo pi = (PageInfo) request.getAttribute("pi");
	String pagingUrl = (String) request.getAttribute("pagingUrl");
	int listCount = pi.getListCount();
	int currentPage = pi.getCurrentPage();
	int maxPage = pi.getMaxPage();
	int startPage = pi.getStartPage();
	int endPage = pi.getEndPage();
	int test = 0;
%>
<!DOCTYPE html>
<html lang="ko">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <style>
        a:link { 
            text-decoration: none;
            color: black;
        }
        a:visited { 
            text-decoration: none;
            color: black;
        }
        .jyh_article1{
            width:840px; 
            height:75px;
        }
        .jyh_article2{
            width:840px; 
            height:35px;
            text-align: center;
        }
        #jyh_nav{
            width:280px;
        }
        #jyh_aside{
            width:280px;
        }
        footer{
            margin-left: auto;
            margin-right: auto;
            width:1400px;
            height:100px;
        }
        #section{
            height:1000px;
        }

        #jyh_outline-table{
            margin:auto; 
            border-collapse:collapse; 
            border-spacing:0;
        }
        #jyh_outline-table td:nth-of-type(1n){
            margin:0;
            padding:0;
        }

        #jyh_search-per{
            padding:5px;
            text-align: left;
            font-weight:bold;
            font-size: 15pt;
        }
        #jyh_page-descript-per{
            width:100%;
            height:100px;
        }
        #jyh_page-descript-per ul{
            margin:0;
            width:85%;
        }
        #jyh_page-descript-per p:nth-of-type(1n){
            margin:8px;
        }
        #jyh_modal_open_btn{
            width:85px;
            height:30px;
            float:left;
            font-weight:500;
            font-size: 12pt;
            border-radius: 5px;
            border:0;
            outline:0;
            margin-top:8px;
        }
        #jyh_modal{
            width:840px;
            display: none;
        }

        #jyh_modal {
            position:relative;
            width:100%;
            height:100%;
            z-index:1;
            }

        #jyh_modal h2 {
            margin:0;   
        }


        #jyh_modal .jyh_modal_content {
            width:500px;
            height:330px;
            margin:100px auto;
            padding:20px 10px;
            background:#fff;
            border:2px solid #666;
        } 
        .jyh_modal_content div:first-child{
            width:100%; 
            height:32px;
        }
        #jyh_modal .jyh_modal_layer {
            position:fixed;
            top:0;
            left:0;
            width:100%;
            height:100%;
            background:rgba(0, 0, 0, 0.5);
            z-index:-1;
        }  
        #jyh_word-list > table{
            margin:auto;
        }
        #jyh_word-list p:nth-of-type(1n){
            padding:10px;
            margin:0;
        }
        #jyh_modal_close_btn, #jyh_modal_next_btn, #jyh_modal_back_btn{
            width:85px;
            height:30px;
            float:right;
            font-weight:500;
            font-size: 12pt;
            border-radius: 5px;
            border:0;
            outline:0;
            margin-top:2px;
        }
        #jyh_per-list {
            border-collapse: collapse;
            border: 1px solid #168;
            width:100%;
        }  
        #jyh_per-list th {
            color: #168;
            background: #f0f6f9;
            text-align: center;
        }
        #jyh_per-list th, #jyh_per-list td {
            padding: 10px;
            /* border: 1px solid #ddd; */
        }
        #jyh_per-list th:first-child, #jyh_per-list td:first-child {
            border-left: 0;
        }
        #jyh_per-list th:last-child, #jyh_per-list td:last-child {
            border-right: 0;
        }
        #jyh_per-list tr td:first-child{
            text-align: center;
        }
        .jyh_search-filter{
            border:1px solid gray;
            border-radius: 5px;
        }
        .jyh_search-select{
            height:24px;
        }
        .jyh_search-line, #search{
            vertical-align: middle;
            margin-left:5px;
        }
        .jyh_search-radius{
            border-radius: 5px;
            border: 1px solid gray;
        }
        #search{
            width:200px;
            height:20px;
        }
        #jyh_per-list tr{
        	text-align:center;
        }
        .hide{
        	display:none;
        }
        .join-date{
        	color:gray;
        }
    </style>
</head>
<body>
    <%@ include file="../common/jyh_adminMenubar.jsp" %>
    
<table id="jyh_outline-table">
    <tr>
        <td id="jyh_nav">
            <%@ include file="../common/jyh_adminMemManageSideMenu.jsp" %>
    </td>
    <td align="top">
            <div class="jyh_article1"></div>
            <div class="jyh_article2">
                <p id="jyh_search-per">기업회원 검색</p>
            </div>
            <hr style="margin-bottom:0;">
            <div id="section">
        <div id="jyh_page-descript-per">
            <ul id="jyh_page-descript-per1" style="float:left;">
                    <li><p>전체 기업 회원을 조회할 수 있는 페이지 입니다.</p></li>
                    <li><p>탈퇴한 유저의 정보 중 개인정보 보유기간을 확인하여 삭제해야 합니다.</p></li>
                    <li><p>부적절한 단어가 포함되어 있는지 확인하여 관리해야 합니다.</p></li>
                </ul>
        </div>

                <form id="searchRequirement" action="<%= request.getContextPath() %>/jyhSearchPerList.me" method="post">
                <div id="jyh_searchbar-outline">
                    <div id="jyh_search_filter" style="float: right; margin-bottom:10px;">
                        <span class="jyh_search-line"><label>가입일</label></span>
                        <input id="allDay" type="radio" name="dateCondition" checked value="TRUNC(SYSDATE) - ENROLL_DATE >= '0' " class="jyh_search-radius hide">
                        <label class="jyh_search-radius jyh_search-line join-date" for="allDay">전체</label>
                        <input id="oneWeek" type="radio" name="dateCondition" value="TRUNC(SYSDATE) - ENROLL_DATE <= '7' " class="jyh_search-radius hide">
                        <label class="jyh_search-radius jyh_search-line join-date" for="oneWeek">1주일 이내</label>
                        <input id="oneMonth" type="radio" name="dateCondition" value="TRUNC(SYSDATE) - ENROLL_DATE <= '30' " class="jyh_search-radius hide">
                        <label class="jyh_search-radius jyh_search-line join-date" for="oneMonth">1개월 이내</label>
                        <input id="sixMonth" type="radio" name="dateCondition" value="TRUNC(SYSDATE) - ENROLL_DATE <= '180' " class="jyh_search-radius hide">
                        <label class="jyh_search-radius jyh_search-line join-date" for="sixMonth">6개월 이내</label>
                        
                        <input id="selectCondition" type="text" name="selectCondition" value="" class="hide">
                        <input id="memType" type="text" name="memType" value="2" class="hide">
                        
                        <select id="select" class="jyh_search-line jyh_search-select jyh_search-radius">
                            <option value=" '%">전체검색</option>
                            <option value="COM_ID LIKE '%">아이디</option>
                            <option value="COM_NAME LIKE '%">이름</option>
                            <option value="EMAIL LIKE '%">이메일</option>
                            <option value="PHONE LIKE '%">휴대전화</option>
                        </select>
                        <input type="text" class="jyh_search-radius" id="search" maxlength="50">
                        <input id="confirmBtn" type="button" name="confirmBtn" value="검색" class="jyh_search-radius">
                    </div>
                </div>
			</form>

                <table id="jyh_per-list" border="1" align="center">
                    <tr>
                        <th><input type="checkbox" name="jyh_per-list-check"></th>
                        <th>회사명</th>
                        <th>아이디</th>
                        <th>사업자번호</th>
                        <th>담당자이름</th>
                        <th>담당휴대전화</th>
                        <th>가입일</th>
                        <th>가입상태</th>
                    </tr>
                    <% for(Jyh_ComMember cm : list) { %>
					<tr>
						<td><input type="checkbox" name="jyh_per-list-check"></td>
						<td style="display:none;"><%= cm.getComNo() %></td>
						<td><%= cm.getCompanyName() %></td>
						<td><%= cm.getComId() %></td>
						<td><%= cm.getComNum() %></td>
						<td><%= cm.getComName() %></td>
						<td><%= cm.getPhone() %></td>
						<td><%= cm.getEnrollDate() %></td>
						<td><%= cm.getStatus() %></td>
					</tr>
					<% } %>
                </table>
                <hr style="margin-right:auto; margin-left:auto; margin-top:10px;">

        <%-- 페이지 처리 --%>
      <div class="pagingArea" align="center">
        <button onclick="location.href='<%=request.getContextPath()%><%= pagingUrl %>currentPage=1'"><<</button>
        <% if(currentPage <= 1) { %>
        <button disabled><</button>
        <% } else { %>
         <button onclick="location.href='<%=request.getContextPath()%><%= pagingUrl %>currentPage=<%=currentPage - 1%>'"><</button>
        <% } %>
        
        <% for(int p = startPage; p <= endPage; p++) { 
        	  if(p == currentPage){
        %>
        		<button disabled><%= p %></button>
        <%
        	  } else {
        %>
        		<button onclick="location.href='<%=request.getContextPath()%><%= pagingUrl %>currentPage=<%=p%>'"><%= p %></button>
        <% 
        	  }
           }
        %>
      
      	
      	<% if(currentPage >= maxPage) { %>
      	<button disabled>></button>
      	<% } else { %>
      	<button onclick="location.href='<%=request.getContextPath()%><%= pagingUrl %>currentPage=<%=currentPage + 1%>'">></button>
      	<% } %>
      	
      	<button onclick="location.href='<%=request.getContextPath()%><%= pagingUrl %>currentPage=<%=maxPage%>'">>></button>
      </div>
            </div>
                <hr style="margin-right:auto; margin-left:auto; margin-top:10px;">
    </td>
    <td id="jyh_aside">
    </td>
    </tr>
</table>
    
    
    <%@ include file="../common/cho_footer.jsp" %>
    
    <script>
            $("#jyh_modal_open_btn").click(function(){
                $("#jyh_modal").attr("style", "display:block");
            });
           
             $("#jyh_modal_close_btn").click(function(){
                $("#jyh_modal").attr("style", "display:none");
            }); 
            
            $("#jyh_per-list tr:not(:first-child)").click(function(){
             	userNo = $(this).children("td:nth-child(2)").text();
             	console.log(userNo);
             		
             	location.href = "<%=request.getContextPath()%>/jyhSelectOne.me?userNo=" + userNo + "&memType=2";
             		
            });
        </script>
        
        <script>
    	$(function(){
    		$("#confirmBtn").click(function(){
    			
    			$("#selectCondition").val($("#select option:selected").val() + $("#search").val() +"%' ");
    			
    			$("#searchRequirement").submit();
    		});
    	});
    </script>
   
</body>
</html>