<!-- 기업회원 결제내역 페이지 입니다.  -->

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.util.*, com.kh.itda.notice.payAndProduct.model.vo.*"%>
<%
	ArrayList<Sb_Payment> list = (ArrayList<Sb_Payment>) request.getAttribute("list");
	Sb_PageInfo pi =  (Sb_PageInfo) request.getAttribute("pi");
	int listCount = pi.getListCount();
	int currentPage = pi.getCurrentPage();
	int maxPage = pi.getMaxPage();
	int startPage = pi.getStartPage();
	int endPage = pi.getEndPage();
%>
<!DOCTYPE html>
<html lang="ko">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <style>
        a:link { 
            text-decoration: none;
            color: black;
        }
        a:visited { 
            text-decoration: none;
            color: black;
        }
        .jyh_article1{
            width:840px; 
            height:75px;
        }
        .jyh_article2{
            width:840px; 
            height:35px;
            text-align: center;
        }
        #jyh_nav{
            width:280px;
        }
        #section{
            /* height: */
        }

        #jyh_aside{
            width:280px;
        }
        footer{
            margin-left: auto;
            margin-right: auto;
            width:1400px;
            height:100px;
        }

        #jyh_outline-table{
            margin:auto; 
            border-collapse:collapse; 
            border-spacing:0;
        }
        #jyh_outline-table td, jyh_detail-info td{
            padding:0px;
        }

        #jyh_per-top-btn{
            float: left;
            margin-right:50px;
            margin-left:10px;
        }

        #jyh_page-descript-per{
            width:100%;
            height:100px;
        }
        #jyh_page-descript-per ul{
            margin:0;
            width:85%;
        }
        #jyh_page-descript-per p:nth-of-type(1n){
            margin:8px;
        }
        #jyh_detail-info{
            margin:auto; 
            border-collapse:collapse; 
            border-spacing:0;
            width:100%;
        }
        .jyh_search-filter{
            border:1px solid gray;
            border-radius: 5px;
        }
        .jyh_search-select{
            height:24px;
        }
        .jyh_search-line, #search, #jyh_modify-release-btn{
            vertical-align: middle;
            margin-left:5px;
        }
        .jyh_search-radius, #jyh_modify-release-btn{
            border-radius: 5px;
            border: 1px solid gray;
        }
        #search{
            width:200px;
            height:20px;
        }
    </style>
</head>
<body>

    <%@ include file="../common/jyh_adminMenubar.jsp" %>

<table id="jyh_outline-table">
    <tr>
        <td id="jyh_nav">
                <div class="jyh_article1" style="width:100%; vertical-align:top;" ></div>
            <%@ include file="../common/jyh_adminMemManageSideMenu.jsp" %>
    </td>
    <td style="width:840px; vertical-align:top;">
            <div class="jyh_article1" ></div>
            <div class="jyh_article2">
            <!--     <sapn id="jyh_per-top-btn">회원상세정보</sapn>
                <sapn id="jyh_per-top-btn">공고등록이력</sapn>
                <sapn id="jyh_per-top-btn">결제내역</sapn> -->
            </div>
            <hr style="margin-bottom:0;">
            <div id="jyh_page-title-detail" style="background:rgb(177, 174, 174)">
                    <h3 style="margin-top:0px">기업목록결제관리</h3>
                </div>
                <hr style="margin-bottom:10px;">

                <div id="jyh_page-descript-per">
                        <ul id="jyh_page-descript-per1" style="float:left;">
                                <li><p>기업의 모든 결재내역을 볼수있는 페이지입니다.</p></li>
                                <li><p>결제내역 영수증은 가입한 이메일로 발송합니다.</p></li>
                                <li><p>결제취소는 최대 결제후 7일이전까지만 가능합니다.</p></li>
                            </ul>
                        <button type="button" id="jyh_modal_open_btn">단어확인</button>
                    </div>
                    <div id="jyh_searchbar-outline">
                        <div id="jyh_search_filter" style="float: right; margin-bottom:10px;">
                            <span class="jyh_search-line"><label>가입일</label></span>
                            <span class="jyh_search-line jyh_search-filter jyh_search-radius">1주일이내</span>
                            <span class="jyh_search-line jyh_search-filter jyh_search-radius">1개월이내</span>
                            <span class="jyh_search-line jyh_search-filter jyh_search-radius">6개월이내</span>
                            <select class="jyh_search-line jyh_search-select jyh_search-radius">
                                <option>전체검색</option>
                                <option>전체검색1</option>
                                <option>전체검색2</option>
                                <option>전체검색3</option>
                            </select>
                            <input type="text" class="jyh_search-radius" id="search" maxlength="50">
                            <button class="jyh_search-radius">확인</button>
                        </div>
                    </div>
                    <div id="jyh_modify-release-btn-outline" style="width:100%; float:left; margin-bottom:10px;">
                        <input type="button" id="jyh_modify-release-btn" value="결제취소" style="float:right;"></input>
                        <input type="button" id="jyh_modify-release-btn" value="결제완료" style="float:right;"></input>
                        <input type="button" id="jyh_modify-release-btn" value="모든결제" style="float:right;"></input>
                </div>
                    <hr style="margin-bottom:10px; width:100%;">
                <div id="section1">
                    <div id="jyh_detail-outline" style="margin-bottom:10px; float:left; width:100%; height:100%; background:rgb(242, 242, 242);">
                    <div id="jyh_detail-table-outline1" style="float:left; width:100%;">
                    <table id="jyh_detail-info" border="1" style="width:100%;">
                            <tr style="background:rgb(102, 140, 255);">
                                    <th><input type="checkbox" name="jyh_per-list-check"></th>
                                    <th>번호</th>
                                    <th>결제상품명</th>
                                    <th>회사명</th>
                                    <th>결제일시</th>
                                    <th>결제금액</th>
                                    <th>결제상태</th>
                                </tr>
                                <tr>
                                 <%for(Sb_Payment pay : list) { %>
                                    <td><input type="checkbox" name="jyh_per-list-check"></td>
                                    <td><%=pay.getPy_num() %></td>
                                    <td><%=pay.getPd_name() %></td>
                                    <td>선빈회사1</td>
                                    <td><%=pay.getPd_date() %></td>
                                    <td><%=pay.getPd_price() %></td>
                                    <td class="division"><%=pay.getPy_division()%></td>
                                </tr>
                                <% } %> 
                    </table>
               
					
                    </div>
                    
            </div>
            <div class="pagingArea" align="center">
			<button onclick="location.href='<%=request.getContextPath()%>/adminPayList.pay?currentPage=1'"><<</button>
			
			<%if(currentPage <= 1) { %>
			<button disabled><</button>
			<% } else  { %>
			<button onclick="location.href='<%=request.getContextPath() %>/adminPayList.pay?currentPage=<%=currentPage -1 %>'"><</button>
			<% } %>
			
			<% for(int p = startPage; p <= endPage; p++) { 
				if(p == currentPage) {
			%>
				<button disabled><%= p %></button>
			<% 
				} else {
			%>		
				<button onclick="location.href='<%=request.getContextPath()%>/adminPayList.pay?currentPage=<%=p %>'"><%=p %></button>
			<%
				}
			}	
			%>
			
			<% if(currentPage >= maxPage) {%>
      		<button disabled>></button>
      		<% } else { %>
      		<button onclick="location.href='<%=request.getContextPath()%>/adminPayList.pay?currentPage=<%=currentPage + 1%>'">></button>
      		<% } %>
      	
      		<button onclick="location.href='<%=request.getContextPath()%>/adminPayList.pay?currentPage=<%=maxPage%>'">>></button>
		</div>
            
            </div>
        </td>
        
    <td id="jyh_aside">
    </td>
    </tr>
    <tr>
    	
    </tr>
</table>
    <%@ include file="../common/cho_footer.jsp" %>
</body>
<script>
	$(function (){
		$(".division").click(function (){
			var thi = $(this).text();
			console.log(thi);
			var payno = $(this).parent().children().eq(1).text();
			console.log(payno);
			if(thi == "취소진행중") {
				var check = confirm("해당 결제를 취소 하시겠습니까?");
				console.log("들ㅇ옴")
				$.ajax({
					url : "updateadminList.pay",
					data:{payno : payno},
					type : "get",
					success : function(data) {
						console.log("성공들어옴");
						location.reload();
						alert("결제가 취소되었습니다.");
					},
					error: function(request, status, error) {
						console.log("서버 전송 실패!");
						console.log(request);
						console.log("status : " + status);
						console.log("error: " + error);
						
					},
					complete : function() {
						console.log("무조건 호출되는 함수");
					}
				}); 
			}
		});
	});
	
</script>
</html>