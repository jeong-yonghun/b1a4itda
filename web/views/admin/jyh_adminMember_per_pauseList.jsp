<!-- 개인회원 제제이력 리스트 페이지 입니다. -->
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"
	import="java.util.ArrayList, java.util.HashMap, com.kh.itda.approval.model.vo.*"%>
<%
	//줄이 길어서 개행은 연산자 앞에서 (ex =앞에서)하도록하고 띄어쓰기는 윗 줄에서 8칸 띄운다.
	//형변환 후 한 칸 띄운다.
	ArrayList<HashMap<String, Object>> list = (ArrayList<HashMap<String, Object>>) request.getAttribute("list");
	PageInfo pi = (PageInfo) request.getAttribute("pi");
	int listCount = pi.getListCount();
	int currentPage = pi.getCurrentPage();
	int maxPage = pi.getMaxPage();
	int startPage = pi.getStartPage();
	int endPage = pi.getEndPage();
%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="UTF-8">
<title>Document</title>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<style>
a:link {
	text-decoration: none;
	color: black;
}

a:visited {
	text-decoration: none;
	color: black;
}

.jyh_article1 {
	width: 840px;
	height: 75px;
}

.jyh_article2 {
	width: 840px;
	height: 35px;
	text-align: center;
}

#jyh_nav {
	width: 280px;
}


#jyh_aside {
	width: 280px;
}

footer {
	margin-left: auto;
	margin-right: auto;
	width: 1400px;
	height: 100px;
}

#section {
	height: 1000px;
}

#jyh_outline-table {
	margin: auto;
	border-collapse: collapse;
	border-spacing: 0;
}

#jyh_outline-table td:nth-of-type(1n) {
	margin: 0;
	padding: 0;
}

#jyh_search-per {
	padding: 5px;
	text-align: left;
	font-weight: bold;
	font-size: 15pt;
}

#jyh_page-descript-per {
	width: 100%;
	height: 100px;
}

#jyh_page-descript-per ul {
	margin: 0;
	width: 85%;
}

#jyh_page-descript-per p:nth-of-type(1n) {
	margin: 8px;
}

#jyh_modal_open_btn {
	width: 85px;
	height: 30px;
	float: left;
	font-weight: 500;
	font-size: 12pt;
	border-radius: 5px;
	border: 0;
	outline: 0;
	margin-top: 8px;
}

#jyh_modal {
	width: 840px;
	display: none;
}

#jyh_modal {
	position: relative;
	width: 100%;
	height: 100%;
	z-index: 1;
}

#jyh_modal h2 {
	margin: 0;
}

#jyh_modal .jyh_modal_content {
	width: 500px;
	height: 330px;
	margin: 100px auto;
	padding: 20px 10px;
	background: #fff;
	border: 2px solid #666;
}

.jyh_modal_content div:first-child {
	width: 100%;
	height: 32px;
}

#jyh_modal .jyh_modal_layer {
	position: fixed;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	background: rgba(0, 0, 0, 0.5);
	z-index: -1;
}

#jyh_word-list>table {
	margin: auto;
}

#jyh_word-list p:nth-of-type(1n) {
	padding: 10px;
	margin: 0;
}

#jyh_modal_close_btn, #jyh_modal_next_btn, #jyh_modal_back_btn {
	width: 85px;
	height: 30px;
	float: right;
	font-weight: 500;
	font-size: 12pt;
	border-radius: 5px;
	border: 0;
	outline: 0;
	margin-top: 2px;
}

#jyh_per-pauseList {
	border-collapse: collapse;
	border: 1px solid #168;
	width: 100%;
}

#jyh_per-pauseList th {
	color: #168;
	background: #f0f6f9;
	text-align: center;
}

#jyh_per-pauseList th, #jyh_per-pauseList td {
	padding: 10px;
	/* border: 1px solid #ddd; */
}

#jyh_per-pauseList th:first-child, #jyh_per-pauseList td:first-child {
	border-left: 0;
}

#jyh_per-pauseList th:last-child, #jyh_per-pauseList td:last-child {
	border-right: 0;
}

#jyh_per-pauseList tr td:first-child {
	text-align: center;
}

.jyh_search-filter {
	border: 1px solid gray;
	border-radius: 5px;
}

.jyh_search-select {
	height: 24px;
}

.jyh_search-line, #search, #jyh_modify-release-btn {
	vertical-align: middle;
	margin-left: 5px;
}

.jyh_search-radius, #jyh_modify-release-btn {
	border-radius: 5px;
	border: 1px solid gray;
}

#search {
	width: 200px;
	height: 20px;
}

#jyh_per-pauseList tr td{
    text-align: center;
}
</style>
</head>
<body>

	<%@ include file="../common/jyh_adminMenubar.jsp"%>

	<table id="jyh_outline-table">
		<tr>
			<td id="jyh_nav">
				<%@ include file="../common/jyh_adminMemManageSideMenu.jsp" %>
			</td>
			<td align="top">
				<div class="jyh_article1"></div>
				<div class="jyh_article2">
					<p id="jyh_search-per">정지회원 관리</p>
				</div>
				<hr style="margin-bottom: 0;">
				<div id="section">
					<div id="jyh_page-descript-per">
						<ul id="jyh_page-descript-per1" style="float: left;">
							<li><p>신고의 의해 정지를 당한 개인유저들의 리스트입니다.</p></li>
							<li><p>악의적 신고에대해서는 관리자 판단하에 수정이 가능합니다.</p></li>
							<li><p>개인유저에게는 정지만 할수있습니다.</p></li>
						</ul>
					</div>

					<div id="jyh_searchbar-outline">
						<div id="jyh_search_filter"
							style="float: right; margin-bottom: 10px;">
							<span class="jyh_search-line"><label>가입일</label></span> <span
								class="jyh_search-line jyh_search-filter jyh_search-radius">1주일이내</span>
							<span class="jyh_search-line jyh_search-filter jyh_search-radius">1개월이내</span>
							<span class="jyh_search-line jyh_search-filter jyh_search-radius">6개월이내</span>
							<select
								class="jyh_search-line jyh_search-select jyh_search-radius">
								<option>전체검색</option>
								<option>전체검색1</option>
								<option>전체검색2</option>
								<option>전체검색3</option>
							</select> <input type="text" class="jyh_search-radius" id="search"
								maxlength="50"> <input type="button"
								class="jyh_search-radius" value="확인"></input>
						</div>
					</div>
					<br>

					<table id="jyh_per-pauseList" border="1" align="center">
						<tr>
							<th><input type="checkbox" name="jyh_per-pauseList-check"></th>
							<th>번호</th>
							<th>아이디</th>
							<th>이름</th>
							<th>이메일</th>
							<th>정지사유</th>
							<th>정지일</th>
							<th>남은 일수</th>
						</tr>
						<%	if(list.size() > 0){
							for (int i = 0; i < list.size(); i++) {
								HashMap<String, Object> hmap = list.get(i);
								System.out.println("listsize : " + list.size());
								System.out.println("list.get() : " + list.get(i));
								System.out.println("hmap : " + hmap);
						%>
						<tr> 
							<td><input type="checkbox" name="jyh_per-pauseList-check"></td>
							<td><%=hmap.get("rnum")%> <input type="text" name="userNo" value="<%=hmap.get("perNo")%>" style="display:none;"></td>
							<td><%=hmap.get("perId")%></td>
							<td><%=hmap.get("perName")%></td>
							<td><%=hmap.get("email")%></td>
							<td><%=hmap.get("memReason")%></td>
							<td><%=hmap.get("memChangeDate")%></td>
							<td><%=hmap.get("releaseDate")%></td>
						</tr>
						<%
						}}else {%>
							<tr>
                            	<td colspan="8">정지 회원 리스트가 없습니다.</td>
                            </tr>
						
						<%}
					%>
					</table>
					<hr
						style="margin-right: auto; margin-left: auto; margin-top: 10px;">

					      <%-- 페이지 처리 --%>
      <div class="pagingArea" align="center">
        <button onclick="location.href='<%=request.getContextPath()%>/jyhPauseList.me?currentPage=1'"><<</button>
        <% if(currentPage <= 1) { %>
        <button disabled><</button>
        <% } else { %>
        <button onclick="location.href='<%=request.getContextPath()%>/jyhPauseList.me?currentPage=<%=currentPage - 1%>'"><</button>
        <% } %>
        
        <% for(int p = startPage; p <= endPage; p++) { 
        	  if(p == currentPage){
        %>
        		<button disabled><%= p %></button>
        <%
        	  } else {
        %>
        		<button onclick="location.href='<%=request.getContextPath()%>/jyhPauseList.me?currentPage=<%=p%>'"><%= p %></button>
        <% 
        	  }
           }
        %>
      
      	
      	<% if(currentPage >= maxPage) { %>
      	<button disabled>></button>
      	<% } else { %>
      	<button onclick="location.href='<%=request.getContextPath()%>/jyhPauseList.me?currentPage=<%=currentPage + 1%>'">></button>
      	<% } %>
      	
      	<button onclick="location.href='<%=request.getContextPath()%>/jyhPauseList.me?currentPage=<%=maxPage%>'">>></button>
      </div>
				</div>
				<hr style="margin-right: auto; margin-left: auto; margin-top: 10px;">
			</td>
			<td id="jyh_aside"></td>
		</tr>
	</table>

	<%@ include file="../common/cho_footer.jsp"%>
	<script>
		$("#jyh_modal_open_btn").click(function() {
			$("#jyh_modal").attr("style", "display:block");
		});

		$("#jyh_modal_close_btn").click(function() {
			$("#jyh_modal").attr("style", "display:none");
		});
	</script>

</body>
</html>