<!-- 기업회원 상세보기 페이지 입니다. -->
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="com.kh.itda.member.model.vo.Jyh_ComMember"%>
<%
	Jyh_ComMember comMember = (Jyh_ComMember) request.getAttribute("comMember");
%>
<!DOCTYPE html>
<html lang="ko">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <style>
        a:link { 
            text-decoration: none;
            color: black;
        }
        a:visited { 
            text-decoration: none;
            color: black;
        }
        .jyh_article1{
            width:840px; 
            height:75px;
        }
        .jyh_article2{
            width:840px; 
            height:35px;
            text-align: center;
        }
        #jyh_nav{
            width:280px;
            height:1000px;
        }
        #jyh_aside{
            width:280px;
        }
        footer{
            margin-left: auto;
            margin-right: auto;
            width:1400px;
            height:100px;
        }

        #jyh_outline-table, #td2 table{
            margin:auto; 
            border-collapse:collapse; 
            border-spacing:0;
            padding:0px;
        }
        #jyh_outline-table td{
            padding:0px;
        }

        .jyh_com-top-btn{
            float: left;
            margin-right:50px;
            margin-left:10px;
        }
        #jyh_detail-info tr td{
        	/* padding:2.5px 10px 2.5px 10px; */
        	padding:10px 10px 10px 10px;
        }
    </style>
</head>
<body>

    <%@ include file="../common/jyh_adminMenubar.jsp" %>

<table id="jyh_outline-table">
    <tr>
        <td id="jyh_nav" style="vertical-align:top;">
                <div class="jyh_article1" style="width:100%; vertical-align:top;" ></div>
            <%@ include file="../common/jyh_adminMemManageSideMenu.jsp" %>
    </td>
    <td id="td2" style="width:840px; vertical-align:top;">
            <div class="jyh_article1"></div>
            <div class="jyh_article2">
                <div id="jyh_com-top-btn1" class="jyh_com-top-btn">회원상세정보<hr style="background:#5A84F1; margin-top:2px;"></div>
                <div id="jyh_com-top-btn2" class="jyh_com-top-btn">채용지원</div>
                <div id="jyh_com-top-btn3" class="jyh_com-top-btn">결제내역</div>
            </div>
            <hr style="background:gray;">
                <div id="jyh_detail-outline" style="margin-bottom:10px; float:left; width:100%; height:100%; ">
                    <div id="jyh_detail-table-outline1" style="float:left; width:60%; ">
                    <table id="jyh_detail-info" style="width:100%;">
                        <tr>
                            <td>아이디</td>
                            <td><span><%= comMember.getComId() %></span></td>
                        </tr>
                        <tr>
                            <td>회사명</td>
                            <td><span><%= comMember.getCompanyName() %></span></td>
                        </tr>
                        <tr>
                            <td>이메일</td>
                            <td><span><%= comMember.getEmail() %></span></td>
                        </tr>
                        <tr>
                            <td>담당자휴대전화</td>
                            <td><span><%= comMember.getComId() %></span></td>
                        </tr>
                        <tr>
                            <td>가입일</td>
                            <td><span><%= comMember.getEnrollDate() %></span></td>
                        </tr>
                        <% if(comMember.getStatus().equals("탈퇴")) {%>
                        <tr>
                            <td>탈퇴일</td>
                            <td><span><%= comMember.getModifyDate() %></span></td>  
                        </tr>
                        <%} else if(comMember.getStatus().equals("정지")) {%>
                        <tr>
                            <td>정지일</td>
                            <td><span><%= comMember.getModifyDate() %></span></td>  
                        </tr>
                        <%} %>
                        <tr>
                            <td>정보보유기간</td>
                            <td><span><%= comMember.getInfoPeriod() %></span></td>
                        </tr>
                        <tr>
                            <td>사업자등록번호</td>
                            <td><span><%= comMember.getComNum() %><span></td>
                        </tr>
                         <%
                         String[] str = comMember.getComAddress().split("&");
                         %>
                        <tr>
                            <td>주소</td>
                            <td><%for(int i = 0; i < str.length; i++){
                            if(i != str.length-2){%>
                            	<span><%=str[i]%><span><br>
                            <%}else{%>
                            	<span><%=str[i]%><span>
                            <%}}%></td>
                        </tr>
                         <% if(comMember.getInvest() != null) {%>
                        <tr>
                            <td>투자금액</td>
                            <td><span>투자금액데이터</span></td>
                        </tr>
                        <%} %>
                         <% if(comMember.getSales() != null) {%>
                        <tr>
                            <td>매출액</td>
                            <td><span><%= comMember.getSales() %></span></td>
                        </tr>
                        <%}%>
                        <tr>
                            <td>회원상태</td>
                            <% if(comMember.getStatus().equals("탈퇴")) {%>
                            <td><span><%= comMember.getStatus() %></span></td>
                            <%} else if(comMember.getStatus().equals("블랙")) {%>
                            <td><span><%= comMember.getStatus() %></span></td>
                            <%} else{%>
                            <td><span>승인</span></td>
                            <%} %>
                        </tr>
                    </table>
                    </div>
                    <div id="jyh_detail-table-photo-outline" style="float:right; width:40%; height:100%;">
                    <div id="jyh_detail-table-photo-inline" style="width:100%; height:200px;">
                        <img>
                    </div>
                </div>
            </div>
            
            <!-- </div> -->
        </td>
    <td id="jyh_aside">
    </td>
    </tr>
</table>
    
    
    <%@ include file="../common/cho_footer.jsp" %>
   
   
   <script>
    	$("#jyh_com-top-btn1").click(function(){
         	location.href = "<%=request.getContextPath()%>/jyhSelectOne.me?userNo=<%=comMember.getComNo()%>&memType=2";
        });
    	
    	$("#jyh_com-top-btn2").click(function(){
        	location.href = "<%=request.getContextPath()%>/jyhAnnList.me?userNo=<%=comMember.getComNo()%>";
        });
    	
    	$("#jyh_com-top-btn3").click(function(){
         	location.href = "<%=request.getContextPath()%>/jyhPaymentList.me?userNo=<%=comMember.getComNo()%>";
        });
    </script>
</body>
</html>