<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<style>
.adminMyPageTable {
	width: 1400px;
	margin-left:auto;
	margin-right:auto;
	margin-top:50px;
}

.table_nav {
	width: 180px;
}

.table_aside {
	width: 180px;
}

.cho_admin_section {
	display: inline-block;
	width: 485px;
	height: 300px;
	margin-left: 20px;
}

.cho_admin_section_table {
	width: 450px;
	margin-top: 30px;
	border: 1px solid #E1E1E1;
}

.cho_admin_section_title {
	background: #E1E1E1;
	color: #7A7A7A;
	width: 100%;
	height: 40px;
	text-align: left;
	font-size: 5spx;
	padding-left: 20px;
}

.memberManageDiv {
	border: 1px solid black;
	border-radius: 1pc;
	display: inline-block;
	width: 31%;
	margin-left: 3px;
}

.resultTable {
	margin-top: 30px;
	margin-bottom: 10px;
	margin-left: auto;
	margin-right: auto;
	width: 450px;
	border: 1px solid #E1E1E1;
}

.resultTable th {
	border-bottom: 1px solid black;
	background: rgb(116, 143, 211);
	color: whitesmoke;
}

.resultTable td {
	text-align: center;
}

.onerow_td {
	font-size: 20px;
	font-weight: 700;
	color: #2C64F6;
}
</style>
</head>
<body>
<%@ include file="../common/jyh_adminMenubar.jsp" %>
	<table class="adminMyPageTable">
		<tr>
			<!-- 첫째 줄 -->
			<td class="table_nav"></td>
			<td class="table_main">
				<div class="cho_admin_section" id="cho_admin_memberAd">
					<table class="cho_admin_section_table">
						<tr>
							<th class="cho_admin_section_title">회원현황</th>
						</tr>
						<tr>
							<td>
								<table class="resultTable">
									<tr>
										<th>개인신규가입</th>
										<th>개인탈퇴현황</th>
										<th>개인가입현황</th>
									</tr>
									<tr>
										<!-- 1달 기준 통계 -->
										<td class="onerow_td">
											<!-- 개인 신규 가입 -->
											 <h4 id="cho_adCount1">
											 1
											 </h4>
										</td>
										<td class="onerow_td">
											<!-- 개인 탈퇴 현황 --> 
											<h4 id="cho_adCount2">
											1</h4>
										</td>
										<td class="onerow_td">
											<!-- 현가입자현황 전체날짜 --> 
											<h4 id="cho_adCount3">
											23</h4>
										</td>
									</tr>
									<tr>
										<th>기업신규가입</th>
										<th>기업탈퇴현황</th>
										<th>기업가입현황</th>
									</tr>
									<tr>
										<td class="onerow_td">
											<!-- 기업신규가입 --> 
											<h4 id="cho_adCount4">1</h4>
										</td>
										<td class="onerow_td">
											<!-- 기업탈퇴현황 --> 
											<h4 id="cho_adCount5">1</h4>
										</td>
										<td class="onerow_td">
											<!-- 기업가입현황 전체날짜 --> 
											<h4 id="cho_adCount6">7</h4>
										</td>
									</tr>
								</table>
								<!-- ajax영역 -->
								<!-- <script type="text/javascript">
								$(function(){
								//기업회원수
								$.ajax({
					    			url: "adminComPerAll.co",
					    			type: "get",
					    			success: function(data){
					    				console.log(data);
					    				$("#cho_adCount6").text(data);
					    				
					    			},
					    			error: function(request){
					    				console.log(request);
					    			}
					    		});
								
								
								//개인 회원수
								$.ajax({
					    			url: "count1.do",
					    			type: "get",
					    			success: function(data){
					    				console.log(data);
					    				$("#cho_adCount3").text(data);
					    				
					    			},
					    			error: function(request){
					    				console.log(request);
					    			}
					    		});
							});
								</script> -->
							</td>
						</tr>
					</table>

				</div>
				<div class="cho_admin_section" id="cho_admin_paymentAd">
					<table class="cho_admin_section_table">
						<tr>
							<th class="cho_admin_section_title">결제현황&결제실적</th>
						</tr>
						<tr>
							<td>
								<table class="resultTable">
									<tr>
										<th>TODAY</th>
										<th>MONTH</th>
										<th>PAYMENT</th>
										<th>MONTHLY PAYMENT</th>
									</tr>
									<tr>
										<!-- 1달 기준 통계 -->
										<td>
											<!-- 오늘결제현황 --> 111,000
										</td>
										<td>
											<!-- 이번달결제현황 --> 22,000,000
										</td>
										<td class="onerow_td">
											<!-- 오늘결제실적 --> 22건
										</td>
										<td class="onerow_td">
											<!-- 한달결제실적 --> 33건
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</div>

			</td>
			<td class="table_aside"></td>
		</tr>
		<tr>
			<!-- 둘째 줄 -->
			<td class="table_nav"></td>
			<td class="table_main">
				<div class="cho_admin_section" id="cho_admin_notice">
					<table class="cho_admin_section_table">
						<tr>
							<th class="cho_admin_section_title">공지사항</th>
						</tr>
						<tr>
							<td>
								<table class="resultTable">
									<tr>
										<th>제목</th>
										<th>작성날짜</th>
									</tr>
									<!-- for문으로 공지사항이 있는 만큼 출력되게 하기 -->
									<tr>
										<td>asdfasdfasdfasdfsadfsd</td>
										<td>
											<!-- 한달결제실적 --> 
											2002-02-10
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</div>
				<div class="cho_admin_section" id="cho_admin_customAsk">
					<table class="cho_admin_section_table">
						<tr>
							<th class="cho_admin_section_title">고객문의현황</th>
						</tr>
						<tr>
							<td>
								<table class="resultTable">
									<tr>
										<th>문의제목</th>
										<th>ID</th>
										<th>문의날짜</th>
										<th>답변상태</th>
									</tr>
									<!-- for문으로 문의내용이 있는 만큼 출력되게 하기 -->
									<tr>
										<td>문의하기</td>
										<td>
											<!-- 한달결제실적 --> 황윤종
										</td>
										<td>2002/02/02</td>
										<td>no</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</div>
			</td>
			<td class="table_aside"></td>
		</tr>


	</table>
	 <%@ include file="../common/cho_footer.jsp" %>
<!-- 	 <script type="text/javascript">
	 $(function){
		 //회원신규가입
		 $.ajax({
			url:"adminCount1.ac",
			type="get",
			success: function(data){
				$("cho_adCount1").text(data);
			},
			error: function(request){
				console.log(request);
			}
		 });
		 //회원 탈퇴
		 
		 //전체 가입자수
	 }
	 
	 </script> -->
</body>
</html>