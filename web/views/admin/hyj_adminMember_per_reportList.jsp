<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <style>
        a:link { 
            text-decoration: none;
            color: black;
        }
        a:visited { 
            text-decoration: none;
            color: black;
        }
        .jyh_article1{
            width:840px; 
            height:75px;
        }
        .jyh_article2{
            width:840px; 
            height:35px;
            text-align: center;
        }
        #jyh_nav{
            width:280px;
        }
        #jyh_nav-div{
            height:1000px;
        }
        #jyh_side-menu-per, #jyh_side-menu-com{
            width:80%;
            margin:auto;
        }
        #jyh_side-menu-com{
            margin-top:50px;
        }
        #jyh_side-menu-outline p{
            margin:10px;
        }
        #jyh_side-menu-outline ul{
            list-style:none; 
            padding-left:5px;
        }
        #jyh_side-menu-outline{
            width:75%;
            margin:auto;
            border:1px solid gray;
        }

        #jyh_aside{
            width:280px;
        }
        footer{
            margin-left: auto;
            margin-right: auto;
            width:1400px;
            height:100px;
        }
        #section{
            height:1000px;
        }

        #jyh_outline-table{
            margin:auto; 
            border-collapse:collapse; 
            border-spacing:0;
        }
        #jyh_outline-table td:nth-of-type(1n){
            margin:0;
            padding:0;
        }

        #jyh_search-per{
            padding:5px;
            text-align: left;
            font-weight:bold;
            font-size: 15pt;
        }
        #jyh_page-descript-per{
            width:100%;
            height:100px;
        }
        #jyh_page-descript-per ul{
            margin:0;
            width:85%;
        }
        #jyh_page-descript-per p:nth-of-type(1n){
            margin:8px;
        }
        #jyh_modal_open_btn{
            width:85px;
            height:30px;
            float:left;
            font-weight:500;
            font-size: 12pt;
            border-radius: 5px;
            border:0;
            outline:0;
            margin-top:8px;
        }
        #jyh_modal{
            width:840px;
            display: none;
        }

        #jyh_modal {
            position:relative;
            width:100%;
            height:100%;
            z-index:1;
            }

        #jyh_modal h2 {
            margin:0;   
        }
        #jyh_modal .jyh_modal_content {
            width:500px;
            height:330px;
            margin:100px auto;
            padding:20px 10px;
            background:#fff;
            border:2px solid #666;
        } 
        .jyh_modal_content div:first-child{
            width:100%; 
            height:32px;
        }
        #jyh_modal .jyh_modal_layer {
            position:fixed;
            top:0;
            left:0;
            width:100%;
            height:100%;
            background:rgba(0, 0, 0, 0.5);
            z-index:-1;
        }  
        #jyh_word-list > table{
            margin:auto;
        }
        #jyh_word-list p:nth-of-type(1n){
            padding:10px;
            margin:0;
        }
        #jyh_modal_close_btn, #jyh_modal_next_btn, #jyh_modal_back_btn{
            width:85px;
            height:30px;
            float:right;
            font-weight:500;
            font-size: 12pt;
            border-radius: 5px;
            border:0;
            outline:0;
            margin-top:2px;
        }
        #jyh_per-list {
            border-collapse: collapse;
            border: 1px solid #168;
            width:100%;
        }  
        #jyh_per-list th {
            color: #168;
            background: #f0f6f9;
            text-align: center;
        }
        #jyh_per-list th, #jyh_per-list td {
            padding: 10px;
            /* border: 1px solid #ddd; */
        }
        #jyh_per-list th:first-child, #jyh_per-list td:first-child {
            border-left: 0;
        }
        #jyh_per-list th:last-child, #jyh_per-list td:last-child {
            border-right: 0;
        }
        #jyh_per-list tr td:first-child{
            text-align: center;
        }
        .jyh_search-filter{
            border:1px solid gray;
            border-radius: 5px;
        }
        .jyh_search-select{
            height:24px;
        }
        .jyh_search-line, #search{
            vertical-align: middle;
            margin-left:5px;
        }
        .jyh_search-radius{
            border-radius: 5px;
            border: 1px solid gray;
        }
        #search{
            width:200px;
            height:20px;
        }
        
   
    </style>
</head>
<body>
<%@ include file="../common/jyh_adminMenubar.jsp" %>
<table id="jyh_outline-table">
    <tr>
        <td id="jyh_nav">
            <div id=jyh_nav-div>
        <div id="jyh_side-menu-outline">
            <div id="jyh_side-menu-per">
                <h3>신고관리</h3>
                <hr style="margin-right:auto; margin-left:auto; margin-top:5px;">
                <ul>
                    <li style="font-weight:bold;">신고관리
                        <ul style="font-weight:400;">
                            <li><p>- 개인 회원</p></li>
                            <li><p>- 기업 회원</p></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    </td>
    <td align="top">
            <div class="jyh_article1"></div>
            <div class="jyh_article2">
                <p id="jyh_search-per">개인전체신고목록</p>
            </div>
            <hr style="margin-bottom:0;">
            <div id="section">
        <div id="jyh_page-descript-per">
            <ul id="jyh_page-descript-per1" style="float:left;">
                    <li><p>부적절한 행동을 한 유저에대해 관리하는 페이지입니다.</p></li>
                    <li><p>약관의 의거하여 유저가 부적절한 행동을 했는지 확인하여 처리해야합니다</p></li>
                    <li><p>첨부파일이 있을시 같이 확인바랍니다</p></li>
                </ul>
            <button type="button" id="jyh_modal_open_btn">단어확인</button>
        </div>
                                     <!-- 모달 창 시작 -->
            <div id="jyh_modal">
                    <div class="jyh_modal_content">
                       <div><h2 style="float:left; margin:0px;">부적절한 단어</h2>
                            <button type="button" id="jyh_modal_close_btn">닫기</button>
                        </div>
                       <hr>
                       <div id="jyh_word-list">
                           <table border="1" align="center">
                               <tr>
                                   <td><p>바</p></td>
                                   <td><p>소믈리에</p></td>
                                   <td><p>노래바</p></td>
                                   <td><p>섹시빠</p></td>
                                   <td><p>강남빠</p></td>
                                </tr>
                               <tr>
                                   <td><p>빠</p></td>
                                   <td><p>웨이터</p></td>
                                   <td><p>노래빠</p></td>
                                   <td><p>강남바</p></td>
                                   <td><p>강남BAR</p></td>
                                </tr>
                               <tr>
                                   <td><p>노래BAR</p></td>
                                   <td><p>황윤종</p></td>
                                   <td><p>째즈빠</p></td>
                                   <td><p>남성전용</p></td>
                                   <td><p>비키니</p></td>
                                </tr>
                               <tr>
                                   <td><p>호빠</p></td>
                                   <td><p>섹시</p></td>
                                   <td><p>빠텐더</p></td>
                                   <td><p>여성빠</p></td>
                                   <td><p>여성전용빠</p></td>
                                </tr>
                               <tr>
                                   <td><p>여성전용바</p></td>
                                   <td><p>모던빠</p></td>
                                   <td><p>모던바</p></td>
                                   <td><p>모던BAR</p></td>
                                   <td><p>바밤BAR</p></td>
                                </tr>
                           </table>
                       </div>
                       <hr>

                       <button type="button" id="jyh_modal_next_btn">다음</button>
                       <button type="button" id="jyh_modal_back_btn" style="display: none;">이전</button>
                    </div>
                   
                    <div class="jyh_modal_layer"></div>
                </div>
                                                <!-- 모달 창 끝 -->

                <div id="jyh_searchbar-outline">
                    <div id="jyh_search_filter" style="float: right; margin-bottom:10px;">
                        <span class="jyh_search-line"><label>가입일</label></span>
                        <span class="jyh_search-line jyh_search-filter jyh_search-radius">1주일이내</span>
                        <span class="jyh_search-line jyh_search-filter jyh_search-radius">1개월이내</span>
                        <span class="jyh_search-line jyh_search-filter jyh_search-radius">6개월이내</span>
                        <select class="jyh_search-line jyh_search-select jyh_search-radius">
                            <option>신고번호</option>
                            <option>아이디</option>
                            <option>제목</option>
                            <option>신고일</option>
                        </select>
                        <input type="text" class="jyh_search-radius" id="search" maxlength="50">
                        <button class="jyh_search-radius">확인</button>
                    </div>
                </div>
                <div><button style="float:left;">삭제</button></div>
                <div><button style="float:left;">수정</button></div>
                <table id="jyh_per-list" border="1" align="center">
                    <tr>
                        <th><input type="checkbox" name="jyh_per-list-check"></th>
                        <th>신고번호</th>
                        <th>아이디</th>
                        <th>카테고리</th>
                        <th>신고제목</th>
                        <th>첨부파일</th>
                        <th>신고일</th>
                        <th>처리상태</th>
                    </tr>
                    <tr>
                        <td><input type="checkbox" name="jyh_per-list-check"></td>
                        <td>신고번호</td>
                        <td>아이디</td>
                        <td>카테고리</td>
                        <td>신고제목</td>
                        <td>첨부파일</td>
                        <td>신고일</td>
                        <td>처리상태</td>
                    </tr>
                    <tr>
                        <td><input type="checkbox" name="jyh_per-list-check"></td>
                        <td>321</td>
                        <td>test1</td>
                        <td>이력서허위기재</td>
                        <td>과장이력서를 작성하여 부당이득을취함!</td>
                        <td><button>다운로드</button></td>
                        <td>20.02.17</td>
                        <td>정지30일</td>
                    </tr>
                </table>
                <hr style="margin-right:auto; margin-left:auto; margin-top:10px;">

                <div class="pagingArea" align="center">
                   <button onclick=""><<</button>

                               <button disabled><</button>

                               <button onclick = ""><</button>
               
                   
                  
                                        <button disabled></button>
                        
                                      <button onclick=""></button>         			 
                              
                                
                               
                                <button disabled>></button>
                           
                                <button onclick="">></button>
                          
                   
                   
                   <button onclick="">>></button>
                </div>
            </div>
                <hr style="margin-right:auto; margin-left:auto; margin-top:10px;">
    </td>
    <td id="jyh_aside">
    </td>
    </tr>
</table>
    
    
    <%@ include file="../common/cho_footer.jsp" %>
    <script>
            $("#jyh_modal_open_btn").click(function(){
                $("#jyh_modal").attr("style", "display:block");
            });
           
             $("#jyh_modal_close_btn").click(function(){
                $("#jyh_modal").attr("style", "display:none");
            });      
        </script>
   
</body>
</html>