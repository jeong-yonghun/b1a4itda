<!-- 기업회원 공고등록이력 페이지 입니다. -->
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.util.ArrayList, java.util.HashMap, com.kh.itda.approval.model.vo.*"%>
<%
	//줄이 길어서 개행은 연산자 앞에서 (ex =앞에서)하도록하고 띄어쓰기는 윗 줄에서 8칸 띄운다.
	//형변환 후 한 칸 띄운다.
	ArrayList<HashMap<String,Object>> list = (ArrayList<HashMap<String, Object>>) request.getAttribute("list");
	System.out.println(list);
	PageInfo pi = (PageInfo) request.getAttribute("pi");
	int listCount = pi.getListCount();
	int currentPage = pi.getCurrentPage();
	int maxPage = pi.getMaxPage();
	int startPage = pi.getStartPage();
	int endPage = pi.getEndPage();
	String userNo = (String) request.getAttribute("userNo");
%>
<!DOCTYPE html>
<html lang="ko">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <style>
        a:link { 
            text-decoration: none;
            color: black;
        }
        a:visited { 
            text-decoration: none;
            color: black;
        }
        .jyh_article1{
            width:840px; 
            height:75px;
        }
        .jyh_article2{
            width:840px; 
            height:35px;
            text-align: center;
        }
        #jyh_nav{
            width:280px;
        }
        #section{
            /* height: */
        }

        #jyh_aside{
            width:280px;
        }
        footer{
            margin-left: auto;
            margin-right: auto;
            width:1400px;
            height:100px;
        }

        #jyh_outline-table{
            margin:auto; 
            border-collapse:collapse; 
            border-spacing:0;
        }
        #jyh_outline-table td, jyh_detail-info td{
            padding:0px;
        }

        .jyh_com-top-btn{
            float: left;
            margin-right:50px;
            margin-left:10px;
        }

        #jyh_page-descript-per{
            width:100%;
        }
        #jyh_page-descript-per ul{
            margin:0;
            width:85%;
        }
        #jyh_page-descript-per p:nth-of-type(1n){
            margin:8px;
        }
        #jyh_detail-info{
            margin:auto; 
            border-collapse:collapse; 
            border-spacing:0;
            width:100%;
        }
        .jyh_search-filter{
            border:1px solid gray;
            border-radius: 5px;
        }
        .jyh_search-select{
            height:24px;
        }
        .jyh_search-line, #search{
            vertical-align: middle;
            margin-left:5px;
        }
        .jyh_search-radius{
            border-radius: 5px;
            border: 1px solid gray;
        }
        #search{
            width:200px;
            height:20px;
        }
        #jyh_detail-info {
            border-collapse: collapse;
            border: 1px solid #168;
            width:100%;
        }  
        #jyh_detail-info th {
            color: #168;
            background: #f0f6f9;
            text-align: center;
        }
        #jyh_detail-info th, #jyh_detail-info td {
            padding: 10px;
            /* border: 1px solid #ddd; */
        }
        #jyh_detail-info th:first-child, #jyh_detail-info td:first-child {
            border-left: 0;
        }
        #jyh_detail-info th:last-child, #jyh_detail-info td:last-child {
            border-right: 0;
        }
        #jyh_detail-info tr td{
            text-align: center;
        }
    </style>
</head>
<body>

    <%@ include file="../common/jyh_adminMenubar.jsp" %>

<table id="jyh_outline-table">
    <tr>
        <td id="jyh_nav">
                <div class="jyh_article1" style="width:100%; vertical-align:top;" ></div>
            <%@ include file="../common/jyh_adminMemManageSideMenu.jsp" %>
    </td>
    <td style="width:840px; vertical-align:top;">
            <div class="jyh_article1" ></div>
            <div class="jyh_article2">
                <div id="jyh_com-top-btn1" class="jyh_com-top-btn">회원상세정보</div>
                <div id="jyh_com-top-btn2" class="jyh_com-top-btn">채용지원<hr style="background:#5A84F1; margin-top:2px;"></div>
                <div id="jyh_com-top-btn3" class="jyh_com-top-btn">결제내역</div>
            </div>
            <hr style="background:gray;">

                <div id="jyh_page-descript-per">
                        <ul id="jyh_page-descript-per1" style="float:left;">
                                <li><p>기업이 공고한 채용공고와 공모전을 확인할수있는 페이지입니다.</p></li>
                                <li><p>부적절한 단어가 있을 시 수정/삭제 됩니다.</p></li>
                            </ul>
                    </div>
                    <div id="jyh_searchbar-outline">
                        <div id="jyh_search_filter" style="float: right; margin-bottom:10px;">
                            <span class="jyh_search-line"><label>가입일</label></span>
                            <span class="jyh_search-line jyh_search-filter jyh_search-radius">1주일이내</span>
                            <span class="jyh_search-line jyh_search-filter jyh_search-radius">1개월이내</span>
                            <span class="jyh_search-line jyh_search-filter jyh_search-radius">6개월이내</span>
                            <select class="jyh_search-line jyh_search-select jyh_search-radius">
                                <option>전체검색</option>
                                <option>전체검색1</option>
                                <option>전체검색2</option>
                                <option>전체검색3</option>
                            </select>
                            <input type="text" class="jyh_search-radius" id="search" maxlength="50">
                            <button class="jyh_search-radius">확인</button>
                        </div>
                    </div>
                    <hr style="margin-bottom:10px; width:100%;">
                <div id="section1">
                    <div id="jyh_detail-outline" style="margin-bottom:10px; float:left; width:100%; height:100%;">
                    <div id="jyh_detail-table-outline1" style="float:left; width:100%;">
                    <table id="jyh_detail-info" border="1" style="width:100%;">
                            <tr>
                            		<th><input type="checkbox" name="jyh_per-pauseList-check"></th>
                                    <th>번호</th>
                                    <th>채용유형</th>
                                    <th>공고제목</th>
                                    <th>이력발생일자</th>
                                    <th>공고상태</th>
                                    <th>지원자수</th>
                                </tr>
                                <% 
                                if(listCount > 0){
                                for(int i = 0; i < list.size(); i++){ 
                                	HashMap<String, Object> hmap = list.get(i);
                              		System.out.println("listsize : " + list.size());
                              		System.out.println("list.get() : " + list.get(i));
                              		System.out.println("hmap : " + hmap);
                                %>
								<tr>
									<td><input type="checkbox" name="jyh_per-pauseList-check"></td>
									<td><%= hmap.get("rnum") %></td>
									<td><%= hmap.get("aDivision") %></td>
									<td><%= hmap.get("aTitle") %></td>
									<td><%= hmap.get("reDate") %></td>
									<td><%= hmap.get("status") %></td>
									<td><%= hmap.get("ctn") %></td>
								</tr>
								<% }}else{%>
								<tr>
								<td colspan="7">공고 등록 이력이 없습니다.</td>
							</tr>
							<%} %>
                    </table>
						
                    </div>
            </div>
            <hr style="margin-right:auto; margin-left:auto; margin-top:10px;">
            <%-- 페이지 처리 --%>
      <div class="pagingArea" align="center">
        <button onclick="location.href='<%=request.getContextPath()%>/jyhAnnList.me?currentPage=1&userNo=<%= userNo %>'"><<</button>
        <% if(currentPage <= 1) { %>
        <button disabled><</button>
        <% } else { %>
        <button onclick="location.href='<%=request.getContextPath()%>/jyhAnnList.me?currentPage=<%=currentPage - 1%>' + '&userNo=<%= userNo %>'"><</button>
        <% } %>
        
        <% for(int p = startPage; p <= endPage; p++) { 
        	  if(p == currentPage){
        %>
        		<button disabled><%= p %></button>
        <%
        	  } else {
        %>
        		<button onclick="location.href='<%=request.getContextPath()%>/jyhAnnList.me?currentPage=<%=p%>' + '&userNo=<%= userNo %>'"><%= p %></button>
        <% 
        	  }
           }
        %>
      
      	
      	<% if(currentPage >= maxPage) { %>
      	<button disabled>></button>
      	<% } else { %>
      	<button onclick="location.href='<%=request.getContextPath()%>/jyhAnnList.me?currentPage=<%=currentPage + 1%>' + '&userNo=<%= userNo %>'">></button>
      	<% } %>
      	
      	<button onclick="location.href='<%=request.getContextPath()%>/jyhAnnList.me?currentPage=<%=maxPage%>' + '&userNo=<%= userNo %>'">>></button>
      </div>
            </div>
      
        </td>
    <td id="jyh_aside">
    </td>
    </tr>
</table>
    
    <%@ include file="../common/cho_footer.jsp" %>
   
   <script>
    	$("#jyh_com-top-btn1").click(function(){
         	location.href = "<%=request.getContextPath()%>/jyhSelectOne.me?userNo=<%= userNo %>&memType=2";
        });
    	
    	$("#jyh_com-top-btn2").click(function(){
        	location.href = "<%=request.getContextPath()%>/jyhAnnList.me?userNo=<%= userNo %>";
        });
    	
    	$("#jyh_com-top-btn3").click(function(){
         	location.href = "<%=request.getContextPath()%>/jyhPaymentList.me?userNo=<%= userNo %>";
        });
    </script>
</body>
</html>