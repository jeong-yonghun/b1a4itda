<!-- 개인회원 채용 지원 이력 페이지 입니다. -->
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.util.ArrayList, java.util.HashMap, com.kh.itda.approval.model.vo.*"%>
<%
	//줄이 길어서 개행은 연산자 앞에서 (ex =앞에서)하도록하고 띄어쓰기는 윗 줄에서 8칸 띄운다.
	//형변환 후 한 칸 띄운다.
	ArrayList<HashMap<String,Object>> list = (ArrayList<HashMap<String, Object>>) request.getAttribute("list");
	PageInfo pi = (PageInfo) request.getAttribute("pi");
	String userNo = (String) request.getAttribute("userNo");
	int listCount = pi.getListCount();
	int currentPage = pi.getCurrentPage();
	int maxPage = pi.getMaxPage();
	int startPage = pi.getStartPage();
	int endPage = pi.getEndPage();
%>
<!DOCTYPE html>
<html lang="ko">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <style>
        a:link { 
            text-decoration: none;
            color: black;
        }
        a:visited { 
            text-decoration: none;
            color: black;
        }
        .jyh_article1{
            width:840px; 
            height:75px;
        }
        .jyh_article2{
            width:840px; 
            height:35px;
            text-align: center;
        }
        #jyh_nav{
            width:280px;
        }
        #section{
            /* height: */
        }

        #jyh_aside{
            width:280px;
        }
        footer{
            margin-left: auto;
            margin-right: auto;
            width:1400px;
            height:100px;
        }

        #jyh_outline-table{
            margin:auto; 
            border-collapse:collapse; 
            border-spacing:0;
        }
        #jyh_outline-table td, jyh_application-list td{
            padding:0px;
        }

        .jyh_per-top-btn{
            float: left;
            margin-right:50px;
            margin-left:10px;
        }

        #jyh_page-descript-per{
            width:100%;
            height:100px;
        }
        #jyh_page-descript-per ul{
            margin:0;
            width:85%;
        }
        #jyh_page-descript-per p:nth-of-type(1n){
            margin:8px;
        }
        #jyh_application-list{
            margin:auto; 
            border-collapse:collapse; 
            border-spacing:0;
            width:100%;
        }
        .jyh_search-filter{
            border:1px solid gray;
            border-radius: 5px;
        }
        .jyh_search-select{
            height:24px;
        }
        .jyh_search-line, #search{
            vertical-align: middle;
            margin-left:5px;
        }
        .jyh_search-radius{
            border-radius: 5px;
            border: 1px solid gray;
        }
        #search{
            width:200px;
            height:20px;
        }
        .comNo:hover, .rNo:hover, .aNo:hover{
        	cursor:pointer;
        }
        .comNo1, .rNo1, .aNo1{
        	display:none;
        }
        #jyh_application-list tr{
        	text-align:center;
        }
        .hide{
        	display:none;
        }
        .join-date{
        	color:gray;
        }
        #jyh_application-list {
            border-collapse: collapse;
            border: 1px solid #168;
            width:100%;
        }  
        #jyh_application-list th {
            color: #168;
            background: #f0f6f9;
            text-align: center;
        }
        #jyh_application-list th, #jyh_application-list td {
            padding: 10px;
            /* border: 1px solid #ddd; */
        }
        #jyh_application-list th:first-child, #jyh_application-list td:first-child {
            border-left: 0;
        }
        #jyh_application-list th:last-child, #jyh_application-list td:last-child {
            border-right: 0;
        }
        #jyh_application-list tr td:first-child{
            text-align: center;
        }
    </style>
</head>
<body>
<%@ include file="../common/jyh_adminMenubar.jsp" %>

<table id="jyh_outline-table">
    <tr>
        <td id="jyh_nav">
                <div class="jyh_article1" style="width:100%; vertical-align:top;" ></div>
            <%@ include file="../common/jyh_adminMemManageSideMenu.jsp" %>
    </td>
    <td style="width:840px; vertical-align:top;">
            <div class="jyh_article1" ></div>
            <div class="jyh_article2">
                <div id="jyh_per-top-btn1" class="jyh_per-top-btn">회원 상세정보</div>
                <div id="jyh_per-top-btn2" class="jyh_per-top-btn">지원 이력<hr style="background:#5A84F1; margin-top:2px;"></div>
                <div id="jyh_per-top-btn3" class="jyh_per-top-btn">이력서/포트폴리오</div>
            </div>
				<hr style="background:gray;">
                <div id="jyh_page-descript-per">
                        <ul id="jyh_page-descript-per1" style="float:left;">
                                <li><p>개인유저가 채용공고/공모전을 지원한 페이지입니다.</p></li>
                                <li><p>지원한 이력서를 삭제했을시 지원한 공고는 나오지않습니다.</p></li>
                                <li><p>지원한 공모전/채용공고가 부적절한 경우 삭제됩니다.</p></li>
                            </ul>
                    </div>
                    <div id="jyh_searchbar-outline">
                        <div id="jyh_search_filter" style="float: right; margin-bottom:10px;">
                            <span class="jyh_search-line"><label>가입일</label></span>
                            <span class="jyh_search-line jyh_search-filter jyh_search-radius">1주일이내</span>
                            <span class="jyh_search-line jyh_search-filter jyh_search-radius">1개월이내</span>
                            <span class="jyh_search-line jyh_search-filter jyh_search-radius">6개월이내</span>
                            <select class="jyh_search-line jyh_search-select jyh_search-radius">
                                <option>전체검색</option>
                                <option>전체검색1</option>
                                <option>전체검색2</option>
                                <option>전체검색3</option>
                            </select>
                            <input type="text" class="jyh_search-radius" id="search" maxlength="50">
                            <button id="test" class="jyh_search-radius">확인</button>
                        </div>
                    </div>
                    <hr style="margin-bottom:10px; width:100%;">
                <div id="section1">
                    <div id="jyh_detail-outline" style="margin-bottom:10px; float:left; width:100%; height:100%;">
                    <div id="jyh_detail-table-outline1" style="float:left; width:100%;">
                    <table id="jyh_application-list" border="1" style="width:100%;">
                            <tr>
                                    <th>번호</th>
                                    <th>회사명</th>
                                    <th>채용유형</th>
                                    <th>이력서제목</th>
                                    <th>채용제목</th>
                                    <th>지원일</th>
                                    <th>상태</th>
                                </tr>
                                <% 
                                if(listCount > 0){
                                for(int i = 0; i < list.size(); i++){ 
                                	HashMap<String, Object> hmap = list.get(i);
                              		System.out.println("listsize : " + list.size());
                              		System.out.println("list.get() : " + list.get(i));
                              		System.out.println("hmap : " + hmap);
                                %>
								<tr>
									<td><%= hmap.get("rnum") %></td>
									<td class="comNo"><%= hmap.get("companyName") %><label class="comNo1"><%= hmap.get("comNo") %></label></td>
									<td><%= hmap.get("aDivision") %></td>
									<td class="rNo"><%= hmap.get("rTitle") %><label class="rNo1"><%= hmap.get("rNo") %></label></td>
									<td class="aNo"><%= hmap.get("aTitle") %><label class="aNo1"><%= hmap.get("aNo") %></label></td>
									<td><%= hmap.get("apvVolDate") %></td>
									<td><%= hmap.get("status") %></td>
								</tr>
								<% }}else{ %>
								<tr>
									<td colspan="7">지원 이력이 없습니다.</td>
								</tr>
								<%} %>
                    </table>
                    </div>
            </div>
                   <hr style="margin-right:auto; margin-left:auto; margin-top:10px;">
                   
            <%-- 페이지 처리 --%>
      <div class="pagingArea" align="center">
            <% if(listCount > 0){ %>
        <button onclick="location.href='<%=request.getContextPath()%>/jyhApplicationList.me?currentPage=1&userNo=<%= list.get(0).get("perNo") %>'"><<</button>
        <% if(currentPage <= 1) { %>
        <button disabled><</button>
        <% } else { %>
        <button onclick="location.href='<%=request.getContextPath()%>/jyhApplicationList.me?currentPage=<%=currentPage - 1%>' + '&userNo=<%= list.get(0).get("perNo") %>'"><</button>
        <% } %>
        
        <% for(int p = startPage; p <= endPage; p++) { 
        	  if(p == currentPage){
        %>
        		<button disabled><%= p %></button>
        <%
        	  } else {
        %>
        		<button onclick="location.href='<%=request.getContextPath()%>/jyhApplicationList.me?currentPage=<%=p%>' + '&userNo=<%= list.get(0).get("perNo") %>'"><%= p %></button>
        <% 
        	  }
           }
        %>
      
      	
      	<% if(currentPage >= maxPage) { %>
      	<button disabled>></button>
      	<% } else { %>
      	<button onclick="location.href='<%=request.getContextPath()%>/jyhApplicationList.me?currentPage=<%=currentPage + 1%>' + '&userNo=<%= list.get(0).get("perNo") %>'">></button>
      	<% } %>
      	
      	<button onclick="location.href='<%=request.getContextPath()%>/jyhApplicationList.me?currentPage=<%=maxPage%>' + '&userNo=<%= list.get(0).get("perNo") %>'">>></button>
            <%} else{%>
            	<button disabled><<</button>
            	<button disabled><</button>
            	<button disabled>></button>
            	<button disabled>>></button>
            <%} %>
      </div>
            
            </div>
        </td>
    <td id="jyh_aside">
    </td>
    </tr>
</table>
    
    <%@ include file="../common/cho_footer.jsp" %>
    
    <script>
    
    	$(function(){
             
            $("#jyh_application-list tr:not(:first-child) td:nth-of-type(2)").click(function(){
            	comNo = $(this).children().text();
            	console.log(comNo);
            		
            	<%-- location.href = "<%=request.getContextPath()%>/jyhSelectOne.me?userNo=" + userNo; --%>
            		
            });
            $("#jyh_application-list tr:not(:first-child) td:nth-of-type(4)").click(function(){
            	rNo = $(this).children().text();
            	console.log(rNo);
            		
            	<%-- location.href = "<%=request.getContextPath()%>/jyhSelectOne.me?userNo=" + userNo; --%>
            		
            });
            $("#jyh_application-list tr:not(:first-child) td:nth-of-type(5)").click(function(){
            	aNo = $(this).children().text();
            	console.log(aNo);
            		
            	<%-- location.href = "<%=request.getContextPath()%>/jyhSelectOne.me?userNo=" + userNo; --%>
            		
            });
            
    	});
    </script>
    <script>
    	$("#jyh_per-top-btn1").click(function(){
         	location.href = "<%=request.getContextPath()%>/jyhSelectOne.me?userNo=<%= userNo %>&memType=1";
        });
    	
    	$("#jyh_per-top-btn2").click(function(){
        	location.href = "<%=request.getContextPath()%>/jyhApplicationList.me?userNo=<%= userNo %>&memType=1";
        });
    	
    	$("#jyh_per-top-btn3").click(function(){
         	location.href = "<%=request.getContextPath()%>/jyhDocumentList.me?userNo=<%= userNo %>&memType=1";
        });
    </script>
    
    <script>
    	$(function(){
    		$("#test").click(function(){
    			var w = window.open("<%=request.getContextPath()%>/jyhDocumentList.me?userNo=<%= userNo %>&memType=1","_blank");
    		});
    	});
    </script>
</body>
</html>