<!-- 기업회원 결제내역 페이지 입니다.  -->
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.util.ArrayList, java.util.HashMap, com.kh.itda.approval.model.vo.*"%>
<%
	//줄이 길어서 개행은 연산자 앞에서 (ex =앞에서)하도록하고 띄어쓰기는 윗 줄에서 8칸 띄운다.
	//형변환 후 한 칸 띄운다.
	ArrayList<HashMap<String,Object>> list = (ArrayList<HashMap<String, Object>>) request.getAttribute("list");
	PageInfo pi = (PageInfo) request.getAttribute("pi");
	int listCount = pi.getListCount();
	int currentPage = pi.getCurrentPage();
	int maxPage = pi.getMaxPage();
	int startPage = pi.getStartPage();
	int endPage = pi.getEndPage();
	String userNo = (String) request.getAttribute("userNo");
%>
<!DOCTYPE html>
<html lang="ko">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <style>
        a:link { 
            text-decoration: none;
            color: black;
        }
        a:visited { 
            text-decoration: none;
            color: black;
        }
        .jyh_article1{
            width:840px; 
            height:75px;
        }
        .jyh_article2{
            width:840px; 
            height:35px;
            text-align: center;
        }
        #jyh_nav{
            width:280px;
        }
        #section{
            /* height: */
        }

        #jyh_aside{
            width:280px;
        }
        footer{
            margin-left: auto;
            margin-right: auto;
            width:1400px;
            height:100px;
        }

        #jyh_outline-table{
            margin:auto; 
            border-collapse:collapse; 
            border-spacing:0;
        }
        #jyh_outline-table td, jyh_detail-info td{
            padding:0px;
        }

        .jyh_com-top-btn{
            float: left;
            margin-right:50px;
            margin-left:10px;
        }

        #jyh_page-descript-per{
            width:100%;
            height:100px;
        }
        #jyh_page-descript-per ul{
            margin:0;
            width:85%;
        }
        #jyh_page-descript-per p:nth-of-type(1n){
            margin:8px;
        }
        #jyh_detail-info{
            margin:auto; 
            border-collapse:collapse; 
            border-spacing:0;
            width:100%;
        }
        .jyh_search-filter{
            border:1px solid gray;
            border-radius: 5px;
        }
        .jyh_search-select{
            height:24px;
        }
        .jyh_search-line, #search, #jyh_modify-release-btn{
            vertical-align: middle;
            margin-left:5px;
        }
        .jyh_search-radius, #jyh_modify-release-btn{
            border-radius: 5px;
            border: 1px solid gray;
        }
        #search{
            width:200px;
            height:20px;
        }
        #jyh_detail-info {
            border-collapse: collapse;
            border: 1px solid #168;
            width:100%;
        }  
        #jyh_detail-info th {
            color: #168;
            background: #f0f6f9;
            text-align: center;
        }
        #jyh_detail-info th, #jyh_detail-info td {
            padding: 10px;
            /* border: 1px solid #ddd; */
        }
        #jyh_detail-info th:first-child, #jyh_detail-info td:first-child {
            border-left: 0;
        }
        #jyh_detail-info th:last-child, #jyh_detail-info td:last-child {
            border-right: 0;
        }
        #jyh_detail-info tr td{
            text-align: center;
        }
    </style>
</head>
<body>

    <%@ include file="../common/jyh_adminMenubar.jsp" %>

<table id="jyh_outline-table">
    <tr>
        <td id="jyh_nav">
                <div class="jyh_article1" style="width:100%; vertical-align:top;" ></div>
            <%@ include file="../common/jyh_adminMemManageSideMenu.jsp" %>
    </td>
    <td style="width:840px; vertical-align:top;">
            <div class="jyh_article1" ></div>
            <div class="jyh_article2">
                <div id="jyh_com-top-btn1" class="jyh_com-top-btn">회원상세정보</div>
                <div id="jyh_com-top-btn2" class="jyh_com-top-btn">채용지원</div>
                <div id="jyh_com-top-btn3" class="jyh_com-top-btn">결제내역<hr style="background:#5A84F1; margin-top:2px;"></div>
            </div>
            <hr style="background:gray;">

                <div id="jyh_page-descript-per">
                        <ul id="jyh_page-descript-per1" style="float:left;">
                                <li><p>기업의 결재내역을 볼수있는 페이지입니다.</p></li>
                                <li><p>결제내역 영수증은 가입한 이메일로 발송합니다.</p></li>
                                <li><p>결제취소는 최대 결제후 7일이전까지만 가능합니다.</p></li>
                            </ul>
                    </div>
                    <div id="jyh_searchbar-outline">
                        <div id="jyh_search_filter" style="float: right; margin-bottom:10px;">
                            <span class="jyh_search-line"><label>가입일</label></span>
                            <span class="jyh_search-line jyh_search-filter jyh_search-radius">1주일이내</span>
                            <span class="jyh_search-line jyh_search-filter jyh_search-radius">1개월이내</span>
                            <span class="jyh_search-line jyh_search-filter jyh_search-radius">6개월이내</span>
                            <select class="jyh_search-line jyh_search-select jyh_search-radius">
                                <option>전체검색</option>
                                <option>전체검색1</option>
                                <option>전체검색2</option>
                                <option>전체검색3</option>
                            </select>
                            <input type="text" class="jyh_search-radius" id="search" maxlength="50">
                            <button class="jyh_search-radius">확인</button>
                        </div>
                    </div>
                    <div id="jyh_modify-release-btn-outline" style="width:100%; float:left; margin-bottom:10px;">
                        <input type="button" id="jyh_modify-release-btn" value="결제취소" style="float:right;"></input>
                        <input type="button" id="jyh_modify-release-btn" value="결제완료" style="float:right;"></input>
                        <input type="button" id="jyh_modify-release-btn" value="모든결제" style="float:right;"></input>
                </div>
                    <hr style="margin-bottom:10px; width:100%;">
                <div id="section1">
                    <div id="jyh_detail-outline" style="margin-bottom:10px; float:left; width:100%; height:100%;">
                    <div id="jyh_detail-table-outline1" style="float:left; width:100%;">
                    <table id="jyh_detail-info" border="1" style="width:100%;">
                            <tr>
                                    <th><input type="checkbox" name="jyh_per-list-check"></th>
                                    <th>번호</th>
                                    <th>결제상품명</th>
                                    <th>회사명</th>
                                    <th>휴대전화</th>
                                    <th>결제일시</th>
                                    <th>결제금액</th>
                                    <th>결제상태</th>
                                </tr>
                                <% 
                                if(listCount > 0){
                                for(int i = 0; i < list.size(); i++){ 
                                	HashMap<String, Object> hmap = list.get(i);
                              		System.out.println("listsize : " + list.size());
                              		System.out.println("list.get() : " + list.get(i));
                              		System.out.println("hmap : " + hmap);
                                %>
								<tr>
									<td><input type="checkbox" name="jyh_per-pauseList-check"></td>
									<td><%= hmap.get("rnum") %></td>
									<td><%= hmap.get("pdName") %></td>
									<td><%= hmap.get("companyName") %></td>
									<td><%= hmap.get("phone") %></td>
									<% if(!hmap.get("pyDivision").equals("환불")){%>
										<td><%=hmap.get("pyDate")%></td>
									<% }else{ %>
										<td><%=hmap.get("reDate")%></td>
									<% } %>
									<td><%= hmap.get("pdPrice") %></td>
									<td><%= hmap.get("pyDivision") %></td>
								</tr>
								<% }}else{%>
								<tr>
								<td colspan="8">결제 내역이 없습니다.</td>
							</tr>
							<%} %>
                    </table>

                    </div>
            </div>
            
            <hr style="margin-right:auto; margin-left:auto; margin-top:10px;">
            <%-- 페이지 처리 --%>
      <div class="pagingArea" align="center">
        <button onclick="location.href='<%=request.getContextPath()%>/jyhPaymentList.me?currentPage=1&userNo=<%= userNo %>'"><<</button>
        <% if(currentPage <= 1) { %>
        <button disabled><</button>
        <% } else { %>
        <button onclick="location.href='<%=request.getContextPath()%>/jyhPaymentList.me?currentPage=<%=currentPage - 1%>' + '&userNo=<%= userNo %>'"><</button>
        <% } %>
        
        <% for(int p = startPage; p <= endPage; p++) { 
        	  if(p == currentPage){
        %>
        		<button disabled><%= p %></button>
        <%
        	  } else {
        %>
        		<button onclick="location.href='<%=request.getContextPath()%>/jyhPaymentList.me?currentPage=<%=p%>' + '&userNo=<%= userNo %>'"><%= p %></button>
        <% 
        	  }
           }
        %>
      
      	
      	<% if(currentPage >= maxPage) { %>
      	<button disabled>></button>
      	<% } else { %>
      	<button onclick="location.href='<%=request.getContextPath()%>/jyhPaymentList.me?currentPage=<%=currentPage + 1%>' + '&userNo=<%= userNo %>'">></button>
      	<% } %>
      	
      	<button onclick="location.href='<%=request.getContextPath()%>/jyhPaymentList.me?currentPage=<%=maxPage%>' + '&userNo=<%= userNo %>'">>></button>
      </div>
      
            </div>
        </td>
    <td id="jyh_aside">
    </td>
    </tr>
</table>
    
    
    <%@ include file="../common/cho_footer.jsp" %>
    
    <script>
    	$("#jyh_com-top-btn1").click(function(){
         	location.href = "<%=request.getContextPath()%>/jyhSelectOne.me?userNo=<%= userNo %>&memType=2";
        });
    	
    	$("#jyh_com-top-btn2").click(function(){
        	location.href = "<%=request.getContextPath()%>/jyhAnnList.me?userNo=<%= userNo %>";
        });
    	
    	$("#jyh_com-top-btn3").click(function(){
         	location.href = "<%=request.getContextPath()%>/jyhPaymentList.me?userNo=<%= userNo %>";
        });
    </script>
   
</body>
</html>