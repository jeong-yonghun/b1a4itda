<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<style>
</style>
</head>
<body>
	<form id="joinForm" name="form" action="<%= request.getContextPath() %>/admin.me" method="post">
	<div id="admin-wrap" style="width:100%; height:800px; background:rgba(5, 5, 6, 0.8); padding-top:280px;">
		<div id="login-box" style="width:400px; height:200px; background:white; margin:auto;">
			<div id="title"style="width:100%; height:20%; padding-top:5px;"><h3 style="text-align:center; ">관리자 로그인</h3></div>
			<div id="login" style="width:100%; height:64px; margin-top:45px;">
				<div style="width:100%; float:left;">
					<div style="width:30%; float:left;"><label style="font-size:15pt;">아이디</label></div>
					<div style="width:70%; float:left;"><input type="text" name="id" style="height:30px; width:95%;"></div>
				</div><br>
				<div style="width:100%; float:left;">
					<div style="width:30%; float:left;"><label style="font-size:15pt;">비밀번호</label></div>
					<div style="width:70%; float:left;"><input type="password" name="perPwd" style="height:30px; width:95%;"></div>
				</div>
				
				<button type="submit" name="submit" style="background:rgba(5, 5, 6, 0.8); color:white; width:100%; height:100%; margin:0px; margin-top:5px;  padding:0;  float:left;">로그인</button>
			</div>
		</div>
	</div>
	</form>
</body>
</html>