<!-- 개인회원의 이력서 및 포트폴리오 페이지 입니다. -->
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.util.ArrayList, java.util.HashMap, com.kh.itda.approval.model.vo.*"%>
<%
	//줄이 길어서 개행은 연산자 앞에서 (ex =앞에서)하도록하고 띄어쓰기는 윗 줄에서 8칸 띄운다.
	//형변환 후 한 칸 띄운다.
	ArrayList<HashMap<String,Object>> list = (ArrayList<HashMap<String, Object>>) request.getAttribute("list");
	PageInfo pi = (PageInfo) request.getAttribute("pi");
	int listCount = pi.getListCount();
	int currentPage = pi.getCurrentPage();
	int maxPage = pi.getMaxPage();
	int startPage = pi.getStartPage();
	int endPage = pi.getEndPage();
	String userNo = (String) request.getAttribute("userNo");
%>
<!DOCTYPE html>
<html lang="ko">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <style>
        a:link { 
            text-decoration: none;
            color: black;
        }
        a:visited { 
            text-decoration: none;
            color: black;
        }
        .jyh_article1{
            width:840px; 
            height:75px;
        }
        .jyh_article2{
            width:840px; 
            height:35px;
            text-align: center;
        }
        #jyh_nav{
            width:280px;
        }
        #section{
            /* height: */
        }

        #jyh_aside{
            width:280px;
        }
        footer{
            margin-left: auto;
            margin-right: auto;
            width:1400px;
            height:100px;
        }

        #jyh_outline-table{
            margin:auto; 
            border-collapse:collapse; 
            border-spacing:0;
        }
        #jyh_outline-table td, #jyh_detail-info td{
            padding:0px;
        }

        .jyh_per-top-btn{
            float: left;
            margin-right:50px;
            margin-left:10px;
        }

        #jyh_page-descript-per{
            width:100%;
            height:70px;
        }
        #jyh_page-descript-per ul{
            margin:0;
            width:85%;
        }
        #jyh_page-descript-per p:nth-of-type(1n){
            margin:8px;
        }

        #jyh_detail-info{
            margin:auto; 
            border-collapse:collapse; 
            border-spacing:0;
            width:100%;
        }
        .rNo1, .perNo1{
        	display:none;
        }
        .rNo:hover, .perNo:hover{
        	cursor:pointer;
        }
        #jyh_resume-list, #jyh_portfolio-list {
            border-collapse: collapse;
            border: 1px solid #168;
            width:100%;
        }  
        #jyh_resume-list th, #jyh_portfolio-list th {
            color: #168;
            background: #f0f6f9;
            text-align: center;
        }
        #jyh_resume-list th, #jyh_resume-list td, #jyh_portfolio-list th, #jyh_portfolio-list td{
            padding: 10px;
            /* border: 1px solid #ddd; */
        }
        #jyh_resume-list th:first-child, #jyh_resume-list td:first-child, jyh_portfolio-list th:first-child, jyh_portfolio-list td:first-child{
            border-left: 0;
        }
        #jyh_resume-list th:last-child, #jyh_resume-list td:last-child, #jyh_portfolio-list th:last-child, #jyh_portfolio-list td:last-child {
            border-right: 0;
        }
        #jyh_resume-list tr td:first-child, #jyh_portfolio-list tr td:first-child{
            text-align: center;
        }
    </style>
</head>
<body>
<%@ include file="../common/jyh_adminMenubar.jsp" %>
<table id="jyh_outline-table">
    <tr>
        <td id="jyh_nav">
                <div class="jyh_article1" style="width:100%; vertical-align:top;" ></div>
            <%@ include file="../common/jyh_adminMemManageSideMenu.jsp" %>
    </td>
    <td style="width:840px; vertical-align:top;">
            <div class="jyh_article1" ></div>
            <div class="jyh_article2">
                <div id="jyh_per-top-btn1" class="jyh_per-top-btn">회원 상세정보</div>
                <div id="jyh_per-top-btn2" class="jyh_per-top-btn">지원 이력</div>
                <div id="jyh_per-top-btn3" class="jyh_per-top-btn">이력서/포트폴리오<hr style="background:#5A84F1; margin-top:2px;"></div>
            </div>
				<hr style="background:gray;">
                <div id="jyh_page-descript-per">
                        <ul id="jyh_page-descript-per1" style="float:left;">
                                <li><p>이력서 / 포트폴리오를 관리하는 페이지입니다</p></li>
                                <li><p>부적절한 단어나 제목이 들어갈시 약관에의거 수정 및 삭제가 가능합니다</p></li>
                            </ul>
                    </div>
            <hr>
                <!-- <div id="section1"> -->
            <div id="jyh_detail-outline" style="margin-bottom:10px; float:left; width:100%; height:100%;">
                <div id="jyh_detail-table-outline1" style="float:left; width:100%;">
                    <table id="jyh_resume-list" border="1" style="width:100%;">
                            <tr>
                                    <th style="width:80%;">이력서 제목</th>
                                    
                                </tr>
                               <% 
                               if(listCount > 0){
                               for(int i = 0; i < list.size(); i++){ 
                                	HashMap<String, Object> hmap = list.get(i);
                              		System.out.println("listsize : " + list.size());
                              		System.out.println("list.get() : " + list.get(i));
                              		System.out.println("hmap : " + hmap);
                                %>
								<tr>
									<td class="rNo"><%= hmap.get("rTitle") %><label class="rNo1"><%= hmap.get("rNo") %></label></td>
									<!-- <td><input type="button" name="btn1" value="수정"><input type="button" name="btn1" value="삭제"></td> -->
								</tr>
								<% }}else{%>
								<tr>
									<td>이력서가 없습니다.</td>
								</tr>
								<%} %>
                    </table>
                </div>
            </div>

                         <hr style="margin-right:auto; margin-left:auto; margin-top:10px;">
                   
            <%-- 페이지 처리 --%>
      <div class="pagingArea" align="center">
      <% if(listCount > 0){ %>
        <button onclick="location.href='<%=request.getContextPath()%>/jyhDocumentList.me?currentPage=1&userNo=<%= list.get(0).get("perNo") %>'"><<</button>
        <% if(currentPage <= 1) { %>
        <button disabled><</button>
        <% } else { %>
        <button onclick="location.href='<%=request.getContextPath()%>/jyhDocumentList.me?currentPage=<%=currentPage - 1%>' + '&userNo=<%= list.get(0).get("perNo") %>'"><</button>
        <% } %>
        
        <% for(int p = startPage; p <= endPage; p++) { 
        	  if(p == currentPage){
        %>
        		<button disabled><%= p %></button>
        <%
        	  } else {
        %>
        		<button onclick="location.href='<%=request.getContextPath()%>/jyhDocumentList.me?currentPage=<%=p%>' + '&userNo=<%= list.get(0).get("perNo") %>'"><%= p %></button>
        <% 
        	  }
           }
        %>
      	
      	<% if(currentPage >= maxPage) { %>
      	<button disabled>></button>
      	<% } else { %>
      	<button onclick="location.href='<%=request.getContextPath()%>/jyhDocumentList.me?currentPage=<%=currentPage + 1%>' + '&userNo=<%= list.get(0).get("perNo") %>'">></button>
      	<% } %>
      	
      	<button onclick="location.href='<%=request.getContextPath()%>/jyhDocumentList.me?currentPage=<%=maxPage%>' + '&userNo=<%= list.get(0).get("perNo") %>'">>></button>
      	<%} else{%>
            	<button disabled><<</button>
            	<button disabled><</button>
            	<button disabled>></button>
            	<button disabled>>></button>
            <%} %>
      	
      </div><br>

                <!-- <div id="section1"> -->
            <div id="jyh_detail-outline" style="margin-bottom:10px; float:left; width:100%; height:100%;">
                <div id="jyh_detail-table-outline1" style="float:left; width:100%;">
                    <table id="jyh_portfolio-list" border="1" style="width:100%;">
                            <tr>
                                <th style="width:80%;">포트폴리오 제목</th>
                            </tr>
                            <%-- <% 
                               if(listCount > 0){%>
                            <tr>
                                <td class="perNo"><%= list.get(0).get("pTitle") %><label class="perNo1"><%= list.get(0).get("perNo") %></label></td>
								<td><input type="button" name="btn1" value="수정"><input type="button" name="btn1" value="삭제"></td>
                            </tr>
                            <%}else{ %> --%>
                            <tr>
                            	<td>포트폴리오가 없습니다.</td>
                            </tr>
                          <%--   <%} %> --%>
                    </table>
                </div>
            </div>
            
            <!-- </div> -->
        </td>
    <td id="jyh_aside">
    </td>
    </tr>
</table>
    
    
    <%@ include file="../common/cho_footer.jsp" %>
    
    <script>
    	$("#jyh_resume-list tr:not(:first-child) td:nth-of-type(1)").click(function(){
    		comNo = $(this).children().text();
    		console.log(comNo);
    		var w = window.open("<%=request.getContextPath()%>/resumeDetail.re?userNo=<%= list.get(0).get("perNo") %>&memType=1&rNo="+comNo,"_blank");
    		<%-- location.href = "<%=request.getContextPath()%>/jyhSelectOne.me?userNo=" + userNo; --%>
    	});
    	
    	$("#jyh_portfolio-list tr:not(:first-child) td:nth-of-type(1)").click(function(){
    		comNo = $(this).children().text();
    		console.log(comNo);
    		<%-- location.href = "<%=request.getContextPath()%>/jyhSelectOne.me?userNo=" + userNo; --%>
    	});
    
    	$("#jyh_per-top-btn1").click(function(){
         	location.href = "<%=request.getContextPath()%>/jyhSelectOne.me?userNo=<%= userNo %>&memType=1";
        });
    	
    	$("#jyh_per-top-btn2").click(function(){
        	location.href = "<%=request.getContextPath()%>/jyhApplicationList.me?userNo=<%= userNo %>&memType=1";
        });
    	
    	$("#jyh_per-top-btn3").click(function(){
         	location.href = "<%=request.getContextPath()%>/jyhDocumentList.me?userNo=<%=userNo %>&memType=1";
        });
    </script>
    
     <script>
    	$(function(){
    		$("#test").click(function(){
    			var w = window.open("<%=request.getContextPath()%>/jyhDocumentList.me?userNo=<%= userNo %>&memType=1","_blank");
    		});
    	});
    </script>
   
</body>
</html>