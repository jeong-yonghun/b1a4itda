<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.util.ArrayList, java.util.HashMap, com.kh.itda.approval.model.vo.*"%>
<%
	ArrayList<HashMap<String,Object>> list = (ArrayList<HashMap<String, Object>>) request.getAttribute("list");
	System.out.println(list);
	PageInfo pi = (PageInfo) request.getAttribute("pi");
	String pagingUrl = (String) request.getAttribute("pagingUrl");
	int listCount = pi.getListCount();
	int currentPage = pi.getCurrentPage();
	int maxPage = pi.getMaxPage();
	int startPage = pi.getStartPage();
	int endPage = pi.getEndPage();
%>
<!DOCTYPE html>
<html lang="ko">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <style>
        a:link { 
            text-decoration: none;
            color: black;
        }
        a:visited { 
            text-decoration: none;
            color: black;
        }
        .jyh_article1{
            width:840px; 
            height:75px;
        }
        #jyh_nav{
            width:280px;
        }
        #jyh_nav-div{
            height:1000px;
        }
        #jyh_side-menu-per, #jyh_side-menu-com{
            width:80%;
            margin:auto;
        }
        #jyh_side-menu-com{
            margin-top:50px;
        }
        #jyh_side-menu-outline p{
            margin:10px;
        }
        #jyh_side-menu-outline ul{
            list-style:none; 
            padding-left:5px;
        }
        #jyh_side-menu-outline{
            width:75%;
            margin:auto;
            border:1px solid gray;
        }

        #jyh_aside{
            width:280px;
        }
        footer{
            margin-left: auto;
            margin-right: auto;
            width:1400px;
            height:100px;
        }
        #section{
            height:1000px;
        }

        #jyh_outline-table{
            margin:auto; 
            border-collapse:collapse; 
            border-spacing:0;
        }
        #jyh_outline-table td:nth-of-type(1n){
            margin:0;
            padding:0;
        }

        #jyh_search-per{
            padding:5px;
            text-align: left;
            font-weight:bold;
            font-size: 15pt;
        }
        #jyh_page-descript-per{
            width:100%;
            height:100px;
        }
        #jyh_page-descript-per ul{
            margin:0;
            width:85%;
        }
        #jyh_page-descript-per p:nth-of-type(1n){
            margin:8px;
        }
        #jyh_word-list > table{
            margin:auto;
        }
        #jyh_word-list p:nth-of-type(1n){
            padding:10px;
            margin:0;
        }
        #jyh_per-list {
            border-collapse: collapse;
            border: 1px solid #168;
            width:100%;
        }  
        #jyh_per-list th {
            color: #168;
            background: #f0f6f9;
            text-align: center;
        }
        #jyh_per-list th, #jyh_per-list td {
            padding: 10px;
            /* border: 1px solid #ddd; */
        }
        #jyh_per-list th:first-child, #jyh_per-list td:first-child {
            border-left: 0;
        }
        #jyh_per-list th:last-child, #jyh_per-list td:last-child {
            border-right: 0;
        }
        #jyh_per-list tr td:first-child{
            text-align: center;
        }
        .jyh_search-filter{
            border:1px solid gray;
            border-radius: 5px;
        }
        .jyh_search-select{
            height:24px;
        }
        .jyh_search-line, #search{
            vertical-align: middle;
            margin-left:5px;
        }
        .jyh_search-radius{
            border-radius: 5px;
            border: 1px solid gray;
        }
        #search{
            width:200px;
            height:20px;
        }
        #jyh_per-top-btn{
            float: left;
            margin-right:50px;
            margin-left:10px;
        }
        #jyh_per-list tr{
        	text-align:center;
        }
        .hide{
        	display:none;
        }
        .join-date{
        	color:gray;
        }
    </style>
</head>
<body>

<%@ include file="../common/jyh_adminMenubar.jsp" %>

<table id="jyh_outline-table">
    <tr>
        <td id="jyh_nav" style="vertical-align: top;">
               <%@ include file="../common/jyh_adminAnnManageSideMenu.jsp" %>
    </td>
    <td style="width:840px; vertical-align:top;">
            <div class="jyh_article1">
            </div>
                <div id="jyh_page-title-detail">
                        <h3 style="margin-top:0px">마감된된 공모전</h3>
                    </div>
                    <hr style="margin-bottom:10px;">
            <div id="section">
        <div id="jyh_page-descript-per">
            <ul id="jyh_page-descript-per1" style="float:left;">
                    <li><p>공모전 공고의 상태를 확인할수있는 페이지입니다.</p></li>
                    <li><p>부적절한 단어가 있을시 반려 확은 삭제 하시길 바랍니다.</p></li>
                    <li><p>부적절한 채용이나 행위가 있을시 약관의 의거하여 제제 받을수있습니다.</p></li>
                </ul>
        </div>

                <form id="searchRequirement" action="<%= request.getContextPath() %>/jyhSelectProgressHireList.re" method="post">
                <div id="jyh_searchbar-outline">
                    <div id="jyh_search_filter" style="float: right; margin-bottom:10px;">
                        <span class="jyh_search-line"><label>가입일</label></span>
                        <input id="allDay" type="radio" name="dateCondition" checked value=">= '0'" class="jyh_search-radius hide">
                        <label class="jyh_search-radius jyh_search-line join-date" for="allDay">전체</label>
                        <input id="oneWeek" type="radio" name="dateCondition" value="<= '7'" class="jyh_search-radius hide">
                        <label class="jyh_search-radius jyh_search-line join-date" for="oneWeek">1주일 이내</label>
                        <input id="oneMonth" type="radio" name="dateCondition" value="<= '30'" class="jyh_search-radius hide">
                        <label class="jyh_search-radius jyh_search-line join-date" for="oneMonth">1개월 이내</label>
                        <input id="sixMonth" type="radio" name="dateCondition" value="<= '180'" class="jyh_search-radius hide">
                        <label class="jyh_search-radius jyh_search-line join-date" for="sixMonth">6개월 이내</label>
                        
                        <input id="selectCondition" type="text" name="selectCondition" value="" class="hide">
                        <input id="annDivision" type="text" name="annDivision" value="3" class="hide">
                        <input id="annType" type="text" name="annType" value="2" class="hide">
                        
                        <select id="select" class="jyh_search-line jyh_search-select jyh_search-radius">
                            <option value=" '%">전체검색</option>
                            <option value="COM_ID LIKE '%">아이디</option>
                            <option value="COM_NAME LIKE '%">이름</option>
                            <option value="COMPANY_NAME LIKE '%">회사명</option>
                            <option value="PHONE LIKE '%">휴대전화</option>
                            <option value="A_TITLE LIKE '%">공고제목</option>
                        </select>
                        <input type="text" class="jyh_search-radius" id="search" maxlength="50">
                        <input id="confirmBtn" type="button" name="confirmBtn" value="검색" class="jyh_search-radius">
                    </div>
                </div>
			</form>

                <table id="jyh_per-list" border="1" align="center">
                    <tr>
                        <th><input type="checkbox" name="jyh_per-list-check"></th>
                        <th>번호</th>
                        <th>아이디</th>
                        <th>회사명</th>
                        <th>채용공고제목</th>
                        <th>담당자이름</th>
                        <th>담당휴대전화</th>
                        <th>공고시작일</th>
                        <th>공고마감일</th>
                        <th>공고상태</th>
                    </tr>
                    <% for(int i = 0; i < list.size(); i++){ 
                        HashMap<String, Object> hmap = list.get(i);
                        System.out.println("listsize : " + list.size());
                        System.out.println("list.get() : " + list.get(i));
                        System.out.println("hmap : " + hmap);
                     %>
					<tr>
						<td><input type="checkbox" name="jyh_per-pauseList-check"></td>
						<td><%= hmap.get("rnum") %></td>
						<td><%= hmap.get("comId") %></td>
						<td><%= hmap.get("companyName") %></td>
						<td><%= hmap.get("aTitle") %></td>
						<td><%= hmap.get("comName") %></td>
						<td><%= hmap.get("phone") %></td>
						<td><%= hmap.get("aStartDate") %></td>
						<td><%= hmap.get("aEndDate") %></td>
						<td><%= hmap.get("status") %></td>
					</tr>
					<% } %>
                </table>
                <hr style="margin-right:auto; margin-left:auto; margin-top:10px;">

                <%-- 페이지 처리 --%>
      <div class="pagingArea" align="center">
        <button onclick="location.href='<%=request.getContextPath()%><%= pagingUrl %>currentPage=1'"><<</button>
        <% if(currentPage <= 1) { %>
        <button disabled><</button>
        <% } else { %>
         <button onclick="location.href='<%=request.getContextPath()%><%= pagingUrl %>currentPage=<%=currentPage - 1%>'"><</button>
        <% } %>
        
        <% for(int p = startPage; p <= endPage; p++) { 
        	  if(p == currentPage){
        %>
        		<button disabled><%= p %></button>
        <%
        	  } else {
        %>
        		<button onclick="location.href='<%=request.getContextPath()%><%= pagingUrl %>currentPage=<%=p%>'"><%= p %></button>
        <% 
        	  }
           }
        %>
      
      	
      	<% if(currentPage >= maxPage) { %>
      	<button disabled>></button>
      	<% } else { %>
      	<button onclick="location.href='<%=request.getContextPath()%><%= pagingUrl %>currentPage=<%=currentPage + 1%>'">></button>
      	<% } %>
      	
      	<button onclick="location.href='<%=request.getContextPath()%><%= pagingUrl %>currentPage=<%=maxPage%>'">>></button>
      </div>
      
      
            </div>
                <hr style="margin-right:auto; margin-left:auto; margin-top:10px;">
    </td>
    <td id="jyh_aside">
    </td>
    </tr>
</table>
    
    
   <%@ include file="../common/cho_footer.jsp" %>
   
   <script>
    	$(function(){
    		$("#confirmBtn").click(function(){
    			
    			$("#selectCondition").val($("#select option:selected").val() + $("#search").val() +"%' ");
    			
    			$("#searchRequirement").submit();
    		});
    	});
    </script>
   
</body>
</html>