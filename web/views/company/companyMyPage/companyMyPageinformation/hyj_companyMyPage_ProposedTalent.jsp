<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <script src="https://ajax.googleapis.hyj_com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <style>
          #cho_middleMenubar{
        margin-left: auto;
            margin-right: auto;
           width:1400px;
           height:220px;
           float:center;
       }
       .cho_h3{
           text-align: center;
           font-weight: bold;
           font-family: 'Noto Sans KR', sans-serif;
           color: #7a7a7a;
       }
       .cho_div{
           background-color: white;
           
         
       }
       .cho_menubar{
         border-top:solid #d8d8d8;
         border-bottom:solid #d8d8d8;

       }
       .cho_menu{
          
           height: 95%;
           width:14%;
           float:left;
       }
         div{
            text-align: center;
            color:black;
        }

        header{
            margin-left: auto;
            margin-right: auto;
            width:1400px;
            height:100px;
        }
        #contents{
            margin-left: auto;
            margin-right: auto;
            width:1400px;
            height:1400px;
            background-color: rgb(255, 255, 255);
        }
        section{
            width:80%;
            height:100%;
            background-color: rgb(255, 255, 255);
            float:left;
        }
        #footerArea{
        margin-left: auto;
            margin-right: auto;
            width:1400px;
            height:150px;  
       }
       
        #hyj_body{
            width:60%; 
            height:80%;   
            display : inline-block; 
         
   
        }
        td{
            width:10%;
            height:57px;
            font-weight: bold;
        }

        #frombtn{
            width: 60%;
            height: 80%; 
            color:white;
            background: rgb(67, 67, 255);
            border-radius: 10px;
        }
        #hyj_checkbtn{
            width: 30%;
            height: 60%; 
            color:white;
            background: rgb(67, 67, 255);
            border-radius: 10px;
        }
        #hyj_td{
            color:white;
            background: rgb(160, 160, 255);
            border-radius: 10px;
            width:10%;
            height:57px;
        }
        #hyj_userid, #hyj_password, #hyj_com, #hyj_number, #hyj_dday, #hyj_username, #hyj_money, #hyj_person, #hyj_ssul, #hyj_ssogae{
            width:100%;
            height:100%;
            border-radius: 10px;
        }
        .hyj_userid, .hyj_password, .hyj_com, .hyj_number, .hyj_dday, .hyj_username, .hyj_money, .hyj_person, .hyj_ssul, .hyj_ssogae{
            width: 60%;
            height: 80%; 
            border: 2px solid rgb(194, 194, 194);
            border-radius: 10px;
        }
        #hyj_phone, #hyj_email{
            width:67%;
            height:100%;
            border-radius: 10px;
        }
        #hyj_image{
            width:100%;
            height:100%;
        }
    </style>
</head>
<body>
    <header id="head" class="test1"> <!-- 메뉴바 시작 -->
        <div style="height:60%; margin:0px; padding:0px;">
            <div style="width:7.5%; height:100%;  float:left;"></div><!-- 빈 공간 -->
            <div style="width:7.5%; height:100%;  float:left;"></div><!-- 로고 들어갈 자리 -->
            <div style="width:25%; height:100%;  float:left;"><!-- 검색 들어갈 자리 시작 -->
                <div style="height:50%; margin-top:3.86%;">
                    <input type="text" name="search" maxlength="100" style="height:80%; width:60%; float: left;">
                    <div style="border:solid; width:10%; height:85%; background: blue; float:left;"><!-- 알림 이미지 시작 -->
                            <img src="sample/image/bell.png" style="width: 100%; max-width: 25px; vertical-align: middle" />
                    </div><!-- 알림 이미지 끝 -->
                </div>
            </div><!-- 검색 들어갈 자리 끝 -->
            <div style="width:45%; height:100%;  float:left;"></div><!-- 빈공간 -->
            <div style="width:15%; height:100%;  float:left;"><!-- 알림, 마이페이지, 로그인, 로그아웃 자리 시작 -->
                <div style="width:100%; height:50%;  float:left; margin-top:15px;">
                    <div style="width:30%; height:100%;  float:left; margin-left:auto; margin-right:auto;"><!-- 알림 이미지 시작 -->
                        <img src="sample/image/bell.png" style="width: 100%; max-width: 30px; vertical-align: middle" />
                    </div><!-- 알림 이미지 끝 -->
                    <div style="width:35%; height:21.6px;  float:left; margin-left:auto; margin-right:auto; margin-top:2.2px;">
                        <label style="color:#5A84F1;">My Page</label>
                    </div>
                    <div style="width:35%; height:21.6px; float:left; margin-left:auto; margin-right:auto; margin-top:4.2px;">
                        <label style="color:#5A84F1">로그인</label>
                    </div>
                </div>
            </div><!-- 알림, 마이페이지, 로그인, 로그아웃 자리 끝 -->
        </div>
        <div style="height:40%; background-color:#4876EF;">
            <div style="width:30%; height:100%; float:left;"></div>
            <div style="width:40%; height:100%; float:left;">
                <div style="width:30%; height:100%; float:left;">
                    <div style="font-weight: bold; color:white; width:80%; height:50%; margin-left:auto; margin-right:auto; margin-top:10px;">
                        개발자 채용
                    </div>
                </div>
                <div style="width:25%; height:100%; float:left; margin-left:23px; margin-right:23px;">
                    <div style="font-weight: bold; color:white; width:80%; height:50%; margin-left:auto; margin-right:auto; margin-top:10px;">
                        공모전
                    </div>
                </div>
                <div style="width:35%; height:100%; float:right;">
                    <div style="font-weight: bold; color:white; width:80%; height:50%; margin-left:auto; margin-right:auto; margin-top:10px;">
                        개발자 커뮤니티
                    </div>
                </div>
            </div>
            <div style="width:30%; height:100%; float:left;" ></div>
        </div>
    </header><!-- 메뉴바 끝 -->
    


    <div id="contents" class="test1"><!-- 전체 화면 div 시작 -->
        <section id="section" class="test1">
   
            <div id="cho_middleMenubar" >
                <div class="cho_div" style="height:150px; background:rgb(255, 255, 255);"><img id="hyj_image" src="images.jpg"></div>
                <div class="cho_menubar"style="height:60px;"><!-- 개인 마이페이지 상단 탭 영역 설정 -->
                    <div class="cho_white" style="width:15%; height:100%; float:left;"></div>
                    <div  class="cho_center"style="width:70%; height:100%; float:left; ">
                        <div class="cho_menu" id="menu1" onclick="goHome()" >
                               <h3 class="cho_h3"> 기업 HOME</h3>
                        </div>
                        <div class="cho_menu" id="menu2" onclick="goDetail()">
                            
                               <h3 class="cho_h3"> 기업 상세정보</h3>
                            
                        </div>
                        <div class="cho_menu" id="menu3" onclick="goResume()" >
                             <h3 class="cho_h3">채용공고관리</h3>
                        </div>
                        <div class="cho_menu" id="menu4" onclick="goVolunteer()">
                             <h3 class="cho_h3">공모전 관리</h3>
                        </div>
                        <div class="cho_menu" id="menu5" onclick="goPortfolio()" >
                             <h3 class="cho_h3">결제내역</h3>
                        </div>
                        <div class="cho_menu" id="menu6" onclick="goMywriting()" >
                             <h3 class="cho_h3">제안한인재</h3>
                        </div>
                        <div class="cho_menu" id="menu7" onclick="goFavorites()">
                            <h3 class="cho_h3">나의즐겨찾기</h3>
                        </div>
                    </div>
                 </div>
            <div style="height:100%; background:rgb(255, 255, 255); display: inline-block;">
                <div style="width:60%; height:7%; background:rgb(255, 255, 255); display: inline-block;">
                    <p style="text-align: left; margin-top: 40px; font-size: 24px; font-weight: bold;">제안한 인재</p></div>
                    <hr style="width:65%; border: 2px solid rgb(57, 70, 255);">
                    <table style="width:60%; height:60%; background: rgb(255, 255, 255); margin-left: auto; margin-right: auto;">
                        <tr>
                            <td>
                                <table style="box-shadow: 0px 3px 5px 3px rgb(194, 194, 194); margin-top:50px; border: 2px solid rgb(199, 199, 199); background: rgb(255, 255, 255); text-align: center; margin-left: auto; margin-right: auto;"> 
                                    <tr>
                                        <th style="border-bottom: 1px solid rgb(202, 202, 202); background: skyblue;">이름</th>
                                        <th style="border-bottom: 1px solid rgb(202, 202, 202); background: skyblue;">제안한 날짜</th>
                                        <th style="border-bottom: 1px solid rgb(202, 202, 202); background: skyblue;">열람여부</th>
                                        <th style="border-bottom: 1px solid rgb(202, 202, 202); background: skyblue;">상태</th>
                                    </tr>
                                    <tr>
                                        <td style="border-bottom: 1px solid black; ">김선빈</td>
                                        <td style="border-bottom: 1px solid black;">20.01.20</td>
                                        <td style="border-bottom: 1px solid black;">Y</td>
                                        <td style="border-bottom: 1px solid black;">승낙</td>
                                    </tr>
                                    <tr>
                                        <td style="border-bottom: 1px solid black;">황윤중</td>
                                        <td style="border-bottom: 1px solid black;">20.01.19</td>
                                        <td style="border-bottom: 1px solid black;">N</td>
                                        <td style="border-bottom: 1px solid black;">거절</td>
                                    </tr>
                                    <tr>
                                        <td style="border-bottom: 1px solid black;">황윤중</td>
                                        <td style="border-bottom: 1px solid black;">20.01.19</td>
                                        <td style="border-bottom: 1px solid black;">N</td>
                                        <td style="border-bottom: 1px solid black;">거절</td>
                                    </tr>
                                    <tr>
                                        <td style="border-bottom: 1px solid black;">황윤중</td>
                                        <td style="border-bottom: 1px solid black;">20.01.19</td>
                                        <td style="border-bottom: 1px solid black;">N</td>
                                        <td style="border-bottom: 1px solid black;">거절</td>
                                    </tr>
                                    <tr>
                                        <td style="border-bottom: 1px solid black;">황윤중</td>
                                        <td style="border-bottom: 1px solid black;">20.01.19</td>
                                        <td style="border-bottom: 1px solid black;">N</td>
                                        <td style="border-bottom: 1px solid black;">거절</td>
                                    </tr>
                                    <tr>
                                        <td style="border-bottom: 1px solid black;">황윤중</td>
                                        <td style="border-bottom: 1px solid black;">20.01.19</td>
                                        <td style="border-bottom: 1px solid black;">N</td>
                                        <td style="border-bottom: 1px solid black;">거절</td>
                                    </tr>
                                    <tr>
                                        <td style="border-bottom: 1px solid black;">황윤중</td>
                                        <td style="border-bottom: 1px solid black;">20.01.19</td>
                                        <td style="border-bottom: 1px solid black;">N</td>
                                        <td style="border-bottom: 1px solid black;">거절</td>
                                    </tr>
                                    <tr>
                                        <td style="border-bottom: 1px solid black;">황윤중</td>
                                        <td style="border-bottom: 1px solid black;">20.01.19</td>
                                        <td style="border-bottom: 1px solid black;">N</td>
                                        <td style="border-bottom: 1px solid black;">거절</td>
                                    </tr>
                                    <tr>
                                        <td style="border-bottom: 1px solid black;">황윤중</td>
                                        <td style="border-bottom: 1px solid black;">20.01.19</td>
                                        <td style="border-bottom: 1px solid black;">N</td>
                                        <td style="border-bottom: 1px solid black;">거절</td>
                                    </tr>
                                    <tr>
                                        <td style="border-bottom: 1px solid black;">황윤중</td>
                                        <td style="border-bottom: 1px solid black;">20.01.19</td>
                                        <td style="border-bottom: 1px solid black;">N</td>
                                        <td style="border-bottom: 1px solid black;">거절</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    
        </section>



        

    </div><!-- 전체 화면 div 끝 -->

    <footer id="footer" class="test1"><!-- footer 시작-->
        <div id="footerArea" style="padding: 0px;">
            <div id="footerLogoArea" style="height: 130px; width: 15%;display: inline-block;">
                <img src="image/logo.PNG"style="height:100%; width:100%;">
            </div>
            <div id="footerTextArea"style="height:50px;white:1000px; backgroundcolor:white;display: inline-block;padding-top:70px;">
            <h4 style="color: #8F8F8F;display: inline;margin-right: 30px;font-family: 'Noto Sans KR', sans-serif;">서비스 이용약관</h4>
            <h4 style="color: #8F8F8F;display: inline;margin-right: 30px;font-family: 'Noto Sans KR', sans-serif;">개인정보 처리방침</h4>
            <h4 style="color: #8F8F8F;display: inline;font-family: 'Noto Sans KR', sans-serif; margin-bottom: 10px;">(주)B1A4프로젝트  |  대표자: 정용훈  | 개인정보 보호책임자: 황윤중  |  사업자 등록번호: 111-00-000000 <br><br>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            서울 특별시 성동구 청계천로 12가길 22-6 2층  |  hyj_email: tkznfk1402@naver.hyj_com  |  고객센터: 1588-1500</h4>
            </div>
            
        </div>
    </footer><!-- footer끝 -->

    <script>
        function goHome(){
            var h3=$('#menu1').children('h3');
            $("#menu1").css("border-bottom","solid #5A84F1");
            h3.css("color","#5A84F1");
           // location.href="";
        }
        function goDetail(){
            var h3=$('#menu2').children('h3');
            $("#menu2").css("border-bottom","solid #5A84F1");
            h3.css("color","#5A84F1");
           // location.href="";
        }
        function goResume(){
            var h3=$('#menu3').children('h3');
            $("#menu3").css("border-bottom","solid #5A84F1");
            h3.css("color","#5A84F1");
           // location.href="";
        }
        function goVolunteer(){
            var h3=$('#menu4').children('h3');
            $("#menu4").css("border-bottom","solid #5A84F1");
            h3.css("color","#5A84F1");
           // location.href="";
        }
        function goPortfolio(){
            var h3=$('#menu5').children('h3');
            $("#menu5").css("border-bottom","solid #5A84F1");
            h3.css("color","#5A84F1");
           // location.href="";
        }
        function goMywriting(){
            var h3=$('#menu6').children('h3');
            $("#menu6").css("border-bottom","solid #5A84F1");
            h3.css("color","#5A84F1");
           // location.href="";
        }
        function goFavorites(){
            var h3=$('#menu7').children('h3');
            $("#menu7").css("border-bottom","solid #5A84F1");
            h3.css("color","#5A84F1");
           // location.href="";
        }

    </script>
</body>
</html>