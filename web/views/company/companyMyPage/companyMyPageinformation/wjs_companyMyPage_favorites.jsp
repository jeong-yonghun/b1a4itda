<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <style>
       
        .wjs_mainTable{
            margin-left: auto;
            margin-right: auto;
            width: 1400px;
            height:1000px;
        }
     
        
        .wjs_tablefavorites{
            width: 840px;
          	vertical-align: top;
            
        }
        
        .wjs_div1{
            border-top: 6px solid #5A84F1;
            height: 150px;
        }
        .portfolioMainTable_tr_td_section_div2{
            border-top: 6px solid #5A84F1;
            height: 580px;
        }
        .portfolioMainTable_tr_td_section_botton{
            float: right;
            width: 210px;
            height: 60px;
            border-radius: 0.5pc;
            background-color: #5A84F1;
            color: white;
            font-size: 15pt;
        }
  
       #wjs_table2{
       		width: 900px;
       		align-content: center;
       		border: 1px solid black;
       }
       #wjs_name{
       		width: 200px;
       		border: 1px solid black;
       		text-align: center;
       }
        #wjs_resume{
       		width: 300px;
       		border: 1px solid black;
       		text-align: center;
       }
        #wjs_favorites{
       		width: 400px;
       		border: 1px solid black;
       		text-align: center;
       }
       
       #wjs_sort{
       		float: right;
       	 background-color: rgba(0,0,0,0);
       	 padding: 5px;
       	 margin-bottom: 6px;


       }
       #sort1{
       	 margin-right:-4px;
       	 border: 0;
       	 outline: 0;
       	 background: white;
       }
       #sort2{
       	  margin-left:-3px;
       	   border: 0;
       	 outline: 0;
       	 background: white;
       }
    </style>
</head>
<body>

<%@include file="/views/common/menubar.jsp" %>
<%@include file="/views/person/personMyPage/personMyPageMenubar.jsp" %>





    <table class="wjs_mainTable">
        <tr class="portfolioMainTable_tr">
            <td class="wjs_nav" style="width:280px; vertical-align:top">
                <!-- nav 개인프로필 영역이랑 사이드 메뉴바인클루드 하기 -->
              	<%@include file="/views/person/person_common/sideMenubarAndUserInfo.jsp" %>
            </td>
            <td class="wjs_tablefavorites">
                <h1 style="font-weight: bold;">나의 즐겨찾기</h1>
                <div class="wjs_div1"></div>
             
                
                
                <div id="wjs_sort">
                	<button id="sort1">최신순</button>
                	<button id="sort2">등록순</button>	
                </div>
                <table id="wjs_table2">
                	<tr>
                		<td id="wjs_name">이름</td>
                		<td id="wjs_resume">이력서</td>
                		<td id="wjs_favorites">즐겨찾기 해제</td>
                	</tr>
                </table>
                
                
                
             
		
                <br><br><br><br>

			 </td>
             
			 <td class="wjs_aside" style="width:280px;">
            
            </td>
	    </tr>
    </table>








<%@include file="/views/common/footer.jsp" %>



</body>
</html>