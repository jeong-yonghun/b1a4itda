<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://t1.daumcdn.net/mapjsapi/bundle/postcode/prod/postcode.v2.js"></script>
    <script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.0.min.js" ></script>

    <style>
       #cho_middleMenubar{
        margin-left: auto;
            margin-right: auto;
           width:1400px;
           height:220px;
           float:center;
       }
       .cho_h3{
           text-align: center;
           font-weight: bold;
           font-family: 'Noto Sans KR', sans-serif;
           color: #7a7a7a;
       }
       .cho_div{
           background-color: white;
           
         
       }
       .cho_menubar{
         border-top:solid #d8d8d8;
         border-bottom:solid #d8d8d8;

       }
       .cho_menu{
          
           height: 95%;
           width:14%;
           float:left;
       }
         div{
            text-align: center;
            color:black;
        }

        header{
            margin-left: auto;
            margin-right: auto;
            width:1400px;
            height:100px;
        }
        #contents{
            margin-left: auto;
            margin-right: auto;
            width:1400px;
            height:1400px;
            background-color: rgb(255, 255, 255);
        }
        section{
            width:80%;
            height:100%;
            background-color: rgb(255, 255, 255);
            float:left;
        }
        #footerArea{
        margin-left: auto;
            margin-right: auto;
            width:1400px;
            height:150px;  
       }
       
        #hyj_body{
            width:60%; 
            height:80%;   
            display : inline-block; 
         
   
        }
        td{
            width:10%;
            height:57px;
            font-weight: bold;
        }

        #frombtn{
            width: 60%;
            height: 80%; 
            color:white;
            background: rgb(67, 67, 255);
            border-radius: 10px;
        }
        #hyj_email_btn, #hyj_phone_btn{
            width: 30%;
            height: 60%; 
            color:white;
            background: rgb(67, 67, 255);
            border-radius: 10px;
        }
        #hyj_td{
            color:white;
            background: rgb(160, 160, 255);
            border-radius: 10px;
            width:10%;
            height:57px;
        }
        #hyj_userid, #perPwd1 , #perPwd2 , #hyj_com, #hyj_number,
        #hyj_dday, #hyj_comusername, #hyj_money, #hyj_personcnum, #hyj_address,
        #hyj_service, #hyj_invest, #hyj_url
        {
        
            width:100%;
            height:100%;
            border-radius: 10px;
        }
        .hyj_userid, .hyj_password, .hyj_com, .hyj_number, .hyj_dday, .hyj_comusername, .hyj_money, .hyj_personcnum, .hyj_address, .hyj_service{
            width: 60%;
            height: 80%; 
            border: 2px solid rgb(194, 194, 194);
            border-radius: 10px;
        }
        #hyj_phone, #hyj_email{
            width:67%;
            height:100%;
            border-radius: 10px;
        }
        #hyj_image{
            width:100%;
            height:100%;
        }
        #popupDiv, #popupDiv1 {  /* 팝업창 css */
          top : 0px;
          position: absolute;
          background: white;
          width: 500px;
          height: 350px;
          display: none; 
       }
        ul {
            list-style:none;
            margin:0;
            padding:0;
        }

        li {
            float: left;
            margin-right: 10px;
        }
       #popup_mask,#popup_mask1 { /* 팝업 배경 css */
           position: fixed;
           width: 100%;
           height: 1000px;
           top: 0px;
           left: 0px;
            display: none; 
            background-color:#000;
            opacity: 0.8;
       }
    </style>
</head>
<body>
    
    
	<%@ include file="/views/company/company_common/sb_companyMenubar.jsp" %>

    <div id="contents" class="test1"><!-- 전체 화면 div 시작 -->
        <section id="section" class="test1">
   
            <div id="cho_middleMenubar" >
                <div class="cho_div" style="height:150px; background:rgb(255, 255, 255);"><img style="height:150px; width:100%;"src="/itda/image/dae.png"></div>
                <div class="cho_menubar"style="height:60px;"><!-- 개인 마이페이지 상단 탭 영역 설정 -->
                    <div class="cho_white" style="width:15%; height:100%; float:left;"></div>
                    <div  class="cho_center"style="width:70%; height:100%; float:left; ">
                        <div class="cho_menu" id="menu1" onclick="goHome()" >
                               <h3 class="cho_h3"> 기업 HOME</h3>
                        </div>
                        <div class="cho_menu" id="menu2" onclick="goDetail()">
                            <h3 class="cho_h3"> 기업 상세정보</h3>
                        </div>
                        <div class="cho_menu" id="menu3" onclick="goResume()" >
                             <h3 class="cho_h3">채용공고관리</h3>
                        </div>
                        <div class="cho_menu" id="menu4" onclick="goVolunteer()">
                             <h3 class="cho_h3">공모전 관리</h3>
                        </div>
                        <div class="cho_menu" id="menu5" onclick="goPortfolio()" >
                             <h3 class="cho_h3">결제내역</h3>
                        </div>
                        <div class="cho_menu" id="menu6" onclick="goMywriting()" >
                             <h3 class="cho_h3">제안한인재</h3>
                        </div>
                        <div class="cho_menu" id="menu7" onclick="goFavorites()">
                            <h3 class="cho_h3">나의즐겨찾기</h3>
                        </div>
                    </div>
                 </div>
            <div style="height:100%; background:rgb(255, 255, 255); display: inline-block;">
                <div style="width:60%; height:7%; background:rgb(255, 255, 255); display: inline-block;">
                    <p style="text-align: left; margin-top: 40px; font-size: 24px; font-weight: bold;">기업 상세정보</p></div>
                    <hr style="width:65%; border: 2px solid rgb(57, 70, 255);">
                    <form id="hyj_test01" action="<%=request.getContextPath()%>/companymoreinformation.co" method="post">
                    <div id="hyj_body">
                        <table>
                            <tr>
                                <td><input type="hidden" value="<%= loginUser.getComNo() %>" name="comno"></td>
                            </tr>
                            <tr>
                                <td id="hyj_td">아이디</td>
                                <td><input type="text" name="hyj_userid" id="hyj_userid" value="<%= loginUser.getComId() %>" disabled></td>
                                
                            </tr>
                            <tr>
                                <td id="hyj_td">비밀번호</td>
                                <td><input type="password" name="perPwd1" id="perPwd1"></td>
                            </tr>
                            <tr>
                                <td id="hyj_td">비밀번호확인</td>
                                <td><input type="password" name="perPwd2" id="perPwd2"></td>
                            </tr>
                            <tr>
                                <td id="hyj_td">회사상호</td>
                                <td><input type="text" name="hyj_com" id="hyj_com" value="<%= loginUser.getCompanyName() %>"></td>
                            </tr>
                            <tr>
                                <td id="hyj_td">사업자 번호</td>
                                <td><input type="text" name="hyj_number" id="hyj_number" value="<%= loginUser.getComNum() %>" disabled></td>
                            </tr>
                            <tr>
                                <td id="hyj_td">인사담당자명</td>
                                <td><input type="text" name="hyj_comusername" id="hyj_comusername" value="<%= loginUser.getComName() %>"></td>
                            </tr>
                            <tr>
                                <td id="hyj_td">인사담당자연락처</td>
                                <td><input type="text" name="hyj_phone" id="hyj_phone" value="<%= loginUser.getPhone() %>">
                                    <input id="hyj_phone_btn" type="button" name="phonebtn" value="인증번호"></td>
                            </tr>
                            <tr>
                                <td id="hyj_td">회사이메일</td>
                                <td><input type="email" name="hyj_email" id="hyj_email" value="<%= loginUser.getEmail() %>">
                                    <input id="hyj_email_btn" type="button" name="emailbtn" value="인증번호"></td>
               
                            </tr>
                            <tr>
                                <td id="hyj_td">투자액</td>
                                <% if(loginUser.getInvest() == null){ %>
                                <td><input type="text" name="hyj_invest" id="hyj_invest"></td>
                                <% }else{ %>
                                <td><input type="text" name="hyj_invest" id="hyj_invest" value="<%= loginUser.getInvest() %>"></td>
                                <% } %>
                            </tr>
                            <tr>
                                <td id="hyj_td">사원수</td>
                                <% if(loginUser.getNumEmp() == null){ %>
                                <td><input type="text" name="hyj_personcnum" id="hyj_personcnum"></td>
                                <%}else{ %>
                                <td><input type="text" name="hyj_personcnum" id="hyj_personcnum" value="<%= loginUser.getNumEmp() %>"></td>
                                <%} %>
                            </tr>
                            <tr>
                                <td id="hyj_td">연매출액</td>
                                <% if(loginUser.getSales() == null) {%>
                                <td><input type="text" name="hyj_money" id="hyj_money"></td>
                                <%} else {%>
                                <td><input type="text" name="hyj_money" id="hyj_money" value="<%= loginUser.getSales() %>"></td>
                                <%} %>
                            </tr>
                            <tr>
                                <td id="hyj_td">홈페이지</td>
                                <td><input type="url" name="hyj_url" id="hyj_url" value="<%= loginUser.getComUrl() %>"></td>
                            </tr>
                            <tr>
                                <td id="hyj_td">주소</td>
                                <td><input type="text" name="hyj_address" id="hyj_address" ></td>
                            </tr>  
                            <tr>
                                <td></td>
                                <td><button id="frombtn">수정완료</button></td>
                            </tr>
                        </table>
                    </div>
                    </form>
        </section>



	 <div id ="popup_mask" ></div>
          <div id="popupDiv">
             <div class="phone-sms-area" style="width:100%; height:100%; position: relative;">
                <h4 align="center" style="margin:auto; margin-top:45px;">인증번호 발송 안내</h4>
                <hr style="width:30%;">
                <div style="width:100%; height:39px; margin-top:40px; margin-bottom:40px;">
                <ul style="display: table; margin: auto; margin-left:7%; padding:0; float:left; width:76%;">
                   <li><h4 style="margin:auto;  margin-top:10%; color:gray; vertical-align:middle">인증번호 입력</h4></li>
                   <li><input id="phone-sms" type="text" name="phoneSms" placeholder="인증번호"  style="vertical-align:middle;"></li>
                </ul>
                   <span style="width:20%;"><p id="timer" style="color:red;  margin:0; margin-top:8.7px; width:10%; height:50%; float:left;">2:59</p></span>
                 </div>
                 <div style="width:100%; height:50px; color:gray; text-align:center; margin-bottom:30px;">
                    <h5 id="timerOver1">제한 시간 안에 인증번호를 입력해주세요.<br>시간 초과 시 창이 닫힙니다.</h5>
                    <h5 id="timerOver2" style="display:none; color:red;">인증번호가 맞지 않습니다.</h5>
                 </div>
                 <div id="btn-area" style="width:40%; height:38px; margin:auto; text-align:center;" >
                    <input id="phone-sms-btn" type="button" name="phone-sms-btn" value="확인">
                    <input id="popCloseBtn" type="button" name="popCloseBtn" value="닫기" style="background:gray;">
                 </div>
              </div>
          </div>

		  <div id ="popup_mask1" ></div>
          <div id="popupDiv1">
             <div class="phone-sms-area1" style="width:100%; height:100%; position: relative;">
                <h4 align="center" style="margin:auto; margin-top:45px;">인증번호 발송 안내</h4>
                <hr style="width:30%;">
                <div style="width:100%; height:39px; margin-top:40px; margin-bottom:40px;">
                <ul style="display: table; margin: auto; margin-left:7%; padding:0; float:left; width:76%;">
                   <li><h4 style="margin:auto;  margin-top:10%; color:gray; vertical-align:middle">인증번호 입력</h4></li>
                   <li><input id="phone-sms1" type="text" name="phoneSms" placeholder="인증번호"  style="vertical-align:middle;"></li>
                </ul>
                   <span style="width:20%;"><p id="timer" style="color:red;  margin:0; margin-top:8.7px; width:10%; height:50%; float:left;">2:59</p></span>
                 </div>
                 <div style="width:100%; height:50px; color:gray; text-align:center; margin-bottom:30px;">
                    <h5 id="timerOver11">제한 시간 안에 인증번호를 입력해주세요.<br>시간 초과 시 창이 닫힙니다.</h5>
                    <h5 id="timerOver21" style="display:none; color:red;">인증번호가 맞지 않습니다.</h5>
                 </div>
                 <div id="btn-area1" style="width:40%; height:38px; margin:auto; text-align:center;" >
                    <input id="phone-sms-btn1" type="button" name="phone-sms-btn" value="확인">
                    <input id="popCloseBtn1" type="button" name="popCloseBtn" value="닫기" style="background:gray;">
                 </div>
              </div>
          </div>
        

    </div><!-- 전체 화면 div 끝 -->
    
 	<%@ include file="/views/common/cho_footer.jsp" %> 
 	
    <script>
    
   
        function goHome(){
            var h3=$('#menu1').children('h3');
            $("#menu1").css("border-bottom","solid #5A84F1");
            h3.css("color","#5A84F1");
           // location.href="";
        }
        function goDetail(){
            var h3=$('#menu2').children('h3');
            $("#menu2").css("border-bottom","solid #5A84F1");
            h3.css("color","#5A84F1");
           // location.href="";
        }
        function goResume(){
            var h3=$('#menu3').children('h3');
            $("#menu3").css("border-bottom","solid #5A84F1");
            h3.css("color","#5A84F1");
           // location.href="";
        }
        function goVolunteer(){
            var h3=$('#menu4').children('h3');
            $("#menu4").css("border-bottom","solid #5A84F1");
            h3.css("color","#5A84F1");
           // location.href="";
        }
        function goPortfolio(){
            var h3=$('#menu5').children('h3');
            $("#menu5").css("border-bottom","solid #5A84F1");
            h3.css("color","#5A84F1");
           // location.href="";
        }
        function goMywriting(){
            var h3=$('#menu6').children('h3');
            $("#menu6").css("border-bottom","solid #5A84F1");
            h3.css("color","#5A84F1");
           // location.href="";
        }
        function goFavorites(){
            var h3=$('#menu7').children('h3');
            $("#menu7").css("border-bottom","solid #5A84F1");
            h3.css("color","#5A84F1");
           // location.href="";
        }
		function hyj_btn1(){
			$("#hyj_test01").submit();
		}; 

		
		
	      //=====================================================================================
	          var randomVal;
	       $("#phone-sms-btn").on("click", function(){ // 인증번호 팝업창의 확인 버튼을 눌렀을 경우
	                           
	            var userSms = $("#phone-sms").val();
	                           
	             if(userSms == randomVal){          //인증번호가 다를 경우
	              console.log("같다");
	              clearInterval(timer);
	               min = 2;
	               sec = 59;
	                $("#popup_mask").css("display","none"); //팝업창 뒷배경 display none
	                   $("#popupDiv").css("display","none"); //팝업창 display none
	                   $("body").css("overflow","auto"); //body 스크롤바 생성
	                 $("#timer").show();
	                 $("#timerOver1").show();
	                $("#timerOver2").hide();
	                   alert("인증성공");
	                   
	                    console.log($("#phone-sms").val(""));
	                 }else{                        //인증번호가 같은 경우
	                 console.log("다르다");
	               $("#timerOver2").show();
	              $("#timerOver1").hide();
	             }
	              
	          });
		//==============================================================================
		
			   var randomVal;
	       $("#phone-sms-btn1").on("click", function(){ // 인증번호 팝업창의 확인 버튼을 눌렀을 경우
	                           
	            var userSms = $("#phone-sms1").val();
	                           
	             if(userSms == randomVal){          //인증번호가 다를 경우
	              console.log("같다");
	              clearInterval(timer);
	               min = 2;
	               sec = 59;
	                $("#popup_mask1").css("display","none"); //팝업창 뒷배경 display none
	                   $("#popupDiv1").css("display","none"); //팝업창 display none
	                   $("body").css("overflow","auto"); //body 스크롤바 생성
	                 $("#timer1").show();
	                 $("#timerOver1").show();
	                $("#timerOver2").hide();
	                   alert("인증성공");
	                   
	                    console.log($("#phone-sms1").val(""));
	                 }else{                        //인증번호가 같은 경우
	                 console.log("다르다");
	               $("#timerOver2").show();
	              $("#timerOver1").hide();
	             }
	              
	          });
			
			
	
			
			
		   $("#hyj_email_btn").on("click", function(){
         
          var getMail = RegExp(/^[A-Za-z0-9_\.\-]+@[A-Za-z0-9\-]+\.[A-Za-z0-9\-]+/);
          var email = $("#hyj_email").val();
          min = 2;
          sec = 59;
 
/*           var email = $(this).parent().children("input:first-of-type").val(); */
       //--------------------------------------이메일 인증 시작-------------------------------------------
         
            if(getMail.test(email)){
               var timer = setInterval(function(){
                  if(sec > 10){
                     $("#timer").text(String(min) + ":" + String(sec));
                     sec--;
                  }else if(sec >= 0 ){
                     $("#timer").text(String(min) + ":0" + String(sec));
                     sec--;
                  }else if(min != 0){
                     min--;
                     sec = 60;
                  }else{
                     clearInterval(timer);
                     min = 2;
                     sec = 59;
                     $("#popup_mask").css("diplay", "none");
                           $("#popupDiv").css("display","none"); //팝업창 display none
                           $("body").css("overflow","auto");//body 스크롤바 생성
                           $("#timer").show();
                           $("#timerOver1").show();
                          $("#timerOver2").hide();
                        }
                     }, 1000);
                $("#popupDiv").css({
                        "top": (($(window).height()-$("#popupDiv").outerHeight())/2+$(window).scrollTop())+"px",
                        "left": (($(window).width()-$("#popupDiv").outerWidth())/2+$(window).scrollLeft())+"px"
                        //팝업창을 가운데로 띄우기 위해 현재 화면의 가운데 값과 스크롤 값을 계산하여 팝업창 CSS 설정
                     }); 
                    
                    $("#popup_mask").css("display","block"); //팝업 뒷배경 display block
                    $("#popupDiv").css("display","block"); //팝업창 display block
                    
                    $("body").css("overflow","hidden");//body 스크롤바 없애기
                
                $("#popCloseBtn").click(function(event){
                    $("#popup_mask").css("display","none"); //팝업창 뒷배경 display none
                    $("#popupDiv").css("display","none"); //팝업창 display none
                    $("body").css("overflow","auto");//body 스크롤바 생성
                    $("#timer").show();
                    $("#timerOver1").show();
                   $("#timerOver2").hide();
                   clearInterval(timer);
                });
                   
              $.ajax({
                url: "/itda/sendMail.se",
                data: {
                   email: email
                },
                type: "post",
                success: function(data) {
                   
                   randomVal = data;
                   console.log(data);
                   
                },
                error: function(error) {
                   console.log(error);
                } 
             });
              
                }
   
         });
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		              

                        

                		$("#hyj_phone_btn").on("click", function(){  // 핸드폰의 인증번호 버튼을 눌렀을 경우
                			var regExp =/(01[016789])([1-9]{1}[0-9]{2,3})([0-9]{4})$/;
                			var phone = $("#hyj_phone").val();
                			  var min = 2;
                              var sec = 59;
                              var timerId = null;
                		
                         
                                    
                	        		if(regExp.test(phone)){
                	        			
                	        			function timer1(){
                                        	if(sec > 10){
                           	        			$("#timer1").text(String(min) + ":" + String(sec));
                           	        			sec--;
                           	        			timerId=setTimeout(timer1,1000);
                           	        		}else if(sec >= 0){
                           	        			$("#timer1").text(String(min) + ":0" + String(sec));
                           	        			sec--;
                           	        			timerId=setTimeout(timer1,1000);
                           	        		}else if(min != 0){
                           	        			min--;
                           	        			sec = 60;
                           	        			timerId=setTimeout(timer1,1000);
                           	        		}else{
                           	        			min = 2;
                           	        			sec = 59;
                           	        			$("#popup_mask1").css("display","none"); //팝업창 뒷배경 display none
                                   	            $("#popupDiv1").css("display","none"); //팝업창 display none
                                   	            $("body").css("overflow","auto");//body 스크롤바 생성
                                   	            $("#timer1").show();
                                   	            $("#timerOver11").show();
                                           		$("#timerOver21").hide();
                           	        		}
                                        }
                	        			
                	        			timer1();
                        	             $("#popupDiv1").css({
                        	                "top": (($(window).height()-$("#popupDiv1").outerHeight())/2+$(window).scrollTop())+"px",
                        	                "left": (($(window).width()-$("#popupDiv1").outerWidth())/2+$(window).scrollLeft())+"px"
                        	                //팝업창을 가운데로 띄우기 위해 현재 화면의 가운데 값과 스크롤 값을 계산하여 팝업창 CSS 설정
                        	             }); 
                        	            
                        	            $("#popup_mask1").css("display","block"); //팝업 뒷배경 display block
                        	            $("#popupDiv1").css("display","block"); //팝업창 display block
                        	            
                        	            $("body").css("overflow","hidden");//body 스크롤바 없애기
                        	        
                        	        $("#popCloseBtn1").click(function(event){
                        	            $("#popup_mask1").css("display","none"); //팝업창 뒷배경 display none
                        	            $("#popupDiv1").css("display","none"); //팝업창 display none
                        	            $("body").css("overflow","auto");//body 스크롤바 생성
                        	            $("#timer1").show();
                        	            $("#timerOver11").show();
                                		$("#timerOver21").hide();
                                		clearTimeout(timerId);
                                		min = 2;
                	        			sec = 59;
                	        		
                                	   /*  clearInterval(timer1); */
                        	        });
                        	        	
                        			$.ajax({
                    					url: "/itda/phoneCertification.ph",
                    					data: {
                    						phone: phone
                    					},
                    					type: "post",
                    					success: function(data) {
                    						
                    						randomVal = data;
                    						console.log(data);
                    						
                    					},
                    					error: function(error) {
                    						console.log(error);
                    					} 
                    				});
                        			
                                	}else{
                                		alert("핸드폰 형식에 맞춰서 작성해주세요");
                                		
                                	}
                	});
		
    </script>
</body>
</html>