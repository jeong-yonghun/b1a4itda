<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="com.kh.itda.addInfo.resume.model.vo.*,java.util.*"%>
    <%
    	ArrayList<Resume> list=(ArrayList<Resume>) request.getAttribute("list");
    %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <style>
    
       
        .resumeManageMainTable{
            margin-left: auto;
            margin-right: auto;
            width: 1400px;
            height:1000px;
        }
     
        
        .resumeManageMainTable_tr_td_section{
            width: 840px;
        }
        
        .resumeManageMainTable_tr_td_section_div1{
            border-top: 6px solid #5A84F1;
            height: 150px;
        }
        .resumeManageMainTable_tr_td_section_div2{
            border-top: 6px solid #5A84F1;
            height: 580px;
        }
        .resumeManageMainTable_tr_td_section_button{
            margin-left: auto;
            margin-right: auto;
            width: 160px;
            height: 50px;
            border-radius: 0.5pc;
            background-color: #5A84F1;
            color: white;
            font-size: 13pt;
        }
       .container{
           width: 840px;
           margin-top:30px;
       }
       .resumeManagerListTable{
           width: 100%;
           height: 60px;
           text-align: center;
           font-size: 20px;
           font-weight: bold;
       }
       .cho_resumeList{
       		color: #505050;
       }
       #resumeWrite{
       		float:right;
       		
       }
       #btnArea{
       	 border-top: 1px solid #D3D3D3;
       }
		#chUpdate{
      	    margin-left: auto;
            margin-right: auto;
            width: 60px;
            height: 25px;
            border-radius: 0.5pc;
            background-color:white ;
            border:1px solid #5A84F1;
            color: #5A84F1;
            font-size: 12pt;
       }
       #chDelete{
      	    margin-left: auto;
            margin-right: auto;
            width: 60px;
            height: 25px;
            border-radius: 0.5pc;
            background-color: white;
            border:1px solid #5A84F1;
            color: gray;
            font-size: 12pt;
       }       
       .cho_resumeList:hover{
       		 border:1px solid #5A84F1;
       		
       		color: #5A84F1;
       } 
       .cho_resumeList{
       		color:#C4C4C4;
       }
       
    </style>
</head>
<body>

<%@ include file="../company_common/sb_companyMenubar.jsp"%>
<%@ include file="../company_common/sb_companyIntroMenubar.jsp"%>


    <table class="resumeManageMainTable">
        <tr class="resumeManageMainTable_tr">
            <td class="resumeManageMainTable_tr_td_nav" style="width:280px; vertical-align:top">
                <!-- nav 개인프로필 영역이랑 사이드 메뉴바인클루드 하기 -->
            </td>
            <td class="resumeManageMainTable_tr_td_section" style="vertical-align:top;">
                <h2 style="font-weight: bold; margin-top:30px;">지원자이력서 관리</h2>
                <div class="resumeManageMainTable_tr_td_section_div1">
                    <table class="resumeManagerListTable">
                   
                     <tr class="resumeManagerListTable_th">
                      <td>이력서 제목</td>
                      <td>작성 날짜</td>
                      <td>수정 날짜</td>
                       <td>인재추천 공개 여부</td>
                       <td></td>
                       <td></td>
                     </tr>
                     <%for (int i=list.size()-1;i>=0;i--){ %>
		 			 	<%System.out.println("list"+list); 
		 			 		Resume r=list.get(i);
		 			 		if(r.getrDelete().equals("Y")){
		 			 		%>
		 			 <tr class="cho_resumeList" >
		 				<td onclick="resumeView('<%=r.getrNo()%>')"><%=r.getrTitle() %></td>
		 				<td onclick="resumeView('<%=r.getrNo()%>')"><%=r.getrWriteDate() %></td>
		 				<td onclick="resumeView('<%=r.getrNo()%>')"><%=r.getrUpdateDate() %></td>
		 				<td onclick="resumeView('<%=r.getrNo()%>')"><%=r.getrPublic()%></td>
		 				<td onclick="resumeSave('<%=r.getrNo()%>')"><input type="button" class="cho_changePu" id="chUpdate"  value="합격"></td>
		 			    <td onclick="resumeUnSave('<%=r.getrNo()%>')"><input type="button" class="cho_changePu" id="chDelete" value="불합격"></td>
		 				

		 			</tr>
		 				<%}
		 			 } %>
					
					
					<tr  id="btnArea">
						<td colspan="6">
							<button class="resumeManageMainTable_tr_td_section_button" id="resumeWrite">이력서 추가하기</button>
						</td>
					</tr>
                    </table>
                    
                </div>
                
                </td>
			 <td class="resumeManageMainTable_tr_td_aside" style="width:280px;">
            
            </td>
	    </tr>
    </table>
<script>
$(function(){
	
	//이력서 작성하기
	$("#resumeWrite").click(function(){
		location.href = "<%=request.getContextPath()%>/beforeinsert.re";
	});
	
	
});
//목록불러오기
function resumeView(rNo){
	console.log(rNo);
	location.href="<%=request.getContextPath()%>/resumeDetail.re?rNo="+rNo;
}
//수정하기
function resumeSave(rNo){
	console.log("수정하기:"+rNo);
	location.href="<%=request.getContextPath()%>/updateResume.re?rNo="+rNo;
}
//삭제하기
function resumeUnSave(rNo){
	console.log(rNo);
	var con_test = confirm("이력서를 삭제하시겠습니까?");
	if(con_test == true){
		console.log("삭제버튼 눌렀을때"+rNo);
	  location.href="<%=request.getContextPath()%>/deleteResume.re?rNo="+rNo;
	}
}
</script>
<%@include file="/views/common/cho_footer.jsp" %>
</body>
</html>