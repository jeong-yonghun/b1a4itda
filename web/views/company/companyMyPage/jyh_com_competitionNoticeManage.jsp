<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <style>
        .article1{
            width:840px; 
        }
        .article2{
            width:840px; 
            text-align: center;
        }
        #nav{
            width:278px;
        }
        #aside{
            width:280px;
        }
        table{
            margin:auto;
            border-collapse:collapse;
            border-spacing:0;
        }
        td{
            margin:0;
            padding:0;
        }
        ul {
            list-style:none;
            margin:0;
            padding:0;
        }
        li {
            float: left;
            padding:15px;
            border-right:1px solid gray;
            border-top:1px solid gray;
        }
        #jyh_per-list {
            border-collapse: collapse;
            width:840px;
            margin:0px;
        }  
        #jyh_per-list th {
            color: #168;
            background: #f0f6f9;
            text-align: center;
        }
        #jyh_per-list th, #jyh_per-list td {
            padding: 10px;
            border-bottom:1px solid gray;
            /* border: 1px solid #ddd; */
        }
        #jyh_per-list tr td{
            text-align: center;
        }
    </style>
</head>
<body>

	<%@include file="/views/common/menubar.jsp" %>
	<%@include file="../company_common/sb_companyIntroMenubar.jsp" %>

<table align="center">

    <tr>
        <td colspan="3"><div style="width:1400px; height:100px;"></div></td>
    </tr>

    <tr>
        <td id="nav" style="vertical-align: top;">

            <!-- 사이드에 회원 정보창 -->
        <div style="width:280px;">
        <div id="cho_userInfo">
            <div id="cho_personImgDiv">
                <img id="cho_userImg" src="../../../image/logo.PNG">
            </div>
            <h3 id="cho_userName">정용훈(회사)</h3>
            <h4 id="cho_phone">010-0000-0000</h4>
            <h4 id="cho_email">이메일 입력</h4>
            <h5 id="cho_github">http://주소입력</h5>
            <p id="cho_deletemember" onclick="deletemember()">회원탈퇴</p>
        </div>
        </div>

        <div id="cho_sideMenubar">
            <table id="cho_sideMenubarTable">
                    <tr id="cho_sideMenubarTableTHaed"><td><p id="cho_sideMenubarTableTHaedP">My Page</p></td></tr>
                    <tr><td class="sideSide" id="cho_dateil" onclick="goDateil()">기업 상세정보</td></tr>
                    <tr><td class="sideSubTitle">- 기업상세정보 보기</td></tr>
                    <tr><td class="sideSubTitle" id="cho_resumewri" onclick="goResumeWriter()">- 기업 상세정보 보기 수정</td></tr>
                    <tr><td class="sideSide" id="cho_resume" onclick="goResume()">공고관리</td></tr>
                    <tr><td class="sideSubTitle">- 채용공고관리</td></tr>
                    <tr><td class="sideSubTitle" id="cho_contest" onclick="goContestApp()">- 공모전 관리</td></tr>
                    <tr><td class="sideSubTitle" id="cho_recruit" onclick="goRecruit()">결제내역</td></tr>
                    <tr><td class="sideSubTitle" id="cho_proposed" onclick="goProposed()">제안한 인재</td></tr>
                    <tr><td class="sideSubTitle" id="cho_portfolio" onclick="goPortfolio()">나의 즐겨찾기</td></tr>
            </table>
    
        </div>
    </td>
    <td style="vertical-align:top;">
            <div class="article1"></div>
            <div class="article2">
                <h3 style="float:left;">공모전공고관리</h3>
            </div>
            <hr style="width:840px; margin-bottom:50px;">
            <h4>진행중인 공모전</h4>
            <div class="div1" style="width:840px; ">
            <table id="jyh_per-list" align="center">
                    <tr>
                        <th>공고제목</th>
                        <th>공고기간</th>
                        <th>접수일자</th>
                        <th>지원자 현황</th>
                        <th>공고상태</th>
                    </tr>
                    <tr>
                        <td>공고제목</td>
                        <td>공고기간</td>
                        <td>접수일자</td>
                        <td>지원자 현황</td>
                        <td>진행중</td>
                    </tr>
                </table>
            </div>
            <h4>심사중인 공모전</h4>
            <div class="div2" style="width:840px;">
                    <table id="jyh_per-list" align="center">
                            <tr>
                                <th>공고제목</th>
                                <th>공고기간</th>
                                <th>접수일자</th>
                                <th>지원자 현황</th>
                                <th>공고상태</th>
                            </tr>
                            <tr>
                                <td>공고제목</td>
                                <td>공고기간</td>
                                <td>접수일자</td>
                                <td>지원자 현황</td>
                                <td>심사중</td>
                            </tr>
                        </table>
                    </div>
                    <h4>반려된 공모전</h4>
                    <div class="div3" style="width:840px;">
                            <table id="jyh_per-list" align="center">
                                    <tr>
                                        <th>공고제목</th>
                                        <th>공고기간</th>
                                        <th>접수일자</th>
                                        <th>지원자 현황</th>
                                        <th>공고상태</th>
                                    </tr>
                                    <tr>
                                        <td>공고제목</td>
                                        <td>공고기간</td>
                                        <td>접수일자</td>
                                        <td>지원자 현황</td>
                                        <td>반려</td>
                                    </tr>
                            </table>
                    </div>
                    <h4>종료된 공모전</h4>
                    <div class="div4" style="width:840px;">
                            <table id="jyh_per-list" align="center">
                                    <tr>
                                        <th>공고제목</th>
                                        <th>공고기간</th>
                                        <th>접수일자</th>
                                        <th>지원자 현황</th>
                                        <th>공고상태</th>
                                    </tr>
                                    <tr>
                                        <td>공고제목</td>
                                        <td>공고기간</td>
                                        <td>접수일자</td>
                                        <td>지원자 현황</td>
                                        <td>종료</td>
                                    </tr>
                            </table>
                    </div>
            <div class="pagingArea" align="center">
                    <button onclick=""><<</button>
 
                                <button disabled><</button>
 
                                <button onclick = ""><</button>
                
                    
                   
                                         <button disabled></button>
                         
                                       <button onclick=""></button>         			 
                               
                                 
                                
                                 <button disabled>></button>
                            
                                 <button onclick="">></button>
                           
                    
                    
                    <button onclick="">>></button>
                 </div>
    </td>
    <td id="aside">
    </td>
    </tr>
</table>
    
<%@ include file="../../common/cho_footer.jsp" %>

</body>
</html>