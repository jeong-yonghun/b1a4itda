<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.util.*, com.kh.itda.notice.payAndProduct.model.vo.*"%>
    <%@ page import="com.kh.itda.recruitAndContest.model.vo.Announcement" %>
<%
	 ArrayList<Announcement> list= (ArrayList<Announcement>)request.getAttribute("list"); 
	
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<style>
	  #cho_userInfo{
        	 
            width: 200px;
            height: 350px;
            border: 1px #CCCCCC solid;
            border-radius: 1pc;
            margin-left: auto;
            margin-right: auto;
            text-align: center;
            font-family: 'Noto Sans KR', sans-serif;
            /* margin-top:20px; */
        }
        #cho_personImgDiv{
            width: 60px;
            height: 60px;
            border: solid #5A84F1 1px;
            border-radius: 10pc;
            margin-right: auto;
            margin-left: auto;
            margin-top: 15px;
        }
        #cho_userImg{
            width: 100%;
            height: 100%;
            border-radius: 10pc;
            margin-right: auto;
            margin-left: auto;
            
        }
        #cho_sideMenubar {
	width: 200px;
	height: 350px;
	border: 2px solid #5A84F1;
	margin-left: auto;
	margin-right: auto;
	margin-top: 30px;
	font-family: 'Noto Sans KR', sans-serif;
}

#cho_sideMenubarTable {
	width: 180px;
	height: 90%;
	margin-left: auto;
	margin-right: auto;
}

#cho_sideMenubarTableTHaedP {
	font-size: 18px;
	font-weight: bold;
}

.sideSubTitle {
	font-size: 16px;
	font-weight: bold;
	border-top: 1px solid #C4C4C4;
	padding-top: 10px;
	padding-bottom: 10px;
}
.resumeManagerListTable{
           width: 100%;
           height: 60px;
           text-align: center;
           font-size: 20px;
           font-weight: bold;
       }
        .useItem {
        	border-top-left-radius: 25px;
			border-bottom-left-radius: 25px;
			border-top-right-radius: 25px;
			border-bottom-right-radius: 25px;
			font-family: 'Noto Sans KR', sans-serif;
        }
        .useItem > table {
        	font-family: 'Noto Sans KR', sans-serif;
        	font-size: 20px;
        	font-weight: bold;
        	
        }

        #cho_AnnList:hover{
        border:1px solid #5A84F1;
       		
       		color: #5A84F1;
        
        }
   .resumeManageMainTable{
            margin-left: auto;
            margin-right: auto;
            width: 1400px;
            height:1000px;
        }
     
        
        .resumeManageMainTable_tr_td_section{
            width: 840px;
        }
        
        .resumeManageMainTable_tr_td_section_div1{
            border-top: 6px solid #5A84F1;
            height: 150px;
        }
        .resumeManageMainTable_tr_td_section_div2{
            border-top: 6px solid #5A84F1;
            height: 580px;
        }
        .resumeManageMainTable_tr_td_section_button{
            margin-left: auto;
            margin-right: auto;
            width: 160px;
            height: 50px;
            border-radius: 0.5pc;
            background-color: #5A84F1;
            color: white;
            font-size: 13pt;
        }
       .container{
           width: 840px;
           margin-top:30px;
       }
       .resumeManagerListTable{
           width: 100%;
           height: 60px;
           text-align: center;
           
           font-size: 20px;
           font-weight: bold;
       }
       .cho_resumeList{
       		color: #505050;
       }
       #resumeWrite{
       		float:right;
       		
       }
       #btnArea{
       	 border-top: 1px solid #D3D3D3;
       }
		#chUpdate{
      	    margin-left: auto;
            margin-right: auto;
            width: 60px;
            height: 25px;
            border-radius: 0.5pc;
            background-color:white ;
            border:1px solid #5A84F1;
            color: #5A84F1;
            font-size: 12pt;
       }
       #chDelete{
      	    margin-left: auto;
            margin-right: auto;
            width: 60px;
            height: 25px;
            border-radius: 0.5pc;
            background-color: white;
            border:1px solid #5A84F1;
            color: gray;
            font-size: 12pt;
       }       
       .cho_resumeList:hover{
       		 border:1px solid #5A84F1;
       		
       		color: #5A84F1;
       } 
       #cho_AnnList{
       		color:#7c7c7c;
       }
</style>
</head>
<body>

<%@ include file="../company_common/sb_companyMenubar.jsp"%>
<br><br><br><br><br><br><br><br><br>
<%@ include file="../company_common/sb_companyIntroMenubar.jsp"%>
	<div style="width: 100px; float: left; border: 1px solid white;"></div>
	<div style="height: 800px; width: 1400px">
		<table>
			<tr>
				<td> <br><br>
					 <div id="cho_userInfo">
				        <div id="cho_personImgDiv">
				            <img id="cho_userImg" src="../../../image/logo.PNG">
				        </div>
				        <h3 id="cho_userName"><%=loginUser.getCompanyName() %></h3>
				        <h4 id="cho_phone"><%=loginUser.getPhone() %></h4>
				        <h4 id="cho_email"><%=loginUser.getEmail() %></h4>
				        <h5 id="cho_github"><%=loginUser.getComUrl() %></h5>
				        <p id="cho_deletemember" onclick="deletemember()">회원탈퇴</p>
				    </div>
				    <div id="cho_sideMenubar">
		<table id="cho_sideMenubarTable">

			<tr id="cho_sideMenubarTableTHaed">
				<td><p id="cho_sideMenubarTableTHaedP">My Page</p></td>
			</tr>
			<tr>
				<td class="sideSubTitle" id="cho_dateil" onclick="goDateil()">기업
					상세정보</td>
			</tr>
			
			<tr>
				<td class="sideSide" id="cho_resumewri" onclick="goResumeWriter()">   - 기업 상세정보 보기</td>
			</tr>
			<tr>
				<td class="sideSide" id="cho_resume" onclick="goResu()">   - 기업 상세정보 수정</td>
			</tr>
			<tr>
				<td class="sideSubTitle">공고 관리</td>
			</tr>
			<tr>
				<td class="sideSide" id="cho_contest">   - 채용공고 관리</td>
			</tr>
			<tr>
				<td class="sideSide" id="cho_recruit" onclick="goRecruit()">- 공모전 관리</td>
			</tr>
			
			<tr>
				<td class="sideSubTitle" id="cho_portfolio" onclick="goPortfoli()">결제내역</td>
			</tr>
			<tr>
				<td class="sideSubTitle" id="cho_writed" onclick="goWrited()">제안한 인재</td>
			</tr>
			<tr>
				<td class="sideSubTitle" id="cho_bookMark" onclick="goBookMark()">나의 즐겨찾기</td>
			</tr>
		</table>



	</div>
				</td>
				<td style="vertical-align:top ;">
					<div class="resumeManageMainTable_tr_td_section_div1">
						<table class="resumeManagerListTable">
						<tr><td colspan="5">일반 채용 공고</td></tr>
						<tr class="resumeManagerListTable_th">
								<td>채용공고 번호</td>
								<td>채용공고 제목</td>
								<td>접수 기간</td>
								<td>공고 기간</td>
								<td>지원자 현황</td>
								<td>상태</td>
						</tr>
						<%for(Announcement a: list) {%>
							<tr id="cho_AnnList" class="resumeManagerListTable_th">
							
								<input type="hidden" value="<%=a.getaNo() %>" name="aNo">
								<td onclick="AnnDetail('<%=a.getaNo() %>')"><%=a.getaNo()%></td>
								<td onclick="AnnDetail('<%=a.getaNo() %>')"><%=a.getaTitle() %></td>
								<td onclick="AnnDetail('<%=a.getaNo() %>')"><%=a.getaStartDate()+"~"+a.getaEndDate() %></td>
								<td onclick="AnnDetail('<%=a.getaNo() %>')"><%=a.getaReceiptStart()+"~"+a.getaReceiptEnd() %></td>
								<td onclick="AnnDetail('<%=a.getaNo() %>')"><input type="button" onclick="AppPerlist('<%=a.getaNo() %>')" value="지원자 목록"></td>
								<td onclick="AnnDetail('<%=a.getaNo() %>')"><%=a.getStatus() %></td>
							</tr>
							<%} %>
						</table>
					</div>
				</td>
			</tr>
		</table>
	</div>
	<script type="text/javascript">
	function AppPerlist(aNo){
		location.href="<%=request.getContextPath()%>/appPerList.ap?aNo="+aNo;
	}
	
	</script>
	<script type="text/javascript">
	$(function(){
		
		$("#cho_contest").click(function(){
			location.href="<%=request.getContextPath()%>/selectAnnPer.li";
		});
		
		function AnnDetail(aNo){
			//준성이한테 공고 디테일 리스트 보여달라고 하기
			location.href="<%=request.getContextPath()%>/resumeDetail.re?aNo="+aNo;
		}
		
	});
	</script>
<%-- <%@ include file="../../common/cho_footer.jsp"%> --%>
</body>
</html>