<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.util.*, com.kh.itda.notice.payAndProduct.model.vo.*"%>
<%
	ArrayList<Sb_Payment> list = (ArrayList<Sb_Payment>) request.getAttribute("list");
	
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<style>
	  #cho_userInfo{
        	 
            width: 200px;
            height: 350px;
            border: 1px #CCCCCC solid;
            border-radius: 1pc;
            margin-left: auto;
            margin-right: auto;
            text-align: center;
            font-family: 'Noto Sans KR', sans-serif;
            /* margin-top:20px; */
        }
        #cho_personImgDiv{
            width: 60px;
            height: 60px;
            border: solid #5A84F1 1px;
            border-radius: 10pc;
            margin-right: auto;
            margin-left: auto;
            margin-top: 15px;
        }
        #cho_userImg{
            width: 100%;
            height: 100%;
            border-radius: 10pc;
            margin-right: auto;
            margin-left: auto;
            
        }
        #cho_sideMenubar {
	width: 200px;
	height: 350px;
	border: 2px solid #5A84F1;
	margin-left: auto;
	margin-right: auto;
	margin-top: 30px;
	font-family: 'Noto Sans KR', sans-serif;
}

#cho_sideMenubarTable {
	width: 180px;
	height: 90%;
	margin-left: auto;
	margin-right: auto;
}

#cho_sideMenubarTableTHaedP {
	font-size: 18px;
	font-weight: bold;
}

.sideSubTitle {
	font-size: 16px;
	font-weight: bold;
	border-top: 1px solid #C4C4C4;
	padding-top: 10px;
	padding-bottom: 10px;
}
        .useItem {
        	border-top-left-radius: 25px;
			border-bottom-left-radius: 25px;
			border-top-right-radius: 25px;
			border-bottom-right-radius: 25px;
			font-family: 'Noto Sans KR', sans-serif;
        }
        .useItem > table {
        	font-family: 'Noto Sans KR', sans-serif;
        	font-size: 20px;
        	font-weight: bold;
        	
        }
</style>
</head>
<body>

<%@ include file="../company_common/sb_companyMenubar.jsp"%>
<br><br><br><br><br><br><br><br><br>
<%@ include file="../company_common/sb_companyIntroMenubar.jsp"%>
	<div style="width: 100px; float: left; border: 1px solid white;"></div>
	<div style="height: 800px; width: 1400px">
		<table>
			<tr>
				<td> <br><br>
					 <div id="cho_userInfo">
				        <div id="cho_personImgDiv">
				            <img id="cho_userImg" src="../../../image/logo.PNG">
				        </div>
				        <h3 id="cho_userName"><%=loginUser.getCompanyName() %></h3>
				        <h4 id="cho_phone"><%=loginUser.getPhone() %></h4>
				        <h4 id="cho_email"><%=loginUser.getEmail() %></h4>
				        <h5 id="cho_github"><%=loginUser.getComUrl() %></h5>
				        <p id="cho_deletemember" onclick="deletemember()">회원탈퇴</p>
				    </div>
				    <div id="cho_sideMenubar">
		<table id="cho_sideMenubarTable">

			<tr id="cho_sideMenubarTableTHaed">
				<td><p id="cho_sideMenubarTableTHaedP">My Page</p></td>
			</tr>
			<tr>
				<td class="sideSubTitle" id="cho_dateil" onclick="goDateil()">기업
					상세정보</td>
			</tr>
			
			<tr>
			
				<td class="sideSide" id="cho_resumewri" onclick="goResumeWriter()">   - 기업 상세정보 보기</td>
			</tr>
			<tr>
				<td class="sideSide" id="cho_resume" onclick="goResu()">   - 기업 상세정보 수정</td>
			</tr>
			<tr>
				<td class="sideSubTitle">공고 관리</td>
			</tr>
			<tr>
				<td class="sideSide" id="cho_contest">   - 채용공고 관리</td>
			</tr>
			<tr>
				<td class="sideSide" id="cho_recruit" onclick="goRecruit()">- 공모전 관리</td>
			</tr>
			
			<tr>
				<td class="sideSubTitle" id="cho_portfolio" onclick="goPortfoli()">결제내역</td>
			</tr>
			<tr>
				<td class="sideSubTitle" id="cho_writed" onclick="goWrited()">제안한 인재</td>
			</tr>
			<tr>
				<td class="sideSubTitle" id="cho_bookMark" onclick="goBookMark()">나의 즐겨찾기</td>
			</tr>
		</table>



	</div>
				</td>
				<td style="vertical-align:top ;">
					<div class="useItem" style="border: 2px solid #5A84F1; margin-left: 50px; margin-top: 30px; ">
						<table style="text-align: center;">
							<tr>
								<td width="300px" height="50px">채용공고상단</td>
								<td width="300px">채용공고중단</td>
								<td width="300px">공모전공고</td>
							</tr>
							<tr>
								<td height="80px">1</td>
								<td>0</td>
								<td>1</td>
							</tr>
							<tr>
								<td width="200px" height="50px">인재열람상품500</td>
								<td width="200px">인재열람상품300</td>
								<td width="200px">인재열람상품100</td>
								<td width="200px">인재열람상품50</td>
							</tr>
							<tr>
								<td height="80px">3</td>
								<td>0</td>
								<td>0</td>
								<td>0</td>
							</tr>
						</table>
					</div>
				</td>
			</tr>
		</table>
	</div>
	<script type="text/javascript">
	$(function(){
		$("#cho_contest").click(function(){
			location.href="<%=request.getContextPath()%>/selectAnnPer.li";
		});
		
		
	});
	</script>
<%@ include file="../../common/cho_footer.jsp"%>
</body>
</html>