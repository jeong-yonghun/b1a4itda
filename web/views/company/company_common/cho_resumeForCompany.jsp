<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <style>
        .cho_resume_section{
            border-bottom: 1px solid #C4C4C4;
        }
        .cho_resumeSkillDiv{
            display: inline-block;
            background-color: #C4C4C4;
        }
        #resume_com{
           
            margin-left: auto;
            margin-right: auto;
            width: 1200px;
            border-collapse: separate;
  			border-spacing: 0 100px;
  			
        }
        .cho_left_td{
            width: 20%;
            border-bottom: 1px solid #C4C4C4;
            padding-top:15px;
             
            
        }
        .cho_right_td{
            width: 78%;
            border-bottom: 1px solid #C4C4C4;
             padding-top:20px;
              /* 인재열람권 사용전에 내용 가리기  */
            visibility:hidden
        }
        .cho_right_tr{
            vertical-align: top;
        }
        .cho_resume_section{
        	padding:18px;
        }
       
        .resume_Com_Table_botton{
            float: right;
            width: 210px;
            height: 60px;
            border-radius: 0.5pc;
            background-color: #5A84F1;
            color: white;
            font-size: 15pt;
           
        }
       
         #bookMarkIcon{
        	width: 25px;
        	height: 25px
        }
         #personDefaultInfo{
        /* 인재열람권 사용하기 전에 개인정보 가리는 기술 */
         visibility:hidden
        }
    </style>
</head>
<body>
    
    <table id="resume_com">
        <tr class="cho_left_tr">
        
            <!-- 오른쪽 영역 개인정보, 기술 스택, 직무 분야 -->
            <td class="cho_rright_td" style="vertical-align: top;">
                <div id="personalInfo" class="cho_resume_section">
                    <!-- 이름 작성부분 -->
                    <h1><img id="bookMarkIcon" src="/itda/image/bookMarkIconDisable.png" onclick="goBookMark()">정용훈</h1>
                    <!-- 연락처, 이메일, 깃허브, 포트폴리오 -->
                    <h3 id="personDefaultInfo">
                    	    연락처: 010-0000-0000<br>
                    	    이메일: b1a4@gmail.com<br>
                   	     깃허브: https://sdfas.com/itda<br>
                   	     포트폴리오: https://asdfsad.com
                    </h3>

                </div>
                <div class="cho_resume_section">
                    <h3>기술 스택: 7개(카운트 영역)</h3>
                    <div id="skill_stack">
                        <!-- 반복문으로 기술별div만들어주기 -->
                        <table style="width: 80%;">
                            <tr>
                                <td>기술</td>
                                <td>스택</td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="cho_resume_section">
                    <h3>직무 분야</h3>
                    <div>
                        <!-- 반복문으로 직무 분야별 div만들어주기 -->
                        <table>
                            <tr>
                                <td>직무</td>
                                <td>분야</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </td>
            
            <!-- 왼쪽영역 프로젝트 교육 등등등 값이 없을 때는 if 문으로 조건 걸고 hide쓰기-->
            <td>
                <table style="width: 98%;">
                    <!-- 회사 경력 줄 -->
                    <tr class="cho_right_tr">
                        <td class="cho_left_td"><h3>회사경력</h3></td>
                        <td class="cho_right_td">
                            <!-- 회사경력div 갯수만큼 for문으로 영역주기 -->
                            <div>
                                <h4>회사명: 선빈회사</h4>
                                <h5>직군: ㅁㄴㅇㄹ</h5>
                            	   <h5> 입사일:<input type="date" disabled><br>
                            	    퇴사일:<input type="date" disabled></h5>
                                <!-- 사용스택 -->
                                <div>
                                    <div class="cho_resumeSkillDiv">java</div>
                                    <div class="cho_resumeSkillDiv">java</div>
                                    <div class="cho_resumeSkillDiv">java</div>
                                    <div class="cho_resumeSkillDiv">java</div>
                                </div>
                              	  <h5>연봉: 33333</h5>
                            </div>
                        </td>
                    </tr>
                    <!-- 개발 경력 줄 -->
                    <tr class="cho_right_tr">
                        <td class="cho_left_td"><h3>개발경력</h3></td>
                        <td class="cho_right_td">
                            <div>
                                <h4>개발경력명: asdf </h4>
                                <h5>담당업무:</h5>
                                 <h5>개발시작일자:<input type="date" disabled><br>
                                  	  개발종료일자:<input type="date" disabled><br>
                                 </h5>
                                    <!-- 사용스택 -->
                                    <h5><div>
                                        <div class="cho_resumeSkillDiv">java</div>
                                        <div class="cho_resumeSkillDiv">java</div>
                                        <div class="cho_resumeSkillDiv">java</div>
                                        <div class="cho_resumeSkillDiv">java</div>
                                    </div>
                                    </h5>
                            </div>
                        </td>
                    </tr>
                    <!-- 수상 경력 -->
                    <tr class="cho_right_tr">
                        <td class="cho_left_td"><h3>수상경력</h3></td>
                        <td class="cho_right_td">
                            <div>
                                <h4>수상명: asdf </h4>
                                <h5>수상날짜:<input type="date" disabled></h5>
                                <h5>수상기관: kh학원</h5>
                            </div>
                        </td>
                    </tr>
                    <tr class="cho_right_tr">
                        <td class="cho_left_td"><h3>개인 <br>프로젝트</h3></td>
                        <td class="cho_right_td">
                            <!-- 프로젝트 div영역 갯수만큼 for문으로 영역주기 -->
                            <div>
                                <h4>프로젝트 제목</h4>
                                <h5>2002</h5>
                                <!-- 기술 스택 div영역 기술 갯수 for문으로 -->
                                <div>
                                    <div class="cho_resumeSkillDiv">java</div>
                                    <div class="cho_resumeSkillDiv">java</div>
                                    <div class="cho_resumeSkillDiv">java</div>
                                    <div class="cho_resumeSkillDiv">java</div>
                                </div>
                                <h5>상세 설명</h5>
                                <div>asdfasdf</div>
                            </div>
                        </td>
                    </tr>
                    <!-- 저장소 -->
                    <tr class="cho_right_tr">
                        <td class="cho_left_td"><h3>저장소</h3></td>
                        <td class="cho_right_td">
                            <div>
                                <h5>저장소 링크: ㅁㄴㅇㄹ</h5>
                            </div>
                        </td>
                    </tr>
                     <!--학력 -->
                     <tr class="cho_right_tr">
                        <td class="cho_left_td"><h3>학력</h3></td>
                        <td class="cho_right_td">
                            <!-- 학력 갯수만큼 반복문 돌력서 div 추가하기 -->
                            <div>
                                <h4>학교/교육 기관명: asdf </h4>
                                <h5>입학일:<input type="date" disabled><br>
                                 	   졸업일:<input type="date" disabled></h5>
                                 <h5> 전공: 전공자리
                                  	  학위: 학위자리</h5>
                                  	  <h5>내학점:3.3  최대학점:4.5</h5>
                                  	 <h5> 기타</h5>
                                    <div style="width: 80%;margin-bottom:20px;">
                                      	  기타내용 들어갈 자리
                                    </div>
                                
                            </div>
                        </td>
                    </tr>
                    <!-- 첨부파일 -->
                    <tr class="cho_right_tr">
                        <td class="cho_left_td"><h3>첨부파일</h3></td>
                        <td class="cho_right_td">
                            <!-- 첨부파일 갯수만큼 for문으로 추가해야함 -->
                            <div>
                                <h5>첨부파일: ㅁㄴㅇㄹ</h5>
                            </div>
                        </td>
                    </tr>
                     <!-- 희망연봉 -->
                     <tr class="cho_right_tr">
                        <td class="cho_left_td"><h3>희망연봉</h3></td>
                        <td class="cho_right_td">
                            <div>
                                <h5>희망연봉: ㅁㄴㅇㄹ</h5>
                            </div>
                        </td>
                    </tr>
                     <!-- 포트폴리오 -->
                     <tr class="cho_right_tr">
                        <td class="cho_left_td"><h3>포트폴리오</h3></td>
                        <td class="cho_right_td">
                            <div>
                                <h5>포트폴리오: ㅁㄴㅇㄹ</h5>
                            </div>
                        </td>
                    </tr>
                    <!--활동 -->
                    <tr class="cho_right_tr">
                        <td class="cho_left_td"><h3>활동</h3></td>
                        <td class="cho_right_td">
                            <!-- 활동 갯수만큼 반복문 돌력서 div 추가하기 -->
                            <div>
                                <h4>활동명: asdf </h4>
                                <h5>시작일:<input type="date" disabled><br>
                                  	  종료일:<input type="date" disabled></h5>
                               	    <h5> 링크: <input type="text"></h5>
                                	    <h5>설명</h5>
                                    <div style="width: 80%; ">
                                  	      기타내용 들어갈 자리
                                    </div>
                                </h5>
                            </div>
                        </td>
                    </tr>
                    <!-- 닫기 버튼 -->
                    <tr>
                        <td>
                        <button onclick="goBack()" class="resume_Com_Table_botton">닫기</button>
                        </td>
                    </tr>
                </table>

            </td>
        </tr>

    </table>
    <script type="text/javascript">
    	function goBookMark(){
    		$("#bookMarkIcon").attr('src','/itda/image/bookMarkIcon.jpg');	
    	}
    
    
    </script>
</body>
</html>