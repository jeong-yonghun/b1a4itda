<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="com.kh.itda.member.model.vo.Jyh_ComMember"%>
<%
   Jyh_ComMember loginUser = (Jyh_ComMember) request.getSession().getAttribute("loginUser");

%>
<link rel="stylesheet"
   href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<script
   src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
 <style>

        /*----------------------------------------- 메뉴바 style 영역 시작 -------------------------------------  */
        .head-blue-text-bold{
            color:#5A84F1;
            font-weight: bold;
            text-align: center;
          
        }

        .head{
            margin-left: auto; 
            margin-right: auto;
            width:1400px;
            height:100px;
            text-align: center;
            
        }
        .head1{
            height:60%;
            margin:0px;
            padding:0px;
            text-align: center;
            
        }

        .head1-1{
            height:100%;  
            float:left;
            text-align: center;
           ;
        }
        .head1-1-empty{
            width:7.5%; 
            text-align: center;
            color:black;
        }
        .head1-2-logo{
            width:7.5%; 
            text-align: center;
            
        }
        .head1-3-search{
            width:25%;
            text-align: center;
            
        }
        input[name=menubar-search]{
            height:86%; 
            width:60%; 
            float: left;
            border:solid 2px; 
            border-color: #5A84F1;
            margin-left: 5px;
            margin-top: 6px;
            text-align: center;
           
        }
        .head1-1-3-menubar-search{
            height:50%; 
            margin-top:3.86%;
            text-align: center;
           
        }
        .search-img-space{
         border:solid 2px;
          border-color: #5A84F1;
           width:10%;
            height:87%; 
            background: #5A84F1;
             float:left;
              margin-top: 6px;
              text-align: center;
           
        }
        .search-img{
            width: 100%; 
            max-width: 25px; 
            vertical-align: middle;
            text-align: center;
           
        }

        .head1-4-empty{
            width:45%;
            text-align: center;
           
        }
        .head1-5{
            width:15%;
            text-align: center;
            
        }
        .head1-5-1{
            width:100%; 
            height:50%;  
            float:left; 
            margin-top:15px;
            text-align: center;
           
        }
        .head1-5-1-alarm-img-space{
            width:30%; 
            height:100%;  
            float:left; 
            margin-left:auto; 
            margin-right:auto;
            text-align: center;
            
        }
        .alarm-img{
            width: 100%; 
            max-width: 30px; 
            vertical-align: middle;
            text-align: center;
            
        }
        .head1-5-2-mypage-label-space{
            width:35%; 
            height:21.6px;  
            float:left; 
            margin-left:auto; 
            margin-right:auto; 
            margin-top:2.2px;
            text-align: center;
            
        }        
        .head1-5-3-login-label-space{
            width:35%; 
            height:21.6px; 
            float:left; 
            margin-left:auto; 
            margin-right:auto; 
            margin-top:4.2px;
            text-align: center;
           
        }
        
        .head2{
            height:40%; 
            background-color:#4876EF;
            text-align: center;
            
            
        }
        .head2-common{
            height:100%; 
            float:left;
            text-align: center;
            
        }
        .head2-1{
            width:30%;
            text-align: center;
            
        }
        .head2-2{
            width:40%;
            text-align: center;
            
        }
        .head2-3{
            width:30%;
            text-align: center;
            
        }

        .head2-text{
            font-weight: bold; 
            color:white; 
            width:80%; 
            height:50%; 
            margin-left:auto; 
            margin-right:auto; 
            margin-top:10px;
            text-align: center;
           
        }
        .head2-2-text1{
            width:30%;
            text-align: center;
            
        }
        .head2-2-text2{
            width:25%; 
            margin-left:23px; 
            margin-right:23px;
            text-align: center;
            
        }
        .head2-2-text3{
            width:35%;
            text-align: center;
            
        }
        #notice{
           width:300px;
           height: 170px;
           float: right;
           display: none;
           background: white;
           margin-right: 50px;
           border-radius: 5px;
           border: 1px solid black;
           margin-top: 10px;
        }
        #content{
           display: none;
            width: 250px;
           height: 50px;
           background: #E6E6E6;
           text-align: center;
           margin-left: 20px;
          /*  border-collapse: separate;
           border-spacing: 0 10px; */
        }
        #wjs_td2{
           width: 300px;
           height: 30px;
           background: #E6E6E6;
           text-align: center;
        }
        
        .menubar_div, .menubar_div2{
           margin-top: 10px;
           width: 300px;
           height: 40px;
           border: 1px solid #E6E6E6;
           border-radius: 10px;
           
        }
        .divp{
           font-weight: bold;
           padding-top: 10px;
           float: left;
           margin-left: 24px;
        }
        
        .divimg{
           width: 20px; height: 20px; float:left;margin-left:15px;
           margin-top: 9px;
           
        }
        #wjs_ok{
           width:90px;
           height: 30px;
           background: #4876EF;
           color:white;
           font-weight: bold;
           margin-top: 50px;
           outline: 0;
           }
           #wjs_update{
           width:90px;
           height: 30px;
           background: #D8D8D8;
           color:white;
           font-weight: bold;
           margin-top: 50px;
           outline: 0;
           }
           
           
      
        /*----------------------------------------- 메뉴바 style 영역 끝 -------------------------------------  */


    </style>

    <header class="head"> <!-- 메뉴바 시작 -->
        <div class="head1">
            <div class="head1-1 head1-1-empty" ></div><!-- 빈 공간 -->

            <div class="head1-1 head1-2-logo" onclick="goIndi()" ><img src="/itda/image/logo.PNG" style="width:100%; height: 100%;"></div><!-- 로고 들어갈 자리 -->

            <div class="head1-1 head1-3-search"><!-- 검색 들어갈 자리 시작 -->
                <div class="head1-1-3-menubar-search">
                    <input type="text" name="menubar-search" maxlength="100">
                    <div class="search-img-space"><!-- 돋보기 이미지 시작 -->
                            <img src="/itda/image/searchIcon.PNG" style="width: 100%; max-width: 25px; vertical-align: middle"  />
                    </div><!-- 돋보기 이미지 끝 -->
                </div>
            </div><!-- 검색 들어갈 자리 끝 -->

            <div class="head1-1 head1-4-empty"></div><!-- 빈공간 -->

            <div class="head1-1 head1-5"><!-- 알림, 마이페이지, 로그인, 로그아웃 자리 시작 -->
               
                <div class="head1-5-1">
                    <div class="head1-5-1-alarm-img-space"><!-- 알림 이미지 시작 -->
                 
                         <img id="bell" src="/itda/image/bell.PNG" style="width: 100%; max-width: 30px; vertical-align: middle" />
                    </div><!-- 알림 이미지 끝 -->
                    <div class="head1-5-2-mypage-label-space" onclick="goMyPage()">
                         <% if(loginUser == null){ %>
                           <label class="head-blue-text-bold" style="margin-top:2px;" onclick="goMainPage()">메인페이지</label>
                   <%}else{ %>
                      <label class="head-blue-text-bold" onclick="goMyPage()">My Page</label>
                   <%}%>
                    </div>
                    <div class="head1-5-3-login-label-space">
                        <% if(loginUser == null){ %>
                           <label id="nickName" onclick="goLogin()" class="head-blue-text-bold">로그인</label>
                   <%}else{ %>
                      <label id="nickName" class="head-blue-text-bold" onclick="logout()">로그아웃</label>
                   <%}%>
                    </div>
                      <div id="notice">
                         <!-- <img src="/itda/image/danger.png" style="width: 20px; height: 20px;"> -->
                      </div>
                </div>
                
                
                
            
            </div><!-- 알림, 마이페이지, 로그인, 로그아웃 자리 끝 -->
            <script>
            //0211조지연이 임시로 만듬
            <%if(loginUser != null){%>
               function goMyPage(){
                  /* location.href="/itda/views/company/companyMyPage/sb_companyFirstPage.jsp"; */
                  var comNo = '<%=loginUser.getComNo()%>';
                  location.href="<%=request.getContextPath()%>/useItem.pay?comNo=" + comNo;
               }
            <%}%>
               function goIndi(){
                  location.href="/itda/views/company/companyMainPage/companyMainPage.jsp";
               }
               function goLogin(){
                  location.href="/itda/views/common/jyh_login.jsp";
               }
               
                function goMainPage(){
                  location.href="/itda/views/company/companyMainPage/companyMainPage.jsp";
               } 
                function goRecruit(){
                   location.href="/itda/views/company/programmerRecruit/hireEnroll.jsp";
                }
                function logout(){
                   location.href="<%=request.getContextPath()%>/logout.me";
                }
                function goCompetition(){
                   location.href="/itda/views/company/com_competition/jyh_competitionForm.jsp";
                }
            </script>

        </div>
        <div class="head2">
            <div class="head2-common head2-1"></div>
            <div class="head2-common head2-2">
                <div class="head2-common head2-2-text1">
                    <div class="head2-text" onclick="goInfo()">
                        <p style="margin:4px;">상품 안내</p>
                    </div>
                </div>   
                <div class="head2-common head2-2-text2">
                    <div class="head2-text" onclick="goRecruit()">
                        <p style="margin:4px;">개발자채용</p>
                    </div>
                </div>
                <div class="head2-common head2-2-text3">
                    <div class="head2-text" onclick="goCompetition()">
                        <p style="margin:4px;">공모전</p>
                    </div>
                </div>  
            </div>
            <div class="head2-common head2-3"></div>
        </div>
        
<div id="modal2" class="modal fade bd-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="margin-right: 90px; margin-top: 100px;">
  <div class="modal-dialog modal-sm">
    <div class="modal-content" style="width: 500px;height: 400px; margin-right: 100px;">
      <div style="width: 300px; height: 50px; margin-left: 160px;">
         <img id="moimg" src="/itda/image/wjs/caution.png" style="width: 35px; height: 35px; float: left; margin-top: 15px;">
         <h3 id="moh" style="margin-left: 15px; float: left; font-weight: bold;">반려 사유</h3>
         
         <img id="wjs_delete" src="/itda/image/wjs/delete2.png" style="width: 35px; height: 35px; float: right; margin-top: 15px;">
      </div>
      <hr align="center" style="border: solid 2px #F78181; width: 80%;">
      <br>
      <div style="width: 500px; height: 50px;">
      <div>
      <p style="margin-left: 30px; float: left; font-size: 14px; margin-top: 6px; font-weight: bold;">공고 제목 : </p>
      <input type="text" value="" class="wjs_name" disabled="disabled" style="margin-right:60px; width: 310px; height: 30px;">      
      </div>
      <br>
      <div>
      <p  id="mop" style="margin-left: 30px; float: left; font-size: 14px; margin-top: 6px; font-weight: bold;">반려 사유 : </p>
      <input id="moinput" type="text" value="" class="wjs_ip" disabled="disabled" style="margin-right:60px; width: 310px; height: 30px;">      
      </div>
      </div>
      <div style="margin-top: 30px;">
        <textarea id="wjs_textarea" name="textarea" rows="4" cols="58" readonly style="text-align: center; margin-top:20px; margin-left: 4px; resize: none;"></textarea>
      </div>
          <button id="wjs_update">수정하기</button> <button id="wjs_ok">확인</button> <!--  <button id="wjs_delete">삭제</button> -->
    </div>
  </div>
   </div>                  
   
         
    </header><!-- 메뉴바 끝 -->
    <script>
          var num= 1;
          var msg = "";
          var div = "";
       function goInfo() {
          location.href="/itda/views/company/productIntroduce/sb_itemInformation.jsp";
       }
       
       
       <%if(loginUser != null){ %>   
        $(function(){
       
          $.ajax({
              url: "/itda/notice.com",
              type: "get",
           data: {no: '<%=loginUser.getComNo()%>'
              },
              success: function(data){
           
                 console.log(data);
                 $("#bell").attr("src", "/itda/image/bell2.PNG");
                 
                 for(i = 0; i<data.length; i++){
                    
                    if(data[i].divide == "1"){
                       $div = $("<div class='menubar_div' name='"+ data[i].nNo + "'>");
                       $img = $("<img src='/itda/image/danger.png' class='divimg'>");
                       $p = $("<p class='divp'>회원가입이 승인되었습니다.</p>");
                       $divBody = $("#notice");
                       $divBody.append($div);
                       $div.prepend($img);
                       $div.append($p);
                    }
                    if(data[i].divide == "2"){
                       $div = $("<div class='menubar_div' name='"+ data[i].nNo + "'>");
                       $img = $("<img src='/itda/image/danger.png' class='divimg'>");
                       $p = $("<p class='divp'>공고가 승인되었습니다.</p>");
                       $divBody = $("#notice");
                       $divBody.append($div);
                       $div.prepend($img);
                       $div.append($p);
                    }
                    if(data[i].divide == "10"){
                       $div = $("<div class='menubar_div' name='"+ data[i].nNo + "'>");
                       $img = $("<img src='/itda/image/danger.png' class='divimg'>");
                       $p = $("<p class='divp'>회원가입이 반려되었습니다.</p>");
                       $divBody = $("#notice");
                       $divBody.append($div);
                       $div.prepend($img);
                       $div.append($p);
                    } 
                    if(data[i].divide == "20"){
                       $div = $("<div class='menubar_div' name='"+ data[i].nNo + "'>"); 
                       $img = $("<img src='/itda/image/danger.png' class='divimg'>");
                       $p = $("<p class='divp'>공고가 반려되었습니다.</p>");
                       $divBody = $("#notice");
                       $divBody.append($div);
                       $div.prepend($img);
                       $div.append($p);
                       console.log(data[i].divide);
                       console.log(data[i])
                    }
                    
                 }
                 
              },
              error: function(status){
                 console.log(status);
              }
           }); 
                 
        });
       
          
       <%}%> 
       
    </script>
    <script>
    var num= 1;
    var val= 0;
    $("#bell").click(function(){
      
      if(num % 2 == 1){
         $("#notice").show();
            
         $("#notice div").click(function(){
            var no1 = $(this).attr('name');
            
            $.ajax({
                url: "/itda/notice2.com",
               type: "get",
                data: {no1: no1
                },
                success:function(data){
                   console.log(data);
                   if(data.num == 1){
                      $(".wjs_ip").val("제목 및 요약");
                   }else if(data.num == 2){
                      $(".wjs_ip").val("업무 소개");
                   }else if(data.num == 3){
                      $(".wjs_ip").val("기술 스택");
                   }else if(data.num == 4){
                      $(".wjs_ip").val("자격 조건");
                   }else if(data.num == 5){
                      $(".wjs_ip").val("우대 사항");
                   }else if(data.num == 6){
                      $(".wjs_ip").val("제출 서류");
                   }else if(data.num == 7){
                      $(".wjs_ip").val("채용 절차");
                   }
                   
                   console.log(data.divide)
                   
                   if(data.divide == 1 || data.divide == 2){
                      $(".wjs_name").val(data.atitle);
                      $("#moimg").attr("src", "/itda/image/wjs/approval.png");
                      $("#wjs_textarea").text("공고가 승인되었습니다.");
                      $("#mop").hide();
                      $("#moh").text("공고 승인");
                      $("#moinput").hide();
                      $("#modal2").modal('show');
                   }else{
                   $(".wjs_name").val(data.atitle);
                   $("#wjs_textarea").text(data.nMsg);
                   $("#moimg").attr("src", "/itda/image/wjs/caution.png");
                   $("#moh").text("반려 사유");
                   $("#mop").show();
                   $("#moinput").show();
                   $("#modal2").modal('show');
                   
                      
                   }
                   
                   val = data.num;
                   console.log("ano"+data.aNo)
                   
                   var nNo = data.nNo;
                   
                   
                   $("#wjs_update").click(function(){
                      location.href = "<%=request.getContextPath()%>/selectUpdate.re?ano="+data.aNo +"&val="+val;
                   });
                   
                   $("#wjs_delete").click(function(){
                      
                      $.ajax({
                         
                         url: "/itda/delete.nt",
                            type: "get",
                             data: {nNo: nNo
                          },
                          success:function(data){
                             console.log(data);
                             $("#modal2").modal('hide');
                          },
                          error: function(status){
                             console.log(status);
                          }
                          
                          
                      });
                      
                   });
                   
                },
                error: function(status){
                     console.log(status);
                  }
                
             });       
            
         });
         }else{
            $("#notice").hide();
            $("#bell").attr("src", "/itda/image/bell.PNG");
         }
         num++; 
         
         }); 
       
    
      $("#wjs_ok").click(function(){
         $("#modal2").modal('hide');
      });     
      
    
    </script>
