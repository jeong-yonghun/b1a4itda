<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <style>
        .jyh_menubar, .jyh_empty{
            margin-left: auto;
            margin-right: auto;
            width:1400px;
            height:100px;
        }
        .jyh_h4{
            text-align: center;
            font-weight: bold;
            font-family: 'Noto Sans KR', sans-serif;
            color: #7a7a7a;
            margin:18px;
        }
        .jyh_menubar{
            border-top:solid #d8d8d8;
            border-bottom:solid #d8d8d8;
        }
        .jyh_menu{
            text-align: center;
            margin:0;
            height: 100%;
            width:50%;
            float:left;
        }
    </style>
        <div class="jyh_empty" style="height:75px;"></div>
        <div class="jyh_menubar"style="height:60px; margin-bottom:50px;">
            <div class="jyh_white" style="width:35%; height:100%; float:left;"></div>
            <div  class="jyh_center"style="width:30%; height:100%; float:left; ">
                <div class="jyh_menu" id="menu1" >
                       <h4 class="jyh_h4"> 채용 공모전</h4>
                </div>
                <div class="jyh_menu" id="menu2">
                       <h4 class="jyh_h4"> 일반 공모전</h4>     
                </div>
            </div>
         <div style="width:35%; height:100%; float:left;"></div>
     </div>