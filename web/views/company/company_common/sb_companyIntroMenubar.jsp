<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <style>
       
   #middleMenubar{
      	    margin-left: auto; 
            margin-right: auto; 
           width:1400px;
           height:50px;
           float:center;
       }
       h4{
           text-align: center;
           font-weight: bold;
           font-family: 'Noto Sans KR', sans-serif;
           color: #7a7a7a;
       }
      /*  div{
           background-color: white;
       } */
       .menubar{
         border-top:solid #d8d8d8;
         border-bottom:solid #d8d8d8;
         width: 1400px;
         margin: auto;

       }
       .menu{
          
           height: 95%;
           width:14.285%;
           float:left;
       }

       
   </style>
</head>
<body>
   <!-- <div id="middleMenubar" class="test1"> -->
           <!-- <div style="height:150px; background:white;"></div> -->
           <div class="menubar"style="height:60px;"><!-- 개인 마이페이지 상단 탭 영역 설정 -->
               <div style="width:10%; height:100%; float:left;"></div>
               <div style="width:80%; height:100%; float:left; ">
                   <div class="menu" id="pro1" onclick="goMenubar1()" >
                          <h4 class="h4">기업 HOME</h4>
                   </div>
                   <div class="menu" id="pro2" onclick="goMenubar2()">
                       
                           <h4 class="h4">기업 상세정보</h4>
                       
                   </div>
                   <div class="menu" id="pro3" onclick="goMenubar3()" >
                         <h4 class="h4">채용공고관리</h4>
                   </div>
                    <div class="menu" id="pro4" onclick="goMenubar4()" >
                         <h4 class="h4">공모전 관리</h4>
                   </div>
                    <div class="menu" id="pro5" onclick="goMenubar5()" >
                         <h4 class="h4">결제내역</h4>
                   </div>
                   <div class="menu" id="pro6" onclick="goMenubar6()" >
                         <h4 class="h4">제안한인재</h4>
                   </div>
                   <div class="menu" id="pro7" onclick="goMenubar7()" >
                         <h4 class="h4">나의 즐겨찾기</h4>
                   </div>
                  
               </div>
               <!--메뉴바 오른쪽여백-->
            <div style="width:10%; height:100%; float:left;"></div>
        </div>
  <!--   </div>전체 화면 div 끝 -->
      <script>
      function goMenubar1(){
          var h4=$('#pro1').children('h4');
          $("#pro1").css("border-bottom","solid #5A84F1");
          h4.css("color","#5A84F1");
         // location.href="";
      }
      function goMenubar2(){
          var h4=$('#pro2').children('h4');
          $("#pro2").css("border-bottom","solid #5A84F1");
          h4.css("color","#5A84F1");
         location.href="/itda/views/company/companyMyPage/companyMyPageinformation/hyj_companyMyPage_information.jsp";
      }
      function goMenubar3(){
          var h4=$('#pro3').children('h4');
          $("#pro3").css("border-bottom","solid #5A84F1");
          h4.css("color","#5A84F1");
         // location.href="";
      }
      function goMenubar4(){
          var h4=$('#pro4').children('h4');
          $("#pro4").css("border-bottom","solid #5A84F1");
          h4.css("color","#5A84F1");
         // location.href="";
      }
      function goMenubar5(){
          var h4=$('#pro5').children('h4');
          $("#pro5").css("border-bottom","solid #5A84F1");
          h4.css("color","#5A84F1"); 
           var comNo = '<%=loginUser.getComNo() %>';
          location.href="<%=request.getContextPath()%>/selectOnePaymentList.pay?comNo=" + comNo;
      }
      function goMenubar6(){
          var h4=$('#pro6').children('h4');
          $("#pro6").css("border-bottom","solid #5A84F1");
          h4.css("color","#5A84F1");
          location.href="/itda/views/company/companyMyPage/companyMyPageinformation/hyj_companyMyPage_ProposedTalent.jsp";
      }
      function goMenubar7(){
          var h4=$('#pro7').children('h4');
          $("#pro7").css("border-bottom","solid #5A84F1");
          h4.css("color","#5A84F1");
         // location.href="";
      }
      
         

      </script>

   

</body>
</html>