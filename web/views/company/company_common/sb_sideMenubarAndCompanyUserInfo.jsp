<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head> 
<meta charset="UTF-8">
<title>Insert title here</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
   <style>
        #cho_userInfo{ 
        	 
            width: 200px;
            height: 350px;
            border: 1px #CCCCCC solid;
            border-radius: 1pc;
            margin-left: auto;
            margin-right: auto;
            text-align: center;
            font-family: 'Noto Sans KR', sans-serif;
            /* margin-top:20px; */
        }
        #cho_personImgDiv{
            width: 60px;
            height: 60px;
            border: solid #5A84F1 1px;
            border-radius: 10pc;
            margin-right: auto;
            margin-left: auto;
            margin-top: 15px;
        }
        #cho_userImg{
            width: 100%;
            height: 100%;
            border-radius: 10pc;
            margin-right: auto;
            margin-left: auto;
            
        }
       #cho_deletemember{
            margin-top: 70px;
            color:red;   
       }
       #cho_sideMenubar{
           width: 200px;
           height: 400px;
           border: 2px solid #5A84F1;
           margin-left: auto;
           margin-right: auto;
           margin-top: 30px;
           font-family: 'Noto Sans KR', sans-serif;
       }
       #cho_sideMenubarTable{
           width: 160px;
           height: 90%;
           margin-left: auto;
           margin-right: auto;
          
       }
       #cho_sideMenubarTableTHaedP{
           font-size: 20px;
           font-weight: bold;
       }
       .sideSubTitle{
           font-size: 13px;
           font-weight: bold;
           border-top: 1px solid #C4C4C4;
           padding-top: 10px;
           padding-bottom: 10px;
       }
       .sideSide{
           padding-left: 0px;
           font-size: 15px;
           font-weight: bold;
       }
    </style>
</head>
<body>
    <!-- 사이드에 회원 정보창 -->
    <div id="cho_userInfo">
        <div id="cho_personImgDiv">
            <img id="cho_userImg" src="../../../image/logo.PNG">
        </div>
				<h3 id="cho_userName"><%=loginUser.getCompanyName() %></h3>
				<h4 id="cho_phone"><%=loginUser.getPhone() %></h4>
				<h4 id="cho_email"><%=loginUser.getEmail() %></h4>
				<h5 id="cho_github"><%=loginUser.getComUrl() %></h5>
				<p id="cho_deletemember" onclick="deletemember()">회원탈퇴</p>
    </div>

    <script>
        function deletemember(){
            //location.href="";
        }
    </script>
    <!-- 개인마이페이지 사이드 메뉴바 미완성!!!!!!!!!!!!!!!!클릭할때 색바뀌는거하기-->
    <div id="cho_sideMenubar">
        <table id="cho_sideMenubarTable">
            
                <tr id="cho_sideMenubarTableTHaed"><td><p id="cho_sideMenubarTableTHaedP">My Page</p></td></tr>
                <tr><td class="sideSide" id="cho_dateil" onclick="goDateil()">기업 상세정보</td></tr>
                <tr><td class="sideSubTitle">- 기업상세정보 보기</td></tr>
                <tr><td class="sideSubTitle" id="cho_resumewri" onclick="goResumeWriter()">- 기업 상세정보 보기 수정</td></tr>
                <tr><td class="sideSide" id="cho_resume" onclick="goResume()">공고관리</td></tr>
                <tr><td class="sideSubTitle">- 채용공고관리</td></tr>
                <tr><td class="sideSubTitle" id="cho_contest" onclick="goContestApp()">- 공모전 관리</td></tr>
                <tr><td class="sideSubTitle" id="cho_recruit" onclick="goRecruit()">결제내역</td></tr>
                <tr><td class="sideSubTitle" id="cho_proposed" onclick="goProposed()">제안한 인재</td></tr>
                <tr><td class="sideSubTitle" id="cho_portfolio" onclick="goPortfolio()">나의 즐겨찾기</td></tr>
        </table>


    </div>
     <script>
        function goDateil(){
            var menu=$('#cho_dateil');
              menu.css("color","#5A84F1");
             // location.href="";
        }
        function goResumeWriter(){
            var menu=$('#cho_resumewri');
              menu.css("color","#5A84F1");
             // location.href="";
        }
        function goResume(){
            var menu=$('#cho_resume');
              menu.css("color","#5A84F1");
             // location.href="";
        }
        function goContestApp(){
            var menu=$('#cho_contest');
              menu.css("color","#5A84F1");
             // location.href="";
        }
        function goRecruit(){
            var menu=$('#cho_recruit');
              menu.css("color","#5A84F1");
             // location.href="";
        }
        function goProposed(){
            var menu=$('#cho_proposed');
              menu.css("color","#5A84F1");
             // location.href="";
        }
        function goPortfolio(){
            var menu=$('#cho_portfolio');
              menu.css("color","#5A84F1");
             // location.href="";
        }
        function goWrited(){
            var menu=$('#cho_writed');
              menu.css("color","#5A84F1");
             // location.href="";
        }
        function goBookMark(){
            var menu=$('#cho_bookMark');
              menu.css("color","#5A84F1");
             // location.href="";
        }

    </script>
</body>
</html>