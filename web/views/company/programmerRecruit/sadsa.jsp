<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <style>
        .jyh_h4{
            text-align: center;
            font-weight: bold;
            font-family: 'Noto Sans KR', sans-serif;
            color: #7a7a7a;
        }
        .jyh_menubar{
            border-top:solid #d8d8d8;
            border-bottom:solid #d8d8d8;
        }
        .jyh_menu{
            text-align: center;
            margin:0;
            height: 95%;
            width:50%;
            float:left;
        }
        a:link { 
            text-decoration: none;
            color: #7A7A7A;
        }
        a:visited { 
            text-decoration: none;
            color: #7A7A7A;
        }
        .article1{
            width:100%;
        }
        .article2{
            width:100%; 
            text-align: center;
        }
        

        #contents{
            margin-left: auto;
            margin-right: auto;
            width:1400px;
            height:1500px;
        }
        #nav{
            width:278px;
        }
        #aside{
            width:280px;
        }
        footer{
            margin-left: auto;
            margin-right: auto;
            width:1400px;
            height:100px;
        }
        .signup-from{
            width:798px;
            margin:auto;
        }
        #jyh_maintable{
            margin:auto; 
            border-collapse:collapse; 
            border-spacing:0;
            width:1400px;

        }
        #jyh_maintable td{
            padding:0px;
        }


        #jyh_sidebar{
            font-weight:bold;
            color:#263747;
        }
    </style>
</head>
<body>
 <%@ include file="../../common/menubar.jsp" %>
  <%@ include file="../company_common/jyh_competitionMenubar.jsp" %>

         <div style="width:35%; height:100%; float:left;"></div>
     </div>

<table id="jyh_maintable" style="width:1400px; " align="center">
    <tr>
        <td style="width:10%;"></td>
        <td colspan="2" style="width:67%;">
            <h3 id="jyh_title" style="margin-left:40px;">채용 공모전 등록</h3>
            <hr style="width:95%; margin-bottom:50px;">
        </td>
        <td style="width:10%;"></td>
    </tr>
    <tr>
        <td style="width:10%;">

        </td>
        <td id="nav" style="vertical-align: top; width:13%; height:500px;">
            <div id="jyh_sidebar" style="margin:auto; border-right:1px solid gray;">
                <p style="margin-top:0px;">제목 및 소개</p>
                <p>참여대상</p>
                <p>채용포지션</p>
                <p>접수 및 과제 제출 기간</p>
                <p>과제 수행 기술 스택</p>
                <p>전형 상세 안내</p>
                <p>요구사항</p>
                <p>개발 환경 설정</p>
            </div>
        </td>
        <td style="vertical-align: top; width:67%;">
            <form id="joinForm" action="asd" method="get" style="width:94%; margin-left:50px;">
                <div class="article1 div1" style="width:100%;">
                    <label>제목</label>
                    <input type="text" name="jyh_title" maxlength="100" placeholder="공모전 제목 입력"><br><br>

                    <label>공모전 소개(자유양식)</label>
                    <div style="width:100%; background:gray; margin-bottom:25px;">
                        api들어갈 자리
                        <br><br><br><br><br><br><br><br><br><br><br><br>
                    </div>
                </div>
                <div class="article1 div2" style="display: none; background:rgb(124, 99, 99);">
                    <h2>공모전 참여 대상</h2>
                    <input id="target" type="text" name="target" value="">
                    <input id="add" type="button" name="add" value="추가">
                    <ul id="ul1">
                        
                    </ul>
                </div>
                <div class="article1 div3" style="display: none; background:rgb(88, 160, 154);">
                        <br><br><br><br><br><br><br><br><br><br><br><br>
                </div>
                <div class="article1 div4" style="display: none; background:rgb(243, 57, 57);">
                        <br><br><br><br><br><br><br><br><br><br><br><br>
                </div>
                <div class="article1 div5" style="display: none; background:rgb(128, 128, 128);">
                        <br><br><br><br><br><br><br><br><br><br><br><br>
                </div>
                <div class="article1 div6" style="display: none; background:rgb(189, 39, 121);">
                        <br><br><br><br><br><br><br><br><br><br><br><br>
                </div>
                <div class="article1 div7" style="display: none; background:rgb(80, 167, 53);">
                        <br><br><br><br><br><br><br><br><br><br><br><br>
                </div>
                <div class="article1 div8" style="display: none; background:rgb(191, 223, 76);">
                        <br><br><br><br><br><br><br><br><br><br><br><br>
                </div>
            </form>
        </td>
    <td id="aside" style="width:10%">
    </td>
    </tr>
    <tr>
        <td colspan="4">
            <div style="width:900px; margin-left:370px; padding-top:10px; padding-bottom:10px;  float:left; border-top:1px solid gray;">
                <input id="next" type="button" name="next" value="다음" style="float:right;">
                <input id="back" type="button" name="back" value="이전" style="float:left; display: none;">
            </div>
        </td>
    </tr>
</table>
    
    <%@ include file="../../common/cho_footer.jsp" %>

    <script>

        var num = 1;

            $(function() {
              //  $(".change-com").hide();
              
			$("#next").click(function() {
                if(num < 8){
                    $(".div" + num).hide();
                    num++;
				    $(".div" + num).show();
                }else if(num == 8){
                    $("#joinForm").submit();
                }
                if(num == 1){
                    $("#back").hide();
                }else{
                    $("#back").show();
                }
			});
			$("#back").click(function() {
                
                if(num > 1){
				    $(".div" + num).hide();
                    num--;
				    $(".div" + num).show();
                }
                if(num == 1){
                    $("#back").hide();
                }else{
                    $("#back").show();
                }
            });
        });

        $(document).on("click","#add",function(){
            $("#ul1").append("<li>"+ $("#target").val() +"</li>");  
        });
        </script>
</body>
</html>