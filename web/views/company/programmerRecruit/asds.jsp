<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="UTF-8">
<title>Document</title>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<style>
a:link {
	text-decoration: none;
	color: #7A7A7A;
}

a:visited {
	text-decoration: none;
	color: #7A7A7A;
}

.article1 {
	width: 840px;
}

.article2 {
	width: 840px;
	text-align: center;
}

#contents {
	margin-left: auto;
	margin-right: auto;
	width: 1400px;
	height: 1500px;
}

#nav {
	width: 278px;
}

#aside {
	width: 280px;
}

footer {
	margin-left: auto;
	margin-right: auto;
	width: 1400px;
	height: 100px;
}

.signup-from {
	width: 798px;
	margin: auto;
}

#jyh_maintable {
	margin: auto;
	border-collapse: collapse;
	border-spacing: 0;
	width: 1400px;
}

#jyh_maintable td {
	padding: 0px;
}

#jyh_sidebar {
	font-weight: bold;
	color: #263747;
	margin-left: 80px;
	
}

#wjs_dv {
	width: 840px;
	height: 120px;
}

#left-img {
	width: 10%;
	height: 8%;
	float: left;
	margin-left: 20px;
	margin-right: 10px;
}

#p1 {
	font-weight: bold;
	font-size: 10pt;
	float: left
}

#left-info {
	height: 5%;
	width: 100%;
	margin-top: 10px;
}
#text-1 {
	font-weight: bold;
	font-size: 19pt;
	margin-top: 20px;
}
#jb {
	width: 1000px;
	height: 10px;
}
</style>
</head>
<body>
	<%@ include file="../../common/menubar.jsp"%>

	<div id="wjs_dv"></div>

	<table id="jyh_maintable" style="border: 1px solid gray;"
		align="center">
		<tr>
			<td colspan="3">
				
				<div
						style=" width: 93%; height: 10%; background-color: rgb(255, 255, 255);  margin-left: 80px; margin-top: 40px;">
						<p id="text-1" style="margin-bottom: 5px;">채용 공고 등록</p>
						<br>
						<div>
							<progress value="15" max="100" id="jb"></progress>
						</div>
						<br><br><br>
					</div>
			</td>
		</tr>
		<tr>
			<td id="nav" style="vertical-align: top;">
				<div id="jyh_sidebar">
					
						<div style="float: left;" id="left-info">
							<img src="/itda/image/wjs/check.png" id="left-img">
							<div id="p1">제목 및 요약</div>
						</div>
						<div id="left-info">
						<div style="float: left;" id="left-info">
							<img src="/itda/image/wjs/nocheck.png" id="left-img">
							<div id="p1">기술 스택</div>
						</div>
						<div id="left-info">
							<div style="float: left;" id="left-info">
								<img src="/itda/image/wjs/nocheck.png" id="left-img">
								<div id="p1">자격 조건</div>
							</div>
						</div>
						<div id="left-info">
							<div style="float: left;" id="left-info">
								<img src="/itda/image/wjs/nocheck.png" id="left-img">
								<div id="p1">우대 사항</div>
							</div>
						</div>
						<div id="left-info">
							<div style="float: left;" id="left-info">
								<img src="/itda/image/wjs/nocheck.png" id="left-img">
								<div id="p1">제출 서류</div>
							</div>
						</div>
						<div id="left-info">
							<div style="float: left;" id="left-info">
								<img src="/itda/image/wjs/nocheck.png" id="left-img">
								<div id="p1">채용 절차</div>
							</div>
						</div>





					</div>
				</div>
			</td>
			<td style="vertical-align: top; width: 840px;" >
				<form id="joinForm" action="asd" method="get" style="width: 840px;">
					<div class="article1 div1">

						<input type="text" id="title" name="title"
							placeholder="채용 공고 제목 입력"
							style="width: 555px; height: 50px; font-size: 1.5em; float: left; margin-left: 50px;">
						<table id="table" style="margin-left: 50px;">
							<tr id="tr">
								<td id="td"><img src="/itda/image/wjs/jikmu.png"
									class="img" style="width: 30px;"></td>
								<td id="td1">
									<!-- <input type="text" id="jikmu" name="jikmu"
													placeholder="직무 내용을 입력하세요"
													style="margin-top: 6px; width: 500px;"> --> <select
									id="jikmu" name="jikmu" multiple="multiple"
									style="width: 500px; height: 50px; margin-top: 10px;">
										<option>웹프로그래머</option>
										<option>DBA·데이터베이스</option>
										<option>응용 프로그래머</option>
										<option>웹 기획PM</option>
										<option>시스템 프로그래머</option>
										<option>웹 디자인</option>
										<option>HTML·퍼블리싱 UI 개발</option>
										<option>웹 마케팅</option>
								</select>

								</td>
							</tr>
							<tr>
								<td id="td"><img src="/itda/image/wjs/gyunglyuk.png"
									class="img" style="width: 30px;"><br></td>
								<td id="td1" style="text-align: left;"><select>
										<option id="option">1</option>
										<option id="option">2</option>
								</select> ~ <select>
										<option id="option">1</option>
										<option id="option">2</option>
								</select>&nbsp;&nbsp;년</td>
							</tr>
							<tr id="tr">
								<td id="td"><img src="/itda/image/wjs/gyumo.png"
									class="img" style="width: 30px;"></td>
								<td id="td1"><input type="text" id="sawon" name="sawon"
									placeholder="회사 사원 수 입력하세요"
									style="margin-top: 6px; width: 500px;"></td>
							</tr>
							<tr id="tr">
								<td id="td"><img src="/itda/image/wjs/service.png"
									class="img" style="width: 30px;"></td>
								<td id="td1"><input type="text" id="service" name="service"
									placeholder="주요 서비스를 입력하세요"
									style="margin-top: 6px; width: 500px;"></td>
							</tr>
							<tr id="tr">
								<td id="td"><img src="/itda/image/wjs/time.png" class="img"
									style="width: 30px;"></td>
								<td id="td1" style="text-align: left;"><input type="text"
									id="time" name="time" placeholder="ex)2020/1/7"
									style="margin-top: 6px; width: 200px;">&nbsp;~ <input
									type="text" id="time1" name="time1" placeholder="ex)2020/1/27"
									style="margin-top: 6px; width: 200px;"></td>
							</tr>
							<tr id="tr">
								<td id="td"><img src="/itda/image/wjs/location.png"
									class="img" style="width: 30px;"></td>
								<td id="td1"><input type="text" id="location"
									name="location" placeholder="회사 위치를 입력해주세요"
									style="margin-top: 6px; width: 500px;"></td>
							</tr>
							<tr id="tr">
								<td id="td" colspan="2"><input type="file"></td>
							</tr>

						</table>
					</div>
					<div class="article1 div2"
						style="display: none; height: 380px;">
						<div
							style="float: left; font-weight: bold; font-size: 18pt; margin-left: 20px;">회사에
							필요한 기술스택은 무엇인가요 ?</div>
						<br> <br>
						<p style="float: left; margin-left: 20px;">더 많은 태그 추가
							(optional)</p>
						<br> <br> <br> <input type="text"
							placeholder="기술 스택 입력 ex) java"
							style="float: left; margin-left: 20px; width: 400px; height: 30px;">
					</div>
					<div class="article1 div3"
						style="display: none;  height: 380px;">
						<div
							style="float: left; font-weight: bold; font-size: 18pt; margin-left: 20px;">이런
							분과 일하고 싶어요.</div>
						<br> <br> <br>

						<div style="float: left; margin-left: 10px;">
							<input type="text" placeholder=" 입력"
								style="width: 400px; height: 30px; margin-left: 10px;">
							<img src="/itda/image/wjs/plus.png"
								style="width: 25px; float: right; margin-left: 20px; margin-top: 6px;">
						</div>
					</div>
					<div class="article1 div4"
						style="display: none;  height: 380px; ">
						<div
							style="float: left; font-weight: bold; font-size: 18pt; margin-left: 20px;">이런
							분은 저희가 더욱 주목합니다.</div>
						<br> <br> <br>
						<div style="float: left; margin-left: 10px;">
							<input type="text" placeholder=" 입력" id="text1" name="text1"
								style="width: 400px; height: 30px; margin-left: 10px;">

							<img src="/itda/image/wjs/plus.png" onclick="insert();"
								style="width: 25px; float: right; margin-left: 20px; margin-top: 6px;">

						</div>

						<br> <br>
						<div id="insertarea" style="float: left; margin-left: 10px;"></div>
					</div>
					<div class="article1 div5"
						style="display: none;  height: 380px;">
						<div
							style="float: left; font-weight: bold; font-size: 18pt; margin-left: 20px;">제출
							서류 입력</div>
						<br> <br> <br>
						<div style="float: left; margin-left: 10px;">
							<input type="text" placeholder=" 제출 서류 입력"
								style="width: 400px; height: 30px; margin-left: 10px;">
							<img src="/itda/image/wjs/plus.png"
								style="width: 25px; float: right; margin-left: 20px; margin-top: 6px;"><br>
							<br>
							<textarea style="float: left; margin-left: 10px; resize: none;"
								rows="5" cols="59" placeholder="제출 서류에 대한 추가 설명"></textarea>
						</div>
					</div>
					<div class="article1 div6"
						style="display: none;  height: 380px;">
						<div
							style="float: left; font-weight: bold; font-size: 18pt; margin-left: 20px;">채용
							절차 입력</div>
						<br> <br> <br>

						<div style="float: left; margin-left: 10px;">
							<textarea style="float: left; margin-left: 10px; resize: none;"
								rows="5" cols="59" placeholder="채용 절차 입력 ex)서류 전형,..."></textarea>
							<img src="/itda/image/wjs/plus.png"
								style="width: 25px; float: right; margin-left: 20px; margin-top: 6px;"><br>
							<br>
						</div>
					</div>
					
				</form>
			</td>
			<td id="aside"></td>
		</tr>
		<tr>
			<td colspan="3">
				<div style="width: 280px; float: left">&nbsp;</div>
				<div
					style="width: 900px; padding-top: 10px; padding-bottom: 10px; float: left;">
					<input id="next" type="button" name="next" value="다음"
						style="float: right;"> <input id="back" type="button"
						name="back" value="이전" style="float: left; display: none;">
				</div>
				<div style="width: 220px; float: left;"></div>
			</td>
		</tr>
	</table>

	<%@ include file="../../common/cho_footer.jsp"%>

	<script>

        var num = 1;

            $(function() {
              //  $(".change-com").hide();
              
			$("#next").click(function() {
				
		    	document.getElementById( 'jb' ).value = 15;
                if(num < 8){
                    $(".div" + num).hide();
                    num++;
				    $(".div" + num).show();
                }else if(num == 8){
                    $("#joinForm").submit();
                }
                if(num == 1){
                    $("#back").hide();
                }else{
                    $("#back").show();
                }
			});
			$("#back").click(function() {
                
                if(num > 1){
				    $(".div" + num).hide();
                    num--;
				    $(".div" + num).show();
                }
                if(num == 1){
                    $("#back").hide();
                }else{
                    $("#back").show();
                }
            });
        });

        $(document).on("click","#add",function(){
            $("#ul1").append("<li>"+ $("#target").val() +"</li>");  
        });
        
        </script>
</body>
</html>