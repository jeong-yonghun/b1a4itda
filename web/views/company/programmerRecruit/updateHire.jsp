<%@page import="com.kh.itda.recruitAndContest.model.vo.Announcement"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	Announcement an = (Announcement) request.getAttribute("an");
%>

<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="UTF-8">
<title>Document</title>
<link rel="stylesheet"
	href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="/resources/demos/style.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>

  $( function() {
    var availableTags = [
    	"DevOn","Front-End","Gauce","Java","Jenkins","Maven","Miplatform","Nexacro","Pro*C","Proframe",
		"Sencha","Spring","Thymeleaf","TrustForm","Tuxedo","Xplatform","Android","Hybrid","iOS(Object-C)",
		"iOS(Swift)","IoT","WebView","ASP","Codeigniter","Laravel","PHP","ZendFramework","Symfony","WordPress",
		"C","C++","ASP.net","C#","MFC","OpenGL","DevExpress","VBA","javaScript","node.js","AngularJs","React.js",
		"Vue.js","jQuery","Embedded","Firmware","LabVIEW","MachineVision","Server","UNIX","MetaLab","Qt",
		"Aduino","CUBRID","MariaDB","MongoDB","MSSQL","MySQL","Oracle","Postgresql","Tibero","ActionScript",
		"CSS","Flash","Git","HTML5","웹접근성","웹표준","MobileApp",".NET","DB","Publisher",
    ];
    $( "#tags" ).autocomplete({
      source: availableTags
    });
  } );
  
  </script>
<style>
.jyh_h4 {
	text-align: center;
	font-weight: bold;
	font-family: 'Noto Sans KR', sans-serif;
	color: #7a7a7a;
}

.jyh_menubar {
	border-top: solid #d8d8d8;
	border-bottom: solid #d8d8d8;
}

.jyh_menu {
	text-align: center;
	margin: 0;
	height: 95%;
	width: 50%;
	float: left;
}

a:link {
	text-decoration: none;
	color: #7A7A7A;
}

a:visited {
	text-decoration: none;
	color: #7A7A7A;
}

.article1 {
	width: 100%;
}

.article2 {
	width: 100%;
	text-align: center;
}

#contents {
	margin-left: auto;
	margin-right: auto;
	width: 1400px;
	height: 1500px;
}

#nav {
	width: 278px;
	height: 40%;
}

#aside {
	width: 280px;
}

footer {
	margin-left: auto;
	margin-right: auto;
	width: 1400px;
	height: 100px;
}

.signup-from {
	width: 798px;
	margin: auto;
}

#jyh_maintable {
	margin: auto;
	border-collapse: collapse;
	border-spacing: 0;
	width: 1400px;
}

#jyh_maintable td {
	padding: 0px;
}

#jyh_sidebar {
	font-weight: bold;
	color: #263747;
}

#left-img {
	width: 13%;
	height: 6%;
	float: left;
	margin-left: 20px;
	margin-right: 10px;
}

#p1 {
	font-weight: bold;
	font-size: 10pt;
	float: left
}

#left-info {
	height: 5%;
	width: 100%;
	margin-top: 10px;
}

#text-1 {
	font-weight: bold;
	font-size: 19pt;
	margin-top: 20px;
}

#jb {
	width: 800px;
	height: 10px;
}

#next {
	background: #5A84F1;
	color: white;
	border-radius: 5px;
	font-size: 12pt;
	border: 0;
	outline: 0;
	width: 80px;
	height: 30px;
}

#back {
	background: #CAC0C0;
	color: black;
	border-radius: 5px;
	font-size: 12pt;
	border: 0;
	outline: 0;
	width: 80px;
	height: 30px;
}

#miri {
	background: white;
	color: #5A84F1;
	border-radius: 5px;
	font-size: 12pt;
	width: 80px;
	height: 30px;
}

#td1 {
	height: 50px;
	margin-top: 10px;
}

#pp {
	margin-top: 15px;
}

#option {
	width: 200px;
}

#modalbtn1, #modalbtn2 {
	color: #175d8b;
	margin: 0 auto;
	width: 70px;
	height: 30px;
}

#skdiv {
	width: 150px;
	height: 40px;
	background: #F7F2E0;
	float: left;
	text-align: center;
	vertical-align: middle;
	padding: 5px 1px 2px 3px;
	border: 1px solid black;
}

#skill {
	width: 800px;
	height: 100px;
}

#skill1 {
	width: 500px;
	height: 200px;
}

.xbtn {
	/* width: 20px;
	height: 20px; */
	border: 0;
	outline: 0;
	background: white;
}
</style>
</head>
<body>
	<%@ include file="../../company/company_common/sb_companyMenubar.jsp"%>

	<div style="width: 35%; height: 100%; float: left;"></div>


	<table id="jyh_maintable" style="width: 1400px;" align="center">
		<tr>
			<td style="width: 10%;"></td>
			<td colspan="2" style="width: 67%;">

				<div
					style="width: 93%; height: 10%; background-color: rgb(255, 255, 255); margin-left: 80px; margin-top: 40px;">
					<br>
					<p id="text-1" style="margin-bottom: 5px;">채용 공고 등록</p>
					<br>
					<div>
						<progress value="100" max="100" id="jb"></progress>
					</div>
					<br> <br> <br>
				</div>
			</td>
			<td style="width: 10%;"></td>
		</tr>
		<tr>
			<td style="width: 15%;"></td>
			<td id="nav" style="vertical-align: top; width: 13%; height: 500px;">
				<div id="jyh_sidebar"
					style="margin: auto; border-right: 1px solid gray; margin-left: 50px;">
					<div style="float: left;" id="left-info">
						<img class="img1" src="/itda/image/wjs/check.png" id="left-img">
						<div id="p1" class="ppp">제목 및 요약</div>
					</div>
					<div id="left-info">
						<div style="float: left;" id="left-info" class="left">
							<img class="img1" src="/itda/image/wjs/nocheck.png" id="left-img">
							<div id="p1" class="ppp">업무 소개</div>
						</div>
						<div id="left-info">
							<div style="float: left;" id="left-info" class="left">
								<img class="img1" src="/itda/image/wjs/nocheck.png"
									id="left-img">
								<div id="p1" class="ppp">기술 스택</div>
							</div>
						</div>
						<div id="left-info">
							<div style="float: left;" id="left-info" class="left">
								<img class="img1" src="/itda/image/wjs/nocheck.png"
									id="left-img">
								<div id="p1" class="ppp">자격 조건</div>
							</div>
						</div>
						<div id="left-info">
							<div style="float: left;" id="left-info" class="left">
								<img class="img1" src="/itda/image/wjs/nocheck.png"
									id="left-img">
								<div id="p1" class="ppp">우대 사항</div>
							</div>
						</div>
						<div id="left-info">
							<div style="float: left;" id="left-info" class="left">
								<img class="img1" src="/itda/image/wjs/nocheck.png"
									id="left-img">
								<div id="p1" class="ppp">제출 서류</div>
							</div>
						</div>
						<div id="left-info">
							<div style="float: left;" id="left-info" class="left">
								<img class="img1" src="/itda/image/wjs/nocheck.png"
									id="left-img">
								<div id="p1" class="ppp">채용 절차</div>
							</div>
						</div>
					</div>
				</div>
			</td>
			<td style="vertical-align: top; width: 62%;">
				<form id="joinForm" name="form" method="post"
					encType="multipart/form-data"
					style="width: 94%; margin-left: 30px;">
					<div class="article1 div1" style="width: 100%; display: none;">
						<input type="text" id="title" name="title"
							placeholder="채용 공고 제목 입력"
							style="width: 619px; height: 50px; font-size: 1.5em; float: left; margin-left: 50px;">
						<table id="table" style="margin-left: 50px;">
							<tr id="tr">
								<td id="td"><img src="/itda/image/wjs/jikmu.png"
									class="img"
									style="width: 30px; margin-top: 10px; margin-right: 10px">
									<p id="pp"
										style="float: right; margin-right: 15px; font-weight: bold;">직무</p>
								</td>
								<td id="td1"><select id="jikmu" name="jikmu"
									style="width: 500px; height: 50px; margin-top: 10px;">

										<option>웹프로그래머</option>
										<option>DBA·데이터베이스</option>
										<option>응용 프로그래머</option>
										<option>웹 기획PM</option>
										<option>시스템 프로그래머</option>
										<option>웹 디자인</option>
										<option>HTML·퍼블리싱 UI 개발</option>
										<option>웹 마케팅</option>
								</select></td>
							</tr>
							<tr>
								<td id="td"><img src="/itda/image/wjs/gyunglyuk.png"
									class="img"
									style="width: 30px; margin-top: 10px; margin-right: 10px">
									<p id="pp"
										style="float: right; margin-right: 15px; font-weight: bold;">경력</p>
									<br></td>
								<td id="td1" style="text-align: left;"><select
									id="gyunglyuk" name="gyunglyuk">
										<option id="option">1</option>
										<option id="option">2</option>
										<option id="option">3</option>
										<option id="option">4</option>
										<option id="option">5</option>
										<option id="option">6</option>
								</select> ~ <select id="gyunglyuk1" name="gyunglyuk1">
										<option id="option">1</option>
										<option id="option">2</option>
										<option id="option">3</option>
										<option id="option">4</option>
										<option id="option">5</option>
										<option id="option">6</option>
								</select>&nbsp;&nbsp;년</td>
							</tr>
							<tr id="tr">
								<td id="td"><img src="/itda/image/wjs/gyumo.png"
									class="img"
									style="width: 30px; margin-top: 10px; margin-right: 10px">
									<p id="pp"
										style="float: right; margin-right: 15px; font-weight: bold;">규모</p>
								</td>

								<td id="td1"><input type="text" id="gyumo" name="gyumo"
									placeholder="회사 사원 수 입력하세요"
									style="margin-top: 6px; width: 500px;"></td>
							</tr>
							<tr id="tr">
								<td id="td"><img src="/itda/image/wjs/service.png"
									class="img"
									style="width: 30px; margin-top: 10px; margin-right: 10px">
									<p id="pp"
										style="float: right; margin-right: 15px; font-weight: bold;">서비스</p>
								</td>
								<td id="td1"><input type="text" id="service" name="service"
									placeholder="주요 서비스를 입력하세요"
									style="margin-top: 6px; width: 500px;"></td>
							</tr>
							<tr id="tr">
								<td id="td"><img src="/itda/image/wjs/time.png" class="img"
									style="width: 30px; margin-top: 10px; margin-right: 10px">
									<p id="pp"
										style="float: right; margin-right: 15px; font-weight: bold;">공고기간</p>
								</td>
								<td id="td1" style="text-align: left;"><input type="date"
									id="time1" name="time1" placeholder="ex)2020/1/7"
									style="margin-top: 6px; width: 200px;">&nbsp;~ <input
									type="date" id="time2" name="time2" placeholder="ex)2020/1/27"
									style="margin-top: 6px; width: 200px;"></td>
							</tr>
							<tr id="tr">
								<td id="td"><img src="/itda/image/wjs/time2.png"
									class="img"
									style="width: 30px; margin-top: 10px; margin-right: 10px">
									<p id="pp"
										style="float: right; margin-right: 15px; font-weight: bold;">접수기간</p>
								</td>
								<td id="td1" style="text-align: left;"><input type="date"
									id="time3" name="time3" placeholder="ex)2020/1/7"
									style="margin-top: 6px; width: 200px;">&nbsp;~ <input
									type="date" id="time4" name="time4" placeholder="ex)2020/1/27"
									style="margin-top: 6px; width: 200px;"></td>
							</tr>


							<tr id="tr">
								<td id="td"><img src="/itda/image/wjs/location.png"
									class="img"
									style="width: 30px; margin-top: 10px; margin-right: 10px">
									<p id="pp"
										style="float: right; margin-right: 15px; font-weight: bold;">위치</p>
								</td>
								<td id="td1">
									<!-- <input type="text" id="location"
									name="location" placeholder="회사 위치를 입력해주세요"
									style="margin-top: 6px; width: 500px;"> --> <input type="text"
									id="sample4_postcode" name="number" placeholder="우편번호">
									<input type="button" onclick="sample4_execDaumPostcode()"
									value="우편번호 찾기"><br> <input type="text"
									id="sample4_roadAddress" name="roadAddress" placeholder="도로명주소">
									<span id="guide" style="color: #999; display: none"></span> <input
									type="text" id="sample4_detailAddress" name="detailAddress"
									placeholder="상세주소"> <!-- <input type="text" id="sample4_extraAddress" placeholder="참고항목"> -->

									<script
										src="https://t1.daumcdn.net/mapjsapi/bundle/postcode/prod/postcode.v2.js"></script>
									<script>
    //본 예제에서는 도로명 주소 표기 방식에 대한 법령에 따라, 내려오는 데이터를 조합하여 올바른 주소를 구성하는 방법을 설명합니다.
    function sample4_execDaumPostcode() {
        new daum.Postcode({
            oncomplete: function(data) {
                // 팝업에서 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분.

                // 도로명 주소의 노출 규칙에 따라 주소를 표시한다.
                // 내려오는 변수가 값이 없는 경우엔 공백('')값을 가지므로, 이를 참고하여 분기 한다.
                var roadAddr = data.roadAddress; // 도로명 주소 변수
                var extraRoadAddr = ''; // 참고 항목 변수

                // 법정동명이 있을 경우 추가한다. (법정리는 제외)
                // 법정동의 경우 마지막 문자가 "동/로/가"로 끝난다.
                if(data.bname !== '' && /[동|로|가]$/g.test(data.bname)){
                    extraRoadAddr += data.bname;
                }
                // 건물명이 있고, 공동주택일 경우 추가한다.
                if(data.buildingName !== '' && data.apartment === 'Y'){
                   extraRoadAddr += (extraRoadAddr !== '' ? ', ' + data.buildingName : data.buildingName);
                }
                // 표시할 참고항목이 있을 경우, 괄호까지 추가한 최종 문자열을 만든다.
                if(extraRoadAddr !== ''){
                    extraRoadAddr = ' (' + extraRoadAddr + ')';
                }

                // 우편번호와 주소 정보를 해당 필드에 넣는다.
                document.getElementById('sample4_postcode').value = data.zonecode;
                document.getElementById("sample4_roadAddress").value = roadAddr;
                document.getElementById("sample4_jibunAddress").value = data.jibunAddress;
                
                // 참고항목 문자열이 있을 경우 해당 필드에 넣는다.
             /*    if(roadAddr !== ''){
                    document.getElementById("sample4_extraAddress").value = extraRoadAddr;
                } else {
                    document.getElementById("sample4_extraAddress").value = '';
                } */

                var guideTextBox = document.getElementById("guide");
                // 사용자가 '선택 안함'을 클릭한 경우, 예상 주소라는 표시를 해준다.
                if(data.autoRoadAddress) {
                    var expRoadAddr = data.autoRoadAddress + extraRoadAddr;
                    guideTextBox.innerHTML = '(예상 도로명 주소 : ' + expRoadAddr + ')';
                    guideTextBox.style.display = 'block';

                } else if(data.autoJibunAddress) {
                    var expJibunAddr = data.autoJibunAddress;
                    guideTextBox.innerHTML = '(예상 지번 주소 : ' + expJibunAddr + ')';
                    guideTextBox.style.display = 'block';
                } else {
                    guideTextBox.innerHTML = '';
                    guideTextBox.style.display = 'none';
                }
            }
        }).open();
    }
</script>
								</td>
							</tr>
							<tr id="tr">
								<td id="td"><img src="/itda/image/wjs/image.png"
									class="img"
									style="width: 30px; margin-top: 10px; margin-right: 10px">
									<p id="pp"
										style="float: right; margin-right: 15px; font-weight: bold;">이미지</p>
								</td>
								<td id="td1"><input type="file" id="companyImage"
									name="companyImage" onchange="loadImg(this);"></td>
							</tr>

						</table>
					</div>
					<div class="article1 div2" style="display: none;">
						<div
							style="float: left; font-weight: bold; font-size: 18pt; margin-left: 20px;">업무소개</div>
						<br> <br> <br>

						<div style="float: left; margin-left: 10px;">
							<textarea style="float: left; margin-left: 10px; resize: none;"
								id="target5" name="target5" rows="5" cols="59"
								placeholder="업무에 대한 간단한 소개"></textarea>
							<img src="/itda/image/wjs/plus.png" id="add4"
								style="width: 25px; margin-left: 20px; margin-top: 20px;">
							<br> <br> <br>
							<br>
							<div style="width: 800px; height: 50px;"></div>

							<ul id="ul5">

							</ul>
						</div>
					</div>

					<div class="article1 div3" style="display: none;">
						<div
							style="float: left; font-weight: bold; font-size: 18pt; margin-left: 20px;">회사에
							필요한 기술스택은 무엇인가요 ?</div>
						<br> <br>
						<p style="float: left; margin-left: 20px;">더 많은 태그 추가
							(optional)</p>
						<br> <br> <br> <input id="tags" name="tags"
							type="text" placeholder="기술 스택 입력 ex) java"
							style="float: left; margin-left: 20px; width: 400px; height: 30px;">
						<img src="/itda/image/wjs/plus.png" id="add7"
							style="width: 25px; margin-left: 20px; margin-top: 12px;">
						<div style="width: 800px; height: 50px;"></div>
						<ul id="ul6">

						</ul>


					</div>
					<div class="article1 div4" style="display: none;">
						<div
							style="float: left; font-weight: bold; font-size: 18pt; margin-left: 20px;">이런
							분과 일하고 싶어요.</div>
						<br> <br> <br>

						<div style="float: left; margin-left: 10px;">
							<input type="text" placeholder=" 입력" id="target" name="target"
								style="width: 400px; height: 30px; margin-left: 10px;">


							<img src="/itda/image/wjs/plus.png" id="add"
								style="width: 25px; margin-left: 20px; margin-top: 12px;">
							<div style="width: 800px; height: 50px;"></div>
							<ul id="ul1">

							</ul>
						</div>
					</div>
					<div class="article1 div5" style="display: none;">
						<div
							style="float: left; font-weight: bold; font-size: 18pt; margin-left: 20px;">이런
							분은 저희가 더욱 주목합니다.</div>
						<br> <br> <br>
						<div style="float: left; margin-left: 10px;">
							<input type="text" placeholder=" 입력" id="target1" name="target1"
								style="width: 400px; height: 30px; margin-left: 10px;">

							<img src="/itda/image/wjs/plus.png" id="add1"
								style="width: 25px; margin-left: 20px; margin-top: 12px;">
							<div style="width: 800px; height: 50px;"></div>
							<ul id="ul2">

							</ul>

						</div>

						<br> <br>
					</div>
					<div class="article1 div6" style="display: none;">
						<div
							style="float: left; font-weight: bold; font-size: 18pt; margin-left: 20px;">제출
							서류 입력</div>
						<br> <br> <br>
						<div style="float: left; margin-left: 10px;">
							<textarea style="float: left; margin-left: 10px; resize: none;"
								id="target3" name="target3" rows="5" cols="59"
								placeholder="제출 서류에 대한 설명"></textarea>
							<img src="/itda/image/wjs/plus.png" id="add2"
								style="width: 25px; margin-left: 20px; margin-top: 12px;">
							<br>
							<br>
							<br>
							<div style="width: 800px; height: 50px;"></div>
							<ul id="ul3">

							</ul>

						</div>
					</div>
					<div class="article1 div7" style="display: none;">
						<div
							style="float: left; font-weight: bold; font-size: 18pt; margin-left: 20px;">채용
							절차 입력</div>
						<br> <br> <br>

						<div style="float: left; margin-left: 10px;">
							<textarea style="float: left; margin-left: 10px; resize: none;"
								id="target4" name="target4" rows="5" cols="59"
								placeholder="채용 절차 입력 ex)서류 전형,..."></textarea>
							<img src="/itda/image/wjs/plus.png" id="add3"
								style="width: 25px; margin-left: 20px; margin-top: 12px;">
							<br> <br> <br>
							<div style="width: 800px; height: 50px;"></div>
							<ul id="ul4">

							</ul>
						</div>
					</div>
					<div id="modal" class="modal fade bs-example-modal-sm"
						tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel"
						aria-hidden="true">
						<div class="modal-dialog modal-sm">
							<div class="modal-content" style="width: 420px;">
								<div>
									<h4
										style="text-align: center; margin-top: 20px; color: #175d8b; font-weight: bold;">유료
										상품 사용 여부</h4>
									<hr>
									<br>
									<div style="float: left; width: 100px; margin-left: 24px;">
										<p style="color: #175d8b; margin-top: 5px;">상품 선택 :</p>
									</div>
									<select name="select" id="select"
										style="float: left; width: 240px; height: 30px;">
										<option id="option1"></option>
										<option id="option2"></option>
										<option id="option3"></option>
									</select> <br>
									<br>
									<br>
									<br>
									<br>
									<br>

									<p style="text-align: center;">사용 시 상품의 수량이 차감됩니다.</p>
									<br>
									<button id="modalbtn1" style="margin-left: 130px;">취소</button>
									&nbsp;&nbsp;
									<button id="modalbtn2">등록</button>
									<div style="height: 30px; background: white;"></div>
								</div>
							</div>
						</div>
					</div>
					<input type="hidden" name="ano" value="<%=an.getaNo() %>">

				</form>
			</td>
			<td id="aside" style="width: 10%"></td>
		</tr>
		<tr>
			<td colspan="4">
				<div
					style="width: 900px; margin-left: 370px; padding-top: 10px; padding-bottom: 10px; float: left; border-top: 1px solid gray;">
					<input id="next" type="button" name="next" value="다음"
						style="float: right;">
					<button id="next2" type="button" class="btn btn-primary"
						data-toggle="modal" data-target=".bs-example-modal-sm"
						style="display: none; float: right; background: #5A84F1; color: white; border-radius: 5px; font-size: 12pt; border: 0; outline: 0; width: 80px; height: 30px;">다음</button>
					<input id="back" type="button" name="back" value="이전"
						style="float: left; display: none;"> <input id="miri"
						type="button" name="miri" onclick="miri();" value="미리보기"
						style="margin-left: 390px; display: none">

				</div>
				<div style="background: white; height: 140px; width: 100%"></div>
			</td>
		</tr>
	</table>


	<%@ include file="../../common/cho_footer.jsp"%>

	<script>
		
            	function miri(){
            		window.open("", "_blank|_self"/* , 'width=1280, height=650, *//*  left=0, top=0, location, menubar, scrollbars, resizable' */); 
            		form.action = "<%=request.getContextPath()%>/miri.re"; 
            		form.target = "_blank|_self"; 
            		form.submit();

            		}
        var val = <%=an.getVal()%>;
        var num = 1;
        var num0 = 1;
        var num1 = 1;
        var num2 = 1;
        var num3 = 1;
        var num4 = 1;

        $(function() {
        	
        	if(val < 6){
			    $(".div" + val).show();
			    $("#miri").css("display", "none");
			    $("#next").val('다음');
			    
        	}
        	 if(val == 1){
                 $("#back").hide();
             }else{
                 $("#back").show();
             }
        	 
        	 if(val == 1){
         		$(".img1").attr("src","/itda/image/wjs/nocheck.png");
         		$(".img1").eq(0).attr("src","/itda/image/wjs/check.png");
         
         	}else if(val == 2){
         		$(".img1").attr("src","/itda/image/wjs/nocheck.png");
         		$(".img1").eq(1).attr("src","/itda/image/wjs/check.png");
         	}else if(val == 3){
         		$(".img1").attr("src","/itda/image/wjs/nocheck.png");
         		$(".img1").eq(2).attr("src","/itda/image/wjs/check.png");
         	}else if(val == 4){
         		$(".img1").attr("src","/itda/image/wjs/nocheck.png");
         		$(".img1").eq(3).attr("src","/itda/image/wjs/check.png");
         	}else if(val == 5){
         		$(".img1").attr("src","/itda/image/wjs/nocheck.png");
         		$(".img1").eq(4).attr("src","/itda/image/wjs/check.png");
         	}else if(val == 6){
         		$(".img1").attr("src","/itda/image/wjs/nocheck.png");
         		$(".img1").eq(5).attr("src","/itda/image/wjs/check.png");
         	}else if(val == 7){
         		$(".img1").attr("src","/itda/image/wjs/nocheck.png");
         		$(".img1").eq(6).attr("src","/itda/image/wjs/check.png");
         	} 
            		
            	    	$(function(){
            	    		console.log(val);
            	    		if(val == 1){
            	    			$(".ppp").eq(0).css("color", "red");
            	    		}else if(val == 2){
            	    			$(".ppp").eq(1).css("color", "red");
            	    		}else if(val == 3){
            	    			$(".ppp").eq(2).css("color", "red");
            	    		}else if(val == 4){
            	    			$(".ppp").eq(3).css("color", "red");
            	    		}else if(val == 5){
            	    			$(".ppp").eq(4).css("color", "red");
            	    		}else if(val == 6){
            	    			$(".ppp").eq(5).css("color", "red");
            	    		}
            	    		
            	    		$("#title").val('<%=an.getaTitle()%>');
            	    		$("#jikmu").val('<%=an.getDuty()%>');
            	    		
            	    		var career = '<%=an.getCareer()%>';
            	    		var ca = career.split("~");
            	    		var c = ca[0];
            	    		var a = ca[1];
            	    		
            	    		$("#gyunglyuk").val(c);
            	    		$("#gyunglyuk1").val(a);
            	    		
            	    		$("#gyumo").val('<%=an.getScale()%>');
            	    		$("#service").val('<%=an.getService()%>');
            	    		
            	    		
            	    		$("input[type=date]").eq(0).val("<%=an.getaStartDate()%>");
            	    		$("input[type=date]").eq(1).val("<%=an.getaEndDate()%>");
            	    		$("input[type=date]").eq(2).val("<%=an.getaReceiptStart()%>");
            	    		$("input[type=date]").eq(3).val("<%=an.getaReceiptEnd()%>");
            	    		
            				var address = '<%=an.getLocation()%>';
            				var address1 = address.split(',');
            				
            				var number = address1[0];
            				var roadAddress = address1[1];
            				var detailAddress = address1[2];
            				
            				$("#sample4_postcode").val(number);
            				$("#sample4_roadAddress").val(roadAddress);
            				$("#sample4_detailAddress").val(detailAddress);
            				
            					
            				var intro = '<%=an.getIntroduce()%>';
            				var intarr = intro.split(',');
            				
            				for(i = 0 ; i < intarr.length; i++){
            					 $('#ul5').append("<li>" + "<label style='margin-bottom:10px; width:80%;'>" 
            		 						+ intarr[i]
            		 						+ "</label> <input type='text' name='target5' readonly value='" 
            		 						+ intarr[i]
            		 						+"' style='display:none;'> <input class='xbtn' type='button' name='delbtn' value='x'>" + "</li>");   
            		            $('#target5').val("");
            		            $('#target5').focus();
            				}
            				
            				var skill = '<%=an.getSkill()%>';
            				var skillarr = skill.split(',');
            				
            				for(i = 0 ; i < skillarr.length; i++){
            					 $('#ul6').append("<li>" + "<label style='margin-bottom:10px; width:80%;'>" 
            		 						+ skillarr[i]
            		 						+ "</label> <input type='text' name='tags' readonly value='" 
            		 						+ skillarr[i]
            		 						+"' style='display:none;'> <input class='xbtn' type='button' name='delbtn' value='x'>" + "</li>");   
            		            $('#tags').val("");
            		            $('#tags').focus();
            				}
            	    		
            				var eli = '<%=an.getEligibility()%>';
            				var eliarr = eli.split(',');
            				
            				for(i = 0 ; i < eliarr.length; i++){
            					 $('#ul1').append("<li>" + "<label style='margin-bottom:10px; width:80%;'>" 
            		 						+ eliarr[i]
            		 						+ "</label> <input type='text' name='target' readonly value='" 
            		 						+ eliarr[i]
            		 						+"' style='display:none;'> <input class='xbtn' type='button' name='delbtn' value='x'>" + "</li>");   
            		            $('#target').val("");
            		            $('#target').focus();
            				}
            				
            				var pref = '<%=an.getPreferential()%>';
            				var prefarr = pref.split(',');
            				
            				for(i = 0 ; i < eliarr.length; i++){
            					 $('#ul2').append("<li>" + "<label style='margin-bottom:10px; width:80%;'>" 
            		 						+ prefarr[i]
            		 						+ "</label> <input type='text' name='target2' readonly value='" 
            		 						+ prefarr[i]
            		 						+"' style='display:none;'> <input class='xbtn' type='button' name='delbtn' value='x'>" + "</li>");   
            		            $('#target2').val("");
            		            $('#target2').focus();
            				}
            				
            				var pres = '<%=an.getPresentation()%>';
            				var presarr = pres.split(',');
            				
            				for(i = 0 ; i < eliarr.length; i++){
            					 $('#ul3').append("<li>" + "<label style='margin-bottom:10px; width:80%;'>" 
            		 						+ presarr[i]
            		 						+ "</label> <input type='text' name='target3' readonly value='" 
            		 						+ presarr[i]
            		 						+"' style='display:none;'> <input class='xbtn' type='button' name='delbtn' value='x'>" + "</li>");   
            		            $('#target3').val("");
            		            $('#target3').focus();
            				}
            				
            				var proc = '<%=an.getProcess()%>';
            				var procarr = proc.split(',');
            				
            				for(i = 0 ; i < eliarr.length; i++){
            					 $('#ul4').append("<li>" + "<label style='margin-bottom:10px; width:80%;'>" 
            		 						+ procarr[i]
            		 						+ "</label> <input type='text' name='target4' readonly value='" 
            		 						+ procarr[i]
            		 						+"' style='display:none;'> <input class='xbtn' type='button' name='delbtn' value='x'>" + "</li>");   
            		            $('#target4').val("");
            		            $('#target4').focus();
            				}
            				
            				
            	    		
            	    	});
            	    	
            	
            	
            	
			$("#next").click(function() {
				document.getElementById( 'jb' ).value += 15;
                if(val < 6){
                    $(".div" + val).hide();
                    val++;
				    $(".div" + val).show();
				    $("#miri").css("display", "none");
				    $("#next").val('다음');
				    
				    if(val == 1){
	            		$(".img1").attr("src","/itda/image/wjs/nocheck.png");
	            		$(".img1").eq(0).attr("src","/itda/image/wjs/check.png");
	            
	            	}else if(val == 2){
	            		$(".img1").attr("src","/itda/image/wjs/nocheck.png");
	            		$(".img1").eq(1).attr("src","/itda/image/wjs/check.png");
	            	}else if(val == 3){
	            		$(".img1").attr("src","/itda/image/wjs/nocheck.png");
	            		$(".img1").eq(2).attr("src","/itda/image/wjs/check.png");
	            	}else if(val == 4){
	            		$(".img1").attr("src","/itda/image/wjs/nocheck.png");
	            		$(".img1").eq(3).attr("src","/itda/image/wjs/check.png");
	            	}else if(val == 5){
	            		$(".img1").attr("src","/itda/image/wjs/nocheck.png");
	            		$(".img1").eq(4).attr("src","/itda/image/wjs/check.png");
	            	}else if(val == 6){
	            		$(".img1").attr("src","/itda/image/wjs/nocheck.png");
	            		$(".img1").eq(5).attr("src","/itda/image/wjs/check.png");
	            	}
				    
				    
				}else if(val == 6){
					$(".img1").attr("src","/itda/image/wjs/nocheck.png");
            		$(".img1").eq(6).attr("src","/itda/image/wjs/check.png");
                	$("#miri").css("display", "block");
                	$(".div" + val).hide();
                    val++;
 				    $(".div" + val).show();
				}
                else if(val == 7){
                 	
                	form.target = "_self"; 
             		$("#joinForm").attr("action","<%=request.getContextPath()%>/update.re")
        			$("#joinForm").submit();
                	
                } 
                if(val == 1){
                    $("#back").hide();
                }else{
                    $("#back").show();
                }
			});
			$("#back").click(function() {
                
                if(val > 1){
				    $(".div" + val).hide();
                    val--;
				    $(".div" + val).show();
				    if(val == 1){
	            		$(".img1").attr("src","/itda/image/wjs/nocheck.png");
	            		$(".img1").eq(0).attr("src","/itda/image/wjs/check.png");
	            
	            	}else if(val == 2){
	            		$(".img1").attr("src","/itda/image/wjs/nocheck.png");
	            		$(".img1").eq(1).attr("src","/itda/image/wjs/check.png");
	            	}else if(val == 3){
	            		$(".img1").attr("src","/itda/image/wjs/nocheck.png");
	            		$(".img1").eq(2).attr("src","/itda/image/wjs/check.png");
	            	}else if(val == 4){
	            		$(".img1").attr("src","/itda/image/wjs/nocheck.png");
	            		$(".img1").eq(3).attr("src","/itda/image/wjs/check.png");
	            	}else if(val == 5){
	            		$(".img1").attr("src","/itda/image/wjs/nocheck.png");
	            		$(".img1").eq(4).attr("src","/itda/image/wjs/check.png");
	            	}else if(val == 6){
	            		$(".img1").attr("src","/itda/image/wjs/nocheck.png");
	            		$(".img1").eq(5).attr("src","/itda/image/wjs/check.png");
	            	}
                }
                if(val == 1){
                    $("#back").hide();
                }else{
                    $("#back").show();
                }
            });
        });
		
   
        $(document).on("click","#add",function(){
       	 $(this).parent().children('ul').append("<li>" + "<label style='margin-bottom:10px; width:80%;'>" 
						+ $(this).parent().children('input[type=text]').val() 
						+ "</label> <input type='text' name='target' readonly value='" 
						+ $(this).parent().children('#target').val() 
						+"' style='display:none;'> <input class='xbtn' type='button' name='delbtn' value='x'>" + "</li>");   
           $(this).parent().children('#target').val("");
           $(this).parent().children('#target').focus();
       });
        
     
       
        $(document).on("click","#add1",function(){
        	 $(this).parent().children('ul').append("<li>" + "<label style='margin-bottom:10px; width:80%;'>" 
 						+ $(this).parent().children('input[type=text]').val() 
 						+ "</label> <input type='text' name='target1' readonly value='" 
 						+ $(this).parent().children('#target1').val()
 						+"' style='display:none;'> <input class='xbtn' type='button' name='delbtn' value='x'>" + "</li>");   
            $(this).parent().children('#target1').val("");
            $(this).parent().children('#target1').focus();
        });
       
        
        $(document).on("click","#add2",function(){
       	 $(this).parent().children('ul').append("<li>" + "<label style='margin-bottom:10px; width:80%;'>" 
						+ $(this).parent().children('textarea').val() 
						+ "</label> <input type='text' name='target3' readonly value='" 
						+ $(this).parent().children('#target3').val() 
						+"' style='display:none;'> <input class='xbtn' type='button' name='delbtn' value='x'>" + "</li>");   
           $(this).parent().children('#target3').val("");
           $(this).parent().children('#target3').focus();
       });
        
        
         $(document).on("click","#add3",function(){
         	 $(this).parent().children('ul').append("<li>" + "<label style='margin-bottom:10px; width:70%;'>" 
  						+ $(this).parent().children('textarea').val() 
  						+ "</label> <input name='target4' readonly  value='"
  						+ $(this).parent().children('#target4').val() 
  						+"' style='display:none;'> <input class='xbtn' type='button' name='delbtn' value='x'>" + "</li>");   
             $(this).parent().children('#target4').val("");
             $(this).parent().children('#target4').focus();
         }); 
        
        $(document).on("click","#add4",function(){
          	 $(this).parent().children('ul').append("<li>" + "<label style='margin-bottom:10px; width:70%;'>" 
   						+ $(this).parent().children('textarea').val() 
   						+ "</label> <input name='target5' readonly  value='"
   						+ $(this).parent().children('#target5').val() 
   						+"' style='display:none;'> <input class='xbtn' type='button' name='delbtn' value='x'>" + "</li>");   
              $(this).parent().children('#target5').val("");
              $(this).parent().children('#target5').focus();
          });
        
        $(document).on("click","#add7",function(){
         	 $(this).parent().children('ul').append("<li>" + "<label style='margin-bottom:10px; width:70%;'>" 
  						+ $(this).parent().children('input[type=text]').val() 
  						+ "</label> <input type='text' name='tags' readonly value='" 
  						+ $(this).parent().children('input[type=text]').val() 
  						+"' style='display:none;'> <input class='xbtn' type='button' name='delbtn' value='x'>" + "</li>");   
             $(this).parent().children('input[type=text]').val("");
             $(this).parent().children('input[type=text]').focus();
         });
      
        
        function loadImg(value){
        		
      			//파일이 존재하면   파일0번째인덱스가 존재하면  파일확인
      			if(value.files && value.files[0]){
      				//파일을  스트림으로 읽어들이는 기능을 하는 api
      				var reader = new FileReader();
      				
      				//파일을 읽어들이면서 변환 베이스64로
      				reader.readAsDataURL(value.files[0]);
      			}
      		}
        
        $("#modalbtn1").click(function(){
        	$('#modal').modal('hide')
        });
        
        $("#modalbtn2").click(function(){
        	form.target = "_self"; 
     		$("#joinForm").attr("action","<%=request.getContextPath()%>/hire.re")
					$("#joinForm").submit();

		});

		$(function() {
			$(".ul1").sortable();
		});

		$(document).on("click", ".xbtn", function() {
			var delList = $(this).parent().remove();
			console.log(delList);
		});
	</script>



</body>
</html>