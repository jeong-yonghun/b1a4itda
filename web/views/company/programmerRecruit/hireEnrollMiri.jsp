<%@page import="com.kh.itda.member.model.vo.Jyh_ComMember"%>
<%@page import="com.kh.itda.recruitAndContest.model.vo.Attachment"%>
<%@page import="com.kh.itda.recruitAndContest.model.vo.Announcement"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
	Announcement an = (Announcement) request.getAttribute("an");
%>
<%
	Attachment at = (Attachment) request.getAttribute("at");
%>
<%
	Jyh_ComMember loginUser = (Jyh_ComMember) request.getAttribute("loginUser");
%>

<!DOCTYPE html>
<html>
<head>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<meta charset="UTF-8">
<title>Insert title here</title>
<style>
	#wjs_div1{
		width:1400px;
		text-align: center;
		height: 100px;
		margin-left: auto;
		margin-right: auto;
	}
	#wjs_h2{
		color: navy;
	}
	#wjs_btn1{
		outline: 0;
		border: 0;
		background: white;
		color:  #5A84F1;
	}
	#nav {
	width: 280px;
	padding: 0;
	}
	#aside{
		width: 280px;
		padding: 0;
	}
	#article1{
		width: 840px;
		height: 50px;
		padding: 0;
		float: left;
	}
	#article2{
		width: 840px;
		height: 1000px;
		padding: 0;
		float: left;
	}
	#wjs_div2{
		width: 560px;
		height: 100%;
		float: left;
	}
	#wjs_img{
		width: 20px;
		height: 20px;
	}
	#wjs_imgtd{
		width: 50px;
		height: 35px;
	}
	#wjs_td1{
		width: 160px;
	}
	#wjs_td2{
		width: 330px;
	}
	#wjs_p{
		font-size: 16px;
		font-weight: bold;
	}
	
	#wjs_div3{
		width: 560px;
		height: 200px;
	}
	body{
		width: 1400px;
		margin-left: auto;
		margin-right: auto;
	}
	#wjs_div4{
		height: auto;
		width: 560px;
	}
	#wjs_p2{
		font-size: 13px;
	}
	
	#wjs_td3{
		width: 150px;
		color: darkgray;
	}
	#wjs_td4{
		width: 300px;
	}
	#wjs_div5{
		float: right;
		width: 280px;
		height: 1500px;
	}
	#wjs_main{
		width: 840px;
		height: 100%;
		float: left;
	}
	#wjs_btn2{
		width: 150px;
		height: 40px;
		border: 0;
		outline: 0;
		background: #5A84F1;
		color: white;
		font-weight: bold;
		border-radius: 5px;
	}
	#wjs_btn3{
		width: 60px;
		height: 40px;
		border: 0;
		outline: 0;
		background: darkgray	;
		color: #5A84F1;
		font-weight: bold;
		border-radius: 5px;
	}
	
	#wjs_imgtd2{
		width: 40px;
		padding: 0;
		height: 24px;
		padding-top: 7px;
	}
	#wjs_td11{
		width: 130px;
		padding: 0;
		margin: 0;
	}
	#wjs_td22{
		width: 120px;
		padding: 0;
	}
</style>
</head>
<body>
	<div id="wjs_div1">
		<h2 id="wjs_h2" >본 화면은 미리보기 화면 입니다.</h2>
		<button id="wjs_btn1" onclick="history.back();">돌아가기</button>
	</div>
	<table>
		<tr>
			<td id="nav"></td>
		
			<td id="article1"></td>
			<td id="article2">
			<div id="wjs_main">
				<div id="wjs_div2">
					<div>
					<h2><%=an.getaTitle() %></h2>
					<h4 style="color:darkgray; "><%=an.getCom_nmae() %></h4>					
					</div>
					<hr style="width: 500px; float: left"> 
					
					<br>
					<p id="wjs_p">요약</p>
					<table>
						<tr>
							<td id="wjs_imgtd"><img id="wjs_img" src="/itda/image/wjs/jikmu.png" style="width: 20px;"></td>
							<td id="wjs_td1">직무</td>
							<td id="wjs_td2"><%=an.getDuty() %></td>
						</tr>
						<tr>
							<td id="wjs_imgtd"><img id="wjs_img" src="/itda/image/wjs/gyunglyuk.png"></td>
							<td id="wjs_td1">경력</td>
							<td id="wjs_td2"><%=an.getCareer() %></td>
						</tr>
						<tr>
							<td id="wjs_imgtd"><img id="wjs_img" src="/itda/image/wjs/gyumo.png"></td>
							<td id="wjs_td1">회사규모</td>
							<td id="wjs_td2"><%=an.getScale() %></td>
						</tr>
						<tr>
							<td id="wjs_imgtd"><img id="wjs_img" src="/itda/image/wjs/service.png"></td>
							<td id="wjs_td1">주요 서비스</td>
							<td id="wjs_td2"><%=an.getService() %></td>
						</tr>
						<tr>
							<td id="wjs_imgtd"><img id="wjs_img" src="/itda/image/wjs/time.png"></td>
							<td id="wjs_td1">기간</td>
							<td id="wjs_td2"><%=an.getaReceiptStart() %> ~ <%=an.getaReceiptEnd() %></td>
						</tr>
						<tr>
							<td id="wjs_imgtd"><img id="wjs_img" src="/itda/image/wjs/location.png"></td>
							<td id="wjs_td1">위치</td>
							<td id="wjs_td2"><%=an.getLocation() %></td>
						</tr>
					</table>
					
					<div>
						<div id="wjs_div3">
							<p id="wjs_p">기술스택</p>
						</div>
						
						
					</div>
					
					<div id="wjs_div4">
						<p id="wjs_p">업무 소개</p>
						<p>
							<%=an.getIntroduce() %>
						</p>
					</div>
					
					<div id="wjs_div4">
						<p id="wjs_p">자격조건</p>
						<p id="wjs_p">이런분과 일하고 싶어요</p>
						<p>
							<%=an.getEligibility()%>
						</p>
									
					</div>
					
					<div id="wjs_div4">
						<p id="wjs_p">우대사항</p>
						<p id="wjs_p">이런분은 저희가 더욱 주목합니다</p>
						<p>
							<%=an.getPreferential() %>
						</p>			
					</div>
					
					<div id="wjs_div4">
						<p id="wjs_p">제출 서류</p>
						<p>
							<%=an.getPresentation() %>
						</p>
					</div>
					
					<div id="wjs_div4">
						<p id="wjs_p">채용 절차</p>
						<p>
							<%=an.getProcess() %>
						</p>		
					</div>
					
				
					<br><br>
					
					
					
					<div id="wjs_div4">
						<p id="wjs_p">[개인정보 처리방침]</p>
						<p id="wjs_p2">당사는 최소한의 개인정보를 수집하여 처리하고 있습니다. 이용자의 개인정보를 목적 달성을 위한 기간동안에만 제한적으로 처리하고 있으며, 처리목적이 달성되면 해당 이용자의 개인정보는 지체 없이 파기 됩니다. (다만 지원이력 확인을 위해 지원일로부터 2년간 보관 후 파기) 이용자는 언제든지 등록되어 있는 자신의 개인정보를 조회하거나 수정하실 수 있으며 정보 삭제 및 처리 정지를 요구 할 수 도 있습니다.</p>		
					</div>
					
					
					
					<div style="height: 200px; background: white"></div>
					
				</div>
					<div id="wjs_div5">
						<button id="wjs_btn2">지원하기</button>
						<button id="wjs_btn3">이전</button>
						
						<br><br><br>	
						
						
					<div id="wjs_div4">
						<p id="wjs_p">회사 정보</p>
						<img src="<%=request.getContextPath() %>/uploadFiles/<%=at.getChangeName()%>" style="width: 60px; height: 60px; float: left; margin-right: 30px;">
						<p style="margin: 5;"><%=an.getCom_nmae() %></p>
						<p style="margin: 0;"><%=an.getService() %></p>
								
					</div>
						
											
						<br><br><br>	
					<table style="float: left">
						<tr>
							<td id="wjs_imgtd2"><img id="wjs_img" src="/itda/image/wjs/homepage.png"></td>
							<td id="wjs_td11">회사 홈페이지</td>
							<td id="wjs_td22"><a style="color:#5A84F1; ">바로가기</a></td>
						</tr>
						<tr>
							<td id="wjs_imgtd2"><img id="wjs_img" src="/itda/image/wjs/people.png"></td>
							<td id="wjs_td11">사원수</td>
							<td id="wjs_td22">비공개</td>
						</tr>
						<tr>
							<td id="wjs_imgtd2"><img id="wjs_img" src="/itda/image/wjs/tuja.png"></td>
							<td id="wjs_td11">투자</td>
							<td id="wjs_td22">비공개</td>
						</tr>
						<tr>
							<td id="wjs_imgtd2"><img id="wjs_img" src="/itda/image/wjs/money.png"></td>
							<td id="wjs_td11">매출</td>
							<td id="wjs_td22">비공개</td>
						</tr>
			
					</table>
						
					<div style="height: 100px; background: white; width: 280px; float: left"></div>
						<p id="wjs_p">위치</p>
					<div id="map" style="width: 300px; height: 300px; float: left;">
					</div>	
<script type="text/javascript" src="//dapi.kakao.com/v2/maps/sdk.js?appkey=4a540e61addaade287b076fd6d8a8d21&libraries=services"></script>

<script>
var mapContainer = document.getElementById('map'), // 지도를 표시할 div 
    mapOption = {
        center: new kakao.maps.LatLng(33.450701, 126.570667), // 지도의 중심좌표
        level: 3 // 지도의 확대 레벨
    };  

// 지도를 생성합니다    
var map = new kakao.maps.Map(mapContainer, mapOption); 

// 주소-좌표 변환 객체를 생성합니다
var geocoder = new kakao.maps.services.Geocoder();

// 주소로 좌표를 검색합니다
geocoder.addressSearch('제주특별자치도 제주시 첨단로 242', function(result, status) {

    // 정상적으로 검색이 완료됐으면 
     if (status === kakao.maps.services.Status.OK) {

        var coords = new kakao.maps.LatLng(result[0].y, result[0].x);

        // 결과값으로 받은 위치를 마커로 표시합니다
        var marker = new kakao.maps.Marker({
            map: map,
            position: coords
        });

        // 인포윈도우로 장소에 대한 설명을 표시합니다
        var infowindow = new kakao.maps.InfoWindow({
            content: '<div style="width:150px;text-align:center;padding:6px 0;">우리회사</div>'
        });
        infowindow.open(map, marker);

        // 지도의 중심을 결과값으로 받은 위치로 이동시킵니다
        map.setCenter(coords);
    } 
});    
</script>
						
						
						
						
						
					</div>
					
				</div>
			</td>
		
			<td id="aside"></td>
		</tr>
	
	</table>

</body>
<script>
	$(function(){
		
		var intro = <%=an.getIntroduce()%>
		var array = intro.split(",");
		

		  for(i = 0; i < array.length; i++){
		  array[i].split("");
		  }
		
	});
</script>
</html>






