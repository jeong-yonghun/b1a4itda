<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="UTF-8">
<title>Document</title>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<style>
.jyh_h4 {
	text-align: center;
	font-weight: bold;
	font-family: 'Noto Sans KR', sans-serif;
	color: #7a7a7a;
}

.jyh_menubar {
	border-top: solid #d8d8d8;
	border-bottom: solid #d8d8d8;
}

.jyh_menu {
	text-align: center;
	margin: 0;
	height: 95%;
	width: 50%;
	float: left;
}

a:link {
	text-decoration: none;
	color: #7A7A7A;
}

a:visited {
	text-decoration: none;
	color: #7A7A7A;
}

.article1 {
	width: 100%;
}

.article2 {
	width: 100%;
	text-align: center;
}

#contents {
	margin-left: auto;
	margin-right: auto;
	width: 1400px;
	height: 1500px;
}

#nav {
	width: 278px;
}

#aside {
	width: 280px;
}

footer {
	margin-left: auto;
	margin-right: auto;
	width: 1400px;
	height: 100px;
}

.signup-from {
	width: 798px;
	margin: auto;
}

#jyh_maintable {
	margin: auto;
	border-collapse: collapse;
	border-spacing: 0;
	width: 1400px;
}

#jyh_maintable td {
	padding: 0px;
}

#jyh_sidebar {
	font-weight: bold;
	color: #263747;
}

ul {
	list-style: none;
	margin: 0;
	padding: 0;
}

li {
	float: left;
	margin-right: 10px;
}

#jyh_per-list {
	border-collapse: collapse;
	border: 1px solid #168;
	width: 100%;
}

#jyh_per-list th {
	color: #168;
	background: #f0f6f9;
	text-align: center;
}

#jyh_per-list th, #jyh_per-list td {
	padding: 10px;
	/* border: 1px solid #ddd; */
}

#jyh_per-list th:first-child, #jyh_per-list td:first-child {
	border-left: 0;
}

#jyh_per-list th:last-child, #jyh_per-list td:last-child {
	border-right: 0;
}

#jyh_per-list tr td:first-child {
	text-align: center;
}
</style>
</head>
<body>

	<%@ include file="../common/menubar.jsp"%>

	<table id="jyh_maintable" style="width: 1400px;" align="center">
		<tr>
			<td colspan="3" style="width: 100%; height: 150px; background: red;">
			</td>
		</tr>
		<tr>
			<td id="nav"
				style="width: 15%; height: 30px; background: rgb(221, 212, 212);">
			</td>


			<td style="vertical-align: top; width: 70%; height: 900px;">
				<div id="jyh_filter-div" style="width: 100%; height: 30px;">
					<div style="float: left;">
						<input type="text" name="skill-search">
					</div>
					<div style="float: right;">
						<select>
							<option>지역1</option>
							<option>지역1</option>
							<option>지역1</option>
						</select> <select>
							<option>직무1</option>
							<option>지역1</option>
							<option>지역1</option>
						</select> <select>
							<option>경력1</option>
							<option>지역1</option>
							<option>지역1</option>
						</select>
					</div>
				</div>
				<div id="jyh_filter-add" style="width: 100%; background: gray;">
					<ul>
						<li style="background: gray;">aaa</li>
						<li style="background: gray;">aaa</li>
						<li style="background: gray;">aaa</li>
						<li style="background: gray;">aaa</li>
					</ul>
				</div>
				<div style="width: 100%;">
					<table id="jyh_per-list" border="1" align="center"
						style="width: 100%;">
						<tr>
							<th colspan="2">이름</th>
							<th>이력서 요약</th>
							<th>업데이트</th>
						</tr>
						<tr>
							<td><input type="button" name="Favorites" value="즐찾">
							</td>
							<td>
								<ul>
									<li>이름</li>
									<li>경력여부</li>
								</ul>
								<br>
								<ul style="margin-top: 10px;">
									<li>생년월일 데이터</li>
								</ul>
							</td>
							<td>
								<h5 style="margin-top: 5px;">이력서 제목 데이터</h5>
								<ul>
									<li>직무 데이터</li>
									<li>지역 데이터</li>
									<li>프로젝트 개수 데이터</li>
								</ul>
								<br>
								<ul style="margin-top: 10px;">
									<li>스킬 데이터</li>
								</ul>
							</td>
							<td>업데이트 날짜 데이터</td>
						</tr>
					</table>
				</div>
			</td>

			<td id="aside"
				style="width: 15%; height: 30px; background: rgb(221, 212, 212);">
			</td>
		</tr>
	</table>

	<%@ include file="../common/cho_footer.jsp"%>

</body>
</html>