<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"
	import="java.util.ArrayList, java.util.HashMap, com.kh.itda.approval.model.vo.*"%>
<%
	ArrayList<HashMap<String, Object>> list = (ArrayList<HashMap<String, Object>>) request.getAttribute("list");
%>
<!DOCTYPE html>
<html lang="ko">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <style>
	.jyh_h4{
            text-align: center;
            font-weight: bold;
            font-family: 'Noto Sans KR', sans-serif;
            color: #7a7a7a;
        }
        .jyh_menubar{
            border-top:solid #d8d8d8;
            border-bottom:solid #d8d8d8;
        }
        .jyh_menu{
            text-align: center;
            margin:0;
            height: 95%;
            width:50%;
            float:left;
        }
        a:link { 
            text-decoration: none;
            color: #7A7A7A;
        }
        a:visited { 
            text-decoration: none;
            color: #7A7A7A;
        }
        .article1{
            width:100%;
        }
        .article2{
            width:100%; 
            text-align: center;
        }
        

        #contents{
            margin-left: auto;
            margin-right: auto;
            width:1400px;
            height:1500px;
        }
        #nav{
            width:278px;
        }
        #aside{
            width:280px;
        }
        footer{
            margin-left: auto;
            margin-right: auto;
            width:1400px;
            height:100px;
        }
        .signup-from{
            width:798px;
            margin:auto;
        }
        #jyh_maintable{
            margin:auto; 
            border-collapse:collapse; 
            border-spacing:0;
            width:1400px;

        }
        #jyh_maintable td{
            padding:0px;
        }


        #jyh_sidebar{
            font-weight:bold;
            color:#263747;
        }

        ul {
            list-style:none;
            margin:0;
            padding:0;
        }

        li {
            margin-right: 10px;
        }
        .jyh_competition-schedule{
        	border-radius: 10px;
        }
        .jb-wrap {
			width: 40%;
			margin: 0px auto;
			position: relative;
		}
		.jb-wrap img {
			width: 100%;
			vertical-align: middle;
		}
		.jb-text {
			position: absolute;
			top: 0px;
			width: 100%;
			height: 100%;
		}
		.jb-text-table {
			display: table;
			width: 100%;
			height: 100%;
		}
		.jb-text-table-row {
			display: table-row;
		}
		.jb-text-table-cell {
			display: table-cell;
			vertical-align: middle;
		}
		.jb-text p, .jb-text h2 {
			margin: 0px 40px;
			padding: 10px;
			background-color: #ffffff;
			text-align: center;
        	background-color: rgba( 255, 255, 255, 0.01 );
		}
		img{
			border-radius: 10px;		
		}	
    </style>
</head>
<body>

<%@include file="/views/common/menubar.jsp" %>

<div class="jyh_empty" style="height:75px;"></div>

<table id="jyh_maintable" style="width:1400px; " align="center">
    <tr>
        <td colspan="3" style="width:100%; height: 30px;">
        </td>
    </tr>
    <tr>
        <td id="nav" style="width:15%; height:30px;">
        </td>


        <td style="vertical-align: top; width:70%;">
            <h4>진행중인 채용 공모전</h4>
            <ul style="width:100%; ">
             			<%
							for (int i = 0; i < list.size(); i++) {
							HashMap<String, Object> hmap = list.get(i);
							/* if(hmap.get("status").equals("진행중")){ */
						 %>
                <li style="width:100%; height:200px; margin-top:15px;">
                	<form class="detailCompetition" name="form" action="<%= request.getContextPath() %>/jyhCompetitionContent.re" method="post">
                		<input type="text" name="aNo" value="<%=hmap.get("aNo") %>" style="display:none;">
                	</form>
                    <div id="jyh_row-div1" style="width:100%; height:100%;  background:#FBFBFD; ">
                        <div id="jyh_img-div" style="width:45%; height:100%; padding:0px 16px; float:left;">
                        
                            <div class="jb-wrap" style="width:100%; height:100%;">
			<div class="jb-image" style="text-align:center;"><img class="img" style="width:100%; max-height: 200px;" src="<%=hmap.get("url")%>"></div>
			<div class="jb-text">
				<div class="jb-text-table">
					<div class="jb-text-table-row">
						<div class="jb-text-table-cell">
							<h2><%=hmap.get("title")%></h2>
						</div>
					</div>
				</div>
			</div>
		</div>
					
                        </div>
                        <div id="jyh_info-div" style="width:45%; height:100%;padding:0px 16px 0px 32px; float:right;">
                            <h2><%=hmap.get("title")%></h2>
                            <ul>
                                <li style="margin-top:10px;">
                                   		<%=hmap.get("receiptStart")%> ~ <%=hmap.get("receiptEnd")%>
                                </li>
                                <li style="margin-top:10px;">
                                    <%=hmap.get("progressStart")%> ~ <%=hmap.get("progressEnd")%>
                                </li>
                                <li style="margin-top:25px;">
                                    <input type="button" name="detail-btn" value="자세히보기">
                                </li>
                            </ul>
                        </div>
                    </div>
                </li>
                	<%
						}/* } */
					%>
            </ul>
            
            
          <%--   <h4 style="margin-top:100px;">종료된 채용 공모전</h4>
            <ul style="width:100%;">
            			<%
							for (int i = 0; i < list.size(); i++) {
							HashMap<String, Object> hmap = list.get(i);
							if(hmap.get("status").equals("종료")){
						 %>
                <li style="width:100%; height:200px; margin-top:15px;">
                	<form class="detailCompetition" name="form" action="<%= request.getContextPath() %>/jyhCompetitionContent.re" method="post">
                		<input type="text" name="aNo" value="<%=hmap.get("aNo") %>" style="display:none;">
                	</form>
                    <div id="jyh_row-div1" style="width:100%; height:100%;  background:#FBFBFD; ">
                        <div id="jyh_img-div" style="width:45%; height:100%; padding:0px 16px; float:left;">
                            <div class="jb-wrap" style="width:100%; height:100%;">
								<div class="jb-image" style="text-align:center;">
									<img class="img" style="width:100%; max-height: 200px;" src="<%=hmap.get("url")%>">
								</div>
								<div class="jb-text">
									<div class="jb-text-table">
										<div class="jb-text-table-row">
											<div class="jb-text-table-cell">
												<h2><%=hmap.get("title")%></h2>
											</div>
										</div>
									</div>
								</div>
							</div>
                        </div>
                        <div id="jyh_info-div" style="width:45%; height:100%; padding:0px 16px 0px 32px; float:right;">
                            <h2><%=hmap.get("title")%></h2>
                            <ul>
                                <li style="margin-top:10px;">
                                    <%=hmap.get("receiptStart")%> ~ <%=hmap.get("receiptEnd")%>
                                </li>
                                <li style="margin-top:10px;">
                                    <%=hmap.get("progressStart")%> ~ <%=hmap.get("progressEnd")%>
                                </li>
                                <li style="margin-top:25px;">
                                    <input type="button" name="detail-btn" value="자세히보기">
                                </li>
                            </ul>
                        </div>
                    </div>
                </li>
                	<%
						}}
					%>
            </ul> --%>
        </td>

    <td id="aside" style="width:15%; height:30px;">
    </td>
    </tr>
</table>

<%@ include file="../../common/cho_footer.jsp"%>

<script>
	$(function(){
		$(".jb-text-table-cell").click(function(){
			/* console.log($(this).parent().parent().parent().parent().parent().parent().parent().children('form').children().val());
			alert($(this).parent().parent().parent().parent().parent().parent().parent().children('form').children().val()); */
			$($(this).parent().parent().parent().parent().parent().parent().parent().children('form')).submit();
		});
		$("input[type='button']").click(function(){
			$($(this).parent().parent().parent().parent().parent().children('form')).submit();
		});
		
		
	});
</script>

</body>
</html>