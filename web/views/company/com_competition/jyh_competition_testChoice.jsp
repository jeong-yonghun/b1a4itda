<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <style>
        a:link { 
            text-decoration: none;
            color: #7A7A7A;
        }
        a:visited { 
            text-decoration: none;
            color: #7A7A7A;
        }
        .article1{
            width:840px;
        }
        .article2{
            width:840px; 
            text-align: center;
        }
        #contents{
            margin-left: auto;
            margin-right: auto;
            width:1400px;
            height:1500px;
        }
        #nav{
            width:530px;
            vertical-align: top;
            color:#4876EF;
        }
        #aside{
            width:530px;
        }
        footer{
            margin-left: auto;
            margin-right: auto;
            width:1400px;
            height:100px;
        }
        table{
            margin:auto;
            border-collapse:collapse;
            border-spacing:0;
        }
        td{
            margin:0;
            padding:0;
        }
        .jyh_testChoice-outline{
            border:1px solid gray;
            width:840px;
            border-radius: 10px;
            height:100%;
        }
        .jyh_testChoice1{
            width:97.5%;
            height:100px;
            margin:auto;
            background:gray;
        }
        h2, h5{
            margin:2px;
        }
        .jyh_test-info{
            margin-left:100px;
        }
    </style>
</head>
<body>
<table>
    <tr>
        <td id="nav">
        </td>
    <td>
            <div class="article1 jyh_empty" style="text-align:center; padding-top:50px; padding-bottom:50px;">
            </div>
            <div class="article2 jyh_title" style="text-align:center; margin-bottom:50px;">
                <h1 class="jyh_title1" >과제 선택</h1>
            </div>
            <div class="jyh_testChoice-outline">
                <div class="jyh_testChoice jyh_testChoice1" style="margin:10px;">
                    <div class="jyh_testChoice-info" style="float:left; width:80%; height:100%;">
                        <h2 class="jyh_test-info" style="margin-top:13px;">테스트 제목 데이터</h2>
                        <h5 class="jyh_test-info">테스트 종류</h5>
                        <h5 class="jyh_test-info">테스트 날짜 데이터</h5>
                    </div>
                    <div class="jyh_testChoice-info" style="float:left; width:20%; height:30px; text-align: right;">
                        <h4 style="margin:37px 10px 37px 0px;">시작안함 &gt;</h4>
                    </div>
                </div>
                <hr>
                <div class="jyh_testChoice jyh_testChoice1" style="margin:10px;">
                        <div class="jyh_testChoice-info" style="float:left; width:80%; height:100%;">
                            <h2 class="jyh_test-info" style="margin-top:13px;">테스트 제목 데이터</h2>
                            <h5 class="jyh_test-info">테스트 종류</h5>
                            <h5 class="jyh_test-info">테스트 날짜 데이터</h5>
                        </div>
                        <div class="jyh_testChoice-info" style="float:left; width:20%; height:30px; text-align: right;">
                            <h4 style="margin:37px 10px 37px 0px;">시작안함 &gt;</h4>
                        </div>
                    </div>
            </div>
    </td>
    <td id="aside">
    </td>
    </tr>
</table>
    
    <%@ include file="../../common/cho_footer.jsp" %>

</body>
</html>