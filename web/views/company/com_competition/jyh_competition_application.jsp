<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.util.HashMap, java.sql.*, com.kh.itda.member.model.vo.*"%>
<%
	Jyh_PerMember pm = (Jyh_PerMember) request.getAttribute("perMem");
	String position = request.getParameter("position");
	String aNo = request.getParameter("aNo");
	String division = request.getParameter("division");
%>

<!DOCTYPE html>
<html lang="ko">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <style>
        a:link { 
            text-decoration: none;
            color: #7A7A7A;
        }
        a:visited { 
            text-decoration: none;
            color: #7A7A7A;
        }
        .article1{
            width:840px;
        }
        .article2{
            width:840px; 
            text-align: center;
        }
        #contents{
            margin-left: auto;
            margin-right: auto;
            width:1400px;
            height:1500px;
        }
        #nav{
            width:530px;
            vertical-align: top;
            color:#4876EF;
        }
        #aside{
            width:530px;
        }
        footer{
            margin-left: auto;
            margin-right: auto;
            width:1400px;
            height:100px;
        }
        table{
            margin:auto;
            border-collapse:collapse;
            border-spacing:0;
        }
        td{
            margin:0;
            padding:0;
        }
        .jyh_competition-application{
            width:45%;
            margin:auto;
        }
        .jyh_competition-application2, .jyh_competition-application3, .jyh_competition-application4{
            width:100%;
            margin:auto;
        }
        .jyh_competition-application4{
            border:1px solid rgba(128, 128, 128, 0.534);
        }
        .jyh_basic-info{
            width:98%;
            height:40px;
            padding:4px 0 0 5px;
            font-size: 15pt;
            border: 1px solid #5A84F1;
            border-radius: 5px;
            margin-top:10px;
            margin-bottom:40px;
        }
        input[name=nextbtn], input[name=backbtn]{
            width:60px;
            height:31px;
            font-size: 15pt;
            border: 1px solid #5A84F1;
            border-radius: 5px;
            margin-top:5px;
            margin-bottom:40px;
            margin-left:10px;
        }
        h3, h5{
            margin:0px;
        }
        #jyh_position{
            width:100%;
            background:#E9ECF3;
            border-radius: 10px;
            padding:5px;
            margin-bottom:10px;
            margin-top:5px;
        }
        #jyh_position ul li{
            margin:10px;
        }
        .jyh_competition-application4{
            width:100%;
            border-radius: 10px;
            padding:5px;
            margin-bottom:10px;
        }
        .jyh_competition-outline *{
            color:rgb(77, 77, 77);
        }
    </style>
</head>
<body>
<table>
    <tr>
        <td id="nav">
                <div id="jyh_all-back"> <h2 style="float:left; margin:0px;">< &nbsp;</h2> <h3 style="float:left; margin:0px; margin-top:6px;">돌아가기</h3></div>
        </td>
    <td>
            <div class="article1 jyh_progress" style="text-align:center; padding-top:50px; padding-bottom:50px;">
                <img class="jyh_progress1" src="/itda/image/application1.PNG" style="max-width: 500px; vertical-align: middle;" />
                <img class="jyh_progress2" src="/itda/image/application2.PNG" style="max-width: 500px; vertical-align: middle; display: none;" />
                <img class="jyh_progress3" src="/itda/image/application3.PNG" style="max-width: 500px; vertical-align: middle; display: none;" />
                <img class="jyh_progress4" src="/itda/image/application4.PNG" style="max-width: 500px; vertical-align: middle; display: none;" />
            </div>
            <div class="article2 jyh_title" style="text-align:center; margin-bottom:50px;">
                <h1 class="jyh_title1" >기본 정보 입력</h1>
                <h1 class="jyh_title2" style="display: none;">추가 정보 입력</h1>
                <h1 class="jyh_title3" style="display: none;">지원 포지션 선택</h1>
                <h1 class="jyh_title4" style="display: none;">미리보기</h1>
                <h3 style="color:rgb(151, 151, 151);">공모전 제목 데이터</h3>
            </div>
            <form id="joinForm" action="<%= request.getContextPath() %>/jyhCompetitionApplication.re" method="post"  style="width:840px;">
            <div class="jyh_competition-outline">
                <div class="jyh_competition-application jyh_competition-application1">
                    <h3>이름</h3>
                    <input id="name" class="jyh_basic-info" type="text" value="<%= pm.getPerName() %>"  readonly name="name">
                    <h3>이메일</h3>
                    <input id="email" class="jyh_basic-info" type="email" value="<%= pm.getEmail() %>"  readonly  name="email">

                </div>
                <div class="jyh_competition-application jyh_competition-application2" style="display: none;">
                    <h3>휴대전화 번호를 입력해주세요.</h3>
                    <input id="phone" class="jyh_basic-info" type="tel" value="<%= pm.getPhone() %>"  readonly  name="phone">
                    <hr style="margin-bottom:50px; height:0px; border:0.5px solid rgba(128, 128, 128, 0.199);">
                    <h3>본인의 개발 업무 경력을 작성해주세요.</h3>
                    <input id="career" class="jyh_basic-info" type="text" name="career" placeholder=" ex) n년 n월">
                </div>
                <div class="jyh_competition-application jyh_competition-application3" style="display: none;">
                    <h5>지원할 포지션을 선택해주세요.</h5>
                    <div id="jyh_position">
                        <ul>
                            <li>한 회사에 1개의 포션만 선택 가능합니다.</li>
                            <li>최대 1개 까지의 포지션을 선택 가능합니다.</li>
                        </ul>
                    </div>
                    <input type="radio" name="position-choice" checked id="choice1"><label><%= position%></label>
                </div>
                <div class="jyh_competition-application jyh_competition-application4" style="display: none;">
                    <div style="margin-bottom: 30px;">
                        <h5>기본 정보 입력</h5>
                        <ul>
                            <li>이름 ㅡ <input id="ex-name" type="text" name="ex-name" readonly style="border:none;"></li>
                            <li>이메일 ㅡ <input id="ex-email" type="text" name="ex-email" readonly style="border:none;"></li>
                        </ul>
                    </div>
                    <div>
                        <h5 style="margin-bottom: 10px;">추가 정보 입력</h5>
                        <div>
                            <div style="margin-bottom: 20px;">
                                <h5>휴대전화 번호를 입력해주세요.</h5>
                                <h3 style="margin:0px; margin-top:5px; float:left;">&rarr;</h3><p style="margin:0px; margin-top:5px; font-size:11pt;"> 
                                	<input id="ex-phone" type="text" name="ex-phone" readonly style="border:none;"></p>
                            </div>
                            <div style="margin-bottom: 20px;">
                                <h5>본인의 개발 업무 경력을 작성해주세요.</h5>
                                <h3 style="margin:0px; margin-top:5px; float:left;">&rarr;</h3><p style="margin:0px; margin-top:5px; font-size:11pt;"> 
                                <input id="ex-career" type="text" name="ex-career" readonly style="border:none;"></p>
                            </div>
                        </div>
                    </div>
                    <div style="margin-bottom: 30px;">
                            <h5>지원 포지션 선택</h5>
                            <ul>
                                <li><input id="ex-position" type="text" name="ex-position" readonly value="<%= position%>" style="border:none;"></li>
                            </ul>
                        </div>
                </div>
            </div>
            <input type="text" name="aNo" value="<%= aNo%>" style="display:none;">
            <input type="text" name="userNo" value="<%= pm.getPerNo()%>" style="display:none;">
            <input type="text" name="division" value="<%= division%>" style="display:none;">
        </form>

            <hr style="height:0px; border:0.5px solid rgba(128, 128, 128, 0.199); margin-top:10px;">
            <div style="text-align: center;">
                <input type="button" name="backbtn" value="이전">
                <input type="button" name="nextbtn" value="다음">
            </div>
            
    </td>
    <td id="aside">
    </td>
    </tr>
</table>
    <div style="width:1000px; height:100px; margin-left:auto; margin-right:auto;">
    	<%@ include file="../../common/cho_footer.jsp" %>
	</div>
    <script>
            var num = 1;

$(function() {
  //  $(".change-com").hide();
  
$("input[name=nextbtn]").click(function() {
    if(num < 4){
        if(num > 2){
            $("input[name=nextbtn]").val("제출");
            $("#ex-name").val($("#name").val());
            $("#ex-email").val($("#email").val());
            $("#ex-phone").val($("#phone").val());
            $("#ex-career").val($("#career").val());
        }else{
            $("input[name=nextbtn]").val("다음");
        }
            $(".jyh_competition-application" + num).hide();
            $(".jyh_title" + num).hide();
            $(".jyh_progress" + num).hide();
            num++;
            $(".jyh_competition-application" + num).show();
            $(".jyh_title" + num).show();
            $(".jyh_progress" + num).show();
        
    }else if(num == 4){
        alert("제출이 완료되었습니다.");
        $("#joinForm").submit();
    }

    if(num == 1){
        $("input[name=backbtn]").hide();
    }else{
        $("input[name=backbtn]").show();
    }
});
$("input[name=backbtn]").click(function() {
    
    if(num > 1){
        $(".jyh_competition-application" + num).hide();
        $(".jyh_title" + num).hide();
        $(".jyh_progress" + num).hide();
        num--;
        $(".jyh_competition-application" + num).show();
        $(".jyh_title" + num).show();
        $(".jyh_progress" + num).show();
    }
    
    if(num < 4){
        $("input[name=nextbtn]").val("다음");
    }

    if(num == 1){
        $("input[name=backbtn]").hide();
    }else{
        $("input[name=backbtn]").show();
    }
});
});
        </script>
</body>
</html>