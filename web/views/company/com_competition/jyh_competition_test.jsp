<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.util.ArrayList, java.util.HashMap"%>
<%
	HashMap<String,Object> testContent = (HashMap<String, Object>) request.getAttribute("testContent");
	String submitResultYN = (String) request.getAttribute("submitResultYN");
%>
<!DOCTYPE html>
<html lang="ko">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <style>
        a:link { 
            text-decoration: none;
            color: #7A7A7A;
        }
        a:visited { 
            text-decoration: none;
            color: #7A7A7A;
        }
        .article1{
            width:840px;
        }
        .article2{
            width:840px; 
            text-align: center;
        }
        #contents{
            margin-left: auto;
            margin-right: auto;
            width:1400px;
            height:1500px;
        }
        #nav{
            width:530px;
            vertical-align: top;
            color:#4876EF;
        }
        #aside{
            width:530px;
        }
        footer{
            margin-left: auto;
            margin-right: auto;
            width:1400px;
            height:100px;
        }
        table{
            margin:auto;
            border-collapse:collapse;
            border-spacing:0;
        }
        td{
            margin:0;
            padding:0;
        }
        .jyh_testChoice-outline{
            width:840px;
            height:100%;
        }
        .jyh_testChoice1{
            width:97.5%;
            height:100px;
            margin:auto;
            border-top:1px solid rgba(128, 128, 128, 0.308);
            border-bottom:1px solid rgba(128, 128, 128, 0.308);
        }
        h2, h5{
            margin:2px;
        }
        .jyh_test-info{
            margin-left:100px;
        }
        #ul {
            list-style:none;
            margin:0;
            padding:0;
        }

        #ul li {
            float: left;
            margin-right: 10px;
            background:#E9ECF3; 
            border-radius: 5px;
        }
        .downtriangle-img{
            width:25px;
            float:right;
            margin-right:10px;
        }
        input[type=button]{
            height:37px;
            background:#5A84F1;
            color:white;
            font-weight:500;
            font-size: 14pt;
            border-radius: 5px;
            border:0;
            outline:0;
        }
        #firstTable td{
        	padding:5px;
        }
        #firstTable {
        	vertical-align:left;
        }
        .jyh_title ul li{
        	padding: 2.5px;
        }
        .git li{
        	margin-bottom:10px;
        }
        div{
        	word-break:break-all;
        }
    </style>
</head>
<body>
<%@ include file="../../common/menubar.jsp" %>
<% if(loginUser != null){ %>
<table id="firstTable">
    <tr><td colspan="3" style="height:50px;"></td></tr>
    <tr>
        <td id="nav">
        </td>
    <td>
        <div  style="border:0.5px solid #CCCCCC; padding:50px; border-radius: 15px;">
            <div class="article2 jyh_title" style="text-align:left; height:150px;">
                <h1 class="jyh_title1" style="margin:0px;" ><%= testContent.get("company")%> <%= testContent.get("position")%> 과제</h1>
                <h4 style="margin:10px 0px 10px 0px;">단계별 안내를 확인하고, 요구사항을 만족하는 프로젝트를 완성하세요.</h4>
                <ul id="ul">
                    <li><%=testContent.get("perName")%></li>
                    <li><%=testContent.get("email")%></li>
                    <li><%=testContent.get("submissionDate")%>일 마감</li>
                </ul>
            </div>
            <div class="jyh_testChoice-outline">
                <div class="jyh_testChoice jyh_testChoice1" style="margin:10px; margin-bottom:0px;">
                    <div class="jyh_testChoice-info" style="float:left; width:80%; height:100%;">
                        <h3 class="jyh_test-info" style="margin:5.25%; ">STEP.1 프로젝트 요구사항</h3>
                    </div>
                    <div class="jyh_testChoice-info" style="float:left; width:20%; height:25px; margin-top:34.3945px;">
                        <img class="downtriangle-img" src="/itda/image/downtriangle.png"/>
                    </div>
                </div>
                <div class="jyh_testChoice1-contents" style="width:820px; margin:auto; word-break:break-all; display:none;">
                    <%= testContent.get("requirements") %>
                </div>
                <div class="jyh_testChoice jyh_testChoice1" style="margin:10px; margin-bottom:0px;">
                    <div class="jyh_testChoice-info" style="float:left; width:80%; height:100%;">
                        <h3 class="jyh_test-info" style="margin:5.25%; ">STEP.2 프로젝트 제출</h3>
                    </div>
                    <div class="jyh_testChoice-info" style="float:left; width:20%; height:25px; margin-top:34.3945px;">
                        <img class="downtriangle-img" src="/itda/image/downtriangle.png"/>
                    </div>
                </div>
                <div class="jyh_testChoice1-contents git" style="width:820px; margin:auto; display:none;">
                    <h3>제출할 프로젝트의 Github 저장소를 등록하세요</h3>
                    <ul>
                    	<li>저장소는 <span style="background:#FBFBFD;">username/repos</span> 형식으로 입력하시면 됩니다.</li>
                    	<li>작업물이 <span style="background:#FBFBFD;">master branch</span>에 있어야 됩니다.</li>
                    </ul>
                    <h3>Github 저장소 입력</h3>
                    <form id="testSubmit" name="testResult" action="<%= request.getContextPath() %>/insertTestResult.re" method="post">
                    <input id="resultUrl" type="url" name="github" placeholder="ex) github.com/username/repos" style="width:60%; height:25px;">
                    <input type="text" name="userNo" value="<%= loginUser.getPerNo() %>" style="display:none;">
                    <input type="text" name="aNo" value="<%= testContent.get("aNo")%>" style="display:none;">
                    </form>
                </div>
            </div>
            <div style="margin-top:10px;">
                <input id="submit-btn" type="button" name="submit-btn" value="최종 제출 및 테스트 종료" <%if(submitResultYN.equals("Y")) {%> disabled <%}%> style="float:left; margin-right:5px;">
                <h5>
                    STEP 2의 프로젝트 제출이 완료되면 최종 제출 버튼이 활성화 됩니다.<br>
                    최종 제출 후 테스트가 종료되면 다시 시작할 수 없으며, 이후 업데이트한 내용은 반영되지 않습니다.
                </h5>
            </div>
        </div>
    </td>
    <td id="aside">
    </td>
    </tr>
</table>
    

    
    <script>
    $(function(){
            $(".jyh_testChoice1-contents").slideUp();
        
            $(".jyh_testChoice1").click(function(){
            
                $(this).next("div").slideToggle(50, function(){
                });
            });
            if($("#submit-btn").is(":disabled")){
            	$("#submit-btn").css("background", "gray")
            }
            
            $("#submit-btn").click(function(){
            	var result = prompt("' " + $("#resultUrl").val() + " '의 주소를 결과물로 제출하시겠습니까?\n최종 제출을 원하시면 ' 제출 ' 입력 후 확인 버튼을 눌러주세요\n단, 최종 제출 후 수정할 수 없습니다.")
            	
            	if(result == "제출"){
            		alert("제출이 완료되었습니다.")
            		$("#testSubmit").submit();
            	}else{
            		if(result != null){
            			alert("입력 값이 올바르지 않습니다.")
            		}
            	}
            })
        });
    </script>
    <%}else{ %>
    	request.setAttribute("msg", "잘못된 경로로 접근하셨습니다.");
    	request.getRequestDispatcher("../common/errorPage.jsp").forward(request, response);
	<%} %>
</body>
</html>