<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    
    <!-- include codemirror (codemirror.css, codemirror.js, xml.js, formatting.js) -->
<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/codemirror/3.20.0/codemirror.css">
<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/codemirror/3.20.0/theme/monokai.css">
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/codemirror/3.20.0/codemirror.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/codemirror/3.20.0/mode/xml/xml.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/codemirror/2.36.0/formatting.js"></script>
    
    <!-- include libraries(jQuery, bootstrap) -->
<!-- summernote홈페이지에서 받은 summernote를 사용하기 위한 코드를 추가 -->
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script> 
<link href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="https://code.jquery.com/ui/1.12.1/jquery-ui.js" ></script>

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<!-- include summernote css/js -->
<!-- 이 css와 js는 로컬에 있는 것들을 링크시킨 것이다. -->
<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.css" rel="stylesheet">
<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.js"></script>
<link rel="stylesheet" href="/itda/views/summernote/example.css">
  <script type="text/javascript" src="../../summernote/plugin/hello/summernote-ext-hello.js"></script>



    <style>
        .jyh_h4{
            text-align: center;
            font-weight: bold;
            font-family: 'Noto Sans KR', sans-serif;
            color: #7a7a7a;
        }
        .jyh_menubar{
            border-top:solid #d8d8d8;
            border-bottom:solid #d8d8d8;
        }
        .jyh_menu{
            text-align: center;
            margin:0;
            height: 95%;
            width:50%;
            float:left;
        }
        a:link { 
            text-decoration: none;
            color: #7A7A7A;
        }
        a:visited { 
            text-decoration: none;
            color: #7A7A7A;
        }
        .article1{
            width:100%;
        }
        .article2{
            width:100%; 
            text-align: center;
        }
        

        #contents{
            margin-left: auto;
            margin-right: auto;
            width:1400px;
            height:1500px;
        }
        #nav{
            width:278px;
        }
        #aside{
            width:280px;
        }
        footer{
            margin-left: auto;
            margin-right: auto;
            width:1400px;
            height:100px;
        }
        .signup-from{
            width:798px;
            margin:auto;
        }
        #jyh_maintable{
            margin:auto; 
            border-collapse:collapse; 
            border-spacing:0;
            width:1400px;

        }
        #jyh_maintable td{
            padding:0px;
        }


        #jyh_sidebar{
            font-weight:bold;
            color:#263747;
        }
        #jyh_sidebar p{
        	margin-bottom:20px;
        }
        .note-editor .note-editable {
    		line-height: 80%;

		}
		.info-input, #phone-sms{
            width:50%;
            height:35px;
            padding:0px 0 0 5px;
            font-size: 15pt;
            font-weight: bold;
            border: 1px solid #505050;
            border-radius: 5px;
        }
        .info-input::placeholder{
            color:rgb(180, 179, 179);
            font-weight: bold;
            font-size: 13pt;
        }
         input[name=delbtn]{
        	width:20px;
            height:20px;
            padding:0px 0 0 5px;
            font-size: 10pt;
            font-weight: bold;
            border: 1px solid #FBFBFD;
            background:#FBFBFD;
            border-radius: 5px;
            color:gray;
            float:right;
        } 
        .ul1 li {
        	/* border:1px solid gray; */
        	margin-top:15px;
        }
    </style>
</head>
<body>
 <%@ include file="../company_common/sb_companyMenubar.jsp" %> 
 <div style="width:1400px; height:50px;"></div>

         <div style="width:35%; height:100%; float:left;"></div>

<table id="jyh_maintable" style="width:1400px; " align="center">
    <tr>
        <td style="width:10%;"></td>
        <td colspan="2" style="width:67%;">
        	<div style="width:100%; height:30px;">
        	<div style="width:26px; height:26px; float:left; margin-left:30px; text-align:center;"><img src="/itda/image/yh_gongo.png" style="max-width: 25px;"></div>
            <div style="width:200px; height:21px; float:left; margin:5px;"><h4 id="jyh_title" style="margin:0px; font-weight:bold;">채용 공모전 등록</h4></div>
            </div>
            <hr style="width:95%; margin-bottom:30px; margin-top:10px;">
        </td>
        <td style="width:10%;"></td>
    </tr>
    <tr>
        <td style="width:10%;">

        </td>
        <td id="nav" style="vertical-align: top; width:13%; height:400px;">
            <div id="jyh_sidebar" style="margin:auto; border-right:1px solid gray;">
                <div><img id="img1" src="/itda/image/wjs/check.png" style="width:17px; float:left; margin-right:5px;"><p class="step1 step" id="1" style="margin-top:0px; color:#5A84F1;">제목 및 소개</p></div>
                <div><img id="img2" src="/itda/image/wjs/nocheck.png" style="width:17px; float:left; margin-right:5px;"><p class="step2 step" id="2" >참여대상</p></div>
                <div><img id="img3" src="/itda/image/wjs/nocheck.png" style="width:17px; float:left; margin-right:5px;"><p class="step3 step" id="3" >채용포지션</p></div>
                <div><img id="img4" src="/itda/image/wjs/nocheck.png" style="width:17px; float:left; margin-right:5px;"><p class="step4 step" id="4" >접수 및 과제 제출 기간</p></div>
                <div><img id="img5" src="/itda/image/wjs/nocheck.png" style="width:17px; float:left; margin-right:5px;"><p class="step5 step" id="5" >과제 수행 기술 스택</p></div>
                <div><img id="img6" src="/itda/image/wjs/nocheck.png" style="width:17px; float:left; margin-right:5px;"><p class="step6 step" id="6" >전형 상세 안내</p></div>
                <div><img id="img7" src="/itda/image/wjs/nocheck.png" style="width:17px; float:left; margin-right:5px;"><p class="step7 step" id="7" >요구사항</p></div>
                <!-- <div><img id="img8" src="/itda/image/wjs/nocheck.png" style="width:17px; float:left; margin-right:5px;"><p class="step8 step" id="8" >개발 환경 설정</p></div> -->
                <div><img id="img8" src="/itda/image/wjs/nocheck.png" style="width:17px; float:left; margin-right:5px;"><p class="step8 step" id="8" >수상자 혜택 설정</p></div>
                <div><img id="img9" src="/itda/image/wjs/nocheck.png" style="width:17px; float:left; margin-right:5px;"><p class="step9 step" id="9" >공모전 대표 이미지 설정</p></div>
                <div><p class="step9 step" id="9" ><center><input id="miri" type="button" name="next" value="미리보기" style="display:none;"></center></p></div>
            </div>
        </td>
        <td style="vertical-align: top; width:67%;">
            <form id="joinForm" name="form" action="<%= request.getContextPath() %>/jyhInertCompetition.re" method="post" style="width:94%; margin-left:50px;">
                <div class="article1 div1" id="div1" style="width:100%;">
                	<div class="article1" style="width:100%;">
                    	<label style="margin:0px 10px 0px 0px; vertical-align:middle; font-size:15pt; color:#505050;">제목</label>
                    	<input id="title" class="info-input" type="text" name="title" maxlength="100" placeholder="공모전 제목 입력"><br><br>
					</div>
                    <label style="font-size:15pt; color:#505050;">공모전 소개(자유양식)</label>
                    <input type="button" name="goContent" id="test" value="기본 양식 불러오기">
                    <div style="width:100%; margin-bottom:25px;">
                    	<!-- <form method="post"> -->
  							<textarea cols="30" id="summernote" name="summernote" style="overflow-x:hiddenl"></textarea>
						<!-- </form> -->
                    </div>
                </div>
                <div class="article1 div2" id="div2" style="display: none;">
                    <h2>공모전 참여 대상</h2>
                    <input type="text" name="target" value="" placeholder="ex)개발경력3년이상" style="margin-bottom:20px; width:500px;">
                    <input class="add1" type="button" name="add" value="추가">
                    <ul class="ul1">
                        
                    </ul>
                    
                </div>
                <div class="article1 div3" id="div3" style="display: none;">
                    <h2>채용 포지션</h2>
                    <input id="position" type="text" name="position" value="" placeholder="ex)백엔드 개발자" style="margin-bottom:20px;  width:500px;">
                    <!-- <input class="add2" type="button" name="add" value="추가"> -->
                    <ul class="ul1">
                        
                    </ul>
                </div>
                <div class="article1 div4" id="div4" style="display: none;">
                
                			공고일자: <input type="text" name="AnnouncementDate1" placeholder="공고 시작 일자 선택" id="datepicker6"> ~ <input type="text" name="AnnouncementDate2" placeholder="공고 종료일자 선택" id="datepicker7"><br><br>
                        	접수일자: <input type="text" name="receiptDate1" placeholder="접수 시작 일자 선택" id="datepicker1"> ~ <input type="text" name="receiptDate2" placeholder="접수 마감 일자 선택" id="datepicker2"><br><br>
                        	진행일자: <input type="text" name="progressDate1" placeholder="진행 시작 일자 선택" id="datepicker3"> ~ <input type="text" name="progressDate2" placeholder="진행 종료 일자 선택" id="datepicker4"><br><br>
                        	과제 제출 마감 일자: <input type="text" name="submissionDate" placeholder="과제 제출 마감 일자 선택" id="datepicker5">
    
                </div>
                <div class="article1 div5" id="div5" style="display: none;">
                    <h2>과제 수행 기술 스택</h2>
                    <input id="skill" type="text" name="skill" value="" placeholder="ex)back-end, spring, ..." style="margin-bottom:20px;  width:500px;">
                    <input class="add3" type="button" name="add" value="추가">
                    <ul class="ul1">
                        
                    </ul>
                </div>
                <div class="article1 div6" id="div6" style="display: none;">
                    <h2>전형 상세 안내</h2>
                    <input type="text" name="guide" value="" placeholder="ex)이력서, 면접, ..." style="margin-bottom:20px;  width:500px;">
                    <input class="add4" type="button" name="add" value="추가">
                    <ul class="ul1">
                        
                    </ul>
                </div>
                <div class="article1 div7" id="div7" style="display: none;">
                    <h2>프로젝트 요구사항</h2>
                    <div style="width:100%; margin-bottom:25px;">
                    	<!-- <form method="post"> -->
  							<textarea id="summernote1" name="summernote1"></textarea>
						<!-- </form> -->
                    </div>
                </div>
               <!--  <div class="article1 div8" id="div8" style="display: none;">
                	<div class="article1" style="width:100%;">
                    	<label style="margin:0px 10px 0px 0px; vertical-align:middle; font-size:18pt; color:#505050;">개발 환경 제공</label>
					</div>
                    <h4>개발 환경 링크 </h4>
                    <input id ="link" type="text" name="link" value="" placeholder="ex)back-end, spring, ..." style="margin-bottom:20px;">
                    <input class="add5" type="button" name="add" value="추가">
                    <ul class="ul1">
                        
                    </ul>
                    <h4>개발 필요 파일 </h4>
                    <input id ="file" type="text" name="file" value="" placeholder="ex)back-end, spring, ..." style="margin-bottom:20px;">
                    <input class="add6" type="button" name="add" value="추가">
                    <ul class="ul2">
                        
                    </ul>
                </div> -->
                <div class="article1 div8" id="div8" style="display: none;">
                    <h2>수상자 혜택 설정</h2>
                    <input type="text" name="privilege" value="" placeholder="ex)기술면접 패스, 서류전형 패스, 상금, ..." style="margin-bottom:20px;  width:500px;">
                    <input class="add7" type="button" name="add" value="추가">
                    <ul class="ul1">
                    </ul>
                </div>
                <div class="article1 div9" id="div9" style="display: none;">
                    <h2>공모전 대표 이미지 설정</h2>
                    <div style="float:left; text-align:center;">
                    <img style="width:290px; height:200px;" src="/itda/image/recruit1.jpg">
                    <br><input type="radio" name="imgChoice" value="/itda/image/recruit1.jpg" checked></div>
                    <div style="float:left; text-align:center;">
                    <img style="width:290px; height:200px;" src="/itda/image/recruit2.jpg">
                    <br><input type="radio" name="imgChoice" value="/itda/image/recruit2.jpg"></div>
                    <div style="float:left; text-align:center;">
                    <img style="width:290px; height:200px;" src="/itda/image/recruit3.png">
                    <br><input type="radio" name="imgChoice" value=""></div>
                    <input type="text" name="imgSrc" value="/itda/image/recruit1.jpg" style="display:none;">
                </div>
            </form> 
                    
        </td>
    <td id="aside" style="width:10%">
    </td>
    </tr>
    <tr>
        <td colspan="4">
            <div style="width:900px; height:50px; margin-left:370px; padding-top:10px; padding-bottom:10px;  float:left; border-top:1px solid gray;">
                <input id="next" type="button" name="next" value="다음" style="float:right;">
                
                <input id="back" type="button" name="back" value="이전" style="float:left; display: none;">
            </div>
        </td>
    </tr>
</table>
    
    <center><%@ include file="../../common/cho_footer.jsp" %></center>
    
    <script>
    $(function(){
    	$("#test").click(function(){
    		$('#summernote').summernote("code",
    		"<div id='intro' class='section' style='margin-top: 2rem;'>"
    		+ "<h1 style='margin-top: 0px; margin-bottom: 0.25rem; line-height: 1.4; font-size: 32px; font-family: Inter, NotoSansKR, &quot;Helvetica Neue&quot;, Helvetica, Arial, &quot;맑은 고딕&quot;, &quot;malgun gothic&quot;, 돋움, Dotum, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Noto Color Emoji&quot;; letter-spacing: -2px; -webkit-font-smoothing: antialiased;'>"
    		+ "어떤 채용 플랫폼보다, 뛰어난 전문성을 가진 채용 플랫폼.<br>개발자와 IT기업을 위한 IT전문 채용 플랫폼 IT'DA입니다."
    		+ "</h1>"
    		+ "<p class='tag-group' style='margin-top: 0.75rem; margin-bottom: 0px; line-height: 1.8; letter-spacing: -0.1px; overflow-wrap: break-word;'>"
    		+ "<span style='display: inline-block; margin-right: 0.5rem; font-size: 0.9rem;'>#코딩테스트없음</span>"
    		+ "<span style='display: inline-block; margin-right: 0.5rem; font-size: 0.9rem;'>#IT전문 채용 플랫폼</span>"
    		+ "<span style='display: inline-block; margin-right: 0.5rem; font-size: 0.9rem;'>#유연 근무제</span>"
    		+ "<span style='display: inline-block; margin-right: 0.5rem; font-size: 0.9rem;'>#서류면접 없음</span>"
    		+ "</p>"
    		+ "<p class='info-box' style='margin-top: 0.75rem; margin-bottom: 0px; line-height: 1.8; letter-spacing: -0.1px; overflow-wrap: break-word; padding: 10px; border: 1px solid rgb(203, 212, 253); border-radius: 5px; font-size: 0.9rem; background-color: rgb(238, 241, 255);'>"
    		+ "본 공모전은 예시용 공모전 내용입니다.&nbsp;"
    		+ "<span class='emp-text' style='color: rgb(119, 117, 255);'>"
    		+ "기술면접 통과 </span>의 혜택이 있는 프로그램입니다. </p> </div>"
    		+ "<div id='about-company' class='section' style='margin-top: 2rem;'>"
    		+ "<p style='margin-bottom: 0px; line-height: 1.8; letter-spacing: -0.1px; overflow-wrap: break-word;'>"
    		+ "IT'DA는 개발자가 원하는 기업, 기업이 원하는 개발자를 발견해 나갈 수 있는 IT전문 채용 플랫폼입니다.<br>전문성 없는 채용 플랫픔을 벗어나 이용자에게 꼭 맞는 서비스를 제공하고 있습니다."
    		+ "</p>"
    		+ "<h2 style='margin: 2.5rem 0px 0.25rem; line-height: 1.4; font-size: 26px; font-family: Inter, NotoSansKR, &quot;Helvetica Neue&quot;, Helvetica, Arial, &quot;맑은 고딕&quot;, &quot;malgun gothic&quot;, 돋움, Dotum, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Noto Color Emoji&quot;; letter-spacing: -2px; -webkit-font-smoothing: antialiased;'>"
    		+ "1등을 향해 나아가는 팀 ' IT'DA ' 에서<br>개발자 동료를 찾아요."
    		+ "</h2>"
    		+ "<p style='margin-top: 0.75rem; margin-bottom: 0px; line-height: 1.8; letter-spacing: -0.1px; overflow-wrap: break-word;'>"
    		+ "코딩테스트 점수나 화려한 이력서보다는, 공모전 지원자들이 제출한 코드의 스타일을 살펴보고 싶다는 IT'DA에 지원해보세요. 2020년 3월 런칭 후 쭉 성장해 본격적인 서비스 확장에 박차를 가하고 있습니다.</p><p style='margin-top: 0.75rem; margin-bottom: 0px; line-height: 1.8; letter-spacing: -0.1px; overflow-wrap: break-word;'>취업과 인재 발견이 어려운 현재, 가장 주목받고 있는 채용 서비스 중 하나인 IT'DA를 함께 성장시켜나갈 백엔드 개발자와 모바일 앱 개발자를 모실게요."
    		+ "</p></div><div id='about-asmnt' class='section' style='margin-top: 2rem;'></div>");
    		//$('#summernote').summernote("code", "<img src='/itda/image/application1.PNG'>");
    		//$('#summernote').summernote('insertText', 'hello world');
    		/* var node = document.createElement('div');
    		// @param {Node} node
    		$('#summernote').summernote('insertNode', node); */
    	});
    });
    
    
    
    $(document).ready(function() {
    	
		$('#summernote,#summernote1').summernote({
		 	height: 300,                 // 에디터 높이
		 	width: 880,
		  	minHeight: null,             // 최소 높이
		  	maxHeight: null,             // 최대 높이
		  	focus: true,                  // 에디터 로딩후 포커스를 맞출지 여부
		  	spellCheck: false,
		  	lang: "ko-KR",					// 한글 설정
		  	placeholder: '최대 2048자까지 쓸 수 있습니다',	//placeholder 설정
		  	codemirror: { // codemirror options
		  		mode: 'text/html',
		        htmlMode: true,
		        lineNumbers: true,
		        theme: 'monokai'
		  	},
		  	/* toolbar: [
		  	  ['style', ['style']],
		  	  ['font', ['bold', 'underline', 'clear']],
		  	  ['fontname', ['fontname']],
		  	  ['color', ['color']],
		  	  ['para', ['ul', 'ol', 'paragraph']],
		  	  ['table', ['table']],
		  	  ['insert', ['link', 'picture', 'video']],
		  	  ['view', ['fullscreen', 'codeview', 'help']],
		  	  ['insert', ['hello']]
		  	], */
		  	callbacks: {
		        onImageUpload: function(files, editor, welEditable) {
		          for (var i = files.length - 1; i >= 0; i--) {
		            sendFile(files[i], this);
		          }
		        }
		    }
		});
	});
    
    function sendFile(file, el) {
        var form_data = new FormData();
        form_data.append('file', file);
        $.ajax({
          data: form_data,
          type: "POST",
          url: '/itda/jyhInsertImg.re',
          cache: false,
          contentType: false,
          enctype: 'multipart/form-data',
          processData: false,
          success: function(data) {
        	  console.log(data);
            $(el).summernote('editor.insertImage', data);
            
          }
        });
      }

        var num = 1;
        var stepNum = 2;
		var stepNum1 = stepNum;
            $(function() {
              //  $(".change-com").hide();
              $(".step").click(function(){
            	  var divName = $(this).attr('id');
            	  
            	  
            	  
            	  for(var i = 1; i <= 9; i++){
            		  if(i != divName){
            			  $("#div"+i).hide();
            			  $("#"+i).css("color", "black");
            			  $("#img"+i).attr("src", "/itda/image/wjs/nocheck.png");
            		  }else{
            			  $("#div"+i).show();
            			  $("#"+i).css("color", "#5A84F1");
            			  $("#img"+i).attr("src", "/itda/image/wjs/check.png");
            			  stepNum = divName + 1;
                    	  num = divName;
            		  }
            	  }
            	  
            	  if(num == 1){
                      $("#back").hide();
                  }else{
                      $("#back").show();
                  }
            	  
            	  if(divName == 9){
            		  $("#miri").show();
            	  }else{
            		  $("#miri").hide();
            	  }
              })
              
			$("#next").click(function() {
                	//$("#" + num).css("color","#5A84F1");
                	/* for(var i = 1; i <= 9; i++){
                		if(i != num){
              			  $("#"+i).css("color", "black");
              		  }else{
              			  $("#"+i).css("color", "#5A84F1");
              		  }
                	} */
                if(num < 9){
                    $(".div" + num).hide();
                    $("#"+num).css("color", "black");
                    $("#img"+num).attr("src", "/itda/image/wjs/nocheck.png");
                    num++;
				    $(".div" + num).show();
				    $("#"+num).css("color", "#5A84F1");
				    $("#img"+num).attr("src", "/itda/image/wjs/check.png");
                }else if(num == 9){
                	form.target = "_self"; 
             		$("#joinForm").attr("action","<%=request.getContextPath() %>/jyhInertCompetition.re" )
                    $("#joinForm").submit();
                    $("#"+num).attr("src", "/itda/image/wjs/check.png");
                }
                if(num == 1){
                    $("#back").hide();
                }else{
                    $("#back").show();
                }
                
                if(num == 9){
                	$("#miri").show();
				}else{
					$("#miri").hide();
				}
			});
			$("#back").click(function() {
                
				/* for(var i = 1; i <= 9; i++){
            		if(i != num){
          			  $("#"+(i+1)).css("color", "black");
          		  }else{
          			  $("#"+(i-1)).css("color", "#5A84F1");
          		  }
            	} */
            	
				
            	
                if(num > 1){
				    $(".div" + num).hide();
				    $("#"+num).css("color", "black");
				    $("#img"+num).attr("src", "/itda/image/wjs/nocheck.png");
                    num--;
				    $(".div" + num).show();
				    $("#"+num).css("color", "#5A84F1");
				    $("#img"+num).attr("src", "/itda/image/wjs/check.png");
                }
                if(num == 1){
                    $("#back").hide();
                }else{
                    $("#back").show();
                }
                
                if(num == 7){
                	$("#miri").show();
				}else{
					$("#miri").hide();
				}
            });
        });

        $(document).on("click",".add1",function(){
            $(this).parent().children('ul').append("<li>" + "<span style='margin-bottom:10px; width:20%;'>" 
            			+ $(this).parent().children('input[type=text]').val() 
            			+ "</span> <input type='text' name='target-li' readonly value='" 
            			+ $(this).parent().children('input[type=text]').val() 
            			+"' style='display:none;'> <input class='xbtn' type='button' name='delbtn' value='x'>" + "</li>");  
          //  $(".ul1").append("<li>" + "<label style='margin-bottom:10px; width:20%;'>" + $("#target").val() + "</label> <input type='text' name='target-li' readonly value='" + $("#target").val() +"' style='display:none;'> <input class='xbtn' type='button' name='delbtn' value='x'>" + "</li>");  
          //  $(".target").val("");
          //  $(".target").focus();
            $(this).parent().children('input[type=text]').val("");
            $(this).parent().children('input[type=text]').focus();
        });
        
        $(document).on("click",".add2",function(){
        	 $(this).parent().children('ul').append("<li>" + "<span style='margin-bottom:10px;'>" 
						+ $(this).parent().children('input[type=text]').val() 
						+ "</span> <input type='text' name='position-li' readonly value='" 
						+ $(this).parent().children('input[type=text]').val() 
						+"' style='display:none;'> <input class='xbtn' type='button' name='delbtn' value='x'>" + "</li>");   
            $(this).parent().children('input[type=text]').val("");
            $(this).parent().children('input[type=text]').focus();
        });
        
        $(document).on("click",".add3",function(){
       	 $(this).parent().children('ul').append("<li>" + "<span style='margin-bottom:10px;'>" 
						+ $(this).parent().children('input[type=text]').val() 
						+ "</span> <input type='text' name='skill-li' readonly value='" 
						+ $(this).parent().children('input[type=text]').val() 
						+"' style='display:none;'> <input class='xbtn' type='button' name='delbtn' value='x'>" + "</li>");   
           $(this).parent().children('input[type=text]').val("");
           $(this).parent().children('input[type=text]').focus();
       });
        
        $(document).on("click",".add4",function(){
          	 $(this).parent().children('ul').append("<li>" + "<span style='margin-bottom:10px;'>" 
   						+ $(this).parent().children('input[type=text]').val() 
   						+ "</span> <input type='text' name='guide-li' readonly value='" 
   						+ $(this).parent().children('input[type=text]').val() 
   						+"' style='display:none;'> <input class='xbtn' type='button' name='delbtn' value='x'>" + "</li>");   
              $(this).parent().children('input[type=text]').val("");
              $(this).parent().children('input[type=text]').focus();
          });
        $(document).on("click",".add5",function(){
         	 $(this).parent().children('.ul1').append("<li>" + "<span style='margin-bottom:10px;'>" 
  						+ $(this).parent().children('#link').val() 
  						+ "</span> <input type='text' name='link-li' readonly value='" 
  						+ $(this).parent().children('#link').val() 
  						+"' style='display:none;'> <input class='xbtn' type='button' name='delbtn' value='x'>" + "</li>");   
             $(this).parent().children('#link').val("");
             $(this).parent().children('#link').focus();
         });
        $(document).on("click",".add6",function(){
         	 $(this).parent().children('.ul2').append("<li>" + "<span style='margin-bottom:10px;'>" 
  						+ $(this).parent().children('#file').val()
  						+ "</span> <input type='text' name='file-li' readonly value='" 
  						+ $(this).parent().children('#file').val() 
  						+"' style='display:none;'> <input class='xbtn' type='button' name='delbtn' value='x'>" + "</li>");   
             $(this).parent().children('#file').val("");
             $(this).parent().children('#file').focus();
         });
        $(document).on("click",".add7",function(){
        	 $(this).parent().children('ul').append("<li>" + "<span style='margin-bottom:10px;'>" 
 						+ $(this).parent().children('input[type=text]').val()
 						+ "</span> <input type='text' name='privilege-li' readonly value='" 
 						+ $(this).parent().children('input[type=text]').val() 
 						+"' style='display:none;'> <input class='xbtn' type='button' name='delbtn' value='x'>" + "</li>");   
            $(this).parent().children('input[type=text]').val("");
            $(this).parent().children('input[type=text]').focus();
        });
        
        $(function(){
            $(".ul1").sortable();
        });
        
        $(document).on("click",".xbtn",function(){
        	var delList = $(this).parent().remove();
        	console.log(delList);  
        });
        </script>
        
        
    <script>
        $(function() {
            //모든 datepicker에 대한 공통 옵션 설정
            $.datepicker.setDefaults({
                dateFormat: 'yy-mm-dd' //Input Display Format 변경
                ,showOtherMonths: true //빈 공간에 현재월의 앞뒤월의 날짜를 표시
                ,showMonthAfterYear:true //년도 먼저 나오고, 뒤에 월 표시
                ,changeYear: true //콤보박스에서 년 선택 가능
                ,changeMonth: true //콤보박스에서 월 선택 가능                
            //    ,showOn: "both" //button:버튼을 표시하고,버튼을 눌러야만 달력 표시 ^ both:버튼을 표시하고,버튼을 누르거나 input을 클릭하면 달력 표시  
            //    ,buttonImage: "/itda/image/calendar.png" //버튼 이미지 경로
            //    ,buttonImageOnly: true //기본 버튼의 회색 부분을 없애고, 이미지만 보이게 함
            //    ,buttonText: "선택" //버튼에 마우스 갖다 댔을 때 표시되는 텍스트                
                ,yearSuffix: "년" //달력의 년도 부분 뒤에 붙는 텍스트
                ,monthNamesShort: ['1','2','3','4','5','6','7','8','9','10','11','12'] //달력의 월 부분 텍스트
                ,monthNames: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'] //달력의 월 부분 Tooltip 텍스트
                ,dayNamesMin: ['일','월','화','수','목','금','토'] //달력의 요일 부분 텍스트
                ,dayNames: ['일요일','월요일','화요일','수요일','목요일','금요일','토요일'] //달력의 요일 부분 Tooltip 텍스트
                ,minDate: "0M" //최소 선택일자(-1D:하루전, -1M:한달전, -1Y:일년전)
                ,maxDate: "+10Y" //최대 선택일자(+1D:하루후, -1M:한달후, -1Y:일년후)                    
            });
 
            //input을 datepicker로 선언
            $("#datepicker1").datepicker();                    
            $("#datepicker2").datepicker();
            $("#datepicker3").datepicker();
            $("#datepicker4").datepicker();
            $("#datepicker5").datepicker();
            $("#datepicker6").datepicker();
            $("#datepicker7").datepicker();
            
        //    $('img.ui-datepicker-trigger').css({'cursor':'pointer', 'width':'25px', 'height':'28px', 'color':'gray', 'margin':'0px'});  //아이콘(icon) 위치
            //From의 초기값을 오늘 날짜로 설정
         //   $('#datepicker').datepicker('setDate', 'today'); //(-1D:하루전, -1M:한달전, -1Y:일년전), (+1D:하루후, -1M:한달후, -1Y:일년후)
            //To의 초기값을 내일로 설정
         //   $('#datepicker2').datepicker('setDate', '+1D'); //(-1D:하루전, -1M:한달전, -1Y:일년전), (+1D:하루후, -1M:한달후, -1Y:일년후)
        });
    </script>
    
    <script>
		$(function() {
			var availableTags = [
				"DevOn","Front-End","Gauce","Java","Jenkins","Maven","Miplatform","Nexacro","Pro*C","Proframe",
				"Sencha","Spring","Thymeleaf","TrustForm","Tuxedo","Xplatform","Android","Hybrid","iOS(Object-C)",
				"iOS(Swift)","IoT","WebView","ASP","Codeigniter","Laravel","PHP","ZendFramework","Symfony","WordPress",
				"C","C++","ASP.net","C#","MFC","OpenGL","DevExpress","VBA","javaScript","node.js","AngularJs","React.js",
				"Vue.js","jQuery","Embedded","Firmware","LabVIEW","MachineVision","Server","UNIX","MetaLab","Qt",
				"Aduino","CUBRID","MariaDB","MongoDB","MSSQL","MySQL","Oracle","Postgresql","Tibero","ActionScript",
				"CSS","Flash","Git","HTML5","웹접근성","웹표준","MobileApp",".NET","DB","Publisher",
				];
			$( "#skill" ).autocomplete({
				source: availableTags
			});
			
			$("#miri").click(function(){
				
				if ($("#title").val() == "") {
                    alert("공모전 제목을 입력해주세요");
                }
                else if($("#summernote").val() == ""){
                    alert("공모전 내용을 입력해주세요");
                }
                else if($("#position").val() == ""){
                    alert("채용 포지션을 입력해주세요");
                }
                else if($("#summernote1").val() == ""){
                    alert("테스트 요구사항 내용을 입력해주세요");
                }
                else if($("#datepicker1").val() == ""){
                    alert("날짜를 입력해주세요");
                }
                else if($("#datepicker2").val() == ""){
                    alert("날짜를 입력해주세요");
                }
                else if($("#datepicker3").val() == ""){
                    alert("날짜를 입력해주세요");
                }
                else if($("#datepicker4").val() == ""){
                    alert("날짜를 입력해주세요");
                }
                else if($("#datepicker5").val() == ""){
                    alert("날짜를 입력해주세요");
                }
                else if($("#datepicker6").val() == ""){
                    alert("날짜를 입력해주세요");
                }
                else if($("#datepicker7").val() == ""){
                    alert("날짜를 입력해주세요");
                }
                else{
        			window.open("", "_blank|_self"/* , 'width=1280, height=650, *//*  left=0, top=0, location, menubar, scrollbars, resizable' */); 
        			form.action = "<%=request.getContextPath()%>/jyhMiri.re"; 
        			form.target = "_blank|_self"; 
        			form.submit();
                }
			})
			
			$("input[name=imgChoice]").click(function(){
				//$(input[name=imgSrc]).val($(this).attr("src"));
				var src = $(this).parent().children('img').attr("src");
				console.log(src);
				$("input[name=imgSrc]").val(src);
			})
		});
	</script>
</body>
</html>