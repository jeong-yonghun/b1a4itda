<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.util.ArrayList, java.util.HashMap"%>
<%
	HashMap<String,Object> content = (HashMap<String, Object>) request.getAttribute("content");
	ArrayList<String> skillArr = (ArrayList<String>) request.getAttribute("skillArr");
	String position = (String) request.getAttribute("position");
	int applyYn = (int) request.getAttribute("applyYn");
%>
<!DOCTYPE html>
<html lang="ko">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <style>
        .jyh_h4{
            text-align: center;
            font-weight: bold;
            font-family: 'Noto Sans KR', sans-serif;
            color: #7a7a7a;
        }
        .jyh_menubar{
            border-top:solid #d8d8d8;
            border-bottom:solid #d8d8d8;
        }
        .jyh_menu{
            text-align: center;
            margin:0;
            height: 95%;
            width:50%;
            float:left;
        }
        a:link { 
            text-decoration: none;
            color: #7A7A7A;
        }
        a:visited { 
            text-decoration: none;
            color: #7A7A7A;
        }
        .article1{
            width:840px; 
        }
        .article2{
            width:840px; 
            text-align: center;
        }
        
        #nav{
            width:278px;
        }
        #aside{
            width:280px;
        }
        footer{
            margin-left: auto;
            margin-right: auto;
            width:1400px;
            height:100px;
        }
        #jyh_maintable{
            margin:auto; 
            border-collapse:collapse; 
            border-spacing:0;
            width:1400px;

        }
        #jyh_maintable td{
            padding:0px;
        }

        #jyh_sidebar{
            font-weight:bold;
            color:#263747;
        }
        input[name=jyh_apply-btn], input[name=jyh_test-btn]{
            width:200px;
            height:50px;
            font-weight:500;
            font-size: 15pt;
            border-radius: 5px;
            border:0;
            outline:0;
            background:#0078FF;
            color:white;
        }
    </style>
</head>
<body>

 <%@ include file="../../common/menubar.jsp" %>
  <%-- <%@ include file="../company_common/jyh_competitionMenubar.jsp" %> --%>
<table id="jyh_maintable" style="border:1px solid gray;" align="center">
    <tr>
        <td style="width:1400px;" colspan="3">
            	<img style="width:1400px;" src="/itda/competition_uploadFiles/2020030202255636988.png">
        </td>
    </tr>
    <tr>
        <td id="nav">
        </td>
        <td style="vertical-align: top;">
                <div class="jyh_competition-title-outline" style="position:relative; width:840px; height:100px; background:rgb(56, 33, 185);">
                    <div class="jyh_competition-title" style="width:70%; height:100%; float:left; background:rgb(130, 129, 133);"><h1><%=content.get("title")%></h1></div>
                    <div class="jyh_competition-" style="width:30%; float:left; background:rgb(170, 56, 164);">
    						<% if(applyYn > 0){%>
                        		<input id="testBtn" type="button" name="jyh_test-btn" value="테스트입장" style="position:absolute; bottom:0px;">
    						<%}else{%>
                        		<input id="applyBtn" type="button" name="jyh_apply-btn" value="지원하기" style="position:absolute; bottom:0px;">
    					<%}%>
                    </div>
                </div>
                <div class="jyh_competition-schedule-outline" style="width:840px; height:100px; background:rgb(115, 209, 78);">
                    <div class="jyh_competition-schedule1" style="width:33.333%; height:100%; float:left; background:rgb(110, 90, 172);">남은 날짜</div>
                    <div class="jyh_competition-schedule2" style="width:33.333%; height:100%; float:left; background:rgb(252, 241, 251);">
                    <p>접수 : <%=content.get("receiptStart")%> ~ <%=content.get("receiptEnd")%></p>
                    <p>진행 : <%=content.get("progressStart")%> ~ <%=content.get("progressEnd")%></p>
                    </div>
                    <div class="jyh_competition-schedule2" style="width:33.333%; height:100%; float:left; background:rgb(46, 150, 190);"><%=position %></div>
                </div>
                <div class="article1 div3">
                       	<div id='notice-zone'>
<p class='info-box-style-important' style='margin-top: 0.75rem; margin-bottom: 0px; line-height: 1.8; letter-spacing: -0.1px; overflow-wrap: break-word; padding: 10px; border: 1px solid rgb(255, 207, 162); border-radius: 5px; font-size: 0.9rem; background-color: rgb(255, 239, 224);'>
<span class='emp-text' style='color: black;'></span>
&nbsp;과제 제출 기한을 꼭 확인해주시고, 제출 마감일에는 트래픽 이슈가 발생할 수 있으니 가급적 빠른 제출을 권장합니다.</p>
</div>
                       	<%-- <%=content.get("contents")%> --%>
                </div>
                <hr style='overflow: visible; margin: 3rem 0px; border-top-width: 0.0625rem; border-top-color: rgb(233, 236, 243);'>
                <div class="article1 div4">
                
                	<%-- <h3>공모전 참여 대상</h3>
                	<ul>
                		<li><%=content.get("target")%></li>
                	</ul><br> --%>
                	<h3 style='margin: 0px; line-height: 1.6; font-size: 20px; font-family: Inter, NotoSansKR, &quot;Helvetica Neue&quot;, Helvetica, Arial, &quot;맑은 고딕&quot;, &quot;malgun gothic&quot;, 돋움, Dotum, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Noto Color Emoji&quot;; letter-spacing: -2px; -webkit-font-smoothing: antialiased;'>
					챌린지 참여 대상
					</h3>
					<ul style='margin: 0.75rem 0px 0px;'>
						<li style='line-height: 1.6; letter-spacing: -0.009em;'>
							개발 경력 3년 이상 백엔드 개발자 지원 가능(Spring)
						</li>
						<li style='line-height: 1.6; letter-spacing: -0.009em; margin-top: 0.5rem;'>
							개발 경력 5년 이상, 앱 개발 경력 3년 이상 모바일 앱 개발자 지원 가능(iOS/Swift, Android/Java, Kotlin 중 택1)
						</li>
						<li style='line-height: 1.6; letter-spacing: -0.009em; margin-top: 0.5rem;'>
							1차 서류전형은 없습니다. 서류 대신 개발 과제가 주어집니다. 자격 사항에 미달하는 분은 지원할 수 없습니다.
						</li>
						<li style='line-height: 1.6; letter-spacing: -0.009em; margin-top: 0.5rem;'>
							제출하신 과제의 정상 작동 여부와, 코드의 퀄리티를 기반으로 면접 진행 여부를 판단합니다.
						</li>
					</ul>
                	
                	
                	<%-- <h3>채용 포지션</h3>
                	<ul>
                		<li id="position"><%=position%></li>
                	</ul><br> --%>
                	
                	<h3 style='margin: 2.5rem 0px 0px; line-height: 1.6; font-size: 20px; font-family: Inter, NotoSansKR, &quot;Helvetica Neue&quot;, Helvetica, Arial, &quot;맑은 고딕&quot;, &quot;malgun gothic&quot;, 돋움, Dotum, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Noto Color Emoji&quot;; letter-spacing: -2px; -webkit-font-smoothing: antialiased;'>
					채용 포지션
					</h3>
					<p style='margin-top: 0.75rem; margin-bottom: 0px; line-height: 1.8; letter-spacing: -0.1px; overflow-wrap: break-word;'>
					<span class='emp-text' style='color: rgb(119, 117, 255);'>
						드림어스컴퍼니
					</span>는 아래의 포지션에 적합한 인재를 찾고 있습니다.</p>
					<ul style='margin: 1rem 0px 0px;'>
						<li style='line-height: 1.6; letter-spacing: -0.009em;'>
							<span class='list-name'>백엔드 개발자</span>
						</li>
						<li style='line-height: 1.6; letter-spacing: -0.009em; margin-top: 0.5rem;'>
						<span class='list-name'>iOS 개발자</span>
						</li>
						<li style='line-height: 1.6; letter-spacing: -0.009em; margin-top: 0.5rem;'>
							<span class='list-name'>Android 개발자</span>
						</li>
					</ul>
                	
                	
                	<%-- <form id="application" action="views/company/com_competition/jyh_competition_application.jsp" method="post">
                		<input type="text" name="position" value="<%=position%>" style="display:none;">
                		<input type="text" name="aNo" value="<%=content.get("aNo")%>" style="display:none;">
                		<input type="text" name="userNo" value="<%= loginUser.getPerNo()%>" style="display:none;">
                	</form> --%>
                	
                	<%-- <h3>과제 제출 기간</h3>
                	<ul>
                		<li>기간 : <%=content.get("progressStart")%> ~ <%=content.get("submissionDate")%></li>
                		<li>과제 개발이 완료되면 제출 기간 내에 자유롭게 제출하시면 됩니다.</li>
                		<li>단, 마감일에는 제출이 몰려 트래픽 이슈가 발생할 수 있으니 가급적 빠르게 진행하시는 것을 권장합니다.</li>
                	</ul><br>
                	<h3>과제 수행 기술 스택</h3>
                	<ul>
                	<% for(int i = 0; i < skillArr.size(); i++){ %>
                		<li>
                			<div style="border:1px solid gray; background:#C4C4C4; border-radius:5px; margin:5px; padding:2px; float:left;"><%= skillArr.get(i) %></div>
                		</li>
                	<%} %>
                	</ul><br> --%>
                	
                	<h3 style='margin: 2.5rem 0px 0px; line-height: 1.6; font-size: 20px; font-family: Inter, NotoSansKR, &quot;Helvetica Neue&quot;, Helvetica, Arial, &quot;맑은 고딕&quot;, &quot;malgun gothic&quot;, 돋움, Dotum, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Noto Color Emoji&quot;; letter-spacing: -2px; -webkit-font-smoothing: antialiased;'>
					과제 제출 기간
					</h3>
					<ul style='margin: 0.75rem 0px 0px;'>
						<li style='line-height: 1.6; letter-spacing: -0.009em;'>
							접수 시작: <%=content.get("progressStart")%> ~ <%=content.get("submissionDate")%>
						</li>
						<li style='line-height: 1.6; letter-spacing: -0.009em; margin-top: 0.5rem;'>
							과제 개발이 완료되면 기간 내에 아무 때나 제출하면 됩니다.
						</li>
						<li style='line-height: 1.6; letter-spacing: -0.009em; margin-top: 0.5rem;'>
							단, 마감일에 제출이 몰릴 경우 트래픽 이슈가 있을 수 있으니 가급적 빠르게 진행하시는 것을 권장합니다.
						</li>
					</ul>
					
					<div id='about-asmnt-stacks' class='section' style='margin-top: 2rem;'>
						<h3 style='margin: 0px; line-height: 1.6; font-size: 20px; font-family: Inter, NotoSansKR, &quot;Helvetica Neue&quot;, Helvetica, Arial, &quot;맑은 고딕&quot;, &quot;malgun gothic&quot;, 돋움, Dotum, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Noto Color Emoji&quot;; letter-spacing: -2px; -webkit-font-smoothing: antialiased;'>
							과제 수행 기술 스택
						</h3>
						<ul style='margin: 0.75rem 0px 0px;'>
							<% for(int i = 0; i < skillArr.size(); i++){ %>
                				<li style='line-height: 1.6; letter-spacing: -0.009em;'>
                					<span class='list-name'><%= skillArr.get(i) %></span>
                				</li>
                			<%} %>
						</ul>
							<p style='margin-top: 0.75rem; margin-bottom: 0px; line-height: 1.8; letter-spacing: -0.1px; overflow-wrap: break-word;'>
								안내된 기술 스택 외의 다른 기술로 개발된 과제는 접수가 불가합니다.
							</p>
					</div>
                </div>
                <hr style='overflow: visible; margin: 3rem 0px; border-top-width: 0.0625rem; border-top-color: rgb(233, 236, 243);'>
                <div class="article1 div5"">
                	<%-- <h2>전형 상세 안내</h2>
                	<p>1차로 IT'DA전형과 2차 <%=content.get("companyName")%> 전형으로 나누어 진행됩니다.</p>
                	<p>1차 과제 전형을 통과해야만 <%=content.get("companyName")%> 2차 전형을 진행할 수 있게 됩니다.</p>
                	<p>과제 평가 기준과 관련해서는 아래의 '1차 : IT'DA전형 부분을 읽어주세요</p>
                	
                	<table style="width:100%;" border="1">
                		<tr style="text-align:left; ">
                			<th style="width:50%; padding-left:20px;">1차 전형 : 과제 퀄리티로 개발 능력 검증</th>
                			<th style="width:50%; padding-left:20px;">2차 전형 : 1차 통과 후 회사와의 핏(Fit) 확인</th>
                		</tr>
                		<tr  style="vertical-align:top;">
                			<td >
                				<ol>
                					<li>간단 정보 입력(<%=content.get("progressStart")%> 부터)</li>
                					<li>과제 수행 및 제출(<%=content.get("progressStart")%> ~ <%=content.get("submissionDate")%> 까지)</li>
                				</ol>
                				
                			</td>
                			<td>
                				<ol>
                					<li><%=content.get("admissionGuide")%></li>
                				</ol>
                			</td>
                		</tr>
                	</table>
                	
                	<h3>1차 IT'DA 전형</h3>
                	<ol>
                		<li><label style="font-weight:bold;">간단한 정보 입력 : </label> 전형을 위해 필요한 아주 기본적인 정보(연락처 등)을 입력합니다. 블라인드 채용이므로 상세한 개인정보나 경력사항은 받지 않습니다.</li><br>
                		<li><label style="font-weight:bold;">과제 진행 및 제출 : </label> 간단한 정보 입력 후 준비된 과제를 열람할 수 있게 됩니다. 요구사항을 꼼꼼히 확인하여 구현한 뒤 기한 내에 제출을 완료합니다.</li><br>
                		<li><label style="font-weight:bold;">과제 검토 및 통과 여부 결정 </label><br>
                			<p>IT'DA는 크게 아래의 기준을 가지고 제출한 기능의 소스코드를 검토하게 됩니다.<br>
                			상세한 평가 기준을 모두 공개해드릴 수 없음을 양해 부탁드립니다.</p>
                			<ul>
                				<li>결과물이 정상적으로 작동하는가? 기능 요구사항이 모두 구현되어 있는가?</li>
                				<li>사용한 언어, 라이브러리, 프레임워크가 공식적으로 권장하는 개발 방식을 잘 따르고 있는가?</li>
                				<li>구조적 확장성과 가독성을 고려하여 코드를 작성하였는가?</li>
                				<li>테스트가 적절히 구현되었는가?</li>
                			</ul>
                			<p>즉, <label style="font-weight:bold;">겉으로 보기에 기능이 돌아가는가?를 넘어서 소스코드 레벨의 검토</label>가 이루어집니다. 해당 직무과 관련한 기술적 이해도가 기본적으로 중요하지만,
                			다른 개발자들과의 협업을 위해 필요한 역량이 드러나는지도 중요합니다. 이 검토 과정을 거친 이후에 과제 통과 여부가 결정됩니다.</p>
                		</li>
                		<li><label style="font-weight:bold;">프로필 입력 요청 :</label> 2차 전형으로 넘어가기 위해 필요한 정보를 전달받습니다. 과제 통과자에 한해서 안내됩니다.</li><br>
                	</ol>
                	<h3>2차 <%=content.get("companyName")%> 전형</h3>
                	<p>과제 통과자에 한해서 <%=content.get("companyName")%> 팀과 직접 2차 전형을 진행할 수 있게 됩니다. 이력서 제출과 함께 곧바로 면접 일정을 잡는 것부터 시작합니다.
                	지원자에 따라 절차와 최종 합불 여부 결정 여부는 달라질 수 있으니 참고해주세요.</p> --%>
                	
                	
                	
                	<div id='about-process-1st' class='section' style='margin-top: 2rem;'>
<h2 style='margin: 0px 0px 0.25rem; line-height: 1.4; font-size: 26px; font-family: Inter, NotoSansKR, &quot;Helvetica Neue&quot;, Helvetica, Arial, &quot;맑은 고딕&quot;, &quot;malgun gothic&quot;, 돋움, Dotum, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Noto Color Emoji&quot;; letter-spacing: -2px; -webkit-font-smoothing: antialiased;'>
전형 상세 안내
</h2>
<p style='margin-top: 0.75rem; margin-bottom: 0px; line-height: 1.8; letter-spacing: -0.1px; overflow-wrap: break-word;'>
1차로 프로그래머스 전형과 2차 드림어스컴퍼니 전형 두 갈래로 나누어 진행됩니다.<br>1차 과제 전형을 통과해야만 드림어스컴퍼니와 2차 전형을 진행할 수 있게 됩니다. 과제 평가 기준과 관련해서는 아래의 '1차: 프로그래머스 전형' 부분을 읽어주세요.
</p>
<table style='margin: 2rem 0px; display: block; min-height: 0.01%; border-top-right-radius: 0.1875rem; border-bottom-right-radius: 0.1875rem; overflow-x: auto;'>
<thead>
<tr>
<th style='text-align: inherit; border: 1px solid rgb(233, 236, 243); background-color: rgba(50, 50, 144, 0.02); line-height: 1.6; letter-spacing: -0.009em; padding: 0.25rem 0.75rem; vertical-align: middle; min-width: 4rem; font-weight: 500;'>
1차 전형: 과제 퀄리티로 백엔드/앱 개발 능력 검증
</th>
<th style='text-align: inherit; border: 1px solid rgb(233, 236, 243); background-color: rgba(50, 50, 144, 0.02); line-height: 1.6; letter-spacing: -0.009em; padding: 0.25rem 0.75rem; vertical-align: middle; min-width: 4rem; font-weight: 500;'>
2차 전형: 1차 통과 후 회사와의 핏(Fit) 확인
</th>
</tr>
</thead>
<tbody>
<tr>
<td style='line-height: 1.6; letter-spacing: -0.009em; padding: 0.25rem 0.75rem; vertical-align: middle; border: 1px solid rgb(233, 236, 243); min-width: 4rem;'><ol style='margin: 1rem 0px; padding-left: 1.5rem;'><li style='line-height: 2rem; letter-spacing: -0.009em;'>
간단 정보 입력(1/21 14:00 부터)
</li>
<li style='line-height: 2rem; letter-spacing: -0.009em; margin-top: 0.5rem;'>
과제 수행 및 제출(1/28~2/10 12:00 까지)
</li>
</ol>
</td>
<td style='line-height: 1.6; letter-spacing: -0.009em; padding: 0.25rem 0.75rem; vertical-align: middle; border: 1px solid rgb(233, 236, 243); min-width: 4rem;'>
<ol style='margin: 1rem 0px; padding-left: 1.5rem;'>
<li style='line-height: 2rem; letter-spacing: -0.009em;'>
이력서 제출
</li>
<li style='line-height: 2rem; letter-spacing: -0.009em; margin-top: 0.5rem;'>
면접 일정 확정 및 조율(3/2~)
</li>
<li style='line-height: 2rem; letter-spacing: -0.009em; margin-top: 0.5rem;'>
최종 합격 여부 발표
</li>
</ol>
</td>
</tr>
<tr class='reminder'>
<td colspan='2' style='font-size: 0.8rem; line-height: 1.5rem; letter-spacing: -0.009em; padding: 0.25rem 0.75rem; vertical-align: middle; border: 1px solid rgb(233, 236, 243); min-width: 4rem; background-color: rgb(238, 241, 255);'>
1차는 프로그래머스, 2차는 드림어스컴퍼니가 주관합니다. 과제와 관련해서는 프로그래머스에 문의하시되, 1차 통과 후 2차 면접 일정이나 과정에 대해서는 드림어스컴퍼니와 직접 커뮤니케이션을 하셔야 합니다.
</td>
</tr>
</tbody>
</table>
<h3 style='margin: 2.5rem 0px 0px; line-height: 1.6; font-size: 20px; font-family: Inter, NotoSansKR, &quot;Helvetica Neue&quot;, Helvetica, Arial, &quot;맑은 고딕&quot;, &quot;malgun gothic&quot;, 돋움, Dotum, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Noto Color Emoji&quot;; letter-spacing: -2px; -webkit-font-smoothing: antialiased;'>
1차: 프로그래머스 전형
</h3>
<ol style='margin: 0.75rem 0px 0px;'>
<li style='line-height: 2rem; letter-spacing: -0.009em;'>
<span class='list-name'>
간단 정보 입력:
</span>&nbsp;전형을 위해 필요한 아주 기본적인 정보(연락처 등)를 입력합니다. 블라인드 채용이므로 상세한 개인정보나 경력사항은 받지 않습니다.
</li>
<li style='line-height: 2rem; letter-spacing: -0.009em; margin-top: 0.5rem;'>
<span class='list-name'>과제 진행 및 제출:</span>&nbsp;백엔드(Spring)/Android(Kotlin/Java)/iOS(Swift) 중 지원할 분야를 선택하면, 해당 분야별로 준비된 과제를 열람할 수 있게 됩니다. 요구사항을 꼼꼼히 확인하여 구현한 뒤 기한 내에(1/28~2/10 PM12) 제출을 완료합니다.
</li>
<li style='line-height: 2rem; letter-spacing: -0.009em; margin-top: 0.5rem;'>
<span class='list-name'>과제 검토 및 통과 여부 결정</span>
</li>
<div class='process-detail' style='margin: 1rem 0px; padding-left: 1rem; border-left: 5px solid rgb(233, 233, 233);'>
<p style='margin-bottom: 0px; line-height: 1.8; letter-spacing: -0.1px; overflow-wrap: break-word;'>
프로그래머스는 크게 아래의 기준을 갖고 제출한 기능의 소스코드를 검토하게 됩니다. 상세한 평가 기준을 모두 공개해드릴 순 없음을 양해 부탁드립니다.
</p>
<ol style='margin-top: 1rem; margin-right: 0px; margin-left: 0px; padding-left: 1.5rem;'>
<li style='line-height: 2rem; letter-spacing: -0.009em;'>결과물이 정상적으로 작동하는가? 기능 요구사항이 모두 구현되어 있는가?</li>
<li style='line-height: 2rem; letter-spacing: -0.009em; margin-top: 0.5rem;'>사용한 언어, 라이브러리, 프레임워크가 공식적으로 권장하는 개발 방식을 잘 따르고 있는가?</li>
<li style='line-height: 2rem; letter-spacing: -0.009em; margin-top: 0.5rem;'>구조적 확장성과 가독성을 고려해 코드를 작성하였는가?</li>
<li style='line-height: 2rem; letter-spacing: -0.009em; margin-top: 0.5rem;'>테스트가 적절히 구현되어 있는가?</li>
</ol>
<p style='margin-top: 0.75rem; margin-bottom: 0px; line-height: 1.8; letter-spacing: -0.1px; overflow-wrap: break-word;'>즉&nbsp;
<span class='emp-text' style='color: inherit;'>겉으로 보기에 기능이 돌아가는가? 를 넘어 소스코드 레벨의 검토</span>&nbsp;가 이루어집니다. 본인의 지원 직무와 관련한 기술적 이해도가 기본적으로 중요하지만, 다른 개발자들과의 협업을 위해 필요한 역량이 드러나는지도 중요합니다. 이 검토 과정을 거친 이후에 과제 통과 여부가 결정됩니다.
</p>
</div>
<li style='line-height: 2rem; letter-spacing: -0.009em;'>
<span class='list-name'>프로필 입력 요청:</span>&nbsp;2차 드림어스컴퍼니 전형으로 넘어가기 위해 필요한 정보를 전달받습니다. 과제 통과자에 한해서 안내됩니다.
</li>
</ol>
<p class='info-box' style='margin-top: 0.75rem; margin-bottom: 0px; line-height: 1.8; letter-spacing: -0.1px; overflow-wrap: break-word; padding: 10px; border: 1px solid rgb(203, 212, 253); border-radius: 5px; font-size: 0.9rem; background-color: rgb(238, 241, 255);'>
<span class='emp-text' style='color: rgb(119, 117, 255);'>제출하신 과제의 평가 내용을 공유해드립니다.</span>프로그래머스는 여러분이 제출한 과제가 정상적으로 작동할 경우(기본 요구사항을 모두 충족해서 2차 평가가 가능한 수준인 경우), 내부 평가 기준을 토대로 외부 전문가들과 함께 소스코드 레벨의 평가를 진행할 예정입니다. 검토 후 합불여부와 관계 없이 과제에 대한 간략한 피드백을 전달드립니다. 모든 과제 제출 건에 대해 리뷰를 진행하는 것이 아님에 유의해주세요.
</p>
</div>
<div id='about-process-2nd' class='section' style='margin-top: 2rem;'><h3 style='margin: 0px; line-height: 1.6; font-size: 20px; font-family: Inter, NotoSansKR, &quot;Helvetica Neue&quot;, Helvetica, Arial, &quot;맑은 고딕&quot;, &quot;malgun gothic&quot;, 돋움, Dotum, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Noto Color Emoji&quot;; letter-spacing: -2px; -webkit-font-smoothing: antialiased;'>2차: 드림어스컴퍼니 전형
</h3>
<p style='margin-top: 0.75rem; margin-bottom: 0px; line-height: 1.8; letter-spacing: -0.1px; overflow-wrap: break-word;'>과제 통과자에 한해 드림어스컴퍼니 팀과 직접 2차 전형을 진행할 수 있게 됩니다. 이력서 제출과 함께 곧바로 면접 일정을 잡는 것 부터 시작합니다. 지원자에 따라 절차와 최종 합불 여부 결정 기간은 달라질 수 있으니 참고해주세요.</p>
<p style='margin-top: 0.75rem; margin-bottom: 0px; line-height: 1.8; letter-spacing: -0.1px; overflow-wrap: break-word;'>프로그래머스는 1차 전형까지 담당하며, 2차 전형부터는 회사와 지원자가 직접적인 커뮤니케이션을 하게 됩니다.</p>
<ul style='margin: 1rem 0px 0px;'>
<li style='line-height: 1.6; letter-spacing: -0.009em;'>
<span class='list-name'>2차 전형 프로세스 요약:</span>&nbsp;이력서 제출 - 1차 실무진 면접 - 2차 경영진 면접 - 합격 여부 발표
</li>
<li style='line-height: 1.6; letter-spacing: -0.009em; margin-top: 0.5rem;'>
2차 전형은 최대 약 1개월이 소요될 예정입니다.
</li>
</ul>
</div>

<hr style='overflow: visible; margin: 3rem 0px; border-top-width: 0.0625rem; border-top-color: rgb(233, 236, 243);'>
 <div id='faq-group' class='section' style='margin-top: 2rem;'>
<h3 style='margin: 0px; line-height: 1.6; font-size: 20px; font-family: Inter, NotoSansKR, &quot;Helvetica Neue&quot;, Helvetica, Arial, &quot;맑은 고딕&quot;, &quot;malgun gothic&quot;, 돋움, Dotum, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Noto Color Emoji&quot;; letter-spacing: -2px; -webkit-font-smoothing: antialiased;'>FAQ 모음</h3>
<p style='margin-top: 0.75rem; margin-bottom: 0px; line-height: 1.8; letter-spacing: -0.1px; overflow-wrap: break-word;'>챌린지에 참여하기 전에 꼭 읽어보세요.</p>
<div class='faq-item' style='margin: 1rem 0px 2rem;'>
<p class='faq-q' style='margin-bottom: 0px; line-height: 1.8; letter-spacing: -0.1px; overflow-wrap: break-word;'>Q. 과제 리뷰가 무엇인가요? 코드리뷰를 해준다는건가요?</p>
<p class='faq-a' style='margin-top: 0.75rem; margin-bottom: 0px; line-height: 1.8; letter-spacing: -0.1px; overflow-wrap: break-word; font-size: 0.9rem;'>세밀한 코드리뷰가 아닌, 내부 채점 기준을 토대로 진행하는 검토에 가깝습니다. 이를 통해 1차 전형 합불여부를 결정하기도 하지만, 소스코드 전반에 걸쳐 보이는 특징이나 부족함에 대한 인사이트를 드리는 것도 목적 중 하나입니다.</p>
</div>
<div class='faq-item' style='margin: 1rem 0px 2rem;'>
<p class='faq-q' style='margin-bottom: 0px; line-height: 1.8; letter-spacing: -0.1px; overflow-wrap: break-word;'>Q. 과제를 내기만 하면 리뷰를 받을 수 있나요?</p>
<p class='faq-a' style='margin-top: 0.75rem; margin-bottom: 0px; line-height: 1.8; letter-spacing: -0.1px; overflow-wrap: break-word; font-size: 0.9rem;'>아닙니다. 제출하신 과제는 바로 리뷰에 들어가는 것이 아닌, 1차적으로 요구사항에 맞게 모든 기능이 구현되었는지 테스트를 거치게 됩니다. 이 과정에서 기능이 누락되어있거나, 미진하게 구현된 부분이 있을 경우 1차 검증 과정에서 탈락할 수 있습니다. 그 경우 리뷰를 받을 수 없습니다.</p>
</div>
<div class='faq-item' style='margin: 1rem 0px 2rem;'><p class='faq-q' style='margin-bottom: 0px; line-height: 1.8; letter-spacing: -0.1px; overflow-wrap: break-word;'>Q. 과제를 일찍 제출하는게 더 유리한가요?</p>
<p class='faq-a' style='margin-top: 0.75rem; margin-bottom: 0px; line-height: 1.8; letter-spacing: -0.1px; overflow-wrap: break-word; font-size: 0.9rem;'>제출 순서는 합격과 직접적인 연관이 없습니다. 기한 내에만 제출하면 됩니다. 소스코드의 퀄리티에 집중적으로 신경써주세요.</p>
</div>
<div class='faq-item' style='margin: 1rem 0px 2rem;'>
<p class='faq-q' style='margin-bottom: 0px; line-height: 1.8; letter-spacing: -0.1px; overflow-wrap: break-word;'>Q. 이 챌린지를 지인에게 공유했는데, 사이닝 보너스를 나눠 받게 되나요?</p>
<p class='faq-a' style='margin-top: 0.75rem; margin-bottom: 0px; line-height: 1.8; letter-spacing: -0.1px; overflow-wrap: break-word; font-size: 0.9rem;'>사이닝 보너스 100만 원은 최종 합격한 지원자에게만 지급되는 금액입니다. 이 챌린지를 공유했거나, 주변에 소개했다고 해서 합격자와 보너스를 나누어 지급받는 구조가 아님에 꼭 유의해주세요. 채용된 당사자만 받게 됩니다.</p>
</div>
<div class='faq-item' style='margin: 1rem 0px 2rem;'>
<p class='faq-q' style='margin-bottom: 0px; line-height: 1.8; letter-spacing: -0.1px; overflow-wrap: break-word;'>Q. 이런 유형의 챌린지(채용 연계 프로그램)가 또 열릴 예정인가요?</p>
<p class='faq-a' style='margin-top: 0.75rem; margin-bottom: 0px; line-height: 1.8; letter-spacing: -0.1px; overflow-wrap: break-word; font-size: 0.9rem;'>지속적인 운영 계획이 있으며, 다양한 기술 스택 기반의 챌린지를 추가해나갈 것입니다. 개발자들이 경력의 유무와 상관없이 본인의 소스코드로 역량을 보일 수 있도록, 블라인드 채용 기반의 이벤트를 지속해 나갈 것입니다.</p>
</div></div>
                	
                 </div>
        </td>
    <td id="aside">
    </td>
    </tr>
</table>

					<form id="application" action="/itda/jyhBasicInfo.me" method="post" style="display:none;">
                		<input type="text" name="position" value="<%= position%>" style="display:none;">
                		<input type="text" name="aNo" value="<%=content.get("aNo")%>" style="display:none;">
                		<input type="text" name="userNo" value="<%= loginUser.getPerNo()%>" style="display:none;">
                		<input type="text" name="division" value="<%= content.get("division")%>" style="display:none;">
                	</form>
					<form id="testApply" action="/itda/jyhSelectTestContents.re" method="post" style="display:none;">
                		<input type="text" name="position" value="<%= position %>" style="display:none;">
                		<input type="text" name="aNo" value="<%=content.get("aNo")%>" style="display:none;">
                		<input type="text" name="userNo" value="<%= loginUser.getPerNo()%>" style="display:none;">
                		<input type="text" name="division" value="<%= content.get("division")%>" style="display:none;">
                	</form>
    
    <%@ include file="../../common/cho_footer.jsp" %>
    
    <script>
    	$(function(){
    		$("#applyBtn").click(function(){
    			$("#application").submit();
    			
    		})
    		
    		$("#testBtn").click(function(){
    			$("#testApply").submit();
    			
    		})
    	});
    </script>

</body>
</html>