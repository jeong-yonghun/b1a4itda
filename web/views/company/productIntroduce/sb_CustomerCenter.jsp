<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<style>
#all {
	width: 1000px;
	height: 1000px;
	margin-left: auto;
	margin-right: auto;
	margin-top: 4%; 
}

#bba {
	border-top: 1px solid black;
	font-family: 'Noto Sans KR', sans-serif;
}

#baa {
	border-top: 1px solid black;
	font-family: 'Noto Sans KR', sans-serif;
}

.sideCustomerCenter {
	border: 1px solid blue;
	width: 150px;
}

.inquiryBoard>table {
	/* border: 1px solid blue; */
	
}

.inquiryBoard {
	width: 700px;
	margin-left: 20px;
}

td {
	height: 30px;
}

#btnArea {
	width: 150px;
	margin-left: auto;
	margin-right: auto;
}

.btn1 {
	background-color: blue;
	color: white;
	border-radius: 5px;
}

.btn2 {
	background-color: gray;
	color: white;
	border-radius: 5px;
}
</style>
</head> 
<body>
	<%@ include file="../../common/menubar.jsp"%>
	<div id="all">
		<div class="sideCustomerCenter" style="float: left;">
			<table>
				<tr>
					<td>
						<table>
							<tr>
								<td
									width: 100px; style="font-family: 'Noto Sans KR', sans-serif;">고객센터</td>
							</tr>
							<tr>
								<td width: 100px; id="baa">FAQ</td>
							</tr>
							<tr>
								<td width: 100px; id="bba">문의하기</td>
							</tr>
							<tr>
								<td width: 100px; id="bba">- 문의접수</td>
							</tr>
							<tr>
								<td width: 100px; id="bba">- 문의내역</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>
		<form>
			<div class="inquiryBoard" style="float: left;">
				<table class="board">
					<tr>
						<td width="100px" height="50px" style="font-size: 20px;">문의접수
						</td>
					</tr>
					<tr>
						<td width="100px" height="50px">문의종류</td>
						<td><select>
								<option>회원가입/정보</option>
								<option>이력서관리/활용</option>
								<option>입사지원</option>
								<option>채용정보</option>
								<option>포트폴리오관리</option>
								<option>기타</option>
						</select></td>
					</tr>
					<tr>
						<td width="100px" height="50px">제목</td>
						<td><input type="text" placeholder="제목을 입력해주세요 "
							maxlength="100" style="width: 300px;"></td>
					</tr>
					<tr>
						<td>내용</td>
						<td><textarea maxlength="1000"
								style="width: 400px; height: 400px;">
                </textarea></td>
					</tr>
					<tr>
						<td>파일첨부</td>
						<td>
							<div id="contentImgArea">
								<img id="contentImg" width="410" height="20">
							</div>
						</td>
					</tr>
				</table>
				<div id="fileArea">
					<input type="file" id="thumbnailImg" name="thumbnailImg"
						onchange="loadImg(this, 1);">
				</div>
				<div id="btnArea">
					<button class="btn1" type="submit">문의하기</button>
					<button class="btn2">취소하기</button>
				</div>
			</div>
		</form>
	</div>
	<script>
        $(function(){
            $("#fileArea").hide();

            $("#contentImgArea").click(function(){
   				$("#thumbnailImg").click();
   			}); 

        });
    </script>
</body>
</html>