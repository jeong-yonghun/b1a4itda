<%@page import="com.sun.java.swing.plaf.windows.WindowsTreeUI.CollapsedIcon"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" import="java.util.*, com.kh.itda.notice.payAndProduct.model.vo.*"%>
	
<%
	ArrayList<Sb_Payment> list = (ArrayList<Sb_Payment>) request.getAttribute("list");
	Sb_PageInfo pi =  (Sb_PageInfo) request.getAttribute("pi");
	int listCount = pi.getListCount();
	int currentPage = pi.getCurrentPage();
	int maxPage = pi.getMaxPage();
	int startPage = pi.getStartPage();
	int endPage = pi.getEndPage();
%> 
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<style> 
.outer {
	width: 1400px;
	height: 800px;
	background: white;
	color: black;
	margin-left: auto;
	margin-right: auto;
	margin-top: 50px;
}

.tableArea>table {
	border: 1px solid white; 
	text-align: center;
}

.tableArea {
	width: 650px;
	height: 200px;
	margin-left: 200px;
	margin-bottom: 500px;
}
.payno {
	color: orange;
}
#modal {
  position:relative;
  width:1400px;
  height:200px;
  z-index:1;
  margin : auto;
  display: none;
  bottom : 700px;
}

#modal h2 {
  margin:0;   
}

#modal button {
  /* display:inline-block;
  width:100px;
  margin-left:calc(100% - 100px - 10px); */
}

#modal .modal_content {
  width:500px;
  height: 530px;
  margin:100px auto;
  padding:20px 10px;
  background:#fff;
  border:2px solid #666;
}

#modal .modal_layer {
  position:fixed;
  top:0;
  left:0;
  width:100%;
  height:100%;
  background:rgba(0, 0, 0, 0.5);
  z-index:-1;
}
.modal_close_btn1 > button {
	border-top-left-radius: 25px;
	border-bottom-left-radius: 25px;
	border-top-right-radius: 25px;
	border-bottom-right-radius: 25px;
	background:#b3b3cc;
	margin-left: 150px;
	width: 130px;
	height: 30px;
	color: white;
	
}
#modal_hr {
	background: #5A84F1;
	border: 1px solid;
	width: 300px;
}
</style>
</head> 
<body>
	<%@ include file="../company_common/sb_companyMenubar.jsp"%>
	<br>
	<br>
	<br>
	<br>
	<%@ include file="../company_common/sb_companyIntroMenubar.jsp"%>
	<div class="outer">
		<br>
		<table>
			<tr>
				<td><%@ include
						file="../company_common/sb_sideMenubarAndCompanyUserInfo.jsp"%>
				</td>
				<td>
					<div class="tableArea">
						<h2>결제내역</h2>
						<hr>
						<table id="listArea">
							<tr>
							<div></div>
								<td width="400px" height="50px">결제일자</td>
								<td width="200px">결제코드</td>
							 	<td width="500px">상품명</td>
								<td width="300px">결제금액</td> 
								<!-- <th width="300px">결제금액</th>  -->
								<td width="300px">사용기간</td> 
								<td width="300px">결제상태</td>
							</tr>
							 <%for(Sb_Payment pay : list) { %>
							<tr>
								<td height="30px"><%=pay.getPd_date() %></td>
								<td><%=pay.getImp_uid() %></td>
							 	<td><%=pay.getPd_name() %></td>
								<td><%=pay.getPd_price() %></td>
								<!-- <td>코드필요없나?</td> -->
								<td><%if(pay.getPd_validity() != null) { %>
								 <%=pay.getPd_validity() %> 
								 <% } %>
								</td>
								<!-- <td><p class="payno"><U>결제취소</U></p></td> -->
								<td class="payno"><% if(pay.getPy_division().equals("결제")) {%><u>결제취소</u><%}else { %><u><%=pay.getPy_division() %></u><% } %></td>
							</tr>
							<% } %> 
						</table>
						<br><br>				<%-- 페이지처리 --%>
						<div class="pagingArea" align="center">
							<button onclick="location.href='<%=request.getContextPath()%>/selectOnePaymentList.pay?currentPage=1&comNo=<%=loginUser.getComNo()%>'"><<</button>
							
							<%if(currentPage <= 1) { %>
							<button disabled><</button>
							<% } else  { %>
							<button onclick="location.href='<%=request.getContextPath() %>/selectOnePaymentList.pay?currentPage=<%=currentPage -1 %>&comNo=<%=loginUser.getComNo()%>'"><</button>
							<% } %>
							
							<% for(int p = startPage; p <= endPage; p++) { 
								if(p == currentPage) {
							%>
								<button disabled><%= p %></button>
							<% 
								} else {
							%>		
								<button onclick="location.href='<%=request.getContextPath()%>/selectOnePaymentList.pay?currentPage=<%=p %>&comNo=<%=loginUser.getComNo()%>'"><%=p %></button>
							<%
								}
							}	
							%>
							
							<% if(currentPage >= maxPage) {%>
				      		<button disabled>></button>
				      		<% } else { %>
				      		<button onclick="location.href='<%=request.getContextPath()%>/selectOnePaymentList.pay?currentPage=<%=currentPage + 1%>&comNo=<%=loginUser.getComNo()%>'">></button>
				      		<% } %>
				      	
				      		<button onclick="location.href='<%=request.getContextPath()%>/selectOnePaymentList.pay?currentPage=<%=maxPage%>&comNo=<%=loginUser.getComNo()%>'">>></button>
						</div>
					</div>
				</td>
			</tr>
			<tr>
				<td>
					<div>
						<button onclick="location.href='<%=request.getContextPath()%>/adminPayList.pay'">관리자페이지</button>
					</div>
				</td>
			</tr>
		</table>
		<!-- 모달 창 띄우기 -->
		<div id="modal">
   
	    	<div class="modal_content">
	    		 <div style="float: left;">
	       		 <h2 text-align="center">결제취소안내</h2>
	       		 <hr id="modal_hr">
	       		 </div>
	       		 <div class="modal_close_btnn">
	       		 	<img src="/itda/image/sb_icon.png" style="width: 100%; max-width: 25px; float: right; margin-right: 24px;">
	       		 </div>
	       		 <textarea cols="60" rows="15"> - 채용광고상품은 결제, 광고승인이 되면 공고시작날짜부터 적용되는 상품입니다. (기업로고가 등록되어 있지 않을 경우 로고영역에 텍스트로 기업명이 노출됩니다.)&#10; - 기업은 포인트가 적용되지 않으므로 참고바랍니다&#10; - [주의] 유료 상품을 이용 중이신 경우, 공고를 조기 마감하더라도 남은 기간에 대한 차액은 환불되지 않습니다.&#10; - 구매한 인재열람 상품은 기업인증 완료 후 이용하실 수 있습니다. (이력서 열람제한 기업, 휴폐업 업체 등은 구매 제한됩니다.)&#10; - 인재열람 상품을 통해, 연락처 확인한 인재의 이력서는 열람일부터 90일 동안 재확인 가능합니다.&#10; - 단, 인재 고객이 연락처를 비공개로 전환한 경우, 이력서를 삭제한 경우, 탈퇴한 경우에는 재확인 불가합니다.&#10;</textarea>
					<br>
	       		 <div>
	       		 	<label><input type="checkbox" id="chk" name="agree">위 내용을 숙지하셨고 결제취소를 하시겠습니까?</label>
	       		 </div>	
	      		  <!-- <button type="button" id="modal_close_btn"  value=" 확 인 ">결제취소</button> -->
	      		  <br><br>
	      		  <div class="modal_close_btn1" style="margin-left: 30px">
	      		  	<button type="button" class="modal_close_btn" disabled>결제취소</button>
	      		  	<!-- <button type="button" class="modal_close_btnn">닫기</button>  -->
	      		  </div>
	    	</div>
   
    		<div class="modal_layer"></div>
		</div>
		<script>
		//$(".modal_close_btn").attr("disabled", true);
		 $("#chk").click(function(){
			 	console.log("드렁옴")
		        var chk = $('input:checkbox[id="chk"]').is(":checked");
		        console.log(chk);
		        if(chk==true){
		            $(".modal_close_btn").removeAttr('disabled');
		            $(".modal_close_btn").attr("style", "background:#5A84F1");
		            $(".box").removeClass("on");
		        }else{
		            $(".modal_close_btn").attr("disabled", true);
		            $(".modal_close_btn").removeAttr("style", "background:#5A84F1");
		            $(".box").addClass("on");
		        }
		 });
		</script>
	</div>
	<script>
			$(".payno").click(function (){
				//document.getElementById("modal").style.display="block";
				var thi = $(this).text();
				console.log(thi);
				var payno = $(this).parent().children().eq(1).text();
				if(thi == "결제취소") {
					//var check = confirm("결제 취소를 하시겠습니까?");
					$("#modal").attr("style", "display:block");
					var test = $("#modal_close_btn").text();
					console.log(test);
						$(".modal_close_btn").click(function() {
							$("#modal").fadeIn();
						$.ajax({
							url : "delete.pay",
							data:{payno : payno},
							type : "get",
							success : function(data) {
								console.log("성공들어옴");
								location.reload();
							},
							error: function(request, status, error) {
								console.log("서버 전송 실패!");
								console.log(request);
								console.log("status : " + status);
								console.log("error: " + error);
								
							},
							complete : function() {
								console.log("무조건 호출되는 함수");
							}
						});
							
						});
					  
				}
			    });    
				$(".modal_close_btnn").click(function(){
			        $("#modal").attr("style", "display:none");
				});
				
	</script>
</body>
</html>