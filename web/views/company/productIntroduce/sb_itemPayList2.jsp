<%@page import="com.sun.java.swing.plaf.windows.WindowsTreeUI.CollapsedIcon"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" import="java.util.*, com.kh.itda.notice.payAndProduct.model.vo.Sb_Payment"%>
	
<%
	ArrayList<Sb_Payment> list = (ArrayList<Sb_Payment>) request.getAttribute("list");
%> 
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<style>
.outer {
	width: 1400px;
	height: 700px;
	background: white;
	color: black;
	margin-left: auto;
	margin-right: auto;
	margin-top: 50px;
}

.tableArea>table {
	border: 1px solid white; 
	text-align: center;
}

.tableArea {
	width: 650px;
	height: 800px;
	margin-left: 200px;
}
</style>
</head> 
<body>
	<%@ include file="../company_common/sb_companyMenubar.jsp"%>
	<br>
	<br>
	<br>
	<br>
	<%@ include file="../company_common/sb_companyIntroMenubar.jsp"%>
	<div class="outer">
		<br>
		<table>
			<tr>
				<td><%@ include
						file="../company_common/sb_sideMenubarAndCompanyUserInfo.jsp"%>
				</td>
				<td>
					<div class="tableArea">
						<h2>결재내역</h2>
						<hr>
						<table align="center" id="listArea">
							<tr>
								<th width="300px">결제일자</th>
								<th width="100px">결제코드</th>
							 	<th width="500px">상품명</th>
								<th width="300px">결제금액</th> 
								<th width="300px">결제금액</th> 
								<th width="300px">남은기간</th> 
							</tr>
							
							 <%for(Sb_Payment pay : list) { %>
							<tr>
								<td><%=pay.getPd_date() %></td>
								<td> <%=pay.getImp_uid() %></td>
							 	<td><%=pay.getPd_name() %></td>
								<td><%=pay.getPd_price() %></td>
								<td>코드필요없나?</td>
								<td><%=pay.getPd_validity() %></td>
							</tr>
							<% } %> 
						</table>
					</div>
				</td>
			</tr>
		</table>
	</div>
</body>
</html>