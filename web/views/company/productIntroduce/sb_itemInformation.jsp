<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script type="text/javascript"
	src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<script type="text/javascript"
	src="https://service.iamport.kr/js/iamport.payment-1.1.5.js"></script>
<style>
div {
	/* text-align: center; */
	color: black;
}

header {
	margin-left: auto;
	margin-right: auto;
	width: 1400px;
	height: 100px;
}

#contents {
	margin-left: auto;
	margin-right: auto;
	width: 1400px;
	height: 100%;
	background-color: white;
}

nav {
	width: 20%;
	height: 100%;
	float: left;
}

section {
	width: 80%;
	height: 98%;
	background-color: white;
	float: left;
}

#ksb_hr1 {
	border: solid 1px slategray;
	width: 800px;
}
.ksb_hr2 {
	border: solid 2px slategray;
	width: 800px;

}

.btn > button {
	border: solid 1px #5A84F1;
	background-color: #5A84F1;
	color: white;
	padding: 5px; 
	border-top-left-radius: 25px;
	border-bottom-left-radius: 25px;
	border-top-right-radius: 25px;
	border-bottom-right-radius: 25px;
}

.ksb_btable {
	text-align: left;
	border: solid 1px #5A84F1;
	background-color: white;
}

#menubar>table {
	color: darkgoldenrod;
	font-size: 30px;
	margin: auto;
}

.item1 {
	width: 80%;
	height: 100%;
	background-color: white;
	float: left;
	text-align: left;
	font-weight: bold;
}

.item2 {
	width: 100%;
	height: 100%;
	background-color: white;
}

.item3 {
	width: 100%;
	height: 100%;
	background-color: white;
}

.per-open {
	position: absolute;
	width: 200px;
	height: 200px;
	top: 350px;
	left: 340px;
}

.per-information {
	position: absolute;
	width: 200px;
	height: 200px;
	top: 400px;
	left: 450px;
}

.itemtable3 {
	border: 2px solid #5A84F1;
}

.hr2 {
	/* style="width: 500px" color="red" */
	width: 400px;
}

.hiddenCheckBox {
	visibility: hidden;
}

#cho_userInfo {
	width: 200px;
	height: 350px;
	border: 1px #CCCCCC solid;
	border-radius: 1pc;
	margin-left: auto;
	margin-right: auto;
	text-align: center;
	font-family: 'Noto Sans KR', sans-serif;
	/* margin-top:20px; */
}

#cho_personImgDiv {
	width: 60px;
	height: 60px;
	border: solid #5A84F1 1px;
	border-radius: 10pc;
	margin-right: auto;
	margin-left: auto;
	margin-top: 15px;
}

#cho_userImg {
	width: 100%;
	height: 100%;
	border-radius: 10pc;
	margin-right: auto;
	margin-left: auto;
}

#cho_deletemember {
	margin-top: 70px;
	color: red;
}

#cho_sideMenubar {
	width: 200px;
	height: 400px;
	border: 2px solid #5A84F1;
	margin-left: auto;
	margin-right: auto;
	margin-top: 30px;
	font-family: 'Noto Sans KR', sans-serif;
}

#cho_sideMenubarTable {
	width: 160px;
	height: 90%;
	margin-left: auto;
	margin-right: auto;
}

#cho_sideMenubarTableTHaedP {
	font-size: 20px;
	font-weight: bold;
}

.sideSubTitle {
	font-size: 13px;
	font-weight: bold;
	border-top: 1px solid #C4C4C4;
	padding-top: 10px;
	padding-bottom: 10px;
}

.sideSide {
	padding-left: 0px;
	font-size: 15px;
	font-weight: bold;
}
.per-infotable {
	border:2px solid #5A84F1;
}
.per-infotable > table > th {
	font-weight: bold;
}
.itemtable {
	text-align: center;
}
.btn2 > button {
	border: solid 1px #5A84F1;
	background-color: #5A84F1;
	color: white;
	border-top-left-radius: 25px;
	border-bottom-left-radius: 25px;
	border-top-right-radius: 25px;
	border-bottom-right-radius: 25px;
	width: 200px;
	height: 70px;
	
}
</style>
</head>
<body>
<body>
	<%@ include file="../company_common/sb_companyMenubar.jsp"%>
	<%-- <%if(loginUser != null) { %> --%>
	<!-- 메뉴바 끝 -->
	<br>
	<br>
	<br>
	<br>
	<%@ include file="../productIntroduce/sb_productIntroMenubar.jsp"%>

	<%--  <%@ include file="../productIntroduce/productIntroMenubar.jsp" %> --%>
	<div id="contents" class="test1">
		<!-- 전체 화면 div 시작 -->

		<nav id="nav" class="test1">
			<br>
			<%-- <%@ include
					file="../productIntroduce/sb_sideMenubarAndCompanyProductUserInfo1.jsp"%> --%>
			<div id="cho_userInfo">
				<div id="cho_personImgDiv">
					<img id="cho_userImg" src="../../../image/logo.PNG">
				</div>
				<h3 id="cho_userName"><%=loginUser.getCompanyName() %></h3>
				<h4 id="cho_phone"><%=loginUser.getPhone() %></h4>
				<h4 id="cho_email"><%=loginUser.getEmail() %></h4>
				<h5 id="cho_github"><%=loginUser.getComUrl() %></h5>
				<p id="cho_deletemember" onclick="deletemember()">회원탈퇴</p>
			</div>

			<script> 
        function deletemember(){
    		        //location.href="";
    		    }
   					 </script>
			<!-- 개인마이페이지 사이드 메뉴바 미완성!!!!!!!!!!!!!!!!클릭할때 색바뀌는거하기-->
			<div id="cho_sideMenubar">
				<table id="cho_sideMenubarTable">

					<tr id="cho_sideMenubarTableTHaed">
						<td><p id="cho_sideMenubarTableTHaedP">상품안내</p></td>
					</tr>
					<tr>
						<td class="sideSide" id="cho_dateil" onclick="goDateil()">채용공고
							상품</td>
					</tr>
					<tr>
						<td><hr></td>
					</tr>
					<tr>
						<td class="sideSide" id="cho_da1" onclick="goContestApp()">인재열람
							상품</td>
					</tr>
					<tr>
						<td><hr></td>
					</tr>
					<tr>
						<td class="sideSide" id="cho_resumewri" onclick="goResumeWriter()">공모전공고
							상품</td>
					</tr>
				</table>
			</div>
		</nav>

		<section id="section" class="test1">
			<br>
			<div style="width: 100%; height: 100%; background-color: coral;">
				<div
					style="width: 10%; height: 100%; background-color: white; float: left;"></div>
				<!--상품안내 여백-->
				<div
					style="width: 10%; height: 100%; background-color: white; float: right;"></div>
				<!--상품안내 여백-->
				<!-- 채용공고상품 -->
				<div class="item1">

					&nbsp;&nbsp;&nbsp;<label
						style="font-size: 30px; font-weight: bold;">채용공고상품</label>
					<hr class="ksb_h2">
					<br> <br> <br>
					<div
						style="border: 2px solid; border-color: #5A84F1; width: 100%; height: 190px;">
						<div
							style="color: white; width: 30%; height: 20%; border: 1px solid; padding: 10px; margin: 20px; float: left;">
							<div style="font-size: 25px; float: left; font-weight: bold;">채용공고상단</div>
							<div
								style="color: white; width: 23px; height: 23px; border: 1px solid; float: left; margin-top: 7px; margin-left: 10px;">
								<input type="radio" name="select" value="AB1">
								<!-- <img src="image/15.PNG" width="23px" height="23px"> -->
							</div>
						</div>
						<div style="font-size: 13px;">
							<table
								style="float: right; margin-top: 50px; margin-right: 200px;">
								<tr>
									<td width="100px">장수선택</td>
									<td><select>
											<option value="1">1장</option>
									</select></td>
								</tr>
								<tr>
									<td><br></td>
								</tr>
								<tr>
									<td>가격</td>
									<td><label style="color: #5A84F1;">1,000,000</label>원</td>
								</tr>
							</table>
						</div>
						<br> <br> <br> <br>
						<div style="float: left; font-size: 13px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;기업로고,
							이미지와 함께 첫페이지 채용공고 최상단에 노출되는 상품입니다.</div>
						<br> <br>
						<hr id="ksb_hr1">
					</div>
					<br> <br>
					<!--채용 공고 상단 끝-->
					<div
						style="border: 2px solid; border-color: #5A84F1; width: 100%; height: 190px;">
						<div
							style="color: white; width: 30%; height: 20%; border: 2px solid; padding: 10px; margin: 20px; float: left;">
							<div style="font-size: 25px; float: left; font-weight: bold;">채용공고중단</div>
							<div
								style="color: white; width: 23px; height: 23px; border: 2px solid; float: left; margin-top: 7px; margin-left: 10px;">
								<input type="radio" name="select" value="AB2">
								<!-- <img src="image/15.PNG" width="23px" height="23px"> -->
							</div>
						</div>
						<div style="font-size: 13px;">
							<table
								style="float: right; margin-top: 50px; margin-right: 200px;">
								<tr>
									<td width="100px">장수선택</td>
									<td><select>
											<option>1장</option>
									</select></td>
								</tr>
								<tr>
									<td><br></td>
								</tr>
								<tr>
									<td>가격</td>
									<td><label style="color: #5A84F1;">600,000</label>원</td>
								</tr>
							</table>
						</div>
						<br> <br> <br> <br>
						<div style="float: left; font-size: 13px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;기업로고,
							이미지와 함께 첫페이지 채용공고 중단에 노출되는 상품입니다.</div>
						<br> <br>
						<hr id=ksb_hr1>
						<div class="more" style="text-align: right; margin-right: 50px;">상세보기
							V</div>

						<div class="ksb_board">
							<table class="ksb_btable">
								<br>
								<tr>
									<td>가격 : 100000원</td>
								</tr>
								<tr>
									<td>* 노출위치 : 채용공고 최상단에 노출(중복공고불가능)</td>
								</tr>
								<tr>
									<td>* 이용안내 : 신청시 2~3영업일 소요</td>
								</tr>
								<tr>
									<td>dsadsa</td>
								</tr>
								<tr>
									<td>dsaa</td>
								</tr>
							</table>
						</div>
					</div>
					<br> <br> <br> <br>
					<hr id=hr1>
					<br> <br> <br>
					<p style="font-size: 10px;">
						- 채용광고상품은 결제, 광고승인이 되면 공고시작날짜부터 적용되는 상품입니다. (기업로고가 등록되어 있지 않을 경우
						로고영역에 텍스트로 기업명이 노출됩니다.)<br> - 기업은 포인트가 적용되지 않으므로 참고바랍니다<br>
						- [주의] 유료 상품을 이용 중이신 경우, 공고를 조기 마감하더라도 남은 기간에 대한 차액은 환불되지 않습니다.<br>
						- 구매한 인재열람 상품은 기업인증 완료 후 이용하실 수 있습니다. (이력서 열람제한 기업, 휴폐업 업체 등은 구매
						제한됩니다.)<br> - 인재열람 상품을 통해, 연락처 확인한 인재의 이력서는 열람일부터 90일 동안 재확인
						가능합니다.<br> - 단, 인재 고객이 연락처를 비공개로 전환한 경우, 이력서를 삭제한 경우, 탈퇴한
						경우에는 재확인 불가합니다.<br> - 인재 이력서 열람일로부터 90일이 경과하였으나, 구매 상품 유효기간이
						만료되지 않은 경우에는 상품 유효기간까지 열람한 인재 이력서 재확인이 가능합니다.<br> - 정보통신망법
						제24조 2항에 근거하여 인재열람 서비스는 채용을 목적으로 하는 경우에만 이용할 수 있으며,<br> - 기업의
						영업, 마케팅 등 채용 이외의 용도로는 이용할 수 없습니다.<br> - 채용 외 목적으로 이용하실 경우,
						제71조 3에 의거 5년 이하 징역 또는 5,000만 원 이하의 벌금에 처할 수 있습니다.<br> -
						정보통신부 고시 제2005-18호에 근거하여 이력서 등과 같이 개인정보가 담긴 출력·복사물을 불법 유출 및 배포하게
						되면<br> - 법에 따라 책임을 지게 됨을 양지하시기 바랍니다.<br> - 인재열람 상품 이용 중
						사람인 개인정보보호 시스템에 의해, 정상적인 검색 패턴이 아닌 것으로 감지될 경우 일시적으로 이용이 제한되며<br>
						- 반복 감지 시 경고 단계에 따라 서비스 이용 및 상품 구매가 차단됩니다. (이용 제한 된 시간만큼 시간 연장
						불가합니다.)<br> - 기업 확인을 위해 고객센터에서 직접 연락을 드릴 수 있으며, 경우에 따라 사업자등록증
						사본 및 기타 증빙자료를 요청할 수 있습니다.<br> - 상품구매시 사이트적합성에 따라 승인/반려가 될수있으며
						반려가 될시 재수정하여 승인이 되어야 사이트에 업로드가 됩니다.<br> - 한채용공고나 한사이트에서 같은
						채용공고를 중복해서 올릴수 없습니다<br>
					</p>
					<br> <br> <br> <br>
					<div class="btn2" style="text-align: center;"> 
						<button type="submit">결제하기</button>
					</div>
					<br><br><br><br><br><br>
				</div>
				<!-- 인재열람 상품 시작 -->
				<div class="item2">
					<div style="float: left;">
						<label style="font-size: 30px; font-weight: bold;">인재열람상품</label>
					</div>
					<br> <br>
					<hr class="ksb_h2">
					<br> <br> <br>
					<div class="per-open">
						<img src="/itda/image/peropen.PNG">
					</div>
					<br> <br> <br> <br> <br> <br> <br>
					<br> <br> <br> <br> <br>
					<div class="itemtable">
						<form>
							<table class="per-infotable">
								<tr>
									<th width="100px" height="40px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;선택</th>
									<th width="100px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;건수</th>
									<th width="100px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;이용기간</th>
									<th width="200px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;가격(VAT포함)</th>
									<th width="500px" rowspan="5">
										<p style="text-align: left;">
											- 등록되어있는 인재들의 이력서 및 포트폴리오를<br> 전체내용을 확인 할 수 있습니다.<br><br>
											- 올린 공고를 통해 지원한 지원자들의 이력서는<br> 인재열람권을 구매하지 않아도 확인이
											가능합니다.<br><br> - 인재열람권을 다 사용하거나 소멸되어야 추가 구매가<br> 가능합니다.<br><br>
											* 자세한 내용은 아래 이용안내를 참고해주시길 바랍니다.
										</p>
									</th>
								</tr>
								<tr>
									<td width="50px" height="80px"><input type="radio"
										name="select" id="itemopen1" value="AB3"></td>
									<td width="100px">500건</td>
									<td width="100px">365일</td>
									<td width="200px"><label style="color: #5A84F1;">500,000</label>원
									</td>
								</tr>
								<tr>
									<td colspan="4"><hr class="hr2"></td>
								</tr>
								<tr>
									<td width="50px" height="80px"><input type="radio"
										name="select" id="itemopen2" value="AB4"></td>
									<td width="100px">300건</td>
									<td width="100px">100일</td>
									<td width="200px"><label style="color: #5A84F1;">300,000</label>원</td>

								</tr>
								<tr>
									<td colspan="4"><hr class="hr2"></td>
								</tr>
								<tr>
									<td width="50px" height="80px"><input type="radio"
										name="select" id="itemopen3" value="AB5"></td>
									<td width="100px">100건</td>
									<td width="100px">100일</td>
									<td width="200px"><label style="color: #5A84F1;">100,000</label>원</td>
									<td><div style="display: none;">
											<input type="date" name="date" id="date">
										</div></td>

								</tr>
								<tr>
									<td colspan="4"><hr class="hr2"></td>
								</tr>
								<tr>
									<td width="50px" height="80px"><input type="radio"
										name="select" id="itemopen4" value="50"></td>
									<td width="100px">50건</td>
									<td width="100px">100일</td>
									<td width="200px"><label style="color: #5A84F1;">70,000</label>원</td>
									<td><div style="display: none;">
											<input type="date" name="date" id="date">
										</div></td>
								</tr>
							</table>
						</form>
					</div>
					<br> <br> <br> <br>
					<div class=".per-information" style="text-align: center;">
						<img src="/itda/image/usage.PNG" width="700px;" height="500px;">
					</div>
					<br> <br> <br> <br>
					<p p style="font-size: 10px;">
							- 채용광고상품은 결제, 광고승인이 되면 공고시작날짜부터 적용되는 상품입니다. (기업로고가 등록되어 있지 않을 경우
							로고영역에 텍스트로 기업명이 노출됩니다.)<br> - 기업은 포인트가 적용되지 않으므로 참고바랍니다<br>
							- [주의] 유료 상품을 이용 중이신 경우, 공고를 조기 마감하더라도 남은 기간에 대한 차액은 환불되지 않습니다.<br>
							- 구매한 인재열람 상품은 기업인증 완료 후 이용하실 수 있습니다. (이력서 열람제한 기업, 휴폐업 업체 등은 구매
							제한됩니다.)<br> - 인재열람 상품을 통해, 연락처 확인한 인재의 이력서는 열람일부터 90일 동안 재확인
							가능합니다.<br> - 단, 인재 고객이 연락처를 비공개로 전환한 경우, 이력서를 삭제한 경우, 탈퇴한
							경우에는 재확인 불가합니다.<br> - 인재 이력서 열람일로부터 90일이 경과하였으나, 구매 상품 유효기간이
							만료되지 않은 경우에는 상품 유효기간까지 열람한 인재 이력서 재확인이 가능합니다.<br> - 정보통신망법
							제24조 2항에 근거하여 인재열람 서비스는 채용을 목적으로 하는 경우에만 이용할 수 있으며,<br> -
							기업의 영업, 마케팅 등 채용 이외의 용도로는 이용할 수 없습니다.<br> - 채용 외 목적으로 이용하실
							경우, 제71조 3에 의거 5년 이하 징역 또는 5,000만 원 이하의 벌금에 처할 수 있습니다.<br> -
							정보통신부 고시 제2005-18호에 근거하여 이력서 등과 같이 개인정보가 담긴 출력·복사물을 불법 유출 및 배포하게
							되면<br> - 법에 따라 책임을 지게 됨을 양지하시기 바랍니다.<br> - 인재열람 상품 이용 중
							사람인 개인정보보호 시스템에 의해, 정상적인 검색 패턴이 아닌 것으로 감지될 경우 일시적으로 이용이 제한되며<br>
							- 반복 감지 시 경고 단계에 따라 서비스 이용 및 상품 구매가 차단됩니다. (이용 제한 된 시간만큼 시간 연장
							불가합니다.)<br> - 기업 확인을 위해 고객센터에서 직접 연락을 드릴 수 있으며, 경우에 따라
							사업자등록증 사본 및 기타 증빙자료를 요청할 수 있습니다.<br> - 상품구매시 사이트적합성에 따라
							승인/반려가 될수있으며 반려가 될시 재수정하여 승인이 되어야 사이트에 업로드가 됩니다.<br> -
							한채용공고나 한사이트에서 같은 채용공고를 중복해서 올릴수 없습니다
						</p><br><br><br><br><br><br>
					<div class="btn2" style="text-align: center;"> 
						<button type="submit">결제하기</button>
					</div>
					<br><br><br><br><br><br><br><br><br><br><br><br>
					<%-- <div class="btn" style="text-align: center;">
						<button type="button"
							onclick="location.href='<%=request.getContextPath()%>/selectPaymentList.pay'">결제하기22</button>
					</div>
					<div class="btn" style="text-align: center;">
						<button type="button"
							onclick="location.href='<%=request.getContextPath()%>/selectOnePaymentList.pay'">결제하기33</button>
					</div> --%>
				</div>
				<!-- 인재열람 상품 끝 -->
				<div class="item3">
					<div style="float: left;">
						<label style="font-size: 30px; font-weight: bold;">공모전공고상품</label>
					</div>
					<br> <br>
					<hr>
					<br> <br> <br>
					<div>
						<table class="itemtable3" width="800px">
							<tr>
								<td width="200px" height="110px"
									style="font-size: 30px; font-weight: bold; float: left; padding: 30px">공모전공고</td>
								<td style="margin-left: 200px"><input type="radio"
									name="select" value="AB7"></td>
							</tr>
							<tr>
								<td width="400px" height="100px">공모전 공고를 등록하기 위해 필요한 상품입니다.</td>
								<td width="200px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;장수선택</td>
								<td><select>
										<option>1장</option>
								</select></td>

							</tr>
							<tr>
								<td rowspan="1"></td>
								<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;장당가격</td>
								<td>상품금액<br> <br></td>
							</tr>
							<tr>
								<td><br>
									<hr width="500px"></td>
							</tr>
						</table>
						<br> <br> <br>
						<hr>

						<p p style="font-size: 10px;">
							- 채용광고상품은 결제, 광고승인이 되면 공고시작날짜부터 적용되는 상품입니다. (기업로고가 등록되어 있지 않을 경우
							로고영역에 텍스트로 기업명이 노출됩니다.)<br> - 기업은 포인트가 적용되지 않으므로 참고바랍니다<br>
							- [주의] 유료 상품을 이용 중이신 경우, 공고를 조기 마감하더라도 남은 기간에 대한 차액은 환불되지 않습니다.<br>
							- 구매한 인재열람 상품은 기업인증 완료 후 이용하실 수 있습니다. (이력서 열람제한 기업, 휴폐업 업체 등은 구매
							제한됩니다.)<br> - 인재열람 상품을 통해, 연락처 확인한 인재의 이력서는 열람일부터 90일 동안 재확인
							가능합니다.<br> - 단, 인재 고객이 연락처를 비공개로 전환한 경우, 이력서를 삭제한 경우, 탈퇴한
							경우에는 재확인 불가합니다.<br> - 인재 이력서 열람일로부터 90일이 경과하였으나, 구매 상품 유효기간이
							만료되지 않은 경우에는 상품 유효기간까지 열람한 인재 이력서 재확인이 가능합니다.<br> - 정보통신망법
							제24조 2항에 근거하여 인재열람 서비스는 채용을 목적으로 하는 경우에만 이용할 수 있으며,<br> -
							기업의 영업, 마케팅 등 채용 이외의 용도로는 이용할 수 없습니다.<br> - 채용 외 목적으로 이용하실
							경우, 제71조 3에 의거 5년 이하 징역 또는 5,000만 원 이하의 벌금에 처할 수 있습니다.<br> -
							정보통신부 고시 제2005-18호에 근거하여 이력서 등과 같이 개인정보가 담긴 출력·복사물을 불법 유출 및 배포하게
							되면<br> - 법에 따라 책임을 지게 됨을 양지하시기 바랍니다.<br> - 인재열람 상품 이용 중
							사람인 개인정보보호 시스템에 의해, 정상적인 검색 패턴이 아닌 것으로 감지될 경우 일시적으로 이용이 제한되며<br>
							- 반복 감지 시 경고 단계에 따라 서비스 이용 및 상품 구매가 차단됩니다. (이용 제한 된 시간만큼 시간 연장
							불가합니다.)<br> - 기업 확인을 위해 고객센터에서 직접 연락을 드릴 수 있으며, 경우에 따라
							사업자등록증 사본 및 기타 증빙자료를 요청할 수 있습니다.<br> - 상품구매시 사이트적합성에 따라
							승인/반려가 될수있으며 반려가 될시 재수정하여 승인이 되어야 사이트에 업로드가 됩니다.<br> -
							한채용공고나 한사이트에서 같은 채용공고를 중복해서 올릴수 없습니다
						</p>
						<br> <br> <br> <br>
						<div class="btn2" style="text-align: center;"> 
							<button type="submit">결제하기</button>
						</div>
						<!-- <p>아임 서포트 결제 모듈 테스트 해보기</p>
							<button id="check_module" type="button">아임 서포트 결제 모듈 테스트 해보기</button> -->

					</div>
				</div>

			</div>
			<br>
		</section>

	</div>
	<%-- <% } else {
		request.setAttribute("msg", "잘못된 경로로 접근하셨습니다.");
		request.getRequestDispatcher("../common/errorPage.jsp").forward(request, response);
		%>
	} --%>
	<!-- 전체 화면 div 끝 -->

	<script>
					$(function() {
						$(".ksb_board").slideUp();

						$(".more").click(function() {
							//$(this).next("p").slideDown();

							$(this).next("div").slideToggle(100, function() {
							});
						});
					});
				     function goDateil(){
				            var menu=$('#cho_dateil');
				              menu.css("color","#5A84F1");
				             // location.href="";
				        }
				        function goResumeWriter(){ 
				            var menu=$('#cho_resumewri');
				              menu.css("color","#5A84F1");
				             // location.href="";
				        }
				        function goResume(){
				            var menu=$('#cho_resume');
				              menu.css("color","#5A84F1");
				             // location.href="";
				        }
				        function goContestApp(){
				            var menu=$('#cho_da1');
				              menu.css("color","#5A84F1");
				             // location.href="";
				        }
				        function goRecruit(){
				            var menu=$('#cho_recruit');
				              menu.css("color","#5A84F1");
				             // location.href="";
				        }
				        function goProposed(){
				            var menu=$('#cho_proposed');
				              menu.css("color","#5A84F1");
				             // location.href="";
				        }
				        function goPortfolio(){
				            var menu=$('#cho_portfolio');
				              menu.css("color","#5A84F1");
				             // location.href="";
				        }
				        function goWrited(){
				            var menu=$('#cho_writed');
				              menu.css("color","#5A84F1");
				             // location.href="";
				        }
				        function goBookMark(){
				            var menu=$('#cho_bookMark');
				              menu.css("color","#5A84F1");
				             // location.href="";
				        }

					$(function() {
						$(".item2").hide();
						$(".item3").hide();
						$("#pro2").click(function() {
							$(".item1").hide();
							$(".item3").hide();
							$(".item2").show();
						});
						$("#pro3").click(function() {
							$(".item1").hide();
							$(".item2").hide();
							$(".item3").show();
						});
						$("#pro1").click(function() {
							$(".item2").hide();
							$(".item3").hide();
							$(".item1").show();
						});
						$(".member-type-com").click(function() {
							$(".change-per").hide();
							$(".change-com").show();
						});
					});
					
					//결제 API 시작
					$(".btn2").click(function() {
						var IMP = window.IMP; // 생략가능
						IMP.init('imp14728960');
						// 'iamport' 대신 부여받은 "가맹점 식별코드"를 사용
						// i'mport 관리자 페이지 -> 내정보 -> 가맹점식별코드
						var item1 = $("#item1").val();
						console.log(item1)
						var item2 = $("#item2").val();
						var item3 = $("input:radio[name='select']:checked").val();
						var date = $("#date").val();
						var id = '<%=loginUser.getComNo()%>';
						<%-- location.href = "<%=request.getContextPath()%>/test.it?id=" + id; --%>
						IMP.request_pay({
							pg : 'inicis', // version 1.1.0부터 지원.
							/*
							'kakao':카카오페이,
							html5_inicis':이니시스(웹표준결제)
							'nice':나이스페이
							'jtnet':제이티넷
							'uplus':LG유플러스
							'danal':다날
							'payco':페이코
							'syrup':시럽페이
							'paypal':페이팔
							 */
							pay_method : 'card',
							/*
							'samsung':삼성페이,
							'card':신용카드,
							'trans':실시간계좌이체,
							'vbank':가상계좌,
							'phone':휴대폰소액결제
							 */
							merchant_uid : 'merchant_' + new Date().getTime(),
							/*
							merchant_uid에 경우
							https://docs.iamport.kr/implementation/payment
							위에 url에 따라가시면 넣을 수 있는 방법이 있습니다.
							참고하세요.
							나중에 포스팅 해볼게요.
							 */
							name : '주문명:결제test',
							//결제창에서 보여질 이름
							amount : 100,
							//가격
							 buyer_email : '<%=loginUser.getEmail()%>',
							buyer_name : '<%=loginUser.getComName()%>',
							buyer_tel : '<%=loginUser.getPhone()%>',
							buyer_addr : '<%=loginUser.getComAddress()%>',
							buyer_postcode : '<%=loginUser.getComNum()%>', 
							m_redirect_url : 'https://www.yourdomain.com/payments/complete'
						/*
						모바일 결제시,
						결제가 끝나고 랜딩되는 URL을 지정
						(카카오페이, 페이코, 다날의 경우는 필요없음. PC와 마찬가지로 callback함수로 결과가 떨어짐)
						 */
						}, function(rsp) {
							//console.log(rsp);
							if (rsp.success) {
								$.ajax ({
									url : "<%=request.getContextPath()%>/test.it",  /* 가맹점 서버 */
									type : "post",
									dataType : 'json',
									data : {
										imp_uid : rsp.imp_uid,
										merchant_uid : rsp.merchant_uid,
										item3 : item3,
										date : date,
										id : id
									}
									
									}).done(function (rsp) {
									 alert("ajax done");
									console.log("ajax done");
									//가맹점 서버 결제 API 성공시 로직
								});
									 var msg = '결제가 완료되었습니다.';
									/* msg += '고유ID : ' + rsp.imp_uid;
									msg += '상점 거래ID : ' + rsp.merchant_uid;
									msg += '결제 금액 : ' + rsp.paid_amount;
									msg += '카드 승인번호 : ' + rsp.apply_num; */
									alert(msg);
							} else {
								var msg = '결제에 실패하였습니다.';
								msg += '에러내용 : ' + rsp.error_msg;
							}
							//alert(msg);
						});
					});
					//결제 API 끝
				</script>
	<!-- <footer id="footer" class="test1">footer</footer> -->
</body>
	<%@ include file="../../common/cho_footer.jsp"%>


</html>