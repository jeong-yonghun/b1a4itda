<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <style>
         div{
            text-align: center;
            color:black;
        }
 
        
        header{
            margin-left: auto;
            margin-right: auto;
            width:1400px;
            height:100px;
        }
        #contents{
            margin-left: auto;
            margin-right: auto;
            width:1400px;
            height:1000px;
            background-color: black;
        }
       
       #footerArea{
        margin-left: auto;
            margin-right: auto;
            width:1400px;
            height:150px;  
       }
        * text{
       font-family: 'Noto Sans KR', sans-serif; 
       } 
    </style>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>
   
     <%@ include file="views/common/menubar.jsp" %> 


    <div id="contents" class="test1"><!-- 전체 화면 div 시작 -->
        <div id="bodyArea1" style="background-color: #BFBFBF;width: 100%; height: 45%;"><!--회색영역시작-->
            <div id="mainText"style=" display:inline-block; width: 40%; height: 80%;margin-top: 60px;margin-right:100px;"><!--회색영역 텍스트 시작-->
              <img src="image/mainText.PNG" style="width: 90%;height: 100%;">
            </div><!--회색영역 텍스트 끝-->
            <div id="mainImage" style="display: inline-block; width: 40%; height: 80%;"><!--회색영역 이미지 시작-->
                <img src="image/mainImg.PNG"style="width:90%;height: 100%">
            </div><!--회색영역 이미지 끝-->
        
        </div><!--회색영역끝-->
        <div id="bodyArea2"style="width:100%;height:20%; background-color:white;"><!--메인의 중간 흰페이지 시작-->
            

        </div><!--메인의 중간 흰페이지 끝-->
        <div id="bodyArea3" style="height: 35%;width: 100%;background-color: #5A84F1;"><!--메인하단의 통계영역시작-->
            <!--등록된공고영역 시작-->
            <div id="countArea1" style="width: 32%;height: 90%; padding-top:40px; display: inline-block;">
                <img src="image/mainbottomArea1.PNG" style="margin-bottom: 30px;">
                <h1 style="color: #E1DC5E;text-align: center;margin-bottom: 30px;" id="cho_count1"></h1>
                <h3 style="text-align: center; color:white;margin-top: 40px;">등록된 공고</h3>
            
            </div><!--등록된공고영역 끝-->
            <!--등록된 인재 영역 시작-->
            <div id="countArea2"style="width: 32%;height: 90%; padding-top:40px; display: inline-block;">
                <img src="image/mainbottomArea2.PNG" style="margin-bottom: 36px;">
                <h1 style="color: #E1DC5E;text-align: center;margin-bottom: 30px;"id="cho_count2"></h1>
                <h3 style="text-align: center; color:white;margin-top: 40px;">등록된 인재</h3>
            </div><!--등록된 인재영역 끝-->
            <!--채용된 인재영역 시작-->
            <div id="countArea3"style="width: 32%;height: 90%; padding-top:48px; display: inline-block;">
                <img src="image/mainbottomArea3.PNG" style="margin-bottom: 40px;">
                <h1 style="color: #E1DC5E;text-align: center;margin-bottom: 30px;"id="cho_count3"></h1>
                <h3 style="text-align: center; color:white; margin-top: 40px;">등록된 공고</h3>
              
            </div><!--채용된 인재영역 끝-->

        </div><!--메인하단의 통계영역 끝-->
    </div><!-- 전체 화면 div 끝 -->
 <%@ include file="views/common/cho_footer.jsp" %> 
 
	<script>
    	$(function(){
    		<% if(loginUser == null){ %>
    			
    			$("#nickName").text("로그인");
    		
    		<%}else{ %>
    			
    			 $("#nickName").text("<%=loginUser.getPerName()%>");
    			
    		<%
    		}
    		%>
    		//등록된 인재, 공고, 통계 수 받아오는  ajax
    		
    		//등록공고
    		$.ajax({
    			url: "count1.do",
    			type: "get",
    			success: function(data){
    				console.log(data);
    				$("#cho_count1").text(data);
    				
    			},
    			error: function(request){
    				console.log(request);
    			}
    		});
    		//이력서 갯수
    		$.ajax({
    			url: "count2.do",
    			type: "get",
    			success: function(data){
    				console.log(data);
    				$("#cho_count2").text(data);
    			},
    			error:function(request){
    				console.log(request);
    			}
    			
    		});
    		//매칭 수
    		$.ajax({
    			url: "count3.do",
    			type: "get",
    			success: function(data){
    				console.log(data);
    				$("#cho_count3").text(data);
    			},
    			error:function(request){
    				console.log(request);
    			}
    		});
    	});
    </script>
</body>
</html>